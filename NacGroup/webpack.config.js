﻿"use strict";

module.exports = [
    {
        entry: "./CmsApp/app.js",
        output: {
            path: __dirname,
            filename: "./wwwroot/js/admin/bundle.js"
        },
        module: {
            rules: [
                {
                    test: /\.(js|jsx)$/,
                    exclude: /node_modules/,
                    use: {
                        loader: "babel-loader",
                        options: {
                            presets: ["@babel/preset-env", "@babel/preset-react"]
                        }
                    }
                }
            ]
        }
    },
    {
        entry: "./ProfileApp/app.js",
        output: {
            path: __dirname,
            filename: "./wwwroot/js/profile.js"
        },
        module: {
            rules: [
                {
                    test: /\.(js|jsx)$/,
                    exclude: /node_modules/,
                    use: {
                        loader: "babel-loader",
                        options: {
                            presets: ["@babel/preset-env", "@babel/preset-react"]
                        }
                    }
                }
            ]
        }
    },
    {
        entry: "./AppointmentApp/app.js",
        output: {
            path: __dirname,
            filename: "./wwwroot/js/appointment/bundle.js"
        },
        module: {
            rules: [
                {
                    test: /\.(js|jsx)$/,
                    exclude: /node_modules/,
                    use: {
                        loader: "babel-loader",
                        options: {
                            presets: ["@babel/preset-env", "@babel/preset-react"]
                        }
                    }
                }
            ]
        }
    }
];
