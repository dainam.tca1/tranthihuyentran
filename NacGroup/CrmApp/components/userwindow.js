﻿import React, { Component } from "react";
import AppContext from "./app-context";
import {
    hi,
    change_pw_btn,
    myprofile,
    setting_account,
    sign_out,
} from "../modules/Core/models/corestaticmessage";
class userWindow extends Component {
  render() {
    return (
        <div className="kt-header__topbar-item kt-header__topbar-item--user">
            <div
                className="kt-header__topbar-wrapper"
                data-toggle="dropdown"
                data-offset="0px,0px"
            >
                <div className="kt-header__topbar-user">
                    <span className="kt-header__topbar-welcome kt-hidden-mobile">{hi[this.context.Language]},</span>
                    <span className="kt-header__topbar-username kt-hidden-mobile">Admin</span>
                    <img src="/adminstatics/global/img/default-user.jpg" />
                    <span className="kt-hidden kt-badge kt-badge--username kt-badge--unified-success kt-badge--lg kt-badge--rounded kt-badge--bold">S</span>
                </div>
            </div>
            <div className="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround dropdown-menu-xl">
                <div
                    className="kt-user-card kt-user-card--skin-dark kt-notification-item-padding-x"
                    style={{backgroundImage:"url('/adminstatics/global/img/bg-1.jpg')"}}
                >
                    <div className="kt-user-card__avatar">
                        <img src="/adminstatics/global/img/default-user.jpg" />
                        <span className="kt-hidden kt-badge kt-badge--lg kt-badge--rounded kt-badge--bold kt-font-success">S</span>
                    </div>
                    <div className="kt-user-card__name">Admin</div>
                    <div className="kt-user-card__badge">
                        <a href="/admin/account/ChangePasswordAdmin">
                            <span className="btn btn-success btn-sm btn-bold btn-font-md">{change_pw_btn[this.context.Language]}</span>
                        </a>
                    </div>
                </div>

                <div className="kt-notification">
                    <a href="#" className="kt-notification__item">
                        <div className="kt-notification__item-icon">
                            <i className="flaticon2-calendar-3 kt-font-success" />
                        </div>
                        <div className="kt-notification__item-details">
                            <div className="kt-notification__item-title kt-font-bold">
                                {myprofile[this.context.Language]}
                                    </div>
                            <div className="kt-notification__item-time">
                                {setting_account[this.context.Language]}
                                    </div>
                        </div>
                    </a>
                    <div className="kt-notification__custom kt-space-between">
                        <a
                            href="/account/logout"
                            className="btn btn-label btn-label-brand btn-sm btn-bold"
                        >
                            {sign_out[this.context.Language]}
                                  </a>
                    </div>
                </div>
            </div>
        </div>
    );
  }
}

userWindow.contextType = AppContext;
export default userWindow;
