﻿import React, { Component } from "react";
import { Link } from "react-router-dom";
import AppContext from "./app-context";
import moment from "moment";
const api = "/cms/api/orderhistories";
class Home extends Component {
    constructor() {
        super();
        var that = this;
        this.ChartBox = React.createRef();
        that.state = {
            model: null,
            ex: {
                Title: "Statistics overview",
                Info: {
                    TotalOrder: 0,
                    TotalAmount: 0,
                    TotalProduct: 0,
                    TotalContact: 0,
                },
                Chart: function () { },   
                InfoChart: [],
                ProductSellingList: []
            }
        };

        that.setState(that.state);
        KTApp.blockPage();
        $.get(api + "/getcount", (response) => {
            if (response.status == "success") {
                that.state.model = response.data.Value;
                that.state.ex.Info.TotalOrder = that.state.model.OrderCount;
                that.state.ex.Info.TotalAmount = that.state.model.TotalSales;
            }
            that.setState(that.state);
        });   
        $.ajax({
            url: `/cms/api/orderhistories/today`,
            type: "GET",
            dataType: "json",
            contentType: "application/json",
            success: response => {
                KTApp.unblockPage();

                if (response.status == "success") {
                   
                    response.data.Value.Labels.forEach((label, index) => {
                        that.state.ex.InfoChart.push({
                            label: label,
                            Total: response.data.Value.TotalSalesList[0][index]
                        })
                    });
                    that.state.ex.ProductSellingList = response.data.Value.ProductBestSelling;
                    that.setState(that.state);
                    that.initChart(that.ChartBox.current, that.state.ex.InfoChart, 1000);

                }

            },
            error: function (er) {
                toastr["error"]("Error", "error");
            }
        });
       
    }
    GetChartToday() {
        var that = this
        that.ClearChart();
        KTApp.blockPage();
        $.ajax({
            url: `/cms/api/orderhistories/today`,
            type: "GET",
            dataType: "json",
            contentType: "application/json",
            success: response => {
                KTApp.unblockPage();

                if (response.status == "success") {

                    response.data.Value.Labels.forEach((label, index) => {
                        that.state.ex.InfoChart.push({
                            label: label,
                            Total: response.data.Value.TotalSalesList[0][index]
                        })
                    })
                  
                    that.state.ex.ProductSellingList = response.data.Value.ProductBestSelling;
                    that.setState(that.state);
                    that.initChart(that.ChartBox.current, that.state.ex.InfoChart, 1000);

                }

            },
            error: function (er) {
                toastr["error"]("Error", "error");
            }
        });
       
    }
    GetChartYesterday() {
        var that = this
        that.ClearChart();
        KTApp.blockPage();
        $.ajax({
            url: `/cms/api/orderhistories/yesterday`,
            type: "GET",
            dataType: "json",
            contentType: "application/json",
            success: response => {
                KTApp.unblockPage();
                if (response.status == "success") {
                    response.data.Value.Labels.forEach((label, index) => {
                        that.state.ex.InfoChart.push({
                            label: label,
                            Total: response.data.Value.TotalSalesList[0][index]
                        })
                    });
                    that.state.ex.ProductSellingList = response.data.Value.ProductBestSelling;
                    that.setState(that.state);
                    that.initChart(that.ChartBox.current, that.state.ex.InfoChart, 1000);

                }

            },
            error: function (er) {
                toastr["error"]("Error", "error");
            }
        });
       
    }
    GetChartLastWeek() {
        var that = this
        that.ClearChart();
        KTApp.blockPage();
        $.ajax({
            url: `/cms/api/orderhistories/lastweek`,
            type: "GET",
            dataType: "json",
            contentType: "application/json",
            success: response => {
                KTApp.unblockPage();
                if (response.status == "success") {
                    response.data.Value.Labels.forEach((label, index) => {
                        that.state.ex.InfoChart.push({
                            label: label,
                            Total: response.data.Value.TotalSalesList[0][index]
                        })
                       
                        
                    })
                    that.state.ex.ProductSellingList = response.data.Value.ProductBestSelling;
                    that.setState(that.state);
                    that.initChart(that.ChartBox.current, that.state.ex.InfoChart, 1000);

                }

            },
            error: function (er) {
                toastr["error"]("Error", "error");
            }
        });
       
    }
    GetChartMonth() {
        var that = this
        that.ClearChart();
        KTApp.blockPage();
        $.ajax({
            url: `/cms/api/orderhistories/month`,
            type: "GET",
            dataType: "json",
            contentType: "application/json",
            success: response => {
                KTApp.unblockPage();
                if (response.status == "success") {
                    response.data.Value.Labels.forEach((label, index) => {
                        that.state.ex.InfoChart.push({
                            label: label,
                            Total: response.data.Value.TotalSalesList[0][index]
                        })
                    });
                 
                    that.state.ex.ProductSellingList = response.data.Value.ProductBestSelling;
                    that.setState(that.state);
                    that.initChart(that.ChartBox.current, that.state.ex.InfoChart, 1000);

                }

            },
            error: function (er) {
                toastr["error"]("Error", "error");
            }
        });
        
    }
    GetChartLastMonth() {
        var that = this
        that.ClearChart();
        KTApp.blockPage();
        $.ajax({
            url: `/cms/api/orderhistories/lastmonth`,
            type: "GET",
            dataType: "json",
            contentType: "application/json",
            success: response => {
                KTApp.unblockPage();
                if (response.status == "success") {
                    response.data.Value.Labels.forEach((label, index) => {
                        that.state.ex.InfoChart.push({
                            label: label,
                            Total: response.data.Value.TotalSalesList[0][index]
                        })
                    });
                 
                    that.state.ex.ProductSellingList = response.data.Value.ProductBestSelling;
                    that.setState(that.state);
                    that.initChart(that.ChartBox.current, that.state.ex.InfoChart, 1000);

                }

            },
            error: function (er) {
                toastr["error"]("Error", "error");
            }
        });
        
    }
    ClearChart() {
        var that = this;
        that.state.ex.InfoChart = [];
        $(that.ChartBox.current).empty();
        that.state.ex.Chart = function () { };
        that.setState(that.state);
    }
    initChart(id, datalist,delay) {
        var that = this;
        if (datalist != null || datalist.length > 0) {
            setTimeout(function () {
                that.state.ex.Chart = new Morris.Bar({
                    element: id,
                    data: datalist,
                    xkey: 'label',
                    ykeys: ['Total'],
                    labels: ['Total Amount'],
                    barColors: ['#fd397a']
                });
            }, delay)
        }
        
    }
    componentWillMount() {
        $("#cssloading").html(
            `<link href="/adminstatics/global/plugins/morris.js/morris.css" rel="stylesheet" type="text/css" />`
        );
        $("#scriptloading").html(
            `<script src="/adminstatics/global/plugins/raphael/raphael.js" type="text/javascript"></script><script src="/adminstatics/global/plugins/morris.js/morris.js" type="text/javascript"></script>`
        );
    }
    componentDidMount() {
        document.title = "Dashboard";
        document.title = this.state.ex.Title;
        $("#kt_aside_menu .kt-menu__item").removeClass("kt-menu__item--active");
        $("#kt_aside_menu .kt-menu__item[data-id='dashboard']").addClass(
            "kt-menu__item--active"
        );

        var that = this;
        var price = 0;
        $.get("/cms/api/product/get", (response) => {
            KTApp.unblockPage();
            if (response.status == "success") {
                that.state.ex.Info.TotalProduct = response.data.length;
            }
            that.setState(that.state);
        });
        $.get("/cms/api/contact/get", (response) => {
            KTApp.unblockPage();
            if (response.status == "success") {
                that.state.ex.Info.TotalContact = response.data.length;
            }
            that.setState(that.state);
        });
       
        this.setState(this.state);
    }
    render() {
        return (
            <React.Fragment>
                {this.state && this.state.ex ? (
                    <React.Fragment>
                        <div className="kt-subheader kt-grid__item" id="kt_subheader">
                            <div className="kt-subheader__main">
                                <div className="kt-subheader__breadcrumbs">
                                    <Link
                                        to="/admin/dashboard"
                                        className="kt-subheader__breadcrumbs-home"
                                    >
                                        <i className="fa fa-home" />
                                    </Link>
                                    <span className="kt-subheader__breadcrumbs-separator" />
                                    <span className="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
                                        {this.state.ex.Title}
                                    </span>
                                </div>
                            </div>
                            <div className="kt-subheader__toolbar">
                                <div className="kt-subheader__wrapper">

                                </div>
                            </div>
                        </div>
                        <div
                            className="kt-content kt-grid__item kt-grid__item--fluid"
                            id="kt_content"
                        >
                            <div className="kt-portlet">
                                <div className="kt-portlet__body  kt-portlet__body--fit">
                                    <div className="row row-no-padding row-col-separator-xl">
                                        <div className="col-md-12 col-lg-6 col-xl-3">
                                            <div className="kt-widget24">
                                                <div className="kt-widget24__details">
                                                    <div className="kt-widget24__info">
                                                        <h4 className="kt-widget24__title">
                                                            Total Revenue

														</h4>
                                                        <span className="kt-widget24__desc">
                                                            All Customs Value
														</span>
                                                    </div>
                                                    <span className="kt-widget24__stats kt-font-brand">
                                                        {this.context.CurrencyType === 0 ? (
                                                            this.state.ex.Info.TotalAmount ? this.state.ex.Info.TotalAmount.formatMoney(
                                                                0,
                                                                ".",
                                                                ",",
                                                                "$"
                                                            ) : "0"
                                                        ) : (
                                                                this.state.ex.Info.TotalAmount ? this.state.ex.Info.TotalAmount.formatMoney(
                                                                    0,
                                                                    ",",
                                                                    "."
                                                                ) : "0"
                                                            )}
                                                    </span>
                                                </div>
                                                
                                            </div>
                                        </div>
                                        <div className="col-md-12 col-lg-6 col-xl-3">
                                            <div className="kt-widget24">
                                                <div className="kt-widget24__details">
                                                    <div className="kt-widget24__info">
                                                        <h4 className="kt-widget24__title">
                                                            Orders
														</h4>
                                                        <span className="kt-widget24__desc">
                                                            All Customs Value
														</span>
                                                    </div>
                                                    <span className="kt-widget24__stats kt-font-warning">
                                                        {this.state.ex.Info.TotalOrder}
                                                    </span>
                                                </div>
                                               
                                            </div>
                                        </div>
                                        <div className="col-md-12 col-lg-6 col-xl-3">
                                            <div className="kt-widget24">
                                                <div className="kt-widget24__details">
                                                    <div className="kt-widget24__info">
                                                        <h4 className="kt-widget24__title">
                                                            Products
														</h4>
                                                        <span className="kt-widget24__desc">
                                                            All Customs Value
														</span>
                                                    </div>
                                                    <span className="kt-widget24__stats kt-font-danger">
                                                        {this.state.ex.Info.TotalProduct}
                                                    </span>
                                                </div>
                                               
                                            </div>
                                        </div>
                                        <div className="col-md-12 col-lg-6 col-xl-3">
                                            <div className="kt-widget24">
                                                <div className="kt-widget24__details">
                                                    <div className="kt-widget24__info">
                                                        <h4 className="kt-widget24__title">
                                                            Contacts
														</h4>
                                                        <span className="kt-widget24__desc">
                                                            All Customs Value
														</span>
                                                    </div>
                                                    <span className="kt-widget24__stats kt-font-success">
                                                        {this.state.ex.Info.TotalContact}
                                                    </span>
                                                </div>
                                            
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="kt-portlet kt-portlet--tab">
                                <div className="kt-portlet__head">
                                    <div className="kt-portlet__head-label">
                                        <span className="kt-portlet__head-icon kt-hidden">
                                            <i className="la la-gear"></i>
                                        </span>
                                        <h3 className="kt-portlet__head-title">
                                            Bar Chart
												</h3>
                                    </div>
                                </div>
                                <div className="kt-portlet__body">
                                    {this.state.model ? (
                                        <ul className="nav nav-tabs  nav-tabs-line" role="tablist">
                                            <li className="nav-item">
                                                <a
                                                    className="nav-link active"
                                                    data-toggle="tab"
                                                    href="#kt_tabs_1_1"
                                                    onClick={e => {
                                                        this.GetChartToday()
                                                    }}
                                                    role="tab">
                                                    <span>Today</span>
                                                    <span>
                                                        {this.context.CurrencyType === 0 ? (
                                                            this.state.model.TotalSalesToday ? this.state.model.TotalSalesToday.formatMoney(
                                                                0,
                                                                ".",
                                                                ",",
                                                                "$"
                                                            ) : "0"
                                                        ) : (
                                                                this.state.model.TotalSalesToday ? this.state.model.TotalSalesToday.formatMoney(
                                                                    0,
                                                                    ",",
                                                                    "."
                                                                ) : "0"
                                                            )}
                                                    </span>
                                                    <span>{this.state.model.OrderCountToday} order</span>
                                                </a>
                                            </li>
                                            <li className="nav-item">
                                                <a className="nav-link" data-toggle="tab" href="#kt_tabs_1_3" role="tab" onClick={e => {
                                                    this.GetChartYesterday();
                                                }}>
                                                    <span>Yesterday</span>
                                                    <span>
                                                        {this.context.CurrencyType === 0 ? (
                                                            this.state.model.TotalSalesYesterday ? this.state.model.TotalSalesYesterday.formatMoney(
                                                                0,
                                                                ".",
                                                                ",",
                                                                "$"
                                                            ) : "0"
                                                        ) : (
                                                                this.state.model.TotalSalesYesterday ? this.state.model.TotalSalesYesterday.formatMoney(
                                                                    0,
                                                                    ",",
                                                                    "."
                                                                ) : "0"
                                                            )}
                                                    </span>
                                                    <span>{this.state.model.OrderCountYesterday} order</span>
                                                </a>
                                            </li>
                                            <li className="nav-item">
                                                <a className="nav-link" data-toggle="tab" href="#kt_tabs_1_3" role="tab" onClick={e => {
                                                    this.GetChartLastWeek();
                                                }}>
                                                    <span>7 days ago</span>
                                                    <span>
                                                        {this.context.CurrencyType === 0 ? (
                                                            this.state.model.TotalSalesLastweek ? this.state.model.TotalSalesLastweek.formatMoney(
                                                                0,
                                                                ".",
                                                                ",",
                                                                "$"
                                                            ) : "0"
                                                        ) : (
                                                                this.state.model.TotalSalesLastweek ? this.state.model.TotalSalesLastweek.formatMoney(
                                                                    0,
                                                                    ",",
                                                                    "."
                                                                ) : "0"
                                                            )}
                                                    </span>
                                                    <span>{this.state.model.OrderCountLastweek} order</span>
                                                </a>
                                            </li>
                                            <li className="nav-item">
                                                <a className="nav-link" data-toggle="tab" href="#kt_tabs_1_3" role="tab" onClick={e => {
                                                    this.GetChartMonth();
                                                }}>
                                                    <span>This month</span>
                                                    <span>
                                                        {this.context.CurrencyType === 0 ? (
                                                            this.state.model.TotalSalesMonth ? this.state.model.TotalSalesMonth.formatMoney(
                                                                0,
                                                                ".",
                                                                ",",
                                                                "$"
                                                            ) : "0"
                                                        ) : (
                                                                this.state.model.TotalSalesMonth ? this.state.model.TotalSalesMonth.formatMoney(
                                                                    0,
                                                                    ",",
                                                                    "."
                                                                ) : "0"
                                                            )}
                                                    </span>
                                                    <span>{this.state.model.OrderCountMonth} order</span>
                                                </a>
                                            </li>
                                            <li className="nav-item">
                                                <a className="nav-link" data-toggle="tab" href="#kt_tabs_1_3" role="tab" onClick={e => {
                                                    this.GetChartLastMonth();
                                                }}>
                                                    <span>Last month</span>
                                                    <span>
                                                        {this.context.CurrencyType === 0 ? (
                                                            this.state.model.TotalSalesLastMonth ? this.state.model.TotalSalesLastMonth.formatMoney(
                                                                0,
                                                                ".",
                                                                ",",
                                                                "$"
                                                            ) : "0"
                                                        ) : (
                                                                this.state.model.TotalSalesLastMonth ? this.state.model.TotalSalesLastMonth.formatMoney(
                                                                    0,
                                                                    ",",
                                                                    "."
                                                                ) : "0"
                                                            )}
                                                    </span>
                                                    <span>{this.state.model.OrderCountLastMonth} order</span>
                                                </a>
                                            </li>
                                        </ul>
                                    ): ""}
                                    <div className="tab-content">
                                        <div className="row">
                                            <div className="col-md-10">
                                                <div className="kt-portlet__head">
                                                    <div className="kt-portlet__head-label">
                                                        <h3 className="kt-portlet__head-title">
                                                            Total Revenue
												        </h3>
                                                    </div>
                                                </div>
                                                <div id="kt_morris_3" ref={this.ChartBox} style={{ "height": "500px" }}></div>
                                            </div>
                                            <div className="col-md-2">
                                                <div className="kt-portlet__head">
                                                    <div className="kt-portlet__head-label">
                                                        <h3 className="kt-portlet__head-title">
                                                            Best Sellers
												        </h3>
                                                    </div>
                                                </div>
                                                <div className="kt-notification-v2">
                                                    {this.state.ex.ProductSellingList ? this.state.ex.ProductSellingList.map(product => {
                                                        return (
                                                            <a href="javascript:;" className="kt-notification-v2__item">
                                                                <div className="kt-notification-v2__item-icon">
                                                                    <img width="50px" src={product.Avatar} />
                                                                </div>
                                                                <div className="kt-notification-v2__itek-wrapper">
                                                                    <div className="kt-notification-v2__item-title">
                                                                        {product.Name}
                                                                    </div>
                                                                    <div className="kt-notification-v2__item-desc">
                                                                        (sold {product.Quantity} products)
                                                                </div>
                                                                </div>
                                                            </a>
                                                        )
                                                    }) : ""}


                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </React.Fragment>
                ) : ""}
            </React.Fragment>
        );
    }
}
Home.contextType = AppContext;
export default Home;
