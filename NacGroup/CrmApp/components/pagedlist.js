import React, { Component } from "react";
class PagedList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Start: this.props.currentpage - 5 >= 1 ? this.props.currentpage - 5 : 1,
      End:
        this.props.currentpage + 5 <= this.props.totalpagecount
          ? this.props.currentpage + 5
          : this.props.totalpagecount
    };
    this.setState(this.state);
  }
  tohandPage(index, e) {
    var { ajaxcallback } = this.props;
    ajaxcallback(index);
  }
  componentWillReceiveProps(nextProps) {
    if (
      nextProps.currentpage != this.props.currentpage ||
      nextProps.totalpagecount != this.props.totalpagecount
    ) {
      this.state = {
        Start: nextProps.currentpage - 5 >= 1 ? nextProps.currentpage - 5 : 1,
        End:
          nextProps.currentpage + 5 <= nextProps.totalpagecount
            ? nextProps.currentpage + 5
            : nextProps.totalpagecount
      };
      this.setState(this.state);
    }
  }
  render() {
    var ll = [];
    for (var i = this.state.Start; i <= this.state.End; i++) {
      ll.push(i);
    }
    return (
      <div className="dataTables_paginate paging_simple_numbers">
        <ul className="pagination">
          <li
            className={
              "paginate_button page-item previous" +
              (this.props.currentpage > 2 ? "" : " disabled")
            }
            id="kt_table_1_previous"
          >
            <a
              href="javascript:"
              onClick={e => {
                if (this.props.currentpage > 2) {
                  this.tohandPage(1, e);
                }
              }}
              aria-controls="kt_table_1"
              data-dt-idx="0"
              tabindex="0"
              className="page-link"
            >
              <i className="la la-angle-double-left"></i>
            </a>
          </li>

          <li
            className={
              "paginate_button page-item previous" +
              (this.props.currentpage > 1 ? "" : " disabled")
            }
            id="kt_table_1_previous"
          >
            <a
              href="javascript:"
              onClick={e => {
                if (this.props.currentpage > 1) {
                  this.tohandPage(this.props.currentpage - 1, e);
                }
              }}
              aria-controls="kt_table_1"
              data-dt-idx="0"
              tabindex="0"
              className="page-link"
            >
              <i className="la la-angle-left"></i>
            </a>
          </li>

          {this.state.Start > 1 && (
            <li v-if="Start > 1" className="paginate_button page-item disabled">
              <a className="page-link">...</a>
            </li>
          )}
          {ll.map((e, i) => {
            var isactive = this.props.currentpage == e ? " active" : "";
            return (
              <li className={"paginate_button page-item" + isactive}>
                {isactive ? (
                  <a className="page-link">{e}</a>
                ) : (
                  <a
                    onClick={ev => this.tohandPage(e, ev)}
                    href="javascript:"
                    className="page-link"
                  >
                    {e}
                  </a>
                )}
              </li>
            );
          })}
          {this.state.End < this.props.totalpagecount && (
            <li className="paginate_button page-item disabled">
              <a className="page-link">...</a>
            </li>
          )}

          <li
            className={
              "paginate_button page-item previous" +
              (this.props.currentpage < this.props.totalpagecount
                ? ""
                : " disabled")
            }
            id="kt_table_1_previous"
          >
            <a
              href="javascript:"
              onClick={e => {
                if (this.props.currentpage < this.props.totalpagecount) {
                  this.tohandPage(this.props.currentpage + 1, e);
                }
              }}
              aria-controls="kt_table_1"
              data-dt-idx="0"
              tabindex="0"
              className="page-link"
            >
              <i className="la la-angle-right"></i>
            </a>
          </li>

          <li
            className={
              "paginate_button page-item previous" +
              (this.props.currentpage < this.props.totalpagecount - 2
                ? ""
                : " disabled")
            }
            id="kt_table_1_previous"
          >
            <a
              href="javascript:"
              onClick={e => {
                if (this.props.currentpage < this.props.totalpagecount - 2) {
                  this.tohandPage(this.props.totalpagecount, e);
                }
              }}
              aria-controls="kt_table_1"
              data-dt-idx="0"
              tabindex="0"
              className="page-link"
            >
              <i className="la la-angle-double-right"></i>
            </a>
          </li>
        </ul>
      </div>
    );
  }
}
export default PagedList;
