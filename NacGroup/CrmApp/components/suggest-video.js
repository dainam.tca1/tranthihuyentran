import React, { Component } from "react";
import { DelayInput } from "react-delay-input";
import PagedList from "./pagedlist";
import {
    GLOBAL_ERROR_MESSAGE,
    ERROR,
    SUCCESS,
    DATA_NOT_RECOVER
} from "../constants/message";
import { Link } from "react-router-dom";
import {PRODUCTTYPE} from "../constants/enums";
const apiurl = "/cms/api/digitalsignage/videos";
class SuggestVideo extends Component {
    constructor(props) {
        super(props);
        var that = this;
        that.state = {
            ex: {
                Title: "Video List",
                Param: {
                    name: null,
                    pagesize: null,
                    url: null
                }
            }
        };
        that.setState(that.state);
        that.handleChangeFilter = that.handleChangeFilter.bind(that);
        this.toPage(1);
    }

    filter(url) {
        var that = this;
        KTApp.blockPage();
        $.ajax({
            url: url,
            type: "GET",
            dataType: "json",
            contentType: "application/json",
            data: { id: that.props.idDigi },
            success: response => {
                KTApp.unblockPage();
                that.state.model = response.data;
                that.state.model.Results = that.state.model.Results.map(e => {
                    return { ...e, IsChecked: false,Sort:0 };
                });
                if (that.props.videoThumbnail != null) {
                    for (var i = 0; i <= that.state.model.Results.length - 1; i++) {
                        that.props.videoThumbnail.map(e => {
                            if (e._id != null) {
                                if (e._id === that.state.model.Results[i]._id) {
                                    that.state.model.Results[i].IsChecked = true;
                                    that.setState(that.state.model.Results[i]);
                                }
                            }
                        });
                    }
                }
                this.setState(that.state);
            },
            error: function(er) {
                KTApp.unblockPage();
            }
        });
    }
    toPage(index) {
        var paramStr = "";
        if (this.state.ex.Param != null) {
            for (var key in this.state.ex.Param) {
                if (this.state.ex.Param[key] == null) continue;
                if (paramStr !== "") {
                    paramStr += "&";
                }
                paramStr += key + "=" + encodeURIComponent(this.state.ex.Param[key]);
            }
        }
        var url = apiurl + "?" + paramStr + (paramStr ? "&" : "") + "page=" + index;
        this.filter(url);
    }
    /**
     * Được gọi khi thay đổi các trường filter cập nhật tham số filter vào state
     * Filter dữ liệu theo tham số mới
     * @param {Event} event
     */
    handleChangeFilter(event) {
        const target = event.target;
        const value = target.type === "checkbox" ? target.checked : target.value;
        const name = target.name;
        this.state.ex.Param[name] = value;
        this.setState(this.state);
        this.toPage(1);
    }
    handleGetVideoList() {
        var that = this;
        var videoLst = that.state.model.Results.filter(e => {
            return e.IsChecked === true;
        });
        // remove field isChecked
        delete videoLst.IsChecked;
        that.props.onSelected(videoLst);
    }
    componentWillReceiveProps() {}
    render() {
        return (
            <React.Fragment>
                {this.state && this.state.model ? (
                    <React.Fragment>
                        <div
                            id="suggest-modal"
                            className="modal fade modalstyle"
                            role="dialog"
                        >
                            <div className="modal-dialog" style={{ maxWidth: "1000px" }}>
                                <div className="modal-content">
                                    <div className="modal-header">
                                        <h5 className="modal-title" id="exampleModalLabel">Select Video</h5>
                                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                    </div>
                                    <div className="modal-body">
                                        <form className="kt-form kt-form--fit">
                                            <div className="row kt-margin-b-20">
                                                <div className="col-lg-3">
                                                    <label>Name:</label>
                                                    <DelayInput
                                                        delayTimeout={1000}
                                                        className="form-control kt-input"
                                                        placeholder="Name"
                                                        value={this.state.ex.Param.name}
                                                        name="name"
                                                        onChange={this.handleChangeFilter}
                                                    />
                                                </div>
                                                <div className="col-lg-3">
                                                    <label>Url:</label>
                                                    <DelayInput
                                                        delayTimeout={1000}
                                                        className="form-control kt-input"
                                                        placeholder="Url"
                                                        value={this.state.ex.Param.url}
                                                        name="url"
                                                        onChange={this.handleChangeFilter}
                                                    />
                                                </div>
                                                <div className="col-lg-3">
                                                    <label>Record Per Page:</label>
                                                    <select
                                                        className="form-control kt-input"
                                                        value={this.state.ex.Param.pagesize}
                                                        name="pagesize"
                                                        onChange={this.handleChangeFilter}
                                                    >
                                                        <option value="10">10 record / page</option>
                                                        <option value="20">20 record / page</option>
                                                        <option value="50">50 record / page</option>
                                                        <option value="100">100 record / page</option>
                                                    </select>
                                                </div>
                                              </div>
                                        </form>
                                        <div className="kt-separator kt-separator--border-dashed"></div>
                                        <div id="kt_table_1_wrapper" className="dataTables_wrapper dt-bootstrap4">
                                            <div className="row">
                                                <div className="col-sm-12">
                                                    <table className="table table-striped- table-bordered table-hover table-checkable dataTable no-footer dtr-inline table-responsive">
                                                        <thead>
                                                             <tr role="row">
                                                                 <th
                                                                        className="dt-right sorting_disabled"
                                                                        rowSpan="1"
                                                                        colSpan="1"
                                                                        style={{ width: "1%" }}
                                                                        aria-label="Record ID"
                                                                    >
                                                                        <label className="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                                                            <input
                                                                                type="checkbox"
                                                                                className="kt-group-checkable"
                                                                                onChange={ev => {
                                                                                    this.state.model.Results = this.state.model.Results.map(
                                                                                        e => {
                                                                                            return {
                                                                                                ...e,
                                                                                                IsChecked: ev.target.checked
                                                                                            };
                                                                                        }
                                                                                    );
                                                                                    this.setState(this.state);
                                                                                }}
                                                                            />
                                                                            <span></span>
                                                                        </label>
                                                                </th>
                                                                 <th style={{ width: "200px" }}>Avatar</th>
                                                                 <th>Title</th>
                                                                 <th>Url</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        {this.state.model.Results
                                                                ? this.state.model.Results.map(c => {
                                                                    return (
                                                                        <tr>
                                                                            <td className="dt-right" tabIndex="0">
                                                                                <label className="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                                                                    <input
                                                                                        type="checkbox"
                                                                                        className="kt-checkable"
                                                                                        value={c._id}
                                                                                        checked={c.IsChecked}
                                                                                        onChange={e => {
                                                                                            c.IsChecked = !c.IsChecked;
                                                                                            this.setState(this.state);
                                                                                        }}
                                                                                    />
                                                                                    <span></span>
                                                                                </label>
                                                                            </td>
                                                                            <td>
                                                                                <Link to={`/admin/digital/video/update/${c.Id}/${this.props.idDigi}`}>
                                                                                    <img style={{ width: '200px' }} src={c.Avatar} alt="" />
                                                                                </Link>
                                                                            </td>
                                                                            <td>
                                                                                <Link to={`/admin/digital/video/update/${c.Id}/${this.props.idDigi}`}>
                                                                                    {c.Title}
                                                                                </Link>
                                                                            </td>
                                                                            <td><a href={c.Url}>{c.Url}</a></td>
                                                                        </tr>
                                                                        )

                                                                }):""}
                                                                
                                                                </tbody>
                                                     </table>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-sm-12 col-md-5">
                                                    <div className="dataTables_info" id="kt_table_1_info" role="status" aria-live="polite">Total {this.state.model.TotalItemCount} records</div>
                                                </div>
                                                <div className="col-sm-12 col-md-7 dataTables_pager">
                                                    <PagedList
                                                        currentpage={this.state.model.CurrentPage}
                                                        pagesize={this.state.model.PageSize}
                                                        totalitemcount={this.state.model.TotalItemCount}
                                                        totalpagecount={this.state.model.TotalPageCount}
                                                        ajaxcallback={this.toPage.bind(this)}
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                      
                                    
                                    </div>
                                    <div className="modal-footer">
                                        <button
                                            type="button"
                                            className="btn btn-danger  btn-outline"
                                            data-dismiss="modal"
                                        >
                                            Close
                                        </button>
                                        <button
                                            type="button"
                                            className="btn btn-primary"
                                            onClick={e => {
                                                this.handleGetVideoList();
                                            }}
                                        >
                                            Get the selected video
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </React.Fragment>
                ) : (
                    ""
                )}
            </React.Fragment>
        );
    }
}

export default SuggestVideo;
