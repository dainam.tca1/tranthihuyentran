import React, { Component } from "react";

class CkEditor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      editor: null
    };
    this.textinput = React.createRef();
  }
  componentDidMount() {
    var that = this;
    that.state.editor = CKEDITOR.replace(that.textinput.current);
    that.state.editor.on("change", () => {
      that.props.onChange(that.state.editor.getData());
    });
    that.setState(that.state);
  }

  componentWillReceiveProps(nexprops) {
      var that = this;
      if (nexprops.value && that.state.editor.getData() == "" ) {
        that.state.editor.setData(nexprops.value);
        that.setState(that.state);
      }
  }

  componentWillUnmount() {
    this.state.editor.destroy(true);
  }
  render() {
    return <textarea value={this.props.value} ref={this.textinput} />;
  }
}

export default CkEditor;
