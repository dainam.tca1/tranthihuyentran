﻿import React, { Component } from "react";
import { Link } from "react-router-dom";
import AppContext from "./app-context";
import {
    dashboard,
    contact,
    services,
    service_ls,
    service_cat,
    pos_product,
    products,
    product_ls,
    product_cat,
    product_cus_cat,
    product_brands,
    product_att_group,
    product_att_name,
    product_att_value,
    order_history,
    dgt_signage,
    device_ls,
    play_ls,
    video_ls,
    promotion,
    promotion_news,
    coupon_code,
    giftcard_coupon,
    blog,
    blog_ls,
    blog_cat,
    gallery,
    static_pages,
    staff,
    gpos_staff,
    conf_title,
    website_conf,
    booking_setting,
    booking_report,
    order_pickup_setting,
    giftcard_his,
    giftcard_temp_setting,
    theme,
    theme_config,
    menu_conf,
    ship_setting,
    ship_by_dist,
    ship_by_total_bill,
    admin_manager,
    key_configuration,
    comment,
    video
} from "../modules/Core/models/corestaticmessage";
class Nav extends Component {
  render() {
    return (
      <div
        className="kt-aside  kt-aside--fixed  kt-grid__item kt-grid kt-grid--desktop kt-grid--hor-desktop"
        id="kt_aside"
      >
        <div className="kt-aside__brand kt-grid__item" id="kt_aside_brand">
          <div className="kt-aside__brand-logo">
            <a href="/">
              <img src="/adminstatics/global/img/logowebnail.png" height="50" />
            </a>
          </div>
          <div className="kt-aside__brand-tools">
            <button
              className="kt-aside__brand-aside-toggler"
              id="kt_aside_toggler"
            >
              <span>
                <i className="flaticon2-fast-back text-primary" />
              </span>
              <span>
                <i className="flaticon2-fast-next text-primary" />
              </span>
            </button>
          </div>
        </div>

        <div
          className="kt-aside-menu-wrapper kt-grid__item kt-grid__item--fluid"
          id="kt_aside_menu_wrapper"
        >
          <div
            id="kt_aside_menu"
            className="kt-aside-menu"
            data-ktmenu-vertical="1"
            data-ktmenu-scroll="1"
            data-ktmenu-dropdown-timeout="500"
          >
            <ul className="kt-menu__nav">
              <li
                className="kt-menu__item"
                aria-haspopup="true"
                data-id="dashboard"
              >
                <Link to="/admin/dashboard" className="kt-menu__link">
                  <span className="kt-menu__link-icon">
                    <i className="la la-home" />
                  </span>
                  <span className="kt-menu__link-text">{dashboard[this.context.Language]}</span>
                </Link>
                        </li>
                        <li
                            className="kt-menu__item"
                            aria-haspopup="true"
                            data-id="customer-label"
                        >
                            <Link to="/crmadmin/customerlabel" className="kt-menu__link">
                                <span className="kt-menu__link-icon">
                                    <i className="la la-home" />
                                </span>
                                <span className="kt-menu__link-text">Customer Label</span>
                            </Link>
                        </li>
                        <li
                            className="kt-menu__item"
                            aria-haspopup="true"
                            data-id="customer-level"
                        >
                            <Link to="/crmadmin/customerlevel" className="kt-menu__link">
                                <span className="kt-menu__link-icon">
                                    <i className="la la-home" />
                                </span>
                                <span className="kt-menu__link-text">Customer Level</span>
                            </Link>
                        </li>
                        <li
                            className="kt-menu__item"
                            aria-haspopup="true"
                            data-id="customer-order"
                        >
                            <Link to="/crmadmin/customerorder" className="kt-menu__link">
                                <span className="kt-menu__link-icon">
                                    <i className="la la-home" />
                                </span>
                                <span className="kt-menu__link-text">Customer Order</span>
                            </Link>
                        </li>
                        <li
                            className="kt-menu__item"
                            aria-haspopup="true"
                            data-id="order-label"
                        >
                            <Link to="/crmadmin/orderlabel" className="kt-menu__link">
                                <span className="kt-menu__link-icon">
                                    <i className="la la-home" />
                                </span>
                                <span className="kt-menu__link-text">Order Label</span>
                            </Link>
                        </li>

             
            
              
              <li className="kt-menu__section">
                <h4 className="kt-menu__section-text">{conf_title[this.context.Language]}</h4>
                <i className="kt-menu__section-icon flaticon-more-v2" />
              </li>

              <li
                className="kt-menu__item"
                aria-haspopup="true"
                data-id="cau-hinh-website"
              >
                <Link
                  to="/admin/websiteconfig"
                  href="#"
                  className="kt-menu__link"
                >
                  <span className="kt-menu__link-icon">
                    <i className="fa fa-cog" />
                  </span>
                  <span className="kt-menu__link-text">
                    {website_conf[this.context.Language]}
                  </span>
                </Link>
              </li>

              {this.context.FeatureList.find(m => m.Url === "booking") !=
                null && (
                <li
                  className="kt-menu__item"
                  aria-haspopup="true"
                  data-id="booking-setting"
                >
                  <Link
                    to="/admin/servicebookingsetting"
                    href="#"
                    className="kt-menu__link"
                  >
                    <span className="kt-menu__link-icon">
                      <i className="fa fa-cog" />
                    </span>
                    <span className="kt-menu__link-text">{booking_setting[this.context.Language]}</span>
                  </Link>
                </li>
              )}
              {this.context.FeatureList.find(
                m => m.Url === "order-pickup-online"
              ) != null && (
                <li
                  className="kt-menu__item"
                  aria-haspopup="true"
                  data-id="orderpickup-setting"
                >
                  <Link
                    to="/admin/gposshoppingcartsetting"
                    className="kt-menu__link"
                  >
                    <span className="kt-menu__link-icon">
                      <i className="fa fa-cog" />
                    </span>
                    <span className="kt-menu__link-text">
                      {order_pickup_setting[this.context.Language]}
                    </span>
                  </Link>
                </li>
              )}
              {this.context.FeatureList.find(m => m.Url === "gift-card") !=
                null && (
                <React.Fragment>
                  <li
                    className="kt-menu__item"
                    aria-haspopup="true"
                    data-id="giftcard-list"
                  >
                    <Link
                      to="/admin/giftcardhistories"
                      className="kt-menu__link"
                    >
                      <span className="kt-menu__link-icon">
                        <i className="fa fa-cog" />
                      </span>
                      <span className="kt-menu__link-text">
                        {giftcard_his[this.context.Language]}
                      </span>
                    </Link>
                  </li>
                  <li
                    className="kt-menu__item"
                    aria-haspopup="true"
                    data-id="giftcardtemplate-setting"
                  >
                    <Link
                      to="/admin/giftcardtemplate"
                      className="kt-menu__link"
                    >
                      <span className="kt-menu__link-icon">
                        <i className="fa fa-cog" />
                      </span>
                      <span className="kt-menu__link-text">
                        {giftcard_temp_setting[this.context.Language]}
                      </span>
                    </Link>
                  </li>
                </React.Fragment>
              )}
              <li
                className="kt-menu__item  kt-menu__item--submenu"
                aria-haspopup="true"
                data-ktmenu-submenu-toggle="hover"
                data-id="theme"
              >
                <a
                  href="javascript:;"
                  className="kt-menu__link kt-menu__toggle"
                >
                  <span className="kt-menu__link-icon">
                    <i className="la la-photo" />
                  </span>
                  <span className="kt-menu__link-text">{theme[this.context.Language]}</span>
                  <i className="kt-menu__ver-arrow la la-angle-right" />
                </a>
                <div className="kt-menu__submenu">
                  <span className="kt-menu__arrow" />
                  <ul className="kt-menu__subnav">
                    <li
                      className="kt-menu__item"
                      aria-haspopup="true"
                      data-id="theme-config"
                    >
                      <Link to="/admin/themeconfig" className="kt-menu__link">
                        <i className="kt-menu__link-bullet kt-menu__link-bullet--dot">
                          <span />
                        </i>
                        <span className="kt-menu__link-text">{theme_config[this.context.Language]}</span>
                      </Link>
                    </li>
                    <li
                      className="kt-menu__item"
                      aria-haspopup="true"
                      data-id="menu-config"
                    >
                      <Link to="/admin/menuconfig" className="kt-menu__link">
                        <i className="kt-menu__link-bullet kt-menu__link-bullet--dot">
                          <span />
                        </i>
                        <span className="kt-menu__link-text">{menu_conf[this.context.Language]}</span>
                      </Link>
                    </li>
                  </ul>
                </div>
              </li>
              <li
                className="kt-menu__item  kt-menu__item--submenu"
                aria-haspopup="true"
                data-ktmenu-submenu-toggle="hover"
                data-id="shipping-setting"
              >
                <a
                  href="javascript:;"
                  className="kt-menu__link kt-menu__toggle"
                >
                  <span className="kt-menu__link-icon">
                    <i className="la la-truck" />
                  </span>
                  <span className="kt-menu__link-text">{ship_setting[this.context.Language]}</span>
                  <i className="kt-menu__ver-arrow la la-angle-right" />
                </a>
                <div className="kt-menu__submenu">
                  <span className="kt-menu__arrow" />
                  <ul className="kt-menu__subnav">
                    <li
                      className="kt-menu__item"
                      aria-haspopup="true"
                      data-id="feeship-bydistrict"
                    >
                      <Link to="/admin/feeshipbyarea" className="kt-menu__link">
                        <i className="kt-menu__link-bullet kt-menu__link-bullet--dot">
                          <span />
                        </i>
                        <span className="kt-menu__link-text">{ship_by_dist[this.context.Language]}</span>
                      </Link>
                    </li>
                    <li
                      className="kt-menu__item"
                      aria-haspopup="true"
                      data-id="total-shipping"
                    >
                      <Link to="/admin/shippingbybill" className="kt-menu__link">
                        <i className="kt-menu__link-bullet kt-menu__link-bullet--dot">
                          <span />
                        </i>
                        <span className="kt-menu__link-text">{ship_by_total_bill[this.context.Language]}</span>
                      </Link>
                    </li>
                  </ul>
                </div>
                        </li>
              <li
                className="kt-menu__item"
                aria-haspopup="true"
                data-id="danh-sach-admin"
              >
                <Link to="/admin/admin-mannager" className="kt-menu__link">
                  <span className="kt-menu__link-icon">
                    <i className="fa fa-users-cog" />
                  </span>
                  <span className="kt-menu__link-text">{admin_manager[this.context.Language]}</span>
                </Link>
              </li>
              <li
                className="kt-menu__item"
                aria-haspopup="true"
                data-id="danh-sach-key"
              >
                <Link to="/admin/configeditor" className="kt-menu__link">
                  <span className="kt-menu__link-icon">
                    <i className="fa fa-key" />
                  </span>
                  <span className="kt-menu__link-text">{key_configuration[this.context.Language]}</span>
                </Link>
              </li>
            </ul>
          </div>
        </div>
      </div>
    );
  }
}

Nav.contextType = AppContext;
export default Nav;
