import React, { Component } from "react";
import moment from "moment";

class DateTimePicker extends Component {
  constructor(props) {
    super(props);
    this.state = {
      flatpick: null
    };
    this.textinput = React.createRef();
  }
  componentDidMount() {
    var that = this;
    that.state.flatpick = flatpickr(that.textinput.current, {
      ...that.props.options,
      onChange: (selectedDates, dateStr, instance) => {
        that.props.onChange(selectedDates, dateStr, instance);
      },
      defaultDate: that.props.value
    });
    that.setState(that.state);
  }
  componentWillReceiveProps(nexprops) {
    var that = this;
    if (nexprops.value !== that.props.value) {
      that.state.flatpick.setDate(nexprops.value);
      that.setState(that.state);
    }
  }
  componentWillUnmount() {
    this.state.flatpick.destroy();
  }

  render() {
    return (
      <React.Fragment>
        <input
          type="text"
          className="form-control"
          ref={this.textinput}
          placeholder={this.props.placeholder}
          name={this.props.name}
        />
        <a
          className="clear-datebutton"
          title="clear"
          onClick={e => {
            this.state.flatpick.clear();
          }}
        >
          <i className="flaticon2-close-cross" />
        </a>
      </React.Fragment>
    );
  }
}

export default DateTimePicker;
