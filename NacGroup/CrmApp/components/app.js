import React, { Component } from "react";
import { BrowserRouter, Redirect, Route } from "react-router-dom";

import Nav from "./nav";

import LanguageMenu from "./languagemenu";
import UserWindow from "./userwindow";
import Home from "./home";
import Empty from "./empty";



import ThemeConfigList from "../modules/Core/theme-config-list";
import ThemeConfigAddUpdate from "../modules/Core/themeconfig-addupdate";

import CustomerLabelAddUpdate from "../modules/CustomerLabel/customer-label-addupdate";
import CustomerLabelList from "../modules/CustomerLabel/customer-label-list";

import CustomerLevelAddUpdate from "../modules/CustomerLevel/customer-level-addupdate";
import CustomerLevelList from "../modules/CustomerLevel/customer-level-list";

import OrderLabelAddUpdate from "../modules/OrderLabel/order-label-addupdate";
import OrderLabelList from "../modules/OrderLabel/order-label-list";

import CustomerOrderAddUpdate from "../modules/CustomerOrder/customer-order-addupdate";
import CustomerOrderList from "../modules/CustomerOrder/customer-order-list";


import configEditorList from "../modules/Core/configeditor-list";
import configEditorAddUpdate from "../modules/Core/configeditor-addupdate";

import menuConfig from "../modules/Core/menu-config";

import webConfig from "../modules/Core/web-config";


import AdminManager from "../modules/Core/admin-manager";



import AppProvider from "./app-provider";

class App extends Component {
  constructor() {
    super();

    this.state = {
      ex: {
        Title: "Statistics overview"
      }
    };
  }
  componentDidMount() {
    document.title = "Dashboard";
  }
  render() {
    return (
      <AppProvider>
        <BrowserRouter>
          <React.Fragment>
            {this.state && this.state.ex ? (
              <React.Fragment>
                <div
                  id="kt_header_mobile"
                  className="kt-header-mobile  kt-header-mobile--fixed "
                >
                  <div className="kt-header-mobile__logo">
                    <a href="/">
                      <img
                        src="/crmadminstatics/global/img/logowebnail.png"
                        height="40"
                      />
                    </a>
                  </div>
                  <div className="kt-header-mobile__toolbar">
                    <button
                      className="kt-header-mobile__toggler kt-header-mobile__toggler--left"
                      id="kt_aside_mobile_toggler"
                    >
                      <span />
                    </button>
                    <button
                      className="kt-header-mobile__toggler"
                      id="kt_header_mobile_toggler"
                    >
                      <span />
                    </button>
                    <button
                      className="kt-header-mobile__topbar-toggler"
                      id="kt_header_mobile_topbar_toggler"
                    >
                      <i className="flaticon-more" />
                    </button>
                  </div>
                </div>

                <div className="kt-grid kt-grid--hor kt-grid--root">
                  <div className="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
                    <button className="kt-aside-close " id="kt_aside_close_btn">
                      <i className="la la-close" />
                    </button>
                    <Nav />
                    <div
                      className="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper"
                      id="kt_wrapper"
                    >
                      <div
                        id="kt_header"
                        className="kt-header kt-grid__item  kt-header--fixed "
                      >
                        <button
                          className="kt-header-menu-wrapper-close"
                          id="kt_header_menu_mobile_close_btn"
                        >
                          <i className="la la-close" />
                        </button>
                        <div
                          className="kt-header-menu-wrapper"
                          id="kt_header_menu_wrapper"
                        >
                          <div
                            id="kt_header_menu"
                            className="kt-header-menu kt-header-menu-mobile  kt-header-menu--layout-default "
                          >
                            <ul className="kt-menu__nav " />
                          </div>
                        </div>

                        <div className="kt-header__topbar">
                          <LanguageMenu />
                          <UserWindow />
                        </div>
                      </div>
                      <div className="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
                        <Route
                          exact
                          path="/crmadmin/emptypage"
                          component={Empty}
                        />
                         <Route exact path="/crmadmin" component={Home} />
                        <Route exact path="/crmadmin/dashboard" component={Home} />

                        <Route
                          exact
                          path="/crmadmin/themeconfig"
                          component={ThemeConfigList}
                        />
                        <Route
                          exact
                          path="/crmadmin/themeconfig/add"
                          component={ThemeConfigAddUpdate}
                        />
                        <Route
                          exact
                          path="/crmadmin/themeconfig/update/:id"
                          component={ThemeConfigAddUpdate}
                        />
                                            <Route
                                                exact
                                                path="/crmadmin/customerlabel/add"
                                                component={CustomerLabelAddUpdate}
                                            />
                        <Route
                          exact
                          path="/crmadmin/customerlabel/update/:id"
                          component={CustomerLabelAddUpdate}
                                            />
                                             <Route
                          exact
                          path="/crmadmin/customerlabel"
                                                component={CustomerLabelList}
                                            />
                                            <Route
                                                exact
                                                path="/crmadmin/customerlevel/add"
                                                component={CustomerLevelAddUpdate}
                                            />
                                            <Route
                                                exact
                                                path="/crmadmin/customerlevel/update/:id"
                                                component={CustomerLevelAddUpdate}
                                            />
                                            <Route
                                                exact
                                                path="/crmadmin/customerlevel"
                                                component={CustomerLevelList}
                                            />
                                            <Route
                                                exact
                                                path="/crmadmin/orderlabel/add"
                                                component={OrderLabelAddUpdate}
                                            />
                                            <Route
                                                exact
                                                path="/crmadmin/orderlabel/update/:id"
                                                component={OrderLabelAddUpdate}
                                            />
                                            <Route
                                                exact
                                                path="/crmadmin/orderlabel"
                                                component={OrderLabelList}
                                            />
                                            <Route
                                                exact
                                                path="/crmadmin/customerOrder/add"
                                                component={CustomerOrderAddUpdate}
                                            />
                                            <Route
                                                exact
                                                path="/crmadmin/customerOrder/update/:id"
                                                component={CustomerOrderAddUpdate}
                                            />
                                            <Route
                                                exact
                                                path="/crmadmin/customerOrder"
                                                component={CustomerOrderList}
                                            />

                      
                  
                
                        <Route
                          exact
                          path="/crmadmin/websiteconfig"
                          component={webConfig}
                        />
                      
                        <Route
                          exact
                          path="/crmadmin/configeditor"
                          component={configEditorList}
                        />
                        <Route
                          exact
                          path="/crmadmin/configeditor/add"
                          component={configEditorAddUpdate}
                        />
                        <Route
                          exact
                          path="/crmadmin/configeditor/update/:id"
                          component={configEditorAddUpdate}
                        />
                      
                     
                        <Route
                          exact
                          path="/crmadmin/menuconfig"
                          component={menuConfig}
                        />
 
                      </div>
                      <div
                        className="kt-footer kt-grid__item kt-grid kt-grid--desktop kt-grid--ver-desktop"
                        id="kt_footer"
                      >
                        <div className="kt-footer__copyright">
                          2019&nbsp;&copy;&nbsp;
                          <a href="#" target="_blank" className="kt-link">
                            NacGroup
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </React.Fragment>
            ) : (
              <div />
            )}
          </React.Fragment>
        </BrowserRouter>
      </AppProvider>
    );
  }
}

export default App;
