import React from "react";
import ReactDOM from "react-dom";
import App from "./components/app";
import "./plugins/string-helper";

ReactDOM.render(<App /> , document.getElementById("app"));
