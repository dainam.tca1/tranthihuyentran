import {
  GLOBAL_ERROR_MESSAGE,
  ERROR,
  SUCCESS,
  DATA_NOT_SELECT,
  DATA_NOT_RECOVER,
  NOTI
} from "../constants/message";

/**
 * Plugin dùng trong trang danh sách data, gọi ajax để xóa dữ liệu sau đó reload lại data
 */
class RemoveListPlugin {
  /**
   * Gọi ajax xóa data theo danh sách đã chọn
   * @param {String} url - Url để gọi ajax
   * @param {Object} cpn - Truyền this của component vào
   */
  static removeList(url, cpn) {
    var that = cpn;

    swal
      .fire({
        title: "Are you sure?",
        text: DATA_NOT_RECOVER,
        type: "warning",
        showCancelButton: true,
        confirmButtonText: "Yes, delete it",
        cancelButtonText: "No, cancel"
      })
      .then(function(result) {
        if (result.value) {
          var delist = that.state.model.Results.filter((e, i) => {
            return e.IsChecked;
          });
          if (delist.length == 0) {
            toastr["error"](DATA_NOT_SELECT, ERROR);
            return false;
          }

          KTApp.blockPage();
          $.ajax({
            url: url,
            type: "POST",
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify(
              delist.map(e => {
                return e.Id;
              })
            ),
            success: response => {
              KTApp.unblockPage();
              toastr.clear();
              if (response.status == "error") {
                toastr["error"](response.message, ERROR);
              } else {
                toastr["success"](response.message, SUCCESS);
                that.toPage(1);
              }
            },
            error: er => {
              KTApp.unblockPage();
              toastr.clear();
              toastr["error"](GLOBAL_ERROR_MESSAGE, ERROR);
            }
          });

          return true;

          //swal.fire(
          //    'Deleted!',
          //    'Your file has been deleted.',
          //    'success'
          //)
        } else if (result.dismiss === "cancel") {
          //swal.fire(
          //    'Cancelled',
          //    'Your imaginary file is safe :)',
          //    'error'
          //)
        }
      });
  }
}

export default RemoveListPlugin;
