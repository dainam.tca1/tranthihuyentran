const orderlabelTitle = ["Order Label", "Nhãn đơn hàng"];
const orderlabelList = ["Order Label List", "Danh sách nhãn đơn hàng"];
const information = ["Information", "Thông tin"];
const name = ["Name", "Tên"];
const choose_color = ["Choose Color", "Chọn màu sắc"];
const addorderlabel = ["Add Customer Label", "Thêm danh sách nhãn đơn hàng"];
const updateorderlabel = ["Update Customer Label", "Cập nhật danh sách nhãn đơn hàng"];
const order_label = ["Nhãn đơn hàng", "Nhãn đơn hàng"]
module.exports = {
        addorderlabel,
        updateorderlabel,
        information,
        name,
        choose_color,
        orderlabelList,
        orderlabelTitle,
        order_label
};
