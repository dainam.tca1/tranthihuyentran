import React, { Component } from "react";
import ReactJson from "react-json-view";
import { Link } from "react-router-dom";
import AppSettingCmsModel from "./models/appsettingmodel";
import {
    globalErrorMessage,
    error,
    success,
    back,
    add,
    remove,
    total,
    record,
    page,
    cDate,
    lDate,
    recordPerPage,
    are_you_sure,
    yes_de,
    no_ca,
    close,
    sort,
    save
} from "../../constants/message";
import {
    key_config,
    key,
    config_key,
    add_key,
    update_key,
    key_value
} from "./models/corestaticmessage";
import AppContext from "../../components/app-context";

//ckeditor
import CkEditor from "../../components/ckeditor";

var apiurl = "/cms/api/configeditor";

class ConfigEditorAddUpdate extends Component {
  constructor(props,context) {
      super(props, context);
    var action = null;
    if (document.location.href.indexOf("/admin/configeditor/add") >= 0) {
      action = "add";
    } else if (
      document.location.href.indexOf("/admin/configeditor/update") >= 0
    ) {
      action = "update";
    }
    this.state = {
      model: new AppSettingCmsModel(),
      ex: {
        Title: null,
        Action: action
      }
    };
    this.setState(this.state);
    if (action === "update") {
      var that = this;
        that.state.ex.Title = update_key[this.context.Language];
      var id = that.props.match.params.id;
      KTApp.blockPage();
      $.ajax({
        url: apiurl + "/" + that.state.ex.Action,
        type: "GET",
        dataType: "json",
        contentType: "application/json",
        data: { id: id },
        success: response => {
          KTApp.unblockPage();
          if (response.status == "success") {
            that.state.model = response.data;
            that.state.ex.Title = update_key[this.context.Language];
            document.title = that.state.ex.Title;
            that.setState(that.state);
          } else {
            toastr["error"](response.message, error[this.context.Language]);
          }
        },
        error: function(er) {
            KTApp.unblockPage();
            toastr["error"](globalErrorMessage[this.context.Language], error[this.context.Language]);
        }
      });
    } else {
        this.state.ex.Title = add_key[this.context.Language];
      this.setState(this.state);
    }
  }

  // this.setState(this.state);
  submitForm() {
    var that = this;
    //  var id = that.props.match.params.id;
    KTApp.blockPage();
    $.ajax({
      url: apiurl + "/" + that.state.ex.Action,
      type: "POST",
      dataType: "json",
      contentType: "application/json",
      data: JSON.stringify(that.state.model),
      success: response => {
        KTApp.unblockPage();
        if (response.status == "success") {
          swal.fire({
            title: success[this.context.Language],
            text: response.message,
            type: "success",
            onClose: () => {
              that.props.history.push("/admin/emptypage");
              that.props.history.replace({
                pathname: that.props.location.pathname,
                search: that.props.location.search
              });
            }
          });
        } else {
            toastr["error"](response.message, error[this.context.Language]);
        }
      },
      error: function(er) {
        KTApp.unblockPage();
          toastr["error"](globalErrorMessage[this.context.Language], error[this.context.Language]);
      }
    });
  }
  componentWillMount() {}

  componentDidMount() {
    document.title = this.state.ex.Title;
    $("#kt_aside_menu .kt-menu__item").removeClass("kt-menu__item--active");
    $("#kt_aside_menu .kt-menu__item[data-id='danh-sach-key']").addClass(
      "kt-menu__item--active"
    );
  }
  componentWillUnmount() {}
  render() {
    return (
      <React.Fragment>
        {this.state && this.state.model ? (
          <React.Fragment>
            <div className="kt-subheader kt-grid__item" id="kt_subheader">
              <div className="kt-subheader__main">
                <div className="kt-subheader__breadcrumbs">
                  <Link
                    to="/admin/dashboard"
                    className="kt-subheader__breadcrumbs-home"
                  >
                    <i className="fa fa-home" />
                  </Link>
                  <span className="kt-subheader__breadcrumbs-separator" />
                  <Link
                    to="/admin/configeditor"
                    className="kt-subheader__breadcrumbs-link"
                                >
                   {key_config[this.context.Language]}
                  </Link>
                  <span className="kt-subheader__breadcrumbs-separator" />
                  <span className="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
                    {this.state.ex.Title}
                  </span>
                </div>
              </div>
              <div className="kt-subheader__toolbar">
                <div className="kt-subheader__wrapper">
                  <Link to="/admin/configeditor" className="btn btn-secondary">
                    <i className="fa fa-chevron-left" /> {back[this.context.Language]}
                  </Link>
                  <a
                    href="javascript:;"
                    className="btn btn-primary"
                    onClick={() => {
                      this.submitForm();
                    }}
                  >
                    <i className="fa fa-save"></i> {save[this.context.Language]}
                  </a>
                </div>
              </div>
            </div>
            <div
              className="kt-content kt-grid__item kt-grid__item--fluid"
              id="kt_content"
            >
              <div className="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                <div className="row">
                  <div className="col-lg-6">
                    <div className="kt-portlet kt-portlet--mobile">
                      <div className="kt-portlet__head kt-portlet__head--lg">
                        <div className="kt-portlet__head-label">
                          <h3 className="kt-portlet__head-title">
                            {config_key[this.context.Language]}
                          </h3>
                        </div>
                      </div>

                      <div className="kt-portlet__body">
                        <div className="form-group">
                          <label className="control-label">{key[this.context.Language]}</label>
                          <input
                            className="form-control"
                            value={this.state.model.Key}
                            placeholder={key[this.context.Language]}
                            onChange={e => {
                              this.state.model.Key = e.target.value;
                              this.setState(this.state);
                            }}
                          />
                        </div>
                        <div className="form-group">
                          <label className="control-label">{key_value[this.context.Language]}</label>
                          <ReactJson
                            src={
                              this.state.model.Value == null
                                ? {}
                                : this.state.model.Value
                            }
                            theme={"monokai"}
                            collapseStringsAfterLength={"20"}
                            onEdit={e => {
                              this.state.model.Value = e.updated_src;
                              this.setState(this.state);
                            }}
                            onDelete={e => {
                              this.state.model.Value = e.updated_src;
                              this.setState(this.state);
                            }}
                            onAdd={e => {
                              this.state.model.Value = e.updated_src;
                              this.setState(this.state);
                            }}
                            displayObjectSize={true}
                            enableClipboard={true}
                            indentWidth={4}
                            displayDataTypes={true}
                            iconStyle={"triangle"}
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </React.Fragment>
        ) : (
          <div />
        )}
      </React.Fragment>
    );
  }
}
ConfigEditorAddUpdate.contextType = AppContext;
export default ConfigEditorAddUpdate;
