class ThemeConfigImageItem {
  constructor() {
    this.Name = null;
    this.Url = null;
    this.Src = null;
    this.Alt = null;
    this.Sort = 0;
    this.ThemeType = 1;
  }
}

class ThemeConfigTextItem {
  constructor() {
    this.Name = null;
    this.Value = null;
    this.Url = null;
    this.Sort = 0;
    this.TextType = 0;
    this.ThemeType = 2;
  }
}

class ThemeConfigModel {
  constructor() {
    this.Name = null;
    this.Slug = null;
    this.ThemeType = 0;
    this.Data = [];
  }
  static toThemeConfigWebModel(model) {
    var temp = new ThemeConfigModel();
    temp.Name = model.Name;
    temp.Slug = model.Slug;
    temp.ThemeType = model.ThemeType;
    temp.Data = model.Data;
    return temp;
  }
}

class ThemeConfigImageList {
  constructor() {
    this.Name = null;
    this.Slug = null;
    this.ThemeType = 3;
    this.Sort = 0;
    this.Data = [];
  }
}

class ThemeConfigTextImageList {
  constructor() {
    this.Name = null;
    this.Slug = null;
    this.ThemeType = 4;
    this.Sort = 0;
    this.Data = [];
    this.Template = {
      ImageList: [],
      TextList: []
    };
  }
}

module.exports = {
  ThemeConfigImageItem,
  ThemeConfigTextItem,
  ThemeConfigImageList,
  ThemeConfigTextImageList,
  ThemeConfigModel
};
