import React, { Component } from "react";
import { DelayInput } from "react-delay-input";
import PagedList from "../../components/pagedlist";
import {
    globalErrorMessage,
    error,
    success,
    back,
    add,
    remove,
    total,
    record,
    page,
    cDate,
    lDate,
    recordPerPage,
    are_you_sure,
    yes_de,
    no_ca,
    close,
    sort,
    save
} from "../../constants/message";
import {
    key_config,
    key,
    conf_key,
    add_key,
    update_key,
} from "./models/corestaticmessage";
import AppContext from "../../components/app-context";
import { Link } from "react-router-dom";
import FilterListPlugin from "../../plugins/filter-list-plugin";
import RemoveListPlugin from "../../plugins/remove-list-plugin";
var apiurl = "/cms/api/ConfigEditor";
class ConfigEditorList extends Component {
  constructor(props,context) {
      super(props, context);
    var that = this;
    that.state = {
      ex: {
            Title: key_config[this.context.Language],
        Param: {
          key: null,
          pagesize: null
        }
      }
    };
    that.setState(that.state);
    that.handleChangeFilter = that.handleChangeFilter.bind(that);
    that.toPage(1);
  }

  toPage(index) {
    FilterListPlugin.filterdata(index, this, apiurl);
  }
  handleChangeFilter(event) {
    FilterListPlugin.handleChangeFilter(event, this);
    this.toPage(1);
  }
  handleDeleteDataRow() {
    RemoveListPlugin.removeList(`${apiurl}/removelist`, this);
  }
  componentDidMount() {
    document.title = "Key Configuration";
    $("#kt_aside_menu .kt-menu__item").removeClass("kt-menu__item--active");
    $("#kt_aside_menu .kt-menu__item[data-id='danh-sach-key']").addClass(
      "kt-menu__item--active"
    );
  }
  render() {
    return (
      <React.Fragment>
        {this.state && this.state.model ? (
          <React.Fragment>
            <div className="kt-subheader kt-grid__item" id="kt_subheader">
              <div className="kt-subheader__main">
                <div className="kt-subheader__breadcrumbs">
                  <Link
                    to="/admin/dashboard"
                    className="kt-subheader__breadcrumbs-home"
                  >
                    <i className="fa fa-home" />
                  </Link>
                  <span className="kt-subheader__breadcrumbs-separator" />
                  <span className="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
                    {this.state.ex.Title}
                  </span>
                </div>
              </div>
              <div className="kt-subheader__toolbar">
                <div className="kt-subheader__wrapper">
                  <Link
                    to="/admin/configeditor/add"
                    className="btn btn-primary"
                  >
                    <i className="fa fa-plus"></i> {add[this.context.Language]}
                  </Link>
                  <a
                    href="javascript:;"
                    onClick={e => this.handleDeleteDataRow()}
                    className="btn btn-danger"
                  >
                    <i className="fa fa-trash-alt"></i> {remove[this.context.Language]}
                  </a>
                </div>
              </div>
            </div>
            <div
              className="kt-content kt-grid__item kt-grid__item--fluid"
              id="kt_content"
            >
              <div className="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                <div className="kt-portlet kt-portlet--mobile">
                  <div className="kt-portlet__head kt-portlet__head--lg">
                    <div className="kt-portlet__head-label">
                      <h3 className="kt-portlet__head-title">
                        {this.state.ex.Title}
                      </h3>
                    </div>
                  </div>

                  <div className="kt-portlet__body">
                    <form className="kt-form kt-form--fit">
                      <div className="row kt-margin-b-20">
                        <div className="col-lg-3">
                          <label>{key[this.context.Language]}:</label>
                          <DelayInput
                            delayTimeout={1000}
                            className="form-control kt-input"
                            placeholder={key[this.context.Language]}
                            value={this.state.ex.Param.key}
                            name="key"
                            onChange={this.handleChangeFilter}
                          />
                        </div>
                        <div className="col-lg-3">
                          <label>{recordPerPage[this.context.Language]}:</label>
                          <select
                            className="form-control kt-input"
                            data-col-index="2"
                            value={this.state.ex.Param.pagesize}
                            name="pagesize"
                            onChange={this.handleChangeFilter}
                          >
                      <option value="10">10 {record[this.context.Language]} / {page[this.context.Language]}</option>
<option value="20">20 {record[this.context.Language]} / {page[this.context.Language]}</option>
<option value="50">50 {record[this.context.Language]} / {page[this.context.Language]}</option>
<option value="100">100 {record[this.context.Language]} / {page[this.context.Language]}</option>
                          </select>
                        </div>
                      </div>
                    </form>

                    <div className="kt-separator kt-separator--border-dashed"></div>

                    <div
                      id="kt_table_1_wrapper"
                      className="dataTables_wrapper dt-bootstrap4"
                    >
                      <div className="row">
                        <div className="col-sm-12">
                          <table className="table table-striped- table-bordered table-hover table-checkable dataTable no-footer dtr-inline table-responsive">
                            <thead>
                              <tr role="row">
                                <th
                                  className="dt-right sorting_disabled"
                                  rowspan="1"
                                  colspan="1"
                                  style={{ width: "1%" }}
                                  aria-label="Record ID"
                                >
                                  <label className="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                    <input
                                      type="checkbox"
                                      className="kt-group-checkable"
                                      onChange={ev => {
                                        this.state.model.Results = this.state.model.Results.map(
                                          e => {
                                            return {
                                              ...e,
                                              IsChecked: ev.target.checked
                                            };
                                          }
                                        );
                                        this.setState(this.state);
                                      }}
                                    />
                                    <span></span>
                                  </label>
                                </th>
                                <th>{key[this.context.Language]}</th>
                              </tr>
                            </thead>
                            <tbody>
                              {this.state.model.Results.map((c, index) => {
                                return (
                                  <tr>
                                    <td className="dt-right" tabindex="0">
                                      <label className="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                        <input
                                          type="checkbox"
                                          className="kt-checkable"
                                          value={c._id}
                                          checked={c.IsChecked}
                                          onChange={e => {
                                            c.IsChecked = !c.IsChecked;
                                            this.setState(this.state);
                                          }}
                                        />
                                        <span></span>
                                      </label>
                                    </td>
                                    <td>
                                      <Link
                                        to={`/admin/configeditor/update/${c.Id}`}
                                      >
                                        {c.Key}
                                      </Link>
                                    </td>
                                  </tr>
                                );
                              })}
                            </tbody>
                          </table>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-sm-12 col-md-5">
                          <div
                            className="dataTables_info"
                            id="kt_table_1_info"
                            role="status"
                            aria-live="polite"
                          >
                            {total[this.context.Language]} {this.state.model.TotalItemCount} {record[this.context.Language]}
                          </div>
                        </div>
                        <div className="col-sm-12 col-md-7 dataTables_pager">
                          <PagedList
                            currentpage={this.state.model.CurrentPage}
                            pagesize={this.state.model.PageSize}
                            totalitemcount={this.state.model.TotalItemCount}
                            totalpagecount={this.state.model.TotalPageCount}
                            ajaxcallback={this.toPage.bind(this)}
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </React.Fragment>
        ) : (
          <div />
        )}
      </React.Fragment>
    );
  }
}
ConfigEditorList.contextType = AppContext;
export default ConfigEditorList;
