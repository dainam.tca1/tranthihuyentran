import React, { Component } from "react";
import { NOTI, DATA_NOT_RECOVER } from "../../constants/message";
import uploadPlugin from "../../plugins/upload-plugin";

class ThemeImageItem extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <div className="form-group">
        <div className="form-group">
          {!this.props.ishidelabel && (
            <a className="btn btn-default kt-font-bolder">
              {this.props.model.Name}
            </a>
          )}{" "}
          <a href="javascript:" className="btn btn-primary btn-customfile">
            <input
              type="file"
              onChange={e => {
                var that = this;
                uploadPlugin.UpdateImage(e, data => {
                  that.props.model.Src = data[0];
                  that.props.onChange(that.props.model);
                });
              }}
            />
            <i className="la la-photo"></i>
            Choose File
          </a>{" "}
          {!this.props.ishideremove && (
            <a
              className="btn btn-danger"
              href="javascript:"
              onClick={e => {
                var that = this;
                swal
                  .fire({
                    title: "Are you sure?",
                    text: DATA_NOT_RECOVER,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Yes, delete it",
                    cancelButtonText: "No, cancel"
                  })
                  .then(function(result) {
                    if (result.value) {
                      that.props.onRemove(that.props.index);
                      return true;
                    }
                  });
              }}
            >
              <i className="fa fa-trash" /> Remove Item
            </a>
          )}
        </div>
        <div className="form-group">
          <img src={this.props.model.Src} style={{ height: "100px" }} />
        </div>
        <div className="form-group">
          <input
            placeholder="Alt"
            className="form-control"
            value={this.props.model.Alt}
            onChange={e => {
              this.props.model.Alt = e.target.value;
              this.props.onChange(this.props.model);
            }}
          />
        </div>
        <div className="form-group">
          <input
            placeholder="Url (Optional)"
            className="form-control"
            value={this.props.model.Url}
            onChange={e => {
              this.props.model.Url = e.target.value;
              this.props.onChange(this.props.model);
            }}
          />
        </div>
      </div>
    );
  }
}

export default ThemeImageItem;
