import React, { Component } from "react";
import {
    globalErrorMessage,
    error,
    success,
    add,
    remove,
    total,
    record,
    page,
    cDate,
    lDate,
    recordPerPage,
    are_you_sure,
    yes_de,
    no_ca,
    data_recover
} from "../../constants/message";
import {
    menu_config,
    add_item,
    edit,
    add_menu,
    add_menu_item,
    text,
    menu_text,
} from "./models/corestaticmessage";
import AppContext from "../../components/app-context";
const apiurl = "/cms/api/menuconfig";

class MenuConfigList extends Component {
  constructor(props,context) {
      super(props, context);
    this.menuBox = React.createRef();
  }

  componentDidMount() {
    $(this.menuBox.current).sortable({
      placeholder: "drag-highlight",
      update: (e, ui) => {
        var updatelist = [];
        $(this.menuBox.current)
          .find("li[data-parent='" + $(this.menuBox.current).attr("id") + "']")
          .each((index, element) => {
            updatelist.push({ Id: $(element).data("id"), Sort: index });
          });
        this.updateSort(updatelist);
      }
    });
  }

  updateSort(list) {
    var that = this;
    KTApp.blockPage();
    $.ajax({
      url: apiurl + "/updatesort",
      type: "POST",
      dataType: "json",
      contentType: "application/json",
      data: JSON.stringify(list),
      success: response => {
        KTApp.unblockPage();
        if (response.status == "success") {
            toastr["success"](response.message, success[this.context.Language]);
          that.props.onChange();
        } else {
            toastr["error"](response.message, error[this.context.Language]);
        }
      },
      error: function(er) {
        KTApp.unblockPage();
          toastr["error"](globalErrorMessage[this.context.Language], error[this.context.Language]);
      }
    });
  }

  remove(id) {
    var that = this;
    swal
      .fire({
          title: are_you_sure[that.context.Language],
          text: data_recover[that.context.Language],
        type: "warning",
        showCancelButton: true,
          confirmButtonText: yes_de[that.context.Language],
          cancelButtonText: no_ca[that.context.Language]
      })
      .then(function(result) {
        if (result.value) {
          KTApp.blockPage();
          $.ajax({
            url: apiurl + "/remove",
            type: "POST",
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify(id),
            success: response => {
              KTApp.unblockPage();
              toastr.clear();
              if (response.status == "error") {
                  toastr["error"](response.message, error[that.context.Language]);
              } else {
                toastr["success"](response.message, success[that.context.Language]);
                that.props.onChange();
              }
            },
            error: er => {
              KTApp.unblockPage();
              toastr.clear();
                toastr["error"](globalErrorMessage[that.context.Language], error[that.context.Language]);
            }
          });

          return true;
        }
      });
  }

  render() {
    return (
      <ul
        ref={this.menuBox}
        id={this.props.value.length > 0 ? this.props.value[0].ParentId : null}
      >
        {this.props.value.map(item => {
          return (
            <li data-parent={item.ParentId} data-id={item.Id}>
              <div>
                {item.Children.length > 0 && (
                  <i
                    className="fa fa-caret-down"
                    onClick={e => {
                      $(e.target)
                        .parent()
                        .next()
                        .slideToggle();
                    }}
                  />
                )}
                <span>{item.Text}</span>
                <div className="btn-group">
                  <button
                    type="button"
                    className="btn btn-primary"
                    onClick={() => {
                      this.props.onclickedUpdate(item);
                    }}
                  >
                              <i className="fa fa-cog"></i> {edit[this.context.Language]}
                  </button>{" "}
                  <button
                    type="button"
                    className="btn btn-danger"
                    onClick={e => {
                      this.remove(item.Id);
                    }}
                  >
                    <i className="fa fa-trash" /> {remove[this.context.Language]}
                  </button>{" "}
                  <button
                    type="button"
                    className="btn btn-primary"
                    onClick={() => {
                      var addModel = {
                        ParentId: item.Id,
                        MenuLevel: item.MenuLevel + 1,
                        Sort: item.Children.length
                      };
                      this.props.onclickedAdd(addModel);
                    }}
                  >
                    <i className="fa fa-plus" /> {add[this.context.Language]}
                  </button>
                </div>
              </div>
              {item.Children.length > 0 && (
                <MenuConfigList
                  value={item.Children}
                  onChange={() => {
                    this.props.onChange();
                  }}
                  onclickedAdd={addModel => {
                    this.props.onclickedAdd(addModel);
                  }}
                  onclickedUpdate={item => {
                    this.props.onclickedUpdate(item);
                  }}
                />
              )}
            </li>
          );
        })}
      </ul>
    );
  }
}
MenuConfigList.contextType = AppContext;
export default MenuConfigList;
