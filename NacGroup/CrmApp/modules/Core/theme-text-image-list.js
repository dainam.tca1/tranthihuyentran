import React, { Component } from "react";
import ThemeImageItem from "./theme-image-item";
import ThemeTextItem from "./theme-text-item";
import { ThemeConfigImageItem, ThemeConfigTextItem } from "./models/thememodel";
import {
  DATA_NOT_SELECT,
  NOTI,
  DATA_NOT_RECOVER,
  ERROR,
  SUCCESS
} from "../../constants/message";

class ThemeTextImageList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      model: {
        ...this.props.model,
        Data: this.props.model.Data.map(e => {
          return { ...e, IsChecked: false };
        })
      }
    };
  }

  removeitem() {
    var that = this;
    swal
      .fire({
        title: "Are you sure?",
        text: DATA_NOT_RECOVER,
        type: "warning",
        showCancelButton: true,
        confirmButtonText: "Yes, delete it",
        cancelButtonText: "No, cancel"
      })
      .then(function(result) {
        if (result.value) {
          that.state.model.Data = that.state.model.Data.filter(
            (item, index) => {
              return !item.IsChecked;
            }
          );
          that.props.onChange(that.state.model);
          return true;
        }
      });
  }

  reorder() {
    var that = this;
    that.state.model.Data.sort(function(a, b) {
      return a.Sort > b.Sort ? 1 : b.Sort > a.Sort ? -1 : 0;
    });
    that.props.onChange(that.state.model);
  }

  render() {
    return (
      <div className="form-group">
        <div className="form-group">
          <a className="btn btn-default bold">{this.props.model.Name}</a>{" "}
          <a
            href="javascript:"
            className="btn btn-primary"
            onClick={e => {
              var that = this;
              var newmodel = {
                Sort:
                  that.state.model.Data.length > 0
                    ? that.state.model.Data[that.state.model.Data.length - 1]
                        .Sort + 1
                    : 0,
                TextList: [],
                ImageList: []
              };

              that.state.model.Template.ImageList.forEach(img => {
                var newimg = new ThemeConfigImageItem();
                newimg.Name = img;
                newmodel.ImageList.push(newimg);
              });

              that.state.model.Template.TextList.forEach(txt => {
                var newtxt = new ThemeConfigTextItem();
                newtxt.Name = txt.Name;
                newtxt.TextType = txt.TextType;
                newmodel.TextList.push(newtxt);
              });
              that.state.model.Data.push(newmodel);
              that.props.onChange(that.state.model);
            }}
          >
            <i className="fa fa-plus"></i>
            Add
          </a>{" "}
          <a
            className="btn btn-danger"
            href="javascript:"
            onClick={e => {
              this.removeitem();
            }}
          >
            <i className="fa fa-trash"></i>
            Delete
          </a>{" "}
          <a
            className="btn btn-danger"
            href="javascript:"
            onClick={e => {
              var that = this;
              swal
                .fire({
                  title: "Are you sure?",
                  text: DATA_NOT_RECOVER,
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonText: "Yes, delete it",
                  cancelButtonText: "No, cancel"
                })
                .then(function(result) {
                  if (result.value) {
                    that.props.onRemove(that.props.index);
                    return true;
                  }
                });
            }}
          >
            <i className="fa fa-trash"></i>
            Delete Group
          </a>
        </div>
        <div className="dataTables_wrapper dt-bootstrap4">
          <table className="table table-striped- table-bordered table-hover table-checkable dataTable no-footer dtr-inline table-responsive">
            <thead>
              <tr>
                <th style={{ width: "35px" }}>
                  <label className="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                    <input
                      type="checkbox"
                      onChange={ev => {
                        var that = this;
                        that.state.model.Data = that.state.model.Data.map(e => {
                          return {
                            ...e,
                            IsChecked: ev.target.checked
                          };
                        });
                        that.setState(that.state);
                      }}
                    />
                    <span />
                  </label>
                </th>
                <th style={{ width: "100px" }}>STT</th>
                {this.state.model.Template.ImageList.map(e => {
                  return <th>{e}</th>;
                })}
                {this.state.model.Template.TextList.map(e => {
                  return <th>{e.Name}</th>;
                })}
              </tr>
            </thead>
            <tbody className="textlist">
              {this.state.model.Data.map((item, index) => {
                return (
                  <tr>
                    <td>
                      <label className="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                        <input
                          type="checkbox"
                          value={index}
                          checked={item.IsChecked}
                          onChange={e => {
                            item.IsChecked = !item.IsChecked;
                            this.setState(this.state);
                          }}
                        />
                        <span />
                      </label>
                    </td>
                    <td>
                      <input
                        placeholder="Order"
                        type="number"
                        className="form-control"
                        value={item.Sort}
                        onChange={e => {
                          item.Sort = parseInt(e.target.value);
                          this.reorder();
                        }}
                      />
                    </td>
                    {this.state.model.Template.ImageList.map((e, i) => {
                      return (
                        <td>
                          <ThemeImageItem
                            model={item.ImageList[i]}
                            ishideremove={true}
                            ishidelabel={true}
                            onChange={newmodel => {
                              item.ImageList[i] = newmodel;
                              this.props.onChange(this.state.model);
                            }}
                          />
                        </td>
                      );
                    })}
                    {this.state.model.Template.TextList.map((e, i) => {
                      return (
                        <td>
                          <ThemeTextItem
                            model={item.TextList[i]}
                            ishideremove={true}
                            ishidelabel={true}
                            onChange={newmodel => {
                              item.TextList[i] = newmodel;
                              this.props.onChange(this.state.model);
                            }}
                          />
                        </td>
                      );
                    })}
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

export default ThemeTextImageList;
