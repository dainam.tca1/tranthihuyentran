import React, { Component } from "react";
import CkEditor from "../../components/ckeditor";
import { NOTI, DATA_NOT_RECOVER } from "../../constants/message";

class ThemeTextItem extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <div className="form-group">
        {!this.props.ishidelabel && (
          <label className="control-label kt-font-bolder">
            {this.props.model.Name}
            {!this.props.ishideremove && (
              <a
                className="text-danger"
                href="javascript:"
                onClick={e => {
                  var that = this;

                  swal
                    .fire({
                      title: "Are you sure?",
                      text: DATA_NOT_RECOVER,
                      type: "warning",
                      showCancelButton: true,
                      confirmButtonText: "Yes, delete it",
                      cancelButtonText: "No, cancel"
                    })
                    .then(function(result) {
                      if (result.value) {
                        that.props.onRemove(that.props.index);
                        return true;
                      }
                    });
                }}
              >
                <i className="fa fa-trash" />
              </a>
            )}
          </label>
        )}

        {this.props.model.TextType === 1 ? (
          <CkEditor
            id={String.randomString()}
            value={this.props.model.Value}
            onChange={e => {
              this.props.model.Value = e;
              this.props.onChange(this.props.model);
            }}
          />
        ) : (
          <textarea
            placeholder={this.props.model.Name}
            value={this.props.model.Value}
            className="form-control"
            onChange={e => {
              this.props.model.Value = e.target.value;
              this.props.onChange(this.props.model);
            }}
          />
        )}
        {this.props.model.TextType === 2 && (
          <input
            placeholder="Url (Optional)"
            className="form-control kt-margin-t-10"
            value={this.props.model.Url}
            onChange={e => {
              this.props.model.Url = e.target.value;
              this.props.onChange(this.props.model);
            }}
          />
        )}
      </div>
    );
  }
}

export default ThemeTextItem;
