const customerlevelTitle = ["Customer Level", "Cấp bậc khách hàng"];
const customerlevelList = ["Customer Level List", "Danh sách cấp bậc khách hàng"];
const information = ["Information", "Thông tin"];
const name = ["Name", "Tên"];
const amount = ["Amount Required", "Số tiền yêu cầu"];
const addcustomerlevel = ["Add Customer Level", "Thêm danh sách cấp bậc khách hàng"];
const updatecustomerlevel = ["Update Customer Level", "Cập nhật danh sách cấp bậc khách hàng"];
const customer_level = ["Customer Level", "Cấp bậc khách hàng"];
module.exports = {
    addcustomerlevel,
    updatecustomerlevel,
    information,
    name,
    amount,
    customerlevelTitle,
    customerlevelList,
    customer_level
};
