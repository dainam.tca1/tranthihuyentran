const customer_label = ["Customer Label", "Nhãn khách hàng"];
const choose_color = ["Choose Color", "Chọn màu sắc"];
const name = ["Name", "Tên"];
const customerlabelTitle = ["Customer Label Title", "Nhãn khách hàng"];
const customerlabelList = ["Customer Label List", "Danh sách nhãn khách hàng"];
const addcustomerlabel = ["Add Customer Label", "Thêm nhãn mới"];
const updatecustomerlabel = ["Update Customer Label", "Cập nhật nhãn"];
const information = ["Information", "Thông tin"];


module.exports = {
    customer_label,
    name,
    choose_color,
    customerlabelTitle,
    customerlabelList,
    addcustomerlabel,
    updatecustomerlabel,
    information,
};
