import React, { Component } from "react";
import { Link } from "react-router-dom";
import CustomerLabelCmsModel from "./models/customer-label-cmsmodel";

import {
    globalErrorMessage,
    error,
    success,
    add,
    save,
    remove,
    total,
    record,
    page,
    cDate,
    lDate,
    back,
    recordPerPage
} from "../../constants/message";
import {
    choose_color,
    customerlabelTitle,
    customerlabelList,
    name,
    addcustomerlabel,
    updatecustomerlabel,
    information,
} from "./models/customerlabelstaticmessage";
import AppContext from "../../components/app-context";
//ckeditor


var apiurl = "/crm/api/customerlabel";

class CustomerLabelAddUpdate extends Component {
    constructor(props, context) {
        super(props, context);
        var that = this;

        //action để nhận biết hiện đang add hay update
        var action = null;
        if (document.location.href.indexOf("/crmadmin/customerlabel/add") >= 0) {
            action = "add";
        } else if (
            document.location.href.indexOf("/crmadmin/customerlabel/update") >= 0
        ) {
            action = "update";
        }
        this.myColor = React.createRef();
        that.state = {
            model: new CustomerLabelCmsModel(),
            ex: {
                Title: null,
                Action: action,
                ColorDropdownList:["green","yellow","red","purple","blue","sky","lime","pink","black"]
            }
        };
        that.setState(that.state);

        if (action === "update") {
            var id = this.props.match.params.id;
            KTApp.blockPage();
            $.ajax({
                url: apiurl + "/getupdate",
                type: "GET",
                dataType: "json",
                contentType: "application/json",
                data: { id },
                success: response => {
                    KTApp.unblockPage();
                    if (response.status == "success") {
                        that.state.model = response.data;
                        that.state.ex.Title = updatecustomerlabel[this.context.Language];
                        document.title = that.state.ex.Title;
                        that.setState(that.state);
                    } else {
                        toastr["error"](response.message, error[this.context.Language]);
                    }
                },
                error: function (er) {
                    KTApp.unblockPage();
                    toastr["error"](globalErrorMessage[this.context.Language], error[this.context.Language]);
                }
            });
        } else {
            that.state.ex.Title = addcustomerlabel[this.context.Language];
            document.title = that.state.ex.Title;
            that.setState(that.state);
        }
    }

   

    submitForm() {
        var that = this;
        KTApp.blockPage();
        $.ajax({
            url: apiurl + "/" + that.state.ex.Action,
            type: "POST",
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify(that.state.model),
            success: response => {
                KTApp.unblockPage();
                if (response.status == "success") {
                    swal.fire({
                        title: success[this.context.Language],
                        text: response.message,
                        type: "success",
                        onClose: () => {
                            that.props.history.push("/crmadmin/emptypage");
                            that.props.history.replace({
                                pathname: that.props.location.pathname,
                                search: that.props.location.search
                            });
                        }
                    });
                } else {
                    toastr["error"](response.message, error[this.context.Language]);
                }
            },
            error: function (er) {
                KTApp.unblockPage();
                toastr["error"](globalErrorMessage[this.context.Language], error[this.context.Language]);
            }
        });
    }

    componentWillMount() {
        //Load script ckeditor lên
      
    }

    componentDidMount() {
        //Active menu
        document.title = this.state.ex.Title;
        $("#kt_aside_menu .kt-menu__item").removeClass("kt-menu__item--active");
        $("#kt_aside_menu .kt-menu__item[data-id='customer-label']").addClass(
            "kt-menu__item--active"
        );
    }

    componentWillUnmount() {
        $("#scriptloading").html("");
    }
    render() {
        return (
            <React.Fragment>
                {this.state && this.state.model ? (
                    <React.Fragment>
                        <div className="kt-subheader kt-grid__item" id="kt_subheader">
                            <div className="kt-subheader__main">
                                <div className="kt-subheader__breadcrumbs">
                                    <Link
                                        to="/crmadmin/dashboard"
                                        className="kt-subheader__breadcrumbs-home"
                                    >
                                        <i className="fa fa-home" />
                                    </Link>
                                    <span className="kt-subheader__breadcrumbs-separator" />
                                    <span className="kt-subheader__breadcrumbs-link">
                                        {customerlabelTitle[this.context.Language]}
                                    </span>
                                    <span className="kt-subheader__breadcrumbs-separator" />
                                    <Link
                                        to="/crmadmin/customerlabel"
                                        className="kt-subheader__breadcrumbs-link"
                                    >
                                        {customerlabelList[this.context.Language]}
                                    </Link>
                                    <span className="kt-subheader__breadcrumbs-separator" />
                                    <span className="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
                                        {this.state.ex.Title}
                                    </span>
                                </div>
                            </div>
                            <div className="kt-subheader__toolbar">
                                <div className="kt-subheader__wrapper">
                                    <Link to="/crmadmin/customerlabel" className="btn btn-secondary">
                                        <i className="fa fa-chevron-left" />  {back[this.context.Language]}
                                    </Link>
                                    <a
                                        href="javascript:;"
                                        className="btn btn-primary"
                                        onClick={() => {
                                            this.submitForm();
                                        }}
                                    >
                                        <i className="fa fa-save"></i> {save[this.context.Language]}
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div
                            className="kt-content kt-grid__item kt-grid__item--fluid"
                            id="kt_content"
                        >
                            <div className="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                                <div className="row">
                                    <div className="col-lg-12">
                                        <div className="kt-portlet kt-portlet--mobile">
                                            <div className="kt-portlet__head kt-portlet__head--lg">
                                                <div className="kt-portlet__head-label">
                                                    <h3 className="kt-portlet__head-title">
                                                        {information[this.context.Language]}
                                                    </h3>
                                                </div>
                                            </div>

                                            <div className="kt-portlet__body">
                                                <div className="form-group">
                                                    <label className="control-label">{name[this.context.Language]}</label>
                                                    <input
                                                        className="form-control"
                                                        value={this.state.model.Name}
                                                        placeholder={name[this.context.Language]}
                                                        onChange={e => {
                                                            this.state.model.Name = e.target.value;                       
                                                            if (this.state.ex.Action == "add") {
                                                                this.state.model.Url = e.target.value.cleanUnicode();
                                                            }
                                                            this.setState(this.state);
                                                        }}
                                                    />
                                                </div>

                                                <div className="form-group">
                                                    <label className="control-label" >{choose_color[this.context.Language]}</label>
                                                    <div className="card-list" ref={this.myColor}>
                                                        {this.state.ex.ColorDropdownList ? this.state.ex.ColorDropdownList.map(c => {
                                                            return (
                                                                <div className={`card-label card-label-${c}`}>
                                                                    <span
                                                                        className={this.state.model.Color == c ? `card-label-color-select-icon fa fa-check active` : `card-label-color-select-icon fa fa-check`}
                                                                        data-color={c}
                                                                        onClick={e => {
                                                                            this.state.model.Color = c;
                                                                            this.setState(this.state);
                                                                        }}
                                                                    ></span>
                                                                </div>
                                                                )
                                                        }) : ""}
                                                    
                                                    </div>
                                                </div>
                                               
                                              
                                            </div>
                                        </div>
                                    </div>
                                
                                </div>
                            </div>
                        </div>
                    </React.Fragment>
                ) : (
                        <div />
                    )}
            </React.Fragment>
        );
    }
}
CustomerLabelAddUpdate.contextType = AppContext;
export default CustomerLabelAddUpdate;
