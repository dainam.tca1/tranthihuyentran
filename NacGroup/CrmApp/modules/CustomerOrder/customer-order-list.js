import React, { Component } from "react";
import { DelayInput } from "react-delay-input";
import PagedList from "../../components/pagedlist";
import { Link } from "react-router-dom";
import FilterListPlugin from "../../plugins/filter-list-plugin";
import RemoveListPlugin from "../../plugins/remove-list-plugin";
import DateTimePicker from "../../components/datetimepicker";
import moment from "moment";
import {
    globalErrorMessage,
    error,
    success,
    add,
    remove,
    total,
    record,
    page,
    cDate,
    lDate,
    recordPerPage
} from "../../constants/message";
import {
    order_Id,
    amount_from,
    amount_to,
    time_from,
    time_to,
    information,
    customer_order
} from "./models/customerorderstaticmessage";
import AppContext from "../../components/app-context";
var apiurl = "/crm/api/customerorder";

class CustomerOrderList extends Component {
    constructor(props, context) {
        super(props, context);

        //Khởi tạo state
        var that = this;
        that.state = {
            ex: {
                Title: customer_order[this.context.Language],
                Param: {
                    //Danh sách tham số để filter
                    pagesize: null,
                    orderId: null,
                    AmountFrom: null,
                    AmountTo: null,
                    Label: null,
                    TimeFrom: null,
                    TimeTo: null
                },
                LabelDropdownList:[]
            },

        }
        this.ViewOrderBox = React.createRef();
        that.setState(that.state);

        //Cấu hình event trên component
        that.handleChangeFilter = that.handleChangeFilter.bind(that);

        //Gọi hàm filter để đổ dữ liệu ra
        that.toPage(1);
        $.get("/crm/api/orderlabel", (response) => {
            that.state.ex.LabelDropdownList = response.data.Results;   
            that.setState(that.state);
        });
    }
    
    componentWillMount() { }
    componentDidMount() {
        document.title = this.state.ex.Title;
        $("#kt_aside_menu .kt-menu__item").removeClass("kt-menu__item--active");
        $("#kt_aside_menu .kt-menu__item[data-id='customer-order']").addClass(
            "kt-menu__item--active"
        );
    }

    toPage(index) {
        FilterListPlugin.filterdata(index, this, apiurl);
    }

    /**
     * Được gọi khi thay đổi các trường filter cập nhật tham số filter vào state
     * Filter dữ liệu theo tham số mới
     * @param {any} event -- Event
     */
    handleChangeFilter(event) {
        FilterListPlugin.handleChangeFilter(event, this);
        this.toPage(1);
    }

    /**
     * Xóa data đã được chọn
     */
    handleDeleteDataRow() {
        RemoveListPlugin.removeList(`${apiurl}/removelist`, this);
    }
    componentWillMount() {
        $("#cssloading").html(
            ` <link href="/adminstatics/global/plugins/flatpickr/css/flatpickr.min.css" rel="stylesheet" type="text/css" />`
        );
        $("#scriptloading").html(
            `<script src="/adminstatics/global/plugins/flatpickr/js/flatpickr.min.js" type="text/javascript"></script>`
        );
    }

    render() {
        return (
            <React.Fragment>
                {this.state && this.state.model ? (
                    <React.Fragment>
                        <div className="kt-subheader kt-grid__item" id="kt_subheader">
                            <div className="kt-subheader__main">
                                <div className="kt-subheader__breadcrumbs">
                                    <Link
                                        to="/crmadmin/customerOrder"
                                        className="kt-subheader__breadcrumbs-home"
                                    >
                                        <i className="fa fa-home" />
                                    </Link>
                                    <span className="kt-subheader__breadcrumbs-separator" />
                                    <span className="kt-subheader__breadcrumbs-link">
                                        {customer_order[this.context.Language]}
                                    </span>
                                    <span className="kt-subheader__breadcrumbs-separator" />
                                    <span className="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
                                        {this.state.ex.Title}
                                    </span>
                                </div>
                            </div>
                            <div className="kt-subheader__toolbar">
                                <div className="kt-subheader__wrapper">
                                    <Link to="/crmadmin/customerOrder/add" className="btn btn-primary">
                                        <i className="fa fa-plus"></i>{add[this.context.Language]}
                                    </Link>
                                    <a
                                        href="javascript:;"
                                        onClick={e => this.handleDeleteDataRow()}
                                        className="btn btn-danger"
                                    >
                                        <i className="fa fa-trash-alt"></i> {remove[this.context.Language]}
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div
                            className="kt-content kt-grid__item kt-grid__item--fluid"
                            id="kt_content"
                        >
                            <div className="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                                <div className="kt-portlet kt-portlet--mobile">
                                    <div className="kt-portlet__head kt-portlet__head--lg">
                                        <div className="kt-portlet__head-label">
                                            <h3 className="kt-portlet__head-title">
                                                {this.state.ex.Title}
                                            </h3>
                                        </div>
                                    </div>
                                    <div className="kt-portlet__body">
                                        <form className="kt-form kt-form--fit">
                                            <div className="row kt-margin-b-20">
                                               
                                             
                                                <div className="col-lg-3">
                                                    <label>{amount_from[this.context.Language]}:</label>
                                                    <DelayInput
                                                        delayTimeout={1000}
                                                        className="form-control"
                                                        placeholder={amount_from[this.context.Language]}
                                                        value={this.state.ex.Param.AmountFrom}
                                                        name="amountFrom"
                                                        onChange={this.handleChangeFilter}
                                                    />
                                                </div>
                                               
                                                <div className="col-lg-3">
                                                    <label>{amount_to[this.context.Language]}:</label>
                                                    <DelayInput
                                                        delayTimeout={1000}
                                                        className="form-control"
                                                        placeholder={amount_from[this.context.Language]}
                                                        value={this.state.ex.Param.AmountTo}
                                                        name="amountTo"
                                                        onChange={this.handleChangeFilter}
                                                    />
                                                </div>
                                                <div className="col-lg-3 kt-margin-b-10">
                                                    <label>{time_from[this.context.Language]}:</label>
                                                    <DateTimePicker
                                                        value={this.state.ex.Param.TimeFrom}
                                                        options={{
                                                            enableTime: false,
                                                            dateFormat: "m/d/Y",
                                                            allowInput: true
                                                        }}
                                                        onChange={(
                                                            selectedDates,
                                                            dateStr,
                                                            instance
                                                        ) => {
                                                            this.state.ex.Param.TimeFrom = dateStr;
                                                            this.setState(this.state);
                                                            this.toPage(1);
                                                        }}
                                                        placeholder={time_from[this.context.Language]}
                                                        datefrom="timeFrom"
                                                    />
                                                </div>
                                                <div className="col-lg-3 kt-margin-b-10">
                                                    <label>{time_to[this.context.Language]}:</label>
                                                    <DateTimePicker
                                                        value={this.state.ex.Param.TimeTo}
                                                        options={{
                                                            enableTime: false,
                                                            dateFormat: "m/d/Y",
                                                            allowInput: true
                                                        }}
                                                        onChange={(
                                                            selectedDates,
                                                            dateStr,
                                                            instance
                                                        ) => {
                                                            this.state.ex.Param.FromDate = dateStr;
                                                            this.setState(this.state);
                                                            this.toPage(1);
                                                        }}
                                                        placeholder={time_to[this.context.Language]}
                                                        datefrom="timeTo"
                                                    />
                                                </div>
                                                <div className="col-lg-3">
                                                    <label>{order_Id[this.context.Language]}:</label>
                                                    <DelayInput
                                                        delayTimeout={1000}
                                                        className="form-control"
                                                        placeholder={order_Id[this.context.Language]}
                                                        value={this.state.ex.Param.orderId}
                                                        name="orderId"
                                                        onChange={this.handleChangeFilter}
                                                    />
                                                </div>
                                                <div className="col-lg-3">
                                                    <label>Label :</label>
                                                    <select
                                                        className="form-control kt-input"
                                                        data-col-index="2"
                                                        value={this.state.ex.Param.Label}
                                                        name="label"
                                                        onChange={this.handleChangeFilter}
                                                    >
                                                        <option value="">---- Select Label ---</option>
                                                        {this.state.ex.LabelDropdownList ? this.state.ex.LabelDropdownList.map(c => {
                                                            return <option value={c.Id}>{c.Name}</option>
                                                        }) : ""}
                                                        
                                                       
                                                    </select>
                                                </div>
                                                <div className="col-lg-3">
                                                    <label>{recordPerPage[this.context.Language]}:</label>
                                                    <select
                                                        className="form-control kt-input"
                                                        data-col-index="2"
                                                        value={this.state.ex.Param.pagesize}
                                                        name="pagesize"
                                                        onChange={this.handleChangeFilter}
                                                    >
                                                        <option value="10">10 {record[this.context.Language]} / {page[this.context.Language]}</option>
                                                        <option value="20">20 {record[this.context.Language]} / {page[this.context.Language]}</option>
                                                        <option value="50">50 {record[this.context.Language]} / {page[this.context.Language]}</option>
                                                        <option value="100">100 {record[this.context.Language]} / {page[this.context.Language]}</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </form>

                                        <div className="kt-separator kt-separator--border-dashed"></div>
                                        <div
                                            id="kt_table_1_wrapper"
                                            className="dataTables_wrapper dt-bootstrap4"
                                        >
                                            <div className="row">
                                                <div className="col-sm-12">
                                                    <table className="table table-striped- table-bordered table-hover table-checkable dataTable no-footer dtr-inline table-responsive">
                                                        <thead>
                                                            <tr role="row">
                                                                <th
                                                                    className="dt-right sorting_disabled"
                                                                    rowSpan="1"
                                                                    colSpan="1"
                                                                    style={{ width: "1%" }}
                                                                    aria-label="Record ID"
                                                                >
                                                                    <label className="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                                                        <input
                                                                            type="checkbox"
                                                                            className="kt-group-checkable"
                                                                            onChange={ev => {
                                                                                this.state.model.Results = this.state.model.Results.map(
                                                                                    e => {
                                                                                        return {
                                                                                            ...e,
                                                                                            IsChecked: ev.target.checked
                                                                                        };
                                                                                    }
                                                                                );
                                                                                this.setState(this.state);
                                                                            }}
                                                                        />
                                                                        <span></span>
                                                                    </label>
                                                                </th>
                                                                <th>OrderId</th>
                                                                <th>Customer Id</th>
                                                                <th>Amount currency</th>
                                                                <th>Order DateTime</th>    
                                                                <th className="text-center"><span className="fa fa-cog"></span></th>    
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            {this.state.model.Results.map((c, index) => {
                                                                return (
                                                                    <tr>
                                                                        <td className="dt-right" tabindex="0">
                                                                            <label className="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                                                                <input
                                                                                    type="checkbox"
                                                                                    className="kt-checkable"
                                                                                    value={c._id}
                                                                                    checked={c.IsChecked}
                                                                                    onChange={e => {
                                                                                        c.IsChecked = !c.IsChecked;
                                                                                        this.setState(this.state);
                                                                                    }}
                                                                                />
                                                                                <span></span>
                                                                            </label>
                                                                        </td>
                                                                        <td>
                                                                            <Link to={`/crmadmin/customerOrder/update/${c.Id}`}>{c.OrderId}</Link>
                                                                        </td>
                                                                        <td>
                                                                            {c.CustomerId}
                                                                        </td>
                                                                        <td>
                                                                            {c.Amount}
                                                                        </td>
                                                                        <td>
                                                                            {moment.utc(c.OrderDateTime).format("M/DD/Y H:mm:ss A")}
                                                                        </td>
                                                                        <td className="text-center">
                                                                            <div className="dropdown dropdown-inline">
                                                                                <button type="button" class="btn btn-clean btn-icon btn-sm btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                                    <i className="fa fa-bars"></i>
                                                                                </button>
                                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                                    <a className="dropdown-item" href="#"><i className="la la-plus"></i> View</a>
                                                                                    <a className="dropdown-item" href="#"><i className="la la-user"></i> Edit</a>
                                                                                    <a className="dropdown-item" href="#"><i className="la la-cloud-download"></i> Delete</a>
                                                                                   
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                );
                                                            })}
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-sm-12 col-md-5">
                                                    <div
                                                        className="dataTables_info"
                                                        id="kt_table_1_info"
                                                        role="status"
                                                        aria-live="polite"
                                                    >
                                                        {total[this.context.Language]} {this.state.model.TotalItemCount} {record[this.context.Language]}
                                                    </div>
                                                </div>
                                                <div className="col-sm-12 col-md-7 dataTables_pager">
                                                    <PagedList
                                                        currentpage={this.state.model.CurrentPage}
                                                        pagesize={this.state.model.PageSize}
                                                        totalitemcount={this.state.model.TotalItemCount}
                                                        totalpagecount={this.state.model.TotalPageCount}
                                                        ajaxcallback={this.toPage.bind(this)}
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="view-order" ref={this.ViewOrderBox}>
                            <a href="javascript:;" id="close-view-order" onClick={e => {
                                e.preventDefault();
                                $(this.ViewOrderBox.current).removeClass("active");
                            }}>
                                <span className="flaticon2-delete"></span>
                                </a>
                        </div>
                    </React.Fragment>
                ) : (
                        <div />
                    )}
            </React.Fragment>
        );
    }
}
CustomerOrderList.contextType = AppContext;
export default CustomerOrderList;
