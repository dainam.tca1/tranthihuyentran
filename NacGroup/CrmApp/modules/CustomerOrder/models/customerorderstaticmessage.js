const order_Id = ["Order Id", "Mã đơn hàng"];
const amount_from = ["Amount From", "Từ"];
const amount_to = ["Amount To", "Đến"];
const information = ["Information", "Thông tin"];
const time_from = ["Time From", "Từ"];
const time_to = ["Time To", "Đến"];
const customer_order = ["Customer Order", "Đơn hàng khách hàng"];
const updatecustomer_order = ["Update Customer Order", "Cập nhật đơn hàng khách hàng"];
const addcustomer_order = ["Add Customer Order", "Thêm đơn hàng khách hàng"];
const customer_order_list = ["Customer Order List", "Danh sách đơn hàng khách hàng"];
const order_id = ["Order Id", "Mã đơn hàng"];
const description = ["Description", "Mô tả"];
const detail_url = ["Detail Url", "Đường dẫn chi tiết"];
const customer_id = ["Customer Id", "Mã khách hàng"];
const amount = ["Amount", "Tổng tiền"];
const order_label = ["Order Label", "Nhãn đơn hàng"];
module.exports = {
    order_Id,
    amount_from,
    amount_to,
    time_from,
    time_to,
    information,
    customer_order,
    updatecustomer_order,
    addcustomer_order,
    customer_order_list,
    order_id,
    description,
    detail_url,
    customer_id,
    amount,
    order_label
};
