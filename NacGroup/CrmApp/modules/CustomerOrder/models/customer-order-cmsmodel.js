﻿class CustomerOrderCmsModel {
    constructor() {
        this.Id = null;
        this.OrderId = null;
        this.Amount = 0;
        this.Description = null;
        this.Label = null;
        this.OrderDateTime = new Date();
        this.CustomerId = null;
        this.DetailUrl = null;
    } 
}

export default CustomerOrderCmsModel;