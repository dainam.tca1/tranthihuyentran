import React, { Component } from "react";
import { Link } from "react-router-dom";
import CustomerOrderCmsModel from "./models/customer-order-cmsmodel";
import ReactSelect from "react-select";
import {
    globalErrorMessage,
    error,
    success,
    add,
    save,
    remove,
    total,
    record,
    page,
    cDate,
    lDate,
    back,
    recordPerPage
} from "../../constants/message";
import {
    order_Id,
    amount_from,
    amount_to,
    time_from,
    time_to,
    order_label,
    information,
    customer_order,
    updatecustomer_order,
    addcustomer_order,
    customer_order_list,
    order_id,
    description,
    detail_url,
    customer_id,
    amount
} from "./models/customerorderstaticmessage";
import AppContext from "../../components/app-context";
//ckeditor


var apiurl = "/crm/api/customerorder";

class CustomerOrderAddUpdate extends Component {
    constructor(props, context) {
        super(props, context);
        var that = this;

        //action để nhận biết hiện đang add hay update
        var action = null;
        if (document.location.href.indexOf("/crmadmin/customerOrder/add") >= 0) {
            action = "add";
        } else if (
            document.location.href.indexOf("/crmadmin/customerOrder/update") >= 0
        ) {
            action = "update";
        }

        that.state = {
            model: new CustomerOrderCmsModel(),
            ex: {
                Title: null,
                Action: action,
                LabelDropdownListTemp: [],
                LabelDropdownList: [],
                LabelId: null,
            }
        };
        that.setState(that.state);
        $.get("/crm/api/orderlabel", (response) => {
            that.state.ex.LabelDropdownListTemp = response.data.Results;
            that.state.ex.LabelDropdownList = response.data.Results.map(c => {
                return {label: c.Name,value: c.Id}
            });
            that.setState(that.state);
        });
        if (action === "update") {
            var id = this.props.match.params.id;
            KTApp.blockPage();
            $.ajax({
                url: apiurl + "/getupdate",
                type: "GET",
                dataType: "json",
                contentType: "application/json",
                data: { id },
                success: response => {
                    KTApp.unblockPage();
                    if (response.status == "success") {
                        that.state.model = response.data;
                        that.state.ex.Title = updatecustomer_order[this.context.Language];
                        that.state.ex.LabelId = that.state.model.Label.Id;
                        document.title = that.state.ex.Title;
                        that.setState(that.state);
                    } else {
                        toastr["error"](response.message, error[this.context.Language]);
                    }
                },
                error: function (er) {
                    KTApp.unblockPage();
                    toastr["error"](globalErrorMessage[this.context.Language], error[this.context.Language]);
                }
            });
        } else {
            that.state.ex.Title = addcustomer_order[this.context.Language];
            document.title = that.state.ex.Title;
            that.setState(that.state);
        }
    }

    submitForm() {
        var that = this;
        KTApp.blockPage();
        $.ajax({
            url: apiurl + "/" + that.state.ex.Action,
            type: "POST",
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify(that.state.model),
            success: response => {
                KTApp.unblockPage();
                if (response.status == "success") {
                    swal.fire({
                        title: success[this.context.Language],
                        text: response.message,
                        type: "success",
                        onClose: () => {
                            that.props.history.push("/crmadmin/emptypage");
                            that.props.history.replace({
                                pathname: that.props.location.pathname,
                                search: that.props.location.search
                            });
                        }
                    });
                } else {
                    toastr["error"](response.message, error[this.context.Language]);
                }
            },
            error: function (er) {
                KTApp.unblockPage();
                toastr["error"](globalErrorMessage[this.context.Language], error[this.context.Language]);
            }
        });
    }

    componentWillMount() {
        //Load script ckeditor lên
      
    }

    componentDidMount() {
        //Active menu
        document.title = this.state.ex.Title;
        $("#kt_aside_menu .kt-menu__item").removeClass("kt-menu__item--active");
        $("#kt_aside_menu .kt-menu__item[data-id='customer-order']").addClass(
            "kt-menu__item--active"
        );
    }

    componentWillUnmount() {
        $("#scriptloading").html("");
    }
    render() {
        return (
            <React.Fragment>
                {this.state && this.state.model ? (
                    <React.Fragment>
                        <div className="kt-subheader kt-grid__item" id="kt_subheader">
                            <div className="kt-subheader__main">
                                <div className="kt-subheader__breadcrumbs">
                                    <Link
                                        to="/crmadmin/dashboard"
                                        className="kt-subheader__breadcrumbs-home"
                                    >
                                        <i className="fa fa-home" />
                                    </Link>
                                    <span className="kt-subheader__breadcrumbs-separator" />
                                    <span className="kt-subheader__breadcrumbs-link">
                                        {customer_order[this.context.Language]}
                                    </span>
                                    <span className="kt-subheader__breadcrumbs-separator" />
                                    <Link
                                        to="/crmadmin/customerOrder"
                                        className="kt-subheader__breadcrumbs-link"
                                    >
                                        {customer_order_list[this.context.Language]}
                                    </Link>
                                    <span className="kt-subheader__breadcrumbs-separator" />
                                    <span className="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
                                        {this.state.ex.Title}
                                    </span>
                                </div>
                            </div>
                            <div className="kt-subheader__toolbar">
                                <div className="kt-subheader__wrapper">
                                    <Link to="/crmadmin/customerOrder" className="btn btn-secondary">
                                        <i className="fa fa-chevron-left" />  {back[this.context.Language]}
                                    </Link>
                                    <a
                                        href="javascript:;"
                                        className="btn btn-primary"
                                        onClick={() => {
                                            this.submitForm();
                                        }}
                                    >
                                        <i className="fa fa-save"></i> {save[this.context.Language]}
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div
                            className="kt-content kt-grid__item kt-grid__item--fluid"
                            id="kt_content"
                        >
                            <div className="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                                <div className="row">
                                    <div className="col-lg-8">
                                        <div className="kt-portlet kt-portlet--mobile">
                                            <div className="kt-portlet__head kt-portlet__head--lg">
                                                <div className="kt-portlet__head-label">
                                                    <h3 className="kt-portlet__head-title">
                                                        {information[this.context.Language]}
                                                    </h3>
                                                </div>
                                            </div>

                                            <div className="kt-portlet__body">
                                                <div className="form-group">
                                                    <label className="control-label">{order_id[this.context.Language]}</label>
                                                    <input
                                                        className="form-control"
                                                        value={this.state.model.OrderId}
                                                        placeholder={order_id[this.context.Language]}
                                                        onChange={e => {
                                                            this.state.model.OrderId = e.target.value;
                                                            this.setState(this.state);
                                                        }}

                                                    />
                                                </div>
                                                <div className="form-group">
                                                    <label className="control-label">{customer_id[this.context.Language]}</label>
                                                    <input
                                                        className="form-control"
                                                        value={this.state.model.CustomerId}
                                                        placeholder={customer_id[this.context.Language]}
                                                        onChange={e => {
                                                            this.state.model.CustomerId = e.target.value;
                                                            this.setState(this.state);
                                                        }}

                                                    />
                                                </div>
                                                <div className="form-group">
                                                    <label className="control-label">{description[this.context.Language]}</label>
                                                    <input
                                                        className="form-control"
                                                        value={this.state.model.Description}
                                                        placeholder={description[this.context.Language]}
                                                        onChange={e => {
                                                            this.state.model.Description = e.target.value;

                                                            this.setState(this.state);
                                                        }}
                                                    />
                                                </div>
                                                <div className="form-group">
                                                    <label className="control-label">{detail_url[this.context.Language]}</label>
                                                    <input
                                                        className="form-control"
                                                        value={this.state.model.DetailUrl}
                                                        placeholder={detail_url[this.context.Language]}
                                                        onChange={e => {
                                                            this.state.model.DetailUrl = e.target.value;

                                                            this.setState(this.state);
                                                        }}
                                                    />
                                                </div>
                                             
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-lg-4">
                                        <div className="kt-portlet kt-portlet--mobile">
                                            <div className="kt-portlet__head kt-portlet__head--lg">
                                                <div className="kt-portlet__head-label">
                                                    <h3 className="kt-portlet__head-title">
                                                        More Info
                                                    </h3>
                                                </div>
                                            </div>

                                            <div className="kt-portlet__body">
                                                <div className="form-group">
                                                    <label className="control-label">{amount[this.context.Language]}</label>
                                                    <input
                                                        className="form-control"
                                                        value={this.state.model.Amount}
                                                        placeholder={amount[this.context.Language]}
                                                        onChange={e => {
                                                            this.state.model.Amount = e.target.value;
                                                            this.setState(this.state);
                                                        }}
                                                    />
                                                </div>
                                                <div className="form-group">
                                                    <label className="control-label">{order_label[this.context.Language]}</label>
                                                    <ReactSelect
                                                        isClearable={false}
                                                        isSearchable={false}
                                                        options={this.state.ex.LabelDropdownList}
                                                        value={
                                                            this.state.ex.LabelDropdownList != null
                                                                ? this.state.ex.LabelDropdownList.filter(
                                                                    option =>
                                                                        option.value == this.state.ex.LabelId
                                                                )
                                                                : ""
                                                        }
                                                        onChange={e => {
                                                            this.state.ex.LabelId = e == null ? "" : e.value;
                                                            this.state.model.Label = this.state.ex.LabelDropdownListTemp.find(l => { return l.Id == this.state.ex.LabelId });
                                                           
                                                            this.setState(this.state);
                                                        }}
                                                    />
                                                </div>


                                          



                                            </div>
                                        </div>
                                    </div>
                                
                                </div>
                            </div>
                        </div>
                    </React.Fragment>
                ) : (
                        <div />
                    )}
            </React.Fragment>
        );
    }
}
CustomerOrderAddUpdate.contextType = AppContext;
export default CustomerOrderAddUpdate;
