class customerModel {
    constructor() {
        this.Id = null;
        this.FirstName = null;
        this.LastName = null;
        this.Phone = null;
        this.Email = null;
        this.BirthDay = 0;
        this.BirthMonth = 0;
        this.BirthYear = 0;
        this.Gender = 0;
        this.AddressList = [];
        this.Labels = [];
        this.Level = null;
      
    }
}

module.exports = customerModel;
