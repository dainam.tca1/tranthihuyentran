class addBookingFormCmsModel {
    constructor() {
        this.Id = null;
        this.FirstName = null;
        this.LastName = null;
        this.CustomerPhone = null;
        this.CustomerEmail = null;
        this.AppointmentItems = [];
        this.Notes = null;
        this.CustomerId = null;
        this.BookingType = 0;
        this.Status = 0;
        this.NumberOfGuest = 0;
        this.CreatedDate = new Date();
        this.UpdatedDate = new Date();
        this.AppointmentDate = new Date();
        this.TotalDuringTime = 5;
        this.BirthDay = 0;
        this.BirtMonth = 0;
        this.BirthYear = 0;
        this.IsRepeat = false;
        this.RepeatReferenceId = null;
        this.RepeatFrequency = null;
        this.RepeatEndType = null;
    }
}

module.exports = addBookingFormCmsModel;
