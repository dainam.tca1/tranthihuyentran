import React, { Component } from "react";
import { BrowserRouter, Redirect, Route } from "react-router-dom";

import AppProvider from "../../CmsApp/components/app-provider";
import Nav from "./nav";
import Home from "./home";
import NailServiceCategoryList from "../../CmsApp/modules/NailService/nailservicecategory-list";
import NailServiceCategoryAddUpdate from "../../CmsApp/modules/NailService/nailservicecategory-addupdate";
import NailServiceList from "../../CmsApp/modules/NailService/nailservice-list";
import NailServiceAddUpdate from "../../CmsApp/modules/NailService/nailservice-addupdate";
import StaffList from "../../CmsApp/modules/ServiceBooking/staff-list";
import StaffAddUpdate from "../../CmsApp/modules/ServiceBooking/staff-addupdate";
import BookingSetting from "../../CmsApp/modules/ServiceBooking/booking-setting";
import AppoinmentList from "../../CmsApp/modules/ServiceBooking/booking-list";
import AppoinmentView from "../../CmsApp/modules/ServiceBooking/booking-view";
import AppointmentCalendar from "./appointment-calendar";
import appointmentReport from "./appointment-report";
import CustomerList from "./customer-list";
class App extends Component {
  constructor() {
    super();

    this.state = {
      ex: {
        Title: "Statistics overview"
      }
    };
  }
  componentDidMount() {
    document.title = "Dashboard";
  }
  render() {
    return (
      <AppProvider>
            <BrowserRouter>
                <React.Fragment>
                    <div className="main-page flex-container">
                        <Nav />
                        <div className="page-content">
                            <Route
                                exact
                                path="/appointment/empty"
                                component={Home}
                            /> 
                            <Route
                                exact
                                path="/appointment"
                                component={appointmentReport}
                            /> 
                            <Route
                                exact
                                path="/appointment/customer"
                                component={CustomerList}
                            /> 
                            <Route
                                exact
                                path="/appointment/appointmentcalendar"
                                component={AppointmentCalendar}
                            /> 
                            <Route
                                exact
                                path="/appointment/list"
                                component={AppoinmentList}
                            />
                            <Route
                                exact
                                path="/appointment/view/:id"
                                component={AppoinmentView}
                            />
                            <Route
                                exact
                                path="/appointment/nailservicecategory"
                                component={NailServiceCategoryList}
                            />
                            <Route
                                exact
                                path="/appointment/nailservicecategory/add"
                                component={NailServiceCategoryAddUpdate}
                            />
                            <Route
                                exact
                                path="/appointment/nailservicecategory/update/:id"
                                component={NailServiceCategoryAddUpdate}
                            />
                            <Route
                                exact
                                path="/appointment/nailservice"
                                component={NailServiceList}
                            />
                            <Route
                                exact
                                path="/appointment/nailservice/add"
                                component={NailServiceAddUpdate}
                            />
                            <Route
                                exact
                                path="/appointment/nailservice/update/:id"
                                component={NailServiceAddUpdate}
                            />
                            <Route
                                exact
                                path="/appointment/appointment-calendar"
                                component={AppointmentCalendar}
                            />
                            <Route
                                exact
                                path="/appointment/servicebookingstaff"
                                component={StaffList}
                            />
                            <Route
                                exact
                                path="/appointment/servicebookingstaff/add"
                                component={StaffAddUpdate}
                            />
                            <Route
                                exact
                                path="/appointment/servicebookingstaff/update/:id"
                                component={StaffAddUpdate}
                            />
                            <Route
                                exact
                                path="/appointment/bookingsetting"
                                component={BookingSetting}
                            />
                        </div>

                    </div>
          </React.Fragment>
        </BrowserRouter>
      </AppProvider>
    );
  }
}
export default App;
