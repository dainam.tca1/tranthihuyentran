﻿import React, { Component } from "react";
import { Link } from "react-router-dom";

class Nav extends Component {
    render() {
        return (
            <div className="nav-container">
                <div className="nav-body">
                    <ul className="flex-center nac-nav-ul">
                        <li
                            className="flex-container flex-center"
                            data-id="appointment-report"
                        >
                            <Link to="/appointment">
                                <i className="la la-chart-bar" />
                                <p>Reports</p>
                            </Link>
                        </li>
                        <li
                            className="flex-container flex-center"
                            data-id="booking-calendar"
                        >
                            <Link to="/appointment/appointmentCalendar">
                                <i className="la la-calendar-check" />
                                <p>Booking</p>
                            </Link>
                        </li>
                        <li
                            className="flex-container flex-center"
                            data-id="customer-list"
                        >
                            <Link to="/appointment/customer">
                                <i className="la la-user" />
                                <p>Customer</p>
                            </Link>
                        </li>
                        <li
                            className="flex-container flex-center"
                            data-id="booking-setting"
                        >
                            <Link to="/appointment/bookingsetting">
                                <i className="la la-cog" />
                                <p>Booking Setting</p>
                            </Link>
                        </li>
                        <li className="flex-container flex-center" data-id="nail-service">
                            <Link to="/appointment/nailservice">
                                <i className="la la-database" />
                                <p>Services</p>
                            </Link>
                        </li>
                        <li
                            className="flex-container flex-center"
                            data-id="nail-service-category"
                        >
                            <Link to="/appointment/nailservicecategory">
                                <i className="la la-object-group" />
                                <p>Categories</p>
                            </Link>
                        </li>
                        <li className="flex-container flex-center" data-id="staff-list">
                            <Link to="/appointment/servicebookingstaff">
                                <i className="la la-users-cog" />
                                <p>Staff</p>
                            </Link>
                        </li>
                    </ul>
                </div>
            </div>
        );
    }
}

export default Nav;
