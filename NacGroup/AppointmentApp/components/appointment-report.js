import React, { Component } from "react";
const api = "/cms/api/appointment/report";
class appointmentReport extends Component {
  constructor() {
    super();
    var that = this;
    this.ChartBox = React.createRef();
    that.state = {
      model: {
        TotalAppointmentToDay: 0,
        TotalAppointmentYesterday: 0,
        TotalAppointmentLastWeek: 0,
        TotalAppointmentMonth: 0,
        TotalAppointmentLastmonth: 0
      },
      ex: {
        Title: "Appointment Report",
        Chart: function() {},
        InfoChart: [],
        TopCustomer: []
      }
    };

    $.get(api + "/GetTotalAppointments", response => {
      that.state.model.TotalAppointmentToDay = response.TotalAppointmentToDay;
      that.state.model.TotalAppointmentYesterday =
        response.TotalAppointmentYesterday;
      that.state.model.TotalAppointmentLastWeek =
        response.TotalAppointmentLastWeek;
      that.state.model.TotalAppointmentMonth = response.TotalAppointmentMonth;
      that.state.model.TotalAppointmentLastmonth =
        response.TotalAppointmentLastmonth;
      that.setState(that.state);
    });
    this.GetChart("today");
  }

  GetChart(apiname) {
    var that = this;
    that.ClearChart();
    KTApp.blockPage();
    $.ajax({
      url: api + `/` + apiname,
      type: "GET",
      dataType: "json",
      contentType: "application/json",
      success: response => {
        KTApp.unblockPage();

        if (response.status == "success") {
          response.data.Value.Labels.forEach((label, index) => {
            that.state.ex.InfoChart.push({
              label: label,
              Total: response.data.Value.TotalAppointmentList[index]
            });
          });

          that.state.ex.TopCustomer = response.data.Value.TopCustomer;
          that.setState(that.state);
          that.initChart(that.ChartBox.current, that.state.ex.InfoChart, 1000);
        }
      },
      error: function(er) {
        toastr["error"]("Error", "error");
      }
    });
  }

  ClearChart() {
    var that = this;
    that.state.ex.InfoChart = [];
    $(that.ChartBox.current).empty();
    that.state.ex.Chart = function() {};
    that.setState(that.state);
  }

  initChart(id, datalist, delay) {
    var that = this;
    if (datalist != null || datalist.length > 0) {
      setTimeout(function() {
        that.state.ex.Chart = new Morris.Bar({
          element: id,
          data: datalist,
          xkey: "label",
          ykeys: ["Total"],
          labels: ["Total Appointments"],
          barColors: ["#fd397a"]
        });
      }, delay);
    }
  }
  componentWillMount() {
    $("#cssloading").html(
      `<link href="/adminstatics/global/plugins/morris.js/morris.css" rel="stylesheet" type="text/css" />`
    );
    $("#scriptloading").html(
      `<script src="/adminstatics/global/plugins/raphael/raphael.js" type="text/javascript"></script><script src="/adminstatics/global/plugins/morris.js/morris.js" type="text/javascript"></script>`
    );
  }

  componentDidMount() {
    document.title = this.state.ex.Title;
    $(".nac-nav-ul li").removeClass("active");
    $(".nac-nav-ul li[data-id='appointment-report']").addClass("active");
  }
  render() {
    return (
      <React.Fragment>
        {this.state && this.state.ex ? (
          <React.Fragment>
            <div className="kt-subheader kt-grid__item" id="kt_subheader">
              <div className="kt-subheader__main">
                <div className="kt-subheader__breadcrumbs">
                  <span className="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
                    {this.state.ex.Title}
                  </span>
                </div>
              </div>
              <div className="kt-subheader__toolbar">
                <div className="kt-subheader__wrapper" />
              </div>
            </div>
            <div
              className="kt-content kt-grid__item kt-grid__item--fluid"
              id="kt_content"
            >
              <div className="kt-portlet kt-portlet--tab">
                <div className="kt-portlet__head">
                  <div className="kt-portlet__head-label">
                    <span className="kt-portlet__head-icon kt-hidden">
                      <i className="la la-gear" />
                    </span>
                    <h3 className="kt-portlet__head-title">
                      Appointment Report
                    </h3>
                  </div>
                </div>
                <div className="kt-portlet__body">
                  {this.state.model ? (
                    <ul className="nav nav-tabs  nav-tabs-line" role="tablist">
                      <li className="nav-item">
                        <a
                          className="nav-link active"
                          data-toggle="tab"
                          href="#kt_tabs_1_1"
                          onClick={e => {
                            this.GetChart("today");
                          }}
                          role="tab"
                        >
                          <span>Today</span>
                          <span>{this.state.model.TotalAppointmentToDay}</span>
                          <span>Appointments</span>
                        </a>
                      </li>
                      <li className="nav-item">
                        <a
                          className="nav-link"
                          data-toggle="tab"
                          href="#kt_tabs_1_3"
                          role="tab"
                          onClick={e => {
                            this.GetChart("yesterday");
                          }}
                        >
                          <span>Yesterday</span>
                          <span>
                            {this.state.model.TotalAppointmentYesterday}
                          </span>
                          <span>Appointments</span>
                        </a>
                      </li>
                      <li className="nav-item">
                        <a
                          className="nav-link"
                          data-toggle="tab"
                          href="#kt_tabs_1_3"
                          role="tab"
                          onClick={e => {
                            this.GetChart("lastweek");
                          }}
                        >
                          <span>7 days</span>
                          <span>
                            {this.state.model.TotalAppointmentLastWeek}
                          </span>
                          <span>Appointments</span>
                        </a>
                      </li>
                      <li className="nav-item">
                        <a
                          className="nav-link"
                          data-toggle="tab"
                          href="#kt_tabs_1_3"
                          role="tab"
                          onClick={e => {
                            this.GetChart("month");
                          }}
                        >
                          <span>This month</span>
                          <span>{this.state.model.TotalAppointmentMonth}</span>
                          <span>Appointments</span>
                        </a>
                      </li>
                      <li className="nav-item">
                        <a
                          className="nav-link"
                          data-toggle="tab"
                          href="#kt_tabs_1_3"
                          role="tab"
                          onClick={e => {
                            this.GetChart("lastmonth");
                          }}
                        >
                          <span>Last month</span>
                          <span>
                            {this.state.model.TotalAppointmentLastmonth}
                          </span>
                          <span>Appointments</span>
                        </a>
                      </li>
                    </ul>
                  ) : (
                    ""
                  )}
                  <div className="tab-content">
                    <div className="row">
                      <div className="col-md-9">
                        <div className="kt-portlet__head">
                          <div className="kt-portlet__head-label">
                            <h3 className="kt-portlet__head-title">
                              Appointments
                            </h3>
                          </div>
                        </div>
                        <div
                          id="kt_morris_3"
                          ref={this.ChartBox}
                          style={{ height: "500px" }}
                        />
                      </div>
                      <div className="col-md-3">
                        <div className="kt-portlet__head">
                          <div className="kt-portlet__head-label">
                            <h3 className="kt-portlet__head-title">
                              Top Customers
                            </h3>
                          </div>
                        </div>
                        <div className="kt-notification-v2">
                          {this.state.ex.TopCustomer
                            ? this.state.ex.TopCustomer.map(cus => {
                                return (
                                  <a
                                    href="javascript:;"
                                    className="kt-notification-v2__item"
                                  >
                                    <div className="kt-notification-v2__item-icon">
                                      {cus.Key.FirstName[0]}
                                    </div>
                                    <div className="kt-notification-v2__itek-wrapper">
                                      <div className="kt-notification-v2__item-title">
                                        {cus.Key.FirstName} {cus.Key.LastName}
                                      </div>
                                      <div className="kt-notification-v2__item-desc">
                                        ({cus.Value} appointments)
                                      </div>
                                    </div>
                                  </a>
                                );
                              })
                            : ""}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </React.Fragment>
        ) : (
          <div />
        )}
      </React.Fragment>
    );
  }
}

export default appointmentReport;
