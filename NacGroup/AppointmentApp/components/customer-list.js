﻿import React, { Component } from "react";
import { DelayInput } from "react-delay-input";
import PagedList from "../../CmsApp/components/pagedlist";
import FilterListPlugin from "../../CmsApp/plugins/filter-list-plugin";
import RemoveListPlugin from "../../CmsApp/plugins/remove-list-plugin";
import customerModel from "./model/customermodel";
import moment from "moment";
const apiurl = "/cms/api/customer";
import {
    globalErrorMessage,
    error,
    success,
    add,
    save,
    close,
    remove,
    total,
    record,
    page,
    cDate,
    lDate,
    recordPerPage,
    are_you_sure,
    data_recover,
    yes_de,
    no_ca,
    data_notfound
} from "../../CmsApp/constants/message";
class CustomerList extends Component {
    constructor() {
        super();
        var that = this;

        that.state = {
            model: null,

            ex: {
                Title: "Customer",
                Param: {
                    name: null,
                },
                AddCustomerModel: new customerModel(),
                UpdateCustomerModel: new customerModel(),

            }
        };
        that.handleChangeFilter = that.handleChangeFilter.bind(that);
        that.handleChangeDataRow = that.handleChangeDataRow.bind(that);

        that.toPage(1);

    }

    toPage(index) {
        FilterListPlugin.filterdata(index, this, apiurl);
    }
    /**
     * Được gọi khi thay đổi các trường filter cập nhật tham số filter vào state
     * Filter dữ liệu theo tham số mới
     * @param {Event} event
     */
    handleChangeFilter(event) {
        FilterListPlugin.handleChangeFilter(event, this);
        this.toPage(1);
    }

    /**
     * Xóa data đã được chọn
     */
    handleDeleteDataRow() {
        RemoveListPlugin.removeList(`${apiurl}/removelist`, this);
    }

    //Được gọi khi change giá trị của mỗi dòng dữ liệu
    handleChangeDataRow(event) {
        const target = event.target;
        const value = target.type === "checkbox" ? target.checked : target.value;
        const name = target.name;
        const index = target.getAttribute("index");
        var newobj = { ...this.state.model.Results[index] };
        newobj[name] = isNaN(value) ? value : parseInt(value);
        var that = this;
        KTApp.blockPage();

        $.ajax({
            url: `${apiurl}/updatecustomize`,
            type: "POST",
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify({
                model: newobj,
                name
            }),

            success: response => {
                KTApp.unblockPage();
                toastr.clear();
                if (response.status == "error") {
                    toastr["error"](response.message, error[that.context.Language]);
                } else {
                    that.state.model.Results[index][name] = newobj[name];
                    that.setState(that.state);
                    toastr["success"](response.message, success[that.context.Language]);
                }
            },
            error: function (er) {
                KTApp.unblockPage();
                toastr["error"](globalErrorMessage[that.context.Language], error[that.context.Language]);
            }
        });
    }

    // Add Customer
    addCustomer() {
        var that = this;
        //var id = that.props.match.params.id;
        KTApp.blockPage();
        $.ajax({
            url: "/cms/api/customer/add",
            type: "POST",
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify(that.state.ex.AddCustomerModel),
            success: response => {
                KTApp.unblockPage();
                if (response.status == "success") {
                    swal.fire({
                        title: success[that.context.Language],
                        text: response.message,
                        type: "success",
                        onClose: () => {  
                            that.props.history.push("/admin/emptypage");
                            that.props.history.replace({
                                pathname: that.props.location.pathname,
                                search: that.props.location.search
                            });
                            
                        }
                    });
                } else {
                    toastr["error"](response.message, error[that.context.Language]);
                }
            },
            error: function (er) {
                KTApp.unblockPage();
                toastr["error"](
                    globalErrorMessage[that.context.Language],
                    error[that.context.Language]
                );
            }
        });
    }

    updateCustomer() {
        var that = this;
        //var id = that.props.match.params.id;
        KTApp.blockPage();
        $.ajax({
            url: "/cms/api/customer/update",
            type: "POST",
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify(that.state.ex.UpdateCustomerModel),
            success: response => {
                KTApp.unblockPage();
                if (response.status == "success") {
                    swal.fire({
                        title: success[that.context.Language],
                        text: response.message,
                        type: "success",
                        onClose: () => {
                            that.props.history.push("/admin/emptypage");
                            that.props.history.replace({
                                pathname: that.props.location.pathname,
                                search: that.props.location.search
                            });
                        }
                    });
                } else {
                    toastr["error"](response.message, error[that.context.Language]);
                }
            },
            error: function (er) {
                KTApp.unblockPage();
                toastr["error"](
                    globalErrorMessage[that.context.Language],
                    error[that.context.Language]
                );
            }
        });
    }


    componentWillMount() {
        $("#cssloading").html(
            ``
        );
        $("#scriptloading").html(
            ``
        );
    }

    componentDidMount() {
        document.title = this.state.ex.Title;
        $(".nac-nav-ul li").removeClass("active");
        $(".nac-nav-ul li[data-id='customer-list']").addClass("active");
    }
    render() {
        return (
            <React.Fragment>
                {this.state && this.state.model ? (
                    <React.Fragment>
                        <div className="kt-subheader kt-grid__item" id="kt_subheader">
                            <div className="kt-subheader__main">
                                <div className="kt-subheader__breadcrumbs">
                                    <span className="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
                                        {this.state.ex.Title}
                                    </span>
                                </div>
                            </div>
                            <div className="kt-subheader__toolbar">
                                <div className="kt-subheader__wrapper">
                                   
                                    <a
                                        href="javascript:;"
                                        onClick={e => {
                                            this.state.ex.AddCustomerModel = new customerModel();     
                                            this.setState(this.state);
                                            $(".addcuspop").fadeIn();
                                        }}
                                        className="btn btn-success"
                                    >
                                        <i className="la la-user"></i> Create 
                                    </a>
                                    <a
                                        href="javascript:;"
                                        onClick={e => this.handleDeleteDataRow()}
                                        className="btn btn-danger"
                                    >
                                        <i className="fa fa-trash-alt"></i> Trash
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div
                            className="kt-content kt-grid__item kt-grid__item--fluid"
                            id="kt_content"
                        >
                            <div className="kt-portlet kt-portlet--tab">
                                <div className="kt-portlet__head">
                                    <div className="kt-portlet__head-label">
                                        <span className="kt-portlet__head-icon kt-hidden">
                                            <i className="la la-gear" />
                                        </span>
                                        <h3 className="kt-portlet__head-title">
                                            Customer List
                                        </h3>
                                    </div>
                                </div>
                                <div className="kt-portlet__body">
                                    <form className="kt-form kt-form--fit">
                                        <div className="row kt-margin-b-20">
                                            <div className="col-lg-3 kt-margin-b-10">
                                                <label>Search:</label>
                                                <DelayInput
                                                    delayTimeout={1000}
                                                    className="form-control kt-input"
                                                    placeholder="Search customer name, phone, email,..."
                                                    value={this.state.ex.Param.name}
                                                    name="keyword"
                                                    onChange={this.handleChangeFilter}
                                                />
                                            </div>

                                            <div className="col-lg-3 kt-margin-b-10">
                                                <label>Record Per Page:</label>
                                                <select
                                                    className="form-control kt-input"
                                                    data-col-index="2"
                                                    value={this.state.ex.Param.pagesize}
                                                    name="pagesize"
                                                    onChange={this.handleChangeFilter}
                                                >
                                                    <option value="10">10 record / page</option>
                                                    <option value="20">20 record / page</option>
                                                    <option value="50">50 record / page</option>
                                                    <option value="100">100 record / page</option>
                                                </select>
                                            </div>
                                        </div>
                                    </form>

                                    <div className="kt-separator kt-separator--border-dashed" />
                                    <div
                                        id="kt_table_1_wrapper"
                                        className="dataTables_wrapper dt-bootstrap4"
                                    >
                                        <div className="row">
                                            <div className="col-sm-12">
                                                <table className="table table-striped- table-bordered table-hover table-checkable responsive no-wrap dataTable dtr-inline collapsed">
                                                    <thead>
                                                        <tr role="row">
                                                            <th
                                                                className="dt-right sorting_disabled"
                                                                rowspan="1"
                                                                colspan="1"
                                                                style={{ width: "1%" }}
                                                                aria-label="Record ID"
                                                            >
                                                                <label className="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                                                    <input
                                                                        type="checkbox"
                                                                        className="kt-group-checkable"
                                                                        onChange={ev => {
                                                                            this.state.model.Results = this.state.model.Results.map(
                                                                                e => {
                                                                                    return {
                                                                                        ...e,
                                                                                        IsChecked: ev.target.checked
                                                                                    };
                                                                                }
                                                                            );
                                                                            this.setState(this.state);
                                                                        }}
                                                                    />
                                                                    <span />
                                                                </label>
                                                            </th>
                                                            <th>Customer Name</th>
                                                            <th>Phone Number</th>
                                                            <th>Email</th>
                                                            <th>Birth Date</th>
                                                       
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        {this.state.model.Results.map((c, index) => {
                                                            return (
                                                                <React.Fragment>
                                                                    <tr>
                                                                        <td className="dt-right" tabindex="0">
                                                                            <label className="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                                                                <input
                                                                                    type="checkbox"
                                                                                    className="kt-checkable"
                                                                                    value={c.Id}
                                                                                    checked={c.IsChecked}
                                                                                    onChange={e => {
                                                                                        c.IsChecked = !c.IsChecked;
                                                                                        this.setState(this.state);
                                                                                    }}
                                                                                />
                                                                                <span />
                                                                            </label>
                                                                        </td>
                                                                        <td>
                                                                            <a
                                                                                href="javascript:;"
                                                                                onClick={e => {
                                                                                    var that = this;
                                                                                    KTApp.blockPage();
                                                                                    $.get(
                                                                                        "/cms/api/customer/GetUpdate?id=" +
                                                                                        c.Id,
                                                                                        res => {
                                                                                            KTApp.unblockPage();
                                                                                            if (!res || res.status == "error") {
                                                                                                toastr["error"](
                                                                                                    data_notfound[that.context.Language],
                                                                                                    error[that.context.Language]
                                                                                                );
                                                                                            } else {
                                                                                                that.state.ex.UpdateCustomerModel = res.data;
                                                                                                that.CallBackUpdateCustomer = this.updateCustomerInAppointmentModel;
                                                                                                that.setState(that.state);
                                                                                                $(".updatecuspop").fadeIn();
                                                                                            }
                                                                                        }
                                                                                    );
                                                                                }}
                                                                            >
                                                                                <i className="fa fa-user" />{" "} 
                                                                                {c.FirstName} {c.LastName}
                                                                            </a>
                                                                        </td>

                                                                        <td>{c.Phone}</td>
                                                                        <td>{c.Email}</td>
                                                                        <td>{`${c.BirthMonth} / ${c.BirthDay} / ${c.BirthYear}`}</td>
                                                                        
                                                                    </tr>
                                                                
                                                                </React.Fragment>
                                                            );
                                                        })}
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-sm-12 col-md-5">
                                                <div
                                                    className="dataTables_info"
                                                    id="kt_table_1_info"
                                                    role="status"
                                                    aria-live="polite"
                                                >
                                                    Total {this.state.model.TotalItemCount} record
                                                </div>
                                            </div>
                                            <div className="col-sm-12 col-md-7 dataTables_pager">
                                                <PagedList
                                                    currentpage={this.state.model.CurrentPage}
                                                    pagesize={this.state.model.PageSize}
                                                    totalitemcount={this.state.model.TotalItemCount}
                                                    totalpagecount={this.state.model.TotalPageCount}
                                                    ajaxcallback={this.toPage.bind(this)}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="au-cuspop addcuspop" style={{ "z-index": "100" , top: 0}}>
                            <div className="p-head">
                                <h3>New Customer</h3>
                                <a
                                    href="javascript:"
                                    className="closebtn"
                                    onClick={() => {
                                        $(".addcuspop").fadeOut();
                                    }}
                                >
                                    <i className="la la-close" />
                                </a>
                            </div>
                            <div className="p-body">
                                <div className="info-form flex-container">
                                    <div className="form-group i-2">
                                        <label className="control-label">First Name</label>
                                        <input
                                            className="form-control"
                                            placeholder="First name"
                                            value={this.state.ex.AddCustomerModel.FirstName}
                                            onChange={e => {
                                                this.state.ex.AddCustomerModel.FirstName = e.target.value;
                                                this.setState(this.state);
                                            }}
                                        />
                                    </div>
                                    <div className="form-group i-2">
                                        <label className="control-label">Last Name</label>
                                        <input
                                            className="form-control"
                                            placeholder="Last name"
                                            value={this.state.ex.AddCustomerModel.LastName}
                                            onChange={e => {
                                                this.state.ex.AddCustomerModel.LastName = e.target.value;
                                                this.setState(this.state);
                                            }}
                                        />
                                    </div>
                                    <div className="form-group i-2">
                                        <label className="control-label">Phone number</label>
                                        <input
                                            className="form-control"
                                            placeholder="Phone number"
                                            value={this.state.ex.AddCustomerModel.Phone}
                                            onChange={e => {
                                                this.state.ex.AddCustomerModel.Phone = e.target.value;
                                                this.setState(this.state);
                                            }}
                                        />
                                    </div>
                                    <div className="form-group i-2">
                                        <label className="control-label">Email</label>
                                        <input
                                            className="form-control"
                                            placeholder="Email"
                                            value={this.state.ex.AddCustomerModel.Email}
                                            onChange={e => {
                                                this.state.ex.AddCustomerModel.Email = e.target.value;
                                                this.setState(this.state);
                                            }}
                                        />
                                    </div>
                                    <div className="form-group i-3">
                                        <label className="control-label">Month of Birth</label>
                                        <input
                                            className="form-control"
                                            placeholder="Month of Birth"
                                            value={
                                                this.state.ex.AddCustomerModel.BirthMonth <= 0
                                                    ? null
                                                    : this.state.ex.AddCustomerModel.BirthMonth
                                            }
                                            onChange={e => {
                                                if (!e.target.value || e.target.value == "") {
                                                    this.state.ex.AddCustomerModel.BirthMonth = 0;
                                                } else {
                                                    this.state.ex.AddCustomerModel.BirthMonth = parseInt(
                                                        e.target.value
                                                    );
                                                }
                                                this.setState(this.state);
                                            }}
                                        />
                                    </div>
                                    <div className="form-group i-3">
                                        <label className="control-label">Day of Birth</label>
                                        <input
                                            className="form-control"
                                            placeholder="Day of Birth"
                                            value={
                                                this.state.ex.AddCustomerModel.BirthDay <= 0
                                                    ? null
                                                    : this.state.ex.AddCustomerModel.BirthDay
                                            }
                                            onChange={e => {
                                                if (!e.target.value || e.target.value == "") {
                                                    this.state.ex.AddCustomerModel.BirthDay = 0;
                                                } else {
                                                    this.state.ex.AddCustomerModel.BirthDay = parseInt(
                                                        e.target.value
                                                    );
                                                }
                                                this.setState(this.state);
                                            }}
                                        />
                                    </div>
                                    <div className="form-group i-3">
                                        <label className="control-label">Year of Birth</label>
                                        <input
                                            className="form-control"
                                            placeholder="Year of Birth"
                                            value={
                                                this.state.ex.AddCustomerModel.BirthYear <= 0
                                                    ? null
                                                    : this.state.ex.AddCustomerModel.BirthYear
                                            }
                                            onChange={e => {
                                                if (!e.target.value || e.target.value == "") {
                                                    this.state.ex.AddCustomerModel.BirthYear = 0;
                                                } else {
                                                    this.state.ex.AddCustomerModel.BirthYear = parseInt(
                                                        e.target.value
                                                    );
                                                }
                                                this.setState(this.state);
                                            }}
                                        />
                                    </div>
                                </div>
                            </div>
                            <div className="p-foot">
                                <button
                                    className="btn btn-primary"
                                    onClick={() => {
                                        this.addCustomer();
                                    }}
                                >
                                    Save Customer
            </button>
                                <button
                                    className="btn btn-secondary"
                                    onClick={() => {
                                        $(".addcuspop").fadeOut();
                                    }}
                                >
                                    Cancel
            </button>
                            </div>
                        </div>
                        <div className="au-cuspop updatecuspop" style={{ "z-index": "100" ,top:0}}>
                            <div className="p-head">
                                <h3>Edit Customer</h3>
                                <a
                                    href="javascript:"
                                    className="closebtn"
                                    onClick={() => {
                                        $(".updatecuspop").fadeOut();
                                    }}
                                >
                                    <i className="la la-close" />
                                </a>
                            </div>
                            <div className="p-body">
                                <div className="info-form flex-container">
                                    <div className="form-group i-2">
                                        <label className="control-label">First Name</label>
                                        <input
                                            className="form-control"
                                            placeholder="First name"
                                            value={this.state.ex.UpdateCustomerModel.FirstName}
                                            onChange={e => {
                                                this.state.ex.UpdateCustomerModel.FirstName =
                                                    e.target.value;
                                                this.setState(this.state);
                                            }}
                                        />
                                    </div>
                                    <div className="form-group i-2">
                                        <label className="control-label">Last Name</label>
                                        <input
                                            className="form-control"
                                            placeholder="Last name"
                                            value={this.state.ex.UpdateCustomerModel.LastName}
                                            onChange={e => {
                                                this.state.ex.UpdateCustomerModel.LastName = e.target.value;
                                                this.setState(this.state);
                                            }}
                                        />
                                    </div>
                                    <div className="form-group i-2">
                                        <label className="control-label">Phone number</label>
                                        <input
                                            className="form-control"
                                            placeholder="Phone number"
                                            value={this.state.ex.UpdateCustomerModel.Phone}
                                            onChange={e => {
                                                this.state.ex.UpdateCustomerModel.Phone = e.target.value;
                                                this.setState(this.state);
                                            }}
                                        />
                                    </div>
                                    <div className="form-group i-2">
                                        <label className="control-label">Email</label>
                                        <input
                                            className="form-control"
                                            placeholder="Email"
                                            value={this.state.ex.UpdateCustomerModel.Email}
                                            onChange={e => {
                                                this.state.ex.UpdateCustomerModel.Email = e.target.value;
                                                this.setState(this.state);
                                            }}
                                        />
                                    </div>
                                    <div className="form-group i-3">
                                        <label className="control-label">Month of Birth</label>
                                        <input
                                            className="form-control"
                                            placeholder="Month of Birth"
                                            value={
                                                this.state.ex.UpdateCustomerModel.BirthMonth <= 0
                                                    ? null
                                                    : this.state.ex.UpdateCustomerModel.BirthMonth
                                            }
                                            onChange={e => {
                                                if (!e.target.value || e.target.value == "") {
                                                    this.state.ex.UpdateCustomerModel.BirthMonth = 0;
                                                } else {
                                                    this.state.ex.UpdateCustomerModel.BirthMonth = parseInt(
                                                        e.target.value
                                                    );
                                                }
                                                this.setState(this.state);
                                            }}
                                        />
                                    </div>
                                    <div className="form-group i-3">
                                        <label className="control-label">Day of Birth</label>
                                        <input
                                            className="form-control"
                                            placeholder="Day of Birth"
                                            value={
                                                this.state.ex.UpdateCustomerModel.BirthDay <= 0
                                                    ? null
                                                    : this.state.ex.UpdateCustomerModel.BirthDay
                                            }
                                            onChange={e => {
                                                if (!e.target.value || e.target.value == "") {
                                                    this.state.ex.UpdateCustomerModel.BirthDay = 0;
                                                } else {
                                                    this.state.ex.UpdateCustomerModel.BirthDay = parseInt(
                                                        e.target.value
                                                    );
                                                }
                                                this.setState(this.state);
                                            }}
                                        />
                                    </div>
                                    <div className="form-group i-3">
                                        <label className="control-label">Year of Birth</label>
                                        <input
                                            className="form-control"
                                            placeholder="Year of Birth"
                                            value={
                                                this.state.ex.UpdateCustomerModel.BirthYear <= 0
                                                    ? null
                                                    : this.state.ex.UpdateCustomerModel.BirthYear
                                            }
                                            onChange={e => {
                                                if (!e.target.value || e.target.value == "") {
                                                    this.state.ex.UpdateCustomerModel.BirthYear = 0;
                                                } else {
                                                    this.state.ex.UpdateCustomerModel.BirthYear = parseInt(
                                                        e.target.value
                                                    );
                                                }
                                                this.setState(this.state);
                                            }}
                                        />
                                    </div>
                                </div>
                            </div>
                            <div className="p-foot">
                                <button
                                    className="btn btn-primary"
                                    onClick={() => {
                                        this.updateCustomer();
                                    }}
                                >
                                    Save Customer
            </button>
                                <button
                                    className="btn btn-secondary"
                                    onClick={() => {
                                        $(".updatecuspop").fadeOut();
                                    }}
                                >
                                    Cancel
            </button>
                            </div>
                        </div>
                    </React.Fragment>
                ) : (
                        <div />
                    )}
            </React.Fragment>
        );
    }
}

export default CustomerList;
