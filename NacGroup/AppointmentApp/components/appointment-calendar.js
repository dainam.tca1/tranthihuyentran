﻿import React, { Component } from "react";
import { Link } from "react-router-dom";
import moment from "moment";
import DateTimePicker from "../../CmsApp/components/datetimepicker";
import addAppointmentModel from "../components/model/addbookingform-cmsmodel";
import customerModel from "./model/customermodel";
import { DelayInput } from "react-delay-input";
import {
  globalErrorMessage,
  error,
  success,
  add,
  save,
  close,
  remove,
  total,
  record,
  page,
  cDate,
  lDate,
  recordPerPage,
  are_you_sure,
  data_recover,
  yes_de,
  no_ca,
  data_notfound
} from "../../CmsApp/constants/message";
const apiurl = "/cms/api/appointment/";
const apiService = "/api/nailservices";
const apiServiceCategory = "/api/nailservicecategories";
import AppContext from "../../CmsApp/components/app-context";

class AppointmentCalendar extends Component {
  constructor(props, context) {
    super(props, context);
    var that = this;
    that.state = {
      model: new addAppointmentModel(),
      ex: {
        Title: "Appointment",
        AllServiceList: [],
        AllCategoryList: [],
        AddCustomerModel: new customerModel(),
        UpdateCustomerModel: new customerModel(),
        SearchCustomerResult: [],
        StaffList: [],
        MainCurrentStaffId: "-1",
        MainCurrentDate: moment.utc(),
        BookingList: [],
        CurrentAppointmentModel: new addAppointmentModel(),
        SelectAppList: [],
        AppointmentAction: "add",
        RepeatDropdownlist: null
      }
    };
    $.get("/cms/api/servicebookingstaff/getallstaff", res => {
      that.state.ex.StaffList = res;
      that.setState(that.state);
    });
    this.getAllServiceList();
    this.getAllServiceCategoryList();
    $.get("/cms/api/appointment/appointmentcalendar/GetDropdownList", res => {
      that.state.ex.RepeatDropdownlist = res;
      that.setState(that.state);
    });
    $.get("/cms/api/appointment/appointmentcalendar/timenow", res => {
      that.state.ex.MainCurrentDate = moment.utc(res).startOf("date");
      that.setState(that.state);
      that.getBooking();
    });
  }

  getAvailableStaff(service, startTime, during, exceptionAppointmentId) {
    var that = this;
    KTApp.blockPage();
    $.ajax({
      url: "/api/nailservicestaffs/available",
      type: "GET",
      dataType: "json",
      contentType: "application/json",
      data: {
        serviceId: service.SkuId,
        startTime: moment.utc(startTime).format("YYYY-MM-DD-hh-mm-A"),
        endTime: moment
          .utc(startTime)
          .add(during, "minutes")
          .format("YYYY-MM-DD-hh-mm-A"),
        exceptionAppointmentId: exceptionAppointmentId
          ? exceptionAppointmentId
          : null
      },
      success: response => {
        KTApp.unblockPage();
        service.StaffList = response;
        if (
          service.StaffList.find(
            m => service.Staff != null && m.Id == service.Staff.Id
          ) == null
        ) {
          service.Staff = null;
          $(service.StaffDom.current).val("");
        }
        that.setState(that.state);
      },
      error: function(er) {
        KTApp.unblockPage();
        toastr["error"]("Staffs are not found", error[this.context.Language]);
      }
    });
  }
  getBooking() {
    var that = this;
    KTApp.blockPage();
    $.ajax({
      url: "/cms/api/appointment/appointmentcalendar/get",
      type: "GET",
      dataType: "json",
      contentType: "application/json",
      data: {
        staffId: that.state.ex.MainCurrentStaffId,
        startTime: that.state.ex.MainCurrentDate.clone()
          .day(0)
          .format("YYYY-MM-DD-hh-mm-A"),
        endTime: that.state.ex.MainCurrentDate.clone()
          .day(6)
          .add(1, "days")
          .format("YYYY-MM-DD-hh-mm-A")
      },
      success: response => {
        KTApp.unblockPage();
        that.state.ex.BookingList = response;
        that.setState(that.state);
      },
      error: function(er) {
        KTApp.unblockPage();
        toastr["error"](
          data_notfound[that.context.Language],
          error[that.context.Language]
        );
      }
    });
  }

  drawDayOfWeek() {
    var dateOfWeek = [];
    for (var i = 0; i < 7; i++) {
      dateOfWeek.push(this.drawDayOfWeekItem(i));
    }
    return dateOfWeek;
  }
  drawDayOfWeekItem(dayindex) {
    return (
      <div>
        <h1>
          {this.state.ex.MainCurrentDate.clone()
            .day(dayindex)
            .format("D")}
        </h1>
        <p className="date">
          {this.state.ex.MainCurrentDate.clone()
            .day(dayindex)
            .format("ddd")}
        </p>
      </div>
    );
  }

  drawBookingOfWeek() {
    var bookingOfWeek = [];
    for (var i = 0; i < 7; i++) {
      bookingOfWeek.push(this.drawBookingOfWeekItem(i));
    }
    return bookingOfWeek;
  }
  drawBookingOfWeekItem(dayindex) {
    var bookingList = this.state.ex.BookingList.filter(m =>
      moment
        .utc(m.AppointmentDate)
        .startOf("date")
        .isSame(this.state.ex.MainCurrentDate.clone().day(dayindex))
    );
    return (
      <div className="time-col">
        {this.createTable()}
        {bookingList.length > 0 && this.drawBookingItem(bookingList)}
        {bookingList.length > 0 && this.drawgroupbooking(bookingList)}
      </div>
    );
  }

  drawBookingItem(bookingList) {
    var that = this;
    var bookingitems = [];
    bookingList.forEach(appoi => {
      appoi.AppointmentItems.forEach((item, index) => {
        if (
          (that.state.ex.MainCurrentStaffId == "-1" && item.Staff == null) ||
          (item.Staff != null &&
            that.state.ex.MainCurrentStaffId == item.Staff.Id)
        ) {
          bookingitems.push(that.drawbookingchildItem(appoi, index));
        }
      });
    });
    return bookingitems;
  }
  drawbookingchildItem(appoi, index) {
    var toppx =
      moment.utc(appoi.AppointmentItems[index].StartDateTime).hour() * 96 +
      moment.utc(appoi.AppointmentItems[index].StartDateTime).minute() /
        60 *
        96;
    var heightpx = appoi.AppointmentItems[index].Duration * 1.6;
    return (
      <a
        className={
          "sticky-bk-view" +
          (appoi.AppointmentType == 1 ? " bgroup" : "") +
          (appoi.BirthDay == moment.utc(appoi.AppointmentDate).dates() &&
          appoi.BirthMonth == moment.utc(appoi.AppointmentDate).months() + 1
            ? " birthday"
            : "")
        }
        style={{ top: toppx + "px", height: heightpx + "px" }}
        onClick={() => {
          this.updateAppointment(appoi.Id);
        }}
      >
        <div className="time flex-container">
          <p className="start-time">
            {moment
              .utc(appoi.AppointmentItems[index].StartDateTime)
              .format("hh:mm A")}
          </p>
          <p className="end-time">
            {moment
              .utc(appoi.AppointmentItems[index].StartDateTime)
              .add(appoi.AppointmentItems[index].Duration, "minutes")
              .format("hh:mm A")}
          </p>
        </div>
        <div className="booking-info">
          <p className="customer-booking-name">
            {appoi.FirstName} {appoi.LastName}
          </p>
          <p className="customer-booking-phone">{appoi.CustomerPhone}</p>
        </div>
      </a>
    );
  }

  drawgroupbooking(bookingList) {
    var that = this;
    var inputlst = [];
    bookingList.forEach(appoi => {
      appoi.AppointmentItems.forEach(item => {
        if (
          (that.state.ex.MainCurrentStaffId == "-1" && item.Staff == null) ||
          (item.Staff != null &&
            that.state.ex.MainCurrentStaffId == item.Staff.Id)
        ) {
          inputlst.push({
            AppointmentId: appoi.Id,
            Start: moment.utc(item.StartDateTime),
            End: moment.utc(item.StartDateTime).add(item.Duration, "minutes"),
            ItemId: item.Id,
            FirstName: appoi.FirstName,
            LastName: appoi.LastName,
            Phone: appoi.CustomerPhone,
            BirthDay: appoi.BirthDay,
            BirthMonth: appoi.BirthMonth,
            BirthYear: appoi.BirthYear
          });
        }
      });
    });
    var templst = [];
    var result = [];
    while (inputlst.length > 0) {
      this.groupingitem(inputlst, templst, result);
    }
    result = result.filter(m => m.length > 1);
    if (result.length == 0) return <React.Fragment />;
    return result.map(g => {
      var gitemMinStart = g.sort((a, b) => {
        return a.Start.isSameOrBefore(b.Start) ? -1 : 1;
      })[0];
      var gitemMaxEnd = g.sort((a, b) => {
        return a.End.isSameOrBefore(b.End) ? 1 : -1;
      })[0];
      var toppx =
        gitemMinStart.Start.hour() * 96 +
        gitemMinStart.Start.minute() / 60 * 96;
      var heightpx = gitemMaxEnd.End.diff(gitemMinStart.Start, "minutes") * 1.6;
      return (
        <div
          className="sticky-bk-group"
          style={{ top: toppx + "px", height: heightpx + "px" }}
          onClick={() => {
            that.state.ex.SelectAppList = g.filter(() => true);
            that.setState(that.state);
            $(".select-appointment-pop").fadeIn();
          }}
        >
          ({g.length}) appointments
        </div>
      );
    });
  }

  groupingitem(inputlst, templst, result) {
    if (templst.length == 0) {
      if (inputlst.length == 0) return 0;
      templst.push(inputlst.pop());
      this.groupingitem(inputlst, templst, result);
    } else {
      this.findfriend(inputlst, templst, templst[0]);
      result.push(templst.filter(() => true));
      templst.splice(0, templst.length);
    }
    return 0;
  }

  findfriend(inputlst, templst, item) {
    var that = this;
    var frends = inputlst.filter(
      m =>
        (item.Start.isSameOrAfter(m.Start) && item.Start.isBefore(m.End)) ||
        (item.End.isAfter(m.Start) && item.End.isSameOrBefore(m.End))
    );

    var ls = inputlst.filter(
      m =>
        !(
          (item.Start.isSameOrAfter(m.Start) && item.Start.isBefore(m.End)) ||
          (item.End.isAfter(m.Start) && item.End.isSameOrBefore(m.End))
        )
    );
    inputlst.splice(0, inputlst.length).push.apply(inputlst, ls);
    templst.push.apply(templst, frends);
    frends.forEach(i => {
      that.findfriend(inputlst, templst, i);
    });
  }

  CallBackAddCustomer(customer) {}

  addCustomer() {
    var that = this;
    //var id = that.props.match.params.id;
    KTApp.blockPage();
    $.ajax({
      url: "/cms/api/customer/add",
      type: "POST",
      dataType: "json",
      contentType: "application/json",
      data: JSON.stringify(that.state.ex.AddCustomerModel),
      success: response => {
        KTApp.unblockPage();
        if (response.status == "success") {
          swal.fire({
            title: success[that.context.Language],
            text: response.message,
            type: "success",
            onClose: () => {
              that.CallBackAddCustomer(response.data);
              $(".addcuspop").fadeOut();
            }
          });
        } else {
          toastr["error"](response.message, error[that.context.Language]);
        }
      },
      error: function(er) {
        KTApp.unblockPage();
        toastr["error"](
          globalErrorMessage[that.context.Language],
          error[that.context.Language]
        );
      }
    });
  }

  CallBackUpdateCustomer(customer) {}
  updateCustomer() {
    var that = this;
    //var id = that.props.match.params.id;
    KTApp.blockPage();
    $.ajax({
      url: "/cms/api/customer/update",
      type: "POST",
      dataType: "json",
      contentType: "application/json",
      data: JSON.stringify(that.state.ex.UpdateCustomerModel),
      success: response => {
        KTApp.unblockPage();
        if (response.status == "success") {
          swal.fire({
            title: success[that.context.Language],
            text: response.message,
            type: "success",
            onClose: () => {
              that.CallBackUpdateCustomer(response.data);
              $(".updatecuspop").fadeOut();
            }
          });
        } else {
          toastr["error"](response.message, error[that.context.Language]);
        }
      },
      error: function(er) {
        KTApp.unblockPage();
        toastr["error"](
          globalErrorMessage[that.context.Language],
          error[that.context.Language]
        );
      }
    });
  }

  updateCustomerInAppointmentModel(customer) {
    this.state.ex.CurrentAppointmentModel.FirstName = customer.FirstName;
    this.state.ex.CurrentAppointmentModel.LastName = customer.LastName;
    this.state.ex.CurrentAppointmentModel.CustomerPhone = customer.Phone;
    this.state.ex.CurrentAppointmentModel.CustomerEmail = customer.Email;
    this.state.ex.CurrentAppointmentModel.BirthDay = customer.BirthDay;
    this.state.ex.CurrentAppointmentModel.BirthMonth = customer.BirthMonth;
    this.state.ex.CurrentAppointmentModel.BirthYear = customer.BirthYear;
    this.state.ex.CurrentAppointmentModel.CustomerId = customer.Id;
    this.setState(this.state);
  }

  onChangeCalendarDate(dateStr) {
    var that = this;
    that.state.ex.MainCurrentDate = moment.utc(dateStr, "MM/DD/YYYY");
    that.setState(that.state);
    that.getBooking();
  }

  getAllServiceList() {
    var that = this;

    KTApp.blockPage();
    $.ajax({
      url: apiService,
      type: "GET",
      dataType: "json",
      contentType: "application/json",
      success: response => {
        KTApp.unblockPage();
        that.state.ex.AllServiceList = response.map(e => {
          return {
            ...e,
            IsChecked: false
          };
        });
        that.setState(that.state);
      },
      error: function(er) {
        KTApp.unblockPage();
        toastr["error"](
          globalErrorMessage[this.context.Language],
          error[this.context.Language]
        );
      }
    });
  }
  getAllServiceCategoryList() {
    var that = this;

    KTApp.blockPage();
    $.ajax({
      url: apiServiceCategory,
      type: "GET",
      dataType: "json",
      contentType: "application/json",
      success: response => {
        KTApp.unblockPage();
        that.state.ex.AllCategoryList = response.map(e => {
          return {
            ...e,
            IsChecked: false
          };
        });
        //
        that.setState(that.state);
        //test
        //console.log(that.state.ex.AllCategoryList);
      },
      error: function(er) {
        KTApp.unblockPage();
        toastr["error"](
          globalErrorMessage[this.context.Language],
          error[this.context.Language]
        );
      }
    });
  }
  createTime() {
    var momentTmp = moment().startOf("date");
    var timeslist = [];

    for (var i = 0; i < 24; i++) {
      momentTmp = momentTmp.add(i > 0 ? 1 : 0, "hours");
      timeslist.push(this.drawTime(momentTmp));
    }
    return timeslist;
  }
  drawTime(momentobj) {
    return (
      <div className="time-box">
        <div className="time-value text-center">
          <span className="hour">{momentobj.format("hh:mm")}</span>
          <br />
          <span className="am-pm">{momentobj.format("A")}</span>
        </div>
      </div>
    );
  }
  createTable() {
    var table = [];

    for (var i = 0; i < 48; i++) {
      table.push(i);
    }
    var indents = table.map(function(i) {
      return <div className="time-of-day-box" />;
    });
    return indents;
  }

  updateAppointment(appointmentId) {
    var that = this;
    $("#addapointmentpop")
      .find(".wrs")
      .val("");
    KTApp.blockPage();
    $.ajax({
      url: "/cms/api/appointment/appointmentcalendar/getupdate",
      type: "GET",
      dataType: "json",
      contentType: "application/json",
      data: { id: appointmentId },
      success: response => {
        KTApp.unblockPage();
        var currentModel = response.data.model;
        currentModel.UpcomingCount = response.data.upcomingcount;

        currentModel.AppointmentItems.forEach(e => {
          var curser = that.state.ex.AllServiceList.find(s => s.Id == e.SkuId);
          if (curser == null) {
            e.SkuId = null;
            e.SkuCode = null;
            e.CategoryId = null;
            e.ServiceList = [];
          } else {
            e.CategoryId = curser.CategoryList[0].Id;
            e.ServiceList = this.state.ex.AllServiceList.filter(
              s => s.CategoryList[0].Id == e.CategoryId
            );
          }
          e.StaffList = [];
          e.ServiceDom = React.createRef();
          e.StaffDom = React.createRef();
          that.getAvailableStaff(
            e,
            e.StartDateTime,
            e.Duration,
            currentModel.Id
          );
        });
        that.state.ex.CurrentAppointmentModel = currentModel;
        that.state.ex.AppointmentAction = "update";
        that.setState(this.state);
        $(".addapointmentpop").fadeIn();
      },
      error: function(er) {
        KTApp.unblockPage();
        toastr["error"](
          data_notfound[that.context.Language],
          error[that.context.Language]
        );
      }
    });
  }

  removeAppointment(id, isremoveupcoming) {
    var that = this;
    swal
      .fire({
        title: are_you_sure[that.context.Language],
        text: data_recover[that.context.Language],
        type: "warning",
        showCancelButton: true,
        confirmButtonText: yes_de[that.context.Language],
        cancelButtonText: no_ca[that.context.Language]
      })
      .then(function(result) {
        if (result.value) {
          KTApp.blockPage();
          $.ajax({
            url: "/cms/api/appointment/appointmentcalendar/delete",
            type: "POST",
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify({
              Id: id,
              IsRemoveUpComming: isremoveupcoming
            }),
            success: response => {
              KTApp.unblockPage();
              if (response.status == "success") {
                swal.fire({
                  title: success[that.context.Language],
                  text: response.message,
                  type: "success",
                  onClose: () => {
                    that.getBooking();
                    $(".addapointmentpop").fadeOut();
                  }
                });
              } else {
                toastr["error"](response.message, error[that.context.Language]);
              }
            },
            error: function(er) {
              KTApp.unblockPage();
              toastr["error"](
                globalErrorMessage[that.context.Language],
                error[that.context.Language]
              );
            }
          });
        } else if (result.dismiss === "cancel") {
        }
      });
  }

  componentWillMount() {
    $("#cssloading").html(
      `<link href="/adminstatics/global/plugins/flatpickr/css/flatpickr.min.css" rel="stylesheet" type="text/css" />`
    );
    $("#scriptloading").html(
      `<script src="/adminstatics/global/plugins/flatpickr/js/flatpickr.min.js" type="text/javascript"></script>`
    );
  }

  componentDidMount() {
    $(".nac-nav-ul li").removeClass("active");
    $(".nac-nav-ul li[data-id='booking-calendar']").addClass("active");
  }
  render() {
    return (
      <div className="appointment-calendar flex-container">
        <div className="calendar">
          <div className="staff-list">
            <div className="section-body">
              {this.state.ex.StaffList.map(staff => (
                <a
                  id={staff.Id}
                  className={
                    "item-staff" +
                    (this.state.ex.MainCurrentStaffId == staff.Id
                      ? " active"
                      : "")
                  }
                  onClick={() => {
                    if (this.state.ex.MainCurrentStaffId != staff.Id) {
                      this.state.ex.MainCurrentStaffId = staff.Id;
                      this.setState(this.state);
                      this.getBooking();
                    }
                  }}
                >
                  <div className="ava">{staff.Name[0]}</div>
                  <div className="des">{staff.Name}</div>
                </a>
              ))}
            </div>
            <div className="section-bot">
              <div
                className={
                  "default-staff" +
                  (this.state.ex.MainCurrentStaffId == "-1" ? " active" : "")
                }
              >
                <a
                  id="staff-default"
                  className="flex-center"
                  onClick={() => {
                    if (this.state.ex.MainCurrentStaffId != "-1") {
                      this.state.ex.MainCurrentStaffId = "-1";
                      this.setState(this.state);
                      this.getBooking();
                    }
                  }}
                >
                  <i className="la la-user" />
                  <div className="des">Default Staff</div>
                </a>
              </div>
            </div>
          </div>
          <div className="calendar-top flex-container">
            <a
              className="btn btn-today btn-primary flex-left"
              onClick={() => {
                var that = this;
                $.get(
                  "/cms/api/appointment/appointmentcalendar/timenow",
                  res => {
                    that.state.ex.MainCurrentDate = moment
                      .utc(res)
                      .startOf("date");
                    that.setState(that.state);
                    that.getBooking();
                  }
                );
              }}
            >
              Today
            </a>
            <div className="control-daytime flex-container flex-left">
              <div className="control-button">
                <a
                  onClick={() => {
                    this.state.ex.MainCurrentDate.add(-7, "days");
                    this.setState(this.state);
                    this.getBooking();
                  }}
                >
                  <i className="la la-angle-left" />
                </a>
                <a
                  onClick={() => {
                    this.state.ex.MainCurrentDate.add(7, "days");
                    this.setState(this.state);
                    this.getBooking();
                  }}
                >
                  <i className="la la-angle-right" />
                </a>
              </div>
              <div className="form-group">
                <DateTimePicker
                  value={this.state.ex.MainCurrentDate.format("MM/DD/YYYY")}
                  options={{
                    enableTime: false,
                    dateFormat: "m/d/Y"
                  }}
                  onChange={(selectedDates, dateStr, instance) => {
                    this.onChangeCalendarDate(dateStr);
                  }}
                />
              </div>
            </div>
            <a
              className="btn btn-booking btn-success flex-right"
              onClick={() => {
                $("#addapointmentpop")
                  .find(".wrs")
                  .val("");
                this.state.ex.CurrentAppointmentModel = new addAppointmentModel();
                this.state.ex.CurrentAppointmentModel.AppointmentDate = this.state.ex.MainCurrentDate.toDate();
                this.state.ex.AppointmentAction = "add";
                this.setState(this.state);
                $(".addapointmentpop").fadeIn();
              }}
            >
              <i className="las la-plus" />Booking
            </a>
          </div>
          <div className="calendar-body">
            <div className="calendar-day flex-container">
              <div id="day-empty" />
              {this.drawDayOfWeek()}
            </div>
            <div className="calendar-time flex-container">
              <div className="time-list">{this.createTime()}</div>
              {this.drawBookingOfWeek()}
            </div>
          </div>
        </div>
        <form
          id="addapointmentpop"
          className="au-popup addapointmentpop"
          style={{ "z-index": "99" }}
        >
          <div className="p-head">
            <h3>
              {this.state.ex.AppointmentAction == "add" ? "New" : "Edit"}{" "}
              Appointment
            </h3>
            <a
              href="javascript:"
              className="closebtn"
              onClick={() => {
                $(".addapointmentpop").fadeOut();
              }}
            >
              <i className="la la-close" />
            </a>
          </div>
          <div className="p-body flex-container">
            <div className="servicecol">
              <div className="bookinginfo flex-container">
                {this.state.ex.CurrentAppointmentModel.SelectedStaff && (
                  <div className="form-group fgroup-f">
                    <strong
                      className="kt-font-danger"
                      style={{ fontSize: "18px" }}
                    >
                      <i className="la la-user-check" /> Request staff:{" "}
                      {this.state.ex.CurrentAppointmentModel.SelectedStaff
                        ? this.state.ex.CurrentAppointmentModel.SelectedStaff
                            .Name
                        : ""}
                    </strong>
                  </div>
                )}
                <div className="form-group fgroup">
                  <label className="control-label">Appointment Date</label>
                  <DateTimePicker
                    value={moment
                      .utc(
                        this.state.ex.CurrentAppointmentModel.AppointmentDate
                      )
                      .format("MM/DD/YYYY")}
                    options={{
                      enableTime: false,
                      dateFormat: "m/d/Y"
                    }}
                    onChange={(selectedDates, dateStr, instance) => {
                      this.state.ex.CurrentAppointmentModel.AppointmentDate = moment
                        .utc(dateStr, "MM/DD/YYYY")
                        .toDate();
                      if (
                        this.state.ex.CurrentAppointmentModel.AppointmentItems
                          .length > 0
                      ) {
                        this.state.ex.CurrentAppointmentModel.AppointmentItems.forEach(
                          ser => {
                            var startTimeTemp = moment.utc(ser.StartDateTime);

                            ser.StartDateTime = moment
                              .utc(
                                this.state.ex.CurrentAppointmentModel
                                  .AppointmentDate
                              )
                              .set({
                                hour: startTimeTemp.hour(),
                                minute: startTimeTemp.minute()
                              })
                              .toDate();
                            this.getAvailableStaff(
                              ser,
                              ser.StartDateTime,
                              ser.Duration,
                              this.state.ex.CurrentAppointmentModel.Id
                            );
                          }
                        );
                      }
                      this.setState(this.state);
                    }}
                  />
                </div>

                <div className="form-group fgroup">
                  <label className="control-label">Status</label>
                  <select
                    className="form-control wrs"
                    value={this.state.ex.CurrentAppointmentModel.Status}
                    onChange={e => {
                      this.state.ex.CurrentAppointmentModel.Status = parseInt(
                        e.target.value
                      );
                      this.setState(this.state);
                    }}
                  >
                    <option value="0">New</option>
                    <option value="1">Confirmed</option>
                    <option value="2">Done</option>
                    <option value="3">Cancel</option>
                  </select>
                </div>
              </div>
              <div className="sv-lst">
                <div className="form-group">
                  {this.state.ex.CurrentAppointmentModel.AppointmentItems.map(
                    (service, index) => {
                      return (
                        <div className="service-item flex-container">
                          <a
                            className="rem"
                            href="javascript:"
                            onClick={() => {
                              var that = this;
                              swal
                                .fire({
                                  title: are_you_sure[this.context.Language],
                                  text: data_recover[this.context.Language],
                                  type: "warning",
                                  showCancelButton: true,
                                  confirmButtonText:
                                    yes_de[this.context.Language],
                                  cancelButtonText: no_ca[this.context.Language]
                                })
                                .then(function(result) {
                                  if (result.value) {
                                    that.state.ex.CurrentAppointmentModel.AppointmentItems.splice(
                                      index,
                                      1
                                    );
                                    that.setState(that.state);
                                  } else if (result.dismiss === "cancel") {
                                  }
                                });
                            }}
                          >
                            <i className="la la-trash" />
                          </a>
                          <div className="form-group fgroup-f">
                            <label className="control-label">Start Time</label>
                            <DateTimePicker
                              value={moment
                                .utc(service.StartDateTime)
                                .format("hh:mm A")}
                              options={{
                                enableTime: true,
                                noCalendar: true,
                                time_24hr: false,
                                dateFormat: "h:i K"
                              }}
                              onChange={(selectedDates, dateStr, instance) => {
                                var selectedMoment = moment(dateStr, "hh:mm A");
                                service.StartDateTime = moment
                                  .utc(service.StartDateTime)
                                  .set({
                                    hour: selectedMoment.hour(),
                                    minute: selectedMoment.minute()
                                  })
                                  .toDate();
                                this.setState(this.state);
                                this.getAvailableStaff(
                                  service,
                                  service.StartDateTime,
                                  service.Duration,
                                  this.state.ex.CurrentAppointmentModel.Id
                                );
                              }}
                            />
                          </div>
                          <div className="form-group fgroup">
                            <label className="control-label">
                              Service Category
                            </label>
                            <select
                              className="form-control"
                              value={service.CategoryId}
                              onChange={e => {
                                if (!e.target.value || e.target.value == "") {
                                  service.CategoryId = null;
                                  service.ServiceList = [];
                                } else {
                                  service.CategoryId = e.target.value;
                                  service.ServiceList = this.state.ex.AllServiceList.filter(
                                    s =>
                                      s.CategoryList[0].Id == service.CategoryId
                                  );
                                }
                                service.SkuId = null;
                                service.SkuCode = null;
                                $(service.ServiceDom.current).val("");
                                this.setState(this.state);
                                this.getAvailableStaff(
                                  service,
                                  service.StartDateTime,
                                  service.Duration,
                                  this.state.ex.CurrentAppointmentModel.Id
                                );
                              }}
                            >
                              <option value="">--Select Category--</option>
                              {this.state.ex.AllCategoryList.map(cat => {
                                return (
                                  <option value={cat.Id}>{cat.Name}</option>
                                );
                              })}
                            </select>
                          </div>
                          <div className="form-group fgroup">
                            <label className="control-label">Service</label>
                            <select
                              ref={service.ServiceDom}
                              className="form-control"
                              value={service.SkuId}
                              onChange={e => {
                                if (!e.target.value || e.target.value == "") {
                                  service.SkuId = null;
                                  service.SkuCode = null;
                                } else {
                                  service.SkuId = e.target.value;
                                  service.SkuCode = e.target.value;
                                  var curservice = service.ServiceList.find(
                                    m => m.Id == service.SkuId
                                  );
                                  if (
                                    curservice != null &&
                                    curservice.Time > 0
                                  ) {
                                    service.Duration = curservice.Time;
                                  }
                                  service.Name = curservice.Name;
                                  service.Avatar = curservice.Avatar;
                                }
                                this.setState(this.state);
                                this.getAvailableStaff(
                                  service,
                                  service.StartDateTime,
                                  service.Duration,
                                  this.state.ex.CurrentAppointmentModel.Id
                                );
                              }}
                            >
                              <option value="">--Select Service--</option>
                              {service.ServiceList.map(ser => {
                                return (
                                  <option value={ser.Id}>{ser.Name}</option>
                                );
                              })}
                            </select>
                          </div>
                          <div className="form-group fgroup">
                            <label className="control-label">Duration</label>
                            <select
                              value={service.Duration}
                              className="form-control"
                              onChange={e => {
                                service.Duration = parseInt(e.target.value);
                                this.setState(this.state);
                                this.getAvailableStaff(
                                  service,
                                  service.StartDateTime,
                                  service.Duration,
                                  this.state.ex.CurrentAppointmentModel.Id
                                );
                              }}
                            >
                              <option value="5">5min</option>
                              <option value="10">10min</option>
                              <option value="15">15min</option>
                              <option value="20">20min</option>
                              <option value="25">25min</option>
                              <option value="30">30min</option>
                              <option value="35">35min</option>
                              <option value="40">40min</option>
                              <option value="45">45min</option>
                              <option value="50">50min</option>
                              <option value="55">55min</option>
                              <option value="60">1h</option>
                              <option value="65">1h 5min</option>
                              <option value="70">1h 10min</option>
                              <option value="75">1h 15min</option>
                              <option value="80">1h 20min</option>
                              <option value="85">1h 25min</option>
                              <option value="90">1h 30min</option>
                              <option value="95">1h 35min</option>
                              <option value="100">1h 40min</option>
                              <option value="105">1h 45min</option>
                              <option value="110">1h 50min</option>
                              <option value="115">1h 55min</option>
                              <option value="120">2h</option>
                              <option value="125">2h 5min</option>
                              <option value="130">2h 10min</option>
                              <option value="135">2h 15min</option>
                              <option value="140">2h 20min</option>
                              <option value="145">2h 25min</option>
                              <option value="150">2h 30min</option>
                              <option value="155">2h 35min</option>
                              <option value="160">2h 40min</option>
                              <option value="165">2h 45min</option>
                              <option value="170">2h 50min</option>
                              <option value="175">2h 55min</option>
                              <option value="180">3h</option>
                              <option value="195">3h 15min</option>
                              <option value="210">3h 30min</option>
                              <option value="225">3h 45min</option>
                              <option value="240">4h</option>
                              <option value="270">4h 30min</option>
                              <option value="300">5h</option>
                              <option value="330">5h 30min</option>
                              <option value="360">6h</option>
                              <option value="390">6h 30min</option>
                              <option value="420">7h</option>
                              <option value="450">7h 30min</option>
                              <option value="480">8h</option>
                              <option value="540">9h</option>
                              <option value="600">10h</option>
                              <option value="660">11h</option>
                              <option value="720">12h</option>
                            </select>
                          </div>
                          <div className="form-group fgroup">
                            <label className="control-label">Staff</label>
                            <select
                              className="form-control"
                              ref={service.StaffDom}
                              value={
                                service.Staff != null ? service.Staff.Id : null
                              }
                              onChange={e => {
                                if (!e.target.value || e.target.value == "") {
                                  service.Staff = null;
                                  $(service.StaffDom.current).val("");
                                } else {
                                  var stafftemp = service.StaffList.find(
                                    s => s.Id == e.target.value
                                  );
                                  service.Staff = {
                                    Id: stafftemp.Id,
                                    Name: stafftemp.Name,
                                    Gender: stafftemp.Gender,
                                    Avatar: stafftemp.Avatar
                                  };
                                }
                                this.setState(this.state);
                              }}
                            >
                              <option value="">--- Select Staff ---</option>
                              {service.StaffList.map(st => {
                                return <option value={st.Id}>{st.Name}</option>;
                              })}
                            </select>
                          </div>
                        </div>
                      );
                    }
                  )}
                </div>
                <div
                  className="form-group add-moresv"
                  onClick={() => {
                    this.state.ex.CurrentAppointmentModel.AppointmentItems.push(
                      {
                        Id: "00000000-0000-0000-0000-000000000000",
                        SkuId: null,
                        SkuCode: null,
                        Name: null,
                        Avatar: null,
                        Staff: null,
                        StartDateTime: this.state.ex.CurrentAppointmentModel
                          .AppointmentDate,
                        Duration: 5,
                        CategoryId: null,
                        ServiceList: [],
                        StaffList: [],
                        ServiceDom: React.createRef(),
                        StaffDom: React.createRef()
                      }
                    );
                    this.setState(this.state);
                  }}
                >
                  <i className="la la-plus" />
                  Add More Service
                </div>
                <div className="form-group">
                  <label class="kt-checkbox kt-checkbox--square kt-checkbox--brand">
                    <input
                      type="checkbox"
                      checked={
                        this.state.ex.CurrentAppointmentModel.AppointmentType ==
                        1
                      }
                      onClick={() => {
                        this.state.ex.CurrentAppointmentModel.AppointmentType =
                          this.state.ex.CurrentAppointmentModel
                            .AppointmentType == 0
                            ? 1
                            : 0;
                        this.setState(this.state);
                      }}
                    />
                    Make group appointments
                    <span />
                  </label>
                </div>
                <div
                  className="form-group"
                  style={{
                    display:
                      this.state.ex.CurrentAppointmentModel.AppointmentType == 1
                        ? "block"
                        : "none"
                  }}
                >
                  <label className="control-label">Number of customers</label>
                  <input
                    type="text"
                    className="form-control wrs"
                    placeholder="Number Of Customers"
                    value={
                      this.state.ex.CurrentAppointmentModel.NumberOfGuest <= 0
                        ? null
                        : this.state.ex.CurrentAppointmentModel.NumberOfGuest
                    }
                    onChange={e => {
                      if (!e.target.value || e.target.value == "") {
                        this.state.ex.CurrentAppointmentModel.NumberOfGuest = 0;
                      } else {
                        this.state.ex.CurrentAppointmentModel.NumberOfGuest = parseInt(
                          e.target.value
                        );
                      }

                      this.setState(this.state);
                    }}
                  />
                </div>
                <div className="form-group">
                  <label className="control-label">Appointment notes</label>
                  <textarea
                    value={this.state.ex.CurrentAppointmentModel.Notes}
                    onChange={e => {
                      this.state.ex.CurrentAppointmentModel.Notes =
                        e.target.value;
                      this.setState(this.state);
                    }}
                    rows="4"
                    className="form-control wrs"
                    placeholder="Add an appointment note (visible to staff only)"
                  />
                </div>
                <div className="form-group">
                  <label class="kt-checkbox kt-checkbox--square kt-checkbox--brand">
                    <input
                      type="checkbox"
                      checked={this.state.ex.CurrentAppointmentModel.IsRepeat}
                      onClick={() => {
                        this.state.ex.CurrentAppointmentModel.IsRepeat = !this
                          .state.ex.CurrentAppointmentModel.IsRepeat;
                        this.setState(this.state);
                      }}
                    />
                    Repeat Appointment
                    <span />
                  </label>
                </div>
                <div
                  className="form-group"
                  style={{
                    display: this.state.ex.CurrentAppointmentModel.IsRepeat
                      ? "block"
                      : "none"
                  }}
                >
                  <label className="control-label">Frequency</label>
                  <select
                    value={
                      this.state.ex.CurrentAppointmentModel.RepeatFrequency
                        ? this.state.ex.CurrentAppointmentModel.RepeatFrequency
                            .Key
                        : null
                    }
                    onChange={e => {
                      this.state.ex.CurrentAppointmentModel.RepeatFrequency = this.state.ex.RepeatDropdownlist.RepeatFrequencyDropdownList.filter(
                        x => x.Key == e.target.value
                      )[0];

                      this.setState(this.state);
                    }}
                    className="form-control wrs"
                  >
                    {this.state.ex.RepeatDropdownlist &&
                      this.state.ex.RepeatDropdownlist.RepeatFrequencyDropdownList.map(
                        e => {
                          return <option value={e.Key}>{e.Name}</option>;
                        }
                      )}
                  </select>
                </div>
                <div
                  className="form-group"
                  style={{
                    display: this.state.ex.CurrentAppointmentModel.IsRepeat
                      ? "block"
                      : "none"
                  }}
                >
                  <label className="control-label">Ends</label>
                  <select
                    value={
                      this.state.ex.CurrentAppointmentModel.RepeatEndType
                        ? this.state.ex.CurrentAppointmentModel.RepeatEndType
                            .Key
                        : null
                    }
                    onChange={e => {
                      this.state.ex.CurrentAppointmentModel.RepeatEndType = this.state.ex.RepeatDropdownlist.RepeatEndTypeDropdownList.filter(
                        x => x.Key == e.target.value
                      )[0];
                      this.setState(this.state);
                    }}
                    className="form-control wrs"
                  >
                    {this.state.ex.RepeatDropdownlist &&
                      this.state.ex.RepeatDropdownlist.RepeatEndTypeDropdownList.map(
                        e => {
                          return <option value={e.Key}>{e.Name}</option>;
                        }
                      )}
                  </select>
                </div>
                {this.state.ex.CurrentAppointmentModel.RepeatEndType &&
                  this.state.ex.CurrentAppointmentModel.RepeatEndType.Key ==
                    "specific-date" && (
                    <div className="form-group">
                      <label className="control-label">Specific End Date</label>
                      <DateTimePicker
                        value={moment
                          .utc(
                            this.state.ex.CurrentAppointmentModel.RepeatEndType
                              .SpecialEndTime
                          )
                          .format("MM/DD/YYYY")}
                        options={{
                          enableTime: false,
                          dateFormat: "m/d/Y"
                        }}
                        onChange={(selectedDates, dateStr, instance) => {
                          this.state.ex.CurrentAppointmentModel.RepeatEndType.SpecialEndTime = moment
                            .utc(dateStr, "MM/DD/YYYY")
                            .toDate();

                          this.setState(this.state);
                        }}
                      />
                    </div>
                  )}
              </div>
            </div>
            <div className="cus-info">
              {!this.state.ex.CurrentAppointmentModel.CustomerId && (
                <div className="searchbox">
                  <div className="search-c">
                    <div className="kt-input-icon kt-input-icon--right">
                      <DelayInput
                        delayTimeout={1000}
                        className="form-control kt-input"
                        placeholder="Search customer..."
                        name="name"
                        onChange={e => {
                          var that = this;
                          KTApp.blockPage();
                          $.get(
                            "/cms/api/customer?keyword=" + e.target.value,
                            res => {
                              KTApp.unblockPage();
                              that.state.ex.SearchCustomerResult =
                                res.data.Results;
                              that.setState(that.state);
                            }
                          );
                        }}
                      />

                      <span className="kt-input-icon__icon kt-input-icon__icon--right">
                        <span>
                          <i className="la la-search" />
                        </span>
                      </span>
                    </div>
                  </div>
                  <div
                    className="addcusstomer"
                    onClick={() => {
                      this.state.ex.AddCustomerModel = new customerModel();
                      this.CallBackAddCustomer = this.updateCustomerInAppointmentModel;
                      this.setState(this.state);
                      $(".addcuspop").fadeIn();
                    }}
                  >
                    <i className="la la-plus" /> Create New Customer
                  </div>
                  <div className="searchresult">
                    {this.state.ex.SearchCustomerResult.map(cus => {
                      return (
                        <div
                          className="search-item"
                          onClick={() => {
                            this.updateCustomerInAppointmentModel(cus);
                          }}
                        >
                          <div className="ava">
                            {cus.FirstName[0].toUpperCase()}
                          </div>
                          <div className="info">
                            <span className="name">
                              {cus.FirstName} {cus.LastName}
                            </span>
                            <span className="phone">{cus.Phone}</span>
                            <span className="email">{cus.Email}</span>
                          </div>
                        </div>
                      );
                    })}
                  </div>
                </div>
              )}

              {this.state.ex.CurrentAppointmentModel.CustomerId && (
                <div className="has-cus">
                  <div className="ava">
                    {this.state.ex.CurrentAppointmentModel.FirstName[0].toUpperCase()}
                  </div>
                  <div className="info">
                    <span className="name">
                      {this.state.ex.CurrentAppointmentModel.FirstName}{" "}
                      {this.state.ex.CurrentAppointmentModel.LastName}
                    </span>
                    <span className="phone">
                      {this.state.ex.CurrentAppointmentModel.CustomerPhone}
                    </span>
                    <span className="email">
                      {this.state.ex.CurrentAppointmentModel.CustomerEmail}
                    </span>
                  </div>
                  <div className="action">
                    <div className="dropdown dropdown-inline">
                      <button
                        type="button"
                        className="btn btn-hover-brand btn-elevate-hover btn-icon btn-sm btn-icon-md"
                        data-toggle="dropdown"
                        aria-haspopup="true"
                        aria-expanded="false"
                      >
                        <i className="flaticon-more" />
                      </button>
                      <div className="dropdown-menu dropdown-menu-right">
                        <a
                          className="dropdown-item"
                          href="javascript:"
                          onClick={() => {
                            var that = this;
                            KTApp.blockPage();
                            $.get(
                              "/cms/api/customer/GetUpdate?id=" +
                                this.state.ex.CurrentAppointmentModel
                                  .CustomerId,
                              res => {
                                KTApp.unblockPage();
                                if (!res || res.status == "error") {
                                  toastr["error"](
                                    data_notfound[that.context.Language],
                                    error[that.context.Language]
                                  );
                                } else {
                                  that.state.ex.UpdateCustomerModel = res.data;
                                  that.CallBackUpdateCustomer = this.updateCustomerInAppointmentModel;
                                  that.setState(that.state);
                                  $(".updatecuspop").fadeIn();
                                }
                              }
                            );
                          }}
                        >
                          <i className="la la-edit" /> Edit detail customer
                        </a>
                        <a
                          className="dropdown-item kt-font-danger"
                          href="javascript:"
                          onClick={() => {
                            this.state.ex.CurrentAppointmentModel.FirstName = null;
                            this.state.ex.CurrentAppointmentModel.LastName = null;
                            this.state.ex.CurrentAppointmentModel.CustomerPhone = null;
                            this.state.ex.CurrentAppointmentModel.CustomerEmail = null;
                            this.state.ex.CurrentAppointmentModel.BirthDay = 0;
                            this.state.ex.CurrentAppointmentModel.BirthMonth = 0;
                            this.state.ex.CurrentAppointmentModel.BirthYear = 0;
                            this.state.ex.CurrentAppointmentModel.CustomerId = null;
                            this.setState(this.state);
                          }}
                        >
                          <i className="la la-trash" /> Remove from appointment
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              )}

              <div className="btn-pop">
                <button
                  type="button"
                  className="btn btn-primary"
                  onClick={() => {
                    var modelajax = {
                      ...this.state.ex.CurrentAppointmentModel
                    };
                    modelajax.AppointmentItems.forEach(e => {
                      e.ServiceDom = null;
                      e.StaffDom = null;
                    });
                    var that = this;
                    //var id = that.props.match.params.id;
                    KTApp.blockPage();
                    $.ajax({
                      url:
                        "/cms/api/appointment/appointmentcalendar/" +
                        that.state.ex.AppointmentAction,
                      type: "POST",
                      dataType: "json",
                      contentType: "application/json",
                      data: JSON.stringify(modelajax),
                      success: response => {
                        KTApp.unblockPage();
                        if (response.status == "success") {
                          swal.fire({
                            title: success[that.context.Language],
                            text: response.message,
                            type: "success",
                            onClose: () => {
                              that.getBooking();
                              $(".addapointmentpop").fadeOut();
                            }
                          });
                        } else {
                          toastr["error"](
                            response.message,
                            error[that.context.Language]
                          );
                        }
                      },
                      error: function(er) {
                        KTApp.unblockPage();
                        toastr["error"](
                          globalErrorMessage[that.context.Language],
                          error[that.context.Language]
                        );
                      }
                    });
                  }}
                >
                  Save Appointment
                </button>
                {this.state.ex.AppointmentAction == "update" &&
                  !this.state.ex.CurrentAppointmentModel.UpcomingCount && (
                    <button
                      type="button"
                      className="btn btn-secondary"
                      style={{ "margin-left": "5px" }}
                      onClick={() => {
                        this.removeAppointment(
                          this.state.ex.CurrentAppointmentModel.Id,
                          false
                        );
                      }}
                    >
                      Remove
                    </button>
                  )}
                {!!this.state.ex.CurrentAppointmentModel.UpcomingCount &&
                  this.state.ex.CurrentAppointmentModel.UpcomingCount > 0 && (
                    <div
                      className="dropdown dropdown-inline"
                      style={{ "margin-left": "5px" }}
                    >
                      <button
                        type="button"
                        className="btn btn-secondary"
                        data-toggle="dropdown"
                        aria-haspopup="true"
                        aria-expanded="false"
                      >
                        Remove
                      </button>
                      <div className="dropdown-menu dropdown-menu-right">
                        <a
                          className="dropdown-item kt-font-danger"
                          href="javascript:"
                          onClick={() => {
                            this.removeAppointment(
                              this.state.ex.CurrentAppointmentModel.Id,
                              false
                            );
                          }}
                        >
                          Remove Only This Appointment
                        </a>
                        <a
                          className="dropdown-item kt-font-danger"
                          href="javascript:"
                          onClick={() => {
                            this.removeAppointment(
                              this.state.ex.CurrentAppointmentModel.Id,
                              true
                            );
                          }}
                        >
                          Remove{" "}
                          {this.state.ex.CurrentAppointmentModel.UpcomingCount}{" "}
                          upcoming repeat appointments
                        </a>
                      </div>
                    </div>
                  )}
              </div>
            </div>
          </div>
        </form>
        <div className="au-cuspop addcuspop" style={{ "z-index": "100" }}>
          <div className="p-head">
            <h3>New Customer</h3>
            <a
              href="javascript:"
              className="closebtn"
              onClick={() => {
                $(".addcuspop").fadeOut();
              }}
            >
              <i className="la la-close" />
            </a>
          </div>
          <div className="p-body">
            <div className="info-form flex-container">
              <div className="form-group i-2">
                <label className="control-label">First Name</label>
                <input
                  className="form-control"
                  placeholder="First name"
                  value={this.state.ex.AddCustomerModel.FirstName}
                  onChange={e => {
                    this.state.ex.AddCustomerModel.FirstName = e.target.value;
                    this.setState(this.state);
                  }}
                />
              </div>
              <div className="form-group i-2">
                <label className="control-label">Last Name</label>
                <input
                  className="form-control"
                  placeholder="Last name"
                  value={this.state.ex.AddCustomerModel.LastName}
                  onChange={e => {
                    this.state.ex.AddCustomerModel.LastName = e.target.value;
                    this.setState(this.state);
                  }}
                />
              </div>
              <div className="form-group i-2">
                <label className="control-label">Phone number</label>
                <input
                  className="form-control"
                  placeholder="Phone number"
                  value={this.state.ex.AddCustomerModel.Phone}
                  onChange={e => {
                    this.state.ex.AddCustomerModel.Phone = e.target.value;
                    this.setState(this.state);
                  }}
                />
              </div>
              <div className="form-group i-2">
                <label className="control-label">Email</label>
                <input
                  className="form-control"
                  placeholder="Email"
                  value={this.state.ex.AddCustomerModel.Email}
                  onChange={e => {
                    this.state.ex.AddCustomerModel.Email = e.target.value;
                    this.setState(this.state);
                  }}
                />
              </div>
              <div className="form-group i-3">
                <label className="control-label">Month of Birth</label>
                <input
                  className="form-control"
                  placeholder="Month of Birth"
                  value={
                    this.state.ex.AddCustomerModel.BirthMonth <= 0
                      ? null
                      : this.state.ex.AddCustomerModel.BirthMonth
                  }
                  onChange={e => {
                    if (!e.target.value || e.target.value == "") {
                      this.state.ex.AddCustomerModel.BirthMonth = 0;
                    } else {
                      this.state.ex.AddCustomerModel.BirthMonth = parseInt(
                        e.target.value
                      );
                    }
                    this.setState(this.state);
                  }}
                />
              </div>
              <div className="form-group i-3">
                <label className="control-label">Day of Birth</label>
                <input
                  className="form-control"
                  placeholder="Day of Birth"
                  value={
                    this.state.ex.AddCustomerModel.BirthDay <= 0
                      ? null
                      : this.state.ex.AddCustomerModel.BirthDay
                  }
                  onChange={e => {
                    if (!e.target.value || e.target.value == "") {
                      this.state.ex.AddCustomerModel.BirthDay = 0;
                    } else {
                      this.state.ex.AddCustomerModel.BirthDay = parseInt(
                        e.target.value
                      );
                    }
                    this.setState(this.state);
                  }}
                />
              </div>
              <div className="form-group i-3">
                <label className="control-label">Year of Birth</label>
                <input
                  className="form-control"
                  placeholder="Year of Birth"
                  value={
                    this.state.ex.AddCustomerModel.BirthYear <= 0
                      ? null
                      : this.state.ex.AddCustomerModel.BirthYear
                  }
                  onChange={e => {
                    if (!e.target.value || e.target.value == "") {
                      this.state.ex.AddCustomerModel.BirthYear = 0;
                    } else {
                      this.state.ex.AddCustomerModel.BirthYear = parseInt(
                        e.target.value
                      );
                    }
                    this.setState(this.state);
                  }}
                />
              </div>
            </div>
          </div>
          <div className="p-foot">
            <button
              className="btn btn-primary"
              onClick={() => {
                this.addCustomer();
              }}
            >
              Save Customer
            </button>
            <button
              className="btn btn-secondary"
              onClick={() => {
                $(".addcuspop").fadeOut();
              }}
            >
              Cancel
            </button>
          </div>
        </div>
        <div className="au-cuspop updatecuspop" style={{ "z-index": "100" }}>
          <div className="p-head">
            <h3>Edit Customer</h3>
            <a
              href="javascript:"
              className="closebtn"
              onClick={() => {
                $(".updatecuspop").fadeOut();
              }}
            >
              <i className="la la-close" />
            </a>
          </div>
          <div className="p-body">
            <div className="info-form flex-container">
              <div className="form-group i-2">
                <label className="control-label">First Name</label>
                <input
                  className="form-control"
                  placeholder="First name"
                  value={this.state.ex.UpdateCustomerModel.FirstName}
                  onChange={e => {
                    this.state.ex.UpdateCustomerModel.FirstName =
                      e.target.value;
                    this.setState(this.state);
                  }}
                />
              </div>
              <div className="form-group i-2">
                <label className="control-label">Last Name</label>
                <input
                  className="form-control"
                  placeholder="Last name"
                  value={this.state.ex.UpdateCustomerModel.LastName}
                  onChange={e => {
                    this.state.ex.UpdateCustomerModel.LastName = e.target.value;
                    this.setState(this.state);
                  }}
                />
              </div>
              <div className="form-group i-2">
                <label className="control-label">Phone number</label>
                <input
                  className="form-control"
                  placeholder="Phone number"
                  value={this.state.ex.UpdateCustomerModel.Phone}
                  onChange={e => {
                    this.state.ex.UpdateCustomerModel.Phone = e.target.value;
                    this.setState(this.state);
                  }}
                />
              </div>
              <div className="form-group i-2">
                <label className="control-label">Email</label>
                <input
                  className="form-control"
                  placeholder="Email"
                  value={this.state.ex.UpdateCustomerModel.Email}
                  onChange={e => {
                    this.state.ex.UpdateCustomerModel.Email = e.target.value;
                    this.setState(this.state);
                  }}
                />
              </div>
              <div className="form-group i-3">
                <label className="control-label">Month of Birth</label>
                <input
                  className="form-control"
                  placeholder="Month of Birth"
                  value={
                    this.state.ex.UpdateCustomerModel.BirthMonth <= 0
                      ? null
                      : this.state.ex.UpdateCustomerModel.BirthMonth
                  }
                  onChange={e => {
                    if (!e.target.value || e.target.value == "") {
                      this.state.ex.UpdateCustomerModel.BirthMonth = 0;
                    } else {
                      this.state.ex.UpdateCustomerModel.BirthMonth = parseInt(
                        e.target.value
                      );
                    }
                    this.setState(this.state);
                  }}
                />
              </div>
              <div className="form-group i-3">
                <label className="control-label">Day of Birth</label>
                <input
                  className="form-control"
                  placeholder="Day of Birth"
                  value={
                    this.state.ex.UpdateCustomerModel.BirthDay <= 0
                      ? null
                      : this.state.ex.UpdateCustomerModel.BirthDay
                  }
                  onChange={e => {
                    if (!e.target.value || e.target.value == "") {
                      this.state.ex.UpdateCustomerModel.BirthDay = 0;
                    } else {
                      this.state.ex.UpdateCustomerModel.BirthDay = parseInt(
                        e.target.value
                      );
                    }
                    this.setState(this.state);
                  }}
                />
              </div>
              <div className="form-group i-3">
                <label className="control-label">Year of Birth</label>
                <input
                  className="form-control"
                  placeholder="Year of Birth"
                  value={
                    this.state.ex.UpdateCustomerModel.BirthYear <= 0
                      ? null
                      : this.state.ex.UpdateCustomerModel.BirthYear
                  }
                  onChange={e => {
                    if (!e.target.value || e.target.value == "") {
                      this.state.ex.UpdateCustomerModel.BirthYear = 0;
                    } else {
                      this.state.ex.UpdateCustomerModel.BirthYear = parseInt(
                        e.target.value
                      );
                    }
                    this.setState(this.state);
                  }}
                />
              </div>
            </div>
          </div>
          <div className="p-foot">
            <button
              className="btn btn-primary"
              onClick={() => {
                this.updateCustomer();
              }}
            >
              Save Customer
            </button>
            <button
              className="btn btn-secondary"
              onClick={() => {
                $(".updatecuspop").fadeOut();
              }}
            >
              Cancel
            </button>
          </div>
        </div>
        <div className="select-appointment-pop">
          <a
            href="javascript:"
            className="closebtn"
            onClick={() => {
              $(".select-appointment-pop").fadeOut();
            }}
          >
            <i className="la la-close" />
          </a>
          <div className="box">
            {this.state.ex.SelectAppList.map(app => {
              return (
                <div
                  className="item"
                  onClick={() => {
                    this.updateAppointment(app.AppointmentId);
                    $(".select-appointment-pop").fadeOut();
                  }}
                >
                  <div className="time">
                    {app.Start.format("hh:mm A")}-{app.End.format("hh:mm A")}
                  </div>
                  <div className="name">
                    {app.FirstName} {app.LastName}
                  </div>
                  <div className="phone">{app.Phone}</div>
                </div>
              );
            })}
          </div>
        </div>
      </div>
    );
  }
}
AppointmentCalendar.contextType = AppContext;
export default AppointmentCalendar;
