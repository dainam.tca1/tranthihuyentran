﻿using System;
using System.Linq;
using System.Net;
using System.Text.Encodings.Web;
using System.Text.Unicode;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using NacGroup.Extensions;
using NacGroup.Infrastructure;
using NacGroup.Module.Core.Data;
using NacGroup.Module.Core.Extensions;
using NacGroup.Module.Core.Interfaces;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Core.Services;
using ServiceCollectionExtensions = NacGroup.Extensions.ServiceCollectionExtensions;

namespace NacGroup
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        private readonly IHostingEnvironment _hostingEnvironment;
        public Startup(IConfiguration configuration, IHostingEnvironment hostingEnvironment)
        {
            Configuration = configuration;
            _hostingEnvironment = hostingEnvironment;
        }


        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            if (_hostingEnvironment.IsDevelopment())
            {
                ServiceCollectionExtensions.CopyModulesToHost(_hostingEnvironment.ContentRootPath);
            }
            services.AddMemoryCache();

            services.AddSingleton(Configuration);

            services.RegisterModules(_hostingEnvironment);

            services.AddAuthentication(o =>
            {
                o.DefaultAuthenticateScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                o.DefaultChallengeScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                o.DefaultSignInScheme = CookieAuthenticationDefaults.AuthenticationScheme;
            }).AddCookie(options =>
            {
                options.LoginPath = "/account/login";
                options.Events = new CookieAuthenticationEvents
                {
                    OnRedirectToLogin = context =>
                    {
                        if ((context.Request.Path.StartsWithSegments("/cms") || context.Request.Path.StartsWithSegments("/admin") || context.Request.Path.StartsWithSegments("/appointment")) && context.Response.StatusCode == (int)HttpStatusCode.OK  )
                        {
                            context.Response.Redirect($"/account/loginadmin{new Uri(context.RedirectUri).Query }");
                            return Task.CompletedTask;
                        }

                        context.Response.Redirect($"/account/login{new Uri(context.RedirectUri).Query }");
                        return Task.CompletedTask;
                    },
                    OnRedirectToAccessDenied = context =>
                    {
                        if ((context.Request.Path.StartsWithSegments("/cms") || context.Request.Path.StartsWithSegments("/admin") || context.Request.Path.StartsWithSegments("/appointment")) && context.Response.StatusCode == (int)HttpStatusCode.OK)
                        {
                            context.Response.Redirect($"/account/loginadmin{new Uri(context.RedirectUri).Query }");
                            return Task.CompletedTask;
                        }

                        context.Response.Redirect($"/account/login{new Uri(context.RedirectUri).Query }");
                        return Task.CompletedTask;
                    }
                };
            }).AddJwtBearer(options =>
            {
                options.SaveToken = true;
                options.RequireHttpsMetadata = false;
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(System.Text.Encoding.ASCII.GetBytes(AllAppGlobalConfig.JwtSecretKey)),

                    ValidateIssuer = false,
                    //ValidIssuer = Configuration["TokenAuthentication:SiteUrl"],

                    ValidateAudience = false,
                    //ValidAudience = Configuration["TokenAuthentication:SiteUrl"],

                    ValidateLifetime = true,
                };
            });

            services.AddAuthorization(o =>
            {
                o.AddPolicy("TenantAuthorize", policy =>
                {
                    policy.Requirements.Add(new TenantRequirement());
                    policy.AuthenticationSchemes.Add(CookieAuthenticationDefaults.AuthenticationScheme);
                });
                o.AddPolicy("TenantJwtAuthorize", policy =>
                {
                    policy.Requirements.Add(new TenantRequirement());
                    policy.AuthenticationSchemes.Add(JwtBearerDefaults.AuthenticationScheme);
                });
            });

            services.AddScoped<IAuthorizationHandler, TenantAuthorizeHandler>();
            

            //Nginx request header
            services.Configure<ForwardedHeadersOptions>(options =>
            {
                options.ForwardedHeaders = Microsoft.AspNetCore.HttpOverrides.ForwardedHeaders.XForwardedFor | Microsoft.AspNetCore.HttpOverrides.ForwardedHeaders.XForwardedProto;
            });


            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            //inject sesssion
            services.AddSession(options =>
            {
                // Set a short timeout for easy testing.
                options.IdleTimeout = TimeSpan.FromHours(4);
                // options.Cookie.HttpOnly = true;
                // Make the session cookie essential
                options.Cookie.IsEssential = true;
            });

            #region Inject database + dbcontext + repository
            services.AddScoped<ITenantProvider, DatabaseTenantProvider>();
            services.AddScoped<IMongoContext, MongoContext>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();

            services.AddTransient(typeof(IRepository<>), typeof(MongoRepository<>));

            #endregion


            //Cái này dùng để call http request trên server
            services.AddHttpClient();

            // customize addMVC và addaplicationpart từng module
            services.AddCustomizedMvc(ModuleManager.Modules);

            services.Configure<RazorViewEngineOptions>(options =>
            {
                options.ViewLocationExpanders.Add(new ThemeableViewLocationExpander());
            });
            // Fix View Source error html
            services.AddSingleton<HtmlEncoder>(
            HtmlEncoder.Create(allowedRanges: new[] {
                UnicodeRanges.All
            }));

            //Chạy từng moduleinit
            var sp = services.BuildServiceProvider();
            var moduleInitializers = sp.GetServices<IModuleInitializer>();
            foreach (var moduleInitializer in moduleInitializers)
            {
                moduleInitializer.ConfigureServices(services);
            }

            services.AddMediatR(ModuleManager.Modules.Select(m => m.Assembly).ToArray());

            ModuleManager.Modules.Clear();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseForwardedHeaders();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/error/page404");
            }
           
            app.UseStaticFiles();
            app.UseAuthentication();

            app.UseCookiePolicy();

            //use session
            app.UseSession();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                  name: "MyArea",
                  template: "{area:exists}/{controller=Home}/{action=Index}/{id?}");
             
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
               
            });

            //Chạy từng moduleinit
            var moduleInitializers = app.ApplicationServices.GetServices<IModuleInitializer>();
            foreach (var moduleInitializer in moduleInitializers)
            {
                moduleInitializer.Configure(app, env);
            }
        }
    }
}
