/**
 * @license Copyright (c) 2003-2019, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

CKEDITOR.editorConfig = function(config) {
  // Define changes to default configuration here. For example:
  // config.language = 'fr';
  // config.uiColor = '#AADC6E';

  config.language = "en";
  config.height = 300;
  config.toolbarCanCollapse = true;
  config.startupMode = "wysiwyg";
  config.htmlEncodeOutput = false;
  config.entities = false;
  config.removeButtons = "Source";

  config.fillEmptyBlocks = false;
  config.tabSpaces = 0;
  // config.forcePasteAsPlainText = true;
  //config.forcePasteAsPlainText = "allow-word";

  config.enterMode = CKEDITOR.ENTER_P;
  config.toolbar = "Full";

  // config.filebrowserBrowseUrl =
  //   "/statics/templates/admin/global/plugins/ckfinder/ckfinder.html";
  // config.filebrowserImageBrowseUrl =
  //   "/statics/templates/admin/global/plugins/ckfinder/ckfinder.html?type=Images";
  // config.filebrowserFlashBrowseUrl =
  //   "/statics/templates/admin/global/plugins/ckfinder/ckfinder.html?type=Flash";
  // config.filebrowserUploadUrl =
  //   "/wwwroot/templates/admin/global/plugins/ckfinder/core/connector/aspx/connector.aspx?command=QuickUpload&type=Files";
  config.filebrowserImageUploadUrl = "/cms/api/cmsplugin/UploadImageCk";
  // config.filebrowserFlashUploadUrl =
  //   "/wwwroot/templates/admin/global/plugins/ckfinder/core/connector/aspx/connector.aspx?command=QuickUpload&type=Flash";
  config.filebrowserWindowWidth = "1000";
  config.filebrowserWindowHeight = "700";

  //   config.extraPlugins = "youtube,sourcedialog";
  config.allowedContent = true;
  config.extraAllowedContent =
    "p(*)[*]{*};div(*)[*]{*};li(*)[*]{*};ul(*)[*]{*}";
  config.image_prefillDimensions = false;
  CKEDITOR.dtd.$removeEmpty.span = false;
  CKEDITOR.dtd.$removeEmpty.i = false;
};
