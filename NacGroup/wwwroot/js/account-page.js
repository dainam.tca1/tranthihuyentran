function submitRegister() {
  var form = $("#register-form");
  var accept = form.find("input[name='accept']").prop("checked");
  if (!accept) {
    alertify.alert("Error", "Please accept the terms of use");
    return -1;
  }
  $("#loading").show();
  $.ajax({
    url: "/account/register",
    type: "POST",
    data: form.serialize(),
    success: function(response) {
      $("#loading").hide();
      if (response.status == "success") {
        alertify.alert("Success", response.message, function() {
            window.location.href = response.data.ReturnUrl ? response.data.ReturnUrl : "/accountapp";
        });
      } else {
        alertify.alert("Error", response.message);
      }
    },
    error: function(er) {
      $("#loading").hide();
      alertify.alert("Error", "Opps! System has some error. Please try again!");
    }
  });
}

function submitLogin() {
  var form = $("#login-form");
  $("#loading").show();
  $.ajax({
    url: location.href,
    type: "POST",
    data: form.serialize(),
    success: function(response) {
      $("#loading").hide();
      if (response.status == "success") {
        alertify.alert("Success", response.message, function() {
            window.location.href = response.data.ReturnUrl ? response.data.ReturnUrl : "/accountapp";
        });
      } else {
        alertify.alert("Error", response.message);
      }
    },
    error: function(er) {
      $("#loading").hide();
      alertify.alert("Error", "Opps! System has some error. Please try again!");
    }
  });
}

function submitforgotpasswordform() {
  var form = $("#forgotpasswordform");
  $("#loading").show();
  $.ajax({
    url: "/account/forgotpassword",
    type: "POST",
    data: form.serialize(),
    success: function(response) {
      $("#loading").hide();
      if (response.status == "success") {
        alertify.alert("Success", response.message, function() {
          window.location.href = "/";
        });
      } else {
        alertify.alert("Error", response.message);
      }
    },
    error: function(er) {
      $("#loading").hide();
      alertify.alert("Error", "Opps! System has some error. Please try again!");
    }
  });
}

function submitResetPasswordForm() {
  var form = $("#resetpasswordform");
  $("#loading").show();
  $.ajax({
    url: window.location.href,
    type: "POST",
    data: form.serialize(),
    success: function(response) {
      $("#loading").hide();
      if (response.status == "success") {
        alertify.alert("Success", response.message, function() {
          window.location.href = "/account/login";
        });
      } else {
        alertify.alert("Error", response.message);
      }
    },
    error: function(er) {
      $("#loading").hide();
      alertify.alert("Error", "Opps! System has some error. Please try again!");
    }
  });
}

