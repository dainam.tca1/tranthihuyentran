/*! jQuery v3.2.1 | (c) JS Foundation and other contributors | jquery.org/license */
function submitcontact(n){var t=$(n).attr("id");return($(n).find(".btn").prop("disabled",!0),$(n).find(".btn-loading").addClass("loading"),loading===!0)?!1:($.ajax({url:$("#"+t).attr("action"),dataType:"json",method:"POST",data:$(n).serialize(),success:function(i){if(loading=!1,i.status=="error")$(n).find(".btn").prop("disabled",!1),$(n).find(".btn-loading").removeClass("loading"),$("#"+t).find(".error-lst").css("display","block"),$("#"+t).find(".error-lst ul").html("<li><span class='ecs-info-circled-alt'><\/span>"+i.message+"<\/li>");else{$("#"+t).find(".error-lst").css("display","none");$(n).find(".btn-loading").removeClass("loading");$("#noti-modal").modal("toggle");$("#noti-modal").on("hide.bs.modal",function(){location.reload()})}},beforeSend:function(){loading=!0},error:function(){loading=!1}}),!1)}!function(n,t){"use strict";"object"==typeof module&&"object"==typeof module.exports?module.exports=n.document?t(n,!0):function(n){if(!n.document)throw new Error("jQuery requires a window with a document");return t(n)}:t(n)}("undefined"!=typeof window?window:this,function(n,t){"use strict";function ir(n,t){t=t||u;var i=t.createElement("script");i.text=n;t.head.appendChild(i).parentNode.removeChild(i)}function fi(n){var t=!!n&&"length"in n&&n.length,r=i.type(n);return"function"!==r&&!i.isWindow(n)&&("array"===r||0===t||"number"==typeof t&&t>0&&t-1 in n)}function l(n,t){return n.nodeName&&n.nodeName.toLowerCase()===t.toLowerCase()}function oi(n,t,r){return i.isFunction(t)?i.grep(n,function(n,i){return!!t.call(n,i,n)!==r}):t.nodeType?i.grep(n,function(n){return n===t!==r}):"string"!=typeof t?i.grep(n,function(n){return ot.call(t,n)>-1!==r}):er.test(t)?i.filter(t,n,r):(t=i.filter(t,n),i.grep(n,function(n){return ot.call(t,n)>-1!==r&&1===n.nodeType}))}function ar(n,t){while((n=n[t])&&1!==n.nodeType);return n}function ne(n){var t={};return i.each(n.match(h)||[],function(n,i){t[i]=!0}),t}function nt(n){return n}function pt(n){throw n;}function vr(n,t,r,u){var f;try{n&&i.isFunction(f=n.promise)?f.call(n).done(t).fail(r):n&&i.isFunction(f=n.then)?f.call(n,t,r):t.apply(void 0,[n].slice(u))}catch(n){r.apply(void 0,[n])}}function bt(){u.removeEventListener("DOMContentLoaded",bt);n.removeEventListener("load",bt);i.ready()}function ht(){this.expando=i.expando+ht.uid++}function re(n){return"true"===n||"false"!==n&&("null"===n?null:n===+n+""?+n:te.test(n)?JSON.parse(n):n)}function pr(n,t,i){var r;if(void 0===i&&1===n.nodeType)if(r="data-"+t.replace(ie,"-$&").toLowerCase(),i=n.getAttribute(r),"string"==typeof i){try{i=re(i)}catch(u){}e.set(n,t,i)}else i=void 0;return i}function kr(n,t,r,u){var h,e=1,l=20,c=u?function(){return u.cur()}:function(){return i.css(n,t,"")},s=c(),o=r&&r[3]||(i.cssNumber[t]?"":"px"),f=(i.cssNumber[t]||"px"!==o&&+s)&&ct.exec(i.css(n,t));if(f&&f[3]!==o){o=o||f[3];r=r||[];f=+s||1;do e=e||".5",f/=e,i.style(n,t,f+o);while(e!==(e=c()/s)&&1!==e&&--l)}return r&&(f=+f||+s||0,h=r[1]?f+(r[1]+1)*r[2]:+r[2],u&&(u.unit=o,u.start=f,u.end=h)),h}function ue(n){var r,f=n.ownerDocument,u=n.nodeName,t=si[u];return t?t:(r=f.body.appendChild(f.createElement(u)),t=i.css(r,"display"),r.parentNode.removeChild(r),"none"===t&&(t="block"),si[u]=t,t)}function tt(n,t){for(var e,u,f=[],i=0,o=n.length;i<o;i++)u=n[i],u.style&&(e=u.style.display,t?("none"===e&&(f[i]=r.get(u,"display")||null,f[i]||(u.style.display="")),""===u.style.display&&kt(u)&&(f[i]=ue(u))):"none"!==e&&(f[i]="none",r.set(u,"display",e)));for(i=0;i<o;i++)null!=f[i]&&(n[i].style.display=f[i]);return n}function o(n,t){var r;return r="undefined"!=typeof n.getElementsByTagName?n.getElementsByTagName(t||"*"):"undefined"!=typeof n.querySelectorAll?n.querySelectorAll(t||"*"):[],void 0===t||t&&l(n,t)?i.merge([n],r):r}function hi(n,t){for(var i=0,u=n.length;i<u;i++)r.set(n[i],"globalEval",!t||r.get(t[i],"globalEval"))}function iu(n,t,r,u,f){for(var e,s,p,a,w,v,h=t.createDocumentFragment(),y=[],l=0,b=n.length;l<b;l++)if(e=n[l],e||0===e)if("object"===i.type(e))i.merge(y,e.nodeType?[e]:e);else if(tu.test(e)){for(s=s||h.appendChild(t.createElement("div")),p=(gr.exec(e)||["",""])[1].toLowerCase(),a=c[p]||c._default,s.innerHTML=a[1]+i.htmlPrefilter(e)+a[2],v=a[0];v--;)s=s.lastChild;i.merge(y,s.childNodes);s=h.firstChild;s.textContent=""}else y.push(t.createTextNode(e));for(h.textContent="",l=0;e=y[l++];)if(u&&i.inArray(e,u)>-1)f&&f.push(e);else if(w=i.contains(e.ownerDocument,e),s=o(h.appendChild(e),"script"),w&&hi(s),r)for(v=0;e=s[v++];)nu.test(e.type||"")&&r.push(e);return h}function gt(){return!0}function it(){return!1}function uu(){try{return u.activeElement}catch(n){}}function ci(n,t,r,u,f,e){var o,s;if("object"==typeof t){"string"!=typeof r&&(u=u||r,r=void 0);for(s in t)ci(n,s,r,u,t[s],e);return n}if(null==u&&null==f?(f=r,u=r=void 0):null==f&&("string"==typeof r?(f=u,u=void 0):(f=u,u=r,r=void 0)),f===!1)f=it;else if(!f)return n;return 1===e&&(o=f,f=function(n){return i().off(n),o.apply(this,arguments)},f.guid=o.guid||(o.guid=i.guid++)),n.each(function(){i.event.add(this,t,f,u,r)})}function fu(n,t){return l(n,"table")&&l(11!==t.nodeType?t:t.firstChild,"tr")?i(">tbody",n)[0]||n:n}function ae(n){return n.type=(null!==n.getAttribute("type"))+"/"+n.type,n}function ve(n){var t=ce.exec(n.type);return t?n.type=t[1]:n.removeAttribute("type"),n}function eu(n,t){var u,c,f,s,h,l,a,o;if(1===t.nodeType){if(r.hasData(n)&&(s=r.access(n),h=r.set(t,s),o=s.events)){delete h.handle;h.events={};for(f in o)for(u=0,c=o[f].length;u<c;u++)i.event.add(t,f,o[f][u])}e.hasData(n)&&(l=e.access(n),a=i.extend({},l),e.set(t,a))}}function ye(n,t){var i=t.nodeName.toLowerCase();"input"===i&&dr.test(n.type)?t.checked=n.checked:"input"!==i&&"textarea"!==i||(t.defaultValue=n.defaultValue)}function rt(n,t,u,e){t=gi.apply([],t);var l,p,c,a,s,w,h=0,v=n.length,k=v-1,y=t[0],b=i.isFunction(y);if(b||v>1&&"string"==typeof y&&!f.checkClone&&he.test(y))return n.each(function(i){var r=n.eq(i);b&&(t[0]=y.call(this,i,r.html()));rt(r,t,u,e)});if(v&&(l=iu(t,n[0].ownerDocument,!1,n,e),p=l.firstChild,1===l.childNodes.length&&(l=p),p||e)){for(c=i.map(o(l,"script"),ae),a=c.length;h<v;h++)s=l,h!==k&&(s=i.clone(s,!0,!0),a&&i.merge(c,o(s,"script"))),u.call(n[h],s,h);if(a)for(w=c[c.length-1].ownerDocument,i.map(c,ve),h=0;h<a;h++)s=c[h],nu.test(s.type||"")&&!r.access(s,"globalEval")&&i.contains(w,s)&&(s.src?i._evalUrl&&i._evalUrl(s.src):ir(s.textContent.replace(le,""),w))}return n}function ou(n,t,r){for(var u,e=t?i.filter(t,n):n,f=0;null!=(u=e[f]);f++)r||1!==u.nodeType||i.cleanData(o(u)),u.parentNode&&(r&&i.contains(u.ownerDocument,u)&&hi(o(u,"script")),u.parentNode.removeChild(u));return n}function lt(n,t,r){var o,s,h,u,e=n.style;return r=r||ni(n),r&&(u=r.getPropertyValue(t)||r[t],""!==u||i.contains(n.ownerDocument,n)||(u=i.style(n,t)),!f.pixelMarginRight()&&li.test(u)&&su.test(t)&&(o=e.width,s=e.minWidth,h=e.maxWidth,e.minWidth=e.maxWidth=e.width=u,u=r.width,e.width=o,e.minWidth=s,e.maxWidth=h)),void 0!==u?u+"":u}function hu(n,t){return{get:function(){return n()?void delete this.get:(this.get=t).apply(this,arguments)}}}function be(n){if(n in vu)return n;for(var i=n[0].toUpperCase()+n.slice(1),t=au.length;t--;)if(n=au[t]+i,n in vu)return n}function yu(n){var t=i.cssProps[n];return t||(t=i.cssProps[n]=be(n)||n),t}function pu(n,t,i){var r=ct.exec(t);return r?Math.max(0,r[2]-(i||0))+(r[3]||"px"):t}function wu(n,t,r,u,f){for(var o=0,e=r===(u?"border":"content")?4:"width"===t?1:0;e<4;e+=2)"margin"===r&&(o+=i.css(n,r+b[e],!0,f)),u?("content"===r&&(o-=i.css(n,"padding"+b[e],!0,f)),"margin"!==r&&(o-=i.css(n,"border"+b[e]+"Width",!0,f))):(o+=i.css(n,"padding"+b[e],!0,f),"padding"!==r&&(o+=i.css(n,"border"+b[e]+"Width",!0,f)));return o}function bu(n,t,r){var o,e=ni(n),u=lt(n,t,e),s="border-box"===i.css(n,"boxSizing",!1,e);return li.test(u)?u:(o=s&&(f.boxSizingReliable()||u===n.style[t]),"auto"===u&&(u=n["offset"+t[0].toUpperCase()+t.slice(1)]),u=parseFloat(u)||0,u+wu(n,t,r||(s?"border":"content"),o,e)+"px")}function s(n,t,i,r,u){return new s.prototype.init(n,t,i,r,u)}function ai(){ti&&(u.hidden===!1&&n.requestAnimationFrame?n.requestAnimationFrame(ai):n.setTimeout(ai,i.fx.interval),i.fx.tick())}function gu(){return n.setTimeout(function(){ut=void 0}),ut=i.now()}function ii(n,t){var r,u=0,i={height:n};for(t=t?1:0;u<4;u+=2-t)r=b[u],i["margin"+r]=i["padding"+r]=n;return t&&(i.opacity=i.width=n),i}function nf(n,t,i){for(var u,f=(a.tweeners[t]||[]).concat(a.tweeners["*"]),r=0,e=f.length;r<e;r++)if(u=f[r].call(i,t,n))return u}function ke(n,t,u){var f,y,w,c,b,s,o,l,k="width"in t||"height"in t,v=this,p={},h=n.style,a=n.nodeType&&kt(n),e=r.get(n,"fxshow");u.queue||(c=i._queueHooks(n,"fx"),null==c.unqueued&&(c.unqueued=0,b=c.empty.fire,c.empty.fire=function(){c.unqueued||b()}),c.unqueued++,v.always(function(){v.always(function(){c.unqueued--;i.queue(n,"fx").length||c.empty.fire()})}));for(f in t)if(y=t[f],ku.test(y)){if(delete t[f],w=w||"toggle"===y,y===(a?"hide":"show")){if("show"!==y||!e||void 0===e[f])continue;a=!0}p[f]=e&&e[f]||i.style(n,f)}if(s=!i.isEmptyObject(t),s||!i.isEmptyObject(p)){k&&1===n.nodeType&&(u.overflow=[h.overflow,h.overflowX,h.overflowY],o=e&&e.display,null==o&&(o=r.get(n,"display")),l=i.css(n,"display"),"none"===l&&(o?l=o:(tt([n],!0),o=n.style.display||o,l=i.css(n,"display"),tt([n]))),("inline"===l||"inline-block"===l&&null!=o)&&"none"===i.css(n,"float")&&(s||(v.done(function(){h.display=o}),null==o&&(l=h.display,o="none"===l?"":l)),h.display="inline-block"));u.overflow&&(h.overflow="hidden",v.always(function(){h.overflow=u.overflow[0];h.overflowX=u.overflow[1];h.overflowY=u.overflow[2]}));s=!1;for(f in p)s||(e?"hidden"in e&&(a=e.hidden):e=r.access(n,"fxshow",{display:o}),w&&(e.hidden=!a),a&&tt([n],!0),v.done(function(){a||tt([n]);r.remove(n,"fxshow");for(f in p)i.style(n,f,p[f])})),s=nf(a?e[f]:0,f,v),f in e||(e[f]=s.start,a&&(s.end=s.start,s.start=0))}}function de(n,t){var r,f,e,u,o;for(r in n)if(f=i.camelCase(r),e=t[f],u=n[r],Array.isArray(u)&&(e=u[1],u=n[r]=u[0]),r!==f&&(n[f]=u,delete n[r]),o=i.cssHooks[f],o&&"expand"in o){u=o.expand(u);delete n[f];for(r in u)r in n||(n[r]=u[r],t[r]=e)}else t[f]=e}function a(n,t,r){var e,o,s=0,l=a.prefilters.length,f=i.Deferred().always(function(){delete c.elem}),c=function(){if(o)return!1;for(var s=ut||gu(),t=Math.max(0,u.startTime+u.duration-s),h=t/u.duration||0,i=1-h,r=0,e=u.tweens.length;r<e;r++)u.tweens[r].run(i);return f.notifyWith(n,[u,i,t]),i<1&&e?t:(e||f.notifyWith(n,[u,1,0]),f.resolveWith(n,[u]),!1)},u=f.promise({elem:n,props:i.extend({},t),opts:i.extend(!0,{specialEasing:{},easing:i.easing._default},r),originalProperties:t,originalOptions:r,startTime:ut||gu(),duration:r.duration,tweens:[],createTween:function(t,r){var f=i.Tween(n,u.opts,t,r,u.opts.specialEasing[t]||u.opts.easing);return u.tweens.push(f),f},stop:function(t){var i=0,r=t?u.tweens.length:0;if(o)return this;for(o=!0;i<r;i++)u.tweens[i].run(1);return t?(f.notifyWith(n,[u,1,0]),f.resolveWith(n,[u,t])):f.rejectWith(n,[u,t]),this}}),h=u.props;for(de(h,u.opts.specialEasing);s<l;s++)if(e=a.prefilters[s].call(u,n,h,u.opts))return i.isFunction(e.stop)&&(i._queueHooks(u.elem,u.opts.queue).stop=i.proxy(e.stop,e)),e;return i.map(h,nf,u),i.isFunction(u.opts.start)&&u.opts.start.call(n,u),u.progress(u.opts.progress).done(u.opts.done,u.opts.complete).fail(u.opts.fail).always(u.opts.always),i.fx.timer(i.extend(c,{elem:n,anim:u,queue:u.opts.queue})),u}function k(n){var t=n.match(h)||[];return t.join(" ")}function d(n){return n.getAttribute&&n.getAttribute("class")||""}function pi(n,t,r,u){var f;if(Array.isArray(t))i.each(t,function(t,i){r||ge.test(n)?u(n,i):pi(n+"["+("object"==typeof i&&null!=i?t:"")+"]",i,r,u)});else if(r||"object"!==i.type(t))u(n,t);else for(f in t)pi(n+"["+f+"]",t[f],r,u)}function cf(n){return function(t,r){"string"!=typeof t&&(r=t,t="*");var u,f=0,e=t.toLowerCase().match(h)||[];if(i.isFunction(r))while(u=e[f++])"+"===u[0]?(u=u.slice(1)||"*",(n[u]=n[u]||[]).unshift(r)):(n[u]=n[u]||[]).push(r)}}function lf(n,t,r,u){function e(s){var h;return f[s]=!0,i.each(n[s]||[],function(n,i){var s=i(t,r,u);return"string"!=typeof s||o||f[s]?o?!(h=s):void 0:(t.dataTypes.unshift(s),e(s),!1)}),h}var f={},o=n===wi;return e(t.dataTypes[0])||!f["*"]&&e("*")}function ki(n,t){var r,u,f=i.ajaxSettings.flatOptions||{};for(r in t)void 0!==t[r]&&((f[r]?n:u||(u={}))[r]=t[r]);return u&&i.extend(!0,n,u),n}function so(n,t,i){for(var e,u,f,o,s=n.contents,r=n.dataTypes;"*"===r[0];)r.shift(),void 0===e&&(e=n.mimeType||t.getResponseHeader("Content-Type"));if(e)for(u in s)if(s[u]&&s[u].test(e)){r.unshift(u);break}if(r[0]in i)f=r[0];else{for(u in i){if(!r[0]||n.converters[u+" "+r[0]]){f=u;break}o||(o=u)}f=f||o}if(f)return f!==r[0]&&r.unshift(f),i[f]}function ho(n,t,i,r){var h,u,f,s,e,o={},c=n.dataTypes.slice();if(c[1])for(f in n.converters)o[f.toLowerCase()]=n.converters[f];for(u=c.shift();u;)if(n.responseFields[u]&&(i[n.responseFields[u]]=t),!e&&r&&n.dataFilter&&(t=n.dataFilter(t,n.dataType)),e=u,u=c.shift())if("*"===u)u=e;else if("*"!==e&&e!==u){if(f=o[e+" "+u]||o["* "+u],!f)for(h in o)if(s=h.split(" "),s[1]===u&&(f=o[e+" "+s[0]]||o["* "+s[0]])){f===!0?f=o[h]:o[h]!==!0&&(u=s[0],c.unshift(s[1]));break}if(f!==!0)if(f&&n.throws)t=f(t);else try{t=f(t)}catch(l){return{state:"parsererror",error:f?l:"No conversion from "+e+" to "+u}}}return{state:"success",data:t}}var p=[],u=n.document,pf=Object.getPrototypeOf,w=p.slice,gi=p.concat,ui=p.push,ot=p.indexOf,vt={},nr=vt.toString,yt=vt.hasOwnProperty,tr=yt.toString,wf=tr.call(Object),f={},rr="3.2.1",i=function(n,t){return new i.fn.init(n,t)},bf=/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,kf=/^-ms-/,df=/-([a-z])/g,gf=function(n,t){return t.toUpperCase()},y,ei,er,or,sr,hr,cr,lr,h,yr,wt,v,st,si,tu,ut,ti,ku,du,tf,ft,rf,uf,ff,vi,af,et,di,ri,vf,yf;i.fn=i.prototype={jquery:rr,constructor:i,length:0,toArray:function(){return w.call(this)},get:function(n){return null==n?w.call(this):n<0?this[n+this.length]:this[n]},pushStack:function(n){var t=i.merge(this.constructor(),n);return t.prevObject=this,t},each:function(n){return i.each(this,n)},map:function(n){return this.pushStack(i.map(this,function(t,i){return n.call(t,i,t)}))},slice:function(){return this.pushStack(w.apply(this,arguments))},first:function(){return this.eq(0)},last:function(){return this.eq(-1)},eq:function(n){var i=this.length,t=+n+(n<0?i:0);return this.pushStack(t>=0&&t<i?[this[t]]:[])},end:function(){return this.prevObject||this.constructor()},push:ui,sort:p.sort,splice:p.splice};i.extend=i.fn.extend=function(){var e,f,r,t,o,s,n=arguments[0]||{},u=1,c=arguments.length,h=!1;for("boolean"==typeof n&&(h=n,n=arguments[u]||{},u++),"object"==typeof n||i.isFunction(n)||(n={}),u===c&&(n=this,u--);u<c;u++)if(null!=(e=arguments[u]))for(f in e)r=n[f],t=e[f],n!==t&&(h&&t&&(i.isPlainObject(t)||(o=Array.isArray(t)))?(o?(o=!1,s=r&&Array.isArray(r)?r:[]):s=r&&i.isPlainObject(r)?r:{},n[f]=i.extend(h,s,t)):void 0!==t&&(n[f]=t));return n};i.extend({expando:"jQuery"+(rr+Math.random()).replace(/\D/g,""),isReady:!0,error:function(n){throw new Error(n);},noop:function(){},isFunction:function(n){return"function"===i.type(n)},isWindow:function(n){return null!=n&&n===n.window},isNumeric:function(n){var t=i.type(n);return("number"===t||"string"===t)&&!isNaN(n-parseFloat(n))},isPlainObject:function(n){var t,i;return!(!n||"[object Object]"!==nr.call(n))&&(!(t=pf(n))||(i=yt.call(t,"constructor")&&t.constructor,"function"==typeof i&&tr.call(i)===wf))},isEmptyObject:function(n){for(var t in n)return!1;return!0},type:function(n){return null==n?n+"":"object"==typeof n||"function"==typeof n?vt[nr.call(n)]||"object":typeof n},globalEval:function(n){ir(n)},camelCase:function(n){return n.replace(kf,"ms-").replace(df,gf)},each:function(n,t){var r,i=0;if(fi(n)){for(r=n.length;i<r;i++)if(t.call(n[i],i,n[i])===!1)break}else for(i in n)if(t.call(n[i],i,n[i])===!1)break;return n},trim:function(n){return null==n?"":(n+"").replace(bf,"")},makeArray:function(n,t){var r=t||[];return null!=n&&(fi(Object(n))?i.merge(r,"string"==typeof n?[n]:n):ui.call(r,n)),r},inArray:function(n,t,i){return null==t?-1:ot.call(t,n,i)},merge:function(n,t){for(var u=+t.length,i=0,r=n.length;i<u;i++)n[r++]=t[i];return n.length=r,n},grep:function(n,t,i){for(var u,f=[],r=0,e=n.length,o=!i;r<e;r++)u=!t(n[r],r),u!==o&&f.push(n[r]);return f},map:function(n,t,i){var e,u,r=0,f=[];if(fi(n))for(e=n.length;r<e;r++)u=t(n[r],r,i),null!=u&&f.push(u);else for(r in n)u=t(n[r],r,i),null!=u&&f.push(u);return gi.apply([],f)},guid:1,proxy:function(n,t){var u,f,r;if("string"==typeof t&&(u=n[t],t=n,n=u),i.isFunction(n))return f=w.call(arguments,2),r=function(){return n.apply(t||this,f.concat(w.call(arguments)))},r.guid=n.guid=n.guid||i.guid++,r},now:Date.now,support:f});"function"==typeof Symbol&&(i.fn[Symbol.iterator]=p[Symbol.iterator]);i.each("Boolean Number String Function Array Date RegExp Object Error Symbol".split(" "),function(n,t){vt["[object "+t+"]"]=t.toLowerCase()});y=function(n){function u(n,t,r,u){var s,w,l,a,d,y,g,p=t&&t.ownerDocument,v=t?t.nodeType:9;if(r=r||[],"string"!=typeof n||!n||1!==v&&9!==v&&11!==v)return r;if(!u&&((t?t.ownerDocument||t:c)!==i&&b(t),t=t||i,h)){if(11!==v&&(d=cr.exec(n)))if(s=d[1]){if(9===v){if(!(l=t.getElementById(s)))return r;if(l.id===s)return r.push(l),r}else if(p&&(l=p.getElementById(s))&&et(t,l)&&l.id===s)return r.push(l),r}else{if(d[2])return k.apply(r,t.getElementsByTagName(n)),r;if((s=d[3])&&e.getElementsByClassName&&t.getElementsByClassName)return k.apply(r,t.getElementsByClassName(s)),r}if(e.qsa&&!lt[n+" "]&&(!o||!o.test(n))){if(1!==v)p=t,g=n;else if("object"!==t.nodeName.toLowerCase()){for((a=t.getAttribute("id"))?a=a.replace(vi,yi):t.setAttribute("id",a=f),y=ft(n),w=y.length;w--;)y[w]="#"+a+" "+yt(y[w]);g=y.join(",");p=ni.test(n)&&ri(t.parentNode)||t}if(g)try{return k.apply(r,p.querySelectorAll(g)),r}catch(nt){}finally{a===f&&t.removeAttribute("id")}}}return si(n.replace(at,"$1"),t,r,u)}function ti(){function n(r,u){return i.push(r+" ")>t.cacheLength&&delete n[i.shift()],n[r+" "]=u}var i=[];return n}function l(n){return n[f]=!0,n}function a(n){var t=i.createElement("fieldset");try{return!!n(t)}catch(r){return!1}finally{t.parentNode&&t.parentNode.removeChild(t);t=null}}function ii(n,i){for(var r=n.split("|"),u=r.length;u--;)t.attrHandle[r[u]]=i}function wi(n,t){var i=t&&n,r=i&&1===n.nodeType&&1===t.nodeType&&n.sourceIndex-t.sourceIndex;if(r)return r;if(i)while(i=i.nextSibling)if(i===t)return-1;return n?1:-1}function ar(n){return function(t){var i=t.nodeName.toLowerCase();return"input"===i&&t.type===n}}function vr(n){return function(t){var i=t.nodeName.toLowerCase();return("input"===i||"button"===i)&&t.type===n}}function bi(n){return function(t){return"form"in t?t.parentNode&&t.disabled===!1?"label"in t?"label"in t.parentNode?t.parentNode.disabled===n:t.disabled===n:t.isDisabled===n||t.isDisabled!==!n&&lr(t)===n:t.disabled===n:"label"in t&&t.disabled===n}}function it(n){return l(function(t){return t=+t,l(function(i,r){for(var u,f=n([],i.length,t),e=f.length;e--;)i[u=f[e]]&&(i[u]=!(r[u]=i[u]))})})}function ri(n){return n&&"undefined"!=typeof n.getElementsByTagName&&n}function ki(){}function yt(n){for(var t=0,r=n.length,i="";t<r;t++)i+=n[t].value;return i}function pt(n,t,i){var r=t.dir,u=t.next,e=u||r,o=i&&"parentNode"===e,s=di++;return t.first?function(t,i,u){while(t=t[r])if(1===t.nodeType||o)return n(t,i,u);return!1}:function(t,i,h){var c,l,a,y=[v,s];if(h){while(t=t[r])if((1===t.nodeType||o)&&n(t,i,h))return!0}else while(t=t[r])if(1===t.nodeType||o)if(a=t[f]||(t[f]={}),l=a[t.uniqueID]||(a[t.uniqueID]={}),u&&u===t.nodeName.toLowerCase())t=t[r]||t;else{if((c=l[e])&&c[0]===v&&c[1]===s)return y[2]=c[2];if(l[e]=y,y[2]=n(t,i,h))return!0}return!1}}function ui(n){return n.length>1?function(t,i,r){for(var u=n.length;u--;)if(!n[u](t,i,r))return!1;return!0}:n[0]}function yr(n,t,i){for(var r=0,f=t.length;r<f;r++)u(n,t[r],i);return i}function wt(n,t,i,r,u){for(var e,o=[],f=0,s=n.length,h=null!=t;f<s;f++)(e=n[f])&&(i&&!i(e,r,u)||(o.push(e),h&&t.push(f)));return o}function fi(n,t,i,r,u,e){return r&&!r[f]&&(r=fi(r)),u&&!u[f]&&(u=fi(u,e)),l(function(f,e,o,s){var l,c,a,p=[],y=[],w=e.length,b=f||yr(t||"*",o.nodeType?[o]:o,[]),v=!n||!f&&t?b:wt(b,p,n,o,s),h=i?u||(f?n:w||r)?[]:e:v;if(i&&i(v,h,o,s),r)for(l=wt(h,y),r(l,[],o,s),c=l.length;c--;)(a=l[c])&&(h[y[c]]=!(v[y[c]]=a));if(f){if(u||n){if(u){for(l=[],c=h.length;c--;)(a=h[c])&&l.push(v[c]=a);u(null,h=[],l,s)}for(c=h.length;c--;)(a=h[c])&&(l=u?nt(f,a):p[c])>-1&&(f[l]=!(e[l]=a))}}else h=wt(h===e?h.splice(w,h.length):h),u?u(null,e,h,s):k.apply(e,h)})}function ei(n){for(var o,u,r,s=n.length,h=t.relative[n[0].type],c=h||t.relative[" "],i=h?1:0,l=pt(function(n){return n===o},c,!0),a=pt(function(n){return nt(o,n)>-1},c,!0),e=[function(n,t,i){var r=!h&&(i||t!==ht)||((o=t).nodeType?l(n,t,i):a(n,t,i));return o=null,r}];i<s;i++)if(u=t.relative[n[i].type])e=[pt(ui(e),u)];else{if(u=t.filter[n[i].type].apply(null,n[i].matches),u[f]){for(r=++i;r<s;r++)if(t.relative[n[r].type])break;return fi(i>1&&ui(e),i>1&&yt(n.slice(0,i-1).concat({value:" "===n[i-2].type?"*":""})).replace(at,"$1"),u,i<r&&ei(n.slice(i,r)),r<s&&ei(n=n.slice(r)),r<s&&yt(n))}e.push(u)}return ui(e)}function pr(n,r){var f=r.length>0,e=n.length>0,o=function(o,s,c,l,a){var y,nt,d,g=0,p="0",tt=o&&[],w=[],it=ht,rt=o||e&&t.find.TAG("*",a),ut=v+=null==it?1:Math.random()||.1,ft=rt.length;for(a&&(ht=s===i||s||a);p!==ft&&null!=(y=rt[p]);p++){if(e&&y){for(nt=0,s||y.ownerDocument===i||(b(y),c=!h);d=n[nt++];)if(d(y,s||i,c)){l.push(y);break}a&&(v=ut)}f&&((y=!d&&y)&&g--,o&&tt.push(y))}if(g+=p,f&&p!==g){for(nt=0;d=r[nt++];)d(tt,w,s,c);if(o){if(g>0)while(p--)tt[p]||w[p]||(w[p]=nr.call(l));w=wt(w)}k.apply(l,w);a&&!o&&w.length>0&&g+r.length>1&&u.uniqueSort(l)}return a&&(v=ut,ht=it),tt};return f?l(o):o}var rt,e,t,st,oi,ft,bt,si,ht,w,ut,b,i,s,h,o,d,ct,et,f="sizzle"+1*new Date,c=n.document,v=0,di=0,hi=ti(),ci=ti(),lt=ti(),kt=function(n,t){return n===t&&(ut=!0),0},gi={}.hasOwnProperty,g=[],nr=g.pop,tr=g.push,k=g.push,li=g.slice,nt=function(n,t){for(var i=0,r=n.length;i<r;i++)if(n[i]===t)return i;return-1},dt="checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",r="[\\x20\\t\\r\\n\\f]",tt="(?:\\\\.|[\\w-]|[^\0-\\xa0])+",ai="\\["+r+"*("+tt+")(?:"+r+"*([*^$|!~]?=)"+r+"*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|("+tt+"))|)"+r+"*\\]",gt=":("+tt+")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|"+ai+")*)|.*)\\)|)",ir=new RegExp(r+"+","g"),at=new RegExp("^"+r+"+|((?:^|[^\\\\])(?:\\\\.)*)"+r+"+$","g"),rr=new RegExp("^"+r+"*,"+r+"*"),ur=new RegExp("^"+r+"*([>+~]|"+r+")"+r+"*"),fr=new RegExp("="+r+"*([^\\]'\"]*?)"+r+"*\\]","g"),er=new RegExp(gt),or=new RegExp("^"+tt+"$"),vt={ID:new RegExp("^#("+tt+")"),CLASS:new RegExp("^\\.("+tt+")"),TAG:new RegExp("^("+tt+"|[*])"),ATTR:new RegExp("^"+ai),PSEUDO:new RegExp("^"+gt),CHILD:new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\("+r+"*(even|odd|(([+-]|)(\\d*)n|)"+r+"*(?:([+-]|)"+r+"*(\\d+)|))"+r+"*\\)|)","i"),bool:new RegExp("^(?:"+dt+")$","i"),needsContext:new RegExp("^"+r+"*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\("+r+"*((?:-\\d)?\\d*)"+r+"*\\)|)(?=[^-]|$)","i")},sr=/^(?:input|select|textarea|button)$/i,hr=/^h\d$/i,ot=/^[^{]+\{\s*\[native \w/,cr=/^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,ni=/[+~]/,y=new RegExp("\\\\([\\da-f]{1,6}"+r+"?|("+r+")|.)","ig"),p=function(n,t,i){var r="0x"+t-65536;return r!==r||i?t:r<0?String.fromCharCode(r+65536):String.fromCharCode(r>>10|55296,1023&r|56320)},vi=/([\0-\x1f\x7f]|^-?\d)|^-$|[^\0-\x1f\x7f-\uFFFF\w-]/g,yi=function(n,t){return t?"\0"===n?"�":n.slice(0,-1)+"\\"+n.charCodeAt(n.length-1).toString(16)+" ":"\\"+n},pi=function(){b()},lr=pt(function(n){return n.disabled===!0&&("form"in n||"label"in n)},{dir:"parentNode",next:"legend"});try{k.apply(g=li.call(c.childNodes),c.childNodes);g[c.childNodes.length].nodeType}catch(wr){k={apply:g.length?function(n,t){tr.apply(n,li.call(t))}:function(n,t){for(var i=n.length,r=0;n[i++]=t[r++];);n.length=i-1}}}e=u.support={};oi=u.isXML=function(n){var t=n&&(n.ownerDocument||n).documentElement;return!!t&&"HTML"!==t.nodeName};b=u.setDocument=function(n){var v,u,l=n?n.ownerDocument||n:c;return l!==i&&9===l.nodeType&&l.documentElement?(i=l,s=i.documentElement,h=!oi(i),c!==i&&(u=i.defaultView)&&u.top!==u&&(u.addEventListener?u.addEventListener("unload",pi,!1):u.attachEvent&&u.attachEvent("onunload",pi)),e.attributes=a(function(n){return n.className="i",!n.getAttribute("className")}),e.getElementsByTagName=a(function(n){return n.appendChild(i.createComment("")),!n.getElementsByTagName("*").length}),e.getElementsByClassName=ot.test(i.getElementsByClassName),e.getById=a(function(n){return s.appendChild(n).id=f,!i.getElementsByName||!i.getElementsByName(f).length}),e.getById?(t.filter.ID=function(n){var t=n.replace(y,p);return function(n){return n.getAttribute("id")===t}},t.find.ID=function(n,t){if("undefined"!=typeof t.getElementById&&h){var i=t.getElementById(n);return i?[i]:[]}}):(t.filter.ID=function(n){var t=n.replace(y,p);return function(n){var i="undefined"!=typeof n.getAttributeNode&&n.getAttributeNode("id");return i&&i.value===t}},t.find.ID=function(n,t){if("undefined"!=typeof t.getElementById&&h){var i,u,f,r=t.getElementById(n);if(r){if(i=r.getAttributeNode("id"),i&&i.value===n)return[r];for(f=t.getElementsByName(n),u=0;r=f[u++];)if(i=r.getAttributeNode("id"),i&&i.value===n)return[r]}return[]}}),t.find.TAG=e.getElementsByTagName?function(n,t){return"undefined"!=typeof t.getElementsByTagName?t.getElementsByTagName(n):e.qsa?t.querySelectorAll(n):void 0}:function(n,t){var i,r=[],f=0,u=t.getElementsByTagName(n);if("*"===n){while(i=u[f++])1===i.nodeType&&r.push(i);return r}return u},t.find.CLASS=e.getElementsByClassName&&function(n,t){if("undefined"!=typeof t.getElementsByClassName&&h)return t.getElementsByClassName(n)},d=[],o=[],(e.qsa=ot.test(i.querySelectorAll))&&(a(function(n){s.appendChild(n).innerHTML="<a id='"+f+"'><\/a><select id='"+f+"-\r\\' msallowcapture=''><option selected=''><\/option><\/select>";n.querySelectorAll("[msallowcapture^='']").length&&o.push("[*^$]="+r+"*(?:''|\"\")");n.querySelectorAll("[selected]").length||o.push("\\["+r+"*(?:value|"+dt+")");n.querySelectorAll("[id~="+f+"-]").length||o.push("~=");n.querySelectorAll(":checked").length||o.push(":checked");n.querySelectorAll("a#"+f+"+*").length||o.push(".#.+[+~]")}),a(function(n){n.innerHTML="<a href='' disabled='disabled'><\/a><select disabled='disabled'><option/><\/select>";var t=i.createElement("input");t.setAttribute("type","hidden");n.appendChild(t).setAttribute("name","D");n.querySelectorAll("[name=d]").length&&o.push("name"+r+"*[*^$|!~]?=");2!==n.querySelectorAll(":enabled").length&&o.push(":enabled",":disabled");s.appendChild(n).disabled=!0;2!==n.querySelectorAll(":disabled").length&&o.push(":enabled",":disabled");n.querySelectorAll("*,:x");o.push(",.*:")})),(e.matchesSelector=ot.test(ct=s.matches||s.webkitMatchesSelector||s.mozMatchesSelector||s.oMatchesSelector||s.msMatchesSelector))&&a(function(n){e.disconnectedMatch=ct.call(n,"*");ct.call(n,"[s!='']:x");d.push("!=",gt)}),o=o.length&&new RegExp(o.join("|")),d=d.length&&new RegExp(d.join("|")),v=ot.test(s.compareDocumentPosition),et=v||ot.test(s.contains)?function(n,t){var r=9===n.nodeType?n.documentElement:n,i=t&&t.parentNode;return n===i||!(!i||1!==i.nodeType||!(r.contains?r.contains(i):n.compareDocumentPosition&&16&n.compareDocumentPosition(i)))}:function(n,t){if(t)while(t=t.parentNode)if(t===n)return!0;return!1},kt=v?function(n,t){if(n===t)return ut=!0,0;var r=!n.compareDocumentPosition-!t.compareDocumentPosition;return r?r:(r=(n.ownerDocument||n)===(t.ownerDocument||t)?n.compareDocumentPosition(t):1,1&r||!e.sortDetached&&t.compareDocumentPosition(n)===r?n===i||n.ownerDocument===c&&et(c,n)?-1:t===i||t.ownerDocument===c&&et(c,t)?1:w?nt(w,n)-nt(w,t):0:4&r?-1:1)}:function(n,t){if(n===t)return ut=!0,0;var r,u=0,o=n.parentNode,s=t.parentNode,f=[n],e=[t];if(!o||!s)return n===i?-1:t===i?1:o?-1:s?1:w?nt(w,n)-nt(w,t):0;if(o===s)return wi(n,t);for(r=n;r=r.parentNode;)f.unshift(r);for(r=t;r=r.parentNode;)e.unshift(r);while(f[u]===e[u])u++;return u?wi(f[u],e[u]):f[u]===c?-1:e[u]===c?1:0},i):i};u.matches=function(n,t){return u(n,null,null,t)};u.matchesSelector=function(n,t){if((n.ownerDocument||n)!==i&&b(n),t=t.replace(fr,"='$1']"),e.matchesSelector&&h&&!lt[t+" "]&&(!d||!d.test(t))&&(!o||!o.test(t)))try{var r=ct.call(n,t);if(r||e.disconnectedMatch||n.document&&11!==n.document.nodeType)return r}catch(f){}return u(t,i,null,[n]).length>0};u.contains=function(n,t){return(n.ownerDocument||n)!==i&&b(n),et(n,t)};u.attr=function(n,r){(n.ownerDocument||n)!==i&&b(n);var f=t.attrHandle[r.toLowerCase()],u=f&&gi.call(t.attrHandle,r.toLowerCase())?f(n,r,!h):void 0;return void 0!==u?u:e.attributes||!h?n.getAttribute(r):(u=n.getAttributeNode(r))&&u.specified?u.value:null};u.escape=function(n){return(n+"").replace(vi,yi)};u.error=function(n){throw new Error("Syntax error, unrecognized expression: "+n);};u.uniqueSort=function(n){var r,u=[],t=0,i=0;if(ut=!e.detectDuplicates,w=!e.sortStable&&n.slice(0),n.sort(kt),ut){while(r=n[i++])r===n[i]&&(t=u.push(i));while(t--)n.splice(u[t],1)}return w=null,n};st=u.getText=function(n){var r,i="",u=0,t=n.nodeType;if(t){if(1===t||9===t||11===t){if("string"==typeof n.textContent)return n.textContent;for(n=n.firstChild;n;n=n.nextSibling)i+=st(n)}else if(3===t||4===t)return n.nodeValue}else while(r=n[u++])i+=st(r);return i};t=u.selectors={cacheLength:50,createPseudo:l,match:vt,attrHandle:{},find:{},relative:{">":{dir:"parentNode",first:!0}," ":{dir:"parentNode"},"+":{dir:"previousSibling",first:!0},"~":{dir:"previousSibling"}},preFilter:{ATTR:function(n){return n[1]=n[1].replace(y,p),n[3]=(n[3]||n[4]||n[5]||"").replace(y,p),"~="===n[2]&&(n[3]=" "+n[3]+" "),n.slice(0,4)},CHILD:function(n){return n[1]=n[1].toLowerCase(),"nth"===n[1].slice(0,3)?(n[3]||u.error(n[0]),n[4]=+(n[4]?n[5]+(n[6]||1):2*("even"===n[3]||"odd"===n[3])),n[5]=+(n[7]+n[8]||"odd"===n[3])):n[3]&&u.error(n[0]),n},PSEUDO:function(n){var i,t=!n[6]&&n[2];return vt.CHILD.test(n[0])?null:(n[3]?n[2]=n[4]||n[5]||"":t&&er.test(t)&&(i=ft(t,!0))&&(i=t.indexOf(")",t.length-i)-t.length)&&(n[0]=n[0].slice(0,i),n[2]=t.slice(0,i)),n.slice(0,3))}},filter:{TAG:function(n){var t=n.replace(y,p).toLowerCase();return"*"===n?function(){return!0}:function(n){return n.nodeName&&n.nodeName.toLowerCase()===t}},CLASS:function(n){var t=hi[n+" "];return t||(t=new RegExp("(^|"+r+")"+n+"("+r+"|$)"))&&hi(n,function(n){return t.test("string"==typeof n.className&&n.className||"undefined"!=typeof n.getAttribute&&n.getAttribute("class")||"")})},ATTR:function(n,t,i){return function(r){var f=u.attr(r,n);return null==f?"!="===t:!t||(f+="","="===t?f===i:"!="===t?f!==i:"^="===t?i&&0===f.indexOf(i):"*="===t?i&&f.indexOf(i)>-1:"$="===t?i&&f.slice(-i.length)===i:"~="===t?(" "+f.replace(ir," ")+" ").indexOf(i)>-1:"|="===t&&(f===i||f.slice(0,i.length+1)===i+"-"))}},CHILD:function(n,t,i,r,u){var s="nth"!==n.slice(0,3),o="last"!==n.slice(-4),e="of-type"===t;return 1===r&&0===u?function(n){return!!n.parentNode}:function(t,i,h){var p,w,y,c,a,b,k=s!==o?"nextSibling":"previousSibling",d=t.parentNode,nt=e&&t.nodeName.toLowerCase(),g=!h&&!e,l=!1;if(d){if(s){while(k){for(c=t;c=c[k];)if(e?c.nodeName.toLowerCase()===nt:1===c.nodeType)return!1;b=k="only"===n&&!b&&"nextSibling"}return!0}if(b=[o?d.firstChild:d.lastChild],o&&g){for(c=d,y=c[f]||(c[f]={}),w=y[c.uniqueID]||(y[c.uniqueID]={}),p=w[n]||[],a=p[0]===v&&p[1],l=a&&p[2],c=a&&d.childNodes[a];c=++a&&c&&c[k]||(l=a=0)||b.pop();)if(1===c.nodeType&&++l&&c===t){w[n]=[v,a,l];break}}else if(g&&(c=t,y=c[f]||(c[f]={}),w=y[c.uniqueID]||(y[c.uniqueID]={}),p=w[n]||[],a=p[0]===v&&p[1],l=a),l===!1)while(c=++a&&c&&c[k]||(l=a=0)||b.pop())if((e?c.nodeName.toLowerCase()===nt:1===c.nodeType)&&++l&&(g&&(y=c[f]||(c[f]={}),w=y[c.uniqueID]||(y[c.uniqueID]={}),w[n]=[v,l]),c===t))break;return l-=u,l===r||l%r==0&&l/r>=0}}},PSEUDO:function(n,i){var e,r=t.pseudos[n]||t.setFilters[n.toLowerCase()]||u.error("unsupported pseudo: "+n);return r[f]?r(i):r.length>1?(e=[n,n,"",i],t.setFilters.hasOwnProperty(n.toLowerCase())?l(function(n,t){for(var u,f=r(n,i),e=f.length;e--;)u=nt(n,f[e]),n[u]=!(t[u]=f[e])}):function(n){return r(n,0,e)}):r}},pseudos:{not:l(function(n){var t=[],r=[],i=bt(n.replace(at,"$1"));return i[f]?l(function(n,t,r,u){for(var e,o=i(n,null,u,[]),f=n.length;f--;)(e=o[f])&&(n[f]=!(t[f]=e))}):function(n,u,f){return t[0]=n,i(t,null,f,r),t[0]=null,!r.pop()}}),has:l(function(n){return function(t){return u(n,t).length>0}}),contains:l(function(n){return n=n.replace(y,p),function(t){return(t.textContent||t.innerText||st(t)).indexOf(n)>-1}}),lang:l(function(n){return or.test(n||"")||u.error("unsupported lang: "+n),n=n.replace(y,p).toLowerCase(),function(t){var i;do if(i=h?t.lang:t.getAttribute("xml:lang")||t.getAttribute("lang"))return i=i.toLowerCase(),i===n||0===i.indexOf(n+"-");while((t=t.parentNode)&&1===t.nodeType);return!1}}),target:function(t){var i=n.location&&n.location.hash;return i&&i.slice(1)===t.id},root:function(n){return n===s},focus:function(n){return n===i.activeElement&&(!i.hasFocus||i.hasFocus())&&!!(n.type||n.href||~n.tabIndex)},enabled:bi(!1),disabled:bi(!0),checked:function(n){var t=n.nodeName.toLowerCase();return"input"===t&&!!n.checked||"option"===t&&!!n.selected},selected:function(n){return n.parentNode&&n.parentNode.selectedIndex,n.selected===!0},empty:function(n){for(n=n.firstChild;n;n=n.nextSibling)if(n.nodeType<6)return!1;return!0},parent:function(n){return!t.pseudos.empty(n)},header:function(n){return hr.test(n.nodeName)},input:function(n){return sr.test(n.nodeName)},button:function(n){var t=n.nodeName.toLowerCase();return"input"===t&&"button"===n.type||"button"===t},text:function(n){var t;return"input"===n.nodeName.toLowerCase()&&"text"===n.type&&(null==(t=n.getAttribute("type"))||"text"===t.toLowerCase())},first:it(function(){return[0]}),last:it(function(n,t){return[t-1]}),eq:it(function(n,t,i){return[i<0?i+t:i]}),even:it(function(n,t){for(var i=0;i<t;i+=2)n.push(i);return n}),odd:it(function(n,t){for(var i=1;i<t;i+=2)n.push(i);return n}),lt:it(function(n,t,i){for(var r=i<0?i+t:i;--r>=0;)n.push(r);return n}),gt:it(function(n,t,i){for(var r=i<0?i+t:i;++r<t;)n.push(r);return n})}};t.pseudos.nth=t.pseudos.eq;for(rt in{radio:!0,checkbox:!0,file:!0,password:!0,image:!0})t.pseudos[rt]=ar(rt);for(rt in{submit:!0,reset:!0})t.pseudos[rt]=vr(rt);return ki.prototype=t.filters=t.pseudos,t.setFilters=new ki,ft=u.tokenize=function(n,i){var e,f,s,o,r,h,c,l=ci[n+" "];if(l)return i?0:l.slice(0);for(r=n,h=[],c=t.preFilter;r;){(!e||(f=rr.exec(r)))&&(f&&(r=r.slice(f[0].length)||r),h.push(s=[]));e=!1;(f=ur.exec(r))&&(e=f.shift(),s.push({value:e,type:f[0].replace(at," ")}),r=r.slice(e.length));for(o in t.filter)(f=vt[o].exec(r))&&(!c[o]||(f=c[o](f)))&&(e=f.shift(),s.push({value:e,type:o,matches:f}),r=r.slice(e.length));if(!e)break}return i?r.length:r?u.error(n):ci(n,h).slice(0)},bt=u.compile=function(n,t){var r,u=[],e=[],i=lt[n+" "];if(!i){for(t||(t=ft(n)),r=t.length;r--;)i=ei(t[r]),i[f]?u.push(i):e.push(i);i=lt(n,pr(e,u));i.selector=n}return i},si=u.select=function(n,i,r,u){var o,f,e,l,a,c="function"==typeof n&&n,s=!u&&ft(n=c.selector||n);if(r=r||[],1===s.length){if(f=s[0]=s[0].slice(0),f.length>2&&"ID"===(e=f[0]).type&&9===i.nodeType&&h&&t.relative[f[1].type]){if(i=(t.find.ID(e.matches[0].replace(y,p),i)||[])[0],!i)return r;c&&(i=i.parentNode);n=n.slice(f.shift().value.length)}for(o=vt.needsContext.test(n)?0:f.length;o--;){if(e=f[o],t.relative[l=e.type])break;if((a=t.find[l])&&(u=a(e.matches[0].replace(y,p),ni.test(f[0].type)&&ri(i.parentNode)||i))){if(f.splice(o,1),n=u.length&&yt(f),!n)return k.apply(r,u),r;break}}}return(c||bt(n,s))(u,i,!h,r,!i||ni.test(n)&&ri(i.parentNode)||i),r},e.sortStable=f.split("").sort(kt).join("")===f,e.detectDuplicates=!!ut,b(),e.sortDetached=a(function(n){return 1&n.compareDocumentPosition(i.createElement("fieldset"))}),a(function(n){return n.innerHTML="<a href='#'><\/a>","#"===n.firstChild.getAttribute("href")})||ii("type|href|height|width",function(n,t,i){if(!i)return n.getAttribute(t,"type"===t.toLowerCase()?1:2)}),e.attributes&&a(function(n){return n.innerHTML="<input/>",n.firstChild.setAttribute("value",""),""===n.firstChild.getAttribute("value")})||ii("value",function(n,t,i){if(!i&&"input"===n.nodeName.toLowerCase())return n.defaultValue}),a(function(n){return null==n.getAttribute("disabled")})||ii(dt,function(n,t,i){var r;if(!i)return n[t]===!0?t.toLowerCase():(r=n.getAttributeNode(t))&&r.specified?r.value:null}),u}(n);i.find=y;i.expr=y.selectors;i.expr[":"]=i.expr.pseudos;i.uniqueSort=i.unique=y.uniqueSort;i.text=y.getText;i.isXMLDoc=y.isXML;i.contains=y.contains;i.escapeSelector=y.escape;var g=function(n,t,r){for(var u=[],f=void 0!==r;(n=n[t])&&9!==n.nodeType;)if(1===n.nodeType){if(f&&i(n).is(r))break;u.push(n)}return u},ur=function(n,t){for(var i=[];n;n=n.nextSibling)1===n.nodeType&&n!==t&&i.push(n);return i},fr=i.expr.match.needsContext;ei=/^<([a-z][^\/\0>:\x20\t\r\n\f]*)[\x20\t\r\n\f]*\/?>(?:<\/\1>|)$/i;er=/^.[^:#\[\.,]*$/;i.filter=function(n,t,r){var u=t[0];return r&&(n=":not("+n+")"),1===t.length&&1===u.nodeType?i.find.matchesSelector(u,n)?[u]:[]:i.find.matches(n,i.grep(t,function(n){return 1===n.nodeType}))};i.fn.extend({find:function(n){var t,r,u=this.length,f=this;if("string"!=typeof n)return this.pushStack(i(n).filter(function(){for(t=0;t<u;t++)if(i.contains(f[t],this))return!0}));for(r=this.pushStack([]),t=0;t<u;t++)i.find(n,f[t],r);return u>1?i.uniqueSort(r):r},filter:function(n){return this.pushStack(oi(this,n||[],!1))},not:function(n){return this.pushStack(oi(this,n||[],!0))},is:function(n){return!!oi(this,"string"==typeof n&&fr.test(n)?i(n):n||[],!1).length}});sr=/^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]+))$/;hr=i.fn.init=function(n,t,r){var f,e;if(!n)return this;if(r=r||or,"string"==typeof n){if(f="<"===n[0]&&">"===n[n.length-1]&&n.length>=3?[null,n,null]:sr.exec(n),!f||!f[1]&&t)return!t||t.jquery?(t||r).find(n):this.constructor(t).find(n);if(f[1]){if(t=t instanceof i?t[0]:t,i.merge(this,i.parseHTML(f[1],t&&t.nodeType?t.ownerDocument||t:u,!0)),ei.test(f[1])&&i.isPlainObject(t))for(f in t)i.isFunction(this[f])?this[f](t[f]):this.attr(f,t[f]);return this}return e=u.getElementById(f[2]),e&&(this[0]=e,this.length=1),this}return n.nodeType?(this[0]=n,this.length=1,this):i.isFunction(n)?void 0!==r.ready?r.ready(n):n(i):i.makeArray(n,this)};hr.prototype=i.fn;or=i(u);cr=/^(?:parents|prev(?:Until|All))/;lr={children:!0,contents:!0,next:!0,prev:!0};i.fn.extend({has:function(n){var t=i(n,this),r=t.length;return this.filter(function(){for(var n=0;n<r;n++)if(i.contains(this,t[n]))return!0})},closest:function(n,t){var r,f=0,o=this.length,u=[],e="string"!=typeof n&&i(n);if(!fr.test(n))for(;f<o;f++)for(r=this[f];r&&r!==t;r=r.parentNode)if(r.nodeType<11&&(e?e.index(r)>-1:1===r.nodeType&&i.find.matchesSelector(r,n))){u.push(r);break}return this.pushStack(u.length>1?i.uniqueSort(u):u)},index:function(n){return n?"string"==typeof n?ot.call(i(n),this[0]):ot.call(this,n.jquery?n[0]:n):this[0]&&this[0].parentNode?this.first().prevAll().length:-1},add:function(n,t){return this.pushStack(i.uniqueSort(i.merge(this.get(),i(n,t))))},addBack:function(n){return this.add(null==n?this.prevObject:this.prevObject.filter(n))}});i.each({parent:function(n){var t=n.parentNode;return t&&11!==t.nodeType?t:null},parents:function(n){return g(n,"parentNode")},parentsUntil:function(n,t,i){return g(n,"parentNode",i)},next:function(n){return ar(n,"nextSibling")},prev:function(n){return ar(n,"previousSibling")},nextAll:function(n){return g(n,"nextSibling")},prevAll:function(n){return g(n,"previousSibling")},nextUntil:function(n,t,i){return g(n,"nextSibling",i)},prevUntil:function(n,t,i){return g(n,"previousSibling",i)},siblings:function(n){return ur((n.parentNode||{}).firstChild,n)},children:function(n){return ur(n.firstChild)},contents:function(n){return l(n,"iframe")?n.contentDocument:(l(n,"template")&&(n=n.content||n),i.merge([],n.childNodes))}},function(n,t){i.fn[n]=function(r,u){var f=i.map(this,t,r);return"Until"!==n.slice(-5)&&(u=r),u&&"string"==typeof u&&(f=i.filter(u,f)),this.length>1&&(lr[n]||i.uniqueSort(f),cr.test(n)&&f.reverse()),this.pushStack(f)}});h=/[^\x20\t\r\n\f]+/g;i.Callbacks=function(n){n="string"==typeof n?ne(n):i.extend({},n);var e,r,h,u,t=[],o=[],f=-1,c=function(){for(u=u||n.once,h=e=!0;o.length;f=-1)for(r=o.shift();++f<t.length;)t[f].apply(r[0],r[1])===!1&&n.stopOnFalse&&(f=t.length,r=!1);n.memory||(r=!1);e=!1;u&&(t=r?[]:"")},s={add:function(){return t&&(r&&!e&&(f=t.length-1,o.push(r)),function u(r){i.each(r,function(r,f){i.isFunction(f)?n.unique&&s.has(f)||t.push(f):f&&f.length&&"string"!==i.type(f)&&u(f)})}(arguments),r&&!e&&c()),this},remove:function(){return i.each(arguments,function(n,r){for(var u;(u=i.inArray(r,t,u))>-1;)t.splice(u,1),u<=f&&f--}),this},has:function(n){return n?i.inArray(n,t)>-1:t.length>0},empty:function(){return t&&(t=[]),this},disable:function(){return u=o=[],t=r="",this},disabled:function(){return!t},lock:function(){return u=o=[],r||e||(t=r=""),this},locked:function(){return!!u},fireWith:function(n,t){return u||(t=t||[],t=[n,t.slice?t.slice():t],o.push(t),e||c()),this},fire:function(){return s.fireWith(this,arguments),this},fired:function(){return!!h}};return s};i.extend({Deferred:function(t){var u=[["notify","progress",i.Callbacks("memory"),i.Callbacks("memory"),2],["resolve","done",i.Callbacks("once memory"),i.Callbacks("once memory"),0,"resolved"],["reject","fail",i.Callbacks("once memory"),i.Callbacks("once memory"),1,"rejected"]],e="pending",f={state:function(){return e},always:function(){return r.done(arguments).fail(arguments),this},"catch":function(n){return f.then(null,n)},pipe:function(){var n=arguments;return i.Deferred(function(t){i.each(u,function(u,f){var e=i.isFunction(n[f[4]])&&n[f[4]];r[f[1]](function(){var n=e&&e.apply(this,arguments);n&&i.isFunction(n.promise)?n.promise().progress(t.notify).done(t.resolve).fail(t.reject):t[f[0]+"With"](this,e?[n]:arguments)})});n=null}).promise()},then:function(t,r,f){function o(t,r,u,f){return function(){var s=this,h=arguments,l=function(){var n,c;if(!(t<e)){if(n=u.apply(s,h),n===r.promise())throw new TypeError("Thenable self-resolution");c=n&&("object"==typeof n||"function"==typeof n)&&n.then;i.isFunction(c)?f?c.call(n,o(e,r,nt,f),o(e,r,pt,f)):(e++,c.call(n,o(e,r,nt,f),o(e,r,pt,f),o(e,r,nt,r.notifyWith))):(u!==nt&&(s=void 0,h=[n]),(f||r.resolveWith)(s,h))}},c=f?l:function(){try{l()}catch(n){i.Deferred.exceptionHook&&i.Deferred.exceptionHook(n,c.stackTrace);t+1>=e&&(u!==pt&&(s=void 0,h=[n]),r.rejectWith(s,h))}};t?c():(i.Deferred.getStackHook&&(c.stackTrace=i.Deferred.getStackHook()),n.setTimeout(c))}}var e=0;return i.Deferred(function(n){u[0][3].add(o(0,n,i.isFunction(f)?f:nt,n.notifyWith));u[1][3].add(o(0,n,i.isFunction(t)?t:nt));u[2][3].add(o(0,n,i.isFunction(r)?r:pt))}).promise()},promise:function(n){return null!=n?i.extend(n,f):f}},r={};return i.each(u,function(n,t){var i=t[2],o=t[5];f[t[1]]=i.add;o&&i.add(function(){e=o},u[3-n][2].disable,u[0][2].lock);i.add(t[3].fire);r[t[0]]=function(){return r[t[0]+"With"](this===r?void 0:this,arguments),this};r[t[0]+"With"]=i.fireWith}),f.promise(r),t&&t.call(r,r),r},when:function(n){var f=arguments.length,t=f,e=Array(t),u=w.call(arguments),r=i.Deferred(),o=function(n){return function(t){e[n]=this;u[n]=arguments.length>1?w.call(arguments):t;--f||r.resolveWith(e,u)}};if(f<=1&&(vr(n,r.done(o(t)).resolve,r.reject,!f),"pending"===r.state()||i.isFunction(u[t]&&u[t].then)))return r.then();while(t--)vr(u[t],o(t),r.reject);return r.promise()}});yr=/^(Eval|Internal|Range|Reference|Syntax|Type|URI)Error$/;i.Deferred.exceptionHook=function(t,i){n.console&&n.console.warn&&t&&yr.test(t.name)&&n.console.warn("jQuery.Deferred exception: "+t.message,t.stack,i)};i.readyException=function(t){n.setTimeout(function(){throw t;})};wt=i.Deferred();i.fn.ready=function(n){return wt.then(n)["catch"](function(n){i.readyException(n)}),this};i.extend({isReady:!1,readyWait:1,ready:function(n){(n===!0?--i.readyWait:i.isReady)||(i.isReady=!0,n!==!0&&--i.readyWait>0||wt.resolveWith(u,[i]))}});i.ready.then=wt.then;"complete"===u.readyState||"loading"!==u.readyState&&!u.documentElement.doScroll?n.setTimeout(i.ready):(u.addEventListener("DOMContentLoaded",bt),n.addEventListener("load",bt));v=function(n,t,r,u,f,e,o){var s=0,c=n.length,h=null==r;if("object"===i.type(r)){f=!0;for(s in r)v(n,t,s,r[s],!0,e,o)}else if(void 0!==u&&(f=!0,i.isFunction(u)||(o=!0),h&&(o?(t.call(n,u),t=null):(h=t,t=function(n,t,r){return h.call(i(n),r)})),t))for(;s<c;s++)t(n[s],r,o?u:u.call(n[s],s,t(n[s],r)));return f?n:h?t.call(n):c?t(n[0],r):e};st=function(n){return 1===n.nodeType||9===n.nodeType||!+n.nodeType};ht.uid=1;ht.prototype={cache:function(n){var t=n[this.expando];return t||(t={},st(n)&&(n.nodeType?n[this.expando]=t:Object.defineProperty(n,this.expando,{value:t,configurable:!0}))),t},set:function(n,t,r){var u,f=this.cache(n);if("string"==typeof t)f[i.camelCase(t)]=r;else for(u in t)f[i.camelCase(u)]=t[u];return f},get:function(n,t){return void 0===t?this.cache(n):n[this.expando]&&n[this.expando][i.camelCase(t)]},access:function(n,t,i){return void 0===t||t&&"string"==typeof t&&void 0===i?this.get(n,t):(this.set(n,t,i),void 0!==i?i:t)},remove:function(n,t){var u,r=n[this.expando];if(void 0!==r){if(void 0!==t)for(Array.isArray(t)?t=t.map(i.camelCase):(t=i.camelCase(t),t=(t in r)?[t]:t.match(h)||[]),u=t.length;u--;)delete r[t[u]];(void 0===t||i.isEmptyObject(r))&&(n.nodeType?n[this.expando]=void 0:delete n[this.expando])}},hasData:function(n){var t=n[this.expando];return void 0!==t&&!i.isEmptyObject(t)}};var r=new ht,e=new ht,te=/^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,ie=/[A-Z]/g;i.extend({hasData:function(n){return e.hasData(n)||r.hasData(n)},data:function(n,t,i){return e.access(n,t,i)},removeData:function(n,t){e.remove(n,t)},_data:function(n,t,i){return r.access(n,t,i)},_removeData:function(n,t){r.remove(n,t)}});i.fn.extend({data:function(n,t){var o,f,s,u=this[0],h=u&&u.attributes;if(void 0===n){if(this.length&&(s=e.get(u),1===u.nodeType&&!r.get(u,"hasDataAttrs"))){for(o=h.length;o--;)h[o]&&(f=h[o].name,0===f.indexOf("data-")&&(f=i.camelCase(f.slice(5)),pr(u,f,s[f])));r.set(u,"hasDataAttrs",!0)}return s}return"object"==typeof n?this.each(function(){e.set(this,n)}):v(this,function(t){var i;if(u&&void 0===t){if((i=e.get(u,n),void 0!==i)||(i=pr(u,n),void 0!==i))return i}else this.each(function(){e.set(this,n,t)})},null,t,arguments.length>1,null,!0)},removeData:function(n){return this.each(function(){e.remove(this,n)})}});i.extend({queue:function(n,t,u){var f;if(n)return t=(t||"fx")+"queue",f=r.get(n,t),u&&(!f||Array.isArray(u)?f=r.access(n,t,i.makeArray(u)):f.push(u)),f||[]},dequeue:function(n,t){t=t||"fx";var r=i.queue(n,t),e=r.length,u=r.shift(),f=i._queueHooks(n,t),o=function(){i.dequeue(n,t)};"inprogress"===u&&(u=r.shift(),e--);u&&("fx"===t&&r.unshift("inprogress"),delete f.stop,u.call(n,o,f));!e&&f&&f.empty.fire()},_queueHooks:function(n,t){var u=t+"queueHooks";return r.get(n,u)||r.access(n,u,{empty:i.Callbacks("once memory").add(function(){r.remove(n,[t+"queue",u])})})}});i.fn.extend({queue:function(n,t){var r=2;return"string"!=typeof n&&(t=n,n="fx",r--),arguments.length<r?i.queue(this[0],n):void 0===t?this:this.each(function(){var r=i.queue(this,n,t);i._queueHooks(this,n);"fx"===n&&"inprogress"!==r[0]&&i.dequeue(this,n)})},dequeue:function(n){return this.each(function(){i.dequeue(this,n)})},clearQueue:function(n){return this.queue(n||"fx",[])},promise:function(n,t){var u,e=1,o=i.Deferred(),f=this,s=this.length,h=function(){--e||o.resolveWith(f,[f])};for("string"!=typeof n&&(t=n,n=void 0),n=n||"fx";s--;)u=r.get(f[s],n+"queueHooks"),u&&u.empty&&(e++,u.empty.add(h));return h(),o.promise(t)}});var wr=/[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,ct=new RegExp("^(?:([+-])=|)("+wr+")([a-z%]*)$","i"),b=["Top","Right","Bottom","Left"],kt=function(n,t){return n=t||n,"none"===n.style.display||""===n.style.display&&i.contains(n.ownerDocument,n)&&"none"===i.css(n,"display")},br=function(n,t,i,r){var f,u,e={};for(u in t)e[u]=n.style[u],n.style[u]=t[u];f=i.apply(n,r||[]);for(u in t)n.style[u]=e[u];return f};si={};i.fn.extend({show:function(){return tt(this,!0)},hide:function(){return tt(this)},toggle:function(n){return"boolean"==typeof n?n?this.show():this.hide():this.each(function(){kt(this)?i(this).show():i(this).hide()})}});var dr=/^(?:checkbox|radio)$/i,gr=/<([a-z][^\/\0>\x20\t\r\n\f]+)/i,nu=/^$|\/(?:java|ecma)script/i,c={option:[1,"<select multiple='multiple'>","<\/select>"],thead:[1,"<table>","<\/table>"],col:[2,"<table><colgroup>","<\/colgroup><\/table>"],tr:[2,"<table><tbody>","<\/tbody><\/table>"],td:[3,"<table><tbody><tr>","<\/tr><\/tbody><\/table>"],_default:[0,"",""]};c.optgroup=c.option;c.tbody=c.tfoot=c.colgroup=c.caption=c.thead;c.th=c.td;tu=/<|&#?\w+;/;!function(){var i=u.createDocumentFragment(),n=i.appendChild(u.createElement("div")),t=u.createElement("input");t.setAttribute("type","radio");t.setAttribute("checked","checked");t.setAttribute("name","t");n.appendChild(t);f.checkClone=n.cloneNode(!0).cloneNode(!0).lastChild.checked;n.innerHTML="<textarea>x<\/textarea>";f.noCloneChecked=!!n.cloneNode(!0).lastChild.defaultValue}();var dt=u.documentElement,fe=/^key/,ee=/^(?:mouse|pointer|contextmenu|drag|drop)|click/,ru=/^([^.]*)(?:\.(.+)|)/;i.event={global:{},add:function(n,t,u,f,e){var v,y,w,p,b,c,s,l,o,k,d,a=r.get(n);if(a)for(u.handler&&(v=u,u=v.handler,e=v.selector),e&&i.find.matchesSelector(dt,e),u.guid||(u.guid=i.guid++),(p=a.events)||(p=a.events={}),(y=a.handle)||(y=a.handle=function(t){if("undefined"!=typeof i&&i.event.triggered!==t.type)return i.event.dispatch.apply(n,arguments)}),t=(t||"").match(h)||[""],b=t.length;b--;)w=ru.exec(t[b])||[],o=d=w[1],k=(w[2]||"").split(".").sort(),o&&(s=i.event.special[o]||{},o=(e?s.delegateType:s.bindType)||o,s=i.event.special[o]||{},c=i.extend({type:o,origType:d,data:f,handler:u,guid:u.guid,selector:e,needsContext:e&&i.expr.match.needsContext.test(e),namespace:k.join(".")},v),(l=p[o])||(l=p[o]=[],l.delegateCount=0,s.setup&&s.setup.call(n,f,k,y)!==!1||n.addEventListener&&n.addEventListener(o,y)),s.add&&(s.add.call(n,c),c.handler.guid||(c.handler.guid=u.guid)),e?l.splice(l.delegateCount++,0,c):l.push(c),i.event.global[o]=!0)},remove:function(n,t,u,f,e){var y,k,c,v,p,s,l,a,o,b,d,w=r.hasData(n)&&r.get(n);if(w&&(v=w.events)){for(t=(t||"").match(h)||[""],p=t.length;p--;)if(c=ru.exec(t[p])||[],o=d=c[1],b=(c[2]||"").split(".").sort(),o){for(l=i.event.special[o]||{},o=(f?l.delegateType:l.bindType)||o,a=v[o]||[],c=c[2]&&new RegExp("(^|\\.)"+b.join("\\.(?:.*\\.|)")+"(\\.|$)"),k=y=a.length;y--;)s=a[y],!e&&d!==s.origType||u&&u.guid!==s.guid||c&&!c.test(s.namespace)||f&&f!==s.selector&&("**"!==f||!s.selector)||(a.splice(y,1),s.selector&&a.delegateCount--,l.remove&&l.remove.call(n,s));k&&!a.length&&(l.teardown&&l.teardown.call(n,b,w.handle)!==!1||i.removeEvent(n,o,w.handle),delete v[o])}else for(o in v)i.event.remove(n,o+t[p],u,f,!0);i.isEmptyObject(v)&&r.remove(n,"handle events")}},dispatch:function(n){var t=i.event.fix(n),u,c,s,e,f,l,h=new Array(arguments.length),a=(r.get(this,"events")||{})[t.type]||[],o=i.event.special[t.type]||{};for(h[0]=t,u=1;u<arguments.length;u++)h[u]=arguments[u];if(t.delegateTarget=this,!o.preDispatch||o.preDispatch.call(this,t)!==!1){for(l=i.event.handlers.call(this,t,a),u=0;(e=l[u++])&&!t.isPropagationStopped();)for(t.currentTarget=e.elem,c=0;(f=e.handlers[c++])&&!t.isImmediatePropagationStopped();)t.rnamespace&&!t.rnamespace.test(f.namespace)||(t.handleObj=f,t.data=f.data,s=((i.event.special[f.origType]||{}).handle||f.handler).apply(e.elem,h),void 0!==s&&(t.result=s)===!1&&(t.preventDefault(),t.stopPropagation()));return o.postDispatch&&o.postDispatch.call(this,t),t.result}},handlers:function(n,t){var f,e,u,o,s,c=[],h=t.delegateCount,r=n.target;if(h&&r.nodeType&&!("click"===n.type&&n.button>=1))for(;r!==this;r=r.parentNode||this)if(1===r.nodeType&&("click"!==n.type||r.disabled!==!0)){for(o=[],s={},f=0;f<h;f++)e=t[f],u=e.selector+" ",void 0===s[u]&&(s[u]=e.needsContext?i(u,this).index(r)>-1:i.find(u,this,null,[r]).length),s[u]&&o.push(e);o.length&&c.push({elem:r,handlers:o})}return r=this,h<t.length&&c.push({elem:r,handlers:t.slice(h)}),c},addProp:function(n,t){Object.defineProperty(i.Event.prototype,n,{enumerable:!0,configurable:!0,get:i.isFunction(t)?function(){if(this.originalEvent)return t(this.originalEvent)}:function(){if(this.originalEvent)return this.originalEvent[n]},set:function(t){Object.defineProperty(this,n,{enumerable:!0,configurable:!0,writable:!0,value:t})}})},fix:function(n){return n[i.expando]?n:new i.Event(n)},special:{load:{noBubble:!0},focus:{trigger:function(){if(this!==uu()&&this.focus)return this.focus(),!1},delegateType:"focusin"},blur:{trigger:function(){if(this===uu()&&this.blur)return this.blur(),!1},delegateType:"focusout"},click:{trigger:function(){if("checkbox"===this.type&&this.click&&l(this,"input"))return this.click(),!1},_default:function(n){return l(n.target,"a")}},beforeunload:{postDispatch:function(n){void 0!==n.result&&n.originalEvent&&(n.originalEvent.returnValue=n.result)}}}};i.removeEvent=function(n,t,i){n.removeEventListener&&n.removeEventListener(t,i)};i.Event=function(n,t){return this instanceof i.Event?(n&&n.type?(this.originalEvent=n,this.type=n.type,this.isDefaultPrevented=n.defaultPrevented||void 0===n.defaultPrevented&&n.returnValue===!1?gt:it,this.target=n.target&&3===n.target.nodeType?n.target.parentNode:n.target,this.currentTarget=n.currentTarget,this.relatedTarget=n.relatedTarget):this.type=n,t&&i.extend(this,t),this.timeStamp=n&&n.timeStamp||i.now(),void(this[i.expando]=!0)):new i.Event(n,t)};i.Event.prototype={constructor:i.Event,isDefaultPrevented:it,isPropagationStopped:it,isImmediatePropagationStopped:it,isSimulated:!1,preventDefault:function(){var n=this.originalEvent;this.isDefaultPrevented=gt;n&&!this.isSimulated&&n.preventDefault()},stopPropagation:function(){var n=this.originalEvent;this.isPropagationStopped=gt;n&&!this.isSimulated&&n.stopPropagation()},stopImmediatePropagation:function(){var n=this.originalEvent;this.isImmediatePropagationStopped=gt;n&&!this.isSimulated&&n.stopImmediatePropagation();this.stopPropagation()}};i.each({altKey:!0,bubbles:!0,cancelable:!0,changedTouches:!0,ctrlKey:!0,detail:!0,eventPhase:!0,metaKey:!0,pageX:!0,pageY:!0,shiftKey:!0,view:!0,char:!0,charCode:!0,key:!0,keyCode:!0,button:!0,buttons:!0,clientX:!0,clientY:!0,offsetX:!0,offsetY:!0,pointerId:!0,pointerType:!0,screenX:!0,screenY:!0,targetTouches:!0,toElement:!0,touches:!0,which:function(n){var t=n.button;return null==n.which&&fe.test(n.type)?null!=n.charCode?n.charCode:n.keyCode:!n.which&&void 0!==t&&ee.test(n.type)?1&t?1:2&t?3:4&t?2:0:n.which}},i.event.addProp);i.each({mouseenter:"mouseover",mouseleave:"mouseout",pointerenter:"pointerover",pointerleave:"pointerout"},function(n,t){i.event.special[n]={delegateType:t,bindType:t,handle:function(n){var u,f=this,r=n.relatedTarget,e=n.handleObj;return r&&(r===f||i.contains(f,r))||(n.type=e.origType,u=e.handler.apply(this,arguments),n.type=t),u}}});i.fn.extend({on:function(n,t,i,r){return ci(this,n,t,i,r)},one:function(n,t,i,r){return ci(this,n,t,i,r,1)},off:function(n,t,r){var u,f;if(n&&n.preventDefault&&n.handleObj)return u=n.handleObj,i(n.delegateTarget).off(u.namespace?u.origType+"."+u.namespace:u.origType,u.selector,u.handler),this;if("object"==typeof n){for(f in n)this.off(f,t,n[f]);return this}return t!==!1&&"function"!=typeof t||(r=t,t=void 0),r===!1&&(r=it),this.each(function(){i.event.remove(this,n,r,t)})}});var oe=/<(?!area|br|col|embed|hr|img|input|link|meta|param)(([a-z][^\/\0>\x20\t\r\n\f]*)[^>]*)\/>/gi,se=/<script|<style|<link/i,he=/checked\s*(?:[^=]|=\s*.checked.)/i,ce=/^true\/(.*)/,le=/^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g;i.extend({htmlPrefilter:function(n){return n.replace(oe,"<$1><\/$2>")},clone:function(n,t,r){var u,c,s,e,h=n.cloneNode(!0),l=i.contains(n.ownerDocument,n);if(!(f.noCloneChecked||1!==n.nodeType&&11!==n.nodeType||i.isXMLDoc(n)))for(e=o(h),s=o(n),u=0,c=s.length;u<c;u++)ye(s[u],e[u]);if(t)if(r)for(s=s||o(n),e=e||o(h),u=0,c=s.length;u<c;u++)eu(s[u],e[u]);else eu(n,h);return e=o(h,"script"),e.length>0&&hi(e,!l&&o(n,"script")),h},cleanData:function(n){for(var u,t,f,s=i.event.special,o=0;void 0!==(t=n[o]);o++)if(st(t)){if(u=t[r.expando]){if(u.events)for(f in u.events)s[f]?i.event.remove(t,f):i.removeEvent(t,f,u.handle);t[r.expando]=void 0}t[e.expando]&&(t[e.expando]=void 0)}}});i.fn.extend({detach:function(n){return ou(this,n,!0)},remove:function(n){return ou(this,n)},text:function(n){return v(this,function(n){return void 0===n?i.text(this):this.empty().each(function(){1!==this.nodeType&&11!==this.nodeType&&9!==this.nodeType||(this.textContent=n)})},null,n,arguments.length)},append:function(){return rt(this,arguments,function(n){if(1===this.nodeType||11===this.nodeType||9===this.nodeType){var t=fu(this,n);t.appendChild(n)}})},prepend:function(){return rt(this,arguments,function(n){if(1===this.nodeType||11===this.nodeType||9===this.nodeType){var t=fu(this,n);t.insertBefore(n,t.firstChild)}})},before:function(){return rt(this,arguments,function(n){this.parentNode&&this.parentNode.insertBefore(n,this)})},after:function(){return rt(this,arguments,function(n){this.parentNode&&this.parentNode.insertBefore(n,this.nextSibling)})},empty:function(){for(var n,t=0;null!=(n=this[t]);t++)1===n.nodeType&&(i.cleanData(o(n,!1)),n.textContent="");return this},clone:function(n,t){return n=null!=n&&n,t=null==t?n:t,this.map(function(){return i.clone(this,n,t)})},html:function(n){return v(this,function(n){var t=this[0]||{},r=0,u=this.length;if(void 0===n&&1===t.nodeType)return t.innerHTML;if("string"==typeof n&&!se.test(n)&&!c[(gr.exec(n)||["",""])[1].toLowerCase()]){n=i.htmlPrefilter(n);try{for(;r<u;r++)t=this[r]||{},1===t.nodeType&&(i.cleanData(o(t,!1)),t.innerHTML=n);t=0}catch(f){}}t&&this.empty().append(n)},null,n,arguments.length)},replaceWith:function(){var n=[];return rt(this,arguments,function(t){var r=this.parentNode;i.inArray(this,n)<0&&(i.cleanData(o(this)),r&&r.replaceChild(t,this))},n)}});i.each({appendTo:"append",prependTo:"prepend",insertBefore:"before",insertAfter:"after",replaceAll:"replaceWith"},function(n,t){i.fn[n]=function(n){for(var u,f=[],e=i(n),o=e.length-1,r=0;r<=o;r++)u=r===o?this:this.clone(!0),i(e[r])[t](u),ui.apply(f,u.get());return this.pushStack(f)}});var su=/^margin/,li=new RegExp("^("+wr+")(?!px)[a-z%]+$","i"),ni=function(t){var i=t.ownerDocument.defaultView;return i&&i.opener||(i=n),i.getComputedStyle(t)};!function(){function r(){if(t){t.style.cssText="box-sizing:border-box;position:relative;display:block;margin:auto;border:1px;padding:1px;top:1%;width:50%";t.innerHTML="";dt.appendChild(e);var i=n.getComputedStyle(t);o="1%"!==i.top;c="2px"===i.marginLeft;s="4px"===i.width;t.style.marginRight="50%";h="4px"===i.marginRight;dt.removeChild(e);t=null}}var o,s,h,c,e=u.createElement("div"),t=u.createElement("div");t.style&&(t.style.backgroundClip="content-box",t.cloneNode(!0).style.backgroundClip="",f.clearCloneStyle="content-box"===t.style.backgroundClip,e.style.cssText="border:0;width:8px;height:0;top:0;left:-9999px;padding:0;margin-top:1px;position:absolute",e.appendChild(t),i.extend(f,{pixelPosition:function(){return r(),o},boxSizingReliable:function(){return r(),s},pixelMarginRight:function(){return r(),h},reliableMarginLeft:function(){return r(),c}}))}();var pe=/^(none|table(?!-c[ea]).+)/,cu=/^--/,we={position:"absolute",visibility:"hidden",display:"block"},lu={letterSpacing:"0",fontWeight:"400"},au=["Webkit","Moz","ms"],vu=u.createElement("div").style;i.extend({cssHooks:{opacity:{get:function(n,t){if(t){var i=lt(n,"opacity");return""===i?"1":i}}}},cssNumber:{animationIterationCount:!0,columnCount:!0,fillOpacity:!0,flexGrow:!0,flexShrink:!0,fontWeight:!0,lineHeight:!0,opacity:!0,order:!0,orphans:!0,widows:!0,zIndex:!0,zoom:!0},cssProps:{float:"cssFloat"},style:function(n,t,r,u){if(n&&3!==n.nodeType&&8!==n.nodeType&&n.style){var e,s,o,c=i.camelCase(t),l=cu.test(t),h=n.style;return l||(t=yu(c)),o=i.cssHooks[t]||i.cssHooks[c],void 0===r?o&&"get"in o&&void 0!==(e=o.get(n,!1,u))?e:h[t]:(s=typeof r,"string"===s&&(e=ct.exec(r))&&e[1]&&(r=kr(n,t,e),s="number"),null!=r&&r===r&&("number"===s&&(r+=e&&e[3]||(i.cssNumber[c]?"":"px")),f.clearCloneStyle||""!==r||0!==t.indexOf("background")||(h[t]="inherit"),o&&"set"in o&&void 0===(r=o.set(n,r,u))||(l?h.setProperty(t,r):h[t]=r)),void 0)}},css:function(n,t,r,u){var f,o,e,s=i.camelCase(t),h=cu.test(t);return h||(t=yu(s)),e=i.cssHooks[t]||i.cssHooks[s],e&&"get"in e&&(f=e.get(n,!0,r)),void 0===f&&(f=lt(n,t,u)),"normal"===f&&t in lu&&(f=lu[t]),""===r||r?(o=parseFloat(f),r===!0||isFinite(o)?o||0:f):f}});i.each(["height","width"],function(n,t){i.cssHooks[t]={get:function(n,r,u){if(r)return!pe.test(i.css(n,"display"))||n.getClientRects().length&&n.getBoundingClientRect().width?bu(n,t,u):br(n,we,function(){return bu(n,t,u)})},set:function(n,r,u){var f,e=u&&ni(n),o=u&&wu(n,t,u,"border-box"===i.css(n,"boxSizing",!1,e),e);return o&&(f=ct.exec(r))&&"px"!==(f[3]||"px")&&(n.style[t]=r,r=i.css(n,t)),pu(n,r,o)}}});i.cssHooks.marginLeft=hu(f.reliableMarginLeft,function(n,t){if(t)return(parseFloat(lt(n,"marginLeft"))||n.getBoundingClientRect().left-br(n,{marginLeft:0},function(){return n.getBoundingClientRect().left}))+"px"});i.each({margin:"",padding:"",border:"Width"},function(n,t){i.cssHooks[n+t]={expand:function(i){for(var r=0,f={},u="string"==typeof i?i.split(" "):[i];r<4;r++)f[n+b[r]+t]=u[r]||u[r-2]||u[0];return f}};su.test(n)||(i.cssHooks[n+t].set=pu)});i.fn.extend({css:function(n,t){return v(this,function(n,t,r){var f,e,o={},u=0;if(Array.isArray(t)){for(f=ni(n),e=t.length;u<e;u++)o[t[u]]=i.css(n,t[u],!1,f);return o}return void 0!==r?i.style(n,t,r):i.css(n,t)},n,t,arguments.length>1)}});i.Tween=s;s.prototype={constructor:s,init:function(n,t,r,u,f,e){this.elem=n;this.prop=r;this.easing=f||i.easing._default;this.options=t;this.start=this.now=this.cur();this.end=u;this.unit=e||(i.cssNumber[r]?"":"px")},cur:function(){var n=s.propHooks[this.prop];return n&&n.get?n.get(this):s.propHooks._default.get(this)},run:function(n){var t,r=s.propHooks[this.prop];return this.pos=this.options.duration?t=i.easing[this.easing](n,this.options.duration*n,0,1,this.options.duration):t=n,this.now=(this.end-this.start)*t+this.start,this.options.step&&this.options.step.call(this.elem,this.now,this),r&&r.set?r.set(this):s.propHooks._default.set(this),this}};s.prototype.init.prototype=s.prototype;s.propHooks={_default:{get:function(n){var t;return 1!==n.elem.nodeType||null!=n.elem[n.prop]&&null==n.elem.style[n.prop]?n.elem[n.prop]:(t=i.css(n.elem,n.prop,""),t&&"auto"!==t?t:0)},set:function(n){i.fx.step[n.prop]?i.fx.step[n.prop](n):1!==n.elem.nodeType||null==n.elem.style[i.cssProps[n.prop]]&&!i.cssHooks[n.prop]?n.elem[n.prop]=n.now:i.style(n.elem,n.prop,n.now+n.unit)}}};s.propHooks.scrollTop=s.propHooks.scrollLeft={set:function(n){n.elem.nodeType&&n.elem.parentNode&&(n.elem[n.prop]=n.now)}};i.easing={linear:function(n){return n},swing:function(n){return.5-Math.cos(n*Math.PI)/2},_default:"swing"};i.fx=s.prototype.init;i.fx.step={};ku=/^(?:toggle|show|hide)$/;du=/queueHooks$/;i.Animation=i.extend(a,{tweeners:{"*":[function(n,t){var i=this.createTween(n,t);return kr(i.elem,n,ct.exec(t),i),i}]},tweener:function(n,t){i.isFunction(n)?(t=n,n=["*"]):n=n.match(h);for(var r,u=0,f=n.length;u<f;u++)r=n[u],a.tweeners[r]=a.tweeners[r]||[],a.tweeners[r].unshift(t)},prefilters:[ke],prefilter:function(n,t){t?a.prefilters.unshift(n):a.prefilters.push(n)}});i.speed=function(n,t,r){var u=n&&"object"==typeof n?i.extend({},n):{complete:r||!r&&t||i.isFunction(n)&&n,duration:n,easing:r&&t||t&&!i.isFunction(t)&&t};return i.fx.off?u.duration=0:"number"!=typeof u.duration&&(u.duration=u.duration in i.fx.speeds?i.fx.speeds[u.duration]:i.fx.speeds._default),null!=u.queue&&u.queue!==!0||(u.queue="fx"),u.old=u.complete,u.complete=function(){i.isFunction(u.old)&&u.old.call(this);u.queue&&i.dequeue(this,u.queue)},u};i.fn.extend({fadeTo:function(n,t,i,r){return this.filter(kt).css("opacity",0).show().end().animate({opacity:t},n,i,r)},animate:function(n,t,u,f){var s=i.isEmptyObject(n),o=i.speed(t,u,f),e=function(){var t=a(this,i.extend({},n),o);(s||r.get(this,"finish"))&&t.stop(!0)};return e.finish=e,s||o.queue===!1?this.each(e):this.queue(o.queue,e)},stop:function(n,t,u){var f=function(n){var t=n.stop;delete n.stop;t(u)};return"string"!=typeof n&&(u=t,t=n,n=void 0),t&&n!==!1&&this.queue(n||"fx",[]),this.each(function(){var s=!0,t=null!=n&&n+"queueHooks",o=i.timers,e=r.get(this);if(t)e[t]&&e[t].stop&&f(e[t]);else for(t in e)e[t]&&e[t].stop&&du.test(t)&&f(e[t]);for(t=o.length;t--;)o[t].elem!==this||null!=n&&o[t].queue!==n||(o[t].anim.stop(u),s=!1,o.splice(t,1));!s&&u||i.dequeue(this,n)})},finish:function(n){return n!==!1&&(n=n||"fx"),this.each(function(){var t,e=r.get(this),u=e[n+"queue"],o=e[n+"queueHooks"],f=i.timers,s=u?u.length:0;for(e.finish=!0,i.queue(this,n,[]),o&&o.stop&&o.stop.call(this,!0),t=f.length;t--;)f[t].elem===this&&f[t].queue===n&&(f[t].anim.stop(!0),f.splice(t,1));for(t=0;t<s;t++)u[t]&&u[t].finish&&u[t].finish.call(this);delete e.finish})}});i.each(["toggle","show","hide"],function(n,t){var r=i.fn[t];i.fn[t]=function(n,i,u){return null==n||"boolean"==typeof n?r.apply(this,arguments):this.animate(ii(t,!0),n,i,u)}});i.each({slideDown:ii("show"),slideUp:ii("hide"),slideToggle:ii("toggle"),fadeIn:{opacity:"show"},fadeOut:{opacity:"hide"},fadeToggle:{opacity:"toggle"}},function(n,t){i.fn[n]=function(n,i,r){return this.animate(t,n,i,r)}});i.timers=[];i.fx.tick=function(){var r,n=0,t=i.timers;for(ut=i.now();n<t.length;n++)r=t[n],r()||t[n]!==r||t.splice(n--,1);t.length||i.fx.stop();ut=void 0};i.fx.timer=function(n){i.timers.push(n);i.fx.start()};i.fx.interval=13;i.fx.start=function(){ti||(ti=!0,ai())};i.fx.stop=function(){ti=null};i.fx.speeds={slow:600,fast:200,_default:400};i.fn.delay=function(t,r){return t=i.fx?i.fx.speeds[t]||t:t,r=r||"fx",this.queue(r,function(i,r){var u=n.setTimeout(i,t);r.stop=function(){n.clearTimeout(u)}})},function(){var n=u.createElement("input"),t=u.createElement("select"),i=t.appendChild(u.createElement("option"));n.type="checkbox";f.checkOn=""!==n.value;f.optSelected=i.selected;n=u.createElement("input");n.value="t";n.type="radio";f.radioValue="t"===n.value}();ft=i.expr.attrHandle;i.fn.extend({attr:function(n,t){return v(this,i.attr,n,t,arguments.length>1)},removeAttr:function(n){return this.each(function(){i.removeAttr(this,n)})}});i.extend({attr:function(n,t,r){var u,f,e=n.nodeType;if(3!==e&&8!==e&&2!==e)return"undefined"==typeof n.getAttribute?i.prop(n,t,r):(1===e&&i.isXMLDoc(n)||(f=i.attrHooks[t.toLowerCase()]||(i.expr.match.bool.test(t)?tf:void 0)),void 0!==r?null===r?void i.removeAttr(n,t):f&&"set"in f&&void 0!==(u=f.set(n,r,t))?u:(n.setAttribute(t,r+""),r):f&&"get"in f&&null!==(u=f.get(n,t))?u:(u=i.find.attr(n,t),null==u?void 0:u))},attrHooks:{type:{set:function(n,t){if(!f.radioValue&&"radio"===t&&l(n,"input")){var i=n.value;return n.setAttribute("type",t),i&&(n.value=i),t}}}},removeAttr:function(n,t){var i,u=0,r=t&&t.match(h);if(r&&1===n.nodeType)while(i=r[u++])n.removeAttribute(i)}});tf={set:function(n,t,r){return t===!1?i.removeAttr(n,r):n.setAttribute(r,r),r}};i.each(i.expr.match.bool.source.match(/\w+/g),function(n,t){var r=ft[t]||i.find.attr;ft[t]=function(n,t,i){var f,e,u=t.toLowerCase();return i||(e=ft[u],ft[u]=f,f=null!=r(n,t,i)?u:null,ft[u]=e),f}});rf=/^(?:input|select|textarea|button)$/i;uf=/^(?:a|area)$/i;i.fn.extend({prop:function(n,t){return v(this,i.prop,n,t,arguments.length>1)},removeProp:function(n){return this.each(function(){delete this[i.propFix[n]||n]})}});i.extend({prop:function(n,t,r){var f,u,e=n.nodeType;if(3!==e&&8!==e&&2!==e)return 1===e&&i.isXMLDoc(n)||(t=i.propFix[t]||t,u=i.propHooks[t]),void 0!==r?u&&"set"in u&&void 0!==(f=u.set(n,r,t))?f:n[t]=r:u&&"get"in u&&null!==(f=u.get(n,t))?f:n[t]},propHooks:{tabIndex:{get:function(n){var t=i.find.attr(n,"tabindex");return t?parseInt(t,10):rf.test(n.nodeName)||uf.test(n.nodeName)&&n.href?0:-1}}},propFix:{"for":"htmlFor","class":"className"}});f.optSelected||(i.propHooks.selected={get:function(n){var t=n.parentNode;return t&&t.parentNode&&t.parentNode.selectedIndex,null},set:function(n){var t=n.parentNode;t&&(t.selectedIndex,t.parentNode&&t.parentNode.selectedIndex)}});i.each(["tabIndex","readOnly","maxLength","cellSpacing","cellPadding","rowSpan","colSpan","useMap","frameBorder","contentEditable"],function(){i.propFix[this.toLowerCase()]=this});i.fn.extend({addClass:function(n){var o,t,r,u,f,s,e,c=0;if(i.isFunction(n))return this.each(function(t){i(this).addClass(n.call(this,t,d(this)))});if("string"==typeof n&&n)for(o=n.match(h)||[];t=this[c++];)if(u=d(t),r=1===t.nodeType&&" "+k(u)+" "){for(s=0;f=o[s++];)r.indexOf(" "+f+" ")<0&&(r+=f+" ");e=k(r);u!==e&&t.setAttribute("class",e)}return this},removeClass:function(n){var o,r,t,u,f,s,e,c=0;if(i.isFunction(n))return this.each(function(t){i(this).removeClass(n.call(this,t,d(this)))});if(!arguments.length)return this.attr("class","");if("string"==typeof n&&n)for(o=n.match(h)||[];r=this[c++];)if(u=d(r),t=1===r.nodeType&&" "+k(u)+" "){for(s=0;f=o[s++];)while(t.indexOf(" "+f+" ")>-1)t=t.replace(" "+f+" "," ");e=k(t);u!==e&&r.setAttribute("class",e)}return this},toggleClass:function(n,t){var u=typeof n;return"boolean"==typeof t&&"string"===u?t?this.addClass(n):this.removeClass(n):i.isFunction(n)?this.each(function(r){i(this).toggleClass(n.call(this,r,d(this),t),t)}):this.each(function(){var t,e,f,o;if("string"===u)for(e=0,f=i(this),o=n.match(h)||[];t=o[e++];)f.hasClass(t)?f.removeClass(t):f.addClass(t);else void 0!==n&&"boolean"!==u||(t=d(this),t&&r.set(this,"__className__",t),this.setAttribute&&this.setAttribute("class",t||n===!1?"":r.get(this,"__className__")||""))})},hasClass:function(n){for(var t,r=0,i=" "+n+" ";t=this[r++];)if(1===t.nodeType&&(" "+k(d(t))+" ").indexOf(i)>-1)return!0;return!1}});ff=/\r/g;i.fn.extend({val:function(n){var t,r,f,u=this[0];return arguments.length?(f=i.isFunction(n),this.each(function(r){var u;1===this.nodeType&&(u=f?n.call(this,r,i(this).val()):n,null==u?u="":"number"==typeof u?u+="":Array.isArray(u)&&(u=i.map(u,function(n){return null==n?"":n+""})),t=i.valHooks[this.type]||i.valHooks[this.nodeName.toLowerCase()],t&&"set"in t&&void 0!==t.set(this,u,"value")||(this.value=u))})):u?(t=i.valHooks[u.type]||i.valHooks[u.nodeName.toLowerCase()],t&&"get"in t&&void 0!==(r=t.get(u,"value"))?r:(r=u.value,"string"==typeof r?r.replace(ff,""):null==r?"":r)):void 0}});i.extend({valHooks:{option:{get:function(n){var t=i.find.attr(n,"value");return null!=t?t:k(i.text(n))}},select:{get:function(n){for(var e,t,o=n.options,u=n.selectedIndex,f="select-one"===n.type,s=f?null:[],h=f?u+1:o.length,r=u<0?h:f?u:0;r<h;r++)if(t=o[r],(t.selected||r===u)&&!t.disabled&&(!t.parentNode.disabled||!l(t.parentNode,"optgroup"))){if(e=i(t).val(),f)return e;s.push(e)}return s},set:function(n,t){for(var u,r,f=n.options,e=i.makeArray(t),o=f.length;o--;)r=f[o],(r.selected=i.inArray(i.valHooks.option.get(r),e)>-1)&&(u=!0);return u||(n.selectedIndex=-1),e}}}});i.each(["radio","checkbox"],function(){i.valHooks[this]={set:function(n,t){if(Array.isArray(t))return n.checked=i.inArray(i(n).val(),t)>-1}};f.checkOn||(i.valHooks[this].get=function(n){return null===n.getAttribute("value")?"on":n.value})});vi=/^(?:focusinfocus|focusoutblur)$/;i.extend(i.event,{trigger:function(t,f,e,o){var w,s,c,b,a,v,l,p=[e||u],h=yt.call(t,"type")?t.type:t,y=yt.call(t,"namespace")?t.namespace.split("."):[];if(s=c=e=e||u,3!==e.nodeType&&8!==e.nodeType&&!vi.test(h+i.event.triggered)&&(h.indexOf(".")>-1&&(y=h.split("."),h=y.shift(),y.sort()),a=h.indexOf(":")<0&&"on"+h,t=t[i.expando]?t:new i.Event(h,"object"==typeof t&&t),t.isTrigger=o?2:3,t.namespace=y.join("."),t.rnamespace=t.namespace?new RegExp("(^|\\.)"+y.join("\\.(?:.*\\.|)")+"(\\.|$)"):null,t.result=void 0,t.target||(t.target=e),f=null==f?[t]:i.makeArray(f,[t]),l=i.event.special[h]||{},o||!l.trigger||l.trigger.apply(e,f)!==!1)){if(!o&&!l.noBubble&&!i.isWindow(e)){for(b=l.delegateType||h,vi.test(b+h)||(s=s.parentNode);s;s=s.parentNode)p.push(s),c=s;c===(e.ownerDocument||u)&&p.push(c.defaultView||c.parentWindow||n)}for(w=0;(s=p[w++])&&!t.isPropagationStopped();)t.type=w>1?b:l.bindType||h,v=(r.get(s,"events")||{})[t.type]&&r.get(s,"handle"),v&&v.apply(s,f),v=a&&s[a],v&&v.apply&&st(s)&&(t.result=v.apply(s,f),t.result===!1&&t.preventDefault());return t.type=h,o||t.isDefaultPrevented()||l._default&&l._default.apply(p.pop(),f)!==!1||!st(e)||a&&i.isFunction(e[h])&&!i.isWindow(e)&&(c=e[a],c&&(e[a]=null),i.event.triggered=h,e[h](),i.event.triggered=void 0,c&&(e[a]=c)),t.result}},simulate:function(n,t,r){var u=i.extend(new i.Event,r,{type:n,isSimulated:!0});i.event.trigger(u,null,t)}});i.fn.extend({trigger:function(n,t){return this.each(function(){i.event.trigger(n,t,this)})},triggerHandler:function(n,t){var r=this[0];if(r)return i.event.trigger(n,t,r,!0)}});i.each("blur focus focusin focusout resize scroll click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup contextmenu".split(" "),function(n,t){i.fn[t]=function(n,i){return arguments.length>0?this.on(t,null,n,i):this.trigger(t)}});i.fn.extend({hover:function(n,t){return this.mouseenter(n).mouseleave(t||n)}});f.focusin="onfocusin"in n;f.focusin||i.each({focus:"focusin",blur:"focusout"},function(n,t){var u=function(n){i.event.simulate(t,n.target,i.event.fix(n))};i.event.special[t]={setup:function(){var i=this.ownerDocument||this,f=r.access(i,t);f||i.addEventListener(n,u,!0);r.access(i,t,(f||0)+1)},teardown:function(){var i=this.ownerDocument||this,f=r.access(i,t)-1;f?r.access(i,t,f):(i.removeEventListener(n,u,!0),r.remove(i,t))}}});var at=n.location,ef=i.now(),yi=/\?/;i.parseXML=function(t){var r;if(!t||"string"!=typeof t)return null;try{r=(new n.DOMParser).parseFromString(t,"text/xml")}catch(u){r=void 0}return r&&!r.getElementsByTagName("parsererror").length||i.error("Invalid XML: "+t),r};var ge=/\[\]$/,of=/\r?\n/g,no=/^(?:submit|button|image|reset|file)$/i,to=/^(?:input|select|textarea|keygen)/i;i.param=function(n,t){var r,u=[],f=function(n,t){var r=i.isFunction(t)?t():t;u[u.length]=encodeURIComponent(n)+"="+encodeURIComponent(null==r?"":r)};if(Array.isArray(n)||n.jquery&&!i.isPlainObject(n))i.each(n,function(){f(this.name,this.value)});else for(r in n)pi(r,n[r],t,f);return u.join("&")};i.fn.extend({serialize:function(){return i.param(this.serializeArray())},serializeArray:function(){return this.map(function(){var n=i.prop(this,"elements");return n?i.makeArray(n):this}).filter(function(){var n=this.type;return this.name&&!i(this).is(":disabled")&&to.test(this.nodeName)&&!no.test(n)&&(this.checked||!dr.test(n))}).map(function(n,t){var r=i(this).val();return null==r?null:Array.isArray(r)?i.map(r,function(n){return{name:t.name,value:n.replace(of,"\r\n")}}):{name:t.name,value:r.replace(of,"\r\n")}}).get()}});var io=/%20/g,ro=/#.*$/,uo=/([?&])_=[^&]*/,fo=/^(.*?):[ \t]*([^\r\n]*)$/gm,eo=/^(?:GET|HEAD)$/,oo=/^\/\//,sf={},wi={},hf="*/".concat("*"),bi=u.createElement("a");return bi.href=at.href,i.extend({active:0,lastModified:{},etag:{},ajaxSettings:{url:at.href,type:"GET",isLocal:/^(?:about|app|app-storage|.+-extension|file|res|widget):$/.test(at.protocol),global:!0,processData:!0,async:!0,contentType:"application/x-www-form-urlencoded; charset=UTF-8",accepts:{"*":hf,text:"text/plain",html:"text/html",xml:"application/xml, text/xml",json:"application/json, text/javascript"},contents:{xml:/\bxml\b/,html:/\bhtml/,json:/\bjson\b/},responseFields:{xml:"responseXML",text:"responseText",json:"responseJSON"},converters:{"* text":String,"text html":!0,"text json":JSON.parse,"text xml":i.parseXML},flatOptions:{url:!0,context:!0}},ajaxSetup:function(n,t){return t?ki(ki(n,i.ajaxSettings),t):ki(i.ajaxSettings,n)},ajaxPrefilter:cf(sf),ajaxTransport:cf(wi),ajax:function(t,r){function b(t,r,u,h){var y,rt,g,p,b,l=r;s||(s=!0,d&&n.clearTimeout(d),a=void 0,k=h||"",e.readyState=t>0?4:0,y=t>=200&&t<300||304===t,u&&(p=so(f,e,u)),p=ho(f,p,e,y),y?(f.ifModified&&(b=e.getResponseHeader("Last-Modified"),b&&(i.lastModified[o]=b),b=e.getResponseHeader("etag"),b&&(i.etag[o]=b)),204===t||"HEAD"===f.type?l="nocontent":304===t?l="notmodified":(l=p.state,rt=p.data,g=p.error,y=!g)):(g=l,!t&&l||(l="error",t<0&&(t=0))),e.status=t,e.statusText=(r||l)+"",y?tt.resolveWith(c,[rt,l,e]):tt.rejectWith(c,[e,l,g]),e.statusCode(w),w=void 0,v&&nt.trigger(y?"ajaxSuccess":"ajaxError",[e,f,y?rt:g]),it.fireWith(c,[e,l]),v&&(nt.trigger("ajaxComplete",[e,f]),--i.active||i.event.trigger("ajaxStop")))}"object"==typeof t&&(r=t,t=void 0);r=r||{};var a,o,k,y,d,l,s,v,g,p,f=i.ajaxSetup({},r),c=f.context||f,nt=f.context&&(c.nodeType||c.jquery)?i(c):i.event,tt=i.Deferred(),it=i.Callbacks("once memory"),w=f.statusCode||{},rt={},ut={},ft="canceled",e={readyState:0,getResponseHeader:function(n){var t;if(s){if(!y)for(y={};t=fo.exec(k);)y[t[1].toLowerCase()]=t[2];t=y[n.toLowerCase()]}return null==t?null:t},getAllResponseHeaders:function(){return s?k:null},setRequestHeader:function(n,t){return null==s&&(n=ut[n.toLowerCase()]=ut[n.toLowerCase()]||n,rt[n]=t),this},overrideMimeType:function(n){return null==s&&(f.mimeType=n),this},statusCode:function(n){var t;if(n)if(s)e.always(n[e.status]);else for(t in n)w[t]=[w[t],n[t]];return this},abort:function(n){var t=n||ft;return a&&a.abort(t),b(0,t),this}};if(tt.promise(e),f.url=((t||f.url||at.href)+"").replace(oo,at.protocol+"//"),f.type=r.method||r.type||f.method||f.type,f.dataTypes=(f.dataType||"*").toLowerCase().match(h)||[""],null==f.crossDomain){l=u.createElement("a");try{l.href=f.url;l.href=l.href;f.crossDomain=bi.protocol+"//"+bi.host!=l.protocol+"//"+l.host}catch(et){f.crossDomain=!0}}if(f.data&&f.processData&&"string"!=typeof f.data&&(f.data=i.param(f.data,f.traditional)),lf(sf,f,r,e),s)return e;v=i.event&&f.global;v&&0==i.active++&&i.event.trigger("ajaxStart");f.type=f.type.toUpperCase();f.hasContent=!eo.test(f.type);o=f.url.replace(ro,"");f.hasContent?f.data&&f.processData&&0===(f.contentType||"").indexOf("application/x-www-form-urlencoded")&&(f.data=f.data.replace(io,"+")):(p=f.url.slice(o.length),f.data&&(o+=(yi.test(o)?"&":"?")+f.data,delete f.data),f.cache===!1&&(o=o.replace(uo,"$1"),p=(yi.test(o)?"&":"?")+"_="+ef+++p),f.url=o+p);f.ifModified&&(i.lastModified[o]&&e.setRequestHeader("If-Modified-Since",i.lastModified[o]),i.etag[o]&&e.setRequestHeader("If-None-Match",i.etag[o]));(f.data&&f.hasContent&&f.contentType!==!1||r.contentType)&&e.setRequestHeader("Content-Type",f.contentType);e.setRequestHeader("Accept",f.dataTypes[0]&&f.accepts[f.dataTypes[0]]?f.accepts[f.dataTypes[0]]+("*"!==f.dataTypes[0]?", "+hf+"; q=0.01":""):f.accepts["*"]);for(g in f.headers)e.setRequestHeader(g,f.headers[g]);if(f.beforeSend&&(f.beforeSend.call(c,e,f)===!1||s))return e.abort();if(ft="abort",it.add(f.complete),e.done(f.success),e.fail(f.error),a=lf(wi,f,r,e)){if(e.readyState=1,v&&nt.trigger("ajaxSend",[e,f]),s)return e;f.async&&f.timeout>0&&(d=n.setTimeout(function(){e.abort("timeout")},f.timeout));try{s=!1;a.send(rt,b)}catch(et){if(s)throw et;b(-1,et)}}else b(-1,"No Transport");return e},getJSON:function(n,t,r){return i.get(n,t,r,"json")},getScript:function(n,t){return i.get(n,void 0,t,"script")}}),i.each(["get","post"],function(n,t){i[t]=function(n,r,u,f){return i.isFunction(r)&&(f=f||u,u=r,r=void 0),i.ajax(i.extend({url:n,type:t,dataType:f,data:r,success:u},i.isPlainObject(n)&&n))}}),i._evalUrl=function(n){return i.ajax({url:n,type:"GET",dataType:"script",cache:!0,async:!1,global:!1,throws:!0})},i.fn.extend({wrapAll:function(n){var t;return this[0]&&(i.isFunction(n)&&(n=n.call(this[0])),t=i(n,this[0].ownerDocument).eq(0).clone(!0),this[0].parentNode&&t.insertBefore(this[0]),t.map(function(){for(var n=this;n.firstElementChild;)n=n.firstElementChild;return n}).append(this)),this},wrapInner:function(n){return i.isFunction(n)?this.each(function(t){i(this).wrapInner(n.call(this,t))}):this.each(function(){var t=i(this),r=t.contents();r.length?r.wrapAll(n):t.append(n)})},wrap:function(n){var t=i.isFunction(n);return this.each(function(r){i(this).wrapAll(t?n.call(this,r):n)})},unwrap:function(n){return this.parent(n).not("body").each(function(){i(this).replaceWith(this.childNodes)}),this}}),i.expr.pseudos.hidden=function(n){return!i.expr.pseudos.visible(n)},i.expr.pseudos.visible=function(n){return!!(n.offsetWidth||n.offsetHeight||n.getClientRects().length)},i.ajaxSettings.xhr=function(){try{return new n.XMLHttpRequest}catch(t){}},af={0:200,1223:204},et=i.ajaxSettings.xhr(),f.cors=!!et&&"withCredentials"in et,f.ajax=et=!!et,i.ajaxTransport(function(t){var i,r;if(f.cors||et&&!t.crossDomain)return{send:function(u,f){var o,e=t.xhr();if(e.open(t.type,t.url,t.async,t.username,t.password),t.xhrFields)for(o in t.xhrFields)e[o]=t.xhrFields[o];t.mimeType&&e.overrideMimeType&&e.overrideMimeType(t.mimeType);t.crossDomain||u["X-Requested-With"]||(u["X-Requested-With"]="XMLHttpRequest");for(o in u)e.setRequestHeader(o,u[o]);i=function(n){return function(){i&&(i=r=e.onload=e.onerror=e.onabort=e.onreadystatechange=null,"abort"===n?e.abort():"error"===n?"number"!=typeof e.status?f(0,"error"):f(e.status,e.statusText):f(af[e.status]||e.status,e.statusText,"text"!==(e.responseType||"text")||"string"!=typeof e.responseText?{binary:e.response}:{text:e.responseText},e.getAllResponseHeaders()))}};e.onload=i();r=e.onerror=i("error");void 0!==e.onabort?e.onabort=r:e.onreadystatechange=function(){4===e.readyState&&n.setTimeout(function(){i&&r()})};i=i("abort");try{e.send(t.hasContent&&t.data||null)}catch(s){if(i)throw s;}},abort:function(){i&&i()}}}),i.ajaxPrefilter(function(n){n.crossDomain&&(n.contents.script=!1)}),i.ajaxSetup({accepts:{script:"text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"},contents:{script:/\b(?:java|ecma)script\b/},converters:{"text script":function(n){return i.globalEval(n),n}}}),i.ajaxPrefilter("script",function(n){void 0===n.cache&&(n.cache=!1);n.crossDomain&&(n.type="GET")}),i.ajaxTransport("script",function(n){if(n.crossDomain){var r,t;return{send:function(f,e){r=i("<script>").prop({charset:n.scriptCharset,src:n.url}).on("load error",t=function(n){r.remove();t=null;n&&e("error"===n.type?404:200,n.type)});u.head.appendChild(r[0])},abort:function(){t&&t()}}}}),di=[],ri=/(=)\?(?=&|$)|\?\?/,i.ajaxSetup({jsonp:"callback",jsonpCallback:function(){var n=di.pop()||i.expando+"_"+ef++;return this[n]=!0,n}}),i.ajaxPrefilter("json jsonp",function(t,r,u){var f,e,o,s=t.jsonp!==!1&&(ri.test(t.url)?"url":"string"==typeof t.data&&0===(t.contentType||"").indexOf("application/x-www-form-urlencoded")&&ri.test(t.data)&&"data");if(s||"jsonp"===t.dataTypes[0])return f=t.jsonpCallback=i.isFunction(t.jsonpCallback)?t.jsonpCallback():t.jsonpCallback,s?t[s]=t[s].replace(ri,"$1"+f):t.jsonp!==!1&&(t.url+=(yi.test(t.url)?"&":"?")+t.jsonp+"="+f),t.converters["script json"]=function(){return o||i.error(f+" was not called"),o[0]},t.dataTypes[0]="json",e=n[f],n[f]=function(){o=arguments},u.always(function(){void 0===e?i(n).removeProp(f):n[f]=e;t[f]&&(t.jsonpCallback=r.jsonpCallback,di.push(f));o&&i.isFunction(e)&&e(o[0]);o=e=void 0}),"script"}),f.createHTMLDocument=function(){var n=u.implementation.createHTMLDocument("").body;return n.innerHTML="<form><\/form><form><\/form>",2===n.childNodes.length}(),i.parseHTML=function(n,t,r){if("string"!=typeof n)return[];"boolean"==typeof t&&(r=t,t=!1);var s,e,o;return t||(f.createHTMLDocument?(t=u.implementation.createHTMLDocument(""),s=t.createElement("base"),s.href=u.location.href,t.head.appendChild(s)):t=u),e=ei.exec(n),o=!r&&[],e?[t.createElement(e[1])]:(e=iu([n],t,o),o&&o.length&&i(o).remove(),i.merge([],e.childNodes))},i.fn.load=function(n,t,r){var u,o,s,f=this,e=n.indexOf(" ");return e>-1&&(u=k(n.slice(e)),n=n.slice(0,e)),i.isFunction(t)?(r=t,t=void 0):t&&"object"==typeof t&&(o="POST"),f.length>0&&i.ajax({url:n,type:o||"GET",dataType:"html",data:t}).done(function(n){s=arguments;f.html(u?i("<div>").append(i.parseHTML(n)).find(u):n)}).always(r&&function(n,t){f.each(function(){r.apply(this,s||[n.responseText,t,n])})}),this},i.each(["ajaxStart","ajaxStop","ajaxComplete","ajaxError","ajaxSuccess","ajaxSend"],function(n,t){i.fn[t]=function(n){return this.on(t,n)}}),i.expr.pseudos.animated=function(n){return i.grep(i.timers,function(t){return n===t.elem}).length},i.offset={setOffset:function(n,t,r){var e,o,s,h,u,c,v,l=i.css(n,"position"),a=i(n),f={};"static"===l&&(n.style.position="relative");u=a.offset();s=i.css(n,"top");c=i.css(n,"left");v=("absolute"===l||"fixed"===l)&&(s+c).indexOf("auto")>-1;v?(e=a.position(),h=e.top,o=e.left):(h=parseFloat(s)||0,o=parseFloat(c)||0);i.isFunction(t)&&(t=t.call(n,r,i.extend({},u)));null!=t.top&&(f.top=t.top-u.top+h);null!=t.left&&(f.left=t.left-u.left+o);"using"in t?t.using.call(n,f):a.css(f)}},i.fn.extend({offset:function(n){if(arguments.length)return void 0===n?this:this.each(function(t){i.offset.setOffset(this,n,t)});var r,u,f,e,t=this[0];if(t)return t.getClientRects().length?(f=t.getBoundingClientRect(),r=t.ownerDocument,u=r.documentElement,e=r.defaultView,{top:f.top+e.pageYOffset-u.clientTop,left:f.left+e.pageXOffset-u.clientLeft}):{top:0,left:0}},position:function(){if(this[0]){var t,r,u=this[0],n={top:0,left:0};return"fixed"===i.css(u,"position")?r=u.getBoundingClientRect():(t=this.offsetParent(),r=this.offset(),l(t[0],"html")||(n=t.offset()),n={top:n.top+i.css(t[0],"borderTopWidth",!0),left:n.left+i.css(t[0],"borderLeftWidth",!0)}),{top:r.top-n.top-i.css(u,"marginTop",!0),left:r.left-n.left-i.css(u,"marginLeft",!0)}}},offsetParent:function(){return this.map(function(){for(var n=this.offsetParent;n&&"static"===i.css(n,"position");)n=n.offsetParent;return n||dt})}}),i.each({scrollLeft:"pageXOffset",scrollTop:"pageYOffset"},function(n,t){var r="pageYOffset"===t;i.fn[n]=function(u){return v(this,function(n,u,f){var e;return i.isWindow(n)?e=n:9===n.nodeType&&(e=n.defaultView),void 0===f?e?e[t]:n[u]:void(e?e.scrollTo(r?e.pageXOffset:f,r?f:e.pageYOffset):n[u]=f)},n,u,arguments.length)}}),i.each(["top","left"],function(n,t){i.cssHooks[t]=hu(f.pixelPosition,function(n,r){if(r)return r=lt(n,t),li.test(r)?i(n).position()[t]+"px":r})}),i.each({Height:"height",Width:"width"},function(n,t){i.each({padding:"inner"+n,content:t,"":"outer"+n},function(r,u){i.fn[u]=function(f,e){var o=arguments.length&&(r||"boolean"!=typeof f),s=r||(f===!0||e===!0?"margin":"border");return v(this,function(t,r,f){var e;return i.isWindow(t)?0===u.indexOf("outer")?t["inner"+n]:t.document.documentElement["client"+n]:9===t.nodeType?(e=t.documentElement,Math.max(t.body["scroll"+n],e["scroll"+n],t.body["offset"+n],e["offset"+n],e["client"+n])):void 0===f?i.css(t,r,s):i.style(t,r,f,s)},t,o?f:void 0,o)}})}),i.fn.extend({bind:function(n,t,i){return this.on(n,null,t,i)},unbind:function(n,t){return this.off(n,null,t)},delegate:function(n,t,i,r){return this.on(t,n,i,r)},undelegate:function(n,t,i){return 1===arguments.length?this.off(n,"**"):this.off(t,n||"**",i)}}),i.holdReady=function(n){n?i.readyWait++:i.ready(!0)},i.isArray=Array.isArray,i.parseJSON=JSON.parse,i.nodeName=l,"function"==typeof define&&define.amd&&define("jquery",[],function(){return i}),vf=n.jQuery,yf=n.$,i.noConflict=function(t){return n.$===i&&(n.$=yf),t&&n.jQuery===i&&(n.jQuery=vf),i},t||(n.jQuery=n.$=i),i});
/*!
 * jQuery Cookie Plugin v1.4.1
 * https://github.com/carhartl/jquery-cookie
 *
 * Copyright 2006, 2014 Klaus Hartl
 * Released under the MIT license
 */
(function(n){typeof define=="function"&&define.amd?define(["jquery"],n):typeof exports=="object"?module.exports=n(require("jquery")):n(jQuery)})(function(n){function i(n){return t.raw?n:encodeURIComponent(n)}function f(n){return t.raw?n:decodeURIComponent(n)}function e(n){return i(t.json?JSON.stringify(n):String(n))}function o(n){n.indexOf('"')===0&&(n=n.slice(1,-1).replace(/\\"/g,'"').replace(/\\\\/g,"\\"));try{return n=decodeURIComponent(n.replace(u," ")),t.json?JSON.parse(n):n}catch(i){}}function r(i,r){var u=t.raw?i:o(i);return n.isFunction(r)?r(u):u}var u=/\+/g,t=n.cookie=function(u,o,s){var v,c;if(arguments.length>1&&!n.isFunction(o))return s=n.extend({},t.defaults,s),typeof s.expires=="number"&&(v=s.expires,c=s.expires=new Date,c.setMilliseconds(c.getMilliseconds()+v*864e5)),document.cookie=[i(u),"=",e(o),s.expires?"; expires="+s.expires.toUTCString():"",s.path?"; path="+s.path:"",s.domain?"; domain="+s.domain:"",s.secure?"; secure":""].join("");for(var l=u?undefined:{},y=document.cookie?document.cookie.split("; "):[],a=0,b=y.length;a<b;a++){var p=y[a].split("="),w=f(p.shift()),h=p.join("=");if(u===w){l=r(h,o);break}u||(h=r(h))===undefined||(l[w]=h)}return l};t.defaults={};n.removeCookie=function(t,i){return n.cookie(t,"",n.extend({},i,{expires:-1})),!n.cookie(t)}});
/*!
 * Bootstrap v3.3.7 (http://getbootstrap.com)
 * Copyright 2011-2018 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 */
/*!
 * Generated using the Bootstrap Customizer (<none>)
 * Config saved to config.json and <none>
 */
if("undefined"==typeof jQuery)throw new Error("Bootstrap's JavaScript requires jQuery");+function(n){"use strict";var t=n.fn.jquery.split(" ")[0].split(".");if(t[0]<2&&t[1]<9||1==t[0]&&9==t[1]&&t[2]<1||t[0]>3)throw new Error("Bootstrap's JavaScript requires jQuery version 1.9.1 or higher, but lower than version 4");}(jQuery);+function(n){"use strict";function r(t){var i=t.attr("data-target"),r;return i||(i=t.attr("href"),i=i&&/#[A-Za-z]/.test(i)&&i.replace(/.*(?=#[^\s]*$)/,"")),r=i&&n(i),r&&r.length?r:t.parent()}function u(t){t&&3===t.which||(n(o).remove(),n(i).each(function(){var u=n(this),i=r(u),f={relatedTarget:this};i.hasClass("open")&&(t&&"click"==t.type&&/input|textarea/i.test(t.target.tagName)&&n.contains(i[0],t.target)||(i.trigger(t=n.Event("hide.bs.dropdown",f)),t.isDefaultPrevented()||(u.attr("aria-expanded","false"),i.removeClass("open").trigger(n.Event("hidden.bs.dropdown",f)))))}))}function e(i){return this.each(function(){var r=n(this),u=r.data("bs.dropdown");u||r.data("bs.dropdown",u=new t(this));"string"==typeof i&&u[i].call(r)})}var o=".dropdown-backdrop",i='[data-toggle="dropdown"]',t=function(t){n(t).on("click.bs.dropdown",this.toggle)},f;t.VERSION="3.3.7";t.prototype.toggle=function(t){var f=n(this),i,o,e;if(!f.is(".disabled, :disabled")){if(i=r(f),o=i.hasClass("open"),u(),!o){if("ontouchstart"in document.documentElement&&!i.closest(".navbar-nav").length&&n(document.createElement("div")).addClass("dropdown-backdrop").insertAfter(n(this)).on("click",u),e={relatedTarget:this},i.trigger(t=n.Event("show.bs.dropdown",e)),t.isDefaultPrevented())return;f.trigger("focus").attr("aria-expanded","true");i.toggleClass("open").trigger(n.Event("shown.bs.dropdown",e))}return!1}};t.prototype.keydown=function(t){var e,o,s,h,f,u;if(/(38|40|27|32)/.test(t.which)&&!/input|textarea/i.test(t.target.tagName)&&(e=n(this),t.preventDefault(),t.stopPropagation(),!e.is(".disabled, :disabled"))){if(o=r(e),s=o.hasClass("open"),!s&&27!=t.which||s&&27==t.which)return 27==t.which&&o.find(i).trigger("focus"),e.trigger("click");h=" li:not(.disabled):visible a";f=o.find(".dropdown-menu"+h);f.length&&(u=f.index(t.target),38==t.which&&u>0&&u--,40==t.which&&u<f.length-1&&u++,~u||(u=0),f.eq(u).trigger("focus"))}};f=n.fn.dropdown;n.fn.dropdown=e;n.fn.dropdown.Constructor=t;n.fn.dropdown.noConflict=function(){return n.fn.dropdown=f,this};n(document).on("click.bs.dropdown.data-api",u).on("click.bs.dropdown.data-api",".dropdown form",function(n){n.stopPropagation()}).on("click.bs.dropdown.data-api",i,t.prototype.toggle).on("keydown.bs.dropdown.data-api",i,t.prototype.keydown).on("keydown.bs.dropdown.data-api",".dropdown-menu",t.prototype.keydown)}(jQuery);+function(n){"use strict";function i(i,r){return this.each(function(){var f=n(this),u=f.data("bs.modal"),e=n.extend({},t.DEFAULTS,f.data(),"object"==typeof i&&i);u||f.data("bs.modal",u=new t(this,e));"string"==typeof i?u[i](r):e.show&&u.show(r)})}var t=function(t,i){this.options=i;this.$body=n(document.body);this.$element=n(t);this.$dialog=this.$element.find(".modal-dialog");this.$backdrop=null;this.isShown=null;this.originalBodyPad=null;this.scrollbarWidth=0;this.ignoreBackdropClick=!1;this.options.remote&&this.$element.find(".modal-content").load(this.options.remote,n.proxy(function(){this.$element.trigger("loaded.bs.modal")},this))},r;t.VERSION="3.3.7";t.TRANSITION_DURATION=300;t.BACKDROP_TRANSITION_DURATION=150;t.DEFAULTS={backdrop:!0,keyboard:!0,show:!0};t.prototype.toggle=function(n){return this.isShown?this.hide():this.show(n)};t.prototype.show=function(i){var r=this,u=n.Event("show.bs.modal",{relatedTarget:i});this.$element.trigger(u);this.isShown||u.isDefaultPrevented()||(this.isShown=!0,this.checkScrollbar(),this.setScrollbar(),this.$body.addClass("modal-open"),this.escape(),this.resize(),this.$element.on("click.dismiss.bs.modal",'[data-dismiss="modal"]',n.proxy(this.hide,this)),this.$dialog.on("mousedown.dismiss.bs.modal",function(){r.$element.one("mouseup.dismiss.bs.modal",function(t){n(t.target).is(r.$element)&&(r.ignoreBackdropClick=!0)})}),this.backdrop(function(){var f=n.support.transition&&r.$element.hasClass("fade"),u;r.$element.parent().length||r.$element.appendTo(r.$body);r.$element.show().scrollTop(0);r.adjustDialog();f&&r.$element[0].offsetWidth;r.$element.addClass("in");r.enforceFocus();u=n.Event("shown.bs.modal",{relatedTarget:i});f?r.$dialog.one("bsTransitionEnd",function(){r.$element.trigger("focus").trigger(u)}).emulateTransitionEnd(t.TRANSITION_DURATION):r.$element.trigger("focus").trigger(u)}))};t.prototype.hide=function(i){i&&i.preventDefault();i=n.Event("hide.bs.modal");this.$element.trigger(i);this.isShown&&!i.isDefaultPrevented()&&(this.isShown=!1,this.escape(),this.resize(),n(document).off("focusin.bs.modal"),this.$element.removeClass("in").off("click.dismiss.bs.modal").off("mouseup.dismiss.bs.modal"),this.$dialog.off("mousedown.dismiss.bs.modal"),n.support.transition&&this.$element.hasClass("fade")?this.$element.one("bsTransitionEnd",n.proxy(this.hideModal,this)).emulateTransitionEnd(t.TRANSITION_DURATION):this.hideModal())};t.prototype.enforceFocus=function(){n(document).off("focusin.bs.modal").on("focusin.bs.modal",n.proxy(function(n){document===n.target||this.$element[0]===n.target||this.$element.has(n.target).length||this.$element.trigger("focus")},this))};t.prototype.escape=function(){this.isShown&&this.options.keyboard?this.$element.on("keydown.dismiss.bs.modal",n.proxy(function(n){27==n.which&&this.hide()},this)):this.isShown||this.$element.off("keydown.dismiss.bs.modal")};t.prototype.resize=function(){this.isShown?n(window).on("resize.bs.modal",n.proxy(this.handleUpdate,this)):n(window).off("resize.bs.modal")};t.prototype.hideModal=function(){var n=this;this.$element.hide();this.backdrop(function(){n.$body.removeClass("modal-open");n.resetAdjustments();n.resetScrollbar();n.$element.trigger("hidden.bs.modal")})};t.prototype.removeBackdrop=function(){this.$backdrop&&this.$backdrop.remove();this.$backdrop=null};t.prototype.backdrop=function(i){var e=this,f=this.$element.hasClass("fade")?"fade":"",r,u;if(this.isShown&&this.options.backdrop){if(r=n.support.transition&&f,this.$backdrop=n(document.createElement("div")).addClass("modal-backdrop "+f).appendTo(this.$body),this.$element.on("click.dismiss.bs.modal",n.proxy(function(n){return this.ignoreBackdropClick?void(this.ignoreBackdropClick=!1):void(n.target===n.currentTarget&&("static"==this.options.backdrop?this.$element[0].focus():this.hide()))},this)),r&&this.$backdrop[0].offsetWidth,this.$backdrop.addClass("in"),!i)return;r?this.$backdrop.one("bsTransitionEnd",i).emulateTransitionEnd(t.BACKDROP_TRANSITION_DURATION):i()}else!this.isShown&&this.$backdrop?(this.$backdrop.removeClass("in"),u=function(){e.removeBackdrop();i&&i()},n.support.transition&&this.$element.hasClass("fade")?this.$backdrop.one("bsTransitionEnd",u).emulateTransitionEnd(t.BACKDROP_TRANSITION_DURATION):u()):i&&i()};t.prototype.handleUpdate=function(){this.adjustDialog()};t.prototype.adjustDialog=function(){var n=this.$element[0].scrollHeight>document.documentElement.clientHeight;this.$element.css({paddingLeft:!this.bodyIsOverflowing&&n?this.scrollbarWidth:"",paddingRight:this.bodyIsOverflowing&&!n?this.scrollbarWidth:""})};t.prototype.resetAdjustments=function(){this.$element.css({paddingLeft:"",paddingRight:""})};t.prototype.checkScrollbar=function(){var n=window.innerWidth,t;n||(t=document.documentElement.getBoundingClientRect(),n=t.right-Math.abs(t.left));this.bodyIsOverflowing=document.body.clientWidth<n;this.scrollbarWidth=this.measureScrollbar()};t.prototype.setScrollbar=function(){var n=parseInt(this.$body.css("padding-right")||0,10);this.originalBodyPad=document.body.style.paddingRight||"";this.bodyIsOverflowing&&this.$body.css("padding-right",n+this.scrollbarWidth)};t.prototype.resetScrollbar=function(){this.$body.css("padding-right",this.originalBodyPad)};t.prototype.measureScrollbar=function(){var n=document.createElement("div"),t;return n.className="modal-scrollbar-measure",this.$body.append(n),t=n.offsetWidth-n.clientWidth,this.$body[0].removeChild(n),t};r=n.fn.modal;n.fn.modal=i;n.fn.modal.Constructor=t;n.fn.modal.noConflict=function(){return n.fn.modal=r,this};n(document).on("click.bs.modal.data-api",'[data-toggle="modal"]',function(t){var r=n(this),f=r.attr("href"),u=n(r.attr("data-target")||f&&f.replace(/.*(?=#[^\s]+$)/,"")),e=u.data("bs.modal")?"toggle":n.extend({remote:!/#/.test(f)&&f},u.data(),r.data());r.is("a")&&t.preventDefault();u.one("show.bs.modal",function(n){n.isDefaultPrevented()||u.one("hidden.bs.modal",function(){r.is(":visible")&&r.trigger("focus")})});i.call(u,e,this)})}(jQuery);+function(n){"use strict";function r(i){return this.each(function(){var u=n(this),r=u.data("bs.tooltip"),f="object"==typeof i&&i;!r&&/destroy|hide/.test(i)||(r||u.data("bs.tooltip",r=new t(this,f)),"string"==typeof i&&r[i]())})}var t=function(n,t){this.type=null;this.options=null;this.enabled=null;this.timeout=null;this.hoverState=null;this.$element=null;this.inState=null;this.init("tooltip",n,t)},i;t.VERSION="3.3.7";t.TRANSITION_DURATION=150;t.DEFAULTS={animation:!0,placement:"top",selector:!1,template:'<div class="tooltip" role="tooltip"><div class="tooltip-arrow"><\/div><div class="tooltip-inner"><\/div><\/div>',trigger:"hover focus",title:"",delay:0,html:!1,container:!1,viewport:{selector:"body",padding:0}};t.prototype.init=function(t,i,r){var f,e,u,o,s;if(this.enabled=!0,this.type=t,this.$element=n(i),this.options=this.getOptions(r),this.$viewport=this.options.viewport&&n(n.isFunction(this.options.viewport)?this.options.viewport.call(this,this.$element):this.options.viewport.selector||this.options.viewport),this.inState={click:!1,hover:!1,focus:!1},this.$element[0]instanceof document.constructor&&!this.options.selector)throw new Error("`selector` option must be specified when initializing "+this.type+" on the window.document object!");for(f=this.options.trigger.split(" "),e=f.length;e--;)if(u=f[e],"click"==u)this.$element.on("click."+this.type,this.options.selector,n.proxy(this.toggle,this));else"manual"!=u&&(o="hover"==u?"mouseenter":"focusin",s="hover"==u?"mouseleave":"focusout",this.$element.on(o+"."+this.type,this.options.selector,n.proxy(this.enter,this)),this.$element.on(s+"."+this.type,this.options.selector,n.proxy(this.leave,this)));this.options.selector?this._options=n.extend({},this.options,{trigger:"manual",selector:""}):this.fixTitle()};t.prototype.getDefaults=function(){return t.DEFAULTS};t.prototype.getOptions=function(t){return t=n.extend({},this.getDefaults(),this.$element.data(),t),t.delay&&"number"==typeof t.delay&&(t.delay={show:t.delay,hide:t.delay}),t};t.prototype.getDelegateOptions=function(){var t={},i=this.getDefaults();return this._options&&n.each(this._options,function(n,r){i[n]!=r&&(t[n]=r)}),t};t.prototype.enter=function(t){var i=t instanceof this.constructor?t:n(t.currentTarget).data("bs."+this.type);return i||(i=new this.constructor(t.currentTarget,this.getDelegateOptions()),n(t.currentTarget).data("bs."+this.type,i)),t instanceof n.Event&&(i.inState["focusin"==t.type?"focus":"hover"]=!0),i.tip().hasClass("in")||"in"==i.hoverState?void(i.hoverState="in"):(clearTimeout(i.timeout),i.hoverState="in",i.options.delay&&i.options.delay.show?void(i.timeout=setTimeout(function(){"in"==i.hoverState&&i.show()},i.options.delay.show)):i.show())};t.prototype.isInStateTrue=function(){for(var n in this.inState)if(this.inState[n])return!0;return!1};t.prototype.leave=function(t){var i=t instanceof this.constructor?t:n(t.currentTarget).data("bs."+this.type);return i||(i=new this.constructor(t.currentTarget,this.getDelegateOptions()),n(t.currentTarget).data("bs."+this.type,i)),t instanceof n.Event&&(i.inState["focusout"==t.type?"focus":"hover"]=!1),i.isInStateTrue()?void 0:(clearTimeout(i.timeout),i.hoverState="out",i.options.delay&&i.options.delay.hide?void(i.timeout=setTimeout(function(){"out"==i.hoverState&&i.hide()},i.options.delay.hide)):i.hide())};t.prototype.show=function(){var c=n.Event("show.bs."+this.type),l,p,e,w,h;if(this.hasContent()&&this.enabled){if(this.$element.trigger(c),l=n.contains(this.$element[0].ownerDocument.documentElement,this.$element[0]),c.isDefaultPrevented()||!l)return;var u=this,r=this.tip(),a=this.getUID(this.type);this.setContent();r.attr("id",a);this.$element.attr("aria-describedby",a);this.options.animation&&r.addClass("fade");var i="function"==typeof this.options.placement?this.options.placement.call(this,r[0],this.$element[0]):this.options.placement,v=/\s?auto?\s?/i,y=v.test(i);y&&(i=i.replace(v,"")||"top");r.detach().css({top:0,left:0,display:"block"}).addClass(i).data("bs."+this.type,this);this.options.container?r.appendTo(this.options.container):r.insertAfter(this.$element);this.$element.trigger("inserted.bs."+this.type);var f=this.getPosition(),o=r[0].offsetWidth,s=r[0].offsetHeight;y&&(p=i,e=this.getPosition(this.$viewport),i="bottom"==i&&f.bottom+s>e.bottom?"top":"top"==i&&f.top-s<e.top?"bottom":"right"==i&&f.right+o>e.width?"left":"left"==i&&f.left-o<e.left?"right":i,r.removeClass(p).addClass(i));w=this.getCalculatedOffset(i,f,o,s);this.applyPlacement(w,i);h=function(){var n=u.hoverState;u.$element.trigger("shown.bs."+u.type);u.hoverState=null;"out"==n&&u.leave(u)};n.support.transition&&this.$tip.hasClass("fade")?r.one("bsTransitionEnd",h).emulateTransitionEnd(t.TRANSITION_DURATION):h()}};t.prototype.applyPlacement=function(t,i){var r=this.tip(),l=r[0].offsetWidth,e=r[0].offsetHeight,o=parseInt(r.css("margin-top"),10),s=parseInt(r.css("margin-left"),10),h,f,u;isNaN(o)&&(o=0);isNaN(s)&&(s=0);t.top+=o;t.left+=s;n.offset.setOffset(r[0],n.extend({using:function(n){r.css({top:Math.round(n.top),left:Math.round(n.left)})}},t),0);r.addClass("in");h=r[0].offsetWidth;f=r[0].offsetHeight;"top"==i&&f!=e&&(t.top=t.top+e-f);u=this.getViewportAdjustedDelta(i,t,h,f);u.left?t.left+=u.left:t.top+=u.top;var c=/top|bottom/.test(i),a=c?2*u.left-l+h:2*u.top-e+f,v=c?"offsetWidth":"offsetHeight";r.offset(t);this.replaceArrow(a,r[0][v],c)};t.prototype.replaceArrow=function(n,t,i){this.arrow().css(i?"left":"top",50*(1-n/t)+"%").css(i?"top":"left","")};t.prototype.setContent=function(){var n=this.tip(),t=this.getTitle();n.find(".tooltip-inner")[this.options.html?"html":"text"](t);n.removeClass("fade in top bottom left right")};t.prototype.hide=function(i){function f(){"in"!=r.hoverState&&u.detach();r.$element&&r.$element.removeAttr("aria-describedby").trigger("hidden.bs."+r.type);i&&i()}var r=this,u=n(this.$tip),e=n.Event("hide.bs."+this.type);return this.$element.trigger(e),e.isDefaultPrevented()?void 0:(u.removeClass("in"),n.support.transition&&u.hasClass("fade")?u.one("bsTransitionEnd",f).emulateTransitionEnd(t.TRANSITION_DURATION):f(),this.hoverState=null,this)};t.prototype.fixTitle=function(){var n=this.$element;(n.attr("title")||"string"!=typeof n.attr("data-original-title"))&&n.attr("data-original-title",n.attr("title")||"").attr("title","")};t.prototype.hasContent=function(){return this.getTitle()};t.prototype.getPosition=function(t){t=t||this.$element;var r=t[0],u="BODY"==r.tagName,i=r.getBoundingClientRect();null==i.width&&(i=n.extend({},i,{width:i.right-i.left,height:i.bottom-i.top}));var f=window.SVGElement&&r instanceof window.SVGElement,e=u?{top:0,left:0}:f?null:t.offset(),o={scroll:u?document.documentElement.scrollTop||document.body.scrollTop:t.scrollTop()},s=u?{width:n(window).width(),height:n(window).height()}:null;return n.extend({},i,o,s,e)};t.prototype.getCalculatedOffset=function(n,t,i,r){return"bottom"==n?{top:t.top+t.height,left:t.left+t.width/2-i/2}:"top"==n?{top:t.top-r,left:t.left+t.width/2-i/2}:"left"==n?{top:t.top+t.height/2-r/2,left:t.left-i}:{top:t.top+t.height/2-r/2,left:t.left+t.width}};t.prototype.getViewportAdjustedDelta=function(n,t,i,r){var f={top:0,left:0},e,u,o,s,h,c;return this.$viewport?(e=this.options.viewport&&this.options.viewport.padding||0,u=this.getPosition(this.$viewport),/right|left/.test(n)?(o=t.top-e-u.scroll,s=t.top+e-u.scroll+r,o<u.top?f.top=u.top-o:s>u.top+u.height&&(f.top=u.top+u.height-s)):(h=t.left-e,c=t.left+e+i,h<u.left?f.left=u.left-h:c>u.right&&(f.left=u.left+u.width-c)),f):f};t.prototype.getTitle=function(){var t=this.$element,n=this.options;return t.attr("data-original-title")||("function"==typeof n.title?n.title.call(t[0]):n.title)};t.prototype.getUID=function(n){do n+=~~(1e6*Math.random());while(document.getElementById(n));return n};t.prototype.tip=function(){if(!this.$tip&&(this.$tip=n(this.options.template),1!=this.$tip.length))throw new Error(this.type+" `template` option must consist of exactly 1 top-level element!");return this.$tip};t.prototype.arrow=function(){return this.$arrow=this.$arrow||this.tip().find(".tooltip-arrow")};t.prototype.enable=function(){this.enabled=!0};t.prototype.disable=function(){this.enabled=!1};t.prototype.toggleEnabled=function(){this.enabled=!this.enabled};t.prototype.toggle=function(t){var i=this;t&&(i=n(t.currentTarget).data("bs."+this.type),i||(i=new this.constructor(t.currentTarget,this.getDelegateOptions()),n(t.currentTarget).data("bs."+this.type,i)));t?(i.inState.click=!i.inState.click,i.isInStateTrue()?i.enter(i):i.leave(i)):i.tip().hasClass("in")?i.leave(i):i.enter(i)};t.prototype.destroy=function(){var n=this;clearTimeout(this.timeout);this.hide(function(){n.$element.off("."+n.type).removeData("bs."+n.type);n.$tip&&n.$tip.detach();n.$tip=null;n.$arrow=null;n.$viewport=null;n.$element=null})};i=n.fn.tooltip;n.fn.tooltip=r;n.fn.tooltip.Constructor=t;n.fn.tooltip.noConflict=function(){return n.fn.tooltip=i,this}}(jQuery);+function(n){"use strict";function r(i){return this.each(function(){var u=n(this),r=u.data("bs.tab");r||u.data("bs.tab",r=new t(this));"string"==typeof i&&r[i]()})}var t=function(t){this.element=n(t)},u,i;t.VERSION="3.3.7";t.TRANSITION_DURATION=150;t.prototype.show=function(){var t=this.element,f=t.closest("ul:not(.dropdown-menu)"),i=t.data("target"),u;if(i||(i=t.attr("href"),i=i&&i.replace(/.*(?=#[^\s]*$)/,"")),!t.parent("li").hasClass("active")){var r=f.find(".active:last a"),e=n.Event("hide.bs.tab",{relatedTarget:t[0]}),o=n.Event("show.bs.tab",{relatedTarget:r[0]});(r.trigger(e),t.trigger(o),o.isDefaultPrevented()||e.isDefaultPrevented())||(u=n(i),this.activate(t.closest("li"),f),this.activate(u,u.parent(),function(){r.trigger({type:"hidden.bs.tab",relatedTarget:t[0]});t.trigger({type:"shown.bs.tab",relatedTarget:r[0]})}))}};t.prototype.activate=function(i,r,u){function e(){f.removeClass("active").find("> .dropdown-menu > .active").removeClass("active").end().find('[data-toggle="tab"]').attr("aria-expanded",!1);i.addClass("active").find('[data-toggle="tab"]').attr("aria-expanded",!0);o?(i[0].offsetWidth,i.addClass("in")):i.removeClass("fade");i.parent(".dropdown-menu").length&&i.closest("li.dropdown").addClass("active").end().find('[data-toggle="tab"]').attr("aria-expanded",!0);u&&u()}var f=r.find("> .active"),o=u&&n.support.transition&&(f.length&&f.hasClass("fade")||!!r.find("> .fade").length);f.length&&o?f.one("bsTransitionEnd",e).emulateTransitionEnd(t.TRANSITION_DURATION):e();f.removeClass("in")};u=n.fn.tab;n.fn.tab=r;n.fn.tab.Constructor=t;n.fn.tab.noConflict=function(){return n.fn.tab=u,this};i=function(t){t.preventDefault();r.call(n(this),"show")};n(document).on("click.bs.tab.data-api",'[data-toggle="tab"]',i).on("click.bs.tab.data-api",'[data-toggle="pill"]',i)}(jQuery);+function(n){"use strict";function i(i){return this.each(function(){var u=n(this),r=u.data("bs.affix"),f="object"==typeof i&&i;r||u.data("bs.affix",r=new t(this,f));"string"==typeof i&&r[i]()})}var t=function(i,r){this.options=n.extend({},t.DEFAULTS,r);this.$target=n(this.options.target).on("scroll.bs.affix.data-api",n.proxy(this.checkPosition,this)).on("click.bs.affix.data-api",n.proxy(this.checkPositionWithEventLoop,this));this.$element=n(i);this.affixed=null;this.unpin=null;this.pinnedOffset=null;this.checkPosition()},r;t.VERSION="3.3.7";t.RESET="affix affix-top affix-bottom";t.DEFAULTS={offset:0,target:window};t.prototype.getState=function(n,t,i,r){var u=this.$target.scrollTop(),f=this.$element.offset(),e=this.$target.height();if(null!=i&&"top"==this.affixed)return i>u?"top":!1;if("bottom"==this.affixed)return null!=i?u+this.unpin<=f.top?!1:"bottom":n-r>=u+e?!1:"bottom";var o=null==this.affixed,s=o?u:f.top,h=o?e:t;return null!=i&&i>=u?"top":null!=r&&s+h>=n-r?"bottom":!1};t.prototype.getPinnedOffset=function(){if(this.pinnedOffset)return this.pinnedOffset;this.$element.removeClass(t.RESET).addClass("affix");var n=this.$target.scrollTop(),i=this.$element.offset();return this.pinnedOffset=i.top-n};t.prototype.checkPositionWithEventLoop=function(){setTimeout(n.proxy(this.checkPosition,this),1)};t.prototype.checkPosition=function(){var i,e,o;if(this.$element.is(":visible")){var s=this.$element.height(),r=this.options.offset,f=r.top,u=r.bottom,h=Math.max(n(document).height(),n(document.body).height());if("object"!=typeof r&&(u=f=r),"function"==typeof f&&(f=r.top(this.$element)),"function"==typeof u&&(u=r.bottom(this.$element)),i=this.getState(h,s,f,u),this.affixed!=i){if(null!=this.unpin&&this.$element.css("top",""),e="affix"+(i?"-"+i:""),o=n.Event(e+".bs.affix"),this.$element.trigger(o),o.isDefaultPrevented())return;this.affixed=i;this.unpin="bottom"==i?this.getPinnedOffset():null;this.$element.removeClass(t.RESET).addClass(e).trigger(e.replace("affix","affixed")+".bs.affix")}"bottom"==i&&this.$element.offset({top:h-s-u})}};r=n.fn.affix;n.fn.affix=i;n.fn.affix.Constructor=t;n.fn.affix.noConflict=function(){return n.fn.affix=r,this};n(window).on("load",function(){n('[data-spy="affix"]').each(function(){var r=n(this),t=r.data();t.offset=t.offset||{};null!=t.offsetBottom&&(t.offset.bottom=t.offsetBottom);null!=t.offsetTop&&(t.offset.top=t.offsetTop);i.call(r,t)})})}(jQuery);+function(n){"use strict";function r(t){var i,r=t.attr("data-target")||(i=t.attr("href"))&&i.replace(/.*(?=#[^\s]+$)/,"");return n(r)}function i(i){return this.each(function(){var u=n(this),r=u.data("bs.collapse"),f=n.extend({},t.DEFAULTS,u.data(),"object"==typeof i&&i);!r&&f.toggle&&/show|hide/.test(i)&&(f.toggle=!1);r||u.data("bs.collapse",r=new t(this,f));"string"==typeof i&&r[i]()})}var t=function(i,r){this.$element=n(i);this.options=n.extend({},t.DEFAULTS,r);this.$trigger=n('[data-toggle="collapse"][href="#'+i.id+'"],[data-toggle="collapse"][data-target="#'+i.id+'"]');this.transitioning=null;this.options.parent?this.$parent=this.getParent():this.addAriaAndCollapsedClass(this.$element,this.$trigger);this.options.toggle&&this.toggle()},u;t.VERSION="3.3.7";t.TRANSITION_DURATION=350;t.DEFAULTS={toggle:!0};t.prototype.dimension=function(){var n=this.$element.hasClass("width");return n?"width":"height"};t.prototype.show=function(){var f,r,e,u,o,s;if(!this.transitioning&&!this.$element.hasClass("in")&&(r=this.$parent&&this.$parent.children(".panel").children(".in, .collapsing"),!(r&&r.length&&(f=r.data("bs.collapse"),f&&f.transitioning))&&(e=n.Event("show.bs.collapse"),this.$element.trigger(e),!e.isDefaultPrevented()))){if(r&&r.length&&(i.call(r,"hide"),f||r.data("bs.collapse",null)),u=this.dimension(),this.$element.removeClass("collapse").addClass("collapsing")[u](0).attr("aria-expanded",!0),this.$trigger.removeClass("collapsed").attr("aria-expanded",!0),this.transitioning=1,o=function(){this.$element.removeClass("collapsing").addClass("collapse in")[u]("");this.transitioning=0;this.$element.trigger("shown.bs.collapse")},!n.support.transition)return o.call(this);s=n.camelCase(["scroll",u].join("-"));this.$element.one("bsTransitionEnd",n.proxy(o,this)).emulateTransitionEnd(t.TRANSITION_DURATION)[u](this.$element[0][s])}};t.prototype.hide=function(){var r,i,u;if(!this.transitioning&&this.$element.hasClass("in")&&(r=n.Event("hide.bs.collapse"),this.$element.trigger(r),!r.isDefaultPrevented()))return i=this.dimension(),this.$element[i](this.$element[i]())[0].offsetHeight,this.$element.addClass("collapsing").removeClass("collapse in").attr("aria-expanded",!1),this.$trigger.addClass("collapsed").attr("aria-expanded",!1),this.transitioning=1,u=function(){this.transitioning=0;this.$element.removeClass("collapsing").addClass("collapse").trigger("hidden.bs.collapse")},n.support.transition?void this.$element[i](0).one("bsTransitionEnd",n.proxy(u,this)).emulateTransitionEnd(t.TRANSITION_DURATION):u.call(this)};t.prototype.toggle=function(){this[this.$element.hasClass("in")?"hide":"show"]()};t.prototype.getParent=function(){return n(this.options.parent).find('[data-toggle="collapse"][data-parent="'+this.options.parent+'"]').each(n.proxy(function(t,i){var u=n(i);this.addAriaAndCollapsedClass(r(u),u)},this)).end()};t.prototype.addAriaAndCollapsedClass=function(n,t){var i=n.hasClass("in");n.attr("aria-expanded",i);t.toggleClass("collapsed",!i).attr("aria-expanded",i)};u=n.fn.collapse;n.fn.collapse=i;n.fn.collapse.Constructor=t;n.fn.collapse.noConflict=function(){return n.fn.collapse=u,this};n(document).on("click.bs.collapse.data-api",'[data-toggle="collapse"]',function(t){var u=n(this);u.attr("data-target")||t.preventDefault();var f=r(u),e=f.data("bs.collapse"),o=e?"toggle":u.data();i.call(f,o)})}(jQuery);+function(n){"use strict";function t(i,r){this.$body=n(document.body);this.$scrollElement=n(n(i).is(document.body)?window:i);this.options=n.extend({},t.DEFAULTS,r);this.selector=(this.options.target||"")+" .nav li > a";this.offsets=[];this.targets=[];this.activeTarget=null;this.scrollHeight=0;this.$scrollElement.on("scroll.bs.scrollspy",n.proxy(this.process,this));this.refresh();this.process()}function i(i){return this.each(function(){var u=n(this),r=u.data("bs.scrollspy"),f="object"==typeof i&&i;r||u.data("bs.scrollspy",r=new t(this,f));"string"==typeof i&&r[i]()})}t.VERSION="3.3.7";t.DEFAULTS={offset:10};t.prototype.getScrollHeight=function(){return this.$scrollElement[0].scrollHeight||Math.max(this.$body[0].scrollHeight,document.documentElement.scrollHeight)};t.prototype.refresh=function(){var t=this,i="offset",r=0;this.offsets=[];this.targets=[];this.scrollHeight=this.getScrollHeight();n.isWindow(this.$scrollElement[0])||(i="position",r=this.$scrollElement.scrollTop());this.$body.find(this.selector).map(function(){var f=n(this),u=f.data("target")||f.attr("href"),t=/^#./.test(u)&&n(u);return t&&t.length&&t.is(":visible")&&[[t[i]().top+r,u]]||null}).sort(function(n,t){return n[0]-t[0]}).each(function(){t.offsets.push(this[0]);t.targets.push(this[1])})};t.prototype.process=function(){var n,i=this.$scrollElement.scrollTop()+this.options.offset,f=this.getScrollHeight(),e=this.options.offset+f-this.$scrollElement.height(),t=this.offsets,r=this.targets,u=this.activeTarget;if(this.scrollHeight!=f&&this.refresh(),i>=e)return u!=(n=r[r.length-1])&&this.activate(n);if(u&&i<t[0])return this.activeTarget=null,this.clear();for(n=t.length;n--;)u!=r[n]&&i>=t[n]&&(void 0===t[n+1]||i<t[n+1])&&this.activate(r[n])};t.prototype.activate=function(t){this.activeTarget=t;this.clear();var r=this.selector+'[data-target="'+t+'"],'+this.selector+'[href="'+t+'"]',i=n(r).parents("li").addClass("active");i.parent(".dropdown-menu").length&&(i=i.closest("li.dropdown").addClass("active"));i.trigger("activate.bs.scrollspy")};t.prototype.clear=function(){n(this.selector).parentsUntil(this.options.target,".active").removeClass("active")};var r=n.fn.scrollspy;n.fn.scrollspy=i;n.fn.scrollspy.Constructor=t;n.fn.scrollspy.noConflict=function(){return n.fn.scrollspy=r,this};n(window).on("load.bs.scrollspy.data-api",function(){n('[data-spy="scroll"]').each(function(){var t=n(this);i.call(t,t.data())})})}(jQuery);+function(n){"use strict";function t(){var i=document.createElement("bootstrap"),n={WebkitTransition:"webkitTransitionEnd",MozTransition:"transitionend",OTransition:"oTransitionEnd otransitionend",transition:"transitionend"};for(var t in n)if(void 0!==i.style[t])return{end:n[t]};return!1}n.fn.emulateTransitionEnd=function(t){var i=!1,u=this,r;n(this).one("bsTransitionEnd",function(){i=!0});return r=function(){i||n(u).trigger(n.support.transition.end)},setTimeout(r,t),this};n(function(){n.support.transition=t();n.support.transition&&(n.event.special.bsTransitionEnd={bindType:n.support.transition.end,delegateType:n.support.transition.end,handle:function(t){if(n(t.target).is(this))return t.handleObj.handler.apply(this,arguments)}})})}(jQuery),function(n){"use strict";function t(n,t){n.className+=" "+t}function r(n,t){for(var u,i=n.className.split(" "),f=t.split(" "),r=0;r<f.length;r+=1)u=i.indexOf(f[r]),u>-1&&i.splice(u,1);n.className=i.join(" ")}function w(){return n.getComputedStyle(document.body).direction==="rtl"}function y(){return document.documentElement&&document.documentElement.scrollTop||document.body.scrollTop}function p(){return document.documentElement&&document.documentElement.scrollLeft||document.body.scrollLeft}function l(n){while(n.lastChild)n.removeChild(n.lastChild)}function v(n){var t,i,r;if(null===n)return n;if(Array.isArray(n)){for(t=[],i=0;i<n.length;i+=1)t.push(v(n[i]));return t}if(n instanceof Date)return new Date(n.getTime());if(n instanceof RegExp)return t=new RegExp(n.source),t.global=n.global,t.ignoreCase=n.ignoreCase,t.multiline=n.multiline,t.lastIndex=n.lastIndex,t;if(typeof n=="object"){t={};for(r in n)n.hasOwnProperty(r)&&(t[r]=v(n[r]));return t}return n}function b(n,t){if(n.elements){var i=n.elements.root;i.parentNode.removeChild(i);delete n.elements;n.settings=v(n.__settings);n.__init=t;delete n.__internal}}function h(n,t){return function(){var i,r;if(arguments.length>0){for(i=[],r=0;r<arguments.length;r+=1)i.push(arguments[r]);return i.push(n),t.apply(n,i)}return t.apply(n,[null,n])}}function k(n,t){return{index:n,button:t,cancel:!1}}function o(n,t){if(typeof t.get(n)=="function")return t.get(n).call(t)}function nt(){function t(n,t){for(var i in t)t.hasOwnProperty(i)&&(n[i]=t[i]);return n}function i(t){var i=n[t].dialog;return i&&typeof i.__init=="function"&&i.__init(i),i}function r(i,r,u,f){var e={dialog:null,factory:r};return f!==undefined&&(e.factory=function(){return t(new n[f].factory,new r)}),u||(e.dialog=t(new e.factory,d)),n[i]=e}var n={};return{defaults:g,dialog:function(n,u,f,e){if(typeof u!="function")return i(n);if(this.hasOwnProperty(n))throw new Error("alertify.dialog: name already exists");var o=r(n,u,f,e);this[n]=f?function(){if(arguments.length===0)return o.dialog;var n=t(new o.factory,d);return n&&typeof n.__init=="function"&&n.__init(n),n.main.apply(n,arguments),n.show.apply(n)}:function(){if(o.dialog&&typeof o.dialog.__init=="function"&&o.dialog.__init(o.dialog),arguments.length===0)return o.dialog;var n=o.dialog;return n.main.apply(o.dialog,arguments),n.show.apply(o.dialog)}},closeAll:function(n){for(var i,r=f.slice(0),t=0;t<r.length;t+=1)i=r[t],(n===undefined||n!==i)&&i.close()},setting:function(n,t,r){if(n==="notifier")return c.setting(t,r);var u=i(n);if(u)return u.setting(t,r)},set:function(n,t,i){return this.setting(n,t,i)},get:function(n,t){return this.setting(n,t)},notify:function(n,t,i,r){return c.create(t,r).push(n,i)},message:function(n,t,i){return c.create(null,i).push(n,t)},success:function(n,t,i){return c.create("success",i).push(n,t)},error:function(n,t,i){return c.create("error",i).push(n,t)},warning:function(n,t,i){return c.create("warning",i).push(n,t)},dismissAll:function(){c.dismissAll()}}}var s={ENTER:13,ESC:27,F1:112,F12:123,LEFT:37,RIGHT:39},g={autoReset:!0,basic:!1,closable:!0,closableByDimmer:!0,frameless:!1,maintainFocus:!0,maximizable:!0,modal:!0,movable:!0,moveBounded:!1,overflow:!0,padding:!0,pinnable:!0,pinned:!0,preventBodyShift:!1,resizable:!0,startMaximized:!1,transition:"pulse",notifier:{delay:5,position:"bottom-right",closeButton:!1},glossary:{title:"AlertifyJS",ok:"OK",cancel:"Cancel",acccpt:"Accept",deny:"Deny",confirm:"Confirm",decline:"Decline",close:"Close",maximize:"Maximize",restore:"Restore"},theme:{input:"ajs-input",ok:"ajs-ok",cancel:"ajs-cancel"}},f=[],u=function(){return document.addEventListener?function(n,t,i,r){n.addEventListener(t,i,r===!0)}:document.attachEvent?function(n,t,i){n.attachEvent("on"+t,i)}:void 0}(),e=function(){return document.removeEventListener?function(n,t,i,r){n.removeEventListener(t,i,r===!0)}:document.detachEvent?function(n,t,i){n.detachEvent("on"+t,i)}:void 0}(),a=function(){var n,t,i=!1,r={animation:"animationend",OAnimation:"oAnimationEnd oanimationend",msAnimation:"MSAnimationEnd",MozAnimation:"animationend",WebkitAnimation:"webkitAnimationEnd"};for(n in r)if(document.documentElement.style[n]!==undefined){t=r[n];i=!0;break}return{type:t,supported:i}}(),d=function(){function st(n){var f,w,l,a,b,y,e,r,p,u,s,o;if(!n.__internal){if(delete n.__init,n.__settings||(n.__settings=v(n.settings)),typeof n.setup=="function"?(f=n.setup(),f.options=f.options||{},f.focus=f.focus||{}):f={buttons:[],focus:{element:null,select:!1},options:{}},typeof n.hooks!="object"&&(n.hooks={}),w=[],Array.isArray(f.buttons))for(l=0;l<f.buttons.length;l+=1){a=f.buttons[l];b={};for(y in a)a.hasOwnProperty(y)&&(b[y]=a[y]);w.push(b)}for(e=n.__internal={isOpen:!1,activeElement:document.body,timerIn:undefined,timerOut:undefined,buttons:w,focus:f.focus,options:{title:undefined,modal:undefined,basic:undefined,frameless:undefined,pinned:undefined,movable:undefined,moveBounded:undefined,resizable:undefined,autoReset:undefined,closable:undefined,closableByDimmer:undefined,maximizable:undefined,startMaximized:undefined,pinnable:undefined,transition:undefined,padding:undefined,overflow:undefined,onshow:undefined,onclosing:undefined,onclose:undefined,onfocus:undefined,onmove:undefined,onmoved:undefined,onresize:undefined,onresized:undefined,onmaximize:undefined,onmaximized:undefined,onrestore:undefined,onrestored:undefined},resetHandler:undefined,beginMoveHandler:undefined,beginResizeHandler:undefined,bringToFrontHandler:undefined,modalClickHandler:undefined,buttonsClickHandler:undefined,commandsClickHandler:undefined,transitionInHandler:undefined,transitionOutHandler:undefined,destroy:undefined},r={},r.root=document.createElement("div"),r.root.className=c.base+" "+c.hidden+" ",r.root.innerHTML=d.dimmer+d.modal,r.dimmer=r.root.firstChild,r.modal=r.root.lastChild,r.modal.innerHTML=d.dialog,r.dialog=r.modal.firstChild,r.dialog.innerHTML=d.reset+d.commands+d.header+d.body+d.footer+d.resizeHandle+d.reset,r.reset=[],r.reset.push(r.dialog.firstChild),r.reset.push(r.dialog.lastChild),r.commands={},r.commands.container=r.reset[0].nextSibling,r.commands.pin=r.commands.container.firstChild,r.commands.maximize=r.commands.pin.nextSibling,r.commands.close=r.commands.maximize.nextSibling,r.header=r.commands.container.nextSibling,r.body=r.header.nextSibling,r.body.innerHTML=d.content,r.content=r.body.firstChild,r.footer=r.body.nextSibling,r.footer.innerHTML=d.buttons.auxiliary+d.buttons.primary,r.resizeHandle=r.footer.nextSibling,r.buttons={},r.buttons.auxiliary=r.footer.firstChild,r.buttons.primary=r.buttons.auxiliary.nextSibling,r.buttons.primary.innerHTML=d.button,r.buttonTemplate=r.buttons.primary.firstChild,r.buttons.primary.removeChild(r.buttonTemplate),p=0;p<n.__internal.buttons.length;p+=1){u=n.__internal.buttons[p];ot.indexOf(u.key)<0&&ot.push(u.key);u.element=r.buttonTemplate.cloneNode();u.element.innerHTML=u.text;typeof u.className=="string"&&u.className!==""&&t(u.element,u.className);for(s in u.attrs)s!=="className"&&u.attrs.hasOwnProperty(s)&&u.element.setAttribute(s,u.attrs[s]);u.scope==="auxiliary"?r.buttons.auxiliary.appendChild(u.element):r.buttons.primary.appendChild(u.element)}n.elements=r;e.resetHandler=h(n,fi);e.beginMoveHandler=h(n,cu);e.beginResizeHandler=h(n,vu);e.bringToFrontHandler=h(n,bi);e.modalClickHandler=h(n,uu);e.buttonsClickHandler=h(n,fu);e.commandsClickHandler=h(n,gr);e.transitionInHandler=h(n,eu);e.transitionOutHandler=h(n,ou);for(o in e.options)f.options[o]!==undefined?n.set(o,f.options[o]):i.defaults.hasOwnProperty(o)?n.set(o,i.defaults[o]):o==="title"&&n.set(o,i.defaults.glossary[o]);typeof n.build=="function"&&n.build()}document.body.appendChild(n.elements.root)}function yr(){ai=p();ut=y()}function vi(){n.scrollTo(ai,ut)}function it(){for(var u,n=0,i=0;i<f.length;i+=1)u=f[i],(u.isModal()||u.isMaximized())&&(n+=1);n===0&&document.body.className.indexOf(c.noOverflow)>=0?(r(document.body,c.noOverflow),wi(!1)):n>0&&document.body.className.indexOf(c.noOverflow)<0&&(wi(!0),t(document.body,c.noOverflow))}function wi(u){i.defaults.preventBodyShift&&document.documentElement.scrollHeight>document.documentElement.clientHeight&&(u?(pi=ut,yi=n.getComputedStyle(document.body).top,t(document.body,c.fixed),document.body.style.top=-ut+"px"):(ut=pi,document.body.style.top=yi,r(document.body,c.fixed),vi()))}function pr(n,i,u){typeof u=="string"&&r(n.elements.root,c.prefix+u);t(n.elements.root,c.prefix+i);gt=n.elements.root.offsetWidth}function wr(n){n.get("modal")?(r(n.elements.root,c.modeless),n.isOpen()&&(sr(n),ii(n),it())):(t(n.elements.root,c.modeless),n.isOpen()&&(or(n),ii(n),it()))}function br(n){n.get("basic")?t(n.elements.root,c.basic):r(n.elements.root,c.basic)}function kr(n){n.get("frameless")?t(n.elements.root,c.frameless):r(n.elements.root,c.frameless)}function bi(n,t){for(var r=f.indexOf(t),i=r+1;i<f.length;i+=1)if(f[i].isModal())return;return document.body.lastChild!==t.elements.root&&(document.body.appendChild(t.elements.root),f.splice(f.indexOf(t),1),f.push(t),ui(t)),!1}function dr(n,i,u,f){switch(i){case"title":n.setHeader(f);break;case"modal":wr(n);break;case"basic":br(n);break;case"frameless":kr(n);break;case"pinned":tu(n);break;case"closable":ru(n);break;case"maximizable":iu(n);break;case"pinnable":nu(n);break;case"movable":lu(n);break;case"resizable":yu(n);break;case"padding":f?r(n.elements.root,c.noPadding):n.elements.root.className.indexOf(c.noPadding)<0&&t(n.elements.root,c.noPadding);break;case"overflow":f?r(n.elements.root,c.noOverflow):n.elements.root.className.indexOf(c.noOverflow)<0&&t(n.elements.root,c.noOverflow);break;case"transition":pr(n,f,u)}typeof n.hooks.onupdate=="function"&&n.hooks.onupdate.call(n,i,u,f)}function ni(n,t,i,r,u){var e={op:undefined,items:[]},s,o,f;if(typeof u=="undefined"&&typeof r=="string")e.op="get",t.hasOwnProperty(r)?(e.found=!0,e.value=t[r]):(e.found=!1,e.value=undefined);else if(e.op="set",typeof r=="object"){o=r;for(f in o)t.hasOwnProperty(f)?(t[f]!==o[f]&&(s=t[f],t[f]=o[f],i.call(n,f,s,o[f])),e.items.push({key:f,value:o[f],found:!0})):e.items.push({key:f,value:o[f],found:!1})}else if(typeof r=="string")t.hasOwnProperty(r)?(t[r]!==u&&(s=t[r],t[r]=u,i.call(n,r,s,u)),e.items.push({key:r,value:u,found:!0})):e.items.push({key:r,value:u,found:!1});else throw new Error("args must be a string or object");return e}function ti(n){var t;ct(n,function(n){return t=n.invokeOnClose===!0});!t&&n.isOpen()&&n.close()}function gr(n,t){var i=n.srcElement||n.target;switch(i){case t.elements.commands.pin:t.isPinned()?di(t):ki(t);break;case t.elements.commands.maximize:t.isMaximized()?ht(t):gi(t);break;case t.elements.commands.close:ti(t)}return!1}function ki(n){n.set("pinned",!0)}function di(n){n.set("pinned",!1)}function gi(n){o("onmaximize",n);t(n.elements.root,c.maximized);n.isOpen()&&it();o("onmaximized",n)}function ht(n){o("onrestore",n);r(n.elements.root,c.maximized);n.isOpen()&&it();o("onrestored",n)}function nu(n){n.get("pinnable")?t(n.elements.root,c.pinnable):r(n.elements.root,c.pinnable)}function nr(n){var t=p();n.elements.modal.style.marginTop=y()+"px";n.elements.modal.style.marginLeft=t+"px";n.elements.modal.style.marginRight=-t+"px"}function tr(n){var r=parseInt(n.elements.modal.style.marginTop,10),u=parseInt(n.elements.modal.style.marginLeft,10),t,i;n.elements.modal.style.marginTop="";n.elements.modal.style.marginLeft="";n.elements.modal.style.marginRight="";n.isOpen()&&(t=0,i=0,n.elements.dialog.style.top!==""&&(t=parseInt(n.elements.dialog.style.top,10)),n.elements.dialog.style.top=t+(r-y())+"px",n.elements.dialog.style.left!==""&&(i=parseInt(n.elements.dialog.style.left,10)),n.elements.dialog.style.left=i+(u-p())+"px")}function ii(n){n.get("modal")||n.get("pinned")?tr(n):nr(n)}function tu(n){n.get("pinned")?(r(n.elements.root,c.unpinned),n.isOpen()&&tr(n)):(t(n.elements.root,c.unpinned),n.isOpen()&&!n.isModal()&&nr(n))}function iu(n){n.get("maximizable")?t(n.elements.root,c.maximizable):r(n.elements.root,c.maximizable)}function ru(n){n.get("closable")?(t(n.elements.root,c.closable),bu(n)):(r(n.elements.root,c.closable),ku(n))}function uu(n,t){if(n.timeStamp-ir>200&&(ir=n.timeStamp)&&!ri){var i=n.srcElement||n.target;return t.get("closableByDimmer")===!0&&i===t.elements.modal&&ti(t),ri=!1,!1}}function ct(n,t){var i,r,u;if(Date.now()-rr>200&&(rr=Date.now()))for(i=0;i<n.__internal.buttons.length;i+=1)if(r=n.__internal.buttons[i],!r.element.disabled&&t(r)){u=k(i,r);typeof n.callback=="function"&&n.callback.apply(n,[u]);u.cancel===!1&&n.close();break}}function fu(n,t){var i=n.srcElement||n.target;ct(t,function(n){return n.element===i&&(ft=!0)})}function ur(n){if(ft){ft=!1;return}var t=f[f.length-1],i=n.keyCode;return t.__internal.buttons.length===0&&i===s.ESC&&t.get("closable")===!0?(ti(t),!1):ot.indexOf(i)>-1?(ct(t,function(n){return n.key===i}),!1):void 0}function fr(n){var u=f[f.length-1],t=n.keyCode,i,r;if(t===s.LEFT||t===s.RIGHT){for(i=u.__internal.buttons,r=0;r<i.length;r+=1)if(document.activeElement===i[r].element)switch(t){case s.LEFT:i[(r||i.length)-1].element.focus();return;case s.RIGHT:i[(r+1)%i.length].element.focus();return}}else if(t<s.F12+1&&t>s.F1-1&&ot.indexOf(t)>-1)return n.preventDefault(),n.stopPropagation(),ct(u,function(n){return n.key===t}),!1}function ui(n,t){if(t)t.focus();else{var r=n.__internal.focus,i=r.element;switch(typeof r.element){case"number":n.__internal.buttons.length>r.element&&(i=n.get("basic")===!0?n.elements.reset[0]:n.__internal.buttons[r.element].element);break;case"string":i=n.elements.body.querySelector(r.element);break;case"function":i=r.element.call(n)}(typeof i=="undefined"||i===null)&&n.__internal.buttons.length===0&&(i=n.elements.reset[0]);i&&i.focus&&(i.focus(),r.select&&i.select&&i.select())}}function fi(n,t){var r,i,u,e;if(!t)for(r=f.length-1;r>-1;r-=1)if(f[r].isModal()){t=f[r];break}t&&t.isModal()&&(u=n.srcElement||n.target,e=u===t.elements.reset[1]||t.__internal.buttons.length===0&&u===document.body,e&&(t.get("maximizable")?i=t.elements.commands.maximize:t.get("closable")&&(i=t.elements.commands.close)),i===undefined&&(typeof t.__internal.focus.element=="number"?u===t.elements.reset[0]?i=t.elements.buttons.auxiliary.firstChild||t.elements.buttons.primary.firstChild:e&&(i=t.elements.reset[0]):u===t.elements.reset[0]&&(i=t.elements.buttons.primary.lastChild||t.elements.buttons.auxiliary.lastChild)),ui(t,i))}function eu(n,t){clearTimeout(t.__internal.timerIn);ui(t);vi();ft=!1;o("onfocus",t);e(t.elements.dialog,a.type,t.__internal.transitionInHandler);r(t.elements.root,c.animationIn)}function ou(n,t){clearTimeout(t.__internal.timerOut);e(t.elements.dialog,a.type,t.__internal.transitionOutHandler);pt(t);dt(t);t.isMaximized()&&!t.get("startMaximized")&&ht(t);i.defaults.maintainFocus&&t.__internal.activeElement&&(t.__internal.activeElement.focus(),t.__internal.activeElement=null);typeof t.__internal.destroy=="function"&&t.__internal.destroy.apply(t)}function su(n,t){var r=n[lt]-ei,i=n[at]-et;vt&&(i-=document.body.scrollTop);t.style.left=r+"px";t.style.top=i+"px"}function hu(n,t){var r=n[lt]-ei,i=n[at]-et;vt&&(i-=document.body.scrollTop);t.style.left=Math.min(nt.maxLeft,Math.max(nt.minLeft,r))+"px";t.style.top=vt?Math.min(nt.maxTop,Math.max(nt.minTop,i))+"px":Math.max(nt.minTop,i)+"px"}function cu(n,i){var u,f,e,r;if(g===null&&!i.isMaximized()&&i.get("movable")&&(f=0,e=0,n.type==="touchstart"?(n.preventDefault(),u=n.targetTouches[0],lt="clientX",at="clientY"):n.button===0&&(u=n),u)){if(r=i.elements.dialog,t(r,c.capture),r.style.left&&(f=parseInt(r.style.left,10)),r.style.top&&(e=parseInt(r.style.top,10)),ei=u[lt]-f,et=u[at]-e,i.isModal()?et+=i.elements.modal.scrollTop:i.isPinned()&&(et-=document.body.scrollTop),i.get("moveBounded")){var s=r,h=-f,l=-e;do h+=s.offsetLeft,l+=s.offsetTop;while(s=s.offsetParent);nt={maxLeft:h,minLeft:-h,maxTop:document.documentElement.clientHeight-r.clientHeight-l,minTop:-l};yt=hu}else nt=null,yt=su;return o("onmove",i),vt=!i.isModal()&&i.isPinned(),tt=i,yt(u,r),t(document.body,c.noSelection),!1}}function oi(n){if(tt){var t;n.type==="touchmove"?(n.preventDefault(),t=n.targetTouches[0]):n.button===0&&(t=n);t&&yt(t,tt.elements.dialog)}}function si(){if(tt){var n=tt;tt=nt=null;r(document.body,c.noSelection);r(n.elements.dialog,c.capture);o("onmoved",n)}}function pt(n){tt=null;var t=n.elements.dialog;t.style.left=t.style.top=""}function lu(n){n.get("movable")?(t(n.elements.root,c.movable),n.isOpen()&&hr(n)):(pt(n),r(n.elements.root,c.movable),n.isOpen()&&cr(n))}function au(n,t,i){var u=t,f=0,h=0,r,o,s,e;do f+=u.offsetLeft,h+=u.offsetTop;while(u=u.offsetParent);i===!0?(r=n.pageX,o=n.pageY):(r=n.clientX,o=n.clientY);s=w();s&&(r=document.body.offsetWidth-r,isNaN(rt)||(f=document.body.offsetWidth-f-t.offsetWidth));t.style.height=o-h+kt+"px";t.style.width=r-f+kt+"px";isNaN(rt)||(e=Math.abs(t.offsetWidth-wt)*.5,s&&(e*=-1),t.offsetWidth>wt?t.style.left=rt+e+"px":t.offsetWidth>=bt&&(t.style.left=rt-e+"px"))}function vu(n,i){var u,r;if(!i.isMaximized()&&(n.type==="touchstart"?(n.preventDefault(),u=n.targetTouches[0]):n.button===0&&(u=n),u))return o("onresize",i),g=i,kt=i.elements.resizeHandle.offsetHeight/2,r=i.elements.dialog,t(r,c.capture),rt=parseInt(r.style.left,10),r.style.height=r.offsetHeight+"px",r.style.minHeight=i.elements.header.offsetHeight+i.elements.footer.offsetHeight+"px",r.style.width=(wt=r.offsetWidth)+"px",r.style.maxWidth!=="none"&&(r.style.minWidth=(bt=r.offsetWidth)+"px"),r.style.maxWidth="none",t(document.body,c.noSelection),!1}function hi(n){if(g){var t;n.type==="touchmove"?(n.preventDefault(),t=n.targetTouches[0]):n.button===0&&(t=n);t&&au(t,g.elements.dialog,!g.get("modal")&&!g.get("pinned"))}}function ci(){if(g){var n=g;g=null;r(document.body,c.noSelection);r(n.elements.dialog,c.capture);ri=!0;o("onresized",n)}}function dt(n){g=null;var t=n.elements.dialog;t.style.maxWidth==="none"&&(t.style.maxWidth=t.style.minWidth=t.style.width=t.style.height=t.style.minHeight=t.style.left="",rt=Number.Nan,wt=bt=kt=0)}function yu(n){n.get("resizable")?(t(n.elements.root,c.resizable),n.isOpen()&&lr(n)):(dt(n),r(n.elements.root,c.resizable),n.isOpen()&&ar(n))}function er(){for(var t,n=0;n<f.length;n+=1)t=f[n],t.get("autoReset")&&(pt(t),dt(t))}function pu(t){f.length===1&&(u(n,"resize",er),u(document.body,"keyup",ur),u(document.body,"keydown",fr),u(document.body,"focus",fi),u(document.documentElement,"mousemove",oi),u(document.documentElement,"touchmove",oi),u(document.documentElement,"mouseup",si),u(document.documentElement,"touchend",si),u(document.documentElement,"mousemove",hi),u(document.documentElement,"touchmove",hi),u(document.documentElement,"mouseup",ci),u(document.documentElement,"touchend",ci));u(t.elements.commands.container,"click",t.__internal.commandsClickHandler);u(t.elements.footer,"click",t.__internal.buttonsClickHandler);u(t.elements.reset[0],"focus",t.__internal.resetHandler);u(t.elements.reset[1],"focus",t.__internal.resetHandler);ft=!0;u(t.elements.dialog,a.type,t.__internal.transitionInHandler);t.get("modal")||or(t);t.get("resizable")&&lr(t);t.get("movable")&&hr(t)}function wu(t){f.length===1&&(e(n,"resize",er),e(document.body,"keyup",ur),e(document.body,"keydown",fr),e(document.body,"focus",fi),e(document.documentElement,"mousemove",oi),e(document.documentElement,"mouseup",si),e(document.documentElement,"mousemove",hi),e(document.documentElement,"mouseup",ci));e(t.elements.commands.container,"click",t.__internal.commandsClickHandler);e(t.elements.footer,"click",t.__internal.buttonsClickHandler);e(t.elements.reset[0],"focus",t.__internal.resetHandler);e(t.elements.reset[1],"focus",t.__internal.resetHandler);u(t.elements.dialog,a.type,t.__internal.transitionOutHandler);t.get("modal")||sr(t);t.get("movable")&&cr(t);t.get("resizable")&&ar(t)}function or(n){u(n.elements.dialog,"focus",n.__internal.bringToFrontHandler,!0)}function sr(n){e(n.elements.dialog,"focus",n.__internal.bringToFrontHandler,!0)}function hr(n){u(n.elements.header,"mousedown",n.__internal.beginMoveHandler);u(n.elements.header,"touchstart",n.__internal.beginMoveHandler)}function cr(n){e(n.elements.header,"mousedown",n.__internal.beginMoveHandler);e(n.elements.header,"touchstart",n.__internal.beginMoveHandler)}function lr(n){u(n.elements.resizeHandle,"mousedown",n.__internal.beginResizeHandler);u(n.elements.resizeHandle,"touchstart",n.__internal.beginResizeHandler)}function ar(n){e(n.elements.resizeHandle,"mousedown",n.__internal.beginResizeHandler);e(n.elements.resizeHandle,"touchstart",n.__internal.beginResizeHandler)}function bu(n){u(n.elements.modal,"click",n.__internal.modalClickHandler)}function ku(n){e(n.elements.modal,"click",n.__internal.modalClickHandler)}var ot=[],gt=null,li=!1,vr=n.navigator.userAgent.indexOf("Safari")>-1&&n.navigator.userAgent.indexOf("Chrome")<0,d={dimmer:'<div class="ajs-dimmer"><\/div>',modal:'<div class="ajs-modal" tabindex="0"><\/div>',dialog:'<div class="ajs-dialog" tabindex="0"><\/div>',reset:'<button class="ajs-reset"><\/button>',commands:'<div class="ajs-commands"><button class="ajs-pin"><\/button><button class="ajs-maximize"><\/button><button class="ajs-close"><\/button><\/div>',header:'<div class="ajs-header"><\/div>',body:'<div class="ajs-body"><\/div>',content:'<div class="ajs-content"><\/div>',footer:'<div class="ajs-footer"><\/div>',buttons:{primary:'<div class="ajs-primary ajs-buttons"><\/div>',auxiliary:'<div class="ajs-auxiliary ajs-buttons"><\/div>'},button:'<button class="ajs-button"><\/button>',resizeHandle:'<div class="ajs-handle"><\/div>'},c={animationIn:"ajs-in",animationOut:"ajs-out",base:"alertify",basic:"ajs-basic",capture:"ajs-capture",closable:"ajs-closable",fixed:"ajs-fixed",frameless:"ajs-frameless",hidden:"ajs-hidden",maximize:"ajs-maximize",maximized:"ajs-maximized",maximizable:"ajs-maximizable",modeless:"ajs-modeless",movable:"ajs-movable",noSelection:"ajs-no-selection",noOverflow:"ajs-no-overflow",noPadding:"ajs-no-padding",pin:"ajs-pin",pinnable:"ajs-pinnable",prefix:"ajs-",resizable:"ajs-resizable",restore:"ajs-restore",shake:"ajs-shake",unpinned:"ajs-unpinned"},ai,ut,yi="",pi=0,ri=!1,ir=0,rr=0,ft=!1,tt=null,ei=0,et=0,lt="pageX",at="pageY",nt=null,vt=!1,yt=null,g=null,rt=Number.Nan,wt=0,bt=0,kt=0;return{__init:st,isOpen:function(){return this.__internal.isOpen},isModal:function(){return this.elements.root.className.indexOf(c.modeless)<0},isMaximized:function(){return this.elements.root.className.indexOf(c.maximized)>-1},isPinned:function(){return this.elements.root.className.indexOf(c.unpinned)<0},maximize:function(){return this.isMaximized()||gi(this),this},restore:function(){return this.isMaximized()&&ht(this),this},pin:function(){return this.isPinned()||ki(this),this},unpin:function(){return this.isPinned()&&di(this),this},bringToFront:function(){return bi(null,this),this},moveTo:function(n,t){var e,s;if(!isNaN(n)&&!isNaN(t)){o("onmove",this);var i=this.elements.dialog,r=i,u=0,f=0;i.style.left&&(u-=parseInt(i.style.left,10));i.style.top&&(f-=parseInt(i.style.top,10));do u+=r.offsetLeft,f+=r.offsetTop;while(r=r.offsetParent);e=n-u;s=t-f;w()&&(e*=-1);i.style.left=e+"px";i.style.top=s+"px";o("onmoved",this)}return this},resizeTo:function(n,t){var r=parseFloat(n),u=parseFloat(t),f=/(\d*\.\d+|\d+)%/,i;return isNaN(r)||isNaN(u)||this.get("resizable")!==!0||(o("onresize",this),(""+n).match(f)&&(r=r/100*document.documentElement.clientWidth),(""+t).match(f)&&(u=u/100*document.documentElement.clientHeight),i=this.elements.dialog,i.style.maxWidth!=="none"&&(i.style.minWidth=(bt=i.offsetWidth)+"px"),i.style.maxWidth="none",i.style.minHeight=this.elements.header.offsetHeight+this.elements.footer.offsetHeight+"px",i.style.width=r+"px",i.style.height=u+"px",o("onresized",this)),this},setting:function(n,t){var e=this,i=ni(this,this.__internal.options,function(n,t,i){dr(e,n,t,i)},n,t),f,r,u;if(i.op==="get")return i.found?i.value:typeof this.settings!="undefined"?ni(this,this.settings,this.settingUpdated||function(){},n,t).value:undefined;if(i.op==="set"){if(i.items.length>0)for(f=this.settingUpdated||function(){},r=0;r<i.items.length;r+=1)u=i.items[r],u.found||typeof this.settings=="undefined"||ni(this,this.settings,f,u.key,u.value);return this}},set:function(n,t){return this.setting(n,t),this},get:function(n){return this.setting(n)},setHeader:function(t){return typeof t=="string"?(l(this.elements.header),this.elements.header.innerHTML=t):t instanceof n.HTMLElement&&this.elements.header.firstChild!==t&&(l(this.elements.header),this.elements.header.appendChild(t)),this},setContent:function(t){return typeof t=="string"?(l(this.elements.content),this.elements.content.innerHTML=t):t instanceof n.HTMLElement&&this.elements.content.firstChild!==t&&(l(this.elements.content),this.elements.content.appendChild(t)),this},showModal:function(n){return this.show(!0,n)},show:function(n,u){var e,s;return st(this),this.__internal.isOpen?(pt(this),dt(this),t(this.elements.dialog,c.shake),s=this,setTimeout(function(){r(s.elements.dialog,c.shake)},200)):(this.__internal.isOpen=!0,f.push(this),i.defaults.maintainFocus&&(this.__internal.activeElement=document.activeElement),document.body.hasAttribute("tabindex")||document.body.setAttribute("tabindex",li="0"),typeof this.prepare=="function"&&this.prepare(),pu(this),n!==undefined&&this.set("modal",n),yr(),it(),typeof u=="string"&&u!==""&&(this.__internal.className=u,t(this.elements.root,u)),this.get("startMaximized")?this.maximize():this.isMaximized()&&ht(this),ii(this),r(this.elements.root,c.animationOut),t(this.elements.root,c.animationIn),clearTimeout(this.__internal.timerIn),this.__internal.timerIn=setTimeout(this.__internal.transitionInHandler,a.supported?1e3:100),vr&&(e=this.elements.root,e.style.display="none",setTimeout(function(){e.style.display="block"},0)),gt=this.elements.root.offsetWidth,r(this.elements.root,c.hidden),typeof this.hooks.onshow=="function"&&this.hooks.onshow.call(this),o("onshow",this)),this},close:function(){return this.__internal.isOpen&&o("onclosing",this)!==!1&&(wu(this),r(this.elements.root,c.animationIn),t(this.elements.root,c.animationOut),clearTimeout(this.__internal.timerOut),this.__internal.timerOut=setTimeout(this.__internal.transitionOutHandler,a.supported?1e3:100),t(this.elements.root,c.hidden),gt=this.elements.modal.offsetWidth,typeof this.__internal.className!="undefined"&&this.__internal.className!==""&&r(this.elements.root,this.__internal.className),typeof this.hooks.onclose=="function"&&this.hooks.onclose.call(this),o("onclose",this),f.splice(f.indexOf(this),1),this.__internal.isOpen=!1,it()),f.length||li!=="0"||document.body.removeAttribute("tabindex"),this},closeOthers:function(){return i.closeAll(this),this},destroy:function(){return this.__internal&&(this.__internal.isOpen?(this.__internal.destroy=function(){b(this,st)},this.close()):this.__internal.destroy||b(this,st)),this}}}(),c=function(){function v(n){n.__internal||(n.__internal={position:i.defaults.notifier.position,delay:i.defaults.notifier.delay},o=document.createElement("DIV"),y(n));o.parentNode!==document.body&&document.body.appendChild(o)}function w(n){n.__internal.pushed=!0;s.push(n)}function b(n){s.splice(s.indexOf(n),1);n.__internal.pushed=!1}function y(n){o.className=f.base;switch(n.__internal.position){case"top-right":t(o,f.top+" "+f.right);break;case"top-left":t(o,f.top+" "+f.left);break;case"top-center":t(o,f.top+" "+f.center);break;case"bottom-left":t(o,f.bottom+" "+f.left);break;case"bottom-center":t(o,f.bottom+" "+f.center);break;default:case"bottom-right":t(o,f.bottom+" "+f.right)}}function k(s,v){function d(n,t){t.__internal.closeButton&&n.target.getAttribute("data-close")!=="true"||t.dismiss(!0)}function k(n,t){e(t.element,a.type,k);o.removeChild(t.element)}function g(n){return n.__internal||(n.__internal={pushed:!1,delay:undefined,timer:undefined,clickHandler:undefined,transitionEndHandler:undefined,transitionTimeout:undefined},n.__internal.clickHandler=h(n,d),n.__internal.transitionEndHandler=h(n,k)),n}function y(n){clearTimeout(n.__internal.timer);clearTimeout(n.__internal.transitionTimeout)}return g({element:s,push:function(n,r){if(!this.__internal.pushed){w(this);y(this);var s,e;switch(arguments.length){case 0:e=this.__internal.delay;break;case 1:typeof n=="number"?e=n:(s=n,e=this.__internal.delay);break;case 2:s=n;e=r}return this.__internal.closeButton=i.defaults.notifier.closeButton,typeof s!="undefined"&&this.setContent(s),c.__internal.position.indexOf("top")<0?o.appendChild(this.element):o.insertBefore(this.element,o.firstChild),p=this.element.offsetWidth,t(this.element,f.visible),u(this.element,"click",this.__internal.clickHandler),this.delay(e)}return this},ondismiss:function(){},callback:v,dismiss:function(n){return this.__internal.pushed&&(y(this),typeof this.ondismiss=="function"&&this.ondismiss.call(this)===!1||(e(this.element,"click",this.__internal.clickHandler),typeof this.element!="undefined"&&this.element.parentNode===o&&(this.__internal.transitionTimeout=setTimeout(this.__internal.transitionEndHandler,a.supported?1e3:100),r(this.element,f.visible),typeof this.callback=="function"&&this.callback.call(this,n)),b(this))),this},delay:function(n){if(y(this),this.__internal.delay=typeof n!="undefined"&&!isNaN(+n)?+n:c.__internal.delay,this.__internal.delay>0){var t=this;this.__internal.timer=setTimeout(function(){t.dismiss()},this.__internal.delay*1e3)}return this},setContent:function(i){if(typeof i=="string"?(l(this.element),this.element.innerHTML=i):i instanceof n.HTMLElement&&this.element.firstChild!==i&&(l(this.element),this.element.appendChild(i)),this.__internal.closeButton){var r=document.createElement("span");t(r,f.close);r.setAttribute("data-close",!0);this.element.appendChild(r)}return this},dismissOthers:function(){return c.dismissAll(this),this}})}var p,o,s=[],f={base:"alertify-notifier",message:"ajs-message",top:"ajs-top",right:"ajs-right",bottom:"ajs-bottom",left:"ajs-left",center:"ajs-center",visible:"ajs-visible",hidden:"ajs-hidden",close:"ajs-close"};return{setting:function(n,t){if(v(this),typeof t=="undefined")return this.__internal[n];switch(n){case"position":this.__internal.position=t;y(this);break;case"delay":this.__internal.delay=t}return this},set:function(n,t){return this.setting(n,t),this},get:function(n){return this.setting(n)},create:function(n,t){v(this);var i=document.createElement("div");return i.className=f.message+(typeof n=="string"&&n!==""?" ajs-"+n:""),k(i,t)},dismissAll:function(n){for(var i,r=s.slice(0),t=0;t<r.length;t+=1)i=r[t],(n===undefined||n!==i)&&i.dismiss()}}}(),i=new nt;i.dialog("alert",function(){return{main:function(n,t,i){var u,r,f;switch(arguments.length){case 1:r=n;break;case 2:typeof t=="function"?(r=n,f=t):(u=n,r=t);break;case 3:u=n;r=t;f=i}return this.set("title",u),this.set("message",r),this.set("onok",f),this},setup:function(){return{buttons:[{text:i.defaults.glossary.ok,key:s.ESC,invokeOnClose:!0,className:i.defaults.theme.ok}],focus:{element:0,select:!1},options:{maximizable:!1,resizable:!1}}},build:function(){},prepare:function(){},setMessage:function(n){this.setContent(n)},settings:{message:undefined,onok:undefined,label:undefined},settingUpdated:function(n,t,i){switch(n){case"message":this.setMessage(i);break;case"label":this.__internal.buttons[0].element&&(this.__internal.buttons[0].element.innerHTML=i)}},callback:function(n){if(typeof this.get("onok")=="function"){var t=this.get("onok").call(this,n);typeof t!="undefined"&&(n.cancel=!t)}}}});i.dialog("confirm",function(){function t(t){n.timer!==null&&(clearInterval(n.timer),n.timer=null,t.__internal.buttons[n.index].element.innerHTML=n.text)}function r(i,r,u){t(i);n.duration=u;n.index=r;n.text=i.__internal.buttons[r].element.innerHTML;n.timer=setInterval(h(i,n.task),1e3);n.task(null,i)}var n={timer:null,index:null,text:null,duration:null,task:function(i,r){if(r.isOpen()){if(r.__internal.buttons[n.index].element.innerHTML=n.text+" (&#8207;"+n.duration+"&#8207;) ",n.duration-=1,n.duration===-1){t(r);var f=r.__internal.buttons[n.index],u=k(n.index,f);typeof r.callback=="function"&&r.callback.apply(r,[u]);u.close!==!1&&r.close()}}else t(r)}};return{main:function(n,t,i,r){var o,u,f,e;switch(arguments.length){case 1:u=n;break;case 2:u=n;f=t;break;case 3:u=n;f=t;e=i;break;case 4:o=n;u=t;f=i;e=r}return this.set("title",o),this.set("message",u),this.set("onok",f),this.set("oncancel",e),this},setup:function(){return{buttons:[{text:i.defaults.glossary.ok,key:s.ENTER,className:i.defaults.theme.ok},{text:i.defaults.glossary.cancel,key:s.ESC,invokeOnClose:!0,className:i.defaults.theme.cancel}],focus:{element:0,select:!1},options:{maximizable:!1,resizable:!1}}},build:function(){},prepare:function(){},setMessage:function(n){this.setContent(n)},settings:{message:null,labels:null,onok:null,oncancel:null,defaultFocus:null,reverseButtons:null},settingUpdated:function(n,t,i){switch(n){case"message":this.setMessage(i);break;case"labels":"ok"in i&&this.__internal.buttons[0].element&&(this.__internal.buttons[0].text=i.ok,this.__internal.buttons[0].element.innerHTML=i.ok);"cancel"in i&&this.__internal.buttons[1].element&&(this.__internal.buttons[1].text=i.cancel,this.__internal.buttons[1].element.innerHTML=i.cancel);break;case"reverseButtons":i===!0?this.elements.buttons.primary.appendChild(this.__internal.buttons[0].element):this.elements.buttons.primary.appendChild(this.__internal.buttons[1].element);break;case"defaultFocus":this.__internal.focus.element=i==="ok"?0:1}},callback:function(n){t(this);var i;switch(n.index){case 0:typeof this.get("onok")=="function"&&(i=this.get("onok").call(this,n),typeof i!="undefined"&&(n.cancel=!i));break;case 1:typeof this.get("oncancel")=="function"&&(i=this.get("oncancel").call(this,n),typeof i!="undefined"&&(n.cancel=!i))}},autoOk:function(n){return r(this,0,n),this},autoCancel:function(n){return r(this,1,n),this}}});i.dialog("prompt",function(){var t=document.createElement("INPUT"),r=document.createElement("P");return{main:function(n,t,i,r,u){var h,f,e,o,s;switch(arguments.length){case 1:f=n;break;case 2:f=n;e=t;break;case 3:f=n;e=t;o=i;break;case 4:f=n;e=t;o=i;s=r;break;case 5:h=n;f=t;e=i;o=r;s=u}return this.set("title",h),this.set("message",f),this.set("value",e),this.set("onok",o),this.set("oncancel",s),this},setup:function(){return{buttons:[{text:i.defaults.glossary.ok,key:s.ENTER,className:i.defaults.theme.ok},{text:i.defaults.glossary.cancel,key:s.ESC,invokeOnClose:!0,className:i.defaults.theme.cancel}],focus:{element:t,select:!0},options:{maximizable:!1,resizable:!1}}},build:function(){t.className=i.defaults.theme.input;t.setAttribute("type","text");t.value=this.get("value");this.elements.content.appendChild(r);this.elements.content.appendChild(t)},prepare:function(){},setMessage:function(t){typeof t=="string"?(l(r),r.innerHTML=t):t instanceof n.HTMLElement&&r.firstChild!==t&&(l(r),r.appendChild(t))},settings:{message:undefined,labels:undefined,onok:undefined,oncancel:undefined,value:"",type:"text",reverseButtons:undefined},settingUpdated:function(n,i,r){switch(n){case"message":this.setMessage(r);break;case"value":t.value=r;break;case"type":switch(r){case"text":case"color":case"date":case"datetime-local":case"email":case"month":case"number":case"password":case"search":case"tel":case"time":case"week":t.type=r;break;default:t.type="text"}break;case"labels":r.ok&&this.__internal.buttons[0].element&&(this.__internal.buttons[0].element.innerHTML=r.ok);r.cancel&&this.__internal.buttons[1].element&&(this.__internal.buttons[1].element.innerHTML=r.cancel);break;case"reverseButtons":r===!0?this.elements.buttons.primary.appendChild(this.__internal.buttons[0].element):this.elements.buttons.primary.appendChild(this.__internal.buttons[1].element)}},callback:function(n){var i;switch(n.index){case 0:this.settings.value=t.value;typeof this.get("onok")=="function"&&(i=this.get("onok").call(this,n,this.settings.value),typeof i!="undefined"&&(n.cancel=!i));break;case 1:typeof this.get("oncancel")=="function"&&(i=this.get("oncancel").call(this,n),typeof i!="undefined"&&(n.cancel=!i));n.cancel||(t.value=this.settings.value)}}}});typeof module=="object"&&typeof module.exports=="object"?module.exports=i:typeof define=="function"&&define.amd?define([],function(){return i}):n.alertify||(n.alertify=i)}(typeof window!="undefined"?window:this);!function(n){"undefined"!=typeof jQuery&&jQuery||"function"!=typeof define||!define.amd?"undefined"!=typeof jQuery&&jQuery||"object"!=typeof exports?n(jQuery,document,window,navigator):n(require("jquery"),document,window,navigator):define(["jquery"],function(t){return n(t,document,window,navigator)})}(function(n,t,i,r,u){"use strict";function s(r,f,e){this.VERSION="2.3.1";this.input=r;this.plugin_count=e;this.current_plugin=0;this.calc_count=0;this.update_tm=0;this.old_from=0;this.old_to=0;this.old_min_interval=null;this.raf_id=null;this.dragging=!1;this.force_redraw=!1;this.no_diapason=!1;this.has_tab_index=!0;this.is_key=!1;this.is_update=!1;this.is_start=!0;this.is_finish=!1;this.is_active=!1;this.is_resize=!1;this.is_click=!1;f=f||{};this.$cache={win:n(i),body:n(t.body),input:n(r),cont:null,rs:null,min:null,max:null,from:null,to:null,single:null,bar:null,line:null,s_single:null,s_from:null,s_to:null,shad_single:null,shad_from:null,shad_to:null,edge:null,grid:null,grid_labels:[]};this.coords={x_gap:0,x_pointer:0,w_rs:0,w_rs_old:0,w_handle:0,p_gap:0,p_gap_left:0,p_gap_right:0,p_step:0,p_pointer:0,p_handle:0,p_single_fake:0,p_single_real:0,p_from_fake:0,p_from_real:0,p_to_fake:0,p_to_real:0,p_bar_x:0,p_bar_w:0,grid_gap:0,big_num:0,big:[],big_w:[],big_p:[],big_x:[]};this.labels={w_min:0,w_max:0,w_from:0,w_to:0,w_single:0,p_min:0,p_max:0,p_from_fake:0,p_from_left:0,p_to_fake:0,p_to_left:0,p_single_fake:0,p_single_left:0};var c,h,l,o=this.$cache.input,s=o.prop("value");for(l in c={skin:"flat",type:"single",min:10,max:100,from:null,to:null,step:1,min_interval:0,max_interval:0,drag_interval:!1,values:[],p_values:[],from_fixed:!1,from_min:null,from_max:null,from_shadow:!1,to_fixed:!1,to_min:null,to_max:null,to_shadow:!1,prettify_enabled:!0,prettify_separator:" ",prettify:null,force_edges:!1,keyboard:!0,grid:!1,grid_margin:!0,grid_num:4,grid_snap:!1,hide_min_max:!1,hide_from_to:!1,prefix:"",postfix:"",max_postfix:"",decorate_both:!0,values_separator:" — ",input_values_separator:";",disable:!1,block:!1,extra_classes:"",scope:null,onStart:null,onChange:null,onFinish:null,onUpdate:null},"INPUT"!==o[0].nodeName&&console&&console.warn&&console.warn("Base element should be <input>!",o[0]),(h={skin:o.data("skin"),type:o.data("type"),min:o.data("min"),max:o.data("max"),from:o.data("from"),to:o.data("to"),step:o.data("step"),min_interval:o.data("minInterval"),max_interval:o.data("maxInterval"),drag_interval:o.data("dragInterval"),values:o.data("values"),from_fixed:o.data("fromFixed"),from_min:o.data("fromMin"),from_max:o.data("fromMax"),from_shadow:o.data("fromShadow"),to_fixed:o.data("toFixed"),to_min:o.data("toMin"),to_max:o.data("toMax"),to_shadow:o.data("toShadow"),prettify_enabled:o.data("prettifyEnabled"),prettify_separator:o.data("prettifySeparator"),force_edges:o.data("forceEdges"),keyboard:o.data("keyboard"),grid:o.data("grid"),grid_margin:o.data("gridMargin"),grid_num:o.data("gridNum"),grid_snap:o.data("gridSnap"),hide_min_max:o.data("hideMinMax"),hide_from_to:o.data("hideFromTo"),prefix:o.data("prefix"),postfix:o.data("postfix"),max_postfix:o.data("maxPostfix"),decorate_both:o.data("decorateBoth"),values_separator:o.data("valuesSeparator"),input_values_separator:o.data("inputValuesSeparator"),disable:o.data("disable"),block:o.data("block"),extra_classes:o.data("extraClasses")}).values=h.values&&h.values.split(","),h)h.hasOwnProperty(l)&&(h[l]!==u&&""!==h[l]||delete h[l]);s!==u&&""!==s&&((s=s.split(h.input_values_separator||f.input_values_separator||";"))[0]&&s[0]==+s[0]&&(s[0]=+s[0]),s[1]&&s[1]==+s[1]&&(s[1]=+s[1]),f&&f.values&&f.values.length?(c.from=s[0]&&f.values.indexOf(s[0]),c.to=s[1]&&f.values.indexOf(s[1])):(c.from=s[0]&&+s[0],c.to=s[1]&&+s[1]));n.extend(c,f);n.extend(c,h);this.options=c;this.update_check={};this.validate();this.result={input:this.$cache.input,slider:null,min:this.options.min,max:this.options.max,from:this.options.from,from_percent:0,from_value:null,to:this.options.to,to_percent:0,to_value:null};this.init()}var e,o,h=0,f=(e=r.userAgent,o=/msie\s\d+/i,0<e.search(o)&&o.exec(e).toString().split(" ")[1]<9&&(n("html").addClass("lt-ie9"),!0));Function.prototype.bind||(Function.prototype.bind=function(n){var t=this,i=[].slice,r,u;if("function"!=typeof t)throw new TypeError;return r=i.call(arguments,1),u=function(){var e,o,f;return this instanceof u?(e=function(){},e.prototype=t.prototype,o=new e,f=t.apply(o,r.concat(i.call(arguments))),Object(f)===f?f:o):t.apply(n,r.concat(i.call(arguments)))},u});Array.prototype.indexOf||(Array.prototype.indexOf=function(n,t){var r,f,u,i;if(null==this)throw new TypeError('"this" is null or not defined');if((f=Object(this),u=f.length>>>0,0==u)||(i=+t||0,Math.abs(i)===1/0&&(i=0),u<=i))return-1;for(r=Math.max(0<=i?i:u-Math.abs(i),0);r<u;){if(r in f&&f[r]===n)return r;r++}return-1});s.prototype={init:function(n){this.no_diapason=!1;this.coords.p_step=this.convertToPercent(this.options.step,!0);this.target="base";this.toggleInput();this.append();this.setMinMax();n?(this.force_redraw=!0,this.calc(!0),this.callOnUpdate()):(this.force_redraw=!0,this.calc(!0),this.callOnStart());this.updateScene()},append:function(){var n='<span class="irs irs--'+this.options.skin+" js-irs-"+this.plugin_count+" "+this.options.extra_classes+'"><\/span>';this.$cache.input.before(n);this.$cache.input.prop("readonly",!0);this.$cache.cont=this.$cache.input.prev();this.result.slider=this.$cache.cont;this.$cache.cont.html('<span class="irs"><span class="irs-line" tabindex="0"><\/span><span class="irs-min">0<\/span><span class="irs-max">1<\/span><span class="irs-from">0<\/span><span class="irs-to">0<\/span><span class="irs-single">0<\/span><\/span><span class="irs-grid"><\/span>');this.$cache.rs=this.$cache.cont.find(".irs");this.$cache.min=this.$cache.cont.find(".irs-min");this.$cache.max=this.$cache.cont.find(".irs-max");this.$cache.from=this.$cache.cont.find(".irs-from");this.$cache.to=this.$cache.cont.find(".irs-to");this.$cache.single=this.$cache.cont.find(".irs-single");this.$cache.line=this.$cache.cont.find(".irs-line");this.$cache.grid=this.$cache.cont.find(".irs-grid");"single"===this.options.type?(this.$cache.cont.append('<span class="irs-bar irs-bar--single"><\/span><span class="irs-shadow shadow-single"><\/span><span class="irs-handle single"><i><\/i><i><\/i><i><\/i><\/span>'),this.$cache.bar=this.$cache.cont.find(".irs-bar"),this.$cache.edge=this.$cache.cont.find(".irs-bar-edge"),this.$cache.s_single=this.$cache.cont.find(".single"),this.$cache.from[0].style.visibility="hidden",this.$cache.to[0].style.visibility="hidden",this.$cache.shad_single=this.$cache.cont.find(".shadow-single")):(this.$cache.cont.append('<span class="irs-bar"><\/span><span class="irs-shadow shadow-from"><\/span><span class="irs-shadow shadow-to"><\/span><span class="irs-handle from"><i><\/i><i><\/i><i><\/i><\/span><span class="irs-handle to"><i><\/i><i><\/i><i><\/i><\/span>'),this.$cache.bar=this.$cache.cont.find(".irs-bar"),this.$cache.s_from=this.$cache.cont.find(".from"),this.$cache.s_to=this.$cache.cont.find(".to"),this.$cache.shad_from=this.$cache.cont.find(".shadow-from"),this.$cache.shad_to=this.$cache.cont.find(".shadow-to"),this.setTopHandler());this.options.hide_from_to&&(this.$cache.from[0].style.display="none",this.$cache.to[0].style.display="none",this.$cache.single[0].style.display="none");this.appendGrid();this.options.disable?(this.appendDisableMask(),this.$cache.input[0].disabled=!0):(this.$cache.input[0].disabled=!1,this.removeDisableMask(),this.bindEvents());this.options.disable||(this.options.block?this.appendDisableMask():this.removeDisableMask());this.options.drag_interval&&(this.$cache.bar[0].style.cursor="ew-resize")},setTopHandler:function(){var i=this.options.min,n=this.options.max,r=this.options.from,t=this.options.to;i<r&&t===n?this.$cache.s_from.addClass("type_last"):t<n&&this.$cache.s_to.addClass("type_last")},changeLevel:function(n){switch(n){case"single":this.coords.p_gap=this.toFixed(this.coords.p_pointer-this.coords.p_single_fake);this.$cache.s_single.addClass("state_hover");break;case"from":this.coords.p_gap=this.toFixed(this.coords.p_pointer-this.coords.p_from_fake);this.$cache.s_from.addClass("state_hover");this.$cache.s_from.addClass("type_last");this.$cache.s_to.removeClass("type_last");break;case"to":this.coords.p_gap=this.toFixed(this.coords.p_pointer-this.coords.p_to_fake);this.$cache.s_to.addClass("state_hover");this.$cache.s_to.addClass("type_last");this.$cache.s_from.removeClass("type_last");break;case"both":this.coords.p_gap_left=this.toFixed(this.coords.p_pointer-this.coords.p_from_fake);this.coords.p_gap_right=this.toFixed(this.coords.p_to_fake-this.coords.p_pointer);this.$cache.s_to.removeClass("type_last");this.$cache.s_from.removeClass("type_last")}},appendDisableMask:function(){this.$cache.cont.append('<span class="irs-disable-mask"><\/span>');this.$cache.cont.addClass("irs-disabled")},removeDisableMask:function(){this.$cache.cont.remove(".irs-disable-mask");this.$cache.cont.removeClass("irs-disabled")},remove:function(){this.$cache.cont.remove();this.$cache.cont=null;this.$cache.line.off("keydown.irs_"+this.plugin_count);this.$cache.body.off("touchmove.irs_"+this.plugin_count);this.$cache.body.off("mousemove.irs_"+this.plugin_count);this.$cache.win.off("touchend.irs_"+this.plugin_count);this.$cache.win.off("mouseup.irs_"+this.plugin_count);f&&(this.$cache.body.off("mouseup.irs_"+this.plugin_count),this.$cache.body.off("mouseleave.irs_"+this.plugin_count));this.$cache.grid_labels=[];this.coords.big=[];this.coords.big_w=[];this.coords.big_p=[];this.coords.big_x=[];cancelAnimationFrame(this.raf_id)},bindEvents:function(){this.no_diapason||(this.$cache.body.on("touchmove.irs_"+this.plugin_count,this.pointerMove.bind(this)),this.$cache.body.on("mousemove.irs_"+this.plugin_count,this.pointerMove.bind(this)),this.$cache.win.on("touchend.irs_"+this.plugin_count,this.pointerUp.bind(this)),this.$cache.win.on("mouseup.irs_"+this.plugin_count,this.pointerUp.bind(this)),this.$cache.line.on("touchstart.irs_"+this.plugin_count,this.pointerClick.bind(this,"click")),this.$cache.line.on("mousedown.irs_"+this.plugin_count,this.pointerClick.bind(this,"click")),this.$cache.line.on("focus.irs_"+this.plugin_count,this.pointerFocus.bind(this)),this.options.drag_interval&&"double"===this.options.type?(this.$cache.bar.on("touchstart.irs_"+this.plugin_count,this.pointerDown.bind(this,"both")),this.$cache.bar.on("mousedown.irs_"+this.plugin_count,this.pointerDown.bind(this,"both"))):(this.$cache.bar.on("touchstart.irs_"+this.plugin_count,this.pointerClick.bind(this,"click")),this.$cache.bar.on("mousedown.irs_"+this.plugin_count,this.pointerClick.bind(this,"click"))),"single"===this.options.type?(this.$cache.single.on("touchstart.irs_"+this.plugin_count,this.pointerDown.bind(this,"single")),this.$cache.s_single.on("touchstart.irs_"+this.plugin_count,this.pointerDown.bind(this,"single")),this.$cache.shad_single.on("touchstart.irs_"+this.plugin_count,this.pointerClick.bind(this,"click")),this.$cache.single.on("mousedown.irs_"+this.plugin_count,this.pointerDown.bind(this,"single")),this.$cache.s_single.on("mousedown.irs_"+this.plugin_count,this.pointerDown.bind(this,"single")),this.$cache.edge.on("mousedown.irs_"+this.plugin_count,this.pointerClick.bind(this,"click")),this.$cache.shad_single.on("mousedown.irs_"+this.plugin_count,this.pointerClick.bind(this,"click"))):(this.$cache.single.on("touchstart.irs_"+this.plugin_count,this.pointerDown.bind(this,null)),this.$cache.single.on("mousedown.irs_"+this.plugin_count,this.pointerDown.bind(this,null)),this.$cache.from.on("touchstart.irs_"+this.plugin_count,this.pointerDown.bind(this,"from")),this.$cache.s_from.on("touchstart.irs_"+this.plugin_count,this.pointerDown.bind(this,"from")),this.$cache.to.on("touchstart.irs_"+this.plugin_count,this.pointerDown.bind(this,"to")),this.$cache.s_to.on("touchstart.irs_"+this.plugin_count,this.pointerDown.bind(this,"to")),this.$cache.shad_from.on("touchstart.irs_"+this.plugin_count,this.pointerClick.bind(this,"click")),this.$cache.shad_to.on("touchstart.irs_"+this.plugin_count,this.pointerClick.bind(this,"click")),this.$cache.from.on("mousedown.irs_"+this.plugin_count,this.pointerDown.bind(this,"from")),this.$cache.s_from.on("mousedown.irs_"+this.plugin_count,this.pointerDown.bind(this,"from")),this.$cache.to.on("mousedown.irs_"+this.plugin_count,this.pointerDown.bind(this,"to")),this.$cache.s_to.on("mousedown.irs_"+this.plugin_count,this.pointerDown.bind(this,"to")),this.$cache.shad_from.on("mousedown.irs_"+this.plugin_count,this.pointerClick.bind(this,"click")),this.$cache.shad_to.on("mousedown.irs_"+this.plugin_count,this.pointerClick.bind(this,"click"))),this.options.keyboard&&this.$cache.line.on("keydown.irs_"+this.plugin_count,this.key.bind(this,"keyboard")),f&&(this.$cache.body.on("mouseup.irs_"+this.plugin_count,this.pointerUp.bind(this)),this.$cache.body.on("mouseleave.irs_"+this.plugin_count,this.pointerUp.bind(this))))},pointerFocus:function(){var n,t;this.target||(n=(t="single"===this.options.type?this.$cache.single:this.$cache.from).offset().left,n+=t.width()/2-1,this.pointerClick("single",{preventDefault:function(){},pageX:n}))},pointerMove:function(n){if(this.dragging){var t=n.pageX||n.originalEvent.touches&&n.originalEvent.touches[0].pageX;this.coords.x_pointer=t-this.coords.x_gap;this.calc()}},pointerUp:function(t){this.current_plugin===this.plugin_count&&this.is_active&&(this.is_active=!1,this.$cache.cont.find(".state_hover").removeClass("state_hover"),this.force_redraw=!0,f&&n("*").prop("unselectable",!1),this.updateScene(),this.restoreOriginalMinInterval(),(n.contains(this.$cache.cont[0],t.target)||this.dragging)&&this.callOnFinish(),this.dragging=!1)},pointerDown:function(t,i){i.preventDefault();var r=i.pageX||i.originalEvent.touches&&i.originalEvent.touches[0].pageX;2!==i.button&&("both"===t&&this.setTempMinInterval(),t=t||this.target||"from",this.current_plugin=this.plugin_count,this.target=t,this.is_active=!0,this.dragging=!0,this.coords.x_gap=this.$cache.rs.offset().left,this.coords.x_pointer=r-this.coords.x_gap,this.calcPointerPercent(),this.changeLevel(t),f&&n("*").prop("unselectable",!0),this.$cache.line.trigger("focus"),this.updateScene())},pointerClick:function(n,t){t.preventDefault();var i=t.pageX||t.originalEvent.touches&&t.originalEvent.touches[0].pageX;2!==t.button&&(this.current_plugin=this.plugin_count,this.target=n,this.is_click=!0,this.coords.x_gap=this.$cache.rs.offset().left,this.coords.x_pointer=+(i-this.coords.x_gap).toFixed(),this.force_redraw=!0,this.calc(),this.$cache.line.trigger("focus"))},key:function(n,t){if(!(this.current_plugin!==this.plugin_count||t.altKey||t.ctrlKey||t.shiftKey||t.metaKey)){switch(t.which){case 83:case 65:case 40:case 37:t.preventDefault();this.moveByKey(!1);break;case 87:case 68:case 38:case 39:t.preventDefault();this.moveByKey(!0)}return!0}},moveByKey:function(n){var i=this.coords.p_pointer,t=(this.options.max-this.options.min)/100;t=this.options.step/t;n?i+=t:i-=t;this.coords.x_pointer=this.toFixed(this.coords.w_rs/100*i);this.is_key=!0;this.calc()},setMinMax:function(){if(this.options){if(this.options.hide_min_max)return this.$cache.min[0].style.display="none",void(this.$cache.max[0].style.display="none");if(this.options.values.length)this.$cache.min.html(this.decorate(this.options.p_values[this.options.min])),this.$cache.max.html(this.decorate(this.options.p_values[this.options.max]));else{var n=this._prettify(this.options.min),t=this._prettify(this.options.max);this.result.min_pretty=n;this.result.max_pretty=t;this.$cache.min.html(this.decorate(n,this.options.min));this.$cache.max.html(this.decorate(t,this.options.max))}this.labels.w_min=this.$cache.min.outerWidth(!1);this.labels.w_max=this.$cache.max.outerWidth(!1)}},setTempMinInterval:function(){var n=this.result.to-this.result.from;null===this.old_min_interval&&(this.old_min_interval=this.options.min_interval);this.options.min_interval=n},restoreOriginalMinInterval:function(){null!==this.old_min_interval&&(this.options.min_interval=this.old_min_interval,this.old_min_interval=null)},calc:function(n){var t;if(this.options&&(this.calc_count++,10!==this.calc_count&&!n||(this.calc_count=0,this.coords.w_rs=this.$cache.rs.outerWidth(!1),this.calcHandlePercent()),this.coords.w_rs)){this.calcPointerPercent();t=this.getHandleX();switch("both"===this.target&&(this.coords.p_gap=0,t=this.getHandleX()),"click"===this.target&&(this.coords.p_gap=this.coords.p_handle/2,t=this.getHandleX(),this.target=this.options.drag_interval?"both_one":this.chooseHandle(t)),this.target){case"base":var f=(this.options.max-this.options.min)/100,e=(this.result.from-this.options.min)/f,h=(this.result.to-this.options.min)/f;this.coords.p_single_real=this.toFixed(e);this.coords.p_from_real=this.toFixed(e);this.coords.p_to_real=this.toFixed(h);this.coords.p_single_real=this.checkDiapason(this.coords.p_single_real,this.options.from_min,this.options.from_max);this.coords.p_from_real=this.checkDiapason(this.coords.p_from_real,this.options.from_min,this.options.from_max);this.coords.p_to_real=this.checkDiapason(this.coords.p_to_real,this.options.to_min,this.options.to_max);this.coords.p_single_fake=this.convertToFakePercent(this.coords.p_single_real);this.coords.p_from_fake=this.convertToFakePercent(this.coords.p_from_real);this.coords.p_to_fake=this.convertToFakePercent(this.coords.p_to_real);this.target=null;break;case"single":if(this.options.from_fixed)break;this.coords.p_single_real=this.convertToRealPercent(t);this.coords.p_single_real=this.calcWithStep(this.coords.p_single_real);this.coords.p_single_real=this.checkDiapason(this.coords.p_single_real,this.options.from_min,this.options.from_max);this.coords.p_single_fake=this.convertToFakePercent(this.coords.p_single_real);break;case"from":if(this.options.from_fixed)break;this.coords.p_from_real=this.convertToRealPercent(t);this.coords.p_from_real=this.calcWithStep(this.coords.p_from_real);this.coords.p_from_real>this.coords.p_to_real&&(this.coords.p_from_real=this.coords.p_to_real);this.coords.p_from_real=this.checkDiapason(this.coords.p_from_real,this.options.from_min,this.options.from_max);this.coords.p_from_real=this.checkMinInterval(this.coords.p_from_real,this.coords.p_to_real,"from");this.coords.p_from_real=this.checkMaxInterval(this.coords.p_from_real,this.coords.p_to_real,"from");this.coords.p_from_fake=this.convertToFakePercent(this.coords.p_from_real);break;case"to":if(this.options.to_fixed)break;this.coords.p_to_real=this.convertToRealPercent(t);this.coords.p_to_real=this.calcWithStep(this.coords.p_to_real);this.coords.p_to_real<this.coords.p_from_real&&(this.coords.p_to_real=this.coords.p_from_real);this.coords.p_to_real=this.checkDiapason(this.coords.p_to_real,this.options.to_min,this.options.to_max);this.coords.p_to_real=this.checkMinInterval(this.coords.p_to_real,this.coords.p_from_real,"to");this.coords.p_to_real=this.checkMaxInterval(this.coords.p_to_real,this.coords.p_from_real,"to");this.coords.p_to_fake=this.convertToFakePercent(this.coords.p_to_real);break;case"both":if(this.options.from_fixed||this.options.to_fixed)break;t=this.toFixed(t+.001*this.coords.p_handle);this.coords.p_from_real=this.convertToRealPercent(t)-this.coords.p_gap_left;this.coords.p_from_real=this.calcWithStep(this.coords.p_from_real);this.coords.p_from_real=this.checkDiapason(this.coords.p_from_real,this.options.from_min,this.options.from_max);this.coords.p_from_real=this.checkMinInterval(this.coords.p_from_real,this.coords.p_to_real,"from");this.coords.p_from_fake=this.convertToFakePercent(this.coords.p_from_real);this.coords.p_to_real=this.convertToRealPercent(t)+this.coords.p_gap_right;this.coords.p_to_real=this.calcWithStep(this.coords.p_to_real);this.coords.p_to_real=this.checkDiapason(this.coords.p_to_real,this.options.to_min,this.options.to_max);this.coords.p_to_real=this.checkMinInterval(this.coords.p_to_real,this.coords.p_from_real,"to");this.coords.p_to_fake=this.convertToFakePercent(this.coords.p_to_real);break;case"both_one":if(this.options.from_fixed||this.options.to_fixed)break;var o=this.convertToRealPercent(t),c=this.result.from_percent,u=this.result.to_percent-c,s=u/2,i=o-s,r=o+s;i<0&&(r=(i=0)+u);100<r&&(i=(r=100)-u);this.coords.p_from_real=this.calcWithStep(i);this.coords.p_from_real=this.checkDiapason(this.coords.p_from_real,this.options.from_min,this.options.from_max);this.coords.p_from_fake=this.convertToFakePercent(this.coords.p_from_real);this.coords.p_to_real=this.calcWithStep(r);this.coords.p_to_real=this.checkDiapason(this.coords.p_to_real,this.options.to_min,this.options.to_max);this.coords.p_to_fake=this.convertToFakePercent(this.coords.p_to_real)}"single"===this.options.type?(this.coords.p_bar_x=this.coords.p_handle/2,this.coords.p_bar_w=this.coords.p_single_fake,this.result.from_percent=this.coords.p_single_real,this.result.from=this.convertToValue(this.coords.p_single_real),this.result.from_pretty=this._prettify(this.result.from),this.options.values.length&&(this.result.from_value=this.options.values[this.result.from])):(this.coords.p_bar_x=this.toFixed(this.coords.p_from_fake+this.coords.p_handle/2),this.coords.p_bar_w=this.toFixed(this.coords.p_to_fake-this.coords.p_from_fake),this.result.from_percent=this.coords.p_from_real,this.result.from=this.convertToValue(this.coords.p_from_real),this.result.from_pretty=this._prettify(this.result.from),this.result.to_percent=this.coords.p_to_real,this.result.to=this.convertToValue(this.coords.p_to_real),this.result.to_pretty=this._prettify(this.result.to),this.options.values.length&&(this.result.from_value=this.options.values[this.result.from],this.result.to_value=this.options.values[this.result.to]));this.calcMinMax();this.calcLabels()}},calcPointerPercent:function(){this.coords.w_rs?(this.coords.x_pointer<0||isNaN(this.coords.x_pointer)?this.coords.x_pointer=0:this.coords.x_pointer>this.coords.w_rs&&(this.coords.x_pointer=this.coords.w_rs),this.coords.p_pointer=this.toFixed(this.coords.x_pointer/this.coords.w_rs*100)):this.coords.p_pointer=0},convertToRealPercent:function(n){return n/(100-this.coords.p_handle)*100},convertToFakePercent:function(n){return n/100*(100-this.coords.p_handle)},getHandleX:function(){var t=100-this.coords.p_handle,n=this.toFixed(this.coords.p_pointer-this.coords.p_gap);return n<0?n=0:t<n&&(n=t),n},calcHandlePercent:function(){this.coords.w_handle="single"===this.options.type?this.$cache.s_single.outerWidth(!1):this.$cache.s_from.outerWidth(!1);this.coords.p_handle=this.toFixed(this.coords.w_handle/this.coords.w_rs*100)},chooseHandle:function(n){return"single"===this.options.type?"single":this.coords.p_from_real+(this.coords.p_to_real-this.coords.p_from_real)/2<=n?this.options.to_fixed?"from":"to":this.options.from_fixed?"to":"from"},calcMinMax:function(){this.coords.w_rs&&(this.labels.p_min=this.labels.w_min/this.coords.w_rs*100,this.labels.p_max=this.labels.w_max/this.coords.w_rs*100)},calcLabels:function(){this.coords.w_rs&&!this.options.hide_from_to&&("single"===this.options.type?(this.labels.w_single=this.$cache.single.outerWidth(!1),this.labels.p_single_fake=this.labels.w_single/this.coords.w_rs*100,this.labels.p_single_left=this.coords.p_single_fake+this.coords.p_handle/2-this.labels.p_single_fake/2):(this.labels.w_from=this.$cache.from.outerWidth(!1),this.labels.p_from_fake=this.labels.w_from/this.coords.w_rs*100,this.labels.p_from_left=this.coords.p_from_fake+this.coords.p_handle/2-this.labels.p_from_fake/2,this.labels.p_from_left=this.toFixed(this.labels.p_from_left),this.labels.p_from_left=this.checkEdges(this.labels.p_from_left,this.labels.p_from_fake),this.labels.w_to=this.$cache.to.outerWidth(!1),this.labels.p_to_fake=this.labels.w_to/this.coords.w_rs*100,this.labels.p_to_left=this.coords.p_to_fake+this.coords.p_handle/2-this.labels.p_to_fake/2,this.labels.p_to_left=this.toFixed(this.labels.p_to_left),this.labels.p_to_left=this.checkEdges(this.labels.p_to_left,this.labels.p_to_fake),this.labels.w_single=this.$cache.single.outerWidth(!1),this.labels.p_single_fake=this.labels.w_single/this.coords.w_rs*100,this.labels.p_single_left=(this.labels.p_from_left+this.labels.p_to_left+this.labels.p_to_fake)/2-this.labels.p_single_fake/2,this.labels.p_single_left=this.toFixed(this.labels.p_single_left)),this.labels.p_single_left=this.checkEdges(this.labels.p_single_left,this.labels.p_single_fake))},updateScene:function(){this.raf_id&&(cancelAnimationFrame(this.raf_id),this.raf_id=null);clearTimeout(this.update_tm);this.update_tm=null;this.options&&(this.drawHandles(),this.is_active?this.raf_id=requestAnimationFrame(this.updateScene.bind(this)):this.update_tm=setTimeout(this.updateScene.bind(this),300))},drawHandles:function(){this.coords.w_rs=this.$cache.rs.outerWidth(!1);this.coords.w_rs&&(this.coords.w_rs!==this.coords.w_rs_old&&(this.target="base",this.is_resize=!0),this.coords.w_rs===this.coords.w_rs_old&&!this.force_redraw||(this.setMinMax(),this.calc(!0),this.drawLabels(),this.options.grid&&(this.calcGridMargin(),this.calcGridLabels()),this.force_redraw=!0,this.coords.w_rs_old=this.coords.w_rs,this.drawShadow()),this.coords.w_rs&&(this.dragging||this.force_redraw||this.is_key)&&((this.old_from!==this.result.from||this.old_to!==this.result.to||this.force_redraw||this.is_key)&&(this.drawLabels(),this.$cache.bar[0].style.left=this.coords.p_bar_x+"%",this.$cache.bar[0].style.width=this.coords.p_bar_w+"%","single"===this.options.type?(this.$cache.bar[0].style.left=0,this.$cache.bar[0].style.width=this.coords.p_bar_w+this.coords.p_bar_x+"%",this.$cache.s_single[0].style.left=this.coords.p_single_fake+"%"):(this.$cache.s_from[0].style.left=this.coords.p_from_fake+"%",this.$cache.s_to[0].style.left=this.coords.p_to_fake+"%",this.old_from===this.result.from&&!this.force_redraw||(this.$cache.from[0].style.left=this.labels.p_from_left+"%"),this.old_to===this.result.to&&!this.force_redraw||(this.$cache.to[0].style.left=this.labels.p_to_left+"%")),this.$cache.single[0].style.left=this.labels.p_single_left+"%",this.writeToInput(),this.old_from===this.result.from&&this.old_to===this.result.to||this.is_start||(this.$cache.input.trigger("change"),this.$cache.input.trigger("input")),this.old_from=this.result.from,this.old_to=this.result.to,this.is_resize||this.is_update||this.is_start||this.is_finish||this.callOnChange(),(this.is_key||this.is_click)&&(this.is_key=!1,this.is_click=!1,this.callOnFinish()),this.is_update=!1,this.is_resize=!1,this.is_finish=!1),this.is_start=!1,this.is_key=!1,this.is_click=!1,this.force_redraw=!1))},drawLabels:function(){var n,u,o,i,r,f,t;if(this.options&&(f=this.options.values.length,t=this.options.p_values,!this.options.hide_from_to))if("single"===this.options.type)n=f?this.decorate(t[this.result.from]):(i=this._prettify(this.result.from),this.decorate(i,this.result.from)),this.$cache.single.html(n),this.calcLabels(),this.$cache.min[0].style.visibility=this.labels.p_single_left<this.labels.p_min+1?"hidden":"visible",this.$cache.max[0].style.visibility=this.labels.p_single_left+this.labels.p_single_fake>99-this.labels.p_max?"hidden":"visible";else{o=f?(this.options.decorate_both?(n=this.decorate(t[this.result.from]),n+=this.options.values_separator,n+=this.decorate(t[this.result.to])):n=this.decorate(t[this.result.from]+this.options.values_separator+t[this.result.to]),u=this.decorate(t[this.result.from]),this.decorate(t[this.result.to])):(i=this._prettify(this.result.from),r=this._prettify(this.result.to),this.options.decorate_both?(n=this.decorate(i,this.result.from),n+=this.options.values_separator,n+=this.decorate(r,this.result.to)):n=this.decorate(i+this.options.values_separator+r,this.result.to),u=this.decorate(i,this.result.from),this.decorate(r,this.result.to));this.$cache.single.html(n);this.$cache.from.html(u);this.$cache.to.html(o);this.calcLabels();var c=Math.min(this.labels.p_single_left,this.labels.p_from_left),s=this.labels.p_single_left+this.labels.p_single_fake,e=this.labels.p_to_left+this.labels.p_to_fake,h=Math.max(s,e);this.labels.p_from_left+this.labels.p_from_fake>=this.labels.p_to_left?(this.$cache.from[0].style.visibility="hidden",this.$cache.to[0].style.visibility="hidden",this.$cache.single[0].style.visibility="visible",h=this.result.from===this.result.to?("from"===this.target?this.$cache.from[0].style.visibility="visible":"to"===this.target?this.$cache.to[0].style.visibility="visible":this.target||(this.$cache.from[0].style.visibility="visible"),this.$cache.single[0].style.visibility="hidden",e):(this.$cache.from[0].style.visibility="hidden",this.$cache.to[0].style.visibility="hidden",this.$cache.single[0].style.visibility="visible",Math.max(s,e))):(this.$cache.from[0].style.visibility="visible",this.$cache.to[0].style.visibility="visible",this.$cache.single[0].style.visibility="hidden");this.$cache.min[0].style.visibility=c<this.labels.p_min+1?"hidden":"visible";this.$cache.max[0].style.visibility=h>99-this.labels.p_max?"hidden":"visible"}},drawShadow:function(){var t,r,u,f,n=this.options,i=this.$cache,e="number"==typeof n.from_min&&!isNaN(n.from_min),o="number"==typeof n.from_max&&!isNaN(n.from_max),s="number"==typeof n.to_min&&!isNaN(n.to_min),h="number"==typeof n.to_max&&!isNaN(n.to_max);"single"===n.type?n.from_shadow&&(e||o)?(t=this.convertToPercent(e?n.from_min:n.min),r=this.convertToPercent(o?n.from_max:n.max)-t,t=this.toFixed(t-this.coords.p_handle/100*t),r=this.toFixed(r-this.coords.p_handle/100*r),t+=this.coords.p_handle/2,i.shad_single[0].style.display="block",i.shad_single[0].style.left=t+"%",i.shad_single[0].style.width=r+"%"):i.shad_single[0].style.display="none":(n.from_shadow&&(e||o)?(t=this.convertToPercent(e?n.from_min:n.min),r=this.convertToPercent(o?n.from_max:n.max)-t,t=this.toFixed(t-this.coords.p_handle/100*t),r=this.toFixed(r-this.coords.p_handle/100*r),t+=this.coords.p_handle/2,i.shad_from[0].style.display="block",i.shad_from[0].style.left=t+"%",i.shad_from[0].style.width=r+"%"):i.shad_from[0].style.display="none",n.to_shadow&&(s||h)?(u=this.convertToPercent(s?n.to_min:n.min),f=this.convertToPercent(h?n.to_max:n.max)-u,u=this.toFixed(u-this.coords.p_handle/100*u),f=this.toFixed(f-this.coords.p_handle/100*f),u+=this.coords.p_handle/2,i.shad_to[0].style.display="block",i.shad_to[0].style.left=u+"%",i.shad_to[0].style.width=f+"%"):i.shad_to[0].style.display="none")},writeToInput:function(){"single"===this.options.type?(this.options.values.length?this.$cache.input.prop("value",this.result.from_value):this.$cache.input.prop("value",this.result.from),this.$cache.input.data("from",this.result.from)):(this.options.values.length?this.$cache.input.prop("value",this.result.from_value+this.options.input_values_separator+this.result.to_value):this.$cache.input.prop("value",this.result.from+this.options.input_values_separator+this.result.to),this.$cache.input.data("from",this.result.from),this.$cache.input.data("to",this.result.to))},callOnStart:function(){this.writeToInput();this.options.onStart&&"function"==typeof this.options.onStart&&(this.options.scope?this.options.onStart.call(this.options.scope,this.result):this.options.onStart(this.result))},callOnChange:function(){this.writeToInput();this.options.onChange&&"function"==typeof this.options.onChange&&(this.options.scope?this.options.onChange.call(this.options.scope,this.result):this.options.onChange(this.result))},callOnFinish:function(){this.writeToInput();this.options.onFinish&&"function"==typeof this.options.onFinish&&(this.options.scope?this.options.onFinish.call(this.options.scope,this.result):this.options.onFinish(this.result))},callOnUpdate:function(){this.writeToInput();this.options.onUpdate&&"function"==typeof this.options.onUpdate&&(this.options.scope?this.options.onUpdate.call(this.options.scope,this.result):this.options.onUpdate(this.result))},toggleInput:function(){this.$cache.input.toggleClass("irs-hidden-input");this.has_tab_index?this.$cache.input.prop("tabindex",-1):this.$cache.input.removeProp("tabindex");this.has_tab_index=!this.has_tab_index},convertToPercent:function(n,t){var i,r=this.options.max-this.options.min,u=r/100;return r?(i=(t?n:n-this.options.min)/u,this.toFixed(i)):(this.no_diapason=!0,0)},convertToValue:function(n){var e,o,i=this.options.min,s=this.options.max,c=i.toString().split(".")[1],l=s.toString().split(".")[1],r=0,h=0,u,t,f;return 0===n?this.options.min:100===n?this.options.max:(c&&(r=e=c.length),l&&(r=o=l.length),e&&o&&(r=o<=e?e:o),i<0&&(i=+(i+(h=Math.abs(i))).toFixed(r),s=+(s+h).toFixed(r)),t=(s-i)/100*n+i,f=this.options.step.toString().split(".")[1],t=f?+t.toFixed(f.length):(t/=this.options.step,+(t*=this.options.step).toFixed(0)),h&&(t-=h),(u=f?+t.toFixed(f.length):this.toFixed(t))<this.options.min?u=this.options.min:u>this.options.max&&(u=this.options.max),u)},calcWithStep:function(n){var t=Math.round(n/this.coords.p_step)*this.coords.p_step;return 100<t&&(t=100),100===n&&(t=100),this.toFixed(t)},checkMinInterval:function(n,t,i){var r,u,f=this.options;return f.min_interval?(r=this.convertToValue(n),u=this.convertToValue(t),"from"===i?u-r<f.min_interval&&(r=u-f.min_interval):r-u<f.min_interval&&(r=u+f.min_interval),this.convertToPercent(r)):n},checkMaxInterval:function(n,t,i){var r,u,f=this.options;return f.max_interval?(r=this.convertToValue(n),u=this.convertToValue(t),"from"===i?u-r>f.max_interval&&(r=u-f.max_interval):r-u>f.max_interval&&(r=u+f.max_interval),this.convertToPercent(r)):n},checkDiapason:function(n,t,i){var r=this.convertToValue(n),u=this.options;return"number"!=typeof t&&(t=u.min),"number"!=typeof i&&(i=u.max),r<t&&(r=t),i<r&&(r=i),this.convertToPercent(r)},toFixed:function(n){return+(n=n.toFixed(20))},_prettify:function(n){return this.options.prettify_enabled?this.options.prettify&&"function"==typeof this.options.prettify?this.options.prettify(n):this.prettify(n):n},prettify:function(n){return n.toString().replace(/(\d{1,3}(?=(?:\d\d\d)+(?!\d)))/g,"$1"+this.options.prettify_separator)},checkEdges:function(n,t){return this.options.force_edges&&(n<0?n=0:100-t<n&&(n=100-t)),this.toFixed(n)},validate:function(){var i,r,n=this.options,t=this.result,u=n.values,f=u.length;if("string"==typeof n.min&&(n.min=+n.min),"string"==typeof n.max&&(n.max=+n.max),"string"==typeof n.from&&(n.from=+n.from),"string"==typeof n.to&&(n.to=+n.to),"string"==typeof n.step&&(n.step=+n.step),"string"==typeof n.from_min&&(n.from_min=+n.from_min),"string"==typeof n.from_max&&(n.from_max=+n.from_max),"string"==typeof n.to_min&&(n.to_min=+n.to_min),"string"==typeof n.to_max&&(n.to_max=+n.to_max),"string"==typeof n.grid_num&&(n.grid_num=+n.grid_num),n.max<n.min&&(n.max=n.min),f)for(n.p_values=[],n.min=0,n.max=f-1,n.step=1,n.grid_num=n.max,n.grid_snap=!0,r=0;r<f;r++)i=+u[r],i=isNaN(i)?u[r]:(u[r]=i,this._prettify(i)),n.p_values.push(i);("number"!=typeof n.from||isNaN(n.from))&&(n.from=n.min);"number"==typeof n.to&&!isNaN(n.to)||(n.to=n.max);"single"===n.type?(n.from<n.min&&(n.from=n.min),n.from>n.max&&(n.from=n.max)):(n.from<n.min&&(n.from=n.min),n.from>n.max&&(n.from=n.max),n.to<n.min&&(n.to=n.min),n.to>n.max&&(n.to=n.max),this.update_check.from&&(this.update_check.from!==n.from&&n.from>n.to&&(n.from=n.to),this.update_check.to!==n.to&&n.to<n.from&&(n.to=n.from)),n.from>n.to&&(n.from=n.to),n.to<n.from&&(n.to=n.from));("number"!=typeof n.step||isNaN(n.step)||!n.step||n.step<0)&&(n.step=1);"number"==typeof n.from_min&&n.from<n.from_min&&(n.from=n.from_min);"number"==typeof n.from_max&&n.from>n.from_max&&(n.from=n.from_max);"number"==typeof n.to_min&&n.to<n.to_min&&(n.to=n.to_min);"number"==typeof n.to_max&&n.from>n.to_max&&(n.to=n.to_max);t&&(t.min!==n.min&&(t.min=n.min),t.max!==n.max&&(t.max=n.max),(t.from<t.min||t.from>t.max)&&(t.from=n.from),(t.to<t.min||t.to>t.max)&&(t.to=n.to));("number"!=typeof n.min_interval||isNaN(n.min_interval)||!n.min_interval||n.min_interval<0)&&(n.min_interval=0);("number"!=typeof n.max_interval||isNaN(n.max_interval)||!n.max_interval||n.max_interval<0)&&(n.max_interval=0);n.min_interval&&n.min_interval>n.max-n.min&&(n.min_interval=n.max-n.min);n.max_interval&&n.max_interval>n.max-n.min&&(n.max_interval=n.max-n.min)},decorate:function(n,t){var r="",i=this.options;return i.prefix&&(r+=i.prefix),r+=n,i.max_postfix&&(i.values.length&&n===i.p_values[i.max]?(r+=i.max_postfix,i.postfix&&(r+=" ")):t===i.max&&(r+=i.max_postfix,i.postfix&&(r+=" "))),i.postfix&&(r+=i.postfix),r},updateFrom:function(){this.result.from=this.options.from;this.result.from_percent=this.convertToPercent(this.result.from);this.result.from_pretty=this._prettify(this.result.from);this.options.values&&(this.result.from_value=this.options.values[this.result.from])},updateTo:function(){this.result.to=this.options.to;this.result.to_percent=this.convertToPercent(this.result.to);this.result.to_pretty=this._prettify(this.result.to);this.options.values&&(this.result.to_value=this.options.values[this.result.to])},updateResult:function(){this.result.min=this.options.min;this.result.max=this.options.max;this.updateFrom();this.updateTo()},appendGrid:function(){if(this.options.grid){var i,f,s,h,c,e,r=this.options,l=r.max-r.min,n=r.grid_num,t=0,u=4,o="";for(this.calcGridMargin(),r.grid_snap&&(n=l/r.step),50<n&&(n=50),s=this.toFixed(100/n),4<n&&(u=3),7<n&&(u=2),14<n&&(u=1),28<n&&(u=0),i=0;i<n+1;i++){for(h=u,100<(t=this.toFixed(s*i))&&(t=100),c=((this.coords.big[i]=t)-s*(i-1))/(h+1),f=1;f<=h&&0!==t;f++)o+='<span class="irs-grid-pol small" style="left: '+this.toFixed(t-c*f)+'%"><\/span>';o+='<span class="irs-grid-pol" style="left: '+t+'%"><\/span>';e=this.convertToValue(t);o+='<span class="irs-grid-text js-grid-text-'+i+'" style="left: '+t+'%">'+(e=r.values.length?r.p_values[e]:this._prettify(e))+"<\/span>"}this.coords.big_num=Math.ceil(n+1);this.$cache.cont.addClass("irs-with-grid");this.$cache.grid.html(o);this.cacheGridLabels()}},cacheGridLabels:function(){for(var t,i=this.coords.big_num,n=0;n<i;n++)t=this.$cache.grid.find(".js-grid-text-"+n),this.$cache.grid_labels.push(t);this.calcGridLabels()},calcGridLabels:function(){for(var u,i=[],r=[],t=this.coords.big_num,n=0;n<t;n++)this.coords.big_w[n]=this.$cache.grid_labels[n].outerWidth(!1),this.coords.big_p[n]=this.toFixed(this.coords.big_w[n]/this.coords.w_rs*100),this.coords.big_x[n]=this.toFixed(this.coords.big_p[n]/2),i[n]=this.toFixed(this.coords.big[n]-this.coords.big_x[n]),r[n]=this.toFixed(i[n]+this.coords.big_p[n]);for(this.options.force_edges&&(i[0]<-this.coords.grid_gap&&(i[0]=-this.coords.grid_gap,r[0]=this.toFixed(i[0]+this.coords.big_p[0]),this.coords.big_x[0]=this.coords.grid_gap),r[t-1]>100+this.coords.grid_gap&&(r[t-1]=100+this.coords.grid_gap,i[t-1]=this.toFixed(r[t-1]-this.coords.big_p[t-1]),this.coords.big_x[t-1]=this.toFixed(this.coords.big_p[t-1]-this.coords.grid_gap))),this.calcGridCollision(2,i,r),this.calcGridCollision(4,i,r),n=0;n<t;n++)u=this.$cache.grid_labels[n][0],this.coords.big_x[n]!==Number.POSITIVE_INFINITY&&(u.style.marginLeft=-this.coords.big_x[n]+"%")},calcGridCollision:function(n,t,i){for(var u,f,e=this.coords.big_num,r=0;r<e&&!(e<=(u=r+n/2));r+=n)f=this.$cache.grid_labels[u][0],f.style.visibility=i[r]<=t[u]?"visible":"hidden"},calcGridMargin:function(){this.options.grid_margin&&(this.coords.w_rs=this.$cache.rs.outerWidth(!1),this.coords.w_rs&&(this.coords.w_handle="single"===this.options.type?this.$cache.s_single.outerWidth(!1):this.$cache.s_from.outerWidth(!1),this.coords.p_handle=this.toFixed(this.coords.w_handle/this.coords.w_rs*100),this.coords.grid_gap=this.toFixed(this.coords.p_handle/2-.1),this.$cache.grid[0].style.width=this.toFixed(100-this.coords.p_handle)+"%",this.$cache.grid[0].style.left=this.coords.grid_gap+"%"))},update:function(t){this.input&&(this.is_update=!0,this.options.from=this.result.from,this.options.to=this.result.to,this.update_check.from=this.result.from,this.update_check.to=this.result.to,this.options=n.extend(this.options,t),this.validate(),this.updateResult(t),this.toggleInput(),this.remove(),this.init(!0))},reset:function(){this.input&&(this.updateResult(),this.update())},destroy:function(){this.input&&(this.toggleInput(),this.$cache.input.prop("readonly",!1),n.data(this.input,"ionRangeSlider",null),this.remove(),this.input=null,this.options=null)}};n.fn.ionRangeSlider=function(t){return this.each(function(){n.data(this,"ionRangeSlider")||n.data(this,"ionRangeSlider",new s(this,t,h++))})},function(){for(var r=0,t=["ms","moz","webkit","o"],n=0;n<t.length&&!i.requestAnimationFrame;++n)i.requestAnimationFrame=i[t[n]+"RequestAnimationFrame"],i.cancelAnimationFrame=i[t[n]+"CancelAnimationFrame"]||i[t[n]+"CancelRequestAnimationFrame"];i.requestAnimationFrame||(i.requestAnimationFrame=function(n){var t=(new Date).getTime(),u=Math.max(0,16-(t-r)),f=i.setTimeout(function(){n(t+u)},u);return r=t+u,f});i.cancelAnimationFrame||(i.cancelAnimationFrame=function(n){clearTimeout(n)})}()});$("main").click(function(){$("main").removeClass("bg-tranparent-black");$("#result-desktop-search").hide()});$("#keywordesktop").focus(function(){$("main").addClass("bg-tranparent-black");$("#result-desktop-search").show()});$(document).ready(function(){var t=$("#nav-pc").width(),i=$(".catalogdetail").height(),n;$(".catalogdetail .nav-child").css({width:t,height:i});$(window).scroll(function(){$(this).scrollTop()>100?$("#scrollup").fadeIn():$("#scrollup").fadeOut()});$("#scrollup").click(function(){return $("html, body").animate({scrollTop:0},600),!1});n=$("header").height()-100;$(window).scroll(function(){$(window).scrollTop()>n?$(".header-body").addClass("nav-fix"):$(".header-body").removeClass("nav-fix")})});var loading=!1;$(document).ready(function(){$(".collapse").on("hide.bs.collapse",function(n){$(this).is(n.target)&&$(this).parent().find("> .collapse-header:first-child > span.icon").html('<i class="demo-icon ecs-down-open-big"><\/i>')});$(".collapse").on("show.bs.collapse",function(n){$(this).is(n.target)&&$(this).parent().find("> .collapse-header:first-child > span.icon").html('<i class="demo-icon ecs-up-open-big"><\/i>')})});$("#menu-btn").click(function(){$(this).toggleClass("open");$("#nav-mobile .nav-header").toggleClass("showmenu");$("#nav-mobile .nav-body").toggleClass("showmenu");$(".nav-parent-action").next().removeClass("showmenu");$("body").hasClass("showmenu1")==!0&&$("body").removeClass("showmenu1");$("body").toggleClass("showmenu");$("#nav-mobile .search-btn-action").removeClass("open");$("#nav-mobile .nav-footer").removeClass("showmenu")});$("#nav-mobile .search-btn-action").click(function(){$(this).toggleClass("open");$("#nav-mobile .nav-footer").toggleClass("showmenu");$("body").hasClass("showmenu")==!0&&$("body").removeClass("showmenu");$("body").toggleClass("showmenu1");$("#menu-btn").removeClass("open");$("#nav-mobile .nav-header").removeClass("showmenu");$("#nav-mobile .nav-body").removeClass("showmenu");$("#nav-mobile .nav-parent-action").removeClass("open");$("#nav-mobile .nav-parent-action").next().removeClass("showmenu")});$("#search-btn-bar").click(function(){$(this).toggleClass("open");$("#nav-mobile .nav-footer").toggleClass("showmenu");$("body").hasClass("showmenu")==!0&&$("body").removeClass("showmenu");$("body").toggleClass("showmenu1");$("#menu-btn").removeClass("open");$("#nav-mobile .nav-header").removeClass("showmenu");$("#nav-mobile .nav-body").removeClass("showmenu");$("#nav-mobile .nav-parent-action").removeClass("open");$("#nav-mobile .nav-parent-action").next().removeClass("showmenu")});$("#nav-mobile .nav-parent-action .icon").click(function(){$(this).parent().toggleClass("open");$(this).parent().next().toggleClass("showmenu")});$("#nav-mobile .back-btn").click(function(){$(this).toggleClass("open");$(this).parent().parent().parent().removeClass("showmenu")});
!function(t,e,i,s){function n(e,i){this.settings=null,this.options=t.extend({},n.Defaults,i),this.$element=t(e),this._handlers={},this._plugins={},this._supress={},this._current=null,this._speed=null,this._coordinates=[],this._breakpoint=null,this._width=null,this._items=[],this._clones=[],this._mergers=[],this._widths=[],this._invalidated={},this._pipe=[],this._drag={time:null,target:null,pointer:null,stage:{start:null,current:null},direction:null},this._states={current:{},tags:{initializing:["busy"],animating:["busy"],dragging:["interacting"]}},t.each(["onResize","onThrottledResize"],t.proxy(function(e,i){this._handlers[i]=t.proxy(this[i],this)},this)),t.each(n.Plugins,t.proxy(function(t,e){this._plugins[t.charAt(0).toLowerCase()+t.slice(1)]=new e(this)},this)),t.each(n.Workers,t.proxy(function(e,i){this._pipe.push({filter:i.filter,run:t.proxy(i.run,this)})},this)),this.setup(),this.initialize()}n.Defaults={items:3,loop:!1,center:!1,rewind:!1,mouseDrag:!0,touchDrag:!0,pullDrag:!0,freeDrag:!1,margin:0,stagePadding:0,merge:!1,mergeFit:!0,autoWidth:!1,startPosition:0,rtl:!1,smartSpeed:250,fluidSpeed:!1,dragEndSpeed:!1,responsive:{},responsiveRefreshRate:200,responsiveBaseElement:e,fallbackEasing:"swing",info:!1,nestedItemSelector:!1,itemElement:"div",stageElement:"div",refreshClass:"owl-refresh",loadedClass:"owl-loaded",loadingClass:"owl-loading",rtlClass:"owl-rtl",responsiveClass:"owl-responsive",dragClass:"owl-drag",itemClass:"owl-item",stageClass:"owl-stage",stageOuterClass:"owl-stage-outer",grabClass:"owl-grab"},n.Width={Default:"default",Inner:"inner",Outer:"outer"},n.Type={Event:"event",State:"state"},n.Plugins={},n.Workers=[{filter:["width","settings"],run:function(){this._width=this.$element.width()}},{filter:["width","items","settings"],run:function(t){t.current=this._items&&this._items[this.relative(this._current)]}},{filter:["items","settings"],run:function(){this.$stage.children(".cloned").remove()}},{filter:["width","items","settings"],run:function(t){var e=this.settings.margin||"",i=!this.settings.autoWidth,s=this.settings.rtl,n={width:"auto","margin-left":s?e:"","margin-right":s?"":e};!i&&this.$stage.children().css(n),t.css=n}},{filter:["width","items","settings"],run:function(t){var e=(this.width()/this.settings.items).toFixed(3)-this.settings.margin,i=null,s=this._items.length,n=!this.settings.autoWidth,o=[];for(t.items={merge:!1,width:e};s--;)i=this._mergers[s],i=this.settings.mergeFit&&Math.min(i,this.settings.items)||i,t.items.merge=i>1||t.items.merge,o[s]=n?e*i:this._items[s].width();this._widths=o}},{filter:["items","settings"],run:function(){var e=[],i=this._items,s=this.settings,n=Math.max(2*s.items,4),o=2*Math.ceil(i.length/2),r=s.loop&&i.length?s.rewind?n:Math.max(n,o):0,a="",h="";for(r/=2;r--;)e.push(this.normalize(e.length/2,!0)),a+=i[e[e.length-1]][0].outerHTML,e.push(this.normalize(i.length-1-(e.length-1)/2,!0)),h=i[e[e.length-1]][0].outerHTML+h;this._clones=e,t(a).addClass("cloned").appendTo(this.$stage),t(h).addClass("cloned").prependTo(this.$stage)}},{filter:["width","items","settings"],run:function(){for(var t=this.settings.rtl?1:-1,e=this._clones.length+this._items.length,i=-1,s=0,n=0,o=[];++i<e;)s=o[i-1]||0,n=this._widths[this.relative(i)]+this.settings.margin,o.push(s+n*t);this._coordinates=o}},{filter:["width","items","settings"],run:function(){var t=this.settings.stagePadding,e=this._coordinates,i={width:Math.ceil(Math.abs(e[e.length-1]))+2*t,"padding-left":t||"","padding-right":t||""};this.$stage.css(i)}},{filter:["width","items","settings"],run:function(t){var e=this._coordinates.length,i=!this.settings.autoWidth,s=this.$stage.children();if(i&&t.items.merge)for(;e--;)t.css.width=this._widths[this.relative(e)],s.eq(e).css(t.css);else i&&(t.css.width=t.items.width,s.css(t.css))}},{filter:["items"],run:function(){this._coordinates.length<1&&this.$stage.removeAttr("style")}},{filter:["width","items","settings"],run:function(t){t.current=t.current?this.$stage.children().index(t.current):0,t.current=Math.max(this.minimum(),Math.min(this.maximum(),t.current)),this.reset(t.current)}},{filter:["position"],run:function(){this.animate(this.coordinates(this._current))}},{filter:["width","position","items","settings"],run:function(){var t,e,i,s,n=this.settings.rtl?1:-1,o=2*this.settings.stagePadding,r=this.coordinates(this.current())+o,a=r+this.width()*n,h=[];for(i=0,s=this._coordinates.length;s>i;i++)t=this._coordinates[i-1]||0,e=Math.abs(this._coordinates[i])+o*n,(this.op(t,"<=",r)&&this.op(t,">",a)||this.op(e,"<",r)&&this.op(e,">",a))&&h.push(i);this.$stage.children(".active").removeClass("active"),this.$stage.children(":eq("+h.join("), :eq(")+")").addClass("active"),this.settings.center&&(this.$stage.children(".center").removeClass("center"),this.$stage.children().eq(this.current()).addClass("center"))}}],n.prototype.initialize=function(){if(this.enter("initializing"),this.trigger("initialize"),this.$element.toggleClass(this.settings.rtlClass,this.settings.rtl),this.settings.autoWidth&&!this.is("pre-loading")){var e,i,n;e=this.$element.find("img"),i=this.settings.nestedItemSelector?"."+this.settings.nestedItemSelector:s,n=this.$element.children(i).width(),e.length&&0>=n&&this.preloadAutoWidthImages(e)}this.$element.addClass(this.options.loadingClass),this.$stage=t("<"+this.settings.stageElement+' class="'+this.settings.stageClass+'"/>').wrap('<div class="'+this.settings.stageOuterClass+'"/>'),this.$element.append(this.$stage.parent()),this.replace(this.$element.children().not(this.$stage.parent())),this.$element.is(":visible")?this.refresh():this.invalidate("width"),this.$element.removeClass(this.options.loadingClass).addClass(this.options.loadedClass),this.registerEventHandlers(),this.leave("initializing"),this.trigger("initialized")},n.prototype.setup=function(){var e=this.viewport(),i=this.options.responsive,s=-1,n=null;i?(t.each(i,function(t){e>=t&&t>s&&(s=Number(t))}),n=t.extend({},this.options,i[s]),"function"==typeof n.stagePadding&&(n.stagePadding=n.stagePadding()),delete n.responsive,n.responsiveClass&&this.$element.attr("class",this.$element.attr("class").replace(new RegExp("("+this.options.responsiveClass+"-)\\S+\\s","g"),"$1"+s))):n=t.extend({},this.options),this.trigger("change",{property:{name:"settings",value:n}}),this._breakpoint=s,this.settings=n,this.invalidate("settings"),this.trigger("changed",{property:{name:"settings",value:this.settings}})},n.prototype.optionsLogic=function(){this.settings.autoWidth&&(this.settings.stagePadding=!1,this.settings.merge=!1)},n.prototype.prepare=function(e){var i=this.trigger("prepare",{content:e});return i.data||(i.data=t("<"+this.settings.itemElement+"/>").addClass(this.options.itemClass).append(e)),this.trigger("prepared",{content:i.data}),i.data},n.prototype.update=function(){for(var e=0,i=this._pipe.length,s=t.proxy(function(t){return this[t]},this._invalidated),n={};i>e;)(this._invalidated.all||t.grep(this._pipe[e].filter,s).length>0)&&this._pipe[e].run(n),e++;this._invalidated={},!this.is("valid")&&this.enter("valid")},n.prototype.width=function(t){switch(t=t||n.Width.Default){case n.Width.Inner:case n.Width.Outer:return this._width;default:return this._width-2*this.settings.stagePadding+this.settings.margin}},n.prototype.refresh=function(){this.enter("refreshing"),this.trigger("refresh"),this.setup(),this.optionsLogic(),this.$element.addClass(this.options.refreshClass),this.update(),this.$element.removeClass(this.options.refreshClass),this.leave("refreshing"),this.trigger("refreshed")},n.prototype.onThrottledResize=function(){e.clearTimeout(this.resizeTimer),this.resizeTimer=e.setTimeout(this._handlers.onResize,this.settings.responsiveRefreshRate)},n.prototype.onResize=function(){return!!this._items.length&&this._width!==this.$element.width()&&!!this.$element.is(":visible")&&(this.enter("resizing"),this.trigger("resize").isDefaultPrevented()?(this.leave("resizing"),!1):(this.invalidate("width"),this.refresh(),this.leave("resizing"),void this.trigger("resized")))},n.prototype.registerEventHandlers=function(){t.support.transition&&this.$stage.on(t.support.transition.end+".owl.core",t.proxy(this.onTransitionEnd,this)),this.settings.responsive!==!1&&this.on(e,"resize",this._handlers.onThrottledResize),this.settings.mouseDrag&&(this.$element.addClass(this.options.dragClass),this.$stage.on("mousedown.owl.core",t.proxy(this.onDragStart,this)),this.$stage.on("dragstart.owl.core selectstart.owl.core",function(){return!1})),this.settings.touchDrag&&(this.$stage.on("touchstart.owl.core",t.proxy(this.onDragStart,this)),this.$stage.on("touchcancel.owl.core",t.proxy(this.onDragEnd,this)))},n.prototype.onDragStart=function(e){var s=null;3!==e.which&&(t.support.transform?(s=this.$stage.css("transform").replace(/.*\(|\)| /g,"").split(","),s={x:s[16===s.length?12:4],y:s[16===s.length?13:5]}):(s=this.$stage.position(),s={x:this.settings.rtl?s.left+this.$stage.width()-this.width()+this.settings.margin:s.left,y:s.top}),this.is("animating")&&(t.support.transform?this.animate(s.x):this.$stage.stop(),this.invalidate("position")),this.$element.toggleClass(this.options.grabClass,"mousedown"===e.type),this.speed(0),this._drag.time=(new Date).getTime(),this._drag.target=t(e.target),this._drag.stage.start=s,this._drag.stage.current=s,this._drag.pointer=this.pointer(e),t(i).on("mouseup.owl.core touchend.owl.core",t.proxy(this.onDragEnd,this)),t(i).one("mousemove.owl.core touchmove.owl.core",t.proxy(function(e){var s=this.difference(this._drag.pointer,this.pointer(e));t(i).on("mousemove.owl.core touchmove.owl.core",t.proxy(this.onDragMove,this)),Math.abs(s.x)<Math.abs(s.y)&&this.is("valid")||(e.preventDefault(),this.enter("dragging"),this.trigger("drag"))},this)))},n.prototype.onDragMove=function(t){var e=null,i=null,s=null,n=this.difference(this._drag.pointer,this.pointer(t)),o=this.difference(this._drag.stage.start,n);this.is("dragging")&&(t.preventDefault(),this.settings.loop?(e=this.coordinates(this.minimum()),i=this.coordinates(this.maximum()+1)-e,o.x=((o.x-e)%i+i)%i+e):(e=this.settings.rtl?this.coordinates(this.maximum()):this.coordinates(this.minimum()),i=this.settings.rtl?this.coordinates(this.minimum()):this.coordinates(this.maximum()),s=this.settings.pullDrag?-1*n.x/5:0,o.x=Math.max(Math.min(o.x,e+s),i+s)),this._drag.stage.current=o,this.animate(o.x))},n.prototype.onDragEnd=function(e){var s=this.difference(this._drag.pointer,this.pointer(e)),n=this._drag.stage.current,o=s.x>0^this.settings.rtl?"left":"right";t(i).off(".owl.core"),this.$element.removeClass(this.options.grabClass),(0!==s.x&&this.is("dragging")||!this.is("valid"))&&(this.speed(this.settings.dragEndSpeed||this.settings.smartSpeed),this.current(this.closest(n.x,0!==s.x?o:this._drag.direction)),this.invalidate("position"),this.update(),this._drag.direction=o,(Math.abs(s.x)>3||(new Date).getTime()-this._drag.time>300)&&this._drag.target.one("click.owl.core",function(){return!1})),this.is("dragging")&&(this.leave("dragging"),this.trigger("dragged"))},n.prototype.closest=function(e,i){var s=-1,n=30,o=this.width(),r=this.coordinates();return this.settings.freeDrag||t.each(r,t.proxy(function(t,a){return"left"===i&&e>a-n&&a+n>e?s=t:"right"===i&&e>a-o-n&&a-o+n>e?s=t+1:this.op(e,"<",a)&&this.op(e,">",r[t+1]||a-o)&&(s="left"===i?t+1:t),-1===s},this)),this.settings.loop||(this.op(e,">",r[this.minimum()])?s=e=this.minimum():this.op(e,"<",r[this.maximum()])&&(s=e=this.maximum())),s},n.prototype.animate=function(e){var i=this.speed()>0;this.is("animating")&&this.onTransitionEnd(),i&&(this.enter("animating"),this.trigger("translate")),t.support.transform3d&&t.support.transition?this.$stage.css({transform:"translate3d("+e+"px,0px,0px)",transition:this.speed()/1e3+"s"}):i?this.$stage.animate({left:e+"px"},this.speed(),this.settings.fallbackEasing,t.proxy(this.onTransitionEnd,this)):this.$stage.css({left:e+"px"})},n.prototype.is=function(t){return this._states.current[t]&&this._states.current[t]>0},n.prototype.current=function(t){if(t===s)return this._current;if(0===this._items.length)return s;if(t=this.normalize(t),this._current!==t){var e=this.trigger("change",{property:{name:"position",value:t}});e.data!==s&&(t=this.normalize(e.data)),this._current=t,this.invalidate("position"),this.trigger("changed",{property:{name:"position",value:this._current}})}return this._current},n.prototype.invalidate=function(e){return"string"===t.type(e)&&(this._invalidated[e]=!0,this.is("valid")&&this.leave("valid")),t.map(this._invalidated,function(t,e){return e})},n.prototype.reset=function(t){t=this.normalize(t),t!==s&&(this._speed=0,this._current=t,this.suppress(["translate","translated"]),this.animate(this.coordinates(t)),this.release(["translate","translated"]))},n.prototype.normalize=function(t,e){var i=this._items.length,n=e?0:this._clones.length;return!this.isNumeric(t)||1>i?t=s:(0>t||t>=i+n)&&(t=((t-n/2)%i+i)%i+n/2),t},n.prototype.relative=function(t){return t-=this._clones.length/2,this.normalize(t,!0)},n.prototype.maximum=function(t){var e,i,s,n=this.settings,o=this._coordinates.length;if(n.loop)o=this._clones.length/2+this._items.length-1;else if(n.autoWidth||n.merge){for(e=this._items.length,i=this._items[--e].width(),s=this.$element.width();e--&&(i+=this._items[e].width()+this.settings.margin,!(i>s)););o=e+1}else o=n.center?this._items.length-1:this._items.length-n.items;return t&&(o-=this._clones.length/2),Math.max(o,0)},n.prototype.minimum=function(t){return t?0:this._clones.length/2},n.prototype.items=function(t){return t===s?this._items.slice():(t=this.normalize(t,!0),this._items[t])},n.prototype.mergers=function(t){return t===s?this._mergers.slice():(t=this.normalize(t,!0),this._mergers[t])},n.prototype.clones=function(e){var i=this._clones.length/2,n=i+this._items.length,o=function(t){return t%2===0?n+t/2:i-(t+1)/2};return e===s?t.map(this._clones,function(t,e){return o(e)}):t.map(this._clones,function(t,i){return t===e?o(i):null})},n.prototype.speed=function(t){return t!==s&&(this._speed=t),this._speed},n.prototype.coordinates=function(e){var i,n=1,o=e-1;return e===s?t.map(this._coordinates,t.proxy(function(t,e){return this.coordinates(e)},this)):(this.settings.center?(this.settings.rtl&&(n=-1,o=e+1),i=this._coordinates[e],i+=(this.width()-i+(this._coordinates[o]||0))/2*n):i=this._coordinates[o]||0,i=Math.ceil(i))},n.prototype.duration=function(t,e,i){return 0===i?0:Math.min(Math.max(Math.abs(e-t),1),6)*Math.abs(i||this.settings.smartSpeed)},n.prototype.to=function(t,e){var i=this.current(),s=null,n=t-this.relative(i),o=(n>0)-(0>n),r=this._items.length,a=this.minimum(),h=this.maximum();this.settings.loop?(!this.settings.rewind&&Math.abs(n)>r/2&&(n+=-1*o*r),t=i+n,s=((t-a)%r+r)%r+a,s!==t&&h>=s-n&&s-n>0&&(i=s-n,t=s,this.reset(i))):this.settings.rewind?(h+=1,t=(t%h+h)%h):t=Math.max(a,Math.min(h,t)),this.speed(this.duration(i,t,e)),this.current(t),this.$element.is(":visible")&&this.update()},n.prototype.next=function(t){t=t||!1,this.to(this.relative(this.current())+1,t)},n.prototype.prev=function(t){t=t||!1,this.to(this.relative(this.current())-1,t)},n.prototype.onTransitionEnd=function(t){return t!==s&&(t.stopPropagation(),(t.target||t.srcElement||t.originalTarget)!==this.$stage.get(0))?!1:(this.leave("animating"),void this.trigger("translated"))},n.prototype.viewport=function(){var s;return this.options.responsiveBaseElement!==e?s=t(this.options.responsiveBaseElement).width():e.innerWidth?s=e.innerWidth:i.documentElement&&i.documentElement.clientWidth?s=i.documentElement.clientWidth:console.warn("Can not detect viewport width."),s},n.prototype.replace=function(e){this.$stage.empty(),this._items=[],e&&(e=e instanceof jQuery?e:t(e)),this.settings.nestedItemSelector&&(e=e.find("."+this.settings.nestedItemSelector)),e.filter(function(){return 1===this.nodeType}).each(t.proxy(function(t,e){e=this.prepare(e),this.$stage.append(e),this._items.push(e),this._mergers.push(1*e.find("[data-merge]").addBack("[data-merge]").attr("data-merge")||1)},this)),this.reset(this.isNumeric(this.settings.startPosition)?this.settings.startPosition:0),this.invalidate("items")},n.prototype.add=function(e,i){var n=this.relative(this._current);i=i===s?this._items.length:this.normalize(i,!0),e=e instanceof jQuery?e:t(e),this.trigger("add",{content:e,position:i}),e=this.prepare(e),0===this._items.length||i===this._items.length?(0===this._items.length&&this.$stage.append(e),0!==this._items.length&&this._items[i-1].after(e),this._items.push(e),this._mergers.push(1*e.find("[data-merge]").addBack("[data-merge]").attr("data-merge")||1)):(this._items[i].before(e),this._items.splice(i,0,e),this._mergers.splice(i,0,1*e.find("[data-merge]").addBack("[data-merge]").attr("data-merge")||1)),this._items[n]&&this.reset(this._items[n].index()),this.invalidate("items"),this.trigger("added",{content:e,position:i})},n.prototype.remove=function(t){t=this.normalize(t,!0),t!==s&&(this.trigger("remove",{content:this._items[t],position:t}),this._items[t].remove(),this._items.splice(t,1),this._mergers.splice(t,1),this.invalidate("items"),this.trigger("removed",{content:null,position:t}))},n.prototype.preloadAutoWidthImages=function(e){e.each(t.proxy(function(e,i){this.enter("pre-loading"),i=t(i),t(new Image).one("load",t.proxy(function(t){i.attr("src",t.target.src),i.css("opacity",1),this.leave("pre-loading"),!this.is("pre-loading")&&!this.is("initializing")&&this.refresh()},this)).attr("src",i.attr("src")||i.attr("data-src")||i.attr("data-src-retina"))},this))},n.prototype.destroy=function(){this.$element.off(".owl.core"),this.$stage.off(".owl.core"),t(i).off(".owl.core"),this.settings.responsive!==!1&&(e.clearTimeout(this.resizeTimer),this.off(e,"resize",this._handlers.onThrottledResize));for(var s in this._plugins)this._plugins[s].destroy();this.$stage.children(".cloned").remove(),this.$stage.unwrap(),this.$stage.children().contents().unwrap(),this.$stage.children().unwrap(),this.$element.removeClass(this.options.refreshClass).removeClass(this.options.loadingClass).removeClass(this.options.loadedClass).removeClass(this.options.rtlClass).removeClass(this.options.dragClass).removeClass(this.options.grabClass).attr("class",this.$element.attr("class").replace(new RegExp(this.options.responsiveClass+"-\\S+\\s","g"),"")).removeData("owl.carousel")},n.prototype.op=function(t,e,i){var s=this.settings.rtl;switch(e){case"<":return s?t>i:i>t;case">":return s?i>t:t>i;case">=":return s?i>=t:t>=i;case"<=":return s?t>=i:i>=t}},n.prototype.on=function(t,e,i,s){t.addEventListener?t.addEventListener(e,i,s):t.attachEvent&&t.attachEvent("on"+e,i)},n.prototype.off=function(t,e,i,s){t.removeEventListener?t.removeEventListener(e,i,s):t.detachEvent&&t.detachEvent("on"+e,i)},n.prototype.trigger=function(e,i,s,o,r){var a={item:{count:this._items.length,index:this.current()}},h=t.camelCase(t.grep(["on",e,s],function(t){return t}).join("-").toLowerCase()),l=t.Event([e,"owl",s||"carousel"].join(".").toLowerCase(),t.extend({relatedTarget:this},a,i));return this._supress[e]||(t.each(this._plugins,function(t,e){e.onTrigger&&e.onTrigger(l)}),this.register({type:n.Type.Event,name:e}),this.$element.trigger(l),this.settings&&"function"==typeof this.settings[h]&&this.settings[h].call(this,l)),l},n.prototype.enter=function(e){t.each([e].concat(this._states.tags[e]||[]),t.proxy(function(t,e){this._states.current[e]===s&&(this._states.current[e]=0),this._states.current[e]++},this))},n.prototype.leave=function(e){t.each([e].concat(this._states.tags[e]||[]),t.proxy(function(t,e){this._states.current[e]--},this))},n.prototype.register=function(e){if(e.type===n.Type.Event){if(t.event.special[e.name]||(t.event.special[e.name]={}),!t.event.special[e.name].owl){var i=t.event.special[e.name]._default;t.event.special[e.name]._default=function(t){return!i||!i.apply||t.namespace&&-1!==t.namespace.indexOf("owl")?t.namespace&&t.namespace.indexOf("owl")>-1:i.apply(this,arguments)},t.event.special[e.name].owl=!0}}else e.type===n.Type.State&&(this._states.tags[e.name]?this._states.tags[e.name]=this._states.tags[e.name].concat(e.tags):this._states.tags[e.name]=e.tags,this._states.tags[e.name]=t.grep(this._states.tags[e.name],t.proxy(function(i,s){return t.inArray(i,this._states.tags[e.name])===s},this)))},n.prototype.suppress=function(e){t.each(e,t.proxy(function(t,e){this._supress[e]=!0},this))},n.prototype.release=function(e){t.each(e,t.proxy(function(t,e){delete this._supress[e]},this))},n.prototype.pointer=function(t){var i={x:null,y:null};return t=t.originalEvent||t||e.event,t=t.touches&&t.touches.length?t.touches[0]:t.changedTouches&&t.changedTouches.length?t.changedTouches[0]:t,t.pageX?(i.x=t.pageX,i.y=t.pageY):(i.x=t.clientX,i.y=t.clientY),i},n.prototype.isNumeric=function(t){return!isNaN(parseFloat(t))},n.prototype.difference=function(t,e){return{x:t.x-e.x,y:t.y-e.y}},t.fn.owlCarousel=function(e){var i=Array.prototype.slice.call(arguments,1);return this.each(function(){var s=t(this),o=s.data("owl.carousel");o||(o=new n(this,"object"==typeof e&&e),s.data("owl.carousel",o),t.each(["next","prev","to","destroy","refresh","replace","add","remove"],function(e,i){o.register({type:n.Type.Event,name:i}),o.$element.on(i+".owl.carousel.core",t.proxy(function(t){t.namespace&&t.relatedTarget!==this&&(this.suppress([i]),o[i].apply(this,[].slice.call(arguments,1)),this.release([i]))},o))})),"string"==typeof e&&"_"!==e.charAt(0)&&o[e].apply(o,i)})},t.fn.owlCarousel.Constructor=n}(window.Zepto||window.jQuery,window,document),function(t,e,i,s){var n=function(e){this._core=e,this._interval=null,this._visible=null,this._handlers={"initialized.owl.carousel":t.proxy(function(t){t.namespace&&this._core.settings.autoRefresh&&this.watch()},this)},this._core.options=t.extend({},n.Defaults,this._core.options),this._core.$element.on(this._handlers)};n.Defaults={autoRefresh:!0,autoRefreshInterval:500},n.prototype.watch=function(){this._interval||(this._visible=this._core.$element.is(":visible"),this._interval=e.setInterval(t.proxy(this.refresh,this),this._core.settings.autoRefreshInterval))},n.prototype.refresh=function(){this._core.$element.is(":visible")!==this._visible&&(this._visible=!this._visible,this._core.$element.toggleClass("owl-hidden",!this._visible),this._visible&&this._core.invalidate("width")&&this._core.refresh())},n.prototype.destroy=function(){var t,i;e.clearInterval(this._interval);for(t in this._handlers)this._core.$element.off(t,this._handlers[t]);for(i in Object.getOwnPropertyNames(this))"function"!=typeof this[i]&&(this[i]=null)},t.fn.owlCarousel.Constructor.Plugins.AutoRefresh=n}(window.Zepto||window.jQuery,window,document),function(t,e,i,s){var n=function(e){this._core=e,this._loaded=[],this._handlers={"initialized.owl.carousel change.owl.carousel resized.owl.carousel":t.proxy(function(e){if(e.namespace&&this._core.settings&&this._core.settings.lazyLoad&&(e.property&&"position"==e.property.name||"initialized"==e.type))for(var i=this._core.settings,n=i.center&&Math.ceil(i.items/2)||i.items,o=i.center&&-1*n||0,r=(e.property&&e.property.value!==s?e.property.value:this._core.current())+o,a=this._core.clones().length,h=t.proxy(function(t,e){this.load(e)},this);o++<n;)this.load(a/2+this._core.relative(r)),a&&t.each(this._core.clones(this._core.relative(r)),h),r++},this)},this._core.options=t.extend({},n.Defaults,this._core.options),this._core.$element.on(this._handlers)};n.Defaults={lazyLoad:!1},n.prototype.load=function(i){var s=this._core.$stage.children().eq(i),n=s&&s.find(".owl-lazy");!n||t.inArray(s.get(0),this._loaded)>-1||(n.each(t.proxy(function(i,s){var n,o=t(s),r=e.devicePixelRatio>1&&o.attr("data-src-retina")||o.attr("data-src");this._core.trigger("load",{element:o,url:r},"lazy"),o.is("img")?o.one("load.owl.lazy",t.proxy(function(){o.css("opacity",1),this._core.trigger("loaded",{element:o,url:r},"lazy")},this)).attr("src",r):(n=new Image,n.onload=t.proxy(function(){o.css({"background-image":'url("'+r+'")',opacity:"1"}),this._core.trigger("loaded",{element:o,url:r},"lazy")},this),n.src=r)},this)),this._loaded.push(s.get(0)))},n.prototype.destroy=function(){var t,e;for(t in this.handlers)this._core.$element.off(t,this.handlers[t]);for(e in Object.getOwnPropertyNames(this))"function"!=typeof this[e]&&(this[e]=null)},t.fn.owlCarousel.Constructor.Plugins.Lazy=n}(window.Zepto||window.jQuery,window,document),function(t,e,i,s){var n=function(e){this._core=e,this._handlers={"initialized.owl.carousel refreshed.owl.carousel":t.proxy(function(t){t.namespace&&this._core.settings.autoHeight&&this.update()},this),"changed.owl.carousel":t.proxy(function(t){t.namespace&&this._core.settings.autoHeight&&"position"==t.property.name&&this.update()},this),"loaded.owl.lazy":t.proxy(function(t){t.namespace&&this._core.settings.autoHeight&&t.element.closest("."+this._core.settings.itemClass).index()===this._core.current()&&this.update()},this)},this._core.options=t.extend({},n.Defaults,this._core.options),this._core.$element.on(this._handlers)};n.Defaults={autoHeight:!1,autoHeightClass:"owl-height"},n.prototype.update=function(){var e=this._core._current,i=e+this._core.settings.items,s=this._core.$stage.children().toArray().slice(e,i),n=[],o=0;t.each(s,function(e,i){n.push(t(i).height())}),o=Math.max.apply(null,n),this._core.$stage.parent().height(o).addClass(this._core.settings.autoHeightClass)},n.prototype.destroy=function(){var t,e;for(t in this._handlers)this._core.$element.off(t,this._handlers[t]);for(e in Object.getOwnPropertyNames(this))"function"!=typeof this[e]&&(this[e]=null)},t.fn.owlCarousel.Constructor.Plugins.AutoHeight=n}(window.Zepto||window.jQuery,window,document),function(t,e,i,s){var n=function(e){this._core=e,this._videos={},this._playing=null,this._handlers={"initialized.owl.carousel":t.proxy(function(t){t.namespace&&this._core.register({type:"state",name:"playing",tags:["interacting"]})},this),"resize.owl.carousel":t.proxy(function(t){t.namespace&&this._core.settings.video&&this.isInFullScreen()&&t.preventDefault()},this),"refreshed.owl.carousel":t.proxy(function(t){t.namespace&&this._core.is("resizing")&&this._core.$stage.find(".cloned .owl-video-frame").remove()},this),"changed.owl.carousel":t.proxy(function(t){t.namespace&&"position"===t.property.name&&this._playing&&this.stop()},this),"prepared.owl.carousel":t.proxy(function(e){if(e.namespace){var i=t(e.content).find(".owl-video");i.length&&(i.css("display","none"),this.fetch(i,t(e.content)))}},this)},this._core.options=t.extend({},n.Defaults,this._core.options),this._core.$element.on(this._handlers),this._core.$element.on("click.owl.video",".owl-video-play-icon",t.proxy(function(t){this.play(t)},this))};n.Defaults={video:!1,videoHeight:!1,videoWidth:!1},n.prototype.fetch=function(t,e){var i=function(){return t.attr("data-vimeo-id")?"vimeo":t.attr("data-vzaar-id")?"vzaar":"youtube"}(),s=t.attr("data-vimeo-id")||t.attr("data-youtube-id")||t.attr("data-vzaar-id"),n=t.attr("data-width")||this._core.settings.videoWidth,o=t.attr("data-height")||this._core.settings.videoHeight,r=t.attr("href");if(!r)throw new Error("Missing video URL.");if(s=r.match(/(http:|https:|)\/\/(player.|www.|app.)?(vimeo\.com|youtu(be\.com|\.be|be\.googleapis\.com)|vzaar\.com)\/(video\/|videos\/|embed\/|channels\/.+\/|groups\/.+\/|watch\?v=|v\/)?([A-Za-z0-9._%-]*)(\&\S+)?/),s[3].indexOf("youtu")>-1)i="youtube";else if(s[3].indexOf("vimeo")>-1)i="vimeo";else{if(!(s[3].indexOf("vzaar")>-1))throw new Error("Video URL not supported.");i="vzaar"}s=s[6],this._videos[r]={type:i,id:s,width:n,height:o},e.attr("data-video",r),this.thumbnail(t,this._videos[r])},n.prototype.thumbnail=function(e,i){var s,n,o,r=i.width&&i.height?'style="width:'+i.width+"px;height:"+i.height+'px;"':"",a=e.find("img"),h="src",l="",c=this._core.settings,p=function(t){n='<div class="owl-video-play-icon"></div>',s=c.lazyLoad?'<div class="owl-video-tn '+l+'" '+h+'="'+t+'"></div>':'<div class="owl-video-tn" style="opacity:1;background-image:url('+t+')"></div>',e.after(s),e.after(n)};return e.wrap('<div class="owl-video-wrapper"'+r+"></div>"),this._core.settings.lazyLoad&&(h="data-src",l="owl-lazy"),a.length?(p(a.attr(h)),a.remove(),!1):void("youtube"===i.type?(o="http://img.youtube.com/vi/"+i.id+"/maxresdefault.jpg",p(o)):"vimeo"===i.type?t.ajax({type:"GET",url:"//vimeo.com/api/v2/video/"+i.id+".json",jsonp:"callback",dataType:"jsonp",success:function(t){o=t[0].thumbnail_large,p(o)}}):"vzaar"===i.type&&t.ajax({type:"GET",url:"//vzaar.com/api/videos/"+i.id+".json",jsonp:"callback",dataType:"jsonp",success:function(t){o=t.framegrab_url,p(o)}}))},n.prototype.stop=function(){this._core.trigger("stop",null,"video"),this._playing.find(".owl-video-frame").remove(),this._playing.removeClass("owl-video-playing"),this._playing=null,this._core.leave("playing"),this._core.trigger("stopped",null,"video")},n.prototype.play=function(e){var i,s=t(e.target),n=s.closest("."+this._core.settings.itemClass),o=this._videos[n.attr("data-video")],r=o.width||"100%",a=o.height||this._core.$stage.height();this._playing||(this._core.enter("playing"),this._core.trigger("play",null,"video"),n=this._core.items(this._core.relative(n.index())),this._core.reset(n.index()),"youtube"===o.type?i='<iframe width="'+r+'" height="'+a+'" src="https://www.youtube.com/embed/'+o.id+"?autoplay=1&rel=0&v="+o.id+'" frameborder="0" allowfullscreen></iframe>':"vimeo"===o.type?i='<iframe src="//player.vimeo.com/video/'+o.id+'?autoplay=1" width="'+r+'" height="'+a+'" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>':"vzaar"===o.type&&(i='<iframe frameborder="0"height="'+a+'"width="'+r+'" allowfullscreen mozallowfullscreen webkitAllowFullScreen src="//view.vzaar.com/'+o.id+'/player?autoplay=true"></iframe>'),t('<div class="owl-video-frame">'+i+"</div>").insertAfter(n.find(".owl-video")),this._playing=n.addClass("owl-video-playing"))},n.prototype.isInFullScreen=function(){var e=i.fullscreenElement||i.mozFullScreenElement||i.webkitFullscreenElement;return e&&t(e).parent().hasClass("owl-video-frame")},n.prototype.destroy=function(){var t,e;this._core.$element.off("click.owl.video");for(t in this._handlers)this._core.$element.off(t,this._handlers[t]);for(e in Object.getOwnPropertyNames(this))"function"!=typeof this[e]&&(this[e]=null)},t.fn.owlCarousel.Constructor.Plugins.Video=n}(window.Zepto||window.jQuery,window,document),function(t,e,i,s){var n=function(e){this.core=e,this.core.options=t.extend({},n.Defaults,this.core.options),this.swapping=!0,this.previous=s,this.next=s,this.handlers={"change.owl.carousel":t.proxy(function(t){t.namespace&&"position"==t.property.name&&(this.previous=this.core.current(),this.next=t.property.value)},this),"drag.owl.carousel dragged.owl.carousel translated.owl.carousel":t.proxy(function(t){t.namespace&&(this.swapping="translated"==t.type)},this),"translate.owl.carousel":t.proxy(function(t){t.namespace&&this.swapping&&(this.core.options.animateOut||this.core.options.animateIn)&&this.swap()},this)},this.core.$element.on(this.handlers)};n.Defaults={animateOut:!1,animateIn:!1},n.prototype.swap=function(){if(1===this.core.settings.items&&t.support.animation&&t.support.transition){this.core.speed(0);var e,i=t.proxy(this.clear,this),s=this.core.$stage.children().eq(this.previous),n=this.core.$stage.children().eq(this.next),o=this.core.settings.animateIn,r=this.core.settings.animateOut;this.core.current()!==this.previous&&(r&&(e=this.core.coordinates(this.previous)-this.core.coordinates(this.next),s.one(t.support.animation.end,i).css({left:e+"px"}).addClass("animated owl-animated-out").addClass(r)),o&&n.one(t.support.animation.end,i).addClass("animated owl-animated-in").addClass(o))}},n.prototype.clear=function(e){t(e.target).css({left:""}).removeClass("animated owl-animated-out owl-animated-in").removeClass(this.core.settings.animateIn).removeClass(this.core.settings.animateOut),this.core.onTransitionEnd()},n.prototype.destroy=function(){var t,e;for(t in this.handlers)this.core.$element.off(t,this.handlers[t]);for(e in Object.getOwnPropertyNames(this))"function"!=typeof this[e]&&(this[e]=null);
},t.fn.owlCarousel.Constructor.Plugins.Animate=n}(window.Zepto||window.jQuery,window,document),function(t,e,i,s){var n=function(e){this._core=e,this._timeout=null,this._paused=!1,this._handlers={"changed.owl.carousel":t.proxy(function(t){t.namespace&&"settings"===t.property.name?this._core.settings.autoplay?this.play():this.stop():t.namespace&&"position"===t.property.name&&this._core.settings.autoplay&&this._setAutoPlayInterval()},this),"initialized.owl.carousel":t.proxy(function(t){t.namespace&&this._core.settings.autoplay&&this.play()},this),"play.owl.autoplay":t.proxy(function(t,e,i){t.namespace&&this.play(e,i)},this),"stop.owl.autoplay":t.proxy(function(t){t.namespace&&this.stop()},this),"mouseover.owl.autoplay":t.proxy(function(){this._core.settings.autoplayHoverPause&&this._core.is("rotating")&&this.pause()},this),"mouseleave.owl.autoplay":t.proxy(function(){this._core.settings.autoplayHoverPause&&this._core.is("rotating")&&this.play()},this),"touchstart.owl.core":t.proxy(function(){this._core.settings.autoplayHoverPause&&this._core.is("rotating")&&this.pause()},this),"touchend.owl.core":t.proxy(function(){this._core.settings.autoplayHoverPause&&this.play()},this)},this._core.$element.on(this._handlers),this._core.options=t.extend({},n.Defaults,this._core.options)};n.Defaults={autoplay:!1,autoplayTimeout:5e3,autoplayHoverPause:!1,autoplaySpeed:!1},n.prototype.play=function(t,e){this._paused=!1,this._core.is("rotating")||(this._core.enter("rotating"),this._setAutoPlayInterval())},n.prototype._getNextTimeout=function(s,n){return this._timeout&&e.clearTimeout(this._timeout),e.setTimeout(t.proxy(function(){this._paused||this._core.is("busy")||this._core.is("interacting")||i.hidden||this._core.next(n||this._core.settings.autoplaySpeed)},this),s||this._core.settings.autoplayTimeout)},n.prototype._setAutoPlayInterval=function(){this._timeout=this._getNextTimeout()},n.prototype.stop=function(){this._core.is("rotating")&&(e.clearTimeout(this._timeout),this._core.leave("rotating"))},n.prototype.pause=function(){this._core.is("rotating")&&(this._paused=!0)},n.prototype.destroy=function(){var t,e;this.stop();for(t in this._handlers)this._core.$element.off(t,this._handlers[t]);for(e in Object.getOwnPropertyNames(this))"function"!=typeof this[e]&&(this[e]=null)},t.fn.owlCarousel.Constructor.Plugins.autoplay=n}(window.Zepto||window.jQuery,window,document),function(t,e,i,s){"use strict";var n=function(e){this._core=e,this._initialized=!1,this._pages=[],this._controls={},this._templates=[],this.$element=this._core.$element,this._overrides={next:this._core.next,prev:this._core.prev,to:this._core.to},this._handlers={"prepared.owl.carousel":t.proxy(function(e){e.namespace&&this._core.settings.dotsData&&this._templates.push('<div class="'+this._core.settings.dotClass+'">'+t(e.content).find("[data-dot]").addBack("[data-dot]").attr("data-dot")+"</div>")},this),"added.owl.carousel":t.proxy(function(t){t.namespace&&this._core.settings.dotsData&&this._templates.splice(t.position,0,this._templates.pop())},this),"remove.owl.carousel":t.proxy(function(t){t.namespace&&this._core.settings.dotsData&&this._templates.splice(t.position,1)},this),"changed.owl.carousel":t.proxy(function(t){t.namespace&&"position"==t.property.name&&this.draw()},this),"initialized.owl.carousel":t.proxy(function(t){t.namespace&&!this._initialized&&(this._core.trigger("initialize",null,"navigation"),this.initialize(),this.update(),this.draw(),this._initialized=!0,this._core.trigger("initialized",null,"navigation"))},this),"refreshed.owl.carousel":t.proxy(function(t){t.namespace&&this._initialized&&(this._core.trigger("refresh",null,"navigation"),this.update(),this.draw(),this._core.trigger("refreshed",null,"navigation"))},this)},this._core.options=t.extend({},n.Defaults,this._core.options),this.$element.on(this._handlers)};n.Defaults={nav:!1,navText:["prev","next"],navSpeed:!1,navElement:"div",navContainer:!1,navContainerClass:"owl-nav",navClass:["owl-prev","owl-next"],slideBy:1,dotClass:"owl-dot",dotsClass:"owl-dots",dots:!0,dotsEach:!1,dotsData:!1,dotsSpeed:!1,dotsContainer:!1},n.prototype.initialize=function(){var e,i=this._core.settings;this._controls.$relative=(i.navContainer?t(i.navContainer):t("<div>").addClass(i.navContainerClass).appendTo(this.$element)).addClass("disabled"),this._controls.$previous=t("<"+i.navElement+">").addClass(i.navClass[0]).html(i.navText[0]).prependTo(this._controls.$relative).on("click",t.proxy(function(t){this.prev(i.navSpeed)},this)),this._controls.$next=t("<"+i.navElement+">").addClass(i.navClass[1]).html(i.navText[1]).appendTo(this._controls.$relative).on("click",t.proxy(function(t){this.next(i.navSpeed)},this)),i.dotsData||(this._templates=[t("<div>").addClass(i.dotClass).append(t("<span>")).prop("outerHTML")]),this._controls.$absolute=(i.dotsContainer?t(i.dotsContainer):t("<div>").addClass(i.dotsClass).appendTo(this.$element)).addClass("disabled"),this._controls.$absolute.on("click","div",t.proxy(function(e){var s=t(e.target).parent().is(this._controls.$absolute)?t(e.target).index():t(e.target).parent().index();e.preventDefault(),this.to(s,i.dotsSpeed)},this));for(e in this._overrides)this._core[e]=t.proxy(this[e],this)},n.prototype.destroy=function(){var t,e,i,s;for(t in this._handlers)this.$element.off(t,this._handlers[t]);for(e in this._controls)this._controls[e].remove();for(s in this.overides)this._core[s]=this._overrides[s];for(i in Object.getOwnPropertyNames(this))"function"!=typeof this[i]&&(this[i]=null)},n.prototype.update=function(){var t,e,i,s=this._core.clones().length/2,n=s+this._core.items().length,o=this._core.maximum(!0),r=this._core.settings,a=r.center||r.autoWidth||r.dotsData?1:r.dotsEach||r.items;if("page"!==r.slideBy&&(r.slideBy=Math.min(r.slideBy,r.items)),r.dots||"page"==r.slideBy)for(this._pages=[],t=s,e=0,i=0;n>t;t++){if(e>=a||0===e){if(this._pages.push({start:Math.min(o,t-s),end:t-s+a-1}),Math.min(o,t-s)===o)break;e=0,++i}e+=this._core.mergers(this._core.relative(t))}},n.prototype.draw=function(){var e,i=this._core.settings,s=this._core.items().length<=i.items,n=this._core.relative(this._core.current()),o=i.loop||i.rewind;this._controls.$relative.toggleClass("disabled",!i.nav||s),i.nav&&(this._controls.$previous.toggleClass("disabled",!o&&n<=this._core.minimum(!0)),this._controls.$next.toggleClass("disabled",!o&&n>=this._core.maximum(!0))),this._controls.$absolute.toggleClass("disabled",!i.dots||s),i.dots&&(e=this._pages.length-this._controls.$absolute.children().length,i.dotsData&&0!==e?this._controls.$absolute.html(this._templates.join("")):e>0?this._controls.$absolute.append(new Array(e+1).join(this._templates[0])):0>e&&this._controls.$absolute.children().slice(e).remove(),this._controls.$absolute.find(".active").removeClass("active"),this._controls.$absolute.children().eq(t.inArray(this.current(),this._pages)).addClass("active"))},n.prototype.onTrigger=function(e){var i=this._core.settings;e.page={index:t.inArray(this.current(),this._pages),count:this._pages.length,size:i&&(i.center||i.autoWidth||i.dotsData?1:i.dotsEach||i.items)}},n.prototype.current=function(){var e=this._core.relative(this._core.current());return t.grep(this._pages,t.proxy(function(t,i){return t.start<=e&&t.end>=e},this)).pop()},n.prototype.getPosition=function(e){var i,s,n=this._core.settings;return"page"==n.slideBy?(i=t.inArray(this.current(),this._pages),s=this._pages.length,e?++i:--i,i=this._pages[(i%s+s)%s].start):(i=this._core.relative(this._core.current()),s=this._core.items().length,e?i+=n.slideBy:i-=n.slideBy),i},n.prototype.next=function(e){t.proxy(this._overrides.to,this._core)(this.getPosition(!0),e)},n.prototype.prev=function(e){t.proxy(this._overrides.to,this._core)(this.getPosition(!1),e)},n.prototype.to=function(e,i,s){var n;!s&&this._pages.length?(n=this._pages.length,t.proxy(this._overrides.to,this._core)(this._pages[(e%n+n)%n].start,i)):t.proxy(this._overrides.to,this._core)(e,i)},t.fn.owlCarousel.Constructor.Plugins.Navigation=n}(window.Zepto||window.jQuery,window,document),function(t,e,i,s){"use strict";var n=function(i){this._core=i,this._hashes={},this.$element=this._core.$element,this._handlers={"initialized.owl.carousel":t.proxy(function(i){i.namespace&&"URLHash"===this._core.settings.startPosition&&t(e).trigger("hashchange.owl.navigation")},this),"prepared.owl.carousel":t.proxy(function(e){if(e.namespace){var i=t(e.content).find("[data-hash]").addBack("[data-hash]").attr("data-hash");if(!i)return;this._hashes[i]=e.content}},this),"changed.owl.carousel":t.proxy(function(i){if(i.namespace&&"position"===i.property.name){var s=this._core.items(this._core.relative(this._core.current())),n=t.map(this._hashes,function(t,e){return t===s?e:null}).join();if(!n||e.location.hash.slice(1)===n)return;e.location.hash=n}},this)},this._core.options=t.extend({},n.Defaults,this._core.options),this.$element.on(this._handlers),t(e).on("hashchange.owl.navigation",t.proxy(function(t){var i=e.location.hash.substring(1),n=this._core.$stage.children(),o=this._hashes[i]&&n.index(this._hashes[i]);o!==s&&o!==this._core.current()&&this._core.to(this._core.relative(o),!1,!0)},this))};n.Defaults={URLhashListener:!1},n.prototype.destroy=function(){var i,s;t(e).off("hashchange.owl.navigation");for(i in this._handlers)this._core.$element.off(i,this._handlers[i]);for(s in Object.getOwnPropertyNames(this))"function"!=typeof this[s]&&(this[s]=null)},t.fn.owlCarousel.Constructor.Plugins.Hash=n}(window.Zepto||window.jQuery,window,document),function(t,e,i,s){function n(e,i){var n=!1,o=e.charAt(0).toUpperCase()+e.slice(1);return t.each((e+" "+a.join(o+" ")+o).split(" "),function(t,e){return r[e]!==s?(n=!i||e,!1):void 0}),n}function o(t){return n(t,!0)}var r=t("<support>").get(0).style,a="Webkit Moz O ms".split(" "),h={transition:{end:{WebkitTransition:"webkitTransitionEnd",MozTransition:"transitionend",OTransition:"oTransitionEnd",transition:"transitionend"}},animation:{end:{WebkitAnimation:"webkitAnimationEnd",MozAnimation:"animationend",OAnimation:"oAnimationEnd",animation:"animationend"}}},l={csstransforms:function(){return!!n("transform")},csstransforms3d:function(){return!!n("perspective")},csstransitions:function(){return!!n("transition")},cssanimations:function(){return!!n("animation")}};l.csstransitions()&&(t.support.transition=new String(o("transition")),t.support.transition.end=h.transition.end[t.support.transition]),l.cssanimations()&&(t.support.animation=new String(o("animation")),t.support.animation.end=h.animation.end[t.support.animation]),l.csstransforms()&&(t.support.transform=new String(o("transform")),t.support.transform3d=l.csstransforms3d())}(window.Zepto||window.jQuery,window,document);
/**
 * Owl carousel
 * @version 2.0.0
 * @author Bartosz Wojciechowski
 * @license The MIT License (MIT)
 * @todo Lazy Load Icon
 * @todo prevent animationend bubling
 * @todo itemsScaleUp
 * @todo Test Zepto
 * @todo stagePadding calculate wrong active classes
 */
;(function($, window, document, undefined) {

	/**
	 * Creates a carousel.
	 * @class The Owl Carousel.
	 * @public
	 * @param {HTMLElement|jQuery} element - The element to create the carousel for.
	 * @param {Object} [options] - The options
	 */
	function Owl(element, options) {

		/**
		 * Current settings for the carousel.
		 * @public
		 */
		this.settings = null;

		/**
		 * Current options set by the caller including defaults.
		 * @public
		 */
		this.options = $.extend({}, Owl.Defaults, options);

		/**
		 * Plugin element.
		 * @public
		 */
		this.$element = $(element);

		/**
		 * Proxied event handlers.
		 * @protected
		 */
		this._handlers = {};

		/**
		 * References to the running plugins of this carousel.
		 * @protected
		 */
		this._plugins = {};

		/**
		 * Currently suppressed events to prevent them from beeing retriggered.
		 * @protected
		 */
		this._supress = {};

		/**
		 * Absolute current position.
		 * @protected
		 */
		this._current = null;

		/**
		 * Animation speed in milliseconds.
		 * @protected
		 */
		this._speed = null;

		/**
		 * Coordinates of all items in pixel.
		 * @todo The name of this member is missleading.
		 * @protected
		 */
		this._coordinates = [];

		/**
		 * Current breakpoint.
		 * @todo Real media queries would be nice.
		 * @protected
		 */
		this._breakpoint = null;

		/**
		 * Current width of the plugin element.
		 */
		this._length = null;

		/**
		 * All real items.
		 * @protected
		 */
		this._items = [];

		/**
		 * All cloned items.
		 * @protected
		 */
		this._clones = [];

		/**
		 * Merge values of all items.
		 * @todo Maybe this could be part of a plugin.
		 * @protected
		 */
		this._mergers = [];

		/**
		 * Widths of all items.
		 */
		this._lengths = [];

		/**
		 * Invalidated parts within the update process.
		 * @protected
		 */
		this._invalidated = {};

		/**
		 * Ordered list of workers for the update process.
		 * @protected
		 */
		this._pipe = [];

		/**
		 * Current state information for the drag operation.
		 * @todo #261
		 * @protected
		 */
		this._drag = {
			time: null,
			target: null,
			pointer: null,
			stage: {
				start: null,
				current: null
			},
			direction: null
		};

		/**
		 * Current state information and their tags.
		 * @type {Object}
		 * @protected
		 */
		this._states = {
			current: {},
			tags: {
				'initializing': [ 'busy' ],
				'animating': [ 'busy' ],
				'dragging': [ 'interacting' ]
			}
		};

		$.each([ 'onResize', 'onThrottledResize' ], $.proxy(function(i, handler) {
			this._handlers[handler] = $.proxy(this[handler], this);
		}, this));

		$.each(Owl.Plugins, $.proxy(function(key, plugin) {
			this._plugins[key.charAt(0).toLowerCase() + key.slice(1)]
				= new plugin(this);
		}, this));

		$.each(Owl.Workers, $.proxy(function(priority, worker) {
			this._pipe.push({
				'filter': worker.filter,
				'run': $.proxy(worker.run, this)
			});
		}, this));

		this.setup();
		this.initialize();
	}

	/**
	 * Default options for the carousel.
	 * @public
	 */
	Owl.Defaults = {
		items: 3,
		loop: false,
		center: false,
		rewind: false,

		mouseDrag: true,
		touchDrag: true,
		pullDrag: true,
		freeDrag: false,

		margin: 0,
		stagePadding: 0,

		merge: false,
		mergeFit: true,
		autoWidth: false,

		startPosition: 0,
		rtl: false,
		vertical: false,

		smartSpeed: 250,
		fluidSpeed: false,
		dragEndSpeed: false,

		responsive: {},
		responsiveRefreshRate: 200,
		responsiveBaseElement: window,

		fallbackEasing: 'swing',

		info: false,

		nestedItemSelector: false,
		itemElement: 'div',
		stageElement: 'div',

		refreshClass: 'owl-refresh',
		loadedClass: 'owl-loaded',
		loadingClass: 'owl-loading',
		rtlClass: 'owl-rtl',
		responsiveClass: 'owl-responsive',
		dragClass: 'owl-drag',
		itemClass: 'owl-item',
		stageClass: 'owl-stage',
		stageOuterClass: 'owl-stage-outer',
		grabClass: 'owl-grab'
	};

	/**
	 * Enumeration for width.
	 * @public
	 * @readonly
	 * @enum {String}
	 */
	Owl.Length = {
		Default: 'default',
		Inner: 'inner',
		Outer: 'outer'
	};

	/**
	 * Enumeration for types.
	 * @public
	 * @readonly
	 * @enum {String}
	 */
	Owl.Type = {
		Event: 'event',
		State: 'state'
	};

	/**
	 * Contains all registered plugins.
	 * @public
	 */
	Owl.Plugins = {};

	/**
	 * List of workers involved in the update process.
	 */
	Owl.Workers = [ {
		filter: [ '_length', 'settings' ],
		run: function() {
			this._length = this.$element[this.settings.vertical ? 'height' : 'width']();
		}
	}, {
		filter: [ '_length', 'items', 'settings' ],
		run: function(cache) {
			cache.current = this._items && this._items[this.relative(this._current)];
		}
	}, {
		filter: [ 'items', 'settings' ],
		run: function() {
			this.$stage.children('.cloned').remove();
		}
	}, {
		filter: [ '_length', 'items', 'settings' ],
		run: function(cache) {

			var margin = this.settings.margin || '',
				grid = !(this.settings.vertical ? this.settings.autoHeight : this.settings.autoWidth),
				rtl = this.settings.rtl, css={};
				css[this.settings.vertical ? 'height' : 'width'] = 'auto';
				css[this.settings.vertical ? 'margin-top' : 'margin-left'] =  rtl ? margin : '',
				css[this.settings.vertical ? 'margin-bottom' : 'margin-right'] = rtl ? '' : margin

			!grid && this.$stage.children().css(css);

			cache.css = css;
		}
	}, {
		filter: [ '_length', 'items', 'settings' ],
		run: function(cache) {
			var _length = (this.getLength() / this.settings.items).toFixed(3) - this.settings.margin,
				merge = null,
				iterator = this._items.length,
				grid = !(this.settings.vertical ? this.settings.autoHeight : this.settings.autoWidth),
				widths = [];

			cache.items = {
				merge: false,
				_length: _length
			};

			while (iterator--) {
				merge = this._mergers[iterator];
				merge = this.settings.mergeFit && Math.min(merge, this.settings.items) || merge;

				cache.items.merge = merge > 1 || cache.items.merge;

				widths[iterator] = !grid ? this._items[iterator][this.settings.vertical ? 'height' : 'width']() : _length * merge;
			}

			this._lengths = widths;
		}
	}, {
		filter: [ 'items', 'settings' ],
		run: function() {
			var clones = [],
				items = this._items,
				settings = this.settings,
				view = Math.max(settings.items * 2, 4),
				size = Math.ceil(items.length / 2) * 2,
				repeat = settings.loop && items.length ? settings.rewind ? view : Math.max(view, size) : 0,
				append = '',
				prepend = '';

			repeat /= 2;

			while (repeat--) {
				clones.push(this.normalize(clones.length / 2, true));
				append = append + items[clones[clones.length - 1]][0].outerHTML;
				clones.push(this.normalize(items.length - 1 - (clones.length - 1) / 2, true));
				prepend = items[clones[clones.length - 1]][0].outerHTML + prepend;
			}

			this._clones = clones;

			$(append).addClass('cloned').appendTo(this.$stage);
			$(prepend).addClass('cloned').prependTo(this.$stage);
		}
	}, {
		filter: [ '_length', 'items', 'settings' ],
		run: function() {
			var rtl = this.settings.rtl ? 1 : -1,
				size = this._clones.length + this._items.length,
				iterator = -1,
				previous = 0,
				current = 0,
				coordinates = [];

			while (++iterator < size) {
				previous = coordinates[iterator - 1] || 0;
				current = this._lengths[this.relative(iterator)] + this.settings.margin;
				coordinates.push(previous + current * rtl);
			}

			this._coordinates = coordinates;
		}
	}, {
		filter: [ '_length', 'items', 'settings' ],
		run: function() {
			var padding = this.settings.stagePadding,
				coordinates = this._coordinates, css = {};
				css[this.settings.vertical ? 'height' : 'width'] = Math.ceil(Math.abs(coordinates[coordinates.length - 1])) + padding * 2;
				css[this.settings.vertical ? 'padding-top' : 'padding-left'] =  padding || '';
				css[this.settings.vertical ? 'padding-bottom' : 'padding-right'] = padding || '';

			this.$stage.css(css);
		}
	}, {
		filter: [ '_length', 'items', 'settings' ],
		run: function(cache) {
			var iterator = this._coordinates.length,
				grid = !(this.settings.vertical ? this.settings.autoHeight : this.settings.autoWidth),
				items = this.$stage.children();

			if (grid && cache.items.merge) {
				while (iterator--) {
					cache.css[this.settings.vertical ? 'height' : 'width'] = this._lengths[this.relative(iterator)];
					items.eq(iterator).css(cache.css);
				}
			} else if (grid) {
				cache.css[this.settings.vertical ? 'height' : 'width'] = cache.items._length;
				items.css(cache.css);
			}
		}
	}, {
		filter: [ 'items' ],
		run: function() {
			this._coordinates.length < 1 && this.$stage.removeAttr('style');
		}
	}, {
		filter: [ '_length', 'items', 'settings' ],
		run: function(cache) {
			cache.current = cache.current ? this.$stage.children().index(cache.current) : 0;
			cache.current = Math.max(this.minimum(), Math.min(this.maximum(), cache.current));
			this.reset(cache.current);
		}
	}, {
		filter: [ 'position' ],
		run: function() {
			this.animate(this.coordinates(this._current));
		}
	}, {
		filter: [ '_length', 'position', 'items', 'settings' ],
		run: function() {
			var rtl = this.settings.rtl ? 1 : -1,
				padding = this.settings.stagePadding * 2,
				begin = this.coordinates(this.current()) + padding,
				end = begin + this.getLength() * rtl,
				inner, outer, matches = [], i, n;

			for (i = 0, n = this._coordinates.length; i < n; i++) {
				inner = this._coordinates[i - 1] || 0;
				outer = Math.abs(this._coordinates[i]) + padding * rtl;

				if ((this.op(inner, '<=', begin) && (this.op(inner, '>', end)))
					|| (this.op(outer, '<', begin) && this.op(outer, '>', end))) {
					matches.push(i);
				}
			}

			this.$stage.children('.active').removeClass('active');
			this.$stage.children(':eq(' + matches.join('), :eq(') + ')').addClass('active');

			if (this.settings.center) {
				this.$stage.children('.center').removeClass('center');
				this.$stage.children().eq(this.current()).addClass('center');
			}
		}
	} ];

	/**
	 * Initializes the carousel.
	 * @protected
	 */
	Owl.prototype.initialize = function() {
		this.enter('initializing');
		this.trigger('initialize');

		this.$element.toggleClass(this.settings.rtlClass, this.settings.rtl);

		if ((this.settings.vertical ? this.settings.autoHeight : this.settings.autoWidth) && !this.is('pre-loading')) {
			var imgs, nestedSelector, width;
			imgs = this.$element.find('img');
			nestedSelector = this.settings.nestedItemSelector ? '.' + this.settings.nestedItemSelector : undefined;
			length = this.$element.children(nestedSelector)[this.settings.vertical ? 'height' : 'width']();

			if (imgs.length && length <= 0) {
				this.preloadAutoWidthImages(imgs);
			}
		}

		this.$element.addClass(this.options.loadingClass);

		// create stage
		this.$stage = $('<' + this.settings.stageElement + ' class="' + this.settings.stageClass + '"/>')
			.wrap('<div class="' + this.settings.stageOuterClass + '"/>');

		// append stage
		this.$element.append(this.$stage.parent());

		// append content
		this.replace(this.$element.children().not(this.$stage.parent()));

		// check visibility
		if (this.$element.is(':visible')) {
			// update view
			this.refresh();
		} else {
			// invalidate length
			this.invalidate('_length');
		}

		this.$element
			.removeClass(this.options.loadingClass)
			.addClass(this.options.loadedClass);

		// register event handlers
		this.registerEventHandlers();

		this.leave('initializing');
		this.trigger('initialized');
	};

	/**
	 * Setups the current settings.
	 * @todo Remove responsive classes. Why should adaptive designs be brought into IE8?
	 * @todo Support for media queries by using `matchMedia` would be nice.
	 * @public
	 */
	Owl.prototype.setup = function() {
		var viewport = this.viewport(),
			overwrites = this.options.responsive,
			match = -1,
			settings = null;

		if (!overwrites) {
			settings = $.extend({}, this.options);
		} else {
			$.each(overwrites, function(breakpoint) {
				if (breakpoint <= viewport && breakpoint > match) {
					match = Number(breakpoint);
				}
			});

			settings = $.extend({}, this.options, overwrites[match]);
			delete settings.responsive;

			// responsive class
			if (settings.responsiveClass) {
				this.$element.attr('class',
					this.$element.attr('class').replace(new RegExp('(' + this.options.responsiveClass + '-)\\S+\\s', 'g'), '$1' + match)
				);
			}
		}

		if (this.settings === null || this._breakpoint !== match) {
			this.trigger('change', { property: { name: 'settings', value: settings } });
			this._breakpoint = match;
			this.settings = settings;
			this.invalidate('settings');
			this.trigger('changed', { property: { name: 'settings', value: this.settings } });
		}
	};

	/**
	 * Updates option logic if necessery.
	 * @protected
	 */
	Owl.prototype.optionsLogic = function() {
		if ((this.settings.vertical ? this.settings.autoHeight : this.settings.autoWidth)) {
			this.settings.stagePadding = false;
			this.settings.merge = false;
		}
	};

	/**
	 * Prepares an item before add.
	 * @todo Rename event parameter `content` to `item`.
	 * @protected
	 * @returns {jQuery|HTMLElement} - The item container.
	 */
	Owl.prototype.prepare = function(item) {
		var event = this.trigger('prepare', { content: item });

		if (!event.data) {
			event.data = $('<' + this.settings.itemElement + '/>')
				.addClass(this.options.itemClass).append(item)
		}

		this.trigger('prepared', { content: event.data });

		return event.data;
	};

	/**
	 * Updates the view.
	 * @public
	 */
	Owl.prototype.update = function() {
		var i = 0,
			n = this._pipe.length,
			filter = $.proxy(function(p) { return this[p] }, this._invalidated),
			cache = {};

		while (i < n) {
			if (this._invalidated.all || $.grep(this._pipe[i].filter, filter).length > 0) {
				this._pipe[i].run(cache);
			}
			i++;
		}

		this._invalidated = {};

		!this.is('valid') && this.enter('valid');
	};

	/**
	 * Gets the width of the view.
	 * @public
	 * @param {Owl.Length} [dimension=Owl.Length.Default] - The dimension to return.
	 * @returns {Number} - The width of the view in pixel.
	 */
	Owl.prototype.getLength = function(dimension) {
		dimension = dimension || Owl.Length.Default;
		switch (dimension) {
			case Owl.Length.Inner:
			case Owl.Length.Outer:
				return this._length;
			default:
				return this._length - this.settings.stagePadding * 2 + this.settings.margin;
		}
	};

	/**
	 * Refreshes the carousel primarily for adaptive purposes.
	 * @public
	 */
	Owl.prototype.refresh = function() {
		this.enter('refreshing');
		this.trigger('refresh');

		this.setup();

		this.optionsLogic();

		this.$element.addClass(this.options.refreshClass);

		this.update();

		this.$element.removeClass(this.options.refreshClass);

		this.leave('refreshing');
		this.trigger('refreshed');
	};

	/**
	 * Checks window `resize` event.
	 * @protected
	 */
	Owl.prototype.onThrottledResize = function() {
		window.clearTimeout(this.resizeTimer);
		this.resizeTimer = window.setTimeout(this._handlers.onResize, this.settings.responsiveRefreshRate);
	};

	/**
	 * Checks window `resize` event.
	 * @protected
	 */
	Owl.prototype.onResize = function() {
		if (!this._items.length) {
			return false;
		}

		if (this._length === this.$element[this.settings.vertical ? 'height' : 'width']()) {
			return false;
		}

		if (!this.$element.is(':visible')) {
			return false;
		}

		this.enter('resizing');

		if (this.trigger('resize').isDefaultPrevented()) {
			this.leave('resizing');
			return false;
		}

		this.invalidate('_length');

		this.refresh();

		this.leave('resizing');
		this.trigger('resized');
	};

	/**
	 * Registers event handlers.
	 * @todo Check `msPointerEnabled`
	 * @todo #261
	 * @protected
	 */
	Owl.prototype.registerEventHandlers = function() {
		if ($.support.transition) {
			this.$stage.on($.support.transition.end + '.owl.core', $.proxy(this.onTransitionEnd, this));
		}

		if (this.settings.responsive !== false) {
			this.on(window, 'resize', this._handlers.onThrottledResize);
		}

		if (this.settings.mouseDrag) {
			this.$element.addClass(this.options.dragClass);
			this.$stage.on('mousedown.owl.core', $.proxy(this.onDragStart, this));
			this.$stage.on('dragstart.owl.core selectstart.owl.core', function() { return false });
		}

		if (this.settings.touchDrag){
			this.$stage.on('touchstart.owl.core', $.proxy(this.onDragStart, this));
			this.$stage.on('touchcancel.owl.core', $.proxy(this.onDragEnd, this));
		}
	};

	/**
	 * Handles `touchstart` and `mousedown` events.
	 * @todo Horizontal swipe threshold as option
	 * @todo #261
	 * @protected
	 * @param {Event} event - The event arguments.
	 */
	Owl.prototype.onDragStart = function(event) {
		var stage = null;

		if (event.which === 3) {
			return;
		}

		if ($.support.transform) {
			stage = this.$stage.css('transform').replace(/.*\(|\)| /g, '').split(',');

			// console.log(stage);
			stage = {
				x: stage[stage.length === 16 ? 12 : 4],
				y: stage[stage.length === 16 ? 13 : 5]
			};
		} else {
			stage = this.$stage.position();

			if(this.settings.vertical){
				stage = {
					x: this.left,
					y: this.settings.rtl ? stage.top + this.$stage.height() - this.getLength() + this.settings.margin : stage.top,
				};
			} else {
				stage = {
					x: this.settings.rtl ? stage.left + this.$stage.width() - this.getLength() + this.settings.margin : stage.left,
					y: stage.top
				};
			}
		}

		if (this.is('animating')) {
			$.support.transform ? this.animate((this.settings.vertical ? stage.y : stage.x)) : this.$stage.stop()
			this.invalidate('position');
		}

		this.$element.toggleClass(this.options.grabClass, event.type === 'mousedown');

		this.speed(0);

		this._drag.time = new Date().getTime();
		this._drag.target = $(event.target);
		this._drag.stage.start = stage;
		this._drag.stage.current = stage;
		this._drag.pointer = this.pointer(event);

		$(document).on('mouseup.owl.core touchend.owl.core', $.proxy(this.onDragEnd, this));

		$(document).one('mousemove.owl.core touchmove.owl.core', $.proxy(function(event) {
			var delta = this.difference(this._drag.pointer, this.pointer(event));

			$(document).on('mousemove.owl.core touchmove.owl.core', $.proxy(this.onDragMove, this));

			if (Math.abs(this.settings.vertical ? delta.y : delta.x) < Math.abs(this.settings.vertical ? delta.x : delta.y) && this.is('valid')) {
				return;
			}

			event.preventDefault();

			this.enter('dragging');
			this.trigger('drag');
		}, this));
	};

	/**
	 * Handles the `touchmove` and `mousemove` events.
	 * @todo #261
	 * @protected
	 * @param {Event} event - The event arguments.
	 */
	Owl.prototype.onDragMove = function(event) {
		var minimum = null,
			maximum = null,
			pull = null,
			delta = this.difference(this._drag.pointer, this.pointer(event)),
			stage = this.difference(this._drag.stage.start, delta);

		if (!this.is('dragging')) {
			return;
		}

		event.preventDefault();

		if (this.settings.loop) {
			minimum = this.coordinates(this.minimum());
			maximum = this.coordinates(this.maximum() + 1) - minimum;
			stage[this.settings.vertical ? 'y' : 'x'] = (((stage[this.settings.vertical ? 'y' : 'x'] - minimum) % maximum + maximum) % maximum) + minimum;
		} else {
			minimum = this.settings.rtl ? this.coordinates(this.maximum()) : this.coordinates(this.minimum());
			maximum = this.settings.rtl ? this.coordinates(this.minimum()) : this.coordinates(this.maximum());
			pull = this.settings.pullDrag ? -1 * (this.settings.vertical ? delta.y : delta.x) / 5 : 0;
			stage[this.settings.vertical ? 'y' : 'x'] = Math.max(Math.min((this.settings.vertical ? stage.y : stage.x), minimum + pull), maximum + pull);
		}

		this._drag.stage.current = stage;

		this.animate(this.settings.vertical ? stage.y : stage.x);
	};

	/**
	 * Handles the `touchend` and `mouseup` events.
	 * @todo #261
	 * @todo Threshold for click event
	 * @protected
	 * @param {Event} event - The event arguments.
	 */
	Owl.prototype.onDragEnd = function(event) {
		var delta = this.difference(this._drag.pointer, this.pointer(event)),
			stage = this._drag.stage.current,
			direction = (this.settings.vertical ? delta.y : delta.x) > 0 ^ this.settings.rtl ? 'left' : 'right';

		$(document).off('.owl.core');

		this.$element.removeClass(this.options.grabClass);

		if ((this.settings.vertical ? delta.y : delta.x) !== 0 && this.is('dragging') || !this.is('valid')) {
			this.speed(this.settings.dragEndSpeed || this.settings.smartSpeed);
			this.current(this.closest((this.settings.vertical ? stage.y : stage.x), (this.settings.vertical ? delta.y : delta.x) !== 0 ? direction : this._drag.direction));
			this.invalidate('position');
			this.update();

			this._drag.direction = direction;

			if (Math.abs(this.settings.vertical ? delta.y : delta.x) > 3 || new Date().getTime() - this._drag.time > 300) {
				this._drag.target.one('click.owl.core', function() { return false; });
			}
		}

		if (!this.is('dragging')) {
			return;
		}

		this.leave('dragging');
		this.trigger('dragged');
	};

	/**
	 * Gets absolute position of the closest item for a coordinate.
	 * @todo Setting `freeDrag` makes `closest` not reusable. See #165.
	 * @protected
	 * @param {Number} coordinate - The coordinate in pixel.
	 * @param {String} direction - The direction to check for the closest item. Ether `left` or `right`.
	 * @return {Number} - The absolute position of the closest item.
	 */
	Owl.prototype.closest = function(coordinate, direction) {
		var position = -1,
			pull = 30,
			_length = this.getLength(),
			coordinates = this.coordinates();

		if (!this.settings.freeDrag) {
			// check closest item
			$.each(coordinates, $.proxy(function(index, value) {
				if (coordinate > value - pull && coordinate < value + pull) {
					position = index;
				} else if (this.op(coordinate, '<', value)
					&& this.op(coordinate, '>', coordinates[index + 1] || value - _length)) {
					position = direction === 'left' ? index + 1 : index;
				}
				return position === -1;
			}, this));
		}

		if (!this.settings.loop) {
			// non loop boundries
			if (this.op(coordinate, '>', coordinates[this.minimum()])) {
				position = coordinate = this.minimum();
			} else if (this.op(coordinate, '<', coordinates[this.maximum()])) {
				position = coordinate = this.maximum();
			}
		}

		return position;
	};

	/**
	 * Animates the stage.
	 * @todo #270
	 * @public
	 * @param {Number} coordinate - The coordinate in pixels.
	 */
	Owl.prototype.animate = function(coordinate) {
		var animate = this.speed() > 0;

		this.is('animating') && this.onTransitionEnd();

		if (animate) {
			this.enter('animating');
			this.trigger('translate');
		}

		if ($.support.transform3d && $.support.transition) {

			this.$stage.css({
				transform: this.options.vertical ? 'translate3d(0px,' + coordinate + 'px,0px)' : 'translate3d(' + coordinate + 'px,0px,0px)',
				transition: (this.speed() / 1000) + 's'
			});
		} else if (animate) {

			var css = {};
			css[this.options.vertical ? 'top' : 'left'] = coordinate + 'px';
			this.$stage.animate(css, this.speed(), this.settings.fallbackEasing, $.proxy(this.onTransitionEnd, this));
		} else {
			this.$stage.css(this.options.vertical ? 'top' : 'left', coordinate + 'px');
		}
	};

	/**
	 * Checks whether the carousel is in a specific state or not.
	 * @param {String} state - The state to check.
	 * @returns {Boolean} - The flag which indicates if the carousel is busy.
	 */
	Owl.prototype.is = function(state) {
		return this._states.current[state] && this._states.current[state] > 0;
	};

	/**
	 * Sets the absolute position of the current item.
	 * @public
	 * @param {Number} [position] - The new absolute position or nothing to leave it unchanged.
	 * @returns {Number} - The absolute position of the current item.
	 */
	Owl.prototype.current = function(position) {
		if (position === undefined) {
			return this._current;
		}

		if (this._items.length === 0) {
			return undefined;
		}

		position = this.normalize(position);

		if (this._current !== position) {
			var event = this.trigger('change', { property: { name: 'position', value: position } });

			if (event.data !== undefined) {
				position = this.normalize(event.data);
			}

			this._current = position;

			this.invalidate('position');

			this.trigger('changed', { property: { name: 'position', value: this._current } });
		}

		return this._current;
	};

	/**
	 * Invalidates the given part of the update routine.
	 * @param {String} [part] - The part to invalidate.
	 * @returns {Array.<String>} - The invalidated parts.
	 */
	Owl.prototype.invalidate = function(part) {
		if ($.type(part) === 'string') {
			this._invalidated[part] = true;
			this.is('valid') && this.leave('valid');
		}
		return $.map(this._invalidated, function(v, i) { return i });
	};

	/**
	 * Resets the absolute position of the current item.
	 * @public
	 * @param {Number} position - The absolute position of the new item.
	 */
	Owl.prototype.reset = function(position) {
		position = this.normalize(position);

		if (position === undefined) {
			return;
		}

		this._speed = 0;
		this._current = position;

		this.suppress([ 'translate', 'translated' ]);

		this.animate(this.coordinates(position));

		this.release([ 'translate', 'translated' ]);
	};

	/**
	 * Normalizes an absolute or a relative position of an item.
	 * @public
	 * @param {Number} position - The absolute or relative position to normalize.
	 * @param {Boolean} [relative=false] - Whether the given position is relative or not.
	 * @returns {Number} - The normalized position.
	 */
	Owl.prototype.normalize = function(position, relative) {
		var n = this._items.length,
			m = relative ? 0 : this._clones.length;

		if (!$.isNumeric(position) || n < 1) {
			position = undefined;
		} else if (position < 0 || position >= n + m) {
			position = ((position - m / 2) % n + n) % n + m / 2;
		}

		return position;
	};

	/**
	 * Converts an absolute position of an item into a relative one.
	 * @public
	 * @param {Number} position - The absolute position to convert.
	 * @returns {Number} - The converted position.
	 */
	Owl.prototype.relative = function(position) {
		position -= this._clones.length / 2;
		return this.normalize(position, true);
	};

	/**
	 * Gets the maximum position for the current item.
	 * @public
	 * @param {Boolean} [relative=false] - Whether to return an absolute position or a relative position.
	 * @returns {Number}
	 */
	Owl.prototype.maximum = function(relative) {
		var settings = this.settings,
			maximum = this._coordinates.length,
			boundary = Math.abs(this._coordinates[maximum - 1]) - this._length,
			i = -1, j;

		if (settings.loop) {
			maximum = this._clones.length / 2 + this._items.length - 1;
		} else if (settings.autoWidth || settings.merge) {
			// binary search
			while (maximum - i > 1) {
				Math.abs(this._coordinates[j = maximum + i >> 1]) < boundary
					? i = j : maximum = j;
			}
		} else if (settings.center) {
			maximum = this._items.length - 1;
		} else {
			maximum = this._items.length - settings.items;
		}

		if (relative) {
			maximum -= this._clones.length / 2;
		}

		return Math.max(maximum, 0);
	};

	/**
	 * Gets the minimum position for the current item.
	 * @public
	 * @param {Boolean} [relative=false] - Whether to return an absolute position or a relative position.
	 * @returns {Number}
	 */
	Owl.prototype.minimum = function(relative) {
		return relative ? 0 : this._clones.length / 2;
	};

	/**
	 * Gets an item at the specified relative position.
	 * @public
	 * @param {Number} [position] - The relative position of the item.
	 * @return {jQuery|Array.<jQuery>} - The item at the given position or all items if no position was given.
	 */
	Owl.prototype.items = function(position) {
		if (position === undefined) {
			return this._items.slice();
		}

		position = this.normalize(position, true);
		return this._items[position];
	};

	/**
	 * Gets an item at the specified relative position.
	 * @public
	 * @param {Number} [position] - The relative position of the item.
	 * @return {jQuery|Array.<jQuery>} - The item at the given position or all items if no position was given.
	 */
	Owl.prototype.mergers = function(position) {
		if (position === undefined) {
			return this._mergers.slice();
		}

		position = this.normalize(position, true);
		return this._mergers[position];
	};

	/**
	 * Gets the absolute positions of clones for an item.
	 * @public
	 * @param {Number} [position] - The relative position of the item.
	 * @returns {Array.<Number>} - The absolute positions of clones for the item or all if no position was given.
	 */
	Owl.prototype.clones = function(position) {
		var odd = this._clones.length / 2,
			even = odd + this._items.length,
			map = function(index) { return index % 2 === 0 ? even + index / 2 : odd - (index + 1) / 2 };

		if (position === undefined) {
			return $.map(this._clones, function(v, i) { return map(i) });
		}

		return $.map(this._clones, function(v, i) { return v === position ? map(i) : null });
	};

	/**
	 * Sets the current animation speed.
	 * @public
	 * @param {Number} [speed] - The animation speed in milliseconds or nothing to leave it unchanged.
	 * @returns {Number} - The current animation speed in milliseconds.
	 */
	Owl.prototype.speed = function(speed) {
		if (speed !== undefined) {
			this._speed = speed;
		}

		return this._speed;
	};

	/**
	 * Gets the coordinate of an item.
	 * @todo The name of this method is missleanding.
	 * @public
	 * @param {Number} position - The absolute position of the item within `minimum()` and `maximum()`.
	 * @returns {Number|Array.<Number>} - The coordinate of the item in pixel or all coordinates.
	 */
	Owl.prototype.coordinates = function(position) {
		var coordinate = null;

		if (position === undefined) {
			return $.map(this._coordinates, $.proxy(function(coordinate, index) {
				return this.coordinates(index);
			}, this));
		}

		if (this.settings.center) {
			coordinate = this._coordinates[position];
			coordinate += (this.getLength() - coordinate + (this._coordinates[position - 1] || 0)) / 2 * (this.settings.rtl ? -1 : 1);
		} else {
			coordinate = this._coordinates[position - 1] || 0;
		}

		return coordinate;
	};

	/**
	 * Calculates the speed for a translation.
	 * @protected
	 * @param {Number} from - The absolute position of the start item.
	 * @param {Number} to - The absolute position of the target item.
	 * @param {Number} [factor=undefined] - The time factor in milliseconds.
	 * @returns {Number} - The time in milliseconds for the translation.
	 */
	Owl.prototype.duration = function(from, to, factor) {
		return Math.min(Math.max(Math.abs(to - from), 1), 6) * Math.abs((factor || this.settings.smartSpeed));
	};

	/**
	 * Slides to the specified item.
	 * @public
	 * @param {Number} position - The position of the item.
	 * @param {Number} [speed] - The time in milliseconds for the transition.
	 */
	Owl.prototype.to = function(position, speed) {
		var current = this.current(),
			revert = null,
			distance = position - this.relative(current),
			direction = (distance > 0) - (distance < 0),
			items = this._items.length,
			minimum = this.minimum(),
			maximum = this.maximum();

		if (this.settings.loop) {
			if (!this.settings.rewind && Math.abs(distance) > items / 2) {
				distance += direction * -1 * items;
			}

			position = current + distance;
			revert = ((position - minimum) % items + items) % items + minimum;

			if (revert !== position && revert - distance <= maximum && revert - distance > 0) {
				current = revert - distance;
				position = revert;
				this.reset(current);
			}
		} else if (this.settings.rewind) {
			maximum += 1;
			position = (position % maximum + maximum) % maximum;
		} else {
			position = Math.max(minimum, Math.min(maximum, position));
		}

		this.speed(this.duration(current, position, speed));
		this.current(position);

		if (this.$element.is(':visible')) {
			this.update();
		}
	};

	/**
	 * Slides to the next item.
	 * @public
	 * @param {Number} [speed] - The time in milliseconds for the transition.
	 */
	Owl.prototype.next = function(speed) {
		speed = speed || false;
		this.to(this.relative(this.current()) + 1, speed);
	};

	/**
	 * Slides to the previous item.
	 * @public
	 * @param {Number} [speed] - The time in milliseconds for the transition.
	 */
	Owl.prototype.prev = function(speed) {
		speed = speed || false;
		this.to(this.relative(this.current()) - 1, speed);
	};

	/**
	 * Handles the end of an animation.
	 * @protected
	 * @param {Event} event - The event arguments.
	 */
	Owl.prototype.onTransitionEnd = function(event) {

		// if css2 animation then event object is undefined
		if (event !== undefined) {
			event.stopPropagation();

			// Catch only owl-stage transitionEnd event
			if ((event.target || event.srcElement || event.originalTarget) !== this.$stage.get(0)) {
				return false;
			}
		}

		this.leave('animating');
		this.trigger('translated');
	};

	/**
	 * Gets viewport width.
	 * @protected
	 * @return {Number} - The width in pixel.
	 */
	Owl.prototype.viewport = function() {
        var length;
        if (this.options.responsiveBaseElement !== window) {
            length = $(this.options.responsiveBaseElement)[this.options.vertical ? 'height' : 'width']();
        } else {

            if (window['inner'+this.options.vertical ? 'Height' : 'Width']) {
                length = window['inner'+this.options.vertical ? 'Height' : 'Width'];
            } else if (document.documentElement && document.documentElement['client'+this.options.vertical ? 'Height' : 'Width']) {
                length = document.documentElement['client'+this.options.vertical ? 'Height' : 'Width'];
            } else {
                length =  $(window)[this.options.vertical ? 'height' : 'width']();
                //throw 'Can not detect viewport length.';
            }
        }
        return length;
	};

	/**
	 * Replaces the current content.
	 * @public
	 * @param {HTMLElement|jQuery|String} content - The new content.
	 */
	Owl.prototype.replace = function(content) {
		this.$stage.empty();
		this._items = [];

		if (content) {
			content = (content instanceof jQuery) ? content : $(content);
		}

		if (this.settings.nestedItemSelector) {
			content = content.find('.' + this.settings.nestedItemSelector);
		}

		content.filter(function() {
			return this.nodeType === 1;
		}).each($.proxy(function(index, item) {
			item = this.prepare(item);
			this.$stage.append(item);
			this._items.push(item);
			this._mergers.push(item.find('[data-merge]').addBack('[data-merge]').attr('data-merge') * 1 || 1);
		}, this));

		this.reset($.isNumeric(this.settings.startPosition) ? this.settings.startPosition : 0);

		this.invalidate('items');
	};

	/**
	 * Adds an item.
	 * @todo Use `item` instead of `content` for the event arguments.
	 * @public
	 * @param {HTMLElement|jQuery|String} content - The item content to add.
	 * @param {Number} [position] - The relative position at which to insert the item otherwise the item will be added to the end.
	 */
	Owl.prototype.add = function(content, position) {
		var current = this.relative(this._current);

		position = position === undefined ? this._items.length : this.normalize(position, true);
		content = content instanceof jQuery ? content : $(content);

		this.trigger('add', { content: content, position: position });

		content = this.prepare(content);

		if (this._items.length === 0 || position === this._items.length) {
			this._items.length === 0 && this.$stage.append(content);
			this._items.length !== 0 && this._items[position - 1].after(content);
			this._items.push(content);
			this._mergers.push(content.find('[data-merge]').addBack('[data-merge]').attr('data-merge') * 1 || 1);
		} else {
			this._items[position].before(content);
			this._items.splice(position, 0, content);
			this._mergers.splice(position, 0, content.find('[data-merge]').addBack('[data-merge]').attr('data-merge') * 1 || 1);
		}

		this._items[current] && this.reset(this._items[current].index());

		this.invalidate('items');

		this.trigger('added', { content: content, position: position });
	};

	/**
	 * Removes an item by its position.
	 * @todo Use `item` instead of `content` for the event arguments.
	 * @public
	 * @param {Number} position - The relative position of the item to remove.
	 */
	Owl.prototype.remove = function(position) {
		position = this.normalize(position, true);

		if (position === undefined) {
			return;
		}

		this.trigger('remove', { content: this._items[position], position: position });

		this._items[position].remove();
		this._items.splice(position, 1);
		this._mergers.splice(position, 1);

		this.invalidate('items');

		this.trigger('removed', { content: null, position: position });
	};

	/**
	 * Preloads images with auto width.
	 * @todo Replace by a more generic approach
	 * @protected
	 */
	Owl.prototype.preloadAutoWidthImages = function(images) {
		images.each($.proxy(function(i, element) {
			this.enter('pre-loading');
			element = $(element);
			$(new Image()).one('load', $.proxy(function(e) {
				element.attr('src', e.target.src);
				element.css('opacity', 1);
				this.leave('pre-loading');
				!this.is('pre-loading') && !this.is('initializing') && this.refresh();
			}, this)).attr('src', element.attr('src') || element.attr('data-src') || element.attr('data-src-retina'));
		}, this));
	};

	/**
	 * Destroys the carousel.
	 * @public
	 */
	Owl.prototype.destroy = function() {

		this.$element.off('.owl.core');
		this.$stage.off('.owl.core');
		$(document).off('.owl.core');

		if (this.settings.responsive !== false) {
			window.clearTimeout(this.resizeTimer);
			this.off(window, 'resize', this._handlers.onThrottledResize);
		}

		for (var i in this._plugins) {
			this._plugins[i].destroy();
		}

		this.$stage.children('.cloned').remove();

		this.$stage.unwrap();
		this.$stage.children().contents().unwrap();
		this.$stage.children().unwrap();

		this.$element
			.removeClass(this.options.refreshClass)
			.removeClass(this.options.loadingClass)
			.removeClass(this.options.loadedClass)
			.removeClass(this.options.rtlClass)
			.removeClass(this.options.dragClass)
			.removeClass(this.options.grabClass)
			.attr('class', this.$element.attr('class').replace(new RegExp(this.options.responsiveClass + '-\\S+\\s', 'g'), ''))
			.removeData('owl.carousel');
	};

	/**
	 * Operators to calculate right-to-left and left-to-right.
	 * @protected
	 * @param {Number} [a] - The left side operand.
	 * @param {String} [o] - The operator.
	 * @param {Number} [b] - The right side operand.
	 */
	Owl.prototype.op = function(a, o, b) {
		var rtl = this.settings.rtl;
		switch (o) {
			case '<':
				return rtl ? a > b : a < b;
			case '>':
				return rtl ? a < b : a > b;
			case '>=':
				return rtl ? a <= b : a >= b;
			case '<=':
				return rtl ? a >= b : a <= b;
			default:
				break;
		}
	};

	/**
	 * Attaches to an internal event.
	 * @protected
	 * @param {HTMLElement} element - The event source.
	 * @param {String} event - The event name.
	 * @param {Function} listener - The event handler to attach.
	 * @param {Boolean} capture - Wether the event should be handled at the capturing phase or not.
	 */
	Owl.prototype.on = function(element, event, listener, capture) {
		if (element.addEventListener) {
			element.addEventListener(event, listener, capture);
		} else if (element.attachEvent) {
			element.attachEvent('on' + event, listener);
		}
	};

	/**
	 * Detaches from an internal event.
	 * @protected
	 * @param {HTMLElement} element - The event source.
	 * @param {String} event - The event name.
	 * @param {Function} listener - The attached event handler to detach.
	 * @param {Boolean} capture - Wether the attached event handler was registered as a capturing listener or not.
	 */
	Owl.prototype.off = function(element, event, listener, capture) {
		if (element.removeEventListener) {
			element.removeEventListener(event, listener, capture);
		} else if (element.detachEvent) {
			element.detachEvent('on' + event, listener);
		}
	};

	/**
	 * Triggers a public event.
	 * @todo Remove `status`, `relatedTarget` should be used instead.
	 * @protected
	 * @param {String} name - The event name.
	 * @param {*} [data=null] - The event data.
	 * @param {String} [namespace=carousel] - The event namespace.
	 * @param {String} [state] - The state which is associated with the event.
	 * @param {Boolean} [enter=false] - Indicates if the call enters the specified state or not.
	 * @returns {Event} - The event arguments.
	 */
	Owl.prototype.trigger = function(name, data, namespace, state, enter) {
		var status = {
			item: { count: this._items.length, index: this.current() }
		}, handler = $.camelCase(
			$.grep([ 'on', name, namespace ], function(v) { return v })
				.join('-').toLowerCase()
		), event = $.Event(
			[ name, 'owl', namespace || 'carousel' ].join('.').toLowerCase(),
			$.extend({ relatedTarget: this }, status, data)
		);

		if (!this._supress[name]) {
			$.each(this._plugins, function(name, plugin) {
				if (plugin.onTrigger) {
					plugin.onTrigger(event);
				}
			});

			this.register({ type: Owl.Type.Event, name: name });
			this.$element.trigger(event);

			if (this.settings && typeof this.settings[handler] === 'function') {
				this.settings[handler].call(this, event);
			}
		}

		return event;
	};

	/**
	 * Enters a state.
	 * @param name - The state name.
	 */
	Owl.prototype.enter = function(name) {
		$.each([ name ].concat(this._states.tags[name] || []), $.proxy(function(i, name) {
			if (this._states.current[name] === undefined) {
				this._states.current[name] = 0;
			}

			this._states.current[name]++;
		}, this));
	};

	/**
	 * Leaves a state.
	 * @param name - The state name.
	 */
	Owl.prototype.leave = function(name) {
		$.each([ name ].concat(this._states.tags[name] || []), $.proxy(function(i, name) {
			this._states.current[name]--;
		}, this));
	};

	/**
	 * Registers an event or state.
	 * @public
	 * @param {Object} object - The event or state to register.
	 */
	Owl.prototype.register = function(object) {
		if (object.type === Owl.Type.Event) {
			if (!$.event.special[object.name]) {
				$.event.special[object.name] = {};
			}

			if (!$.event.special[object.name].owl) {
				var _default = $.event.special[object.name]._default;
				$.event.special[object.name]._default = function(e) {
					if (_default && _default.apply && (!e.namespace || e.namespace.indexOf('owl') === -1)) {
						return _default.apply(this, arguments);
					}
					return e.namespace && e.namespace.indexOf('owl') > -1;
				};
				$.event.special[object.name].owl = true;
			}
		} else if (object.type === Owl.Type.State) {
			if (!this._states.tags[object.name]) {
				this._states.tags[object.name] = object.tags;
			} else {
				this._states.tags[object.name] = this._states.tags[object.name].concat(object.tags);
			}

			this._states.tags[object.name] = $.grep(this._states.tags[object.name], $.proxy(function(tag, i) {
				return $.inArray(tag, this._states.tags[object.name]) === i;
			}, this));
		}
	};

	/**
	 * Suppresses events.
	 * @protected
	 * @param {Array.<String>} events - The events to suppress.
	 */
	Owl.prototype.suppress = function(events) {
		$.each(events, $.proxy(function(index, event) {
			this._supress[event] = true;
		}, this));
	};

	/**
	 * Releases suppressed events.
	 * @protected
	 * @param {Array.<String>} events - The events to release.
	 */
	Owl.prototype.release = function(events) {
		$.each(events, $.proxy(function(index, event) {
			delete this._supress[event];
		}, this));
	};

	/**
	 * Gets unified pointer coordinates from event.
	 * @todo #261
	 * @protected
	 * @param {Event} - The `mousedown` or `touchstart` event.
	 * @returns {Object} - Contains `x` and `y` coordinates of current pointer position.
	 */
	Owl.prototype.pointer = function(event) {
		var result = { x: null, y: null };

		event = event.originalEvent || event || window.event;

		event = event.touches && event.touches.length ?
			event.touches[0] : event.changedTouches && event.changedTouches.length ?
				event.changedTouches[0] : event;

		if (event.pageX) {
			result.x = event.pageX;
			result.y = event.pageY;
		} else {
			result.x = event.clientX;
			result.y = event.clientY;
		}

		return result;
	};

	/**
	 * Gets the difference of two vectors.
	 * @todo #261
	 * @protected
	 * @param {Object} - The first vector.
	 * @param {Object} - The second vector.
	 * @returns {Object} - The difference.
	 */
	Owl.prototype.difference = function(first, second) {
		return {
			x: first.x - second.x,
			y: first.y - second.y
		};
	};

	/**
	 * The jQuery Plugin for the Owl Carousel
	 * @todo Navigation plugin `next` and `prev`
	 * @public
	 */
	$.fn.owlCarousel = function(option) {
		var args = Array.prototype.slice.call(arguments, 1);

		return this.each(function() {
			var $this = $(this),
				data = $this.data('owl.carousel');

			if (!data) {
				data = new Owl(this, typeof option == 'object' && option);
				$this.data('owl.carousel', data);

				$.each([
					'next', 'prev', 'to', 'destroy', 'refresh', 'replace', 'add', 'remove'
				], function(i, event) {
					data.register({ type: Owl.Type.Event, name: event });
					data.$element.on(event + '.owl.carousel.core', $.proxy(function(e) {
						if (e.namespace && e.relatedTarget !== this) {
							this.suppress([ event ]);
							data[event].apply(this, [].slice.call(arguments, 1));
							this.release([ event ]);
						}
					}, data));
				});
			}

			if (typeof option == 'string' && option.charAt(0) !== '_') {
				data[option].apply(data, args);
			}
		});
	};

	/**
	 * The constructor for the jQuery Plugin
	 * @public
	 */
	$.fn.owlCarousel.Constructor = Owl;

})(window.Zepto || window.jQuery, window, document);

/**
 * AutoRefresh Plugin
 * @version 2.0.0
 * @author Artus Kolanowski
 * @license The MIT License (MIT)
 */
;(function($, window, document, undefined) {

	/**
	 * Creates the auto refresh plugin.
	 * @class The Auto Refresh Plugin
	 * @param {Owl} carousel - The Owl Carousel
	 */
	var AutoRefresh = function(carousel) {
		/**
		 * Reference to the core.
		 * @protected
		 * @type {Owl}
		 */
		this._core = carousel;

		/**
		 * Refresh interval.
		 * @protected
		 * @type {number}
		 */
		this._interval = null;

		/**
		 * Whether the element is currently visible or not.
		 * @protected
		 * @type {Boolean}
		 */
		this._visible = null;

		/**
		 * All event handlers.
		 * @protected
		 * @type {Object}
		 */
		this._handlers = {
			'initialized.owl.carousel': $.proxy(function(e) {
				if (e.namespace && this._core.settings.autoRefresh) {
					this.watch();
				}
			}, this)
		};

		// set default options
		this._core.options = $.extend({}, AutoRefresh.Defaults, this._core.options);

		// register event handlers
		this._core.$element.on(this._handlers);
	};

	/**
	 * Default options.
	 * @public
	 */
	AutoRefresh.Defaults = {
		autoRefresh: true,
		autoRefreshInterval: 500
	};

	/**
	 * Watches the element.
	 */
	AutoRefresh.prototype.watch = function() {
		if (this._interval) {
			return;
		}

		this._visible = this._core.$element.is(':visible');
		this._interval = window.setInterval($.proxy(this.refresh, this), this._core.settings.autoRefreshInterval);
	};

	/**
	 * Refreshes the element.
	 */
	AutoRefresh.prototype.refresh = function() {
		if (this._core.$element.is(':visible') === this._visible) {
			return;
		}

		this._visible = !this._visible;

		this._core.$element.toggleClass('owl-hidden', !this._visible);

		this._visible && (this._core.invalidate('_length') && this._core.refresh());
	};

	/**
	 * Destroys the plugin.
	 */
	AutoRefresh.prototype.destroy = function() {
		var handler, property;

		window.clearInterval(this._interval);

		for (handler in this._handlers) {
			this._core.$element.off(handler, this._handlers[handler]);
		}
		for (property in Object.getOwnPropertyNames(this)) {
			typeof this[property] != 'function' && (this[property] = null);
		}
	};

	$.fn.owlCarousel.Constructor.Plugins.AutoRefresh = AutoRefresh;

})(window.Zepto || window.jQuery, window, document);

/**
 * Lazy Plugin
 * @version 2.0.0
 * @author Bartosz Wojciechowski
 * @license The MIT License (MIT)
 */
;(function($, window, document, undefined) {

	/**
	 * Creates the lazy plugin.
	 * @class The Lazy Plugin
	 * @param {Owl} carousel - The Owl Carousel
	 */
	var Lazy = function(carousel) {

		/**
		 * Reference to the core.
		 * @protected
		 * @type {Owl}
		 */
		this._core = carousel;

		/**
		 * Already loaded items.
		 * @protected
		 * @type {Array.<jQuery>}
		 */
		this._loaded = [];

		/**
		 * Event handlers.
		 * @protected
		 * @type {Object}
		 */
		this._handlers = {
			'initialized.owl.carousel change.owl.carousel': $.proxy(function(e) {
				if (!e.namespace) {
					return;
				}

				if (!this._core.settings || !this._core.settings.lazyLoad) {
					return;
				}

				if ((e.property && e.property.name == 'position') || e.type == 'initialized') {
					var settings = this._core.settings,
						n = (settings.center && Math.ceil(settings.items / 2) || settings.items),
						i = ((settings.center && n * -1) || 0),
						position = ((e.property && e.property.value) || this._core.current()) + i,
						clones = this._core.clones().length,
						load = $.proxy(function(i, v) { this.load(v) }, this);

					while (i++ < n) {
						this.load(clones / 2 + this._core.relative(position));
						clones && $.each(this._core.clones(this._core.relative(position)), load);
						position++;
					}
				}
			}, this)
		};

		// set the default options
		this._core.options = $.extend({}, Lazy.Defaults, this._core.options);

		// register event handler
		this._core.$element.on(this._handlers);
	}

	/**
	 * Default options.
	 * @public
	 */
	Lazy.Defaults = {
		lazyLoad: false
	}

	/**
	 * Loads all resources of an item at the specified position.
	 * @param {Number} position - The absolute position of the item.
	 * @protected
	 */
	Lazy.prototype.load = function(position) {
		var $item = this._core.$stage.children().eq(position),
			$elements = $item && $item.find('.owl-lazy');

		if (!$elements || $.inArray($item.get(0), this._loaded) > -1) {
			return;
		}

		$elements.each($.proxy(function(index, element) {
			var $element = $(element), image,
				url = (window.devicePixelRatio > 1 && $element.attr('data-src-retina')) || $element.attr('data-src');

			this._core.trigger('load', { element: $element, url: url }, 'lazy');

			if ($element.is('img')) {
				$element.one('load.owl.lazy', $.proxy(function() {
					$element.css('opacity', 1);
					this._core.trigger('loaded', { element: $element, url: url }, 'lazy');
				}, this)).attr('src', url);
			} else {
				image = new Image();
				image.onload = $.proxy(function() {
					$element.css({
						'background-image': 'url(' + url + ')',
						'opacity': '1'
					});
					this._core.trigger('loaded', { element: $element, url: url }, 'lazy');
				}, this);
				image.src = url;
			}
		}, this));

		this._loaded.push($item.get(0));
	}

	/**
	 * Destroys the plugin.
	 * @public
	 */
	Lazy.prototype.destroy = function() {
		var handler, property;

		for (handler in this.handlers) {
			this._core.$element.off(handler, this.handlers[handler]);
		}
		for (property in Object.getOwnPropertyNames(this)) {
			typeof this[property] != 'function' && (this[property] = null);
		}
	};

	$.fn.owlCarousel.Constructor.Plugins.Lazy = Lazy;

})(window.Zepto || window.jQuery, window, document);

/**
 * AutoHeight Plugin
 * @version 2.0.0
 * @author Bartosz Wojciechowski
 * @license The MIT License (MIT)
 */
;(function($, window, document, undefined) {

	/**
	 * Creates the auto height plugin.
	 * @class The Auto Height Plugin
	 * @param {Owl} carousel - The Owl Carousel
	 */
	var AutoHeight = function(carousel) {
		/**
		 * Reference to the core.
		 * @protected
		 * @type {Owl}
		 */
		this._core = carousel;

		/**
		 * All event handlers.
		 * @protected
		 * @type {Object}
		 */
		this._handlers = {
			'initialized.owl.carousel refreshed.owl.carousel': $.proxy(function(e) {
				if (e.namespace && this._core.settings.autoHeight) {
					this.update();
				}
			}, this),
			'changed.owl.carousel': $.proxy(function(e) {
				if (e.namespace && this._core.settings.autoHeight && e.property.name == 'position'){
					this.update();
				}
			}, this),
			'loaded.owl.lazy': $.proxy(function(e) {
				if (e.namespace && this._core.settings.autoHeight
					&& e.element.closest('.' + this._core.settings.itemClass).index() === this._core.current()) {
					this.update();
				}
			}, this)
		};

		// set default options
		this._core.options = $.extend({}, AutoHeight.Defaults, this._core.options);

		// register event handlers
		this._core.$element.on(this._handlers);
	};

	/**
	 * Default options.
	 * @public
	 */
	AutoHeight.Defaults = {
		autoHeight: false,
		autoHeightClass: 'owl-height'
	};

	/**
	 * Updates the view.
	 */
	AutoHeight.prototype.update = function() {
		var start = this._core._current,
			end = start + this._core.settings.items,
			visible = this._core.$stage.children().toArray().slice(start, end);
			heights = [],
			maxheight = 0;

		$.each(visible, function(index, item) {
			heights.push($(item).height());
		});

		maxheight = Math.max.apply(null, heights);

		this._core.$stage.parent()
			.height(maxheight)
			.addClass(this._core.settings.autoHeightClass);
	};

	AutoHeight.prototype.destroy = function() {
		var handler, property;

		for (handler in this._handlers) {
			this._core.$element.off(handler, this._handlers[handler]);
		}
		for (property in Object.getOwnPropertyNames(this)) {
			typeof this[property] != 'function' && (this[property] = null);
		}
	};

	$.fn.owlCarousel.Constructor.Plugins.AutoHeight = AutoHeight;

})(window.Zepto || window.jQuery, window, document);

/**
 * Video Plugin
 * @version 2.0.0
 * @author Bartosz Wojciechowski
 * @license The MIT License (MIT)
 */
;(function($, window, document, undefined) {

	/**
	 * Creates the video plugin.
	 * @class The Video Plugin
	 * @param {Owl} carousel - The Owl Carousel
	 */
	var Video = function(carousel) {
		/**
		 * Reference to the core.
		 * @protected
		 * @type {Owl}
		 */
		this._core = carousel;

		/**
		 * Cache all video URLs.
		 * @protected
		 * @type {Object}
		 */
		this._videos = {};

		/**
		 * Current playing item.
		 * @protected
		 * @type {jQuery}
		 */
		this._playing = null;

		/**
		 * All event handlers.
		 * @todo The cloned content removale is too late
		 * @protected
		 * @type {Object}
		 */
		this._handlers = {
			'initialized.owl.carousel': $.proxy(function(e) {
				if (e.namespace) {
					this._core.register({ type: 'state', name: 'playing', tags: [ 'interacting' ] });
				}
			}, this),
			'resize.owl.carousel': $.proxy(function(e) {
				if (e.namespace && this._core.settings.video && this.isInFullScreen()) {
					e.preventDefault();
				}
			}, this),
			'refreshed.owl.carousel': $.proxy(function(e) {
				if (e.namespace && this._core.is('resizing')) {
					this._core.$stage.find('.cloned .owl-video-frame').remove();
				}
			}, this),
			'changed.owl.carousel': $.proxy(function(e) {
				if (e.namespace && e.property.name === 'position' && this._playing) {
					this.stop();
				}
			}, this),
			'prepared.owl.carousel': $.proxy(function(e) {
				if (!e.namespace) {
					return;
				}

				var $element = $(e.content).find('.owl-video');

				if ($element.length) {
					$element.css('display', 'none');
					this.fetch($element, $(e.content));
				}
			}, this)
		};

		// set default options
		this._core.options = $.extend({}, Video.Defaults, this._core.options);

		// register event handlers
		this._core.$element.on(this._handlers);

		this._core.$element.on('click.owl.video', '.owl-video-play-icon', $.proxy(function(e) {
			this.play(e);
		}, this));
	};

	/**
	 * Default options.
	 * @public
	 */
	Video.Defaults = {
		video: false,
		videoHeight: false,
		videoWidth: false
	};

	/**
	 * Gets the video ID and the type (YouTube/Vimeo only).
	 * @protected
	 * @param {jQuery} target - The target containing the video data.
	 * @param {jQuery} item - The item containing the video.
	 */
	Video.prototype.fetch = function(target, item) {
		var type = target.attr('data-vimeo-id') ? 'vimeo' : 'youtube',
			id = target.attr('data-vimeo-id') || target.attr('data-youtube-id'),
			width = target.attr('data-width') || this._core.settings.videoWidth,
			height = target.attr('data-height') || this._core.settings.videoHeight,
			url = target.attr('href');

		if (url) {
			id = url.match(/(http:|https:|)\/\/(player.|www.)?(vimeo\.com|youtu(be\.com|\.be|be\.googleapis\.com))\/(video\/|embed\/|watch\?v=|v\/)?([A-Za-z0-9._%-]*)(\&\S+)?/);

			if (id[3].indexOf('youtu') > -1) {
				type = 'youtube';
			} else if (id[3].indexOf('vimeo') > -1) {
				type = 'vimeo';
			} else {
				throw new Error('Video URL not supported.');
			}
			id = id[6];
		} else {
			throw new Error('Missing video URL.');
		}

		this._videos[url] = {
			type: type,
			id: id,
			width: width,
			height: height
		};

		item.attr('data-video', url);

		this.thumbnail(target, this._videos[url]);
	};

	/**
	 * Creates video thumbnail.
	 * @protected
	 * @param {jQuery} target - The target containing the video data.
	 * @param {Object} info - The video info object.
	 * @see `fetch`
	 */
	Video.prototype.thumbnail = function(target, video) {
		var tnLink,
			icon,
			path,
			dimensions = video.width && video.height ? 'style="width:' + video.width + 'px;height:' + video.height + 'px;"' : '',
			customTn = target.find('img'),
			srcType = 'src',
			lazyClass = '',
			settings = this._core.settings,
			create = function(path) {
				icon = '<div class="owl-video-play-icon"></div>';

				if (settings.lazyLoad) {
					tnLink = '<div class="owl-video-tn ' + lazyClass + '" ' + srcType + '="' + path + '"></div>';
				} else {
					tnLink = '<div class="owl-video-tn" style="opacity:1;background-image:url(' + path + ')"></div>';
				}
				target.after(tnLink);
				target.after(icon);
			};

		// wrap video content into owl-video-wrapper div
		target.wrap('<div class="owl-video-wrapper"' + dimensions + '></div>');

		if (this._core.settings.lazyLoad) {
			srcType = 'data-src';
			lazyClass = 'owl-lazy';
		}

		// custom thumbnail
		if (customTn.length) {
			create(customTn.attr(srcType));
			customTn.remove();
			return false;
		}

		if (video.type === 'youtube') {
			path = "https://img.youtube.com/vi/" + video.id + "/hqdefault.jpg";
			create(path);
		} else if (video.type === 'vimeo') {
			$.ajax({
				type: 'GET',
				url: 'https://vimeo.com/api/v2/video/' + video.id + '.json',
				jsonp: 'callback',
				dataType: 'jsonp',
				success: function(data) {
					path = data[0].thumbnail_large;
					create(path);
				}
			});
		}
	};

	/**
	 * Stops the current video.
	 * @public
	 */
	Video.prototype.stop = function() {
		this._core.trigger('stop', null, 'video');
		this._playing.find('.owl-video-frame').remove();
		this._playing.removeClass('owl-video-playing');
		this._playing = null;
		this._core.leave('playing');
		this._core.trigger('stopped', null, 'video');
	};

	/**
	 * Starts the current video.
	 * @public
	 * @param {Event} event - The event arguments.
	 */
	Video.prototype.play = function(event) {
		var target = $(event.target),
			item = target.closest('.' + this._core.settings.itemClass),
			video = this._videos[item.attr('data-video')],
			width = video.width || '100%',
			height = video.height || this._core.$stage.height(),
			html;

		if (this._playing) {
			return;
		}

		this._core.enter('playing');
		this._core.trigger('play', null, 'video');

		item = this._core.items(this._core.relative(item.index()));

		this._core.reset(item.index());

		if (video.type === 'youtube') {
			html = '<iframe width="' + width + '" height="' + height + '" src="https://www.youtube.com/embed/' +
				video.id + '?autoplay=1&v=' + video.id + '" frameborder="0" allowfullscreen></iframe>';
		} else if (video.type === 'vimeo') {
			html = '<iframe src="https://player.vimeo.com/video/' + video.id +
				'?autoplay=1" width="' + width + '" height="' + height +
				'" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
		}

		$('<div class="owl-video-frame">' + html + '</div>').insertAfter(item.find('.owl-video'));

		this._playing = item.addClass('owl-video-playing');
	};

	/**
	 * Checks whether an video is currently in full screen mode or not.
	 * @todo Bad style because looks like a readonly method but changes members.
	 * @protected
	 * @returns {Boolean}
	 */
	Video.prototype.isInFullScreen = function() {
		var element = document.fullscreenElement || document.mozFullScreenElement ||
				document.webkitFullscreenElement;

		return element && $(element).parent().hasClass('owl-video-frame');
	};

	/**
	 * Destroys the plugin.
	 */
	Video.prototype.destroy = function() {
		var handler, property;

		this._core.$element.off('click.owl.video');

		for (handler in this._handlers) {
			this._core.$element.off(handler, this._handlers[handler]);
		}
		for (property in Object.getOwnPropertyNames(this)) {
			typeof this[property] != 'function' && (this[property] = null);
		}
	};

	$.fn.owlCarousel.Constructor.Plugins.Video = Video;

})(window.Zepto || window.jQuery, window, document);

/**
 * Animate Plugin
 * @version 2.0.0
 * @author Bartosz Wojciechowski
 * @license The MIT License (MIT)
 */
;(function($, window, document, undefined) {

	/**
	 * Creates the animate plugin.
	 * @class The Navigation Plugin
	 * @param {Owl} scope - The Owl Carousel
	 */
	var Animate = function(scope) {
		this.core = scope;
		this.core.options = $.extend({}, Animate.Defaults, this.core.options);
		this.swapping = true;
		this.previous = undefined;
		this.next = undefined;

		this.handlers = {
			'change.owl.carousel': $.proxy(function(e) {
				if (e.namespace && e.property.name == 'position') {
					this.previous = this.core.current();
					this.next = e.property.value;
				}
			}, this),
			'drag.owl.carousel dragged.owl.carousel translated.owl.carousel': $.proxy(function(e) {
				if (e.namespace) {
					this.swapping = e.type == 'translated';
				}
			}, this),
			'translate.owl.carousel': $.proxy(function(e) {
				if (e.namespace && this.swapping && (this.core.options.animateOut || this.core.options.animateIn)) {
					this.swap();
				}
			}, this)
		};

		this.core.$element.on(this.handlers);
	};

	/**
	 * Default options.
	 * @public
	 */
	Animate.Defaults = {
		animateOut: false,
		animateIn: false
	};

	/**
	 * Toggles the animation classes whenever an translations starts.
	 * @protected
	 * @returns {Boolean|undefined}
	 */
	Animate.prototype.swap = function() {

		if (this.core.settings.items !== 1) {
			return;
		}

		if (!$.support.animation || !$.support.transition) {
			return;
		}

		this.core.speed(0);

		var left,
			clear = $.proxy(this.clear, this),
			previous = this.core.$stage.children().eq(this.previous),
			next = this.core.$stage.children().eq(this.next),
			incoming = this.core.settings.animateIn,
			outgoing = this.core.settings.animateOut;

		if (this.core.current() === this.previous) {
			return;
		}

		if (outgoing) {
			left = this.core.coordinates(this.previous) - this.core.coordinates(this.next);
			previous.one($.support.animation.end, clear)
				.css( { 'left': left + 'px' } )
				.addClass('animated owl-animated-out')
				.addClass(outgoing);
		}

		if (incoming) {
			next.one($.support.animation.end, clear)
				.addClass('animated owl-animated-in')
				.addClass(incoming);
		}
	};

	Animate.prototype.clear = function(e) {
		$(e.target).css( { 'left': '' } )
			.removeClass('animated owl-animated-out owl-animated-in')
			.removeClass(this.core.settings.animateIn)
			.removeClass(this.core.settings.animateOut);
		this.core.onTransitionEnd();
	};

	/**
	 * Destroys the plugin.
	 * @public
	 */
	Animate.prototype.destroy = function() {
		var handler, property;

		for (handler in this.handlers) {
			this.core.$element.off(handler, this.handlers[handler]);
		}
		for (property in Object.getOwnPropertyNames(this)) {
			typeof this[property] != 'function' && (this[property] = null);
		}
	};

	$.fn.owlCarousel.Constructor.Plugins.Animate = Animate;

})(window.Zepto || window.jQuery, window, document);

/**
 * Autoplay Plugin
 * @version 2.0.0
 * @author Bartosz Wojciechowski
 * @author Artus Kolanowski
 * @license The MIT License (MIT)
 */
;(function($, window, document, undefined) {

	/**
	 * Creates the autoplay plugin.
	 * @class The Autoplay Plugin
	 * @param {Owl} scope - The Owl Carousel
	 */
	var Autoplay = function(carousel) {
		/**
		 * Reference to the core.
		 * @protected
		 * @type {Owl}
		 */
		this._core = carousel;

		/**
		 * The autoplay interval.
		 * @type {Number}
		 */
		this._interval = null;

		/**
		 * Indicates whenever the autoplay is paused.
		 * @type {Boolean}
		 */
		this._paused = false;

		/**
		 * All event handlers.
		 * @protected
		 * @type {Object}
		 */
		this._handlers = {
			'changed.owl.carousel': $.proxy(function(e) {
				if (e.namespace && e.property.name === 'settings') {
					if (this._core.settings.autoplay) {
						this.play();
					} else {
						this.stop();
					}
				}
			}, this),
			'initialized.owl.carousel': $.proxy(function(e) {
				if (e.namespace && this._core.settings.autoplay) {
					this.play();
				}
			}, this),
			'play.owl.autoplay': $.proxy(function(e, t, s) {
				if (e.namespace) {
					this.play(t, s);
				}
			}, this),
			'stop.owl.autoplay': $.proxy(function(e) {
				if (e.namespace) {
					this.stop();
				}
			}, this),
			'mouseover.owl.autoplay': $.proxy(function() {
				if (this._core.settings.autoplayHoverPause && this._core.is('rotating')) {
					this.pause();
				}
			}, this),
			'mouseleave.owl.autoplay': $.proxy(function() {
				if (this._core.settings.autoplayHoverPause && this._core.is('rotating')) {
					this.play();
				}
			}, this)
		};

		// register event handlers
		this._core.$element.on(this._handlers);

		// set default options
		this._core.options = $.extend({}, Autoplay.Defaults, this._core.options);
	};

	/**
	 * Default options.
	 * @public
	 */
	Autoplay.Defaults = {
		autoplay: false,
		autoplayTimeout: 5000,
		autoplayHoverPause: false,
		autoplaySpeed: false
	};

	/**
	 * Starts the autoplay.
	 * @public
	 * @param {Number} [timeout] - The interval before the next animation starts.
	 * @param {Number} [speed] - The animation speed for the animations.
	 */
	Autoplay.prototype.play = function(timeout, speed) {
		this._paused = false;

		if (this._core.is('rotating')) {
			return;
		}

		this._core.enter('rotating');

		this._interval = window.setInterval($.proxy(function() {
			if (this._paused || this._core.is('busy') || this._core.is('interacting') || document.hidden) {
				return;
			}
			this._core.next(speed || this._core.settings.autoplaySpeed);
		}, this), timeout || this._core.settings.autoplayTimeout);
	};

	/**
	 * Stops the autoplay.
	 * @public
	 */
	Autoplay.prototype.stop = function() {
		if (!this._core.is('rotating')) {
			return;
		}

		window.clearInterval(this._interval);
		this._core.leave('rotating');
	};

	/**
	 * Stops the autoplay.
	 * @public
	 */
	Autoplay.prototype.pause = function() {
		if (!this._core.is('rotating')) {
			return;
		}

		this._paused = true;
	};

	/**
	 * Destroys the plugin.
	 */
	Autoplay.prototype.destroy = function() {
		var handler, property;

		this.stop();

		for (handler in this._handlers) {
			this._core.$element.off(handler, this._handlers[handler]);
		}
		for (property in Object.getOwnPropertyNames(this)) {
			typeof this[property] != 'function' && (this[property] = null);
		}
	};

	$.fn.owlCarousel.Constructor.Plugins.autoplay = Autoplay;

})(window.Zepto || window.jQuery, window, document);

/**
 * Navigation Plugin
 * @version 2.0.0
 * @author Artus Kolanowski
 * @license The MIT License (MIT)
 */
;(function($, window, document, undefined) {
	'use strict';

	/**
	 * Creates the navigation plugin.
	 * @class The Navigation Plugin
	 * @param {Owl} carousel - The Owl Carousel.
	 */
	var Navigation = function(carousel) {
		/**
		 * Reference to the core.
		 * @protected
		 * @type {Owl}
		 */
		this._core = carousel;

		/**
		 * Indicates whether the plugin is initialized or not.
		 * @protected
		 * @type {Boolean}
		 */
		this._initialized = false;

		/**
		 * The current paging indexes.
		 * @protected
		 * @type {Array}
		 */
		this._pages = [];

		/**
		 * All DOM elements of the user interface.
		 * @protected
		 * @type {Object}
		 */
		this._controls = {};

		/**
		 * Markup for an indicator.
		 * @protected
		 * @type {Array.<String>}
		 */
		this._templates = [];

		/**
		 * The carousel element.
		 * @type {jQuery}
		 */
		this.$element = this._core.$element;

		/**
		 * Overridden methods of the carousel.
		 * @protected
		 * @type {Object}
		 */
		this._overrides = {
			next: this._core.next,
			prev: this._core.prev,
			to: this._core.to
		};

		/**
		 * All event handlers.
		 * @protected
		 * @type {Object}
		 */
		this._handlers = {
			'prepared.owl.carousel': $.proxy(function(e) {
				if (e.namespace && this._core.settings.dotsData) {
					this._templates.push('<div class="' + this._core.settings.dotClass + '">' +
						$(e.content).find('[data-dot]').addBack('[data-dot]').attr('data-dot') + '</div>');
				}
			}, this),
			'added.owl.carousel': $.proxy(function(e) {
				if (e.namespace && this._core.settings.dotsData) {
					this._templates.splice(e.position, 0, this._templates.pop());
				}
			}, this),
			'remove.owl.carousel': $.proxy(function(e) {
				if (e.namespace && this._core.settings.dotsData) {
					this._templates.splice(e.position, 1);
				}
			}, this),
			'changed.owl.carousel': $.proxy(function(e) {
				if (e.namespace && e.property.name == 'position') {
					this.draw();
				}
			}, this),
			'initialized.owl.carousel': $.proxy(function(e) {
				if (e.namespace && !this._initialized) {
					this._core.trigger('initialize', null, 'navigation');
					this.initialize();
					this.update();
					this.draw();
					this._initialized = true;
					this._core.trigger('initialized', null, 'navigation');
				}
			}, this),
			'refreshed.owl.carousel': $.proxy(function(e) {
				if (e.namespace && this._initialized) {
					this._core.trigger('refresh', null, 'navigation');
					this.update();
					this.draw();
					this._core.trigger('refreshed', null, 'navigation');
				}
			}, this)
		};

		// set default options
		this._core.options = $.extend({}, Navigation.Defaults, this._core.options);

		// register event handlers
		this.$element.on(this._handlers);
	};

	/**
	 * Default options.
	 * @public
	 * @todo Rename `slideBy` to `navBy`
	 */
	Navigation.Defaults = {
		nav: false,
		navText: [ 'prev', 'next' ],
		navSpeed: false,
		navElement: 'div',
		navContainer: false,
		navContainerClass: 'owl-nav',
		navClass: [ 'owl-prev', 'owl-next' ],
		slideBy: 1,
		dotClass: 'owl-dot',
		dotsClass: 'owl-dots',
		dots: true,
		dotsEach: false,
		dotsData: false,
		dotsSpeed: false,
		dotsContainer: false
	};

	/**
	 * Initializes the layout of the plugin and extends the carousel.
	 * @protected
	 */
	Navigation.prototype.initialize = function() {
		var override,
			settings = this._core.settings;

		// create DOM structure for relative navigation
		this._controls.$relative = (settings.navContainer ? $(settings.navContainer)
			: $('<div>').addClass(settings.navContainerClass).appendTo(this.$element)).addClass('disabled');

		this._controls.$previous = $('<' + settings.navElement + '>')
			.addClass(settings.navClass[0])
			.html(settings.navText[0])
			.prependTo(this._controls.$relative)
			.on('click', $.proxy(function(e) {
				this.prev(settings.navSpeed);
			}, this));
		this._controls.$next = $('<' + settings.navElement + '>')
			.addClass(settings.navClass[1])
			.html(settings.navText[1])
			.appendTo(this._controls.$relative)
			.on('click', $.proxy(function(e) {
				this.next(settings.navSpeed);
			}, this));

		// create DOM structure for absolute navigation
		if (!settings.dotsData) {
			this._templates = [ $('<div>')
				.addClass(settings.dotClass)
				.append($('<span>'))
				.prop('outerHTML') ];
		}

		this._controls.$absolute = (settings.dotsContainer ? $(settings.dotsContainer)
			: $('<div>').addClass(settings.dotsClass).appendTo(this.$element)).addClass('disabled');

		this._controls.$absolute.on('click', 'div', $.proxy(function(e) {
			var index = $(e.target).parent().is(this._controls.$absolute)
				? $(e.target).index() : $(e.target).parent().index();

			e.preventDefault();

			this.to(index, settings.dotsSpeed);
		}, this));

		// override public methods of the carousel
		for (override in this._overrides) {
			this._core[override] = $.proxy(this[override], this);
		}
	};

	/**
	 * Destroys the plugin.
	 * @protected
	 */
	Navigation.prototype.destroy = function() {
		var handler, control, property, override;

		for (handler in this._handlers) {
			this.$element.off(handler, this._handlers[handler]);
		}
		for (control in this._controls) {
			this._controls[control].remove();
		}
		for (override in this.overides) {
			this._core[override] = this._overrides[override];
		}
		for (property in Object.getOwnPropertyNames(this)) {
			typeof this[property] != 'function' && (this[property] = null);
		}
	};

	/**
	 * Updates the internal state.
	 * @protected
	 */
	Navigation.prototype.update = function() {
		var i, j, k,
			lower = this._core.clones().length / 2,
			upper = lower + this._core.items().length,
			maximum = this._core.maximum(true),
			settings = this._core.settings,
			size = settings.center || settings.autoWidth || settings.dotsData
				? 1 : settings.dotsEach || settings.items;

		if (settings.slideBy !== 'page') {
			settings.slideBy = Math.min(settings.slideBy, settings.items);
		}

		if (settings.dots || settings.slideBy == 'page') {
			this._pages = [];

			for (i = lower, j = 0, k = 0; i < upper; i++) {
				if (j >= size || j === 0) {
					this._pages.push({
						start: Math.min(maximum, i - lower),
						end: i - lower + size - 1
					});
					if (Math.min(maximum, i - lower) === maximum) {
						break;
					}
					j = 0, ++k;
				}
				j += this._core.mergers(this._core.relative(i));
			}
		}
	};

	/**
	 * Draws the user interface.
	 * @todo The option `dotsData` wont work.
	 * @protected
	 */
	Navigation.prototype.draw = function() {
		var difference,
			settings = this._core.settings,
			disabled = this._core.items().length <= settings.items,
			index = this._core.relative(this._core.current()),
			loop = settings.loop || settings.rewind;

		this._controls.$relative.toggleClass('disabled', !settings.nav || disabled);

		if (settings.nav) {
			this._controls.$previous.toggleClass('disabled', !loop && index <= this._core.minimum(true));
			this._controls.$next.toggleClass('disabled', !loop && index >= this._core.maximum(true));
		}

		this._controls.$absolute.toggleClass('disabled', !settings.dots || disabled);

		if (settings.dots) {
			difference = this._pages.length - this._controls.$absolute.children().length;

			if (settings.dotsData && difference !== 0) {
				this._controls.$absolute.html(this._templates.join(''));
			} else if (difference > 0) {
				this._controls.$absolute.append(new Array(difference + 1).join(this._templates[0]));
			} else if (difference < 0) {
				this._controls.$absolute.children().slice(difference).remove();
			}

			this._controls.$absolute.find('.active').removeClass('active');
			this._controls.$absolute.children().eq($.inArray(this.current(), this._pages)).addClass('active');
		}
	};

	/**
	 * Extends event data.
	 * @protected
	 * @param {Event} event - The event object which gets thrown.
	 */
	Navigation.prototype.onTrigger = function(event) {
		var settings = this._core.settings;

		event.page = {
			index: $.inArray(this.current(), this._pages),
			count: this._pages.length,
			size: settings && (settings.center || settings.autoWidth || settings.dotsData
				? 1 : settings.dotsEach || settings.items)
		};
	};

	/**
	 * Gets the current page position of the carousel.
	 * @protected
	 * @returns {Number}
	 */
	Navigation.prototype.current = function() {
		var current = this._core.relative(this._core.current());
		return $.grep(this._pages, $.proxy(function(page, index) {
			return page.start <= current && page.end >= current;
		}, this)).pop();
	};

	/**
	 * Gets the current succesor/predecessor position.
	 * @protected
	 * @returns {Number}
	 */
	Navigation.prototype.getPosition = function(successor) {
		var position, length,
			settings = this._core.settings;

		if (settings.slideBy == 'page') {
			position = $.inArray(this.current(), this._pages);
			length = this._pages.length;
			successor ? ++position : --position;
			position = this._pages[((position % length) + length) % length].start;
		} else {
			position = this._core.relative(this._core.current());
			length = this._core.items().length;
			successor ? position += settings.slideBy : position -= settings.slideBy;
		}

		return position;
	};

	/**
	 * Slides to the next item or page.
	 * @public
	 * @param {Number} [speed=false] - The time in milliseconds for the transition.
	 */
	Navigation.prototype.next = function(speed) {
		$.proxy(this._overrides.to, this._core)(this.getPosition(true), speed);
	};

	/**
	 * Slides to the previous item or page.
	 * @public
	 * @param {Number} [speed=false] - The time in milliseconds for the transition.
	 */
	Navigation.prototype.prev = function(speed) {
		$.proxy(this._overrides.to, this._core)(this.getPosition(false), speed);
	};

	/**
	 * Slides to the specified item or page.
	 * @public
	 * @param {Number} position - The position of the item or page.
	 * @param {Number} [speed] - The time in milliseconds for the transition.
	 * @param {Boolean} [standard=false] - Whether to use the standard behaviour or not.
	 */
	Navigation.prototype.to = function(position, speed, standard) {
		var length;

		if (!standard) {
			length = this._pages.length;
			$.proxy(this._overrides.to, this._core)(this._pages[((position % length) + length) % length].start, speed);
		} else {
			$.proxy(this._overrides.to, this._core)(position, speed);
		}
	};

	$.fn.owlCarousel.Constructor.Plugins.Navigation = Navigation;

})(window.Zepto || window.jQuery, window, document);

/**
 * Hash Plugin
 * @version 2.0.0
 * @author Artus Kolanowski
 * @license The MIT License (MIT)
 */
;(function($, window, document, undefined) {
	'use strict';

	/**
	 * Creates the hash plugin.
	 * @class The Hash Plugin
	 * @param {Owl} carousel - The Owl Carousel
	 */
	var Hash = function(carousel) {
		/**
		 * Reference to the core.
		 * @protected
		 * @type {Owl}
		 */
		this._core = carousel;

		/**
		 * Hash index for the items.
		 * @protected
		 * @type {Object}
		 */
		this._hashes = {};

		/**
		 * The carousel element.
		 * @type {jQuery}
		 */
		this.$element = this._core.$element;

		/**
		 * All event handlers.
		 * @protected
		 * @type {Object}
		 */
		this._handlers = {
			'initialized.owl.carousel': $.proxy(function(e) {
				if (e.namespace && this._core.settings.startPosition === 'URLHash') {
					$(window).trigger('hashchange.owl.navigation');
				}
			}, this),
			'prepared.owl.carousel': $.proxy(function(e) {
				if (e.namespace) {
					var hash = $(e.content).find('[data-hash]').addBack('[data-hash]').attr('data-hash');

					if (!hash) {
						return;
					}

					this._hashes[hash] = e.content;
				}
			}, this),
			'changed.owl.carousel': $.proxy(function(e) {
				if (e.namespace && e.property.name === 'position') {
					var current = this._core.items(this._core.relative(this._core.current())),
						hash = $.map(this._hashes, function(item, hash) {
							return item === current ? hash : null;
						}).join();

					if (!hash || window.location.hash.slice(1) === hash) {
						return;
					}

					window.location.hash = hash;
				}
			}, this)
		};

		// set default options
		this._core.options = $.extend({}, Hash.Defaults, this._core.options);

		// register the event handlers
		this.$element.on(this._handlers);

		// register event listener for hash navigation
		$(window).on('hashchange.owl.navigation', $.proxy(function(e) {
			var hash = window.location.hash.substring(1),
				items = this._core.$stage.children(),
				position = this._hashes[hash] && items.index(this._hashes[hash]);

			if (position === undefined || position === this._core.current()) {
				return;
			}

			this._core.to(this._core.relative(position), false, true);
		}, this));
	};

	/**
	 * Default options.
	 * @public
	 */
	Hash.Defaults = {
		URLhashListener: false
	};

	/**
	 * Destroys the plugin.
	 * @public
	 */
	Hash.prototype.destroy = function() {
		var handler, property;

		$(window).off('hashchange.owl.navigation');

		for (handler in this._handlers) {
			this._core.$element.off(handler, this._handlers[handler]);
		}
		for (property in Object.getOwnPropertyNames(this)) {
			typeof this[property] != 'function' && (this[property] = null);
		}
	};

	$.fn.owlCarousel.Constructor.Plugins.Hash = Hash;

})(window.Zepto || window.jQuery, window, document);

/**
 * Support Plugin
 *
 * @version 2.0.0
 * @author Vivid Planet Software GmbH
 * @author Artus Kolanowski
 * @license The MIT License (MIT)
 */
;(function($, window, document, undefined) {

	var style = $('<support>').get(0).style,
		prefixes = 'Webkit Moz O ms'.split(' '),
		events = {
			transition: {
				end: {
					WebkitTransition: 'webkitTransitionEnd',
					MozTransition: 'transitionend',
					OTransition: 'oTransitionEnd',
					transition: 'transitionend'
				}
			},
			animation: {
				end: {
					WebkitAnimation: 'webkitAnimationEnd',
					MozAnimation: 'animationend',
					OAnimation: 'oAnimationEnd',
					animation: 'animationend'
				}
			}
		},
		tests = {
			csstransforms: function() {
				return !!test('transform');
			},
			csstransforms3d: function() {
				return !!test('perspective');
			},
			csstransitions: function() {
				return !!test('transition');
			},
			cssanimations: function() {
				return !!test('animation');
			}
		};

	function test(property, prefixed) {
		var result = false,
			upper = property.charAt(0).toUpperCase() + property.slice(1);

		$.each((property + ' ' + prefixes.join(upper + ' ') + upper).split(' '), function(i, property) {
			if (style[property] !== undefined) {
				result = prefixed ? property : true;
				return false;
			}
		});

		return result;
	}

	function prefixed(property) {
		return test(property, true);
	}

	if (tests.csstransitions()) {
		/* jshint -W053 */
		$.support.transition = new String(prefixed('transition'))
		$.support.transition.end = events.transition.end[ $.support.transition ];
	}

	if (tests.cssanimations()) {
		/* jshint -W053 */
		$.support.animation = new String(prefixed('animation'))
		$.support.animation.end = events.animation.end[ $.support.animation ];
	}

  
	if (tests.csstransforms()) {
		/* jshint -W053 */
		$.support.transform = new String(prefixed('transform'));
		$.support.transform3d = tests.csstransforms3d();
	}

})(window.Zepto || window.jQuery, window, document);

// $(".owl-carousel").owlCarousel({
//   loop: true,
//   autoplay: true,
//   items: 2,
//   vertical:true,
// });