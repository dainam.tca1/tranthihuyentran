// core js
// $('.lazy').lazy({
//   effect: "fadeIn"
// });
$("main").click(function () {
    $("main").removeClass("bg-tranparent-black");
    $("#result-desktop-search").hide();
})
$("#keywordesktop").focus(function () {
    $("main").addClass("bg-tranparent-black");
    $("#result-desktop-search").show();

});
function submitcontact(obj) {
    var target = $(obj).attr("id");
    $(obj)
        .find(".btn")
        .prop("disabled", true);
    $(obj)
        .find(".btn-loading")
        .addClass("loading");
    if (loading === true) return false;
    $.ajax({
        url: $("#" + target).attr("action"),
        dataType: "json",
        method: "POST",
        data: $(obj).serialize(),
        success: function (response) {
            loading = false;
            if (response.status == "error") {
                $(obj)
                    .find(".btn")
                    .prop("disabled", false);
                $(obj)
                    .find(".btn-loading")
                    .removeClass("loading");
                $("#" + target)
                    .find(".error-lst")
                    .css("display", "block");
                $("#" + target)
                    .find(".error-lst ul")
                    .html("<li><span class='ecs-info-circled-alt'></span>" + response.message + "</li>");
                //alert(response.Msg);
            } else {
                $("#" + target)
                    .find(".error-lst")
                    .css("display", "none");
                $(obj)
                    .find(".btn-loading")
                    .removeClass("loading");
                $("#noti-modal").modal("toggle");
                $("#noti-modal").on("hide.bs.modal", function () {
                    location.reload();
                });
            }
        },
        beforeSend: function () {
            loading = true;
        },
        error: function () {
            loading = false;
        }
    });
    return false;
}
//$("a[href^='#']").click(function (e) {
//    e.preventDefault();

//    var position = $($(this).attr("href")).offset().top;

//    $("body, html").animate({
//        scrollTop: position
//    } /* speed */);
//});



$(document).ready(function () {
    //if (!$.cookie("ispopup")) {
    //  $("#img-modal").modal("toggle");
    //  $.cookie("ispopup", true, { expires: 1 });
    //}
    var width_nav_pc = $("#nav-pc").width();
    var height_categories = $(".catalogdetail").height();
    $(".catalogdetail .nav-child").css({ "width": width_nav_pc, "height": height_categories });

    // Go top
    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $("#scrollup").fadeIn();
        } else {
            $("#scrollup").fadeOut();
        }
    });
    $("#scrollup").click(function () {
        $("html, body").animate(
            {
                scrollTop: 0
            },
            600
        );
        return false;
    });
    // End go top
    // Stick top
    var navdesktop = $("header").height() - 100;
    $(window).scroll(function () {
        if ($(window).scrollTop() > navdesktop) {
            $(".header-body").addClass("nav-fix");
        } else {
            $(".header-body").removeClass("nav-fix");
        }
    });
    // var navmobile = $('#nav-mobile .nav-header').offset();
    // $(window).scroll(function () {
    //     if ($(window).scrollTop() > navmobile.top) {
    //         $('#nav-mobile .nav-header').addClass('nav-fix')
    //     } else {
    //         $('#nav-mobile .nav-header').removeClass('nav-fix')
    //     }
    // });
    // End Stick top
});
// end core js
// global js
var loading = false;
$(document).ready(function () {
    $(".collapse").on("hide.bs.collapse", function (e) {
        if ($(this).is(e.target)) {
            $(this)
                .parent()
                .find("> .collapse-header:first-child > span.icon")
                .html('<i class="demo-icon ecs-down-open-big"></i>');
        }
    });
    $(".collapse").on("show.bs.collapse", function (e) {
        if ($(this).is(e.target)) {
            $(this)
                .parent()
                .find("> .collapse-header:first-child > span.icon")
                .html('<i class="demo-icon ecs-up-open-big"></i>');
        }
    });
    // $("#js-facebook-chat").click(function() {
    //   $(this).toggleClass("open");
    // });
    // var targetfacebook = document.getElementById("js-facebook-content");
    // var targetfacebook1 = document.getElementById("facebook-opacity");
    // $(document).on("click", function(event) {
    //   if (event.target != targetfacebook && event.target != targetfacebook1) {
    //     $("#js-facebook-chat").removeClass("open");
    //   }
    // });
    // $("#show-cart-container .btn-close").click(function(event) {
    //   $("#show-cart-container").toggleClass("showmenu");
    //   $("body").toggleClass("showmenu");
    // });
    // var targetcart = document.getElementById("show-cart-container");
    // $(document).on("click", function(event) {
    //   if (event.target == targetcart) {
    //     $("#show-cart-container").toggleClass("showmenu");
    //     $("body").toggleClass("showmenu");
    //   }
    // });
});
// Menu mobile
$("#menu-btn").click(function () {
    $(this).toggleClass("open");
    $("#nav-mobile .nav-header").toggleClass("showmenu");
    $("#nav-mobile .nav-body").toggleClass("showmenu");
    //$("main").toggleClass("showmenu");
    //$("footer").toggleClass("showmenu");
    $(".nav-parent-action")
        .next()
        .removeClass("showmenu");
    if ($("body").hasClass("showmenu1") == true) {
        $("body").removeClass("showmenu1");
    }
    $("body").toggleClass("showmenu");

    $("#nav-mobile .search-btn-action").removeClass("open");
    $("#nav-mobile .nav-footer").removeClass("showmenu");
});
$("#nav-mobile .search-btn-action").click(function () {
    $(this).toggleClass("open");
    $("#nav-mobile .nav-footer").toggleClass("showmenu");
    if ($("body").hasClass("showmenu") == true) {
        $("body").removeClass("showmenu");
    }
    $("body").toggleClass("showmenu1");
    $("#menu-btn").removeClass("open");
    $("#nav-mobile .nav-header").removeClass("showmenu");
    $("#nav-mobile .nav-body").removeClass("showmenu");
    $("#nav-mobile .nav-parent-action").removeClass("open");
    $("#nav-mobile .nav-parent-action")
        .next()
        .removeClass("showmenu");
});
$("#search-btn-bar").click(function () {
    $(this).toggleClass("open");
    $("#nav-mobile .nav-footer").toggleClass("showmenu");
    if ($("body").hasClass("showmenu") == true) {
        $("body").removeClass("showmenu");
    }
    $("body").toggleClass("showmenu1");
    $("#menu-btn").removeClass("open");
    $("#nav-mobile .nav-header").removeClass("showmenu");
    $("#nav-mobile .nav-body").removeClass("showmenu");
    $("#nav-mobile .nav-parent-action").removeClass("open");
    $("#nav-mobile .nav-parent-action")
        .next()
        .removeClass("showmenu");
});
$("#nav-mobile .nav-parent-action .icon").click(function () {
    $(this)
        .parent()
        .toggleClass("open");
    $(this)
        .parent()
        .next()
        .toggleClass("showmenu");
});
$("#nav-mobile .back-btn").click(function () {
    $(this).toggleClass("open");
    $(this)
        .parent()
        .parent()
        .parent()
        .removeClass("showmenu");
});
// Gửi form
// End gửi form
// end global js
