function addtoCart(itemId) {
    $("#loading").show();
    $.ajax({
      url: "/booking/default/addtocart",
      type: "POST",
      dataType: "json",
      contentType: "application/json",
      data: JSON.stringify({
        itemId: itemId
      }),
      success: function(response){
        $("#loading").hide();
        var data = response.data.ServiceList;
        for(var i = 0 ; i <= (data.length - 1) ; i++){
            $('.btn-book-'+data[i]._id).addClass('btn-default');
            $('.btn-book-'+data[i]._id).removeClass('btn-primary');
            $('.btn-book-'+data[i]._id).html('<i class="demo-icon ecs-cross-out"></i>')
        }
        document.location.href = "/booking";
      },
      error: function(er) {
        $("#loading").hide();
      }
    });
}
function servicedetail(itemid){
  var url = "/service/getDetail";
  $.ajax({
    url: url,
    type: "POST",
    data: {id: itemid},
    success: function (response) {
        var name = response.data.Title;
        var des = response.data.ShortDescription;
        $("#servicedetail-modal").modal("toggle");
        $('#servicedetail-modal .modal-header h4').html(name);
        $('#servicedetail-modal .modal-body p').html(des);
        
    },
});
}