﻿"use strict";

$(document).ready(function () {
  var $zoom = $(".zoom").magnify();
  $("#related-slider").owlCarousel({
    loop: false,
    autoplay: false,
    navText: ["<i class='demo-icon ecs-left-open-big'></i>", "<i class='demo-icon ecs-right-open-big'></i>"],
    margin: 0,
    autoplayHoverPause: false,
    lazyLoad: true,
    autoHeight: false,
    smartSpeed: 1000,
    responsive: {
      0: {
        nav: true,
        dots: false,
        items: 1,
        margin: 15
      },
      768: {
        items: 3,
        dots: true,
        nav: false,
        margin: 10
      },
      1024: {
        items: 4,
        dots: false,
        nav: true,
        margin: 15
      },
      1300: {
        items: 4,
        dots: false,
        nav: true,
        margin: 20
      }
    }
  });
  $(".tab-container").each(function (index, el) {
    var check = $(this).height();
    if (check < 50) {
      $(this).parent().next().hide();
      $(this).parent().css("height", "auto").height();
    }
  });
  // slider
  $("#slider-single").slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    fade: true,
    adaptiveHeight: true,
    infinite: false,
    useTransform: true,
    speed: 1000,
    arrows: false,
    prevArrow: '<div><i class="demo-icon ecs-left-open-big"></i></div>',
    nextArrow: '<div><i class="demo-icon ecs-right-open-big"></div>'
  });

  $("#slider-nav").on("init", function (event, slick) {
    $("#slider-nav .slick-slide.slick-current").addClass("is-active");
  }).slick({
    dots: false,
    focusOnSelect: false,
    slidesToShow: 5,
    slidesToScroll: 1,
    infinite: false,
    vertical: false,
    draggable: false,
    verticalSwiping: false,
    arrows: true,
    speed: 1000,
    prevArrow: '<div><i class="demo-icon ecs-left-open-mini"></i></div>',
    nextArrow: '<div><i class="demo-icon ecs-right-open-mini"></div>',
    responsive: [{
      breakpoint: 1280,
      settings: {
        slidesToShow: 5,
        slidesToScroll: 1
      }
    }, {
      breakpoint: 1025,
      settings: {
        slidesToShow: 4,
        slidesToScroll: 1
      }
    }, {
      breakpoint: 770,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1
      }
    }]
  });

  $("#slider-single").on("afterChange", function (event, slick, currentSlide) {
    $("#slider-nav").slick("slickGoTo", currentSlide);
    var currrentNavSlideElem = '#slider-nav .slick-slide[data-slick-index="' + currentSlide + '"]';
    $("#slider-nav .slick-slide.is-active").removeClass("is-active");
    $(currrentNavSlideElem).addClass("is-active");
    $zoom.destroy().magnify();
  });

  $("#slider-nav").on("click", ".slick-slide", function (event) {
    event.preventDefault();
    var goToSingleSlide = $(this).data("slick-index");

    $("#slider-single").slick("slickGoTo", goToSingleSlide);
  });
  // End slider
  /* End slider and thumbnail */
  $(".slider-5item").owlCarousel({
    loop: false,
    autoplay: true,
    autoplayHoverPause: true,
    navText: ["<i class='demo-icon ecs-left-open-big'></i>", "<i class='demo-icon ecs-right-open-big'></i>"],
    margin: 0,
    lazyLoad: false,
    autoHeight: false,
    smartSpeed: 1000,
    responsive: {
      0: {
        nav: true,
        dots: false,
        items: 2,
        margin: 10
      },
      768: {
        items: 3,
        dots: false,
        nav: true,
        margin: 15
      },
      1024: {
        items: 4,
        nav: true,
        dots: false,
        margin: 15
      },
      1280: {
        items: 5,
        nav: true,
        dots: false,
        margin: 15
      }
    }
  });
});
function openmore(obj) {
  var location = $(obj).parent();
  var target = $(location).prev();
  var el = target;
  curHeight = el.height();
  autoHeight = el.css("height", "auto").height();
  el.height(curHeight).animate({ height: autoHeight }, 1000);
  location.fadeOut();
}
function addReplyComment(id, proid) {
  var form = $("#formreply" + id);
  $.ajax({
    url: $(form).attr("action"),
    type: "POST",
    data: $(form).serialize(),
    beforeSend: function beforeSend() {
      $(".loading").show();
    },
    success: function success(result) {
      $(".loading").hide();
      if (result.Ok) {
        alert("Cảm ơn bạn đã bình luận / đánh giá sản phẩm, chúng tôi sẽ phản hồi trong thời gian sớm nhất.");
        location.reload();
      } else {
        alert(result.Msg);
      }
    }
  });
}

function addComment() {
  var form = $("#addcommentform");
  $.ajax({
    url: $(form).attr("action"),
    type: "POST",
    data: $(form).serialize(),
    beforeSend: function beforeSend() {
      $(".loading").show();
    },
    success: function success(result) {
      $(".loading").hide();
      if (result.Ok) {
        alert("Cảm ơn bạn đã bình luận / đánh giá sản phẩm, chúng tôi sẽ phản hồi trong thời gian sớm nhất.");
        location.reload();
      } else {
        alert(result.Msg);
      }
    }
  });
}

function showreply(objid) {
  $(".formreply").hide();
  $(objid).show();
}

function loadmorecomment(proid, page, pagesize) {
  $.ajax({
    url: "/aj/Product/LoadmoreComment",
    type: "GET",
    data: { productId: proid, page: page, pagesize: pagesize },
    beforeSend: function beforeSend() {
      $(".loading").show();
    },
    success: function success(result) {
      $(".loading").hide();
      $("#comment .col-loadmore").append(result.viewsrc);
      if (result.CurrentPage >= result.TotalPageCount) $("#loadmorecmt").hide();else $("#loadmorecmt").html('<a href="javascript:" class="btn btn-primary" onclick="loadmorecomment(' + proid + "," + (result.CurrentPage + 1) + "," + result.PageSize + ')">Xem thêm</a>');
    }
  });
}

function addRating() {
  var form = $("#addratingform");
  $.ajax({
    url: $(form).attr("action"),
    type: "POST",
    data: $(form).serialize(),
    beforeSend: function beforeSend() {
      $(".loading").show();
    },
    success: function success(result) {
      $(".loading").hide();
      if (result.Ok) {
        alert("Cảm ơn bạn đã bình luận / đánh giá sản phẩm, chúng tôi sẽ phản hồi trong thời gian sớm nhất.");
        location.reload();
      } else {
        alert(result.Msg);
      }
    }
  });
}

function loadmorerating(proid, page, pagesize) {
  $.ajax({
    url: "/aj/Product/LoadmoreRating",
    type: "GET",
    data: { productId: proid, page: page, pagesize: pagesize },
    beforeSend: function beforeSend() {
      $(".loading").show();
    },
    success: function success(result) {
      $(".loading").hide();
      $("#danhgia .col-loadmore").append(result.viewsrc);
      if (result.CurrentPage >= result.TotalPageCount) $("#loadmorerating").hide();else $("#loadmorerating").html('<a href="javascript:" class="btn btn-primary" onclick="loadmorerating(' + proid + "," + (result.CurrentPage + 1) + "," + result.PageSize + ')">Xem thêm</a>');
    }
  });
}
// chose product ver
function initslideevent() {
  var $zoom = $(".zoom").magnify();
  $(".slider-for").slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    asNavFor: ".slider-nav",
    dots: true,
    infinite: false,
    adaptiveHeight: true,
    swipe: false,
    lazyLoad: "progressive"
  });
  $(".slider-nav").slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    asNavFor: ".slider-for",
    arrows: false,
    focusOnSelect: true,
    vertical: true,
    verticalSwiping: true,
    infinite: false,
    draggable: true,
    lazyLoad: "progressive",
    responsive: [{
      breakpoint: 1200,
      settings: {
        slidesToShow: 3
      }
    }, {
      breakpoint: 800,
      settings: {
        slidesToShow: 2
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
    ]
  });
  $(".slider-for").on("afterChange", function (event, slick, currentSlide, nextSlide) {
    $zoom.destroy().magnify();
  });
}

function choosesizecolor(curid) {
  var attrlst = [];
  $(".attrlst").each(function (i, e) {
    attrlst.push($(e).find("input:checked").val());
  });
  $.ajax({
    url: "/aj/product/choosecolorandsize",
    type: "POST",
    data: { attrlst: attrlst, curid: curid },
    beforeSend: function beforeSend() {
      $(".loading").show();
    },
    success: function success(result) {
      $(".loading").hide();
      if (result.Ok) {
        $("#productdetail > .section-header").html(result.Msg);
        initslideevent();
      } else {
        alert(result.Msg);
      }
    }
  });
}
// chose product ver
function selectSizeColor(obj) {
  var id = $(obj).data("id");
  var vid = $(obj).data("vid");
  var skulist = $("#skulist").val();
  console.log(skulist);
  console.log(vid);
  console.log(id);
}

