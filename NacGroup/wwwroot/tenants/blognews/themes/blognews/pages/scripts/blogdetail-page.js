$(document).ready(function() {
    $(".slider-3item").owlCarousel({
        loop: false,
        autoplay: true,
        autoplayHoverPause: true,
        navText: [
            "<i class='demo-icon ecs-left-open-big'></i>",
            "<i class='demo-icon ecs-right-open-big'></i>"
        ],
        margin: 0,
        lazyLoad: false,
        autoHeight: false,
        smartSpeed: 1000,
        responsive: {
            0: {
                nav: false,
                dots: true,
                items: 1,
                margin: 20
            },
            768: {
                items: 2,
                dots: true,
                nav: false,
                margin: 15
            },
            1024: {
                items: 2,
                nav: false,
                dots: true,
                margin: 20
            },
            1280: {
                items: 3,
                nav: false,
                dots: true,
                margin: 25
            }
        }
    });
});
