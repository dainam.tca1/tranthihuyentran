$(document).ready(function () {
    if ($("#type").val() == "Collection") {
        isActiveProduct();
    }
    
})

$("#openform").click(function () {
    var classList = $("#openform").attr('class').split(/\s+/);
    for (var i = 0; i < classList.length; i++) {
        if (classList[i] === 'active') {
            $("#openform").removeClass("active");
            $("#openform").html("Gửi đánh giá của bạn");
            $(".nacratingin-2").fadeOut("slow", function () {
                $(".nacratingin-2").hide();
                $(".nacratingin-form").hide();
            });
            $(".nacratingin-form").fadeOut("slow");
            
            
        } else {
            $("#openform").addClass("active");
            $("#openform").html("Đóng lại");
            $(".nacratingin-form").fadeIn("slow", function () {
                $(".nacratingin-2").css("display", "flex");
                $(".nacratingin-form").show();
            });
            $(".nacratingin-2").fadeIn("slow");
          
            
        }
    }
});
$(".back").click(function () {
    $("#wrap_popup").fadeOut("slow", function () {
        $("#wrap_popup").hide();
    })
    $(".ajaxcomment").css("height", "0");
});
function format2(n, currency) {
    return currency + " " + n.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
}
$("#multi-product").click(function () {
    var skuId = $("#productId").val();
    var sku_arr = [];
    $(".v-product-list-collection .box-gel-kit").each(function () {
        if ($(this).find(".input-check-gel").is(':checked')) {
            var qty = $(this).find(".input-qty-gel").val();
            if (qty > 0) {
                var skuCode = $(this).find(".input-check-gel").val();
                sku_arr.push({id:skuId,code: skuCode,qty:qty});
            } else {
                $(this).find(".input-check-gel").prop("checked", false);
            }
        }
    });
    if (sku_arr.length == 0) {
        alert("Select product")
        return -1;
    }
    $("#loading").show();
    $.ajax({
        url: "/ordernacgroup/addtocartmulti",
        type: "POST",
        dataType: "json",
        contentType: "application/json",
        data: JSON.stringify(sku_arr),
        beforeSend: xhr => {
            $("#loading").show();
        },
        success: response => {
            $("#loading").hide();
            if (response.status == "success") {
                $("#cart-modal").modal("toggle");
                $("#count").text(response.data.NewCart.OrderItems.length);
                //  alertify.error(response.message);
            }
            else {

                alertify.error(response.message);
            }

        },
        error: e => {
            $("#loading").show();
        }
    })
})

$("#multi-product-2").click(function () {
    var skuId = $("#productId").val();
    var sku_arr = [];
    $(".v-product-list-collection .box-gel-kit").each(function () {
        if ($(this).find(".input-check-gel").is(':checked')) {
            var qty = $(this).find(".input-qty-gel").val();
            if (qty > 0) {
                var skuCode = $(this).find(".input-check-gel").val();
                sku_arr.push({ id: skuId, code: skuCode, qty: qty });
            } else {
                $(this).find(".input-check-gel").prop("checked", false);
            }
        }
    });
    if (sku_arr.length == 0) {
        alert("Select product")
        return -1;
    }
    $("#loading").show();
    $.ajax({
        url: "/ordernacgroup/addtocartmulti",
        type: "POST",
        dataType: "json",
        contentType: "application/json",
        data: JSON.stringify(sku_arr),
        beforeSend: xhr => {
            $("#loading").show();
        },
        success: response => {
            $("#loading").hide();
            if (response.status == "success") {
                $("#cart-modal").modal("toggle");
                $("#count").text(response.data.NewCart.OrderItems.length);
                //  alertify.error(response.message);
            }
            else {

                alertify.error(response.message);
            }

        },
        error: e => {
            $("#loading").show();
        }
    })
});

function isActiveProduct() {
    var id = $(this).attr("href");
    $(id).addClass("in active");
    $("#loading").show();
    setTimeout(function () {
        $("#tab-product .tab-pane").each(function (index, element) {
            var active = $(element).hasClass("active");
            var id = $(element).attr("id");
            if (active) {              
                $("#" + id + " .box-gel-kit").each(function (i, e) {
                    setTimeout(function () {
                        lazyLoad(e);
                    },300)
                });
                $("#loading").hide();
            }
        })
    }, 1000)
}
function lazyLoad(ele) {
    var _class = $(ele).attr("class");
    var data_img = $(ele).find("img").attr("data-src");
    if (data_img == null || data_img == "") {
        return -1;
    }
    $("." + _class + " img").fadeIn(1000, function () {
        $(ele).find("img").attr("src", data_img);
        $(ele).find("img").attr("data-src", "");
    });
}
function CardCollection(id, code, idQty) {
    var qty = $(idQty).val();
    if (qty == null || qty == "" || qty == 0 || qty <= 0) {
       // qty = 1;
        alertify.error("Please select quantity before add to cart");
        return -1;
    }

    $.ajax({
        url: "/ordernacgroup/addtocart",
        type: "POST",
        dataType: "json",
        contentType: "application/json",
        data: JSON.stringify({
            id: id,
            skucode: code,
            quantity: qty
        }),
        beforeSend: xhr => {
            $("#loading").show();
        },
        success: response => {
            $("#loading").hide();
            if (response.status == "success") {


                $("#cart-modal").modal("toggle");
                $("#count").text(response.data.NewCart.OrderItems.length);
                //  alertify.error(response.message);
            }
            else {

                alertify.error(response.message);
            }

        },
        error: e => {
            $("#loading").show();
        }
    });
}
function updatenumkit() {
    var count = 0;
    var price = 0.00;
    var quantity = 0;
    $(".v-product-list-collection .box-gel-kit").each(function () {
        if ($(this).find(".input-check-gel").is(':checked')) {
            quantity = parseInt($(this).find(".input-qty-gel").val());
            if (quantity > 0) {
                $(this).find(".input-qty-gel").val(quantity);
                count += quantity;
                price += parseFloat($(this).find(".name-price-gel-kit ").attr("data-price") * quantity);
            } else {
                $(this).find(".input-check-gel").prop("checked", false);
            }


        }
    });

    $("#quantity_count").empty().text(count);
    $("#price_count").empty().text(format2(price, "$"));

}
function addproduct(id) { 
    var IsChecked = $("#check-" + id).prop("checked");
    if (!IsChecked) {
        $("#qty-v-" + id).val(0);
    } else {       
        $("#qty-v-" + id).val(1);
    }
    updatenumkit();
}
function updateqty(id) {
    var checkedId = $("#check-" + id);
    var qtyId = $("#qty-v-" + id);
    var qty = parseInt(qtyId.val());
    if (qty > 0) {
        checkedId.prop("checked", true);
        if (qty >= 20) {
            qtyId.val(0);
            checkedId.prop("checked", false);
            alert("Maximum select");
            return -1;
        }
    } else {
        checkedId.prop("checked", false);
    }
    updatenumkit();
}
function increment(id) {
    var checkedId = $("#check-" + id);
    var qtyId = $("#qty-v-" + id);
    var qty = parseInt(qtyId.val());
    if (qty >= 20) {
        qtyId.val(0);
        checkedId.prop("checked", false);
        alert("Maximum select");
        return -1;
    }
    qtyId.val(qty + 1);
    var IsChecked = checkedId.prop("checked");
    if (!IsChecked) {
        checkedId.prop("checked",true);
    }
    updatenumkit();
}
function decrement(id) {
    var checkedId = $("#check-" + id);
    var qtyId = $("#qty-v-" + id);
    var qty = parseInt(qtyId.val());
    if (qty <= 0) {   
        checkedId.prop("checked", false);
        return -1;
    }
    qtyId.val(qty - 1);
    updatenumkit()
}
function reply(obj) {
    var name = $(obj).data("name");
    var id = $(obj).data("id");
    $(".repy-form").fadeOut("slow", function () {
        $(".repy-form").hide();     
    })
    $("#Reply-form-" + id).fadeIn("slow", function () {
        $("#Reply-form-" + id).show();
    })   
    $("#Reply-content-" + id).html("@" + name+": ");
}
function Submit(obj) {
    var id = $(obj).data("id");
    var content = $("#Reply-content-" + id).val();
    if (content == null || content == "") {
        alert("Mời bạn nhập bình luận");
        return -1;
    }
    if (content.length <= 80) {
        alert("Nhập tối thiểu 80 kí tự");
        return -1;
    }
    $("#wrap_popup-" + id).fadeIn("slow", function () {
        $("#wrap_popup-" + id).show();
    })
   
    $("#popup-" + id).css("height", "100vh");
}
function cmtConfirmUser(obj) {
   //#region validate
    var pid = $(obj).data("pid");
    var id = $(obj).data("id");
    var content = $("#Reply-content-" + id).val(); 
    var name = $(".cfmUserName").val();
    if (name == null || name == "") {
        $("#lbMsgPopCmt-" + id).html("Vui lòng nhập họ và tên");
        return -1;
    }
    var email = $(".cfmUserEmail").val();
    var phone = $(".cfmPhone").val();
    if (phone == null || phone == "") {
        $("#lbMsgPopCmt-" + id).html("Vui lòng nhập số điện thoại");
        return -1;
    }
   
    //#endregion
    $.ajax({
        url: "/comment/reply",
        type: "POST",
        data: "name="+name+"&phone="+phone+"&email="+email+"&productid="+pid+"&parentid="+id+"&body="+content,
        beforeSend: function () {
            $(".loading").show();
        },
        success: function (result) {
            $(".loading").hide();
            if (result.status == "success") {
                alert(
                    "Cảm ơn bạn đã bình luận / đánh giá sản phẩm, chúng tôi sẽ phản hồi trong thời gian sớm nhất."
                );
                location.reload();
            } else {
                alert(result.message);
            }
        }
    });
}
function submitForm(obj) {
    //#region validate
    var form = $(obj).serialize();
    //#endregion
    $.ajax({
        url: "/contact-us/submitcontactproduct",
        type: "POST",
        data: form,
        beforeSend: function () {
            $(".loading").show();
        },
        success: function (result) {
            $(".loading").hide();
            if (result.status == "success") {
                alert(
                    "Cảm ơn bạn đã liên hệ sản phẩm, chúng tôi sẽ phản hồi trong thời gian sớm nhất."
                );
                location.reload();
            } else {
                alert(result.message);
            }
        }
    });
}
$(document).ready(function () {
    var productId = $("#productId").val();
    $(".slider-5item").owlCarousel({
        loop: false,
        autoplay: false,
        autoplayHoverPause: false,
        navText: [
            "<i class='demo-icon ecs-left-open-big'></i>",
            "<i class='demo-icon ecs-right-open-big'></i>"
        ],
        margin: 0,
        lazyLoad: true,
        autoHeight: false,
        touchDrag: false,
        pullDrag: false,
        freeDrag: false,
        smartSpeed: 1000,
        responsive: {
            0: {
                nav: true,
                dots: false,
                items: 2,
            },
            768: {
                items: 4,
                dots: false,
                nav: true,
            },
            1024: {
                items: 5,
                nav: true,
                dots: false,
            },
            1280: {
                items: 5,
                nav: true,
                dots: false,
            }
        }
    });
    $(".slider-1item").owlCarousel({
        loop: false,
        autoplay: false,
        autoplayHoverPause: false,
        navText: [
            "<i class='demo-icon ecs-left-open-big'></i>",
            "<i class='demo-icon ecs-right-open-big'></i>"
        ],
        margin: 0,
        lazyLoad: true,
        autoHeight: false,
        touchDrag: false,
        pullDrag: false,
        freeDrag: false,
        smartSpeed: 1000,
        responsive: {
            0: {
                nav: true,
                dots: false,
                items: 1,
            },
            768: {
                items: 1,
                dots: false,
                nav: true,
            },
            1024: {
                items: 1,
                nav: true,
                dots: false,
            },
            1280: {
                items: 1,
                nav: true,
                dots: false,
            }
        }
    });
    //$.get("/comment?id=" + productId, function (response) {
       
    //    if (response.status == "success") {
    //        const reducer = (accumulator, currentValue) => accumulator + currentValue;
    //        if (response.data.length > 0) {
    //            var newArr = [];
    //            //var html = 
    //            response.data.forEach(function (e) {
    //                if (e.ActiveStatus && e.ParentId == null || e.ParentId == "") {
    //                    newArr.push(e.Rating);
    //                }
    //            });
    //            //console.log(newArr);
    //            //$(".count-comment").html(newArr.length);
    //            //var vs = newArr.filter(function (e) {
    //            //    return e == 5
    //            //});
    //            //var ivs = newArr.filter(function (e) {
    //            //    return e == 4
    //            //});
    //            //var iiis = newArr.filter(function (e) {
    //            //    return e == 3
    //            //});
    //            //var iis = newArr.filter(function (e) {
    //            //    return e == 2
    //            //});
    //            //var is = newArr.filter(function (e) {
    //            //    return e == 1
    //            //});
    //            //console.log(vs);
    //            //$(".5-star").html(vs.length);
    //            //$(".4-star").html(ivs.length);
    //            //$(".3-star").html(iiis.length);
    //            //$(".2-star").html(iis.length);
    //            //$(".1-star").html(is.length);
    //            //if (vs.length > 0) {
    //            //    $(".bgb-in-5").css("width", ((5 * 100) / vs.length) + "%")
    //            //}
    //            //if (ivs.length > 0) {
    //            //    $(".bgb-in-4").css("width", ((4 * 100) / ivs.length) + "%")
    //            //}
    //            //if (iiis.length > 0) {
    //            //    $(".bgb-in-3").css("width", ((3 * 100) / iiis.length) + "%")
    //            //}
    //            //if (iis.length > 0) {
    //            //    $(".bgb-in-2").css("width", ((2 * 100) / iis.length) + "%")
    //            //}
    //            //if (is.length > 0) {
    //            //    $(".bgb-in-1").css("width", ((1 * 100) / is.length) + "%")
    //            //}
    //            //$(".total-rating").html(newArr.reduce(reducer) /);
    //        }
    //    }
    //});

  var $zoom = $(".zoom").magnify();
  $("#related-slider").owlCarousel({
    loop: false,
    autoplay: false,
    navText: [
      "<i class='demo-icon ecs-left-open-big'></i>",
      "<i class='demo-icon ecs-right-open-big'></i>"
    ],
    margin: 0,
    autoplayHoverPause: false,
    lazyLoad: true,
    autoHeight: false,
    smartSpeed: 1000,
    responsive: {
      0: {
        nav: true,
        dots: false,
        items: 2,
        margin: 5
      },
      768: {
        items: 4,
        dots: true,
        nav: false,
        margin: 10
      },
      1024: {
        items: 5,
        dots: false,
        nav: true,
        margin: 10
      },
      1300: {
        items: 5,
        dots: false,
        nav: true,
        margin: 10
      }
    }
  });
  $(".tab-container").each(function(index, el) {
    var check = $(this).height();
    if (check < 50) {
      $(this)
        .parent()
        .next()
        .hide();
      $(this)
        .parent()
        .css("height", "auto")
        .height();
    }
  });
  // slider
  $("#slider-single").slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    fade: true,
    adaptiveHeight: true,
    infinite: false,
    useTransform: true,
    speed: 1000,
    arrows: false,
    prevArrow: '<div><i class="demo-icon ecs-left-open-big"></i></div>',
    nextArrow: '<div><i class="demo-icon ecs-right-open-big"></div>'
  });

  $("#slider-nav")
    .on("init", function(event, slick) {
      $("#slider-nav .slick-slide.slick-current").addClass("is-active");
    })
    .slick({
      dots: false,
      focusOnSelect: false,
      slidesToShow: 5,
      slidesToScroll: 1,
      infinite: false,
      vertical: false,
      draggable: false,
      verticalSwiping: false,
      arrows: true,
      speed:1000,
      prevArrow: '<div><i class="demo-icon ecs-left-open-mini"></i></div>',
      nextArrow: '<div><i class="demo-icon ecs-right-open-mini"></div>',
      responsive: [
        {
          breakpoint: 1280,
          settings: {
            slidesToShow: 5,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 1025,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 770,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1
          }
        }
      ]
    });

  $("#slider-single").on("afterChange", function(event, slick, currentSlide) {
    $("#slider-nav").slick("slickGoTo", currentSlide);
    var currrentNavSlideElem =
      '#slider-nav .slick-slide[data-slick-index="' + currentSlide + '"]';
    $("#slider-nav .slick-slide.is-active").removeClass("is-active");
    $(currrentNavSlideElem).addClass("is-active");
    $zoom.destroy().magnify();
  });

  $("#slider-nav").on("click", ".slick-slide", function(event) {
    event.preventDefault();
    var goToSingleSlide = $(this).data("slick-index");

    $("#slider-single").slick("slickGoTo", goToSingleSlide);
  });
  // End slider
  /* End slider and thumbnail */
  $(".slider-5item").owlCarousel({
    loop: false,
    autoplay: true,
    autoplayHoverPause: true,
    navText: [
      "<i class='demo-icon ecs-left-open-big'></i>",
      "<i class='demo-icon ecs-right-open-big'></i>"
    ],
    margin: 0,
    lazyLoad: false,
    autoHeight: false,
    smartSpeed: 1000,
    responsive: {
      0: {
        nav: true,
        dots: false,
        items: 2,
        margin: 10
      },
      768: {
        items: 3,
        dots: false,
        nav: true,
        margin: 15
      },
      1024: {
        items: 4,
        nav: true,
        dots: false,
        margin: 15
      },
      1280: {
        items: 5,
        nav: true,
        dots: false,
        margin: 15
      }
    }
  });
});
function openmore(obj) {
  var location = $(obj).parent();
  var target = $(location).prev();
  var el = target;
  curHeight = el.height();
  autoHeight = el.css("height", "auto").height();
  el.height(curHeight).animate({ height: autoHeight }, 1000);
  location.fadeOut();
}
function addReplyComment(id, proid) {
  var form = $("#formreply" + id);
  $.ajax({
    url: $(form).attr("action"),
    type: "POST",
    data: $(form).serialize(),
    beforeSend: function() {
      $(".loading").show();
    },
    success: function(result) {
      $(".loading").hide();
      if (result.Ok) {
        alert(
          "Cảm ơn bạn đã bình luận / đánh giá sản phẩm, chúng tôi sẽ phản hồi trong thời gian sớm nhất."
        );
        location.reload();
      } else {
        alert(result.Msg);
      }
    }
  });
}

function addRating(idform) {
    var form = $(idform);

    $.ajax({
        url: "/comment/rating",
        type: "POST",
        data: $(form).serialize() + '&rating=' + $('input[name=Rating]:checked').val(),
        beforeSend: function () {
            $(".loading").show();
        },
        success: function (result) {
            $(".loading").hide();
            if (result.status == "success") {
                alert(
                    "Cảm ơn bạn đã bình luận / đánh giá sản phẩm, chúng tôi sẽ phản hồi trong thời gian sớm nhất."
                );
                location.reload();
            } else {
                alert(result.message);
            }
        }
    });
}

function addComment() {
  var form = $("#addcommentform");
  $.ajax({
    url: $(form).attr("action"),
    type: "POST",
    data: $(form).serialize(),
    beforeSend: function() {
      $(".loading").show();
    },
    success: function(result) {
      $(".loading").hide();
      if (result.Ok) {
        alert(
          "Cảm ơn bạn đã bình luận / đánh giá sản phẩm, chúng tôi sẽ phản hồi trong thời gian sớm nhất."
        );
        location.reload();
      } else {
        alert(result.Msg);
      }
    }
  });
}

function showreply(objid) {
  $(".formreply").hide();
  $(objid).show();
}

function loadmorecomment(proid, page, pagesize) {
  $.ajax({
    url: "/aj/Product/LoadmoreComment",
    type: "GET",
    data: { productId: proid, page: page, pagesize: pagesize },
    beforeSend: function() {
      $(".loading").show();
    },
    success: function(result) {
      $(".loading").hide();
      $("#comment .col-loadmore").append(result.viewsrc);
      if (result.CurrentPage >= result.TotalPageCount) $("#loadmorecmt").hide();
      else
        $("#loadmorecmt").html(
          '<a href="javascript:" class="btn btn-primary" onclick="loadmorecomment(' +
            proid +
            "," +
            (result.CurrentPage + 1) +
            "," +
            result.PageSize +
            ')">Xem thêm</a>'
        );
    }
  });
}

//function addRating() {
//  var form = $("#addratingform");
//  $.ajax({
//    url: $(form).attr("action"),
//    type: "POST",
//    data: $(form).serialize(),
//    beforeSend: function() {
//      $(".loading").show();
//    },
//    success: function(result) {
//      $(".loading").hide();
//      if (result.Ok) {
//        alert(
//          "Cảm ơn bạn đã bình luận / đánh giá sản phẩm, chúng tôi sẽ phản hồi trong thời gian sớm nhất."
//        );
//        location.reload();
//      } else {
//        alert(result.Msg);
//      }
//    }
//  });
//}

function loadmorerating(proid, page, pagesize) {
  $.ajax({
    url: "/aj/Product/LoadmoreRating",
    type: "GET",
    data: { productId: proid, page: page, pagesize: pagesize },
    beforeSend: function() {
      $(".loading").show();
    },
    success: function(result) {
      $(".loading").hide();
      $("#danhgia .col-loadmore").append(result.viewsrc);
      if (result.CurrentPage >= result.TotalPageCount)
        $("#loadmorerating").hide();
      else
        $("#loadmorerating").html(
          '<a href="javascript:" class="btn btn-primary" onclick="loadmorerating(' +
            proid +
            "," +
            (result.CurrentPage + 1) +
            "," +
            result.PageSize +
            ')">Xem thêm</a>'
        );
    }
  });
}
// chose product ver
function initslideevent() {
  var $zoom = $(".zoom").magnify();
    $("#slider-single").slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    asNavFor: "#slider-nav",
    dots: true,
    infinite: false,
    adaptiveHeight: true,
    swipe: false,
    lazyLoad: "progressive"
  });
    $("#slider-nav").slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    asNavFor: "#slider-single",
    arrows: false,
    focusOnSelect: true,
    vertical: true,
    verticalSwiping: true,
    infinite: false,
    draggable: true,
    lazyLoad: "progressive",
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 3
        }
      },
      {
        breakpoint: 800,
        settings: {
          slidesToShow: 2
        }
      }
      // You can unslick at a given breakpoint now by adding:
      // settings: "unslick"
      // instead of a settings object
    ]
  });
    $("#slider-single").on("afterChange", function(
    event,
    slick,
    currentSlide,
    nextSlide
  ) {
    $zoom.destroy().magnify();
  });
}

function choosesizecolor(curid) {
  var attrlst = [];
  $(".attrlst").each(function(i, e) {
    attrlst.push(
      $(e)
        .find("input:checked")
        .val()
    );
  });
  $.ajax({
    url: "/aj/product/choosecolorandsize",
    type: "POST",
    data: { attrlst: attrlst, curid: curid },
    beforeSend: function() {
      $(".loading").show();
    },
    success: function(result) {
      $(".loading").hide();
      if (result.Ok) {
        $("#productdetail > .section-header").html(result.Msg);
        initslideevent();
      } else {
        alert(result.Msg);
      }
    }
  });
}
// chose product ver
function selectSizeColor(obj) {
    $(obj).parent().find("a").removeClass("active");
    $(obj).addClass("active");
   

    var attrparam = "";
    $(".attribute-row").each(function (index, element) {
        var activeelement = $(element).find("a.active");
        attrparam += (index > 0 ?"@":"") + $(activeelement).data("attr-slug") + "|" + $(activeelement).data("value-slug")
    });
    var urlbulder = window.location.href.split('?')[0] + "?attr=" + attrparam;
    window.location.href = urlbulder;

    initslideevent();
}
function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}