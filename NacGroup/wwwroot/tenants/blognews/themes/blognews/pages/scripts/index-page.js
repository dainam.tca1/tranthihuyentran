﻿var money = 0;
var month = 0;
var percent = 0;
var interes_rates = [];
Array.prototype.sum = function (prop) {
    var total = 0
    for (var i = 0, _len = this.length; i < _len; i++) {
        total += this[i][prop]
    }
    return total
}
function open_popup_cal() {
    $("#calculate-modal").modal("show");

}
function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
function cal_interes_rate() {
    interes_rates = [];
    for (var i = 0; i <= month; i++) {
        var money_origin = parseFloat(money) / parseInt(month);
        var sum = interes_rates.sum("money_origin");
        var interes_rate = ((parseFloat(percent) / 100) / 12) * (parseFloat(money) - sum);
        var total = money_origin + interes_rate;
        interes_rates.push({
            "money_origin": money_origin,
            "interes_rate": interes_rate,
            "total": total
        });
    } 
    if (interes_rates.length <= 1) {
        $("#price-total").html(0);
        $("#price-origin").html(0);
        $("#price-interest").html(0);
        return false;
    }
    $("#price-total").html(numberWithCommas(interes_rates[0]["total"].toFixed(0)));
    $("#price-origin").html(numberWithCommas(interes_rates[0]["money_origin"].toFixed(0)));
    $("#price-interest").html(numberWithCommas(interes_rates[0]["interes_rate"].toFixed(0)));
    render_table();
}
function render_table() {
    if (interes_rates.length <= 1) {
        $(".spreadsheet-popup__table tbody").empty();
        return false;
    }
    interes_rates.splice(-1, 1);
    var newList = interes_rates.map(function (obj, index) {
        var stt = index + 1;
        var money_origin = numberWithCommas(obj["money_origin"].toFixed(0));
        var money_interest = numberWithCommas(obj["interes_rate"].toFixed(0));
        var money_total = numberWithCommas(obj["total"].toFixed(0));
        return "<tr><td>" + stt + "</td><td>" + money_origin + "VND</td><td>" + money_interest + "VND</td><td>" + money_total + "VND</td></tr>"
    });
   
    $(".spreadsheet-popup__table tbody").html(newList);
    $("#total-origin").html(numberWithCommas(interes_rates.sum("money_origin").toFixed(0)));
    $("#total-interest").html(numberWithCommas(interes_rates.sum("interes_rate").toFixed(0)));
    $("#total-money").html(numberWithCommas(interes_rates.sum("total").toFixed(0)));
}
function activedTab(ele) {
    $(".my-ul li").removeClass("active"); 
    $(ele).parent().addClass("active");
    $(".tab-body .tab-pane").each(function (index, ele) {
        $(ele).removeClass("in active");
    })
    $($(ele).attr("href")).addClass('in active'); 
}
$(document).ready(function () {
    //#region range money
    var $range_money = $(".js-range-slider-money"),
        $input_money = $(".js-input-money"),
        instance_money,
        min_money = 0,
        max_money = 30000000000;
    $("#min-money").html(min_money + " VND");
    $("#max-money").html(numberWithCommas(max_money) + " VND");
    $range_money.ionRangeSlider({
        skin: "round",
        type: "single",
        min: min_money,
        max: max_money,
        from: 100000000,
        onStart: function (data) {
            $input_money.prop("value", numberWithCommas(data.from));
            $("#price-origin-popup").html(numberWithCommas(data.from));
            money = data.from;
            cal_interes_rate();
        },
        onChange: function (data) {
            $input_money.prop("value", numberWithCommas(data.from));
            $("#price-origin-popup").html(numberWithCommas(data.from));
            money = data.from;
            cal_interes_rate();
        }
    });

    instance_money = $range_money.data("ionRangeSlider");

    $input_money.on("change keyup", function () {
        var that = this;
        var val = $(this).prop("value");
        if (val == "" || val == null) {
            val = min_money;
        }
        money = val.replace(/,/g, "");
        // validate
        if (parseInt(money) < min_money) {
            money = min_money;
        } else if (parseInt(money) > max_money) {
            money = max_money;
        }

        $("#price-origin-popup").html(numberWithCommas(money));
        $(this).val(numberWithCommas(money.toString().replace(/,/g, "")));
        cal_interes_rate();
        instance_money.update({
            from: $(that).prop("value").replace(/,/g, "")
        });
    });
    //#endregion
    //#region range month
    var $range_month = $(".js-range-slider-month"),
        $input_month = $(".js-input-month"),
        instance_month,
        min_month = 0,
        max_month = 360;
    $("#min-month").html(min_month + " tháng");
    $("#max-month").html(max_month + " tháng");
    $range_month.ionRangeSlider({
        skin: "round",
        type: "single",
        min: min_month,
        max: max_month,
        from: 120,
        step: 1,
        onStart: function (data) {
            $input_month.prop("value", data.from);
            month = data.from;
            $("#time-origin-popup").html(month);
            cal_interes_rate();
        },
        onChange: function (data) {
            $input_month.prop("value", data.from);
            month = data.from;
            $("#time-origin-popup").html(month);
            cal_interes_rate();
        }
    });

    instance_month = $range_month.data("ionRangeSlider");

    $input_month.on("change keyup", function () {
        var val = $(this).prop("value");
        // validate
        if (val == "" || val == null) {
            val = min_month;
        }
        if (val < min_month) {
            val = min_month;
        } else if (val > max_month) {
            val = max_month;
        }
        month = val;
        $(this).val(month);
        $("#time-origin-popup").html(month);
        cal_interes_rate();
        instance_month.update({
            from: parseInt(val)
        });
    });
    //#endregion
    //#region range percent
    var $range_percent = $(".js-range-slider-percent"),
        $input_percent = $(".js-input-percent"),
        instance_percent,
        min_percent = 0.0,
        max_percent = 20.0;
    $("#min-percent").html(min_percent + "%/năm");
    $("#max-percent").html(max_percent + "%/năm");
    $range_percent.ionRangeSlider({
        skin: "round",
        type: "single",
        min: min_percent,
        max: max_percent,
        from: 15.0,
        step: 0.1,
        onStart: function (data) {
            $input_percent.prop("value", data.from);
            percent = data.from;
            $("#interest-origin-popup").html(percent);
            cal_interes_rate();
        },
        onChange: function (data) {
            $input_percent.prop("value", data.from);
            percent = data.from;
            $("#interest-origin-popup").html(percent);
            cal_interes_rate();
        }
    });

    instance_percent = $range_percent.data("ionRangeSlider");

    $input_percent.on("change keyup", function () {
        var val = $(this).prop("value");
        // validate
        if (val == "" || val == null) {
            val = min_percent;
        }
        if (val < min_percent) {
            val = min_percent;
        } else if (val > max_percent) {
            val = max_percent;
        }
        percent = val;
        $(this).val(percent);
        $("#interest-origin-popup").html(percent);
        cal_interes_rate();
        instance_percent.update({
            from: val
        });
    });
    //#endregion
    $(".slider-4item").owlCarousel({
        loop: true,
        autoplay: true,
        autoplayHoverPause: false,
        navText: [
            "<i class='demo-icon ecs-left-open-big'></i>",
            "<i class='demo-icon ecs-right-open-big'></i>"
        ],
        nav: false,
        margin: 0,
        lazyLoad: true,
        autoHeight: false,
        touchDrag: true,
        pullDrag: false,
        freeDrag: false,
        smartSpeed: 1000,
        responsive: {
            0: {
                nav: false,
                dots: false,
                items: 2,
            },
            768: {
                items: 3,
                dots: false,
                nav: false,
            },
            1024: {
                items: 4,
                nav: true,
                dots: false,
            },
            1280: {
                items: 4,
                nav: false,
                dots: false,
            }
        }
    });
    $(".index-banner").owlCarousel({
        loop: true,
        autoplay: true,
        autoplayHoverPause: false,
        navText: [
            "<i class='demo-icon ecs-left-open-big'></i>",
            "<i class='demo-icon ecs-right-open-big'></i>"
        ],
        nav: false,
        margin: 0,
        lazyLoad: true,
        autoHeight: false,
        touchDrag: true,
        pullDrag: false,
        freeDrag: false,
        smartSpeed: 1000,
        responsive: {
            0: {
                nav: false,
                dots: false,
                items: 1,
            },
            768: {
                items: 1,
                dots: false,
                nav: false,
            },
            1024: {
                items: 1,
                nav: true,
                dots: false,
            },
            1280: {
                items: 1,
                nav: false,
                dots: false,
            }
        }
    });
})
