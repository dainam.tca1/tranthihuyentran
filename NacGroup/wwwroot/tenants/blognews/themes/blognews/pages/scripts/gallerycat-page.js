$(document).ready(function() {
  $("#mixitup-filters").owlCarousel({
    loop: false,
    autoplay: false,
    navText: [
      "<i class='demo-icon ecs-left-open-big'></i>",
      "<i class='demo-icon ecs-right-open-big'></i>"
    ],
    margin: 0,
    autoplayHoverPause: false,
    lazyLoad: true,
    autoHeight: true,
    smartSpeed: 1000,
    dots: false,
    responsive: {
      0: {
        items: 2,
        margin: 0,
        nav: true,
        dots: false
      },
      768: {
        items: 6,
        margin: 0,
        nav: true,
        dots: false
      },
      1024: {
        items: 8,
        margin: 0,
        nav: true,
        dots: false
      }
    }
  });
  $(".album")
    .first()
    .addClass("mixitup-control-active");
});
function album(id, page) {
  $(".album").removeClass("mixitup-control-active");
  var url = "/gallery/getalbum";
  $("#loading").show();
  $.ajax({
    url: url,
    type: "POST",
    data: { id: id, pages: page },
    success: function(response) {
      $("#loading").hide();
      $(".filter-" + id).addClass("mixitup-control-active");
      $(".list-item").html(response);
    }
  });
}
