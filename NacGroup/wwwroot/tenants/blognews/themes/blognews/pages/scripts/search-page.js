﻿$(document).ready(function() {
    $("#sortdropdown").val($("#filterform").find("input[name='sort']").val());
})

function refreshfilter() {
    var form = $("#filterform");

    ////Lấy thuộc tính thả vào hidden field
    //var pp = [];
    //$("#left-bar input[name='attr']:checked").each(function () {
    //    var atval = $(this).val().split("-");
    //    if (atval.length == 2) {
    //        var curentattr = pp.find(function (e) {
    //            return e.attid == atval[0];
    //        });

    //        if (curentattr) {
    //            curentattr.valid.push(atval[1]);
    //        } else {
    //            pp.push({
    //                attid: atval[0],
    //                valid: [atval[1]]
    //            });
    //        }
    //    }
    //});

    //form.find("input[name='pp']").val("");
    //if (pp.length > 0) {
    //    var valstr = "";
    //    for (var i = 0; i < pp.length; i++) {
    //        if (i != 0) {
    //            valstr += "v";
    //        }
    //        valstr += pp[i].attid + "-" + pp[i].valid.join(",");
    //    }
    //    console.log(valstr);
    //    form.find("input[name='pp']").val(valstr);
    //}

    ////Lấy thương hiệu thả vào hidden field
    //var bp = [];
    //$("#left-bar input[name='brand']:checked").each(function () {
    //    bp.push($(this).val());
    //});

    //form.find("input[name='bp']").val("");
    //if (bp.length > 0) {
    //    form.find("input[name='bp']").val(bp.join(","));
    //}
    ////Lấy giá thả vào hidden field
    //var pr = [];
    //$("#left-bar input[name='price']:checked").each(function () {
    //    pr.push($(this).val());
    //});
    //form.find("input[name='pr']").val("");
    //if (pr.length > 0) {
    //    form.find("input[name='pr']").val(pr.join(","));
    //}

    //Lấy cách sắp xếp thả vào hidden field
    form.find("input[name='sort']").val($("#sortdropdown").val());
    form.find("input[name='page']").val(1);
    getfilterresult();
}

function getfilterresult() {
    var frmData = $("#filterform").serializeArray();
    var qr = "",
        url = window.location.href.split("?")[0];
    for (var i = 0; i < frmData.length; i++) {
        var d = frmData[i];
        if (d.value === undefined || d.value === "") continue;
        switch (d.name) {
            case "pp":
                qr += "&pp=" + d.value;
                break;
            case "bp":
                qr += "&bp=" + d.value;
                break;
            case "pr":
                qr += "&pr=" + d.value;
                break;
            case "sort":
                qr += "&sort=" + d.value;
                break;
            case "keyword":
                qr += "&keyword=" + d.value;
                break;
        }
    }

    //var cc = window.uQuery('clearcache');
    //if (cc != undefined && cc !== '' && cc !== 'null')
    //    qr += '&clearcache=' + cc;
    qr = qr.replace(/(^&)|(&$)/g, "");
    if (qr === "") qr = url;
    else qr = url + "?" + qr;
    window.location.href = qr;
}