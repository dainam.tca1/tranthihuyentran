﻿using System;

namespace NacGroup.Models
{
    public class SitemapNode
    {
        public SitemapFrequency? Frequency { get; set; }
        public DateTime? LastModified { get; set; }
        public double? Priority { get; set; }
        public string Url { get; set; }
        public SiteMapNodeImage Image { get; set; }
    }
    public enum SitemapFrequency
    {
        Never,
        Yearly,
        Monthly,
        Weekly,
        Daily,
        Hourly,
        Always
    }
    public class SiteMapNodeImage
    {
        public string loc { get; set; }
        public string title { get; set; }
    }
}
