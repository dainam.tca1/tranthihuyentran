﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace NacGroup.Pages
{
    [Authorize(Roles = "Admin",Policy = "TenantAuthorize")]
    public class CrmAdminModel : PageModel
    {
        public CrmAdminModel()
        {
          
        }
        public void OnGet()
        {

        }
    }
}