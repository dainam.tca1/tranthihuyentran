﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Xml.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using NacGroup.Models;
using NacGroup.Module.Blog.Data;
using NacGroup.Module.Core.Data;
using NacGroup.Module.Core.Interfaces;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Core.Models.Schema;
using NacGroup.Module.Core.Services;
using NacGroup.Module.MiniCRM.Data;
using NacGroup.Module.MiniCRM.Models.Schema;
using NacGroup.Module.Product.Data;
using NacGroup.Module.Product.Models.Schema;
using NacGroup.Module.StaticPage.Data;
using Newtonsoft.Json;

namespace NacGroup.Controllers
{
    public class DevelopController : Controller
    {
        private readonly IMongoContext _mongoContext;
        private readonly GlobalConfiguration _globalConfiguration;
        private readonly IStaticPageRepository _staticPageRepository;
        private readonly IProductCategoryRepository _productCategoryRepository;
        private readonly IProductRepository _productRepository;
        private readonly IBlogCategoryRepository _blogCategoryRepository;
        private readonly IBlogRepository _blogRepository;
        private readonly ITagsRepository _tagRepository;
        private readonly IMemoryCache _memoryCache;
        private readonly IUnitOfWork _unitOfWork;

        public DevelopController(IMongoContext mongoContext, IGettingGlobalConfigService gettingGlobalConfigService,ITagsRepository tagsRepository, IStaticPageRepository staticPageRepository, IProductCategoryRepository productCategoryRepository, IProductRepository productRepository, IBlogCategoryRepository blogCategoryRepository, IBlogRepository blogRepository, IMemoryCache memoryCache, ICustomerRepository customerRepository, IUnitOfWork unitOfWork)
        {
            _globalConfiguration = gettingGlobalConfigService.GetConfiguration();
            _mongoContext = mongoContext;
            _staticPageRepository = staticPageRepository;
            _productCategoryRepository = productCategoryRepository;
            _productRepository = productRepository;
            _blogCategoryRepository = blogCategoryRepository;
            _blogRepository = blogRepository;
            _tagRepository = tagsRepository;
            _memoryCache = memoryCache;
            _unitOfWork = unitOfWork;
        }

        public IActionResult Index(string secure)
        {
            if (string.IsNullOrEmpty(secure) || secure != "seedingsecure") return Unauthorized();
            var types = AppDomain.CurrentDomain.GetAssemblies().SelectMany(s => s.GetTypes()).Where(p => typeof(IMigrationRepository).IsAssignableFrom(p) && !p.IsInterface);
            foreach (var item in types)
            {
                IMigrationRepository sss = (IMigrationRepository)Activator.CreateInstance(item, args: _mongoContext);
                sss.Migration();
                sss.Seed();
            }
            return Ok();
        }


        public IActionResult Seed()
        {
            var product = _productRepository.Get().FirstOrDefault();
            var allcategory = _productCategoryRepository.Get().ToArray();
            foreach (var cat in allcategory)
            {
                for (var i = 1; i <= 8; i++)
                {
                    var pro = JsonConvert.DeserializeObject<Product>(JsonConvert.SerializeObject(product));
                    pro.Id = null;
                    pro.Url = product.Url + cat.Url + "-" + i;
                    pro.Skus.FirstOrDefault().SkuCode = pro.Skus.FirstOrDefault().SkuCode + cat.Url + "-" + i;
                    pro.Categories.Clear();
                    pro.Categories.Add(cat);
                    _productRepository.Add(pro);
                }
            }

            _unitOfWork.Commit();
            return Ok();
        }



        [Route("/sitemap.xml")]
        public IActionResult GenerateSiteMap()
        {
            if (!_memoryCache.TryGetValue($"{_globalConfiguration.TenantName}_{ConstKey.SiteMapCacheKey}", out string result))
            {
                var sitemapNodes = GetSitemapNodes();
                XNamespace xmlns = "http://www.sitemaps.org/schemas/sitemap/0.9";
                XNamespace xmlns1 = "http://www.google.com/schemas/sitemap-image/1.1";
                XElement root = new XElement(xmlns + "urlset", new XAttribute(XNamespace.Xmlns + "image", xmlns1));
                foreach (var sitemapNode in sitemapNodes)
                {
                    XElement urlElement = new XElement(
                        xmlns + "url",
                        new XElement(xmlns + "loc", Uri.EscapeUriString(sitemapNode.Url)),
                        sitemapNode.LastModified == null ? null : new XElement(
                            xmlns + "lastmod",
                            sitemapNode.LastModified.Value.ToLocalTime().ToString("yyyy-MM-ddTHH:mm:sszzz")),
                        sitemapNode.Frequency == null ? null : new XElement(
                            xmlns + "changefreq",
                            sitemapNode.Frequency.Value.ToString().ToLowerInvariant()),
                        sitemapNode.Priority == null ? null : new XElement(
                            xmlns + "priority",
                            sitemapNode.Priority.Value.ToString("F1", CultureInfo.InvariantCulture)),
                        sitemapNode.Image == null ? null : new XElement(
                            xmlns1 + "image",
                            new XElement(xmlns1 + "loc", sitemapNode.Image.loc),
                            new XElement(xmlns1 + "title", sitemapNode.Image.title)
                        )
                    );
                    root.Add(urlElement);
                }
                XDocument document = new XDocument(root);
                result = document.ToString();
                _memoryCache.Set($"{_globalConfiguration.TenantName}_{ConstKey.SiteMapCacheKey}", result, TimeSpan.FromDays(1));
            }

            return Content(result, "application/xml");
        }

        public List<SitemapNode> GetSitemapNodes()
        {

            List<SitemapNode> nodes = new List<SitemapNode>();

            var hompageurl = _globalConfiguration.WebsiteUrl;
            nodes.Add(
                new SitemapNode()
                {
                    Url = hompageurl,
                    Frequency = SitemapFrequency.Daily,
                });
            #region Sitemap cho trang tĩnh
            nodes.Add(
               new SitemapNode()
               {
                   Url = _globalConfiguration.WebsiteUrl + "/contact-us",
                   Frequency = SitemapFrequency.Monthly,
               });
            nodes.Add(
             new SitemapNode()
             {
                 Url = _globalConfiguration.WebsiteUrl + "/vay-mua-nha",
                 Frequency = SitemapFrequency.Monthly,
             });
            nodes.Add(
            new SitemapNode()
            {
                Url = _globalConfiguration.WebsiteUrl + "/vay-mua-xe",
                Frequency = SitemapFrequency.Monthly,
            });
            nodes.Add(
            new SitemapNode()
            {
                Url = _globalConfiguration.WebsiteUrl + "/dao-han-giai-chap",
                Frequency = SitemapFrequency.Monthly,
            });
            nodes.Add(
          new SitemapNode()
          {
              Url = _globalConfiguration.WebsiteUrl + "/the-tin-dung",
              Frequency = SitemapFrequency.Monthly,
          });
            nodes.Add(
        new SitemapNode()
        {
            Url = _globalConfiguration.WebsiteUrl + "/kien-thuc-vay-von",
            Frequency = SitemapFrequency.Monthly,
        });
            var statistlst = _staticPageRepository.Get().ToList();
            foreach (var item in statistlst)
            {
                nodes.Add(
                    new SitemapNode()
                    {
                        Url = $"{_globalConfiguration.WebsiteUrl}/page/{item.Url}",
                        Frequency = SitemapFrequency.Weekly,
                        LastModified = DateTime.Now,
                    }
                );
            }
            #endregion
            #region Sitemap cho sản phẩm
            //var procatebrandlst = _brandService.GetAllBrandViewModel();
            //foreach (var item in procatebrandlst)
            //{
            //    nodes.Add(
            //        new SitemapNode()
            //        {
            //            Url = Url.RouteUrl("brands", new { url = item.Url }, Request.Url.Scheme),
            //            Frequency = SitemapFrequency.Daily,
            //            LastModified = DateTime.Now,
            //        }
            //    );
            //}

            var procatelst = _productCategoryRepository.Get().ToList();
            foreach (var item in procatelst)
            {
                nodes.Add(
                    new SitemapNode()
                    {
                        Url = $"{_globalConfiguration.WebsiteUrl}/productcat/{item.Url}",
                        Frequency = SitemapFrequency.Daily,
                        LastModified = DateTime.Now,
                    }
                );
            }

            var proList = _productRepository.Get().ToList();

            foreach (var item in proList)
            {
                nodes.Add(
                    new SitemapNode()
                    {
                        Url = $"{_globalConfiguration.WebsiteUrl}/productdetail/{item.Url}",
                        Frequency = SitemapFrequency.Weekly,
                        LastModified = DateTime.Now,
                        Image = new SiteMapNodeImage
                        {
                            loc = hompageurl + item.Avatar,
                            title = item.Name
                        }
                    }
                );

            }
            #endregion
            #region Sitemap tin tức
            var newscatelst = _blogCategoryRepository.Get().ToList();
            foreach (var item in newscatelst)
            {
                nodes.Add(
                    new SitemapNode()
                    {
                        Url = $"{_globalConfiguration.WebsiteUrl}/danh-muc/{item.Url}",
                        Frequency = SitemapFrequency.Daily,
                    });
            }
            var newlst = _blogRepository.Get().ToList();
            foreach (var item in newlst)
            {
                nodes.Add(
                    new SitemapNode()
                    {
                        Url = $"{_globalConfiguration.WebsiteUrl}/{item.CategoryUrl}/{item.Url}/{item.CreatedDate.Day}/{item.CreatedDate.Month}/{item.CreatedDate.Year}",
                        Frequency = SitemapFrequency.Weekly,
                        LastModified = DateTime.Now,
                        Image = new SiteMapNodeImage
                        {
                            loc = hompageurl + item.Avatar,
                            title = item.Title
                        }
                    }
                );
            }
            var tagList = _tagRepository.Get();
            //blog-topic/
            foreach (var tag in tagList)
            {
                nodes.Add(
                   new SitemapNode()
                   {
                       Url = $"{_globalConfiguration.WebsiteUrl}/blog-topic/{tag.Slug}",
                       Frequency = SitemapFrequency.Weekly,
                       LastModified = DateTime.Now,  
                   }
               );
            }
            #endregion
            return nodes;
        }

    }
}