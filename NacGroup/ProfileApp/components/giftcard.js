﻿import React, { Component } from "react";
import { Link } from "react-router-dom";
import "../plugins/string-helper";
import { GLOBAL_ERROR_MESSAGE } from "../constants/message";
import moment from "moment";

function formatPhoneNumber(phoneNumberString) {
  var cleaned = ("" + phoneNumberString).replace(/\D/g, "");
  var match = cleaned.match(/^(\d{3})(\d{3})(\d{4})$/);
  if (match) {
    return [match[1], "-", match[2], "-", match[3]].join("");
  }
  return phoneNumberString;
}

class giftcard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      model: null
    };
    var that = this;
    $.get("/account/giftcard-histories", res => {
      that.state.model = res;
      that.setState(that.state);
      $(".acc-menulst li").removeClass("active");
      $(".acc-menulst li[data-id='giftcard']").addClass("active");
    }).fail(res => {
        location.href = "/account/login?ReturnUrl=/accountapp/giftcard/";
    });
  }

  componentDidMount() {
    document.title = "Gift Card";
  }

  render() {
    return this.state.model ? (
      <div className="giftcardlist">
        {this.state.model.Results.map(gift => (
          <Link to={"/accountapp/giftcard/" + gift.Id} className="item">
            <div className="avatar">
              <img src={gift.GiftCardTemplateImage} />
            </div>
            <div className="des">
              <div className="amount">
                <i className="demo-icon nac-gift" /> ${gift.TotalAmount}
              </div>
              <div className="dat">
                <i className="demo-icon nac-clock" />{" "}
                {moment.utc(gift.CreatedDate).format("MM/DD/YYYY hh:mm:ss A")}
              </div>
            </div>
          </Link>
        ))}
      </div>
    ) : (
      <div />
    );
  }
}

export default giftcard;
