import { Component } from "react";

class Empty extends Component {
  componentDidMount() {
    document.title = "Empty";
  }
  render() {
    return null;
  }
}

export default Empty;
