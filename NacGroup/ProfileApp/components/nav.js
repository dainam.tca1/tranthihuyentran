﻿import React, { Component } from "react";
import { Link } from "react-router-dom";
import AppContext from "./app-context";
class Nav extends Component {
    constructor(props) {
        super(props);
        this.state = {
            model: null
        };

        var that = this;
        $.get("/account/profile", res => {
            that.state.model = res;
            that.setState(that.state);
        });
    }
    render() {
        return this.state.model ? (
            <div className="account-menu">
                <div className="profile-thumb">
                    <div className="avatar">
                        <span>{this.state.model.FirstName[0]}</span>
                    </div>
                    <div className="des">
                        <div className="t-1">Tài khoản</div>
                        <div className="username">
                            {this.state.model.FirstName} {this.state.model.LastName}
                        </div>
                    </div>
                </div>
                <ul className="acc-menulst">
                    <li data-id="profile">
                        <Link to="/accountapp/profile">
                            <i className="demo-icon nac-user" /> <span>Thông tin</span>
                        </Link>
                    </li>
                    <li data-id="s-address">
                        <Link to="/accountapp/shippingAddress">
                            <i className="demo-icon nac-location" /> <span>Địa chỉ</span>
                        </Link>
                    </li>
                
                
                    {this.context.FeatureList.find(m => m.Url === "order") !=
                        null && (
                            <li data-id="order">
                            <Link to="/accountapp/orderhistory">
                                    <i className="demo-icon nac-doc-text" /> <span>Lịch sử đơn hàng</span>
                                </Link>
                            </li>
                        )}
                    {this.context.FeatureList.find(m => m.Url === "gift-card") !=
                        null && (
                            <li data-id="giftcard" class="hidden">
                            <Link to="/accountapp/giftcard">
                                    <i className="demo-icon nac-gift" /> <span>Gift Card</span>
                                </Link>
                            </li>
                        )}


                    <li>
                        <a href="/account/logout">
                            <i className="demo-icon nac-logout" /> <span>Đăng xuất</span>
                        </a>
                    </li>
                </ul>
            </div>
        ) : (
                <div />
            );
    }
}
Nav.contextType = AppContext;
export default Nav;
