﻿import React, { Component } from "react";
import { Link } from "react-router-dom";
import "../plugins/string-helper";
import { GLOBAL_ERROR_MESSAGE } from "../constants/message";

function formatPhoneNumber(phoneNumberString) {
  var cleaned = ("" + phoneNumberString).replace(/\D/g, "");
  var match = cleaned.match(/^(\d{3})(\d{3})(\d{4})$/);
  if (match) {
    return [match[1], "-", match[2], "-", match[3]].join("");
  }
  return phoneNumberString;
}

class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
        model: null,

    };
      var that = this;
      $("#loading").show();
      $.get("/account/profile", res => {
          $("#loading").hide();
          that.state.model = res;
          that.setState(that.state);
         
            $(".acc-menulst li").removeClass("active");
            $(".acc-menulst li[data-id='profile']").addClass("active");
    }).fail(res => {
        location.href = "/account/login?ReturnUrl=/accountapp/profile";
    });
  }

  submitprofile() {
    var that = this;
    $("#loading").show();
    $.ajax({
      url: "/account/profile",
      type: "POST",
      data: this.state.model,
      success: function(response) {
        $("#loading").hide();
        if (response.status == "success") {
          alertify.alert("Success", response.message, function() {
            that.props.history.push("/accountapp/emptypage");
            that.props.history.replace({
              pathname: that.props.location.pathname,
              search: that.props.location.search
            });
          });
        } else {
          alertify.alert("Rất tiếc!", response.message);
        }
      },
      error: function(er) {
        $("#loading").hide();
        alertify.alert(
          "Error",
          "Rất tiếc! Hệ thống có một số lỗi. Vui lòng thử lại!"
        );
      }
    });
  }

  componentDidMount() {
    document.title = "Thông tin tài khoản";
  }

  render() {
    return this.state.model ? (
      <form
        action=""
        method="post"
        className="section-body"
        id="accountinfoform"
        onSubmit={e => {
          e.preventDefault();
          this.submitprofile();
        }}
      >
        <div className="form-group">
          <label>Họ</label>
          <input
            type="text"
            className="form-control"
            name="Firstname"
            placeholder="Họ"
            value={this.state.model.FirstName}
            onChange={e => {
              this.state.model.FirstName = e.target.value;
              this.setState(this.state);
            }}
          />
        </div>
        <div className="form-group">
          <label>Tên</label>
          <input
            type="text"
            className="form-control"
            name="Lastname"
            placeholder="Tên"
            value={this.state.model.LastName}
            onChange={e => {
              this.state.model.LastName = e.target.value;
              this.setState(this.state);
            }}
          />
        </div>
        <div className="form-group">
          <label>Số điện thoại</label>
          <input
            type="text"
            className="form-control"
            name="Phone"
            placeholder="Số điện thoại"
            disabled
            value={formatPhoneNumber(this.state.model.Phone)}
          />
        </div>
        <div className="form-group">
          <label>Địa chỉ email</label>
          <input
            type="text"
            className="form-control"
            name="Email"
            disabled
            value={this.state.model.Email}
          />
        </div>
        <div className="form-group">
          <div className="checkbox-container">
            <input
              type="checkbox"
              id="IsChangePassword"
              name="IsChangePassword"
              onChange={() => {
                $(".changepassform").slideToggle();
              }}
            />
            <label for="IsChangePassword">Thay đổi mật khẩu</label>
          </div>
        </div>

        <div className="changepassform">
          <div className="form-group">
            <label>Mật khẩu hiện tại</label>
            <input
              name="Password"
                        type="password"
                        placeholder="Mật khẩu hiện tại"
              className="form-control"
              value={this.state.model.Password}
              onChange={e => {
                this.state.model.Password = e.target.value;
                this.setState(this.state);
              }}
            />
          </div>
          <div className="form-group">
            <label>Mật khẩu mới</label>
            <input
              name="NewPassword"
              type="password"
              placeholder="Mật khẩu mới"
              className="form-control"
              value={this.state.model.NewPassword}
              onChange={e => {
                this.state.model.NewPassword = e.target.value;
                this.setState(this.state);
              }}
            />
          </div>
          <div className="form-group">
            <label>Nhập lại mật khẩu mới</label>
            <input
              name="ConfirmPassword"
              type="password"
              placeholder="Nhập lại mật khẩu mới"
              className="form-control"
              value={this.state.model.ConfirmPassword}
              onChange={e => {
                this.state.model.ConfirmPassword = e.target.value;
                this.setState(this.state);
              }}
            />
          </div>
        </div>
        <div className="btn-container">
          <button className="btn btn-primary" type="submit">    
            Cập nhật
          </button>
        </div>
      </form>
    ) : (
      <div />
    );
  }
}

export default Profile;
