﻿import React, { Component } from "react";
import { Link } from "react-router-dom";
import "../plugins/string-helper";

function formatPhoneNumber(phoneNumberString) {
    var cleaned = ("" + phoneNumberString).replace(/\D/g, "");
    var match = cleaned.match(/^(\d{3})(\d{3})(\d{4})$/);
    if (match) {
        return [match[1], "-", match[2], "-", match[3]].join("");
    }
    return phoneNumberString;
}

class addressList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            model: null,
            ex: {
                Title: null,
                Url: "shippingAddress"
            }
        };
        var that = this;
        $("#loading").show();
        $.get("/account/profile", res => {
            that.state.model = res;
            this.state.ex.Title = "Danh sách địa chỉ";
            $(".acc-menulst li").removeClass("active");
            $(".acc-menulst li[data-id='s-address']").addClass("active");
            $("#loading").hide();
            that.setState(that.state);
        }).fail(res => {
            location.href = "/account/login?ReturnUrl=/accountapp/shippingAddress";
        });
    }

    removeAddress(id) {
        var that = this;
        if (id == "" || id == null) {
            alertify.alert("Rất tiếc!", "Tham số không hợp lệ");
            return -1;
        }
        alertify.confirm('Bạn có chắc chắn!', 'Dữ liệu không thể hồi phục', function () {
            $("#loading").show();
            that.state.model.Recipient = that.state.model.Recipient.filter(e => { return e.Id != id });
            if (that.state.model.Recipient.length == 0) {
                that.state.model.Recipient = [];

            }
            $.ajax({
                url: "/account/profile",
                type: "POST",
                data: that.state.model,
                success: function (response) {
                    $("#loading").hide();
                    if (response.status == "success") {
                        alertify.alert("Thành công", response.message, function () {
                            that.props.history.push("/accountapp/emptypage");
                            that.props.history.replace({
                                pathname: that.props.location.pathname,
                                search: that.props.location.search
                            });
                        });
                    } else {
                        alertify.alert("Rất tiếc!", response.message);
                    }
                },
                error: function (er) {
                    $("#loading").hide();
                    alertify.alert(
                        "Lỗi",
                        "Rất tiếc!  Hệ thống có một số lỗi. Vui lòng thử lại!"
                    );
                }
            });

        }
            , function () { alertify.error('Hủy bỏ') });

    }

    componentDidMount() {
        document.title = "Danh sách địa chỉ";
    }

    render() {
        return this.state.model ? (
            <div className="addresslist">
                <h2 className="section-header flex-container flex-center h5">{this.state.ex.Title}   <Link to={`/accountapp/${this.state.ex.Url}/add`} className="btn btn-primary">Thêm địa chỉ mới</Link></h2>

                <table className="table table-hover hidden-xs" style={{ "width": "100%" }}>
                    <thead>
                        <tr>
                            <th>Họ và tên</th>
                            <th>Địa chỉ</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.model.Recipient.map(c => {
                            return (
                                <tr>
                                    <td><Link to={`/accountapp/${this.state.ex.Url}/update/` + c.Id}>{c.FullName}</Link></td>
                                    <td>{`${c.Street}, ${c.Ward}, ${c.District}, ${c.City}, VN`}</td>
                                    <td>
                                        <a href="javascript:void(0)" onClick={e => { this.removeAddress(c.Id) }} ><span className="nac-trash"></span></a>
                                    </td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
                <div className="visible-xs">
                    {this.state.model.Recipient.map(c => {
                        return (
                            <div className="item">
                                <p><Link to={`/accountapp/${this.state.ex.Url}/update/` + c.Id}>#{c.FullName}</Link></p>
                                <p>{`${c.Street}, ${c.Ward}, ${c.District}, ${c.City}, VN`}</p>
                                <p>
                                    <a href="javascript:void(0)" onClick={e => { this.removeAddress(c.Id) }} ><span className="nac-trash"></span></a>
                                </p>
                            </div>
                        )
                    })}

                </div>
            </div>
        ) : (
                <div />
            );
    }
}

export default addressList;
