﻿import React, { Component } from "react";
import { Link } from "react-router-dom";
import "../plugins/string-helper";
import addressCmsModel from "./models/address-cmsmodel";
import profileCmsModel from "./models/profile-cmsmodel";
function formatPhoneNumber(phoneNumberString) {
    var cleaned = ("" + phoneNumberString).replace(/\D/g, "");
    var match = cleaned.match(/^(\d{3})(\d{3})(\d{4})$/);
    if (match) {
        return [match[1], "-", match[2], "-", match[3]].join("");
    }
    return phoneNumberString;
}

class addressAddUpdate extends Component {
    constructor(props) {
        super(props);
        var action = null;
        if (document.location.href.indexOf("/accountapp/shippingAddress/add") >= 0) {
            action = "add";
        } else if (
            document.location.href.indexOf("/accountapp/shippingAddress/update") >= 0
        ) {
            action = "update";
        }
        var that = this;
        this.state = {
            model: new addressCmsModel(),
            modelProfile: new profileCmsModel(),
            ex: {
                Title: null,
                Action: action,
                CityList:[],
                DistrictList: [],
                DistrictTempList: [],
                WardList: [],
                WardTempList: [],
                Url:"dia-chi"
            }
        };
        that.setState(that.state);
        
        $("#loading").show();
        // Get All User
        $.get("/account/profile", res => {
            that.state.modelProfile = res;
            that.setState(that.state);
            this.state.ex.Url = "dia-chi";
            $(".acc-menulst li").removeClass("active");
            $(".acc-menulst li[data-id='s-address']").addClass("active");
            $.get("/ordernacgroup/getcity",
                res => {
                    $("#loading").hide();
                    that.state.ex.CityList = res;
                    if (action == "update") {
                        if (that.state.modelProfile.Recipient.length > 0) {
                            // Find user by id
                            that.state.ex.Title = "Cập nhật địa chỉ";
                            that.state.model = that.state.modelProfile.Recipient.find(e => {
                                return e.Id == that.props.match.params.id;
                            });
                          
                            that.setState(that.state);
                        }
                        that.setState(that.state);
                    } 
                    that.setState(that.state);
                });
            $.get("/ordernacgroup/getdistrict",
                res => {
                    $("#loading").hide();
                    that.state.ex.DistrictTempList = res;
                    if (action == "update") {
                        that.state.ex.DistrictList =
                            that.state.ex.DistrictTempList ? that.state.ex.DistrictTempList.filter(d => { return d.City.Name == that.state.model.City }) : [];
                        
                        that.setState(that.state);
                    } 
                    that.setState(that.state);
                });
            $.get("/ordernacgroup/getward",
                res => {
                    $("#loading").hide();
                    that.state.ex.WardTempList = res;
                    if (action == "update") {
                        that.state.ex.WardList =
                            that.state.ex.WardTempList ? that.state.ex.WardTempList.filter(w => { return w.District.Name == that.state.model.District }) : [];
                        that.setState(that.state);
                    } 
                    that.setState(that.state);
                });
        }).fail(res => {
            location.href = "/account/login?ReturnUrl=/accountapp/shippingAddress";
        });
        
        
    }

    submitprofile() {
        var that = this;
        $("#loading").show();
        //#region Validate
        
        if (this.state.model.FirstName == null || this.state.model.FirstName == "") {
            $("#loading").hide();
            alertify.alert("Lỗi!", "Vui lòng nhập thông tin họ và tên đầy đủ");
            return -1;
        }
      
        if (this.state.model.PhoneNumber == null || this.state.model.PhoneNumber == "") {
            $("#loading").hide();
            alertify.alert("Lỗi!", "Vui lòng nhập thông tin số điện thoại");
            return -1;
        }
       
        if (this.state.model.City == null || this.state.model.City == "") {
            $("#loading").hide();
            alertify.alert("Lỗi!", "Vui lòng chọn tỉnh, thành phố");
            return -1;
        }
        if (this.state.model.District == null || this.state.model.District == "") {
            $("#loading").hide();
            alertify.alert("Lỗi!", "Vui lòng chọn quận, huyện");
            return -1;
        }
        if (this.state.model.Ward == null || this.state.model.Ward == "") {
            $("#loading").hide();
            alertify.alert("Lỗi!", "Vui lòng chọn phường");
            return -1;
        }
        if (this.state.model.Street1 == null || this.state.model.Street1 == "") {
            $("#loading").hide();
            alertify.alert("Lỗi!", "Vui lòng nhập thông tin địa chỉ");
            return -1;
        }
        //#endregion
        if (this.state.ex.Action == "add") {
            // add new 
          
            if (this.state.modelProfile.Recipient.length == 0) {
                //check if addresslist of user not found Id set = 1 
                this.state.model.Id = 1;
                this.state.model.IsDefault = true;
                // push object to array
                this.state.modelProfile.Recipient.push(this.state.model);
               
            }
            
            else {
                //find address newest of modelProfile
                var oldAddress = this.state.modelProfile.Recipient[this.state.modelProfile.Recipient.length - 1];
                if (oldAddress == null) {
                    $("#loading").hide();
                    alertify.alert("Lỗi!", "Không tìm thấy dữ liệu");
                    return -1;
                }
                //if model IsDefault == true
                if (this.state.model.IsDefault) {
                    var findOldDefaultAddress = this.state.modelProfile.Recipient.filter(e => { return e.IsDefault == true });
                    if (findOldDefaultAddress.length > 0) {
                        this.state.modelProfile.Recipient = this.state.modelProfile.Recipient ? this.state.modelProfile.Recipient.map(e => {
                            return { ...e, IsDefault: false};
                        }) : [];
                        
                    }
                }
                this.state.model.Id = parseInt(oldAddress.Id) + 1;
                this.state.modelProfile.Recipient.push(this.state.model);
               
            }
        } else {
            //update
           
            //if model IsDefault == true
            if (that.state.model.IsDefault == true) {
                that.state.modelProfile.Recipient.forEach(e => {
                    if (e.Id == that.state.model.Id) {
                        e = that.state.model;
                        e.IsDefault = true;
                       
                    } else {
                        e.IsDefault = false;
                    }

                })
            } else {
                that.state.modelProfile.Recipient.forEach(e => {
                    if (e.Id == that.state.model.Id) {
                        e = that.state.model;

                    }
                });
            }
           
            
        }
       
        //update profile ajax
        $.ajax({
            url: "/account/profile",
            type: "POST",
            data: this.state.modelProfile,
            success: function (response) {
                $("#loading").hide();
                if (response.status == "success") {
                    alertify.alert("Thành công", response.message, function () {
                        that.props.history.push("/accountapp/emptypage");
                        that.props.history.replace({
                            pathname: that.props.location.pathname,
                            search: that.props.location.search
                        });
                    });
                } else {
                    alertify.alert("Rất tiếc!", response.message);
                }
            },
            error: function (er) {
                $("#loading").hide();
                alertify.alert(
                    "Lỗi",
                    "Rất tiếc! Hệ thống có một số lỗi. Vui lòng thử lại!"
                );
            }
        });
    }

    componentDidMount() {
        document.title = "Danh sách địa chỉ";
    }

    render() {
        return (
            <React.Fragment>
                {this.state.ex && this.state.model ? (
                    <React.Fragment>
                        <form
                            action=""
                            method="post"
                            className="section-body"
                            id="accountinfoform"
                            onSubmit={e => {
                                e.preventDefault();
                                this.submitprofile();
                            }}
                        >
                           
                            <div className="form-group">
                                <label>Họ và tên (<span className="required">*</span>)</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    name="fullname"
                                    placeholder="Họ và tên"
                                    value={this.state.model.FullName}
                                    onChange={e => {
                                        this.state.model.FullName = e.target.value;
                                        this.setState(this.state);
                                    }}
                                />
                            </div>
                          
                            <div className="form-group">
                                <label>Số điện thoại (<span className="required">*</span>)</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    name="phonenumber"
                                    placeholder="Số điện thoại"
                                    value={this.state.model.PhoneNumber}
                                    onChange={e => {
                                        this.state.model.PhoneNumber = e.target.value;
                                        this.setState(this.state);
                                    }}
                                />
                            </div>
                           
                      
                            <div className="form-group">
                                <label>Tỉnh, thành phố(<span className="required">*</span>)</label>
                                <select className="form-control"
                                    onChange={e => {
                                        var that = this;
                                        if (e.target.value == "" || e.target.value == null) {
                                            that.state.ex.DistrictList = [];
                                            that.state.ex.WardList = [];
                                            that.state.model.District = "";
                                            that.state.model.Ward = "";
                                            that.setState(that.state);
                                            return -1;
                                        };

                                        this.state.ex.DistrictList = this.state.ex.DistrictTempList.filter(d => { return d.City.Name == e.target.value });
                                        that.state.ex.WardList = [];
                                        that.state.model.Ward = "";
                                        this.state.model.City = e.target.value;
                                        this.setState(this.state);
                                    }}>
                                    <option value="" readOnly>-- Chọn tỉnh, thành phố --</option>
                                    {this.state.ex.CityList ? this.state.ex.CityList.map(c => {
                                        return <option value={c.Name} selected={this.state.model.City == c.Name ? "selected" : ""}>{c.Name}</option>
                                    }) : <option value="" readOnly>-- Chọn tỉnh, thành phố --</option>}

                                </select>
                            </div>
                            <div className="form-group">
                                <label>Quận, huyện (<span className="required">*</span>)</label>
                                <select
                                    className="form-control"
                                    onChange={e => {
                                        var that = this;
                                        if (e.target.value == "" || e.target.value == null) {
                                            this.state.ex.WardList = [];
                                            that.state.model.Ward = "";
                                            that.setState(that.state);
                                            return -1;
                                        };

                                        this.state.ex.WardList = this.state.ex.WardTempList.filter(d => { return d.District.Name == e.target.value });
                                        this.state.model.District = e.target.value;
                                        this.setState(this.state);
                                    }}>
                                    <option value="" readOnly>-- Chọn quận, huyện --</option>
                                    {this.state.ex.DistrictList ? this.state.ex.DistrictList.map(c => {
                                        return <option value={c.Name} selected={this.state.model.District == c.Name ? "selected" : ""}>{c.Name}</option>
                                    }) : <option value="" readOnly>-- Chọn quận, huyện --</option>}

                                </select>
                            </div>
                            <div className="form-group">
                                <label>Phường, xã (<span className="required">*</span>)</label>
                                <select
                                    className="form-control"
                                    onChange={e => {
                                        this.state.model.Ward = e.target.value;
                                        this.setState(this.state);
                                    }}>
                                    <option value="" readOnly>-- Chọn quận, huyện --</option>
                                    {this.state.ex.WardList ? this.state.ex.WardList.map(c => {
                                        return <option value={c.Name} selected={this.state.model.Ward == c.Name ? "selected" : ""}>{c.Name}</option>
                                    }) : <option value="" readOnly>-- Chọn quận, huyện --</option>}

                                </select>
                            </div>
                            <div className="form-group">
                                <label>Địa chỉ (<span className="required">*</span>)</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    name="address"
                                    placeholder="Địa chỉ"
                                    value={this.state.model.Street}
                                    onChange={e => {
                                        this.state.model.Street = e.target.value;
                                        this.setState(this.state);
                                    }}
                                />
                            </div>
                           
                            <div className="form-group">
                                
                                <input
                                    type="checkbox"  
                                    name="default"
                                    checked={this.state.model.IsDefault}
                                    value={this.state.model.IsDefault}
                                    onChange={e => {
                                        this.state.model.IsDefault = e.target.checked;
                                        this.setState(this.state);
                                    }}
                                />
                                <label>Chọn làm địa chỉ mặc định</label>
                            </div>
                            <div className="btn-container">
                                
                                <button className="btn btn-primary" type="submit">
                                    Cập nhật địa chỉ
                                </button>
                            </div>
                        </form>
                    </React.Fragment>
                ) : ""}
            </React.Fragment>
            )
    }
}

export default addressAddUpdate;
