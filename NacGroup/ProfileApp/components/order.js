﻿import React, { Component } from "react";
import { Link } from "react-router-dom";

import {
    waiting,
    processing,
    delivery,
    complete,
    cancel,
    order_history,
    c_date,
    total,
    status,
    order_number,
    order_id,
    order
} from "../constants/orderstaticmessage";
import moment from "moment";

function formatPhoneNumber(phoneNumberString) {
  var cleaned = ("" + phoneNumberString).replace(/\D/g, "");
  var match = cleaned.match(/^(\d{3})(\d{3})(\d{4})$/);
  if (match) {
    return [match[1], "-", match[2], "-", match[3]].join("");
  }
  return phoneNumberString;
}
function order_status1(status) {
    var html = ''
    switch (status) {
        case 0:
            html += "ON_HOLD";
            break;
        case 1:
            html += "PENDING_PAYMENT";
            break;
        case 2:
            html += "PAYMENT_RECEIVED";
            break;
        case 3:
            html += "PACKAGING";
            break;
        case 4:
            html += "ORDER_SHIPPED";
            break;
        case 5:
            html += "ORDER_ARCHIVED";
            break;
        case 6:
            html += "BACK_ORDER";
            break;
        case 7:
            html += "CANCELED";
            break;
    }
    return html;
}
import AppContext from "./app-context";
class Order extends Component {
  constructor(props,context) {
      super(props, context);
    this.state = {
      model: null
    };
      var that = this;
      $("#loading").show();
      $.get("/api/accountapp/order-histories", res => {
          that.state.model = res.data.Results;
              that.setState(that.state);
              $(".acc-menulst li").removeClass("active");
              $(".acc-menulst li[data-id='order']").addClass("active");
              $("#loading").hide();
            }).fail(res => {
                location.href = "/account/login?ReturnUrl=/accountapp/orderhistory";
            });
  }

  componentDidMount() {
        document.title = order[this.context.Language];
  }

  render() {
    return this.state.model ? (
        <div className="orderlist">
            <h2 className="section-header h5">Lịch sử đơn hàng</h2>
                <table className="table table-hover hidden-xs">
                    <thead>
                        <tr>
                        <th>#Mã đơn hàng</th>
                        <th>Ngày tạo</th>
                        <th>Thành tiền</th>
                        <th>Trang thái</th>
                        </tr>
                    </thead>
                       <tbody>
                    {this.state.model.map(e => {
                        return (
                            <tr>
                                <td><Link to={`/accountapp/orderhistory/` + e.OrderNumber}>#{e.OrderNumber}</Link></td>
                                <td>{moment.utc(e.CreatedDate).format("MM/DD/YYYY hh:mm:ss A")}</td>
                                <td>
                                    {this.context.CurrencyType === 0 ? (
                                        e.TotalAmount
                                            ? e.TotalAmount.formatMoney(
                                                2,
                                                ".",
                                                ",",
                                                "$"
                                            )
                                            : "0"
                                    ) : (
                                            e.TotalAmount
                                                ? e.TotalAmount.formatMoney(
                                                    0,
                                                    ",",
                                                    "."
                                                )
                                                : "0"
                                        )}
                                </td>
                                <td>{order_status1(e.OrderStatus)}</td>
                            </tr>
                        );
                        })}
                     </tbody>
                </table>
            <div className="visible-xs">
                {this.state.model.map(e => {
                    return (
                        <div className="item">
                            <p><Link to={`/accountapp/orderhistory/` + e.OrderNumber}>{order_number[this.context.Language]}: #{e.OrderNumber}</Link></p>
                            <p>{moment.utc(e.OrderDate).format("MM/DD/YYYY hh:mm:ss A")}</p>
                            <p>
                                <span>{total[this.context.Language]}:
                                {this.context.CurrencyType === 0 ? (
                                    e.TotalAmount
                                        ? e.TotalAmount.formatMoney(
                                            2,
                                            ".",
                                            ",",
                                            "$"
                                        )
                                        : "0"
                                ) : (
                                        e.TotalAmount
                                            ? e.TotalAmount.formatMoney(
                                                0,
                                                ",",
                                                "."
                                            )
                                            : "0"
                                    )}</span>
                            </p>
                        </div>
                        )
                })}
              
                </div>
            </div>
    ) : (
      <div />
    );
  }
}
Order.contextType = AppContext;
export default Order;
