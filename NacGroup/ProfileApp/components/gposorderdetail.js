﻿import React, { Component } from "react";
import { Link } from "react-router-dom";
import "../plugins/string-helper";
import { GLOBAL_ERROR_MESSAGE } from "../constants/message";
import moment from "moment";

function formatPhoneNumber(phoneNumberString) {
  var cleaned = ("" + phoneNumberString).replace(/\D/g, "");
  var match = cleaned.match(/^(\d{3})(\d{3})(\d{4})$/);
  if (match) {
    return [match[1], "-", match[2], "-", match[3]].join("");
  }
  return phoneNumberString;
}

class orderdetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      model: null
    };
    var that = this;
    var id = that.props.match.params.id;
    $.get("/api/accountapp/order-histories/" + id, res => {
      that.state.model = res.data;
      that.setState(that.state);
      $(".acc-menulst li").removeClass("active");
      $(".acc-menulst li[data-id='gpos-order']").addClass("active");
    }).fail(res => {
      //location.href =
      //  "/account/login?ReturnUrl=/accountapp/orderhistory/" + id;
    });
  }

  componentDidMount() {
    document.title = "Order Pickup Detail";
  }

  render() {
    return this.state.model ? (
      <div className="order-detail">
        <div className="section-header flex-center hidden-xs hidden-sm">
          <h2 className="h4">
            Order Detail #{this.state.model.OrderNumber}
            <p>
              Request Time :{" "}
              {moment
                .utc(this.state.model.RequestDeliveryTime)
                .format("MM/DD/YYYY hh:mm:ss A")}
            </p>
          </h2>
          <div className="date">
            CreatedDate:{" "}
            {moment
              .utc(this.state.model.OrderDate)
              .format("MM/DD/YYYY hh:mm:ss A")}
          </div>
        </div>
        <div className="section-header  visible-xs visible-sm">
          <h2 className="h4">
            Order Detail #{this.state.model.OrderNumber}
            <p>
              Request Time :{" "}
              {moment
                .utc(this.state.model.RequestDeliveryTime)
                .format("MM/DD/YYYY hh:mm:ss A")}
            </p>
          </h2>
          <div className="date">
            CreatedDate:{" "}
            {moment
              .utc(this.state.model.OrderDate)
              .format("MM/DD/YYYY hh:mm:ss A")}
          </div>
        </div>
        <div className="section-body flex-container" id="orderdetail">
          <div className="col">
            <h2 className="col-header h">Customer Information</h2>
            <div className="col-body">
              <p className="">
                {this.state.model.CustomerFirstName}{" "}
                {this.state.model.CustomerLastName}
              </p>
              <p>Phone: {this.state.model.CustomerPhone}</p>
              <p>Email: {this.state.model.CustomerEmailAddress}</p>
            </div>
          </div>
          <div className="col">
            <h2 className="col-header h">Payment Information</h2>
            <div className="col-body">
              <p className="">
                <span>Method: Credit Card</span>
              </p>
              <p className="info">
                <i>
                  <span>
                    Status:{" "}
                    {this.state.model.Paid == false ? "Spending" : "Paid"}
                  </span>
                </i>
              </p>
            </div>
          </div>
          <div className="col hidden">
            <h2 className="col-header h">Payment Method</h2>
            <div className="col-body">
              <p className="info">
                <span>Credit Card</span>
              </p>
              <p className="">
                <span>
                  {" "}
                  {this.state.model.Paid == false ? "Spending" : "Paid"}
                </span>
              </p>
            </div>
          </div>
          <div className="col" id="listitemorder">
            <div className="col-header flex-container">
              <div className="text-container">Name</div>
              <div className="price">Price</div>
              <div className="quantity-container">Quantity</div>
              <div className="tax">Tax</div>
              <div className="total">Total</div>
            </div>
            <div className="col-body">
              {this.state.model.SaleOrderItems
                ? this.state.model.SaleOrderItems.map(e => {
                    return (
                      <div className="item flex-container">
                        <div className="text-container">
                          <h3 className="h">
                            <a href="javascript:;">{e.Name}</a>
                          </h3>
                        </div>
                        <div className="price">
                          <div className="new-price h">${e.UnitPrice}</div>
                        </div>
                        <div className="quantity-container">{e.Quantity}</div>
                        <div className="tax">${e.SaleTax}</div>
                        <div className="total-container">${e.GrandTotal}</div>
                      </div>
                    );
                  })
                : ""}
            </div>
            <div className="col-footer">
              <h2 className="h5">Payment</h2>
              <div className="item flex-center">
                <label for="">Sub Total:</label>
                <p className="h2">${this.state.model.SubtotalBeforeTax}</p>
              </div>
              <div className="item flex-center">
                <label for="">Total Tax:</label>
                <p className="h2">${this.state.model.SaleTax}</p>
              </div>
              <div className="item flex-center">
                <label for="">Total:</label>
                <p className="h2">${this.state.model.GrandTotal}</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    ) : (
      <div />
    );
  }
}

export default orderdetail;
