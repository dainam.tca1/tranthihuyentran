﻿import React, { Component } from "react";

import "../plugins/string-helper";
import {
    waiting,
    processing,
    delivery,
    complete,
    cancel,
    order_history,
    c_date,
    status,
    order_number,
    order_id,
    order,
    not_paid,
    paid,
    tranfer,
    online,
    order_status,
    order_detail,
    c_info,
    phone,
    address,
    pay_info,
    method,
    ava,
    name,
    price,
    quantity,
    total,
    payment,
    sub_total,
    pay_method
} from "../constants/orderstaticmessage";
import AppContext from "./app-context";
import moment from "moment";

function formatPhoneNumber(phoneNumberString) {
  var cleaned = ("" + phoneNumberString).replace(/\D/g, "");
  var match = cleaned.match(/^(\d{3})(\d{3})(\d{4})$/);
  if (match) {
    return [match[1], "-", match[2], "-", match[3]].join("");
  }
  return phoneNumberString;
}
function order_status1(status) {
    var html = ''
    switch (status) {
        case 0:
            html += "ON_HOLD";
            break;
        case 1:
            html += "PENDING_PAYMENT";
            break;
        case 2:
            html += "PAYMENT_RECEIVED";
            break;
        case 3:
            html += "PACKAGING";
            break;
        case 4:
            html += "ORDER_SHIPPED";
            break;
        case 5:
            html += "ORDER_ARCHIVED";
            break;
        case 6:
            html += "BACK_ORDER";
            break;
        case 7:
            html += "CANCELED";
            break;
    }
    return html;
}
function payment_status(status) {
    var html = ''
    switch (status) {
        case 0:
            html += "CAPTURED";
            break;
        case 1:
            html += "AUTHORIZED";
            break;
        case 2:
            html += "DECILNED";
            break;  
        case 3:
            html += "ERROR";
            break;  
        case 4:
            html += "REVIEW";
            break;  
        case 5:
            html += "VOIDED";
            break;  
        case 6:
            html += "UNKNOWN";
            break;  
    }
    return html;
}
function payment_method(status) {
    var html = ''
    switch (status) {
        case 0:
            html += 'COD';
            break;
        case 1:
            html += "Credit Card";
            break;  
    }
    return html;
}

class orderdetail extends Component {
  constructor(props,context) {
    super(props,context);
    this.state = {
      model: null
    };
    var that = this;
      var id = that.props.match.params.id;
      $("#loading").show();
      $.get("/api/accountapp/order-histories/" + id, res => {
          that.state.model = res.data;
          that.setState(that.state);
          $(".acc-menulst li").removeClass("active");
          $(".acc-menulst li[data-id='order']").addClass("active");
          $("#loading").hide();
    }).fail(res => {
      location.href =
        "/account/login?ReturnUrl=/accountapp/orderhistory/" + id;
    });
  }

  componentDidMount() {
      document.title = order_detail[this.context.Language];
  }

    render() {
        return this.state.model ? (
            <div className="order-detail">
                <div className="section-header flex-center hidden-xs hidden-sm">
                    <h2 className="h4">
                        Chi tiết đơn hàng  #<span className="text-primary">{this.state.model.OrderNumber}</span> - <span className="text-bold">{order_status1(this.state.model.OrderStatus)}</span>

                    </h2>
                    <div className="date">Ngày tạo: {moment.utc(this.state.model.CreatedDate).format("DD/MM/YYYY hh:mm:ss A")}</div>
                </div>
                <div className="section-header  visible-xs visible-sm">
                    <h2 className="h4">
                        {order_detail[this.context.Language]}  #{this.state.model.OrderNumber}
                    </h2>
                    <div className="date">{c_date[this.context.Language]}: {moment.utc(this.state.model.CreatedDate).format("DD/MM/YYYY hh:mm:ss A")}</div>
                </div>
                <div className="section-body flex-container" id="orderdetail-2">
                    <div className="col">
                        <h2 className="col-header h">
                            Thông tin người nhận
                            </h2>
                        <div className="col-body">
                            <p className="">
                                <p className="">{this.state.model.Recipient.FullName}</p>
                            </p>
                            <p>{phone[this.context.Language]}: {this.state.model.CustomerPhone}</p>
                            <p>{address[this.context.Language]}: {this.state.model.Street}, {this.state.model.Ward}, {this.state.model.District} {this.state.model.City}</p>
                            <p>VN</p>
                        </div>
                    </div>
                    <div className="col">
                        <h2 className="col-header h">
                            Thông tin thanh toán
                     </h2>
                        <div className="col-body">
                            <p className="">
                                <p className="">{this.state.model.Recipient.FullName}</p>
                            </p>
                            <p>{phone[this.context.Language]}: {this.state.model.CustomerPhone}</p>
                            <p>{address[this.context.Language]}: {this.state.model.Street}, {this.state.model.Ward}, {this.state.model.District} {this.state.model.City}</p>
                            <p>VN</p>
                        </div>
                    </div>       
                    <div className="col">
                        <h2 className="col-header h">
                            Phương thức thanh toán
                        </h2>
                        <div className="col-body">
                            <p className="">
                                <span>Phương thức: {payment_method(this.state.model.PaymentMethod)} </span>
                            </p>
                            <p className="info">
                                <i>
                                    <span>Trạng thái: {payment_status(this.state.model.PaymentStatus)}</span>
                                </i></p>

                        </div>
                    </div>
                    <div className="col hidden">
                        <h2 className="col-header h">
                            {pay_method[this.context.Language]}
                            </h2>
                        <div className="col-body">
                            <p className="info"><span>{payment_method(this.state.model.PaymentMethod)}</span></p>
                            <p className="">
                                <span><span>{order_status[this.context.Language]}: {payment_status(this.state.model.PaymentStatus)}</span></span>
                            </p>
                        </div>
                    </div>
                    <div className="col" id="listitemorder">
                        <div className="col-header flex-container">
                            <div className="img-container">Hình ảnh</div>
                            <div className="text-container">Tên</div>
                            <div className="price">Đơn giá</div>
                            <div className="quantity-container">Số lượng</div>
                            <div className="total">Thành tiền</div>
                        </div>
                        <div className="col-body">
                            {this.state.model.OrderItems ? this.state.model.OrderItems.map(e => {
                                return (
                                    <div className="item flex-container">
                                        <div className="img-container">
                                            <img src={e.Avatar} />
                                        </div>
                                        <div className="text-container">
                                            <h3 className="h">
                                                <a href="javascript:;">{e.Name}</a>
                                            </h3>
                                        </div>
                                        <div className="price">
                                            <div className="new-price h">
                                                {this.context.CurrencyType === 0 ? (
                                                    e.Price
                                                        ? e.Price.formatMoney(
                                                            2,
                                                            ".",
                                                            ",",
                                                            "$"
                                                        )
                                                        : "0"
                                                ) : (
                                                        e.Price
                                                            ? e.Price.formatMoney(
                                                                0,
                                                                ",",
                                                                "."
                                                            )
                                                            : "0"
                                                    )}
                                            </div>
                                        </div>
                                        <div className="quantity-container">
                                            {e.Quantity}
                                        </div>

                                        <div className="total-container">
                                            {this.context.CurrencyType === 0 ? (
                                                e.Price
                                                    ? (e.Price * e.Quantity).formatMoney(
                                                        2,
                                                        ".",
                                                        ",",
                                                        "$"
                                                    )
                                                    : "0"
                                            ) : (
                                                    
                                                        e.Price
                                                            ? (e.Price * e.Quantity).formatMoney(
                                                                0,
                                                                ",",
                                                                "."
                                                            )
                                                            : "0"
                                                )}
                                        </div>
                                    </div>
                                )
                            }) : ""}
                        </div>
                        <div className="col-footer">
                            
                            <div className="item flex-center">
                                <label for="">Tạm tính:</label>
                                <p className="h2">
                                    {this.context.CurrencyType === 0 ? (
                                        <td>
                                            {this.state.model.SubTotalAmount
                                                ? this.state.model.SubTotalAmount.formatMoney(
                                                    2,
                                                    ".",
                                                    ",",
                                                    "$"
                                                )
                                                : "0"}
                                        </td>
                                    ) : (
                                            <td>
                                                {this.state.model.SubTotalAmount
                                                    ? this.state.model.SubTotalAmount.formatMoney(
                                                        0,
                                                        ",",
                                                        "."
                                                    )
                                                    : "0"}
                                            </td>
                                        )}
                                </p>
                            </div>
                            <div className="item flex-center">
                                <label for="">Phí vận chuyển:</label>
                                <p className="h2">
                                    {this.context.CurrencyType === 0 ? (
                                        this.state.model.ShippingAmount
                                            ? this.state.model.ShippingAmount.formatMoney(
                                                2,
                                                ".",
                                                ",",
                                                "$"
                                            )
                                            : "0"
                                    ) : (
                                            this.state.model.ShippingAmount
                                                ? this.state.model.ShippingAmount.formatMoney(
                                                    0,
                                                    ",",
                                                    ".",
                                                )
                                                : "0"
                                        )}
                                </p>
                            </div>
                            <div className="item flex-center">
                                <label for="">Giảm giá:</label>
                                <p className="h2">
                                    {this.context.CurrencyType === 0 ? (
                                        this.state.model.DiscountAmount
                                            ? this.state.model.DiscountAmount.formatMoney(
                                                2,
                                                ".",
                                                ",",
                                                "$"
                                            )
                                            : "0"
                                    ) : (
                                            this.state.model.DiscountAmount
                                                ? this.state.model.DiscountAmount.formatMoney(
                                                    0,
                                                    ",",
                                                    ".",
                                                )
                                                : "0"
                                        )}
                                </p>
                            </div>
                            <div className="item flex-center">
                                <label for="">Thuế:</label>
                                <p className="h2">
                                    {this.context.CurrencyType === 0 ? (
                                        this.state.model.TotalTax
                                            ? this.state.model.TotalTax.formatMoney(
                                                2,
                                                ".",
                                                ",",
                                                "$"
                                            )
                                            : "0"
                                    ) : (
                                            this.state.model.TotalTax
                                                ? this.state.model.TotalTax.formatMoney(
                                                    0,
                                                    ",",
                                                    ".",
                                                )
                                                : "0"
                                        )}
                                </p>
                            </div>
                            <div className="item flex-center">
                                <label for="">Thành tiền:</label>
                                <p className="h2">
                                    {this.context.CurrencyType === 0 ? (
                                        this.state.model.TotalAmount
                                            ? this.state.model.TotalAmount.formatMoney(
                                                2,
                                                ".",
                                                ",",
                                                "$"
                                            )
                                            : "0"
                                    ) : (
                                            this.state.model.TotalAmount
                                                ? this.state.model.TotalAmount.formatMoney(
                                                    0,
                                                    ",",
                                                    ".",
                                                )
                                                : "0"
                                        )}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        ) : (
                <div />
            )
    }
}
orderdetail.contextType = AppContext;
export default orderdetail;
