import React from "react";

import AppContext from "./app-context";

class AppProvider extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
        FeatureList: [],
        CurrencyType: 0,
        Language: 0,
    };
    var that = this;
    $.ajax({
      url: `/cms/api/cmsplugin/getfeatures`,
      type: "GET",
      dataType: "json",
      contentType: "application/json",
      success: response => {
        that.state.FeatureList = response;
        that.setState(that.state);
      },
      error: function(er) {
        //toastr["error"]("Error", "error");
      }
    });
      $.ajax({
          url: `/cms/api/cmsplugin/GetCurrentType`,
          type: "GET",
          dataType: "json",
          contentType: "application/json",
          success: response => {
              that.state.CurrencyType = response;
              that.setState(that.state);
          },
          error: function (er) {
             // toastr["error"]("Error", "error");
          }
      });
      //if (Cookies.get("cmslang")) {
      //    this.state.Language = Cookies.get("cmslang");
      //} else {
      //    this.state.Language = 0;
      //}
  }

  render() {
    return (
      <AppContext.Provider value={this.state}>
        {this.props.children}
      </AppContext.Provider>
    );
  }
}

export default AppProvider;
