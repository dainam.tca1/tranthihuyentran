﻿import React, { Component } from "react";
import { Link } from "react-router-dom";
import "../plugins/string-helper";

function formatPhoneNumber(phoneNumberString) {
  var cleaned = ("" + phoneNumberString).replace(/\D/g, "");
  var match = cleaned.match(/^(\d{3})(\d{3})(\d{4})$/);
  if (match) {
    return [match[1], "-", match[2], "-", match[3]].join("");
  }
  return phoneNumberString;
}

class AddressList extends Component {
  constructor(props) {
      super(props);
    this.state = {
        model: null,
        ex: {        
            Title: null,
            Url: "billingAddress"
        }
    };
      var that = this;
      $("#loading").show();
      $.get("/account/profile", res => {
          $("#loading").hide();
          that.state.model = res;
          this.state.ex.Title = "Billing Address";
          this.state.ex.Url = "billingAddress";
          $(".acc-menulst li").removeClass("active");
          $(".acc-menulst li[data-id='b-address']").addClass("active");
        that.setState(that.state);
    }).fail(res => {
        location.href = "/account/login?ReturnUrl=/accountapp/billingAddress";
    });
  }

  removeAddress(id) {
      var that = this;
      if (id == "" || id == null) {
          alertify.alert("Opps!", "Parameter invalid");
          return -1;
      }    
      alertify.confirm('Are you sure!', 'Data not recover', function () {
          $("#loading").show();
          that.state.model.Billing = that.state.model.Billing.filter(e => { return e.Id != id });
          if (that.state.model.Billing.length == 0) {
              that.state.model.Billing = [];
             
          }
         
          $.ajax({
              url: "/account/profile",
              type: "POST",
              data: that.state.model,
              success: function (response) {
                  $("#loading").hide();
                  if (response.status == "success") {
                      alertify.alert("Success", response.message, function () {
                          that.props.history.push("/accountapp/emptypage");
                          that.props.history.replace({
                              pathname: that.props.location.pathname,
                              search: that.props.location.search
                          });
                      });
                  } else {
                      alertify.alert("Opps!", response.message);
                  }
              },
              error: function (er) {
                  $("#loading").hide();
                  alertify.alert(
                      "Error",
                      "Opps! System has some error. Please try again!"
                  );
              }
          });

      }
          , function () { alertify.error('Cancel') });
      
  }

    componentDidMount() {
        if (this.state.ex.AddressType == 0) {
            document.title = "Shipping Address";
        } else {
            document.title = "Billing Address";
        }
   
  }

  render() {
    return this.state.model ? (
        <div className="addresslist">
            <h2 className="section-header flex-container flex-center h5">{this.state.ex.Title}   <Link to={`/accountapp/${this.state.ex.Url}/add`} className="btn btn-primary">Add new Info</Link></h2>
          
            <table className="table table-hover hidden-xs" style={{"width":"100%"}}>
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Customer Name</th>
                        <th>Address</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    {this.state.model.Billing.map(c => {
                        return (
                            <tr>
                                <td><Link to={`/accountapp/${this.state.ex.Url}/update/` + c.Id}>#{c.BusinessName}</Link></td>
                                <td>{c.FirstName} {c.LastName}</td>
                                <td>{`${c.Street1}, ${c.City}, ${c.State.Value} ${c.ZipCode}, ${c.Country.Name}`}</td>
                                <td>
                                    <a href="javascript:void(0)" onClick={e => { this.removeAddress(c.Id) }} ><span className="nac-trash"></span></a>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
            <div className="visible-xs">
                {this.state.model.Billing.map(c => {
                    return (
                        <div className="item">
                            <p><Link to={`/accountapp/${this.state.ex.Url}/update/` + c.Id}>#{c.BusinessName}</Link></p>
                            <p>{`${c.Street1}, ${c.City}, ${c.State.Value} ${c.ZipCode}, ${c.Country.Name}`} </p>
                            <p>
                                <a href="javascript:void(0)" onClick={e => { this.removeAddress(c.Id) }} ><span className="nac-trash"></span></a>
                            </p>
                        </div>
                    )
                })}

            </div>
        </div>
    ) : (
      <div />
    );
  }
}

export default AddressList;
