﻿import React, { Component } from "react";
import { Link } from "react-router-dom";
import "../plugins/string-helper";
import "../plugins/string-helper";

import moment from "moment";

function formatPhoneNumber(phoneNumberString) {
  var cleaned = ("" + phoneNumberString).replace(/\D/g, "");
  var match = cleaned.match(/^(\d{3})(\d{3})(\d{4})$/);
  if (match) {
    return [match[1], "-", match[2], "-", match[3]].join("");
  }
  return phoneNumberString;
}

class GPosOrder extends Component {
  constructor(props) {
    super(props);
    this.state = {
      model: null
    };
    var that = this;
      $.get("/api/accountapp/order-histories", res => {
          that.state.model = res.data;       
              that.setState(that.state);
              $(".acc-menulst li").removeClass("active");
              $(".acc-menulst li[data-id='gpos-order']").addClass("active");
            }).fail(res => {
               // location.href = "/account/login?ReturnUrl=/accountapp/orderhistory/";
            });
  }

  componentDidMount() {
    document.title = "Order Pickup History";
  }

  render() {
    return this.state.model ? (
        <div className="orderlist">
            <h2 className="section-header h5">Order PickUp History </h2>
                <table className="table table-hover hidden-xs">
                    <thead>
                        <tr>
                            <th>#Order Id</th>
                            <th>Created Date</th>
                            <th>Total</th>
                        </tr>
                    </thead>
                       <tbody>
                    {this.state.model.map(e => {
                        return (
                            <tr>
                                <td><Link to={`/accountapp/gposorderhistory/` + e.OrderNumber}>#{e.OrderNumber}</Link></td>
                                <td>{moment.utc(e.OrderDate).format("MM/DD/YYYY hh:mm:ss A")}</td>
                                <td>${e.GrandTotal}</td>
                            </tr>
                        );
                        })}
                     </tbody>
                </table>
            <div class="visible-xs">
                {this.state.model.map(e => {
                    return (
                        <div class="item">
                            <p><Link to={`/accountapp/gposorderhistory/` + e.OrderNumber}>Order Number: #{e.OrderNumber}</Link></p>
                            <p>{moment.utc(e.OrderDate).format("MM/DD/YYYY hh:mm:ss A")}</p>
                            <p>
                                <span>Total: ${e.GrandTotal}</span>
                            </p>
                        </div>
                        )
                })}
              
                </div>
            </div>
    ) : (
      <div />
    );
  }
}

export default GPosOrder;
