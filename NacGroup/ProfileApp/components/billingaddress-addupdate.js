﻿import React, { Component } from "react";
import { Link } from "react-router-dom";
import "../plugins/string-helper";
import AddressCmsModel from "./models/address-cmsmodel";
import ProfileCmsModel from "./models/profile-cmsmodel";
function formatPhoneNumber(phoneNumberString) {
    var cleaned = ("" + phoneNumberString).replace(/\D/g, "");
    var match = cleaned.match(/^(\d{3})(\d{3})(\d{4})$/);
    if (match) {
        return [match[1], "-", match[2], "-", match[3]].join("");
    }
    return phoneNumberString;
}

class AddressAddUpdate extends Component {
    constructor(props) {
        super(props);
        var action = null;
        if (document.location.href.indexOf("/accountapp/billingAddress/add") >= 0) {
            action = "add";
        } else if (
             document.location.href.indexOf("/accountapp/billingAddress/update") >= 0
        ) {
            action = "update";
        }
        var that = this;
        this.state = {
            model: new AddressCmsModel(),
            modelProfile: new ProfileCmsModel(),
            ex: {
                Title: null,
                Action: action,
                CountryList: [],
                StateList:[],
                Url: "billingAddress",
            }
        };
        that.setState(that.state);
        
        $("#loading").show();
        // Get All User
        $.get("/account/profile", res => {
            that.state.modelProfile = res;
            that.setState(that.state);
            $(".acc-menulst li").removeClass("active");
            $(".acc-menulst li[data-id='b-address']").addClass("active");
            $.get("/account/getcountry", res => {
                $("#loading").hide();
                that.state.ex.CountryList = res; 
                if (action == "update") {
                    if (that.state.modelProfile.Billing.length > 0) {
                        // Find user by id
                        that.state.ex.Title = "Update Billing Address";
                        that.state.model = that.state.modelProfile.Billing.find(e => { return e.Id == that.props.match.params.id });
                        that.state.ex.StateList = (that.state.ex.CountryList.find(country => { return country.Code == that.state.model.Country.Value }) != null) ?
                            that.state.ex.CountryList.find(country => { return country.Code == that.state.model.Country.Value }).StateList : [];
                        that.setState(that.state);
                    }
                    that.setState(that.state);
                } else {
                    that.state.ex.Title = "Add Profile Address";
                    that.setState(that.state);
                }
                that.setState(that.state);
            })    
            
        }).fail(res => {
            location.href = "/account/login?ReturnUrl=/accountapp/billingAddress";
        });
        
        
    }

    submitprofile() {
        $("#loading").show();
        var that = this;
        //#region Validate
        if (this.state.model.BusinessName == null || this.state.model.BusinessName == "") {
            $("#loading").hide();
            alertify.alert("Opps!", "Please enter display name address");
            return -1;
        }
        if (this.state.model.FirstName == null || this.state.model.FirstName == "") {
            $("#loading").hide();
            alertify.alert("Opps!", "Please enter first name");
            return -1;
        }
        if (this.state.model.LastName == null || this.state.model.LastName == "") {
            $("#loading").hide();
            alertify.alert("Opps!", "Please enter last name");
            return -1;
        }
        if (this.state.model.PhoneNumber == null || this.state.model.PhoneNumber == "") {
            $("#loading").hide();
            alertify.alert("Opps!", "Please enter phone number");
            return -1;
        }
        if (this.state.model.Street1 == null || this.state.model.Street1 == "") {
            $("#loading").hide();
            alertify.alert("Opps!", "Please enter address");
            return -1;
        }
        if (this.state.model.Country == null || this.state.model.Country == "") {
            $("#loading").hide();
            alertify.alert("Opps!", "Please enter country");
            return -1;
        }
        if (this.state.ex.StateList.length > 0) {
            if (this.state.model.State == null || this.state.model.State == "") {
                $("#loading").hide();
                alertify.alert("Opps!", "Please enter state");
                return -1;
            }
        }
        if (this.state.model.City == null || this.state.model.City == "") {
            $("#loading").hide();
            alertify.alert("Opps!", "Please enter city");
            return -1;
        }
       
       
        if (this.state.model.ZipCode == null || this.state.model.ZipCode == "") {
            $("#loading").hide();
            alertify.alert("Opps!", "Please enter zip code");
            return -1;
        }
        //#endregion
        if (this.state.ex.Action == "add") {
            // add new 
            if (this.state.modelProfile.Billing.length == 0) {
                //check if addresslist of user not found Id set = 1 
                this.state.model.Id = 1;
                this.state.model.IsDefault = true;
                // push object to array
                this.state.modelProfile.Billing.push(this.state.model);
               
            }
            
            else {
                //find address newest of modelProfile
                var old_address = this.state.modelProfile.Billing[this.state.modelProfile.Billing.length - 1];
                if (old_address == null) {
                    $("#loading").hide();
                    alertify.alert("Opps!", "Data not found");
                    return -1;
                }
                //if model IsDefault == true
                if (this.state.model.IsDefault == true) {
                    var find_old_default_address = this.state.modelProfile.Billing.filter(e => { return e.IsDefault == true });
                    if (find_old_default_address.length > 0) {
                        this.state.modelProfile.Billing = this.state.modelProfile.Billing ? this.state.modelProfile.Billing.map(e => {
                            return { ...e, IsDefault: false };
                        }) : []
                        this.setState(this.state);
                    }
                }
                this.state.model.Id = parseInt(old_address.Id) + 1;
                this.state.modelProfile.Billing.push(this.state.model);
                
            }
        }
        else {
            //update
            //if model IsDefault == true
            if (that.state.model.IsDefault == true) {
                that.state.modelProfile.Billing.forEach(e => {
                    if (e.Id == that.state.model.Id) {
                        e = that.state.model;
                        e.IsDefault = true;
                        
                    } else {
                        e.IsDefault = false;
                       
                    }

                })
            } else {
                that.state.modelProfile.Billing.forEach(e => {
                    if (e.Id == that.state.model.Id) {
                        e = that.state.model;
                       
                    }
                })
            }
        }
       
       
        //update profile ajax
        $.ajax({
            url: "/account/profile",
            type: "POST",
            data: this.state.modelProfile,
            success: (response) => {
                $("#loading").hide();
                if (response.status == "success") {
                    alertify.alert("Success", response.message, function () {
                        that.props.history.push("/accountapp/emptypage");
                        that.props.history.replace({
                            pathname: that.props.location.pathname,
                            search: that.props.location.search
                        });
                    });
                } else {
                    alertify.alert("Opps!", response.message);
                }
            },
            error: function (er) {
                $("#loading").hide();
                alertify.alert(
                    "Error",
                    "Opps! System has some error. Please try again!"
                );
            }
        });
    }

    componentDidMount() {
        document.title = "Billing Address";
    }

    render() {
        return (
            <React.Fragment>
                {this.state.ex && this.state.model ? (
                    <React.Fragment>
                        <form
                            action=""
                            method="post"
                            className="section-body"
                            id="accountinfoform"
                            onSubmit={e => {
                                e.preventDefault();
                                this.submitprofile();
                            }}
                        >
                            <div className="form-group">
                                <label>Business Name (<span className="required">*</span>)</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    name="nameaddress"
                                    placeholder="Business Name"
                                    value={this.state.model.BusinessName}
                                    onChange={e => {
                                        this.state.model.BusinessName = e.target.value;
                                        this.setState(this.state);
                                    }}
                                />
                            </div>
                            <div className="form-group">
                                <label>First Name (<span className="required">*</span>)</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    name="Firstname"
                                    placeholder="First Name"
                                    value={this.state.model.FirstName}
                                    onChange={e => {
                                        this.state.model.FirstName = e.target.value;
                                        this.setState(this.state);
                                    }}
                                />
                            </div>
                            <div className="form-group">
                                <label>Last Name (<span className="required">*</span>)</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    name="Lastname"
                                    placeholder="Last Name"
                                    value={this.state.model.LastName}
                                    onChange={e => {
                                        this.state.model.LastName = e.target.value;
                                        this.setState(this.state);
                                    }}
                                />
                            </div>
                            <div className="form-group">
                                <label>Phone Number</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    name="phonenumber"
                                    placeholder="Phone Number"
                                    value={this.state.model.PhoneNumber}
                                    onChange={e => {
                                        this.state.model.PhoneNumber = e.target.value;
                                        this.setState(this.state);
                                    }}
                                />
                            </div>
                            <div className="form-group">
                                <label>Address Line 1 (<span className="required">*</span>)</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    name="Address1"
                                    placeholder="Address Line 1"
                                    value={this.state.model.Street1}
                                    onChange={e => {
                                        this.state.model.Street1 = e.target.value;
                                        this.setState(this.state);
                                    }}
                                />
                            </div>
                            <div className="form-group">
                                <label>Address Line 2</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    name="Address1"
                                    placeholder="Address Line 2"
                                    value={this.state.model.Street2}
                                    onChange={e => {
                                        this.state.model.Street2 = e.target.value;
                                        this.setState(this.state);
                                    }}
                                />
                            </div>
                            <div className="form-group">
                                <label>Country (<span className="required">*</span>)</label>
                                <select className="form-control"
                                    onChange={e => {
                                        var that = this;   
                                        if (e.target.value == "" || e.target.value == null) {
                                            that.state.model.Country.Name = null;
                                            that.state.model.Country.Value = null;
                                            that.state.model.State.Name = null;
                                            that.state.model.State.Value = null;
                                            that.state.ex.StateList = [];
                                            that.setState(that.state);
                                            return -1;
                                        };
                                        $("#loading").show();
                                        var country = this.state.ex.CountryList.find(country => { return country.Code == e.target.value });
                                        this.state.model.Country.Name = country.Name;
                                        this.state.model.Country.Value = country.Code;
                                        if (this.state.model.Country == null || this.state.model.Country == "") {
                                            setTimeout(function () {
                                                that.state.model.State.Name = null;
                                                that.state.model.State.Value = null;
                                                that.state.ex.StateList = [];
                                                $("#loading").hide();
                                            }, 500)
                                            return -1;
                                        }
                                        this.state.ex.StateList = this.state.ex.CountryList.find(d => { return d.Code == this.state.model.Country.Value }).StateList;
                                        setTimeout(function () {  
                                            that.state.model.State.Name = null;
                                            that.state.model.State.Value = null;
                                            $("#loading").hide();
                                        }, 500)
                                        this.setState(this.state);
                                    }}>
                                    <option value="" readOnly>-- Select Country --</option>
                                    {this.state.ex.CountryList ? this.state.ex.CountryList.map(c => {
                                        return <option value={c.Code} selected={this.state.model.Country.Value == c.Code ? "selected" : ""}>{c.Name}</option>
                                    }) : <option value="" readOnly>-- Select Country --</option>}

                                </select>
                            </div>                            
                            <div className="form-group">
                                <label>State (<span className="required">*</span>)</label>
                                <select
                                    className="form-control"
                                    onChange={e => {
                                        var state = this.state.ex.StateList.find(state => { return state.CodeProvince == e.target.value });
                                       
                                        this.state.model.State.Name = state.Name;
                                        this.state.model.State.Value = state.CodeProvince;
                                       
                                        this.setState(this.state)
                                    }}>
                                    <option value="" readOnly>-- Select State --</option>
                                    {this.state.ex.StateList ? this.state.ex.StateList.map(c => {
                                        return <option value={c.CodeProvince} selected={this.state.model.State.Value == c.CodeProvince ? "selected" : ""}>{c.Name}</option>
                                    }) : <option value="">-- Select State --</option>}

                                </select>
                            </div>
                            <div className="form-group">
                                <label>City (<span className="required">*</span>)</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    name="City"
                                    placeholder="City"
                                    value={this.state.model.City}
                                    onChange={e => {
                                        this.state.model.City = e.target.value;
                                        this.setState(this.state);
                                    }}
                                />
                            </div>
                            <div className="form-group">
                                <label>Zip Code (<span className="required">*</span>)</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    name="zipcode"
                                    placeholder="Zip Code"
                                    value={this.state.model.ZipCode}
                                    onChange={e => {
                                        this.state.model.ZipCode = e.target.value;
                                        this.setState(this.state);
                                    }}
                                />
                            </div>
                            <div className="form-group">
                                <input
                                    type="checkbox"
                                    name="default"
                                    checked={this.state.model.IsDefault}
                                    value={this.state.model.IsDefault}
                                    onChange={e => {
                                        this.state.model.IsDefault = e.target.checked;
                                        this.setState(this.state);
                                    }}
                                />
                                <label>Make default</label>
                            </div>
                            <div className="btn-container">
                                <button className="btn btn-primary" type="submit">
                                    Update Address
                    </button>
                            </div>
                        </form>
                    </React.Fragment>
                ) : ""}
            </React.Fragment>
            )
    }
}

export default AddressAddUpdate;
