import React, { Component } from "react";
import { BrowserRouter, Redirect, Route } from "react-router-dom";

import profile from "./profile";
import empty from "./empty";
import giftcard from "./giftcard";
import giftcarddetail from "./giftcarddetail";
import orderdetail from "./orderdetail";
import order from "./order";
import ShippingAddress from "./shipping-address";
import ShippingAddUpdate from "./shippingaddress-addupdate";

import wishlist from "./wishlist";
import AppProvider from "./app-provider";
import Nav from "./nav";

class App extends Component {
  render() {
      return (
          <AppProvider>
              <BrowserRouter>
                  <div className="profile-page container">
                      <Nav />
                      <div className="profile-pagecontent">
                          <Route exact path="/accountapp/emptypage" component={empty} />
                          <Route
                              exact
                              path="/accountapp/giftcard/:id"
                              component={giftcarddetail}
                          />
                          <Route exact path="/accountapp/orderhistory/:id" component={orderdetail} />
                          <Route exact path="/accountapp/orderhistory" component={order} />
                          
                          <Route exact path="/accountapp/giftcard" component={giftcard} />
                          <Route exact path="/accountapp/profile" component={profile} />
                          <Route exact path="/accountapp/wishlist" component={wishlist} />
                          <Route exact path="/accountapp/shippingAddress" component={ShippingAddress} />
                          <Route exact path="/accountapp/shippingAddress/update/:id" component={ShippingAddUpdate} />
                          <Route exact path="/accountapp/shippingAddress/add" component={ShippingAddUpdate} />
                          <Route exact path="/accountapp" component={profile} />
                      </div>
                  </div>
              </BrowserRouter>
          </AppProvider>
    );
  }
}

export default App;
