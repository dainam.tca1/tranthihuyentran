const THEME_TEXT_TYPE = {
  Normal: 0,
  Html: 1,
  Link: 2
};

const THEMETYPE = {
  Group: 0,
  Image: 1,
  Text: 2,
  ImageList: 3,
  TextImageList: 4
};

const BOOKINGTYPE = {
  Individual: 0,
  Group: 1
};

const GENDERTYPE = {
  Undefine: 0,
  Male: 1,
  Female: 2
};

module.exports = {
  THEMETYPE,
  THEME_TEXT_TYPE,
  BOOKINGTYPE,
  GENDERTYPE
};
