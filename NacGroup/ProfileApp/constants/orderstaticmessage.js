//#region message Order Profile
const waiting = ["Waiting", "Đang đợi"];
const processing = ["Processing", "Đang xử lí"];
const delivery = ["Delivery", "Đang giao hàng"];
const complete = ["Complete", "Hoàn thành"];
const cancel = ["Cancel", "Hủy bỏ"];
const order_history = ["Order History", "Lịch sử đơn hàng"];
const c_date = ["Created Date", "Ngày tạo"];
const status = ["Status", "Trạng thái"];
const order_number = ["Order Number", "Mã đơn hàng"];
const total = ["Total", "Tổng cộng"];
const order_id = ["Order Id", "Mã đơn hàng"];
const order = ["Order", "Đơn hàng"];
const not_paid = ["NotPaid", "Đơn hàng"];
const paid = ["Paid", "Đơn hàng"];
const tranfer = ["Transfer", "Chuyển khoản"];
const online = ["Online", "Online"];
const order_detail = ["Order Detail", "Chi tiết đơn hàng"];
const c_info = ["Customer Information", "Thông tin khách hàng"];
const phone = ["Phone", "Số điện thoại"];
const address = ["Address", "Địa chỉ"];
const pay_info = ["Payment Information", "Thông tin thanh toán"];
const pay_method = ["Payment Method", "Phương thức thanh tóan"];
const method = ["Method", "Phương thức"];
const ava = ["Avatar", "Hình ảnh"];
const name = ["Name", "Tên"];
const price = ["Price", "Giá"];
const quantity = ["Quantity", "Số lượng"];
const payment = ["Payment", "Thanh toán"];
const sub_total = ["Sub Total", "Tạm tính"];
const order_status = ["Status", "Trạng thái"];
//#endregion
module.exports = {
    //#region message Order Profile
    waiting,
    processing,
    delivery,
    complete,
    cancel,
    order_history,
    c_date,
    status,
    order_status,
    order_number,
    total,
    order_id,
    order,
    not_paid,
    paid,
    tranfer,
    online,
    order_detail,
    c_info,
    phone,
    address,
    pay_info,
    method,
    ava,
    name,
    price,
    quantity,
    payment,
    sub_total,
    pay_method
//#endregion
};
