﻿using Microsoft.AspNetCore.Mvc;
using NacGroup.Module.Core.Extensions;
using NacGroup.Module.Order.Models.Schema;

namespace NacGroup.Themes.BlogNews.Views.Shared.Components.Footer
{
    [ViewComponent(Name = "Footer")]
    public class FooterComponent : ViewComponent
    {
        public FooterComponent()
        {
        }
        public IViewComponentResult Invoke()
        {
            ViewData["ShoppingCart"] = HttpContext.Session.Get<Order>("ShoppingCartNacGroup") ?? new Order();
            return View("Footer");
        }
    }
}
