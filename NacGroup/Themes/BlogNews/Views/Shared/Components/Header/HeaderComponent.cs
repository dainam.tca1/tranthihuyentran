﻿using Microsoft.AspNetCore.Mvc;
using NacGroup.Module.Core.Extensions;
using NacGroup.Module.Order.Models.Schema;

namespace NacGroup.Themes.BlogNews.Views.Shared.Components.Header
{
    [ViewComponent(Name = "Header")]
    public class HeaderViewComponent : ViewComponent
    {
        public HeaderViewComponent()
        {
          
        }
        public IViewComponentResult Invoke()
        {
            ViewData["ShoppingCart"] = HttpContext.Session.Get<Order>("ShoppingCartNacGroup") ?? new Order();
            return View("Header");
        }
    }
}
