import React, { Component } from "react";
import ReactJson from 'react-json-view'
import { Link } from "react-router-dom";
import AppSettingCmsModel from "../../../../core-module/cms/models/appsetting-cmsmodel";
import {
  GLOBAL_ERROR_MESSAGE,
  DATA_UPDATE_SUCCESSFULL,
  ERROR,
  SUCCESS,
  NOTI,
  DATA_NOT_RECOVER
} from "../../../../../libs/constants/message";

//ckeditor
import CkEditor from "../../../../core-module/cms/reactapp/components/ckeditor.jsx";

var apiurl = "/api/configkey";

class ConfigKeyAddUpdate extends Component {
  constructor(props) {
    super(props);
    var action = null;
    if (document.location.href.indexOf("/admin/configkey/add") >= 0) {
      action = "add";
    } else if (document.location.href.indexOf("/admin/configkey/update") >= 0) {
      action = "update";
    }
    this.state = {
      model: new AppSettingCmsModel(),
      ex: {
        Title: null,
        Action: action
      }
    };
    this.setState(this.state);
    if (action === "update") {
      var that = this;
      that.state.ex.Title = "Update Configkey";
      var id = that.props.match.params.id;
      App.startPageLoading({ animate: !0 });
      $.ajax({
        url: apiurl + "/" + that.state.ex.Action,
        type: "GET",
        dataType: "json",
        contentType: "application/json",
        data: { id: id },
        success: response => {
          App.stopPageLoading();
          if (response.status == "success") {
            that.state.model = response.data;
            that.state.ex.Title = "Update Configkey";
            document.title = that.state.ex.Title;
            that.setState(that.state);
          } else {
            toastr["error"](response.message, ERROR);
          }
        },
        error: function(er) {
          App.stopPageLoading();
          toastr["error"](GLOBAL_ERROR_MESSAGE, ERROR);
        }
      });
    } else {
      this.state.ex.Title = "Add Key";
      this.setState(this.state);
    }
  }

  // this.setState(this.state);
  submitForm() {
    var that = this;
    //  var id = that.props.match.params.id;
    App.startPageLoading({ animate: !0 });
    $.ajax({
      url: apiurl + "/" + that.state.ex.Action,
      type: "POST",
      dataType: "json",
      contentType: "application/json",
      data: JSON.stringify(that.state.model),
      success: response => {
        App.stopPageLoading();
        if (response.status == "success") {
          alertify.alert(SUCCESS, response.message, () => {
            that.props.history.push("/admin/emptypage");
            that.props.history.replace({
              pathname: that.props.location.pathname
            });
          });
        } else {
          toastr["error"](response.message, ERROR);
        }
      },
      error: function(er) {
        App.stopPageLoading();
        toastr["error"](GLOBAL_ERROR_MESSAGE, ERROR);
      }
    });
  }
  componentWillMount() {
    $("#scriptloading").html(
      `<script src="/statics/templates/admin/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>`
    );
  }

  componentDidMount() {
    document.title = this.state.ex.Title;
    $(".page-sidebar-menu .nav-item").removeClass("active");
    $(".page-sidebar-menu .nav-item[data-id='danh-sach-key']").addClass(
      "active"
    );
  }
  componentWillUnmount() {
    $("#scriptloading").html("");
  }
  render() {
    return (
      <React.Fragment>
        {this.state && this.state.model ? (
          <React.Fragment>
            <div className="page-bar">
              <ul className="page-breadcrumb">
                <li>
                  <a href="javascript:">Website Configuration</a>
                  <i className="fa fa-circle" />
                </li>
                <li>
                  <span>{this.state.ex.Title}</span>
                  <span className="hidden">{this.state.model._id}</span>
                </li>
              </ul>
              <div className="page-toolbar">
                <div className="btn-group pull-right">
                  <Link
                    className="btn green btn-sm btn-outline"
                    to="/admin/configkey/add"
                  >
                    Add <i className="icon-plus" />
                  </Link>
                  <Link
                    className="btn green btn-sm btn-outline"
                    to="/admin/configkey"
                  >
                    Back
                  </Link>
                  <button
                    type="button"
                    className="btn green btn-sm btn-outline"
                    onClick={e => {
                      this.submitForm();
                    }}
                  >
                    Save
                  </button>
                </div>
              </div>
            </div>
            <h3 className="page-title">{this.state.ex.Title}</h3>
            <div className="row">
              <div className="col-md-12">
                <div className="portlet box green-meadow">
                  <div className="portlet-title">
                    <div className="caption">
                      <i className=" icon-layers font-white" />
                      <span className="caption-subject font-white sbold uppercase">
                        CONFIG KEY FOR (FOR TECHNICAL ONLY)
                      </span>
                    </div>
                    <div className="tools">
                      <a href="" className="collapse" />
                    </div>
                  </div>
                  <div className="portlet-body">
                    <div className="form-body">
                      <div className="form-group">
                        <label className="control-label">Key</label>
                        <span className="required"> * </span>
                        <div className="input-icon right">
                          <i className="fa" />
                          <input
                            className="form-control"
                            value={this.state.model.Key}
                            placeholder="Key"
                            onChange={e => {
                              this.state.model.Key = e.target.value;
                              this.setState(this.state);
                            }}
                          />
                        </div>
                      </div>

                      <div className="form-group">
                        <label className="control-label">Value</label>
                        <span className="required" />
                        <div className="input-icon right">
                          <i className="fa" />
                          <ReactJson
                            src={
                              this.state.model.Value == null
                                ? {}
                                : this.state.model.Value
                            }
                            theme={"monokai"}
                            collapseStringsAfterLength={"20"}
                            onEdit={e => {
                              this.state.model.Value = e.updated_src;
                              this.setState(this.state);
                            }}
                            onDelete={e => {
                              this.state.model.Value = e.updated_src;
                              this.setState(this.state);
                            }}
                            onAdd={e => {
                              this.state.model.Value = e.updated_src;
                              this.setState(this.state);
                            }}
                            displayObjectSize={true}
                            enableClipboard={true}
                            indentWidth={4}
                            displayDataTypes={true}
                            iconStyle={"triangle"}
                          />
                          <p />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </React.Fragment>
        ) : (
          <div />
        )}
      </React.Fragment>
    );
  }
}

export default ConfigKeyAddUpdate;
