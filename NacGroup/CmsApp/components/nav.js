﻿import React, { Component } from "react";
import { Link } from "react-router-dom";
import AppContext from "./app-context";
import {
    dashboard,
    contact,
    services,
    service_ls,
    service_cat,
    pos_product,
    products,
    product_ls,
    product_cat,
    product_cus_cat,
    product_brands,
    product_att_group,
    product_att_name,
    product_att_value,
    label_ls,
    order_history,
    dgt_signage,
    device_ls,
    play_ls,
    video_ls,
    promotion,
    promotion_news,
    coupon_code,
    giftcard_coupon,
    blog,
    blog_ls,
    blog_cat,
    gallery,
    static_pages,
    staff,
    gpos_staff,
    conf_title,
    website_conf,
    booking_setting,
    booking_report,
    order_pickup_setting,
    giftcard_his,
    giftcard_temp_setting,
    theme,
    theme_config,
    menu_conf,
    ship_setting,
    ship_by_dist,
    ship_by_total_bill,
    admin_manager,
    key_configuration,
    comment,
    video,
    tags
} from "../modules/Core/models/corestaticmessage";
class Nav extends Component {
    render() {
        return (
            <div
                className="kt-aside  kt-aside--fixed  kt-grid__item kt-grid kt-grid--desktop kt-grid--hor-desktop"
                id="kt_aside"
            >
                <div className="kt-aside__brand kt-grid__item" id="kt_aside_brand">
                    <div className="kt-aside__brand-logo">
                        <a href="/">
                            <img src="/adminstatics/global/img/logowebnail.png" height="50" />
                        </a>
                    </div>
                    <div className="kt-aside__brand-tools">
                        <button
                            className="kt-aside__brand-aside-toggler"
                            id="kt_aside_toggler"
                        >
                            <span>
                                <i className="flaticon2-fast-back text-primary" />
                            </span>
                            <span>
                                <i className="flaticon2-fast-next text-primary" />
                            </span>
                        </button>
                    </div>
                </div>

                <div
                    className="kt-aside-menu-wrapper kt-grid__item kt-grid__item--fluid"
                    id="kt_aside_menu_wrapper"
                >
                    <div
                        id="kt_aside_menu"
                        className="kt-aside-menu"
                        data-ktmenu-vertical="1"
                        data-ktmenu-scroll="1"
                        data-ktmenu-dropdown-timeout="500"
                    >
                        <ul className="kt-menu__nav">
                            <li
                                className="kt-menu__item"
                                aria-haspopup="true"
                                data-id="dashboard"
                            >
                                <Link to="/admin/dashboard" className="kt-menu__link">
                                    <span className="kt-menu__link-icon">
                                        <i className="la la-home" />
                                    </span>
                                    <span className="kt-menu__link-text">{dashboard[this.context.Language]}</span>
                                </Link>
                            </li>

                            {this.context.FeatureList.find(m => m.Url === "booking") !=
                                null && (
                                    <li
                                        className="kt-menu__item"
                                        aria-haspopup="true"
                                        data-id="booking"
                                    >
                                        <Link to="/admin/booking" className="kt-menu__link">
                                            <span className="kt-menu__link-icon">
                                                <i className="la la-calendar-check-o" />
                                            </span>
                                            <span className="kt-menu__link-text">{booking_report[this.context.Language]}</span>
                                        </Link>
                                    </li>
                                )}
                            {this.context.FeatureList.find(m => m.Url === "contact") !=
                                null && (
                                    <li
                                        className="kt-menu__item"
                                        aria-haspopup="true"
                                        data-id="lien-he"
                                    >
                                        <Link to="/admin/contact" className="kt-menu__link">
                                            <span className="kt-menu__link-icon">
                                                <i className="la la-phone" />
                                            </span>
                                            <span className="kt-menu__link-text">{contact[this.context.Language]}</span>
                                        </Link>
                                    </li>
                                )}
                            {this.context.FeatureList.find(m => m.Url === "nail-services") !=
                                null && (
                                    <li
                                        className="kt-menu__item  kt-menu__item--submenu"
                                        aria-haspopup="true"
                                        data-ktmenu-submenu-toggle="hover"
                                        data-id="dich-vu"
                                    >
                                        <a
                                            href="javascript:;"
                                            className="kt-menu__link kt-menu__toggle"
                                        >
                                            <span className="kt-menu__link-icon">
                                                <i className="la la-dropbox" />
                                            </span>
                                            <span className="kt-menu__link-text">{services[this.context.Language]}</span>
                                            <i className="kt-menu__ver-arrow la la-angle-right" />
                                        </a>
                                        <div className="kt-menu__submenu">
                                            <span className="kt-menu__arrow" />
                                            <ul className="kt-menu__subnav">
                                                <li
                                                    className="kt-menu__item"
                                                    aria-haspopup="true"
                                                    data-id="danh-sach-dich-vu"
                                                >
                                                    <Link to="/admin/nailservice" className="kt-menu__link">
                                                        <i className="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                                            <span />
                                                        </i>
                                                        <span className="kt-menu__link-text">
                                                            {service_ls[this.context.Language]}
                                                        </span>
                                                    </Link>
                                                </li>
                                                <li
                                                    className="kt-menu__item"
                                                    aria-haspopup="true"
                                                    data-id="danh-muc-dich-vu"
                                                >
                                                    <Link
                                                        to="/admin/nailservicecategory"
                                                        href="#"
                                                        className="kt-menu__link"
                                                    >
                                                        <i className="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                                            <span />
                                                        </i>
                                                        <span className="kt-menu__link-text">
                                                            {service_cat[this.context.Language]}
                                                        </span>
                                                    </Link>
                                                </li>
                                            </ul>
                                        </div>
                                    </li>
                                )}
                            {this.context.FeatureList.find(m => m.Url === "pos-product") !=
                                null && (
                                    <li
                                        className="kt-menu__item  kt-menu__item--submenu"
                                        aria-haspopup="true"
                                        data-ktmenu-submenu-toggle="hover"
                                        data-id="san-pham-pos"
                                    >
                                        <a
                                            href="javascript:;"
                                            className="kt-menu__link kt-menu__toggle"
                                        >
                                            <span className="kt-menu__link-icon">
                                                <i className="la la-tablet" />
                                            </span>
                                            <span className="kt-menu__link-text">{pos_product[this.context.Language]}</span>
                                            <i className="kt-menu__ver-arrow la la-angle-right" />
                                        </a>
                                        <div className="kt-menu__submenu">
                                            <span className="kt-menu__arrow" />
                                            <ul className="kt-menu__subnav">
                                                <li
                                                    className="kt-menu__item"
                                                    aria-haspopup="true"
                                                    data-id="danh-sach-san-pham-pos"
                                                >
                                                    <Link to="/admin/productgpos" className="kt-menu__link">
                                                        <i className="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                                            <span />
                                                        </i>
                                                        <span className="kt-menu__link-text">
                                                            {product_ls[this.context.Language]}
                                                        </span>
                                                    </Link>
                                                </li>
                                                <li
                                                    className="kt-menu__item"
                                                    aria-haspopup="true"
                                                    data-id="danh-muc-san-pham-pos"
                                                >
                                                    <Link
                                                        to="/admin/productcategorygpos"
                                                        className="kt-menu__link"
                                                    >
                                                        <i className="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                                            <span />
                                                        </i>
                                                        <span className="kt-menu__link-text">
                                                            {product_cat[this.context.Language]}
                                                        </span>
                                                    </Link>
                                                </li>
                                            </ul>
                                        </div>
                                    </li>
                                )}
                            {this.context.FeatureList.find(m => m.Url === "product") !=
                                null && (
                                    <li
                                        className="kt-menu__item  kt-menu__item--submenu"
                                        aria-haspopup="true"
                                        data-ktmenu-submenu-toggle="hover"
                                        data-id="san-pham"
                                    >
                                        <a
                                            href="javascript:;"
                                            className="kt-menu__link kt-menu__toggle"
                                        >
                                            <span className="kt-menu__link-icon">
                                                <i className="la la-tablet" />
                                            </span>
                                            <span className="kt-menu__link-text">{products[this.context.Language]}</span>
                                            <i className="kt-menu__ver-arrow la la-angle-right" />
                                        </a>
                                        <div className="kt-menu__submenu">
                                            <span className="kt-menu__arrow" />
                                            <ul className="kt-menu__subnav">
                                                <li
                                                    className="kt-menu__item"
                                                    aria-haspopup="true"
                                                    data-id="danh-sach-san-pham"
                                                >
                                                    <Link to="/admin/product" className="kt-menu__link">
                                                        <i className="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                                            <span />
                                                        </i>
                                                        <span className="kt-menu__link-text">
                                                            {product_ls[this.context.Language]}
                                                        </span>
                                                    </Link>
                                                </li>
                                                <li
                                                    className="kt-menu__item"
                                                    aria-haspopup="true"
                                                    data-id="danh-muc-san-pham"
                                                >
                                                    <Link
                                                        to="/admin/productcategory"
                                                        className="kt-menu__link"
                                                    >
                                                        <i className="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                                            <span />
                                                        </i>
                                                        <span className="kt-menu__link-text">
                                                            {product_cat[this.context.Language]}
                                                        </span>
                                                    </Link>
                                                </li>
                                                <li
                                                    className="kt-menu__item"
                                                    aria-haspopup="true"
                                                    data-id="danh-muc-tuy-chon-san-pham"
                                                >
                                                    <Link
                                                        to="/admin/productcustomcategory"
                                                        className="kt-menu__link"
                                                    >
                                                        <i className="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                                            <span />
                                                        </i>
                                                        <span className="kt-menu__link-text">
                                                            {product_cus_cat[this.context.Language]}
                                                        </span>
                                                    </Link>
                                                </li>
                                                <li
                                                    className="kt-menu__item"
                                                    aria-haspopup="true"
                                                    data-id="thuong-hieu-san-pham"
                                                >
                                                    <Link to="/admin/brand" className="kt-menu__link">
                                                        <i className="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                                            <span />
                                                        </i>
                                                        <span className="kt-menu__link-text">{product_brands[this.context.Language]}</span>
                                                    </Link>
                                                </li>
                                                <li
                                                    className="kt-menu__item"
                                                    aria-haspopup="true"
                                                    data-id="nhom-thuoc-tinh"
                                                >
                                                    <Link
                                                        to="/admin/productattributegroup"
                                                        className="kt-menu__link"
                                                    >
                                                        <i className="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                                            <span />
                                                        </i>
                                                        <span className="kt-menu__link-text">
                                                            {product_att_group[this.context.Language]}
                                                        </span>
                                                    </Link>
                                                </li>

                                                <li
                                                    className="kt-menu__item"
                                                    aria-haspopup="true"
                                                    data-id="ten-thuoc-tinh"
                                                >
                                                    <Link
                                                        to="/admin/productattribute"
                                                        className="kt-menu__link"
                                                    >
                                                        <i className="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                                            <span />
                                                        </i>
                                                        <span className="kt-menu__link-text">
                                                            {product_att_name[this.context.Language]}
                                                        </span>
                                                    </Link>
                                                </li>
                                                <li
                                                    className="kt-menu__item"
                                                    aria-haspopup="true"
                                                    data-id="giatri-thuoc-tinh"
                                                >
                                                    <Link
                                                        to="/admin/productattributevalue"
                                                        className="kt-menu__link"
                                                    >
                                                        <i className="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                                            <span />
                                                        </i>
                                                        <span className="kt-menu__link-text">
                                                            {product_att_value[this.context.Language]}
                                                        </span>
                                                    </Link>
                                                </li>
                                                <li
                                                    className="kt-menu__item"
                                                    aria-haspopup="true"
                                                    data-id="danh-sach-nhan"
                                                >
                                                    <Link
                                                        to="/admin/label"
                                                        className="kt-menu__link"
                                                    >
                                                        <i className="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                                            <span />
                                                        </i>
                                                        <span className="kt-menu__link-text">
                                                            {label_ls[this.context.Language]}
                                                        </span>
                                                    </Link>
                                                </li>
                                            </ul>
                                        </div>
                                    </li>
                                )}
                            {this.context.FeatureList.find(m => m.Url === "product") !=
                                null && (
                                    <li
                                        className="kt-menu__item"
                                        aria-haspopup="true"
                                        data-id="order-history"
                                    >
                                        <Link to="/admin/orderhistory" className="kt-menu__link">
                                            <span className="kt-menu__link-icon">
                                                <i className="la la-shopping-cart" />
                                            </span>
                                            <span className="kt-menu__link-text">{order_history[this.context.Language]}</span>
                                        </Link>
                                    </li>
                                )}
                            {this.context.FeatureList.find(m => m.Url === "product") !=
                                null && (
                                    <li
                                        className="kt-menu__item  kt-menu__item--submenu"
                                        aria-haspopup="true"
                                        data-ktmenu-submenu-toggle="hover"
                                        data-id="comment-rating"
                                    >
                                        <a
                                            href="javascript:;"
                                            className="kt-menu__link kt-menu__toggle"
                                        >
                                            <span className="kt-menu__link-icon">
                                                <i className="la la-tablet" />
                                            </span>
                                            <span className="kt-menu__link-text">{comment[this.context.Language]}</span>
                                            <i className="kt-menu__ver-arrow la la-angle-right" />
                                        </a>
                                        <div className="kt-menu__submenu">
                                            <span className="kt-menu__arrow" />
                                            <ul className="kt-menu__subnav">
                                                <li
                                                    className="kt-menu__item"
                                                    aria-haspopup="true"
                                                    data-id="comment-list"
                                                >
                                                    <Link to="/admin/comment" className="kt-menu__link">
                                                        <i className="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                                            <span />
                                                        </i>
                                                        <span className="kt-menu__link-text">
                                                            {products[this.context.Language]}
                                                        </span>
                                                    </Link>
                                                </li>
                                                <li
                                                    className="kt-menu__item"
                                                    aria-haspopup="true"
                                                    data-id="comment-list"
                                                >
                                                    <Link to="/admin/blog-comment" className="kt-menu__link">
                                                        <i className="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                                            <span />
                                                        </i>
                                                        <span className="kt-menu__link-text">
                                                            {blog[this.context.Language]}
                                                        </span>
                                                    </Link>
                                                </li>
                                            </ul>
                                        </div>
                                    </li>

                                )}

                            {this.context.FeatureList.find(
                                m => m.Url === "digital-signage"
                            ) != null && (
                                    <li
                                        className="kt-menu__item  kt-menu__item--submenu"
                                        aria-haspopup="true"
                                        data-ktmenu-submenu-toggle="hover"
                                        data-id="digital-signage"
                                    >
                                        <a
                                            href="javascript:;"
                                            className="kt-menu__link kt-menu__toggle"
                                        >
                                            <span className="kt-menu__link-icon">
                                                <i className="la la-television" />
                                            </span>
                                            <span className="kt-menu__link-text">{dgt_signage[this.context.Language]}</span>
                                            <i className="kt-menu__ver-arrow la la-angle-right" />
                                        </a>
                                        <div className="kt-menu__submenu">
                                            <span className="kt-menu__arrow" />
                                            <ul className="kt-menu__subnav">
                                                <li
                                                    className="kt-menu__item"
                                                    aria-haspopup="true"
                                                    data-id="device-list"
                                                >
                                                    <Link
                                                        to="/admin/digital/devices"
                                                        className="kt-menu__link"
                                                    >
                                                        <i className="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                                            <span />
                                                        </i>
                                                        <span className="kt-menu__link-text">{device_ls[this.context.Language]}</span>
                                                    </Link>
                                                </li>
                                                <li
                                                    className="kt-menu__item"
                                                    aria-haspopup="true"
                                                    data-id="play-list"
                                                >
                                                    <Link
                                                        to="/admin/digital/playlistvideo"
                                                        className="kt-menu__link"
                                                    >
                                                        <i className="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                                            <span />
                                                        </i>
                                                        <span className="kt-menu__link-text">{play_ls[this.context.Language]}</span>
                                                    </Link>
                                                </li>
                                                <li
                                                    className="kt-menu__item"
                                                    aria-haspopup="true"
                                                    data-id="video-list"
                                                >
                                                    <Link
                                                        to="/admin/digital/video"
                                                        className="kt-menu__link"
                                                    >
                                                        <i className="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                                            <span />
                                                        </i>
                                                        <span className="kt-menu__link-text">{video_ls[this.context.Language]}</span>
                                                    </Link>
                                                </li>
                                            </ul>
                                        </div>
                                    </li>
                                )}
                            <li
                                className="kt-menu__item  kt-menu__item--submenu"
                                aria-haspopup="true"
                                data-ktmenu-submenu-toggle="hover"
                                data-id="promotion"
                            >
                                <a
                                    href="javascript:;"
                                    className="kt-menu__link kt-menu__toggle"
                                >
                                    <span className="kt-menu__link-icon">
                                        <i className="fa fa-percent" />
                                    </span>
                                    <span className="kt-menu__link-text">{promotion[this.context.Language]}</span>
                                    <i className="kt-menu__ver-arrow la la-angle-right" />
                                </a>
                                <div className="kt-menu__submenu">
                                    <span className="kt-menu__arrow" />
                                    <ul className="kt-menu__subnav">
                                        {this.context.FeatureList.find(
                                            m => m.Url === "promotion-news"
                                        ) != null && (
                                                <li
                                                    className="kt-menu__item"
                                                    aria-haspopup="true"
                                                    data-id="promotion-list"
                                                >
                                                    <Link to="/admin/promotion" className="kt-menu__link">
                                                        <i className="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                                            <span />
                                                        </i>
                                                        <span className="kt-menu__link-text">
                                                            {promotion_news[this.context.Language]}
                                                        </span>
                                                    </Link>
                                                </li>
                                            )}
                                        {this.context.FeatureList.find(
                                            m => m.Url === "promotion-code"
                                        ) != null && (
                                                <li
                                                    className="kt-menu__item"
                                                    aria-haspopup="true"
                                                    data-id="coupon-list"
                                                >
                                                    <Link to="/admin/coupon" className="kt-menu__link">
                                                        <i className="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                                            <span />
                                                        </i>
                                                        <span className="kt-menu__link-text">
                                                            {coupon_code[this.context.Language]}
                                                        </span>
                                                    </Link>
                                                </li>
                                            )}
                                        {this.context.FeatureList.find(
                                            m => m.Url === "promotion-giftcard-code"
                                        ) != null && (
                                                <li
                                                    className="kt-menu__item"
                                                    aria-haspopup="true"
                                                    data-id="giftcard-coupon-list"
                                                >
                                                    <Link
                                                        to="/admin/giftcardcoupon"
                                                        className="kt-menu__link"
                                                    >
                                                        <i className="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                                            <span />
                                                        </i>
                                                        <span className="kt-menu__link-text">
                                                            {giftcard_coupon[this.context.Language]}
                                                        </span>
                                                    </Link>
                                                </li>
                                            )}
                                    </ul>
                                </div>
                            </li>
                            {this.context.FeatureList.find(m => m.Url === "blog") != null && (
                                <li
                                    className="kt-menu__item  kt-menu__item--submenu"
                                    aria-haspopup="true"
                                    data-ktmenu-submenu-toggle="hover"
                                    data-id="tin-tuc"
                                >
                                    <a
                                        href="javascript:;"
                                        className="kt-menu__link kt-menu__toggle"
                                    >
                                        <span className="kt-menu__link-icon">
                                            <i className="fa fa-newspaper" />
                                        </span>
                                        <span className="kt-menu__link-text">{blog[this.context.Language]}</span>
                                        <i className="kt-menu__ver-arrow la la-angle-right" />
                                    </a>
                                    <div className="kt-menu__submenu">
                                        <span className="kt-menu__arrow" />
                                        <ul className="kt-menu__subnav">
                                            <li
                                                className="kt-menu__item"
                                                aria-haspopup="true"
                                                data-id="tags"
                                            >
                                                <Link to="/admin/tags" className="kt-menu__link">
                                                    <i className="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                                        <span />
                                                    </i>
                                                    <span className="kt-menu__link-text">{tags[this.context.Language]}</span>
                                                </Link>
                                            </li>
                                            <li
                                                className="kt-menu__item"
                                                aria-haspopup="true"
                                                data-id="danh-sach-tin-tuc"
                                            >
                                                <Link to="/admin/blog" className="kt-menu__link">
                                                    <i className="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                                        <span />
                                                    </i>
                                                    <span className="kt-menu__link-text">{blog_ls[this.context.Language]}</span>
                                                </Link>
                                            </li>
                                            <li
                                                className="kt-menu__item"
                                                aria-haspopup="true"
                                                data-id="danh-muc-tin-tuc"
                                            >
                                                <Link
                                                    to="/admin/blogcategory"
                                                    className="kt-menu__link"
                                                >
                                                    <i className="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                                        <span />
                                                    </i>
                                                    <span className="kt-menu__link-text">
                                                        {blog_cat[this.context.Language]}
                                                    </span>
                                                </Link>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                            )}
                            {this.context.FeatureList.find(m => m.Url === "video") !=
                                null && (
                                    <li
                                        className="kt-menu__item"
                                        aria-haspopup="true"
                                        data-id="lien-he"
                                    >
                                        <Link to="/admin/contact" className="kt-menu__link">
                                            <span className="kt-menu__link-icon">
                                                <i className="la la-youtube-play" />
                                            </span>
                                            <span className="kt-menu__link-text">{video[this.context.Language]}</span>
                                        </Link>
                                    </li>
                                )}
                            {this.context.FeatureList.find(m => m.Url === "gallery") !=
                                null && (
                                    <li
                                        className="kt-menu__item"
                                        aria-haspopup="true"
                                        data-id="gallery"
                                    >
                                        <Link to="/admin/gallery" className="kt-menu__link">
                                            <span className="kt-menu__link-icon">
                                                <i className="la la-photo" />
                                            </span>
                                            <span className="kt-menu__link-text">{gallery[this.context.Language]}</span>
                                        </Link>
                                    </li>
                                )}
                            {this.context.FeatureList.find(m => m.Url === "static-page") !=
                                null && (
                                    <li
                                        className="kt-menu__item"
                                        aria-haspopup="true"
                                        data-id="trang-tinh"
                                    >
                                        <Link to="/admin/staticpage" className="kt-menu__link">
                                            <span className="kt-menu__link-icon">
                                                <i className="fa fa-pager" />
                                            </span>
                                            <span className="kt-menu__link-text">{static_pages[this.context.Language]}</span>
                                        </Link>
                                    </li>
                                )}
                            {this.context.FeatureList.find(m => m.Url === "booking") !=
                                null && (
                                    <li
                                        className="kt-menu__item"
                                        aria-haspopup="true"
                                        data-id="staff"
                                    >
                                        <Link
                                            to="/admin/servicebookingstaff"
                                            className="kt-menu__link"
                                        >
                                            <span className="kt-menu__link-icon">
                                                <i className="la la-user" />
                                            </span>
                                            <span className="kt-menu__link-text">{staff[this.context.Language]}</span>
                                        </Link>
                                    </li>

                                )}
                            {this.context.FeatureList.find(m => m.Url === "booking-gpos") !=
                                null && (
                                    <li
                                        className="kt-menu__item"
                                        aria-haspopup="true"
                                        data-id="gpos-staff"
                                    >
                                        <Link
                                            to="/admin/servicebookingstaffgpos"
                                            className="kt-menu__link"
                                        >
                                            <span className="kt-menu__link-icon">
                                                <i className="la la-user" />
                                            </span>
                                            <span className="kt-menu__link-text">{gpos_staff[this.context.Language]}</span>
                                        </Link>
                                    </li>

                                )}
                            <li className="kt-menu__section">
                                <h4 className="kt-menu__section-text">{conf_title[this.context.Language]}</h4>
                                <i className="kt-menu__section-icon flaticon-more-v2" />
                            </li>

                            <li
                                className="kt-menu__item"
                                aria-haspopup="true"
                                data-id="cau-hinh-website"
                            >
                                <Link
                                    to="/admin/websiteconfig"
                                    href="#"
                                    className="kt-menu__link"
                                >
                                    <span className="kt-menu__link-icon">
                                        <i className="fa fa-cog" />
                                    </span>
                                    <span className="kt-menu__link-text">
                                        {website_conf[this.context.Language]}
                                    </span>
                                </Link>
                            </li>

                            {this.context.FeatureList.find(m => m.Url === "booking") !=
                                null && (
                                    <li
                                        className="kt-menu__item"
                                        aria-haspopup="true"
                                        data-id="booking-setting"
                                    >
                                        <Link
                                            to="/admin/servicebookingsetting"
                                            href="#"
                                            className="kt-menu__link"
                                        >
                                            <span className="kt-menu__link-icon">
                                                <i className="fa fa-cog" />
                                            </span>
                                            <span className="kt-menu__link-text">{booking_setting[this.context.Language]}</span>
                                        </Link>
                                    </li>
                                )}
                            {this.context.FeatureList.find(
                                m => m.Url === "order-pickup-online"
                            ) != null && (
                                    <li
                                        className="kt-menu__item"
                                        aria-haspopup="true"
                                        data-id="orderpickup-setting"
                                    >
                                        <Link
                                            to="/admin/gposshoppingcartsetting"
                                            className="kt-menu__link"
                                        >
                                            <span className="kt-menu__link-icon">
                                                <i className="fa fa-cog" />
                                            </span>
                                            <span className="kt-menu__link-text">
                                                {order_pickup_setting[this.context.Language]}
                                            </span>
                                        </Link>
                                    </li>
                                )}
                            {this.context.FeatureList.find(m => m.Url === "gift-card") !=
                                null && (
                                    <React.Fragment>
                                        <li
                                            className="kt-menu__item"
                                            aria-haspopup="true"
                                            data-id="giftcard-list"
                                        >
                                            <Link
                                                to="/admin/giftcardhistories"
                                                className="kt-menu__link"
                                            >
                                                <span className="kt-menu__link-icon">
                                                    <i className="fa fa-cog" />
                                                </span>
                                                <span className="kt-menu__link-text">
                                                    {giftcard_his[this.context.Language]}
                                                </span>
                                            </Link>
                                        </li>
                                        <li
                                            className="kt-menu__item"
                                            aria-haspopup="true"
                                            data-id="giftcardtemplate-setting"
                                        >
                                            <Link
                                                to="/admin/giftcardtemplate"
                                                className="kt-menu__link"
                                            >
                                                <span className="kt-menu__link-icon">
                                                    <i className="fa fa-cog" />
                                                </span>
                                                <span className="kt-menu__link-text">
                                                    {giftcard_temp_setting[this.context.Language]}
                                                </span>
                                            </Link>
                                        </li>
                                    </React.Fragment>
                                )}
                            <li
                                className="kt-menu__item  kt-menu__item--submenu"
                                aria-haspopup="true"
                                data-ktmenu-submenu-toggle="hover"
                                data-id="theme"
                            >
                                <a
                                    href="javascript:;"
                                    className="kt-menu__link kt-menu__toggle"
                                >
                                    <span className="kt-menu__link-icon">
                                        <i className="la la-photo" />
                                    </span>
                                    <span className="kt-menu__link-text">{theme[this.context.Language]}</span>
                                    <i className="kt-menu__ver-arrow la la-angle-right" />
                                </a>
                                <div className="kt-menu__submenu">
                                    <span className="kt-menu__arrow" />
                                    <ul className="kt-menu__subnav">
                                        <li
                                            className="kt-menu__item"
                                            aria-haspopup="true"
                                            data-id="theme-config"
                                        >
                                            <Link to="/admin/themeconfig" className="kt-menu__link">
                                                <i className="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                                    <span />
                                                </i>
                                                <span className="kt-menu__link-text">{theme_config[this.context.Language]}</span>
                                            </Link>
                                        </li>
                                        <li
                                            className="kt-menu__item"
                                            aria-haspopup="true"
                                            data-id="menu-config"
                                        >
                                            <Link to="/admin/menuconfig" className="kt-menu__link">
                                                <i className="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                                    <span />
                                                </i>
                                                <span className="kt-menu__link-text">{menu_conf[this.context.Language]}</span>
                                            </Link>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li
                                className="kt-menu__item  kt-menu__item--submenu"
                                aria-haspopup="true"
                                data-ktmenu-submenu-toggle="hover"
                                data-id="shipping-setting"
                            >
                                <a
                                    href="javascript:;"
                                    className="kt-menu__link kt-menu__toggle"
                                >
                                    <span className="kt-menu__link-icon">
                                        <i className="la la-truck" />
                                    </span>
                                    <span className="kt-menu__link-text">{ship_setting[this.context.Language]}</span>
                                    <i className="kt-menu__ver-arrow la la-angle-right" />
                                </a>
                                <div className="kt-menu__submenu">
                                    <span className="kt-menu__arrow" />
                                    <ul className="kt-menu__subnav">
                                        <li
                                            className="kt-menu__item"
                                            aria-haspopup="true"
                                            data-id="feeship-bydistrict"
                                        >
                                            <Link to="/admin/feeshipbyarea" className="kt-menu__link">
                                                <i className="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                                    <span />
                                                </i>
                                                <span className="kt-menu__link-text">{ship_by_dist[this.context.Language]}</span>
                                            </Link>
                                        </li>
                                        <li
                                            className="kt-menu__item"
                                            aria-haspopup="true"
                                            data-id="total-shipping"
                                        >
                                            <Link to="/admin/shippingbybill" className="kt-menu__link">
                                                <i className="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                                    <span />
                                                </i>
                                                <span className="kt-menu__link-text">{ship_by_total_bill[this.context.Language]}</span>
                                            </Link>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li
                                className="kt-menu__item"
                                aria-haspopup="true"
                                data-id="danh-sach-admin"
                            >
                                <Link to="/admin/admin-mannager" className="kt-menu__link">
                                    <span className="kt-menu__link-icon">
                                        <i className="fa fa-users-cog" />
                                    </span>
                                    <span className="kt-menu__link-text">{admin_manager[this.context.Language]}</span>
                                </Link>
                            </li>
                            <li
                                className="kt-menu__item"
                                aria-haspopup="true"
                                data-id="danh-sach-key"
                            >
                                <Link to="/admin/configeditor" className="kt-menu__link">
                                    <span className="kt-menu__link-icon">
                                        <i className="fa fa-key" />
                                    </span>
                                    <span className="kt-menu__link-text">{key_configuration[this.context.Language]}</span>
                                </Link>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        );
    }
}

Nav.contextType = AppContext;
export default Nav;
