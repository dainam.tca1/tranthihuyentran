﻿import React, { Component } from "react";

class Switch extends Component {

    constructor(props) {
        super(props);

        this.state = {
            isChecked: null
        }
        this.switchbutton = React.createRef();
    }

    componentWillMount() {
        this.setState({ isChecked: this.props.isChecked });
    }

  

    render() {

        return (
            <div className="switch-container">
                <label>
                    <input ref={this.switchbutton} checked={this.props.isChecked} value={this.props.value} onChange={ this.props.onChange} className="switch" type="checkbox" />
                    <div>
                        <div></div>
                    </div>
                </label>
            </div>
        );
    }
}
export default Switch;