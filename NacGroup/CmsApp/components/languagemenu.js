﻿import React, { Component } from "react";
import AppContext from "./app-context";

class languageMenu extends Component {
  render() {
    return (
      <div className="kt-header__topbar-item kt-header__topbar-item--langs">
        <div
          className="kt-header__topbar-wrapper"
          data-toggle="dropdown"
          data-offset="10px,0px"
          aria-expanded="false"
        >
          <span className="kt-header__topbar-icon">
            <img
              className=""
              src={
                "/adminstatics/global/img/" +
                (this.context.Language == 0 ? "us.svg" : "vietnam.svg")
              }
              alt=""
            />
          </span>
        </div>
        <div
          className="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround"
          x-placement="bottom-end"
        >
          <ul className="kt-nav kt-margin-t-10 kt-margin-b-10">
            <li className="kt-nav__item kt-nav__item--active">
              <a
                href="javascript:"
                onClick={() => {
                    Cookies.set("cmslang", 0, { expires: 365 });
                  window.location.reload();
                }}
                className="kt-nav__link"
              >
                <span className="kt-nav__link-icon">
                  <img src="/adminstatics/global/img/us.svg" alt="" />
                </span>
                <span className="kt-nav__link-text">English</span>
              </a>
            </li>
            <li className="kt-nav__item">
              <a
                href="javascript:"
                onClick={() => {
                    Cookies.set("cmslang", 1, { expires: 365 });
                  window.location.reload();
                }}
                className="kt-nav__link"
              >
                <span className="kt-nav__link-icon">
                  <img src="/adminstatics/global/img/vietnam.svg" alt="" />
                </span>
                <span className="kt-nav__link-text">Vietnamese</span>
              </a>
            </li>
          </ul>{" "}
        </div>
      </div>
    );
  }
}

languageMenu.contextType = AppContext;
export default languageMenu;
