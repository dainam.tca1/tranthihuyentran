import React, { Component } from "react";
import { BrowserRouter, Redirect, Route } from "react-router-dom";

import Nav from "./nav";

import LanguageMenu from "./languagemenu";
import UserWindow from "./userwindow";
import Home from "./home";
import Empty from "./empty";

import StaticPageList from "../modules/StaticPage/static-page-list";
import StaticPageAddUpdate from "../modules/StaticPage/static-page-addupdate";

import ThemeConfigList from "../modules/Core/theme-config-list";
import ThemeConfigAddUpdate from "../modules/Core/themeconfig-addupdate";

import BlogCategoryAddUpdate from "../modules/Blog/blogcategory-addupdate";
import BlogCategoryList from "../modules/Blog/blogcategory-list";
import BlogAddUpdate from "../modules/Blog/blog-addupdate";
import BlogList from "../modules/Blog/blog-list";
import PromotionList from "../modules/Blog/promotion-list";

import ContactList from "../modules/Contact/contact-list";

import GalleryList from "../modules/Gallery/gallery-list";
import GalleryAddUpdate from "../modules/Gallery/gallery-addupdate";

import ManagerVideoList from "../modules/DigitalSignage/video-list";
import ManagerVideoAddUpdate from "../modules/DigitalSignage/video-addupdate";
import PlayListVideoList from "../modules/DigitalSignage/playlistvideo-list";
import PlayListVideoAddUpdate from "../modules/DigitalSignage/playlistvideo-addupdate";
import DevicesList from "../modules/DigitalSignage/devices-list";
import DevicesAddUpdate from "../modules/DigitalSignage/devices-addupdate";

import configEditorList from "../modules/Core/configeditor-list";
import configEditorAddUpdate from "../modules/Core/configeditor-addupdate";

import nailServiceCategoryList from "../modules/NailService/nailservicecategory-list";
import nailServiceCategoryAddUpdate from "../modules/NailService/nailservicecategory-addupdate";
import nailServiceAddUpdate from "../modules/NailService/nailservice-addupdate";
import nailServiceList from "../modules/NailService/nailservice-list";

import staffAddUpdate from "../modules/ServiceBooking/staff-addupdate";
import staffList from "../modules/ServiceBooking/staff-list";
import gposStaffList from "../modules/ServiceBooking/gposstaff-list";
import bookingSetting from "../modules/ServiceBooking/booking-setting";
import BookingList from "../modules/ServiceBooking/booking-list";
import BookingView from "../modules/ServiceBooking/booking-view";

import menuConfig from "../modules/Core/menu-config";

import webConfig from "../modules/Core/web-config";

import productPosList from "../modules/Product/productpos-list";
import productCategoryPosList from "../modules/Product/productcategorypos-list";
import productAddUpdate from "../modules/Product/product-addupdate";
import productList from "../modules/Product/product-list";
import productCategoryAddUpdate from "../modules/Product/productcategory-addupdate";
import productCustomCategoryList from "../modules/Product/productcustomcategory-list";
import productCustomCategoryAddUpdate from "../modules/Product/productcustomcategory-addupdate";
import productCategoryList from "../modules/Product/productcategory-list";
import brandAddUpdate from "../modules/Product/brand-addupdate";
import brandList from "../modules/Product/brand-list";
import productAttributeGroupList from "../modules/Product/productattributegroup-list";
import productAttributeList from "../modules/Product/productattribute-list";
import productAttributeValueList from "../modules/Product/productattributevalue-list";
import labelList from "../modules/Product/label-list";

import gposShoppingCartSetting from "../modules/Order/gposshoppingcart-setting";
import couponAddUpdate from "../modules/Order/coupon-addupdate";
import couponList from "../modules/Order/coupon-list";

import GiftCardCouponAddUpdate from "../modules/GiftCard/giftcardcoupon-addupdate";
import GiftCardCouponList from "../modules/GiftCard/giftcardcoupon-list";

import giftCardTemplateAddUpdate from "../modules/GiftCard/giftcardtemplate-addupdate";
import giftCardTemplateList from "../modules/GiftCard/giftcardtemplate-list";

import GiftCardHistoriesAddUpdate from "../modules/GiftCard/giftcardhistories-addupdate";
import GiftCardHistoriesList from "../modules/GiftCard/giftcardhistories-list";

import OrderAddUpdate from "../modules/Order/order-addupdate";
import OrderList from "../modules/Order/order-list";

import ShippingByBill from "../modules/Order/shipping-totalbill";
import shippingByBillAddUpdate from "../modules/Order/shipping-totalbill-addupdate";

import ShippingAreaList from "../modules/Order/shipping-area";
import ShippingAreaAddUpdate from "../modules/Order/shipping-area-addupdate";

import Comment from "../modules/Product/comment-list";
import BlogComment from "../modules/Blog/comment-list";
import Tag from "../modules/Blog/tags";

import Video from "../modules/Product/video-list";

import AdminManager from "../modules/Core/admin-manager";

import mxMerchantSetting from "../modules/Payment/mxmerchantsetting";

import AppProvider from "./app-provider";

class App extends Component {
    constructor() {
        super();

        this.state = {
            ex: {
                Title: "Statistics overview"
            }
        };
    }
    componentDidMount() {
        document.title = "Dashboard";
    }
    render() {
        return (
            <AppProvider>
                <BrowserRouter>
                    <React.Fragment>
                        {this.state && this.state.ex ? (
                            <React.Fragment>
                                <div
                                    id="kt_header_mobile"
                                    className="kt-header-mobile  kt-header-mobile--fixed "
                                >
                                    <div className="kt-header-mobile__logo">
                                        <a href="/">
                                            <img
                                                src="/adminstatics/global/img/logowebnail.png"
                                                height="40"
                                            />
                                        </a>
                                    </div>
                                    <div className="kt-header-mobile__toolbar">
                                        <button
                                            className="kt-header-mobile__toggler kt-header-mobile__toggler--left"
                                            id="kt_aside_mobile_toggler"
                                        >
                                            <span />
                                        </button>
                                        <button
                                            className="kt-header-mobile__toggler"
                                            id="kt_header_mobile_toggler"
                                        >
                                            <span />
                                        </button>
                                        <button
                                            className="kt-header-mobile__topbar-toggler"
                                            id="kt_header_mobile_topbar_toggler"
                                        >
                                            <i className="flaticon-more" />
                                        </button>
                                    </div>
                                </div>

                                <div className="kt-grid kt-grid--hor kt-grid--root">
                                    <div className="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
                                        <button className="kt-aside-close " id="kt_aside_close_btn">
                                            <i className="la la-close" />
                                        </button>
                                        <Nav />
                                        <div
                                            className="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper"
                                            id="kt_wrapper"
                                        >
                                            <div
                                                id="kt_header"
                                                className="kt-header kt-grid__item  kt-header--fixed "
                                            >
                                                <button
                                                    className="kt-header-menu-wrapper-close"
                                                    id="kt_header_menu_mobile_close_btn"
                                                >
                                                    <i className="la la-close" />
                                                </button>
                                                <div
                                                    className="kt-header-menu-wrapper"
                                                    id="kt_header_menu_wrapper"
                                                >
                                                    <div
                                                        id="kt_header_menu"
                                                        className="kt-header-menu kt-header-menu-mobile  kt-header-menu--layout-default "
                                                    >
                                                        <ul className="kt-menu__nav " />
                                                    </div>
                                                </div>

                                                <div className="kt-header__topbar">
                                                    <LanguageMenu />
                                                    <UserWindow />
                                                </div>
                                            </div>
                                            <div className="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
                                                <Route
                                                    exact
                                                    path="/admin/emptypage"
                                                    component={Empty}
                                                />
                                                <Route exact path="/admin" component={Home} />
                                                <Route exact path="/admin/dashboard" component={Home} />

                                                <Route
                                                    exact
                                                    path="/admin/themeconfig"
                                                    component={ThemeConfigList}
                                                />
                                                <Route
                                                    exact
                                                    path="/admin/themeconfig/add"
                                                    component={ThemeConfigAddUpdate}
                                                />
                                                <Route
                                                    exact
                                                    path="/admin/themeconfig/update/:id"
                                                    component={ThemeConfigAddUpdate}
                                                />

                                                <Route
                                                    exact
                                                    path="/admin/tags"
                                                    component={Tag}
                                                />
                                                <Route
                                                    exact
                                                    path="/admin/blogcategory"
                                                    component={BlogCategoryList}
                                                />
                                                <Route
                                                    exact
                                                    path="/admin/blogcategory/add"
                                                    component={BlogCategoryAddUpdate}
                                                />
                                                <Route
                                                    exact
                                                    path="/admin/blogcategory/update/:id"
                                                    component={BlogCategoryAddUpdate}
                                                />
                                                <Route exact path="/admin/blog" component={BlogList} />
                                                <Route
                                                    exact
                                                    path="/admin/promotion"
                                                    component={PromotionList}
                                                />
                                                <Route
                                                    exact
                                                    path="/admin/blog/add"
                                                    component={BlogAddUpdate}
                                                />
                                                <Route
                                                    exact
                                                    path="/admin/blog/update/:id"
                                                    component={BlogAddUpdate}
                                                />

                                                <Route
                                                    exact
                                                    path="/admin/staticpage"
                                                    component={StaticPageList}
                                                />
                                                <Route
                                                    exact
                                                    path="/admin/staticpage/add"
                                                    component={StaticPageAddUpdate}
                                                />
                                                <Route
                                                    exact
                                                    path="/admin/staticpage/update/:id"
                                                    component={StaticPageAddUpdate}
                                                />
                                                <Route
                                                    exact
                                                    path="/admin/digital/playlistvideo"
                                                    component={PlayListVideoList}
                                                />
                                                <Route
                                                    exact
                                                    path="/admin/digital/playlistvideo/add"
                                                    component={PlayListVideoAddUpdate}
                                                />
                                                <Route
                                                    exact
                                                    path="/admin/digital/playlistvideo/update/:id"
                                                    component={PlayListVideoAddUpdate}
                                                />
                                                <Route
                                                    exact
                                                    path="/admin/digital/devices"
                                                    component={DevicesList}
                                                />
                                                <Route
                                                    exact
                                                    path="/admin/digital/devices/add"
                                                    component={DevicesAddUpdate}
                                                />
                                                <Route
                                                    exact
                                                    path="/admin/digital/devices/update/:id"
                                                    component={DevicesAddUpdate}
                                                />
                                                <Route
                                                    exact
                                                    path="/admin/digital/video"
                                                    component={ManagerVideoList}
                                                />
                                                <Route
                                                    exact
                                                    path="/admin/digital/video/add"
                                                    component={ManagerVideoAddUpdate}
                                                />
                                                <Route
                                                    exact
                                                    path="/admin/digital/video/update/:id"
                                                    component={ManagerVideoAddUpdate}
                                                />
                                                <Route
                                                    exact
                                                    path="/admin/gallery"
                                                    component={GalleryList}
                                                />
                                                <Route
                                                    exact
                                                    path="/admin/gallery/add"
                                                    component={GalleryAddUpdate}
                                                />
                                                <Route
                                                    exact
                                                    path="/admin/gallery/update/:id"
                                                    component={GalleryAddUpdate}
                                                />
                                                <Route
                                                    exact
                                                    path="/admin/websiteconfig"
                                                    component={webConfig}
                                                />
                                                <Route
                                                    exact
                                                    path="/admin/contact"
                                                    component={ContactList}
                                                />

                                                <Route
                                                    exact
                                                    path="/admin/configeditor"
                                                    component={configEditorList}
                                                />
                                                <Route
                                                    exact
                                                    path="/admin/configeditor/add"
                                                    component={configEditorAddUpdate}
                                                />
                                                <Route
                                                    exact
                                                    path="/admin/configeditor/update/:id"
                                                    component={configEditorAddUpdate}
                                                />
                                                <Route
                                                    exact
                                                    path="/admin/nailservicecategory"
                                                    component={nailServiceCategoryList}
                                                />
                                                <Route
                                                    exact
                                                    path="/admin/nailservicecategory/add"
                                                    component={nailServiceCategoryAddUpdate}
                                                />
                                                <Route
                                                    exact
                                                    path="/admin/nailservicecategory/update/:id"
                                                    component={nailServiceCategoryAddUpdate}
                                                />

                                                <Route
                                                    exact
                                                    path="/admin/nailservice"
                                                    component={nailServiceList}
                                                />

                                                <Route
                                                    exact
                                                    path="/admin/nailservice/add"
                                                    component={nailServiceAddUpdate}
                                                />
                                                <Route
                                                    exact
                                                    path="/admin/nailservice/update/:id"
                                                    component={nailServiceAddUpdate}
                                                />
                                                <Route
                                                    exact
                                                    path="/admin/servicebookingstaffgpos"
                                                    component={gposStaffList}
                                                />
                                                <Route
                                                    exact
                                                    path="/admin/servicebookingstaff"
                                                    component={staffList}
                                                />
                                                <Route
                                                    exact
                                                    path="/admin/servicebookingstaff/add"
                                                    component={staffAddUpdate}
                                                />
                                                <Route
                                                    exact
                                                    path="/admin/servicebookingstaff/update/:id"
                                                    component={staffAddUpdate}
                                                />
                                                <Route
                                                    exact
                                                    path="/admin/servicebookingsetting"
                                                    component={bookingSetting}
                                                />
                                                <Route
                                                    exact
                                                    path="/admin/booking"
                                                    component={BookingList}
                                                />
                                                <Route
                                                    exact
                                                    path="/admin/booking/view/:id"
                                                    component={BookingView}
                                                />
                                                <Route
                                                    exact
                                                    path="/admin/menuconfig"
                                                    component={menuConfig}
                                                />
                                                <Route
                                                    exact
                                                    path="/admin/productcategorygpos"
                                                    component={productCategoryPosList}
                                                />
                                                <Route
                                                    exact
                                                    path="/admin/productgpos"
                                                    component={productPosList}
                                                />

                                                <Route
                                                    exact
                                                    path="/admin/product/add"
                                                    component={productAddUpdate}
                                                />
                                                <Route
                                                    exact
                                                    path="/admin/product/update/:id"
                                                    component={productAddUpdate}
                                                />
                                                <Route
                                                    exact
                                                    path="/admin/product"
                                                    component={productList}
                                                />
                                                <Route
                                                    exact
                                                    path="/admin/productcategory/update/:id"
                                                    component={productCategoryAddUpdate}
                                                />
                                                <Route
                                                    exact
                                                    path="/admin/productcategory/add"
                                                    component={productCategoryAddUpdate}
                                                />
                                                <Route
                                                    exact
                                                    path="/admin/productcategory"
                                                    component={productCategoryList}
                                                />
                                                <Route
                                                    exact
                                                    path="/admin/productcustomcategory/update/:id"
                                                    component={productCustomCategoryAddUpdate}
                                                />
                                                <Route
                                                    exact
                                                    path="/admin/productcustomcategory/add"
                                                    component={productCustomCategoryAddUpdate}
                                                />
                                                <Route
                                                    exact
                                                    path="/admin/productcustomcategory"
                                                    component={productCustomCategoryList}
                                                />
                                                <Route
                                                    exact
                                                    path="/admin/productattributegroup"
                                                    component={productAttributeGroupList}
                                                />
                                                <Route
                                                    exact
                                                    path="/admin/productattribute"
                                                    component={productAttributeList}
                                                />
                                                <Route
                                                    exact
                                                    path="/admin/productattributevalue"
                                                    component={productAttributeValueList}
                                                />
                                                <Route
                                                    exact
                                                    path="/admin/label"
                                                    component={labelList}
                                                />
                                                <Route
                                                    exact
                                                    path="/admin/brand/update/:id"
                                                    component={brandAddUpdate}
                                                />
                                                <Route
                                                    exact
                                                    path="/admin/brand/add"
                                                    component={brandAddUpdate}
                                                />
                                                <Route
                                                    exact
                                                    path="/admin/brand"
                                                    component={brandList}
                                                />
                                                
                                                <Route
                                                    exact
                                                    path="/admin/gposshoppingcartsetting"
                                                    component={gposShoppingCartSetting}
                                                />
                                                <Route
                                                    exact
                                                    path="/admin/coupon/add"
                                                    component={couponAddUpdate}
                                                />
                                                <Route
                                                    exact
                                                    path="/admin/coupon/update/:id"
                                                    component={couponAddUpdate}
                                                />
                                                <Route
                                                    exact
                                                    path="/admin/coupon"
                                                    component={couponList}
                                                />

                                                <Route
                                                    exact
                                                    path="/admin/giftcardcoupon/add"
                                                    component={GiftCardCouponAddUpdate}
                                                />
                                                <Route
                                                    exact
                                                    path="/admin/giftcardcoupon/update/:id"
                                                    component={GiftCardCouponAddUpdate}
                                                />
                                                <Route
                                                    exact
                                                    path="/admin/giftcardcoupon"
                                                    component={GiftCardCouponList}
                                                />

                                                <Route
                                                    exact
                                                    path="/admin/giftcardtemplate/add"
                                                    component={giftCardTemplateAddUpdate}
                                                />
                                                <Route
                                                    exact
                                                    path="/admin/giftcardtemplate/update/:id"
                                                    component={giftCardTemplateAddUpdate}
                                                />
                                                <Route
                                                    exact
                                                    path="/admin/giftcardtemplate"
                                                    component={giftCardTemplateList}
                                                />
                                                <Route
                                                    exact
                                                    path="/admin/giftcardhistories/view/:id"
                                                    component={GiftCardHistoriesAddUpdate}
                                                />
                                                <Route
                                                    exact
                                                    path="/admin/giftcardhistories"
                                                    component={GiftCardHistoriesList}
                                                />
                                                <Route
                                                    exact
                                                    path="/admin/orderhistory/update/:id"
                                                    component={OrderAddUpdate}
                                                />
                                                <Route
                                                    exact
                                                    path="/admin/orderhistory"
                                                    component={OrderList}
                                                />
                                                <Route
                                                    exact
                                                    path="/admin/shippingbybill"
                                                    component={ShippingByBill}
                                                />
                                                <Route
                                                    exact
                                                    path="/admin/shippingbybill/add"
                                                    component={shippingByBillAddUpdate}
                                                />
                                                <Route
                                                    exact
                                                    path="/admin/shippingbybill/update/:id"
                                                    component={shippingByBillAddUpdate}
                                                />
                                                <Route
                                                    exact
                                                    path="/admin/feeshipbyarea"
                                                    component={ShippingAreaList}
                                                />
                                                <Route
                                                    exact
                                                    path="/admin/feeshipbyarea/add"
                                                    component={ShippingAreaAddUpdate}
                                                />
                                                <Route
                                                    exact
                                                    path="/admin/feeshipbyarea/update/:id"
                                                    component={ShippingAreaAddUpdate}
                                                />
                                                <Route
                                                    exact
                                                    path="/admin/admin-mannager"
                                                    component={AdminManager}
                                                />
                                                <Route
                                                    exact
                                                    path="/admin/mxmerchantpayment"
                                                    component={mxMerchantSetting}
                                                />
                                                <Route
                                                    exact
                                                    path="/admin/comment"
                                                    component={Comment}
                                                />
                                                <Route
                                                    exact
                                                    path="/admin/blog-comment"
                                                    component={BlogComment}
                                                />
                                            </div>
                                            <div
                                                className="kt-footer kt-grid__item kt-grid kt-grid--desktop kt-grid--ver-desktop"
                                                id="kt_footer"
                                            >
                                                <div className="kt-footer__copyright">
                                                    2019&nbsp;&copy;&nbsp;
                                                  <a href="#" target="_blank" className="kt-link">
                                                                                NacGroup
                                                  </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </React.Fragment>
                        ) : (
                                <div />
                            )}
                    </React.Fragment>
                </BrowserRouter>
            </AppProvider>
        );
    }
}

export default App;
