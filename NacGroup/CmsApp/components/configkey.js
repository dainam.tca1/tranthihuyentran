import React, { Component } from "react";
import { DelayInput } from "react-delay-input";
import PagedList from "../../../../core-module/cms/reactapp/components/pagedlist.jsx";
import {
  GLOBAL_ERROR_MESSAGE,
  ERROR,
  SUCCESS,
  DATA_NOT_RECOVER
} from "../../../../../libs/constants/message";
import { Link } from "react-router-dom";
import FilterListPlugin from "../../../../core-module/cms/reactapp/plugins/filter-list-plugin";
import RemoveListPlugin from "../../../../core-module/cms/reactapp/plugins/remove-list-plugin";
var apiurl = "/api/configkey";
class ConfigKey extends Component {
  constructor(props) {
    super(props);
    var that = this;
    that.state = {
      ex: {
        Title: "Key Configuration",
        Param: {
          key: null,
          pagesize: null,
          categoryId: null
        }
      }
    };
    that.setState(that.state);
    that.handleChangeFilter = that.handleChangeFilter.bind(that);
    that.toPage(1);
  }

  toPage(index) {
    FilterListPlugin.filterdata(index, this, apiurl);
  }
  handleChangeFilter(event) {
    FilterListPlugin.handleChangeFilter(event, this);
    this.toPage(1);
  }
  handleDeleteDataRow() {
    RemoveListPlugin.removeList(`${apiurl}/removelist`, this);
  }
  componentDidMount() {
    document.title = "Key Configuration";
    $(".page-sidebar-menu .nav-item").removeClass("active");
    $(".page-sidebar-menu .nav-item[data-id='danh-sach-key']").addClass("active");
  }
  render() {
    return (
      <React.Fragment>
        {this.state && this.state.model ? (
          <React.Fragment>
            <div className="page-bar">
              <ul className="page-breadcrumb">
                <li>
                  <a href="javascript:">Website Configuration</a>
                  <i className="fa fa-circle" />
                </li>
                <li>
                  <span>{this.state.ex.Title}</span>
                </li>
              </ul>
              <div className="page-toolbar">
                <div className="btn-group pull-right">
                  <Link
                    to="/admin/configkey/add"
                    className="btn green btn-sm btn-outline"
                  >
                    Add <i className="icon-plus" />
                  </Link>
                  <a
                    href="javascript:"
                    className="btn green btn-sm btn-outline"
                    onClick={e => this.handleDeleteDataRow()}
                  >
                    Trash <i className="icon-trash" />
                  </a>
                </div>
              </div>
            </div>
            <h3 className="page-title">{this.state.ex.Title}</h3>
            <div className="row">
                <div className="col-md-12">
                  <div className="filter-bar form-inline">
                    <div className="form-group navbar-left">
                      <div className="input-group">
                        <DelayInput
                          delayTimeout={1000}
                          className="form-control"
                          placeholder="Key"
                          value={this.state.ex.Param.key}
                          name="key"
                          onChange={this.handleChangeFilter}
                        />
                      </div>
                      <div className="input-group">
                        <select
                          className="form-control"
                          value={this.state.ex.Param.pagesize}
                          name="pagesize"
                          onChange={this.handleChangeFilter}
                        >
                          <option value="10">10 record/page</option>
                          <option value="20">20 record/page</option>
                          <option value="50">50 record/page</option>
                          <option value="100">100 record/page</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div className="Ajax-Table">
                    <table className="table table-hover table-striped">
                      <thead>
                        <tr>
                        <th className="stt">
                          <label
                            className="mt-checkbox mt-checkbox-outline"
                            onChange={ev => {
                              this.state.model.Results = this.state.model.Results.map(
                                e => {
                                  return { ...e, IsChecked: ev.target.checked };
                                }
                              );
                              this.setState(this.state);
                            }}
                          >
                            <input type="checkbox" />
                            <span />
                          </label>
                        </th>
                        <th>Key</th>
                        <th>Name</th>
                        </tr>
                      </thead>
                      <tbody>
                        {this.state.model.Results.map((c, index) => {
                          return (
                            <tr>
                              <td>
                                <label className="mt-checkbox mt-checkbox-outline">
                                  <input
                                    type="checkbox"
                                    value={c._id}
                                    checked={c.IsChecked}
                                    onChange={e => {
                                      c.IsChecked = !c.IsChecked;
                                      this.setState(this.state);
                                    }}
                                  />
                                  <span />
                                </label>
                              </td>
                              <td>
                                <Link to={`/admin/configkey/update/${c._id}`}>
                                  {c.Key}
                                </Link>
                              </td>
                              <td>{c.Value.Name}</td>
                            </tr>
                          );
                        })}
                      </tbody>
                    </table>
                  </div>
                </div>
            </div>
            <div className="row">
              <div className="col-md-12 text-center">
                  <div id="nav_grid">
                    <PagedList
                      currentpage={this.state.model.CurrentPage}
                      pagesize={this.state.model.PageSize}
                      totalitemcount={this.state.model.TotalItemCount}
                      totalpagecount={this.state.model.TotalPageCount}
                      ajaxcallback={this.toPage.bind(this)}
                    />
                  </div>
                  <span>
                          Total {this.state.model.TotalItemCount} Key
                  </span>
              </div>
              <div className="clearfix" />
            </div>
          </React.Fragment>
        ) : (
          <div />
        )}
      </React.Fragment>
    );
  }
}

export default ConfigKey;
