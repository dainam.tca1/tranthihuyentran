import React, { Component } from "react";
import { Link } from "react-router-dom";
import {
  globalErrorMessage,
  error,
  success,
  save
} from "../../constants/message";

import AppContext from "../../components/app-context";
const apiurl = "/cms/api/mxmerchantpayment";

class mxMerchantSetting extends Component {
  constructor(props, context) {
    super(props, context);
    var that = this;
    this.state = {
      model: null,
      ex: {
        Title: "MxMerchant Payment Setting",
        PaymentLinkTemp: {
          name: null,
          description: null,
          onSuccessUrl: null,
          onFailureUrl: null
        }
      }
    };
    KTApp.blockPage();

    $.ajax({
      url: apiurl,
      type: "GET",
      dataType: "json",
      contentType: "application/json",
      success: response => {
        KTApp.unblockPage();
        toastr.clear();
        if (response.status === "success") {
          that.state.model = response.data;
          that.setState(that.state);
        } else {
          toastr["error"](response.message, error[this.context.Language]);
        }
      },
      error: function(er) {
        KTApp.unblockPage();
        toastr.clear();
        toastr["error"](
          globalErrorMessage[this.context.Language],
          error[this.context.Language]
        );
      }
    });
  }
  componentWillMount() {}
  componentDidMount() {
    document.title = this.state.ex.Title;
    $("#kt_aside_menu .kt-menu__item").removeClass("kt-menu__item--active");
    $("#kt_aside_menu .kt-menu__item[data-id='cau-hinh-website']").addClass(
      "kt-menu__item--active"
    );
  }
  submitForm() {
    var that = this;
    KTApp.blockPage();

    $.ajax({
      url: apiurl + "/update",
      type: "POST",
      dataType: "json",
      contentType: "application/json",
      data: JSON.stringify(that.state.model),
      success: response => {
        KTApp.unblockPage();
        if (response.status == "success") {
          swal.fire({
            title: success[this.context.Language],
            text: response.message,
            type: "success",
            onClose: () => {
              that.props.history.push("/admin/emptypage");
              that.props.history.replace({
                pathname: that.props.location.pathname,
                search: that.props.location.search
              });
            }
          });
        } else {
          console.log(response.data);
          toastr["error"](response.message, error[this.context.Language]);
        }
      },
      error: function(er) {
        KTApp.unblockPage();
        toastr["error"](
          globalErrorMessage[this.context.Language],
          error[this.context.Language]
        );
      }
    });
  }

  addPaymentLink() {
    var that = this;
    KTApp.blockPage();

    $.ajax({
      url: apiurl + "/AddPaymentLink",
      type: "POST",
      dataType: "json",
      contentType: "application/json",
      data: JSON.stringify(that.state.ex.PaymentLinkTemp),
      success: response => {
        KTApp.unblockPage();
        if (response.status == "success") {
          $("#add-paymentlink-popup").modal("hide");
          swal.fire({
            title: success[this.context.Language],
            text: response.message,
            type: "success",
            onClose: () => {
              that.props.history.push("/admin/emptypage");
              that.props.history.replace({
                pathname: that.props.location.pathname,
                search: that.props.location.search
              });
            }
          });
        } else {
          console.log(response.data);
          toastr["error"](response.message, error[this.context.Language]);
        }
      },
      error: function(er) {
        KTApp.unblockPage();
        toastr["error"](
          globalErrorMessage[this.context.Language],
          error[this.context.Language]
        );
      }
    });
  }

  render() {
    return (
      <React.Fragment>
        {this.state && this.state.model ? (
          <React.Fragment>
            <div className="kt-subheader kt-grid__item" id="kt_subheader">
              <div className="kt-subheader__main">
                <div className="kt-subheader__breadcrumbs">
                  <Link
                    to="/admin/dashboard"
                    className="kt-subheader__breadcrumbs-home"
                  >
                    <i className="fa fa-home" />
                  </Link>

                  <span className="kt-subheader__breadcrumbs-separator" />
                  <span className="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
                    {this.state.ex.Title}
                  </span>
                </div>
              </div>
              <div className="kt-subheader__toolbar">
                <div className="kt-subheader__wrapper">
                  <a
                    href="javascript:;"
                    className="btn btn-primary"
                    onClick={() => {
                      this.submitForm();
                    }}
                  >
                    <i className="fa fa-save" /> {save[this.context.Language]}
                  </a>
                  <a
                    href="javascript:;"
                    className="btn btn-primary"
                    onClick={() => {
                      $("#add-paymentlink-popup").modal("show");
                    }}
                  >
                    <i className="fa fa-save" /> Add Payment Link
                  </a>
                </div>
              </div>
            </div>
            <div
              className="kt-content kt-grid__item kt-grid__item--fluid"
              id="kt_content"
            >
              <div className="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                <div className="row">
                  <div className="col-lg-4">
                    <div className="kt-portlet kt-portlet--mobile">
                      <div className="kt-portlet__head kt-portlet__head--lg">
                        <div className="kt-portlet__head-label">
                          <h3 className="kt-portlet__head-title">
                            Payment API
                          </h3>
                        </div>
                      </div>

                      <div className="kt-portlet__body">
                        <div className="form-group">
                          <label className="control-label">User Name</label>{" "}
                          <span class="required" />
                          <div className="input-icon right">
                            <i className="fa" />
                            <input
                              placeholder="User Name"
                              className="form-control"
                              value={this.state.model.UserName}
                              onChange={e => {
                                this.state.model.UserName = e.target.value;
                                this.setState(this.state);
                              }}
                            />
                          </div>
                        </div>
                        <div className="form-group">
                          <label className="control-label">Password</label>{" "}
                          <span class="required" />
                          <div className="input-icon right">
                            <i className="fa" />
                            <input
                              placeholder="Password"
                              className="form-control"
                              value={this.state.model.Password}
                              onChange={e => {
                                this.state.model.Password = e.target.value;
                                this.setState(this.state);
                              }}
                            />
                          </div>
                        </div>
                        <div className="form-group">
                          <label className="control-label">Merchant Id</label>{" "}
                          <span class="required" />
                          <div className="input-icon right">
                            <i className="fa" />
                            <input
                              placeholder="Merchant Id"
                              className="form-control"
                              value={this.state.model.MerchantId}
                              onChange={e => {
                                this.state.model.MerchantId = e.target.value;
                                this.setState(this.state);
                              }}
                            />
                          </div>
                        </div>
                        <div className="form-group">
                          <label className="control-label">Api Url</label>{" "}
                          <span class="required" />
                          <div className="input-icon right">
                            <i className="fa" />
                            <input
                              placeholder="Api Url"
                              className="form-control"
                              value={this.state.model.ApiUrl}
                              onChange={e => {
                                this.state.model.ApiUrl = e.target.value;
                                this.setState(this.state);
                              }}
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-8">
                    <div className="kt-portlet kt-portlet--mobile">
                      <div className="kt-portlet__head kt-portlet__head--lg">
                        <div className="kt-portlet__head-label">
                          <h3 className="kt-portlet__head-title">
                            Payment Links
                          </h3>
                        </div>
                      </div>

                      <div className="kt-portlet__body">
                        <div className="dataTables_wrapper dt-bootstrap4">
                          <table className="table table-striped- table-bordered table-hover table-checkable dataTable no-footer dtr-inline table-responsive">
                            <thead>
                              <tr>
                                <th>Name</th>
                                <th>Description</th>
                                <th>Url</th>
                                <th>Success Callback</th>
                                <th>Fail Callback</th>
                              </tr>
                            </thead>
                            <tbody>
                              {this.state.model.PaymentLinkList.map(pml => {
                                return (
                                  <tr>
                                    <td>{pml.name}</td>
                                    <td>{pml.description}</td>
                                    <td>{pml.Url}</td>
                                    <td>{pml.onSuccessUrl}</td>
                                    <td>{pml.onFailureUrl}</td>
                                  </tr>
                                );
                              })}
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div
              class="modal fade"
              id="add-paymentlink-popup"
              tabindex="-1"
              role="dialog"
              aria-labelledby="exampleModalLabel"
              aria-hidden="true"
            >
              <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                      Add new payment link
                    </h5>
                    <button
                      type="button"
                      class="close"
                      data-dismiss="modal"
                      aria-label="Close"
                    />
                  </div>
                  <div class="modal-body">
                    <div className="form-group">
                      <label className="control-label">Name</label>
                      <input
                        className="form-control"
                        placeholder="Name"
                        value={this.state.ex.PaymentLinkTemp.name}
                        onChange={e => {
                          this.state.ex.PaymentLinkTemp.name = e.target.value;
                          this.setState(this.state);
                        }}
                      />
                    </div>
                    <div className="form-group">
                      <label className="control-label">Description</label>
                      <input
                        className="form-control"
                        placeholder="Description"
                        value={this.state.ex.PaymentLinkTemp.description}
                        onChange={e => {
                          this.state.ex.PaymentLinkTemp.description =
                            e.target.value;
                          this.setState(this.state);
                        }}
                      />
                    </div>
                    <div className="form-group">
                      <label className="control-label">
                        Successfull callback
                      </label>
                      <input
                        className="form-control"
                        placeholder="Successfull callback"
                        value={this.state.ex.PaymentLinkTemp.onSuccessUrl}
                        onChange={e => {
                          this.state.ex.PaymentLinkTemp.onSuccessUrl =
                            e.target.value;
                          this.setState(this.state);
                        }}
                      />
                    </div>
                    <div className="form-group">
                      <label className="control-label">Fail callback</label>
                      <input
                        className="form-control"
                        placeholder="Fail callback"
                        value={this.state.ex.PaymentLinkTemp.onFailureUrl}
                        onChange={e => {
                          this.state.ex.PaymentLinkTemp.onFailureUrl =
                            e.target.value;
                          this.setState(this.state);
                        }}
                      />
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button
                      type="button"
                      className="btn btn-default"
                      data-dismiss="modal"
                    >
                      {close[this.context.Language]}
                    </button>
                    <button
                      type="button"
                      className="btn btn-primary"
                      onClick={() => {
                        this.addPaymentLink();
                      }}
                    >
                      {save[this.context.Language]}
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </React.Fragment>
        ) : (
          <div />
        )}
      </React.Fragment>
    );
  }
}
mxMerchantSetting.contextType = AppContext;
export default mxMerchantSetting;
