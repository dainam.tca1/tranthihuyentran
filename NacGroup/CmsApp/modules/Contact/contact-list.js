import React, { Component } from "react";;
import { Link } from "react-router-dom";
import { DelayInput } from "react-delay-input";
import PagedList from "../../components/pagedlist";
import moment from "moment";
import FilterListPlugin from "../../plugins/filter-list-plugin";
import RemoveListPlugin from "../../plugins/remove-list-plugin";
import DateTimePicker from "../../components/datetimepicker";
const apiurl = "/cms/api/contact";
import {
    globalErrorMessage,
    error,
    success,
    add,
    remove,
    total,
    record,
    page,
    cDate,
    lDate,
    recordPerPage
} from "../../constants/message";
import {
    title,
    seen,
    notseen,
    done,
    cname,
    phone,
    email,
    status,
    fdate,
    tdate,
    contactList,
    name,
    cphone,
    sent_date
} from "./contactstaticmessage";
import AppContext from "../../components/app-context";
class ContactList extends Component {
  //#region Khởi tạo
    constructor(props, context) {
        super(props, context);
    var that = this;
    that.state = {
      ex: {
        Title: contactList[this.context.Language],
        Param: {
          pagesize: null,
          name: null,
          phone: null,
          email: null,
          status: null,
          fromdate: null,
          todate: null
        },
        StatusList: [
          {
            Id: 0,
                Text: notseen[this.context.Language]
          },
          {
            Id: 1,
            Text: seen[this.context.Language],
          },
          {
            Id: 2,
            Text: done[this.context.Language],
          }
        ]
      }
    };
    that.setState(that.state);

    that.handleChangeFilter = that.handleChangeFilter.bind(that);
    that.handleChangeDataRow = that.handleChangeDataRow.bind(that);

    that.toPage(1);
  }
  //#endregion

  toPage(index) {
    FilterListPlugin.filterdata(index, this, apiurl);
  }
  /**
   * Được gọi khi thay đổi các trường filter cập nhật tham số filter vào state
   * Filter dữ liệu theo tham số mới
   * @param {Event} event
   */
  handleChangeFilter(event) {
    FilterListPlugin.handleChangeFilter(event, this);
    this.toPage(1);
  }

  /**
   * Xóa data đã được chọn
   */
  handleDeleteDataRow() {
    RemoveListPlugin.removeList(`${apiurl}/removelist`, this);
  }

  //Được gọi khi change giá trị của mỗi dòng dữ liệu
  handleChangeDataRow(event) {
    const target = event.target;
    const value = target.type === "checkbox" ? target.checked : target.value;
    const name = target.name;
    const index = target.getAttribute("index");
    var newobj = { ...this.state.model.Results[index] };
    newobj[name] = isNaN(value) ? value : parseInt(value);
    var that = this;
    KTApp.blockPage();

    $.ajax({
      url: `${apiurl}/updatecustomize`,
      type: "POST",
      dataType: "json",
      contentType: "application/json",
      data: JSON.stringify({
        model: newobj,
        name
      }),

      success: response => {
        KTApp.unblockPage();
        toastr.clear();
        if (response.status == "error") {
          toastr["error"](response.message, error[that.context.Language]);
        } else {
          that.state.model.Results[index][name] = newobj[name];
          that.setState(that.state);
          toastr["success"](response.message, success[that.context.Language]);
        }
      },
      error: function(er) {
        KTApp.unblockPage();
        toastr["error"](globalErrorMessage[that.context.Language], error[that.context.Language]);
      }
    });
  }

  componentWillMount() {
    $("#cssloading").html(
      ` <link href="/adminstatics/global/plugins/flatpickr/css/flatpickr.min.css" rel="stylesheet" type="text/css" />`
    );
    $("#scriptloading").html(
      `<script src="/adminstatics/global/plugins/flatpickr/js/flatpickr.min.js" type="text/javascript"></script>`
    );
  }

  componentWillUnmount() {
    $("#cssloading").html("");
    $("#scriptloading").html("");
  }
  componentDidMount() {
    //Active menu
    document.title = this.state.ex.Title;

    $("#kt_aside_menu .kt-menu__item").removeClass("kt-menu__item--active");
    $("#kt_aside_menu .kt-menu__item[data-id='lien-he']").addClass(
      "kt-menu__item--active"
    );
  }
  render() {
    return (
      <React.Fragment>
        {this.state && this.state.model ? (
          <React.Fragment>
            <div className="kt-subheader kt-grid__item" id="kt_subheader">
              <div className="kt-subheader__main">
                <div className="kt-subheader__breadcrumbs">
                  <Link
                    to="/admin/dashboard"
                    className="kt-subheader__breadcrumbs-home"
                  >
                    <i className="fa fa-home" />
                  </Link>
                  <span className="kt-subheader__breadcrumbs-separator" />

                  <span className="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
                    {this.state.ex.Title}
                  </span>
                </div>
              </div>
              <div className="kt-subheader__toolbar">
                <div className="kt-subheader__wrapper">
                  <a
                    href="javascript:;"
                    onClick={e => this.handleDeleteDataRow()}
                    className="btn btn-danger"
                  >
                   <i className="fa fa-trash-alt" /> {remove[this.context.Language]}
                  </a>
                </div>
              </div>
            </div>

            <div
              className="kt-content kt-grid__item kt-grid__item--fluid"
              id="kt_content"
            >
              <div className="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                <div className="row">
                  <div className="col-lg-12">
                    <div className="kt-portlet" data-ktportlet="true">
                      <div className="kt-portlet__head">
                        <div className="kt-portlet__head-label">
                          <h3 className="kt-portlet__head-title">
                            {" "}
                            {this.state.ex.Title}
                          </h3>
                        </div>
                      </div>

                      <div className="kt-portlet__body">
                        <form className="kt-form kt-form--fit">
                          <div className="row kt-margin-b-20">
                            <div className="col-lg-3 kt-margin-b-10">
                              <label>{cname[this.context.Language]}:</label>
                              <DelayInput
                                delayTimeout={1000}
                                className="form-control kt-input"
                                placeholder={cname[this.context.Language]}
                                value={this.state.ex.Param.name}
                                name="name"
                                onChange={this.handleChangeFilter}
                              />
                            </div>
                            <div className="col-lg-3  kt-margin-b-10">
                              <label>{cphone[this.context.Language]}:</label>
                              <DelayInput
                                delayTimeout={1000}
                                className="form-control kt-input"
                                placeholder={cphone[this.context.Language]}
                                value={this.state.ex.Param.phone}
                                name="phone"
                                onChange={this.handleChangeFilter}
                              />
                            </div>
                            <div className="col-lg-3 kt-margin-b-10">
                              <label>{email[this.context.Language]}:</label>
                              <DelayInput
                                delayTimeout={1000}
                                className="form-control kt-input"
                                placeholder={email[this.context.Language]}
                                value={this.state.ex.Param.email}
                                name="email"
                                onChange={this.handleChangeFilter}
                              />
                            </div>
                            <div className="col-lg-3 kt-margin-b-10">
                              <label>{status[this.context.Language]}:</label>
                              <select
                                className="form-control kt-input"
                                data-col-index="2"
                                value={this.state.ex.Param.status}
                                name="Status"
                                onChange={this.handleChangeFilter}
                              >
                                <option value="">--Select--</option>
                                {this.state.ex.StatusList.map((e, ix) => {
                                  return <option value={e.Id}>{e.Text}</option>;
                                })}
                              </select>
                            </div>
                            <div className="col-lg-3 kt-margin-b-10">
                              <label>{fdate[this.context.Language]}:</label>
                              <DateTimePicker
                                value={this.state.ex.Param.fromdate}
                                options={{
                                  enableTime: false,
                                  dateFormat: "m/d/Y",
                                  allowInput: true
                                }}
                                onChange={(
                                  selectedDates,
                                  dateStr,
                                  instance
                                ) => {
                                  this.state.ex.Param.fromdate = dateStr;
                                  this.setState(this.state);
                                  this.toPage(1);
                                }}
                                placeholder={fdate[this.context.Language]}
                                datefrom="fromdate"
                              />
                            </div>
                            <div className="col-lg-3 kt-margin-b-10">
                              <label>{tdate[this.context.Language]}:</label>
                              <DateTimePicker
                                value={this.state.ex.Param.todate}
                                options={{
                                  enableTime: false,
                                  dateFormat: "m/d/Y",
                                  allowInput: true
                                }}
                                onChange={(
                                  selectedDates,
                                  dateStr,
                                  instance
                                ) => {
                                  this.state.ex.Param.todate = dateStr;
                                  this.setState(this.state);
                                  this.toPage(1);
                                }}
                                placeholder={tdate[this.context.Language]}
                                datefrom="todate"
                              />
                            </div>
                            <div className="col-lg-3 kt-margin-b-10">
                              <label>{recordPerPage[this.context.Language]}:</label>
                              <select
                                className="form-control kt-input"
                                data-col-index="2"
                                value={this.state.ex.Param.pagesize}
                                name="pagesize"
                                onChange={this.handleChangeFilter}
                              >
                                <option value="10">10 {record[this.context.Language]} / {page[this.context.Language]}</option>
                                <option value="20">20 {record[this.context.Language]} / {page[this.context.Language]}</option>
                                <option value="50">50 {record[this.context.Language]} / {page[this.context.Language]}</option>
                                <option value="100">100 {record[this.context.Language]} / {page[this.context.Language]}</option>
                              </select>
                            </div>
                          </div>
                        </form>

                        <div className="kt-separator kt-separator--border-dashed" />
                        <div
                          id="kt_table_1_wrapper"
                          className="dataTables_wrapper dt-bootstrap4"
                        >
                          <div className="row">
                            <div className="col-sm-12">
                              <table className="table table-striped- table-bordered table-hover table-checkable responsive no-wrap dataTable dtr-inline collapsed">
                                <thead>
                                  <tr role="row">
                                    <th
                                      className="dt-right sorting_disabled"
                                      rowspan="1"
                                      colspan="1"
                                      style={{ width: "1%" }}
                                      aria-label="Record ID"
                                    >
                                      <label className="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                        <input
                                          type="checkbox"
                                          className="kt-group-checkable"
                                          onChange={ev => {
                                            this.state.model.Results = this.state.model.Results.map(
                                              e => {
                                                return {
                                                  ...e,
                                                  IsChecked: ev.target.checked
                                                };
                                              }
                                            );
                                            this.setState(this.state);
                                          }}
                                        />
                                        <span />
                                      </label>
                                    </th>
                                    <th>{title[this.context.Language]}</th>
                                    <th>{name[this.context.Language]}</th>
                                    <th>{phone[this.context.Language]}</th>
                                    <th>{email[this.context.Language]}</th>
                                    <th>{sent_date[this.context.Language]}</th>
                                    <th>{status[this.context.Language]}</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  {this.state.model.Results.map((c, index) => {
                                    return (
                                      <React.Fragment>
                                        <tr>
                                          <td className="dt-right" tabindex="0">
                                            <label className="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                              <input
                                                type="checkbox"
                                                className="kt-checkable"
                                                value={c.Id}
                                                checked={c.IsChecked}
                                                onChange={e => {
                                                  c.IsChecked = !c.IsChecked;
                                                  this.setState(this.state);
                                                }}
                                              />
                                              <span />
                                            </label>
                                          </td>
                                          <td>
                                            <a
                                              href="javascript:;"
                                              onClick={e => {
                                                $(`#detail-${c.Id}`).toggle();
                                                $(e.target)
                                                  .parent()
                                                  .find("i")
                                                  .toggleClass("rotate");
                                              }}
                                            >
                                              <i className="fa fa-chevron-down" />{" "}
                                              {c.Title}
                                            </a>
                                          </td>

                                          <td>{c.Name}</td>
                                          <td>{c.PhoneNumber}</td>
                                          <td>{c.Email}</td>
                                          <td>
                                            {moment
                                              .utc(c.CreatedDate)
                                              .format("MM/DD/YYYY hh:mm:ss A")}
                                          </td>
                                          <td>
                                            <select
                                              className="form-control"
                                              value={c.Status}
                                              name="Status"
                                              index={index}
                                              onChange={
                                                this.handleChangeDataRow
                                              }
                                            >
                                              {this.state.ex.StatusList.map(
                                                (e, ix) => {
                                                  return (
                                                    <option value={e.Id}>
                                                      {e.Text}
                                                    </option>
                                                  );
                                                }
                                              )}
                                            </select>
                                          </td>
                                        </tr>
                                        <tr
                                          className="detail collapse show"
                                          id={`detail-${c.Id}`}
                                          style={{ display: "none" }}
                                        >
                                          <td colSpan="7">
                                            <div className="row">
                                              <div className="col-lg-6">
                                                <div class="kt-portlet kt-portlet--height-fluid">
                                                  <div class="kt-portlet__head kt-portlet__head--noborder">
                                                    <div class="kt-portlet__head-label">
                                                      <h3 class="kt-portlet__head-title" />
                                                    </div>
                                                  </div>
                                                  <div class="kt-portlet__body">
                                                    <div class="kt-widget kt-widget--user-profile-2">
                                                      <div class="kt-widget__head">
                                                        <div class="kt-widget__media">
                                                          <div class="kt-widget__pic kt-widget__pic--success kt-font-success kt-font-boldest">
                                                            {c.Name ? c.Name[0] : ""}
                                                          </div>
                                                        </div>
                                                        <div class="kt-widget__info">
                                                          <a
                                                            href="#"
                                                            class="kt-widget__username"
                                                          >
                                                            {c.Name}
                                                          </a>
                                                          <span class="kt-widget__desc">
                                                            {c.Title}
                                                          </span>
                                                        </div>
                                                      </div>
                                                      <div class="kt-widget__body">
                                                        <div class="kt-widget__section">
                                                          {c.Body}
                                                        </div>
                                                        <div class="kt-widget__item">
                                                          <div class="kt-widget__contact">
                                                            <span class="kt-widget__label">
                                                              Email:
                                                            </span>
                                                            <a
                                                              href="#"
                                                              class="kt-widget__data"
                                                            >
                                                              {c.Email}
                                                            </a>
                                                          </div>
                                                          <div class="kt-widget__contact">
                                                            <span class="kt-widget__label">
                                                              Phone:
                                                            </span>
                                                            <a
                                                              href="#"
                                                              class="kt-widget__data"
                                                            >
                                                              {c.PhoneNumber}
                                                            </a>
                                                           </div>
                                                                                {c.AloVay247 && (
                                                                                    <React.Fragment>
                                                                                        <div class="kt-widget__contact">
                                                                                            <span class="kt-widget__label">
                                                                                                CMND:
                                                                                            </span>
                                                                                            <a
                                                                                                href="#"
                                                                                                class="kt-widget__data"
                                                                                            >
                                                                                                {c.AloVay247.Passport}
                                                                                            </a>
                                                                                        </div>
                                                                                        <div class="kt-widget__contact">
                                                                                            <span class="kt-widget__label">
                                                                                                Số điện thoại cơ quan:
                                                                                            </span>
                                                                                            <a
                                                                                                href="#"
                                                                                                class="kt-widget__data"
                                                                                            >
                                                                                                {c.AloVay247.PhoneNumberHome}
                                                                                            </a>
                                                                                        </div>
                                                                                        <div class="kt-widget__contact">
                                                                                            <span class="kt-widget__label">
                                                                                               Tỉnh / Thành phố:
                                                                                            </span>
                                                                                            <a
                                                                                                href="#"
                                                                                                class="kt-widget__data"
                                                                                            >
                                                                                                {c.AloVay247.City}
                                                                                            </a>
                                                                                        </div>
                                                                                        <div class="kt-widget__contact">
                                                                                            <span class="kt-widget__label">
                                                                                                Thu nhập:
                                                                                            </span>
                                                                                            <a
                                                                                                href="#"
                                                                                                class="kt-widget__data"
                                                                                            >
                                                                                               
                                                                                                {c.AloVay247.Salary}
                                                                                            </a>
                                                                                        </div>
                                                                                        <div class="kt-widget__contact">
                                                                                            <span class="kt-widget__label">
                                                                                                Số tiền cần vay:
                                                                                            </span>
                                                                                            <a
                                                                                                href="#"
                                                                                                class="kt-widget__data"
                                                                                            >

                                                                                                {c.AloVay247.AmountToBorrow}
                                                                                            </a>
                                                                                        </div>
                                                                                        <div class="kt-widget__contact">
                                                                                            <span class="kt-widget__label">
                                                                                                Nguồn thu nhập:
                                                                                            </span>
                                                                                            <a
                                                                                                href="#"
                                                                                                class="kt-widget__data"
                                                                                            >

                                                                                                {c.AloVay247.InCome}
                                                                                            </a>
                                                                                        </div>
                                                                                    </React.Fragment>
                                                                                    )}
                                                          <div class="kt-widget__contact">
                                                            <span class="kt-widget__label">
                                                              Created Date:
                                                            </span>
                                                            <a
                                                              href="#"
                                                              class="kt-widget__data"
                                                            >
                                                              {moment
                                                                .utc(
                                                                  c.CreatedDate
                                                                )
                                                                .format(
                                                                  "MM/DD/YYYY hh:mm:ss A"
                                                                )}
                                                            </a>
                                                          </div>
                                                          <div class="kt-widget__contact">
                                                            <span class="kt-widget__label">
                                                              Status:
                                                            </span>
                                                            <a
                                                              href="#"
                                                              class="kt-widget__data"
                                                            >
                                                              <span
                                                                className={
                                                                  "kt-badge kt-badge--inline" +
                                                                  (c.Status == 0
                                                                    ? " kt-badge--primary"
                                                                    : c.Status ==
                                                                      1
                                                                    ? " kt-badge--warning"
                                                                    : " kt-badge--success")
                                                                }
                                                              >
                                                                {
                                                                  this.state.ex.StatusList.find(
                                                                    x =>
                                                                      x.Id ==
                                                                      c.Status
                                                                  ).Text
                                                                }
                                                              </span>
                                                            </a>
                                                          </div>
                                                        </div>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                          </td>
                                        </tr>
                                      </React.Fragment>
                                    );
                                  })}
                                </tbody>
                              </table>
                            </div>
                          </div>
                          <div className="row">
                            <div className="col-sm-12 col-md-5">
                              <div
                                className="dataTables_info"
                                id="kt_table_1_info"
                                role="status"
                                aria-live="polite"
                              >
                               {total[this.context.Language]} {this.state.model.TotalItemCount} {record[this.context.Language]}
                              </div>
                            </div>
                            <div className="col-sm-12 col-md-7 dataTables_pager">
                              <PagedList
                                currentpage={this.state.model.CurrentPage}
                                pagesize={this.state.model.PageSize}
                                totalitemcount={this.state.model.TotalItemCount}
                                totalpagecount={this.state.model.TotalPageCount}
                                ajaxcallback={this.toPage.bind(this)}
                              />
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </React.Fragment>
        ) : (
          <div />
        )}
      </React.Fragment>
    );
  }
}
ContactList.contextType = AppContext;
export default ContactList;
