const contactList = ["Contact List", "Danh sách liên hệ"];
const seen = ["Seen", "Đã xem"];
const notseen = ["Not Seen", "Chưa xem"];
const done = ["Done", "Xong"];
const cname = ["Customer Name", "Tên khách hàng"];
const cphone = ["Customer Phone", "Số điện thoại"];
const email = ["Email", "Email"];
const status = ["Status", "Trạng thái"];
const fdate = ["From Date", "Từ ngày"];
const tdate = ["To Date", "Đến ngày"];
const title = ["Title", "Tiêu đề"];
const name = ["Name", "Tên"];
const phone = ["Phone", "Số điện thoại"];
const sent_date = ["Sent Date", "Ngày gửi"];
module.exports = {
    contactList,
    seen,
    notseen,
    done,
    cname,
    phone,
    email,
    status,
    fdate,
    tdate,
    title,
    name,
    cphone,
    sent_date
};
