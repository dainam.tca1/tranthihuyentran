import React, { Component } from "react";
import { Link } from "react-router-dom";
import OrderCmsModel from "./models/order-cmsmodel";
import ReactSelect from "react-select";
//ckeditor
import {
    globalErrorMessage,
    error,
    back,
    save,
    success,
    add,
    remove,
    total,
    record,
    page,
    cDate,
    lDate,
    recordPerPage,
    status,
    se_status,
    waiting,
    paid,
    processing,
    s_delivery,
    complete,
    cancel,
    f_date,
    t_date,
    data_notfound,
    qty,
    price,
} from "../../constants/message";
import {
    tranfer,
    online,
    o_his,
    p_info,
    avatar,
    sku,
    p_name,
    bill_info,
    o_status,
    p_status,
    p_method,
    c_info,
    c_name,
    c_phone,
    c_email,
    city,
    district,
    ward,
    c_address,
    note,
    o_view
} from "./models/orderstaticmessage";
import AppContext from "../../components/app-context";
import { PAYMENT_STATUS, ORDER_STATUS } from "../../constants/enums";
import moment from "moment";
const apiurl = "/cms/api/orderhistories";

class OrderAddUpdate extends Component {
  constructor(props,context) {
      super(props, context);
    var that = this;

    //action để nhận biết hiện đang add hay update
    var action = null;
      if (document.location.href.indexOf("/admin/orderhistory/update") >= 0) {
      action = "update";
    } 

    that.state = {
      model: new OrderCmsModel(),
      ex: {
        Title: null,
        Action: action,
        DistrictList: [],
        DistrictListDefault: [],
        CityList: [],
        CityListDefault: [],
        WardListDefault: [],
        WardList:[],
        PaymentStatusList:
           [
                {
                    value: PAYMENT_STATUS.CAPTURED,
                    label: "CAPTURED"
                },
                {
                    value: PAYMENT_STATUS.AUTHORIZED,
                    label: "AUTHORIZED"
                },
                {
                    value: PAYMENT_STATUS.DECILNED,
                    label: "DECILNED"
                },
                {
                    value: PAYMENT_STATUS.ERROR,
                    label: "ERROR"
                },
                {
                    value: PAYMENT_STATUS.REVIEW,
                    label: "REVIEW"
                },
                {
                    value: PAYMENT_STATUS.VOIDED,
                    label: "VOIDED"
                },
                {
                    value: PAYMENT_STATUS.UNKNOWN,
                    label: "UNKNOWN"
                }
           ],
        PaymentMethodList:
           [
                { label: "COD", value: 0 },
                { label: "Credit Card", value: 1 },
              
              ],
        OrderStatusList: [
            {
                value: ORDER_STATUS.ON_HOLD,
                label: "ON_HOLD"
            },          
            {
                value: ORDER_STATUS.PENDING_PAYMENT,
                label: "PENDING_PAYMENT"
            },
            {
                value: ORDER_STATUS.PAYMENT_RECEIVED,
                label: "PAYMENT_RECEIVED"
            },
            {
                value: ORDER_STATUS.PACKAGING,
                label: "PACKAGING"
            },
            {
                value: ORDER_STATUS.ORDER_SHIPPED,
                label: "ORDER_SHIPPED"
            },
            {
                value: ORDER_STATUS.ORDER_ARCHIVED,
                label: "ORDER_ARCHIVED"
            },
            {
                value: ORDER_STATUS.BACK_ORDER,
                label: "BACK_ORDER"
            },
            {
                value: ORDER_STATUS.CANCELED,
                label: "CANCELED"
            },
          ]
      }
    };
      that.setState(that.state);
      $.get("/cms/api/shippingbyarea/getcity", function (response) {
          that.state.ex.CityListDefault = response.data;
          that.state.ex.CityList = response.data ? response.data.map(e => { return { label: e.Name, value: e.Name } }) : [];
          that.setState(that.state);
      });
      $.get("/cms/api/shippingbyarea/getdistrict", function (response) {
          that.state.ex.DistrictListDefault = response.data;  
          if (that.state.model.District != null) {
              
              that.state.ex.DistrictList = that.state.ex.DistrictListDefault.filter(e => { return e.City.Name == that.state.model.City });
              that.state.ex.DistrictList = that.state.ex.DistrictList.map(e => { return { label: e.Name, value: e.Name } });
         
          }
          that.setState(that.state);
      });
      $.get("/cms/api/shippingbyarea/getward", function (response) {
          that.state.ex.WardListDefault = response.data;
          if (that.state.model.Ward != null) {        
              that.state.ex.WardList = that.state.ex.WardListDefault.filter(e => { return e.District.Name == that.state.model.District });          
              that.state.ex.WardList = that.state.ex.WardList.map(e => { return { label: e.Name, value: e.Name } })       
          }
          that.setState(that.state);
      });
     
    if (action === "update") {
      var id = this.props.match.params.id;
      KTApp.blockPage();
      $.ajax({
        url: apiurl + "/getupdate",
        type: "GET",
        dataType: "json",
        contentType: "application/json",
        data: { id },
        success: response => {
          KTApp.unblockPage();
          if (response.status == "success") {
              that.state.model = response.data;
              that.state.ex.Title = o_view[this.context.Language];
            document.title = that.state.ex.Title;
            that.setState(that.state);
          } else {
            toastr["error"](response.message, error[this.context.Language]);
          }
        },
        error: function(er) {
          KTApp.unblockPage();
          toastr["error"](globalErrorMessage[this.context.Language], error[this.context.Language]);
        }
      });
    } 
  }

    submitForm() {
    var that = this;
    KTApp.blockPage();
    $.ajax({
      url: apiurl + "/" + that.state.ex.Action,
      type: "POST",
      dataType: "json",
      contentType: "application/json",
      data: JSON.stringify(that.state.model),
      success: response => {
        KTApp.unblockPage();
        if (response.status == "success") {
          swal.fire({
            title: success[this.context.Language],
            text: response.message,
            type: "success",
            onClose: () => {
              that.props.history.push("/admin/emptypage");
              that.props.history.replace({
                pathname: that.props.location.pathname,
                search: that.props.location.search
              });
            }
          });
        } else {
          toastr["error"](response.message, error[this.context.Language]);
        }
      },
      error: function(er) {
        KTApp.unblockPage();
        toastr["error"](globalErrorMessage[this.context.Language], error[this.context.Language]);
      }
    });
    }

    handleGetCity() {
        var that = this;
        KTApp.blockPage();
        that.state.ex.WardList = [];
        that.state.ex.DistrictList = [];  
        that.setState(that.state);
         
    }

    handleChangeDistrict(value) {
        var that = this;
        KTApp.blockPage();
        if (value == null || value == "") {
           
            that.state.ex.DistrictList = [];
            that.setState(that.state);
            KTApp.unblockPage();
            return -1;
        }
        that.state.ex.DistrictList = that.state.ex.DistrictListDefault.filter(e => { return e.City.Name == value });
        if (that.state.ex.DistrictList.length < 0) {
            KTApp.unblockPage();
            toastr["error"](data_notfound[this.context.Language], error[this.context.Language]);
        } else {
            KTApp.unblockPage();
        }
        that.state.ex.DistrictList = that.state.ex.DistrictList.map(e => { return {label: e.Name, value: e.Name} })
        that.setState(that.state);
    }

    handleChangeWard(value) {
        var that = this;
        KTApp.blockPage();
        if (value == null || value == "") {
            that.state.ex.WardList = [];
            that.setState(that.state);
            KTApp.unblockPage();
            return -1;
        }
        that.state.ex.WardList = that.state.ex.WardListDefault.filter(e => { return e.District.Name == value });
        if (that.state.ex.WardList.length < 0) {
            KTApp.unblockPage();
            toastr["error"](data_notfound[this.context.Language], error[this.context.Language]);
        }
        else {
          KTApp.unblockPage();
        }
        that.state.ex.WardList = that.state.ex.WardList.map(e => { return { label: e.Name, value: e.Name } })
        that.setState(that.state);
    }
   
    componentWillMount() {
        var that = this;
       // this.initAddress();
    //Load script ckeditor lên
        $("#scriptloading").html(
      `<script src="/adminstatics/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>`
      );
       
        
        
  }

  componentDidMount() {
    //Active menu
    document.title = this.state.ex.Title;
    $("#kt_aside_menu .kt-menu__item").removeClass("kt-menu__item--active");
      $("#kt_aside_menu .kt-menu__item[data-id='order-history']").addClass(
      "kt-menu__item--active"
      );
      
  }

  componentWillUnmount() {
    $("#scriptloading").html("");
  }
    render() {
        const convertMoney = (price) => {
            return (
                this.context.CurrencyType === 0 ? (
                    price ? price.formatMoney(
                        2,
                        ".",
                        ",",
                        "$"
                    ) : "0"
                ) : (
                        price ? price.formatMoney(
                            0,
                            ",",
                            "."
                        ) : "0"
                    )
            )
        }
    return (
      <React.Fragment>
        {this.state && this.state.model ? (
          <React.Fragment>
            <div className="kt-subheader kt-grid__item" id="kt_subheader">
              <div className="kt-subheader__main">
                <div className="kt-subheader__breadcrumbs">
                  <Link
                    to="/admin/dashboard"
                    className="kt-subheader__breadcrumbs-home"
                  >
                    <i className="fa fa-home" />
                  </Link>
                  <span className="kt-subheader__breadcrumbs-separator" />
                  <span className="kt-subheader__breadcrumbs-link">
                    Order 
                  </span>
                  <span className="kt-subheader__breadcrumbs-separator" />
                  <Link
                    to="/admin/orderhistory"
                    className="kt-subheader__breadcrumbs-link"
                  >
                                    {o_his[this.context.Language]}
                  </Link>
                  <span className="kt-subheader__breadcrumbs-separator" />
                  <span className="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
                    {this.state.ex.Title}
                  </span>
                </div>
              </div>
              <div className="kt-subheader__toolbar">
                <div className="kt-subheader__wrapper">
                                <Link to="/admin/orderhistory" className="btn btn-secondary">
                    <i className="fa fa-chevron-left" /> {back[this.context.Language]}
                  </Link>
                  <a
                    href="javascript:;"
                    className="btn btn-primary"
                    onClick={() => {
                      this.submitForm();
                    }}
                  >
                                    <i className="fa fa-save"></i> {save[this.context.Language]}
                  </a>
                </div>
              </div>
            </div>
            <div
              className="kt-content kt-grid__item kt-grid__item--fluid"
              id="kt_content"
            >
              <div className="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                            <div class="kt-portlet">
                                <div class="kt-portlet__body kt-portlet__body--fit">
                                    <div class="kt-invoice-1">
                                        <div class="kt-invoice__wrapper">
                                            <div class="kt-invoice__head" style={{ backgroundImage: "url(/img/bg-6.jpg)" }}>
                                                <div class="kt-invoice__container kt-invoice__container--centered">
                                                    <div class="kt-invoice__logo">
                                                        <a href="#">
                                                            <h1>INVOICE</h1>
                                                        </a>
                                                        <a href="#">
                                                            <img src="/img/logo_client_white.png" />
                            </a>
                        </div>
                                                        <span class="kt-invoice__desc" style={{ fontSize: '17px' }}>
                                                        <span>{this.state.model.Recipient.FirstName} {this.state.model.Recipient.LastName}, {this.state.model.Recipient.Street1}, {this.state.model.Recipient.City}, {this.state.model.Recipient.State.Value} {this.state.model.Recipient.ZipCode}</span>
                                                        <span>{this.state.model.Recipient.Country.Name}</span>
                                                        </span>
                                                        <div class="kt-invoice__items">
                                                            <div class="kt-invoice__item">
                                                            <span class="kt-invoice__subtitle">Order Date</span>
                                                            <span class="kt-invoice__text" style={{ fontSize: '17px' }}>{moment.utc(this.state.model.CreatedDate).format("MM/DD/YYYY hh:mm:ss A")}</span>
                                                            </div>
                                                            <div class="kt-invoice__item">
                                                                <span class="kt-invoice__subtitle">INVOICE NO.</span>
                                                            <span class="kt-invoice__text" style={{ fontSize: '17px' }}>#{this.state.model.OrderNumber}</span>
                                                            </div>
                                                            <div class="kt-invoice__item">
                                                                <span class="kt-invoice__subtitle">INVOICE TO.</span>
                                                            <span class="kt-invoice__text" style={{ fontSize: '17px' }}>{this.state.model.Billing.FirstName} {this.state.model.Billing.LastName}, {this.state.model.Billing.Street1}, {this.state.model.Billing.City}, {this.state.model.Billing.State.Value} {this.state.model.Billing.ZipCode}.
                                                                <br />{this.state.model.Billing.Country.Name}</span>
                                                    </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="kt-invoice__body kt-invoice__body--centered">
                                                        <div class="table-responsive">
                                                            <table class="table" >
                                                                <thead>
                                                                    <tr>
                                                                        <th >{avatar[this.context.Language]}</th>
                                                                        <th style={{ textAlign: 'center' }}>{sku[this.context.Language]}</th>
                                                                        <th style={{ textAlign: 'left' }}>{p_name[this.context.Language]}</th>
                                                                        <th>{qty[this.context.Language]}</th>
                                                                        <th>{price[this.context.Language]}</th>
                                                                        <th>{total[this.context.Language]}</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    {this.state.model.OrderItems ? this.state.model.OrderItems.map(e => {
                                                                        return (
                                                                            <tr>
                                                                                <td><img src={e.Avatar} style={{ 'width': "150px", }} /></td>
                                                                                <td style={{ fontWeight: 'bold', textAlign: 'center' }}>{e.SkuCode}</td>
                                                                                <td style={{ fontWeight: 'bold', textAlign: 'left' }}>{e.Name}</td>
                                                                                <td>{e.Quantity}</td>
                                                                                <td>{convertMoney(e.SkuPrice)}</td>
                                                                                <td>{convertMoney(e.SkuPrice * e.Quantity)}</td>
                                                                            </tr>
                                                                        )
                                                                    }) : ""}
                                                                    <tr>
                                                                        <td colSpan="5" style={{ textAlign: 'right' }}>Subtotal Amount</td>
                                                                        <td>{convertMoney(this.state.model.SubTotalAmount)}</td>
                                                                    </tr>

                                                                    <tr>
                                                                    <td colSpan="5" style={{ textAlign: 'right' }}>Discount Amount</td>
                                                                        <td>{convertMoney(this.state.model.DiscountAmount)}</td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td colSpan="5" style={{ textAlign: 'right' }}>Shipping Fee</td>
                                                                        <td>{convertMoney(this.state.model.ShippingAmount)}</td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td colSpan="5" style={{ textAlign: 'right' }}>Taxes</td>
                                                                        <td>{convertMoney(this.state.model.TotalTax)}</td>
                                                                    </tr>
                                                                 
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <div class="kt-invoice__footer">
                                                <div class="kt-invoice__container kt-invoice__container--centered">
                                                    <div class="kt-invoice__content">
                                                        <div className="form-group">
                                                            <label className="control-label">{o_status[this.context.Language]}</label>
                                                            <ReactSelect
                                                                isClearable={false}
                                                                isSearchable={false}
                                                                options={this.state.ex.OrderStatusList}
                                                                value={
                                                                    this.state.ex.OrderStatusList != null
                                                                        ? this.state.ex.OrderStatusList.filter(
                                                                            option =>
                                                                                option.value == this.state.model.OrderStatus
                                                                        )
                                                                        : ""
                                                                }
                                                                onChange={e => {
                                                                    const value = e === null ? "" : e.value;
                                                                    this.state.model.OrderStatus = value;
                                                                    this.setState(this.state);
                                                                }}
                                                            />
                                                        </div>
                                                        <div className="form-group">
                                                            <label className="control-label">{p_status[this.context.Language]}</label>
                                                            <ReactSelect
                                                                isClearable={false}
                                                                isSearchable={false}
                                                                options={this.state.ex.PaymentStatusList}
                                                                value={
                                                                    this.state.ex.PaymentStatusList != null
                                                                        ? this.state.ex.PaymentStatusList.filter(
                                                                            option =>
                                                                                option.value == this.state.model.PaymentStatus
                                                                        )
                                                                        : ""
                                                                }
                                                                // placeholder="Select Main Category"
                                                                onChange={e => {
                                                                    const value = e === null ? "" : e.value;
                                                                    this.state.model.PaymentStatus = value;
                                                                    this.setState(this.state);
                                                                }}
                                                            />
                                                        </div>
                                                        <div className="form-group">
                                                            <label className="control-label">{p_method[this.context.Language]}</label>
                                                            <ReactSelect
                                                                isClearable={false}
                                                                isSearchable={false}
                                                                options={this.state.ex.PaymentMethodList}
                                                                value={
                                                                    this.state.ex.PaymentMethodList != null
                                                                        ? this.state.ex.PaymentMethodList.filter(
                                                                            option =>
                                                                                option.value === this.state.model.PaymentMethod
                                                                        )
                                                                        : ""
                                                                }
                                                                //  placeholder="Select Main Category"
                                                                onChange={e => {
                                                                    const value = e === null ? "" : e.value;
                                                                    this.state.model.PaymentMethod = value;
                                                                    this.setState(this.state);
                                                                }}
                                                            />
                                                        </div>
                                                    </div>
                                                            <div class="kt-invoice__content">
                                                                <span>SHIPMENT INFORMATION</span>
                                                                <span><span>Shiping Id:</span><span>{this.state.model.EasyShipId}</span></span>
                                                               
                                                            </div>
                                                   
                                                            <div class="kt-invoice__content">
                                                                <span>TOTAL AMOUNT</span>
                                                                    <span class="kt-invoice__price">{convertMoney(this.state.model.TotalAmount)}</span>
                                                                <span>Taxes Included</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                <div className="row kt-hidden">
                  <div className="col-lg-8">
                                    <div className="kt-portlet kt-portlet--mobile ">
                      <div className="kt-portlet__head kt-portlet__head--lg">
                        <div className="kt-portlet__head-label">
                          <h3 className="kt-portlet__head-title">
                            {p_info[this.context.Language]}
                          </h3>
                        </div>
                        <div className="kt-portlet__head-toolbar">
                            <div className="kt-portlet__head-group"><a href="javascript:;" data-ktportlet-tool="toggle" className="btn btn-sm btn-icon btn-brand btn-icon-md"><i className="la la-angle-down"></i></a></div>
                        </div>
                      </div>
                   <div className="kt-portlet__body" >
                                            <div className="kt-section">
                                                <div className="kt-section__content">
                                                    <table className="table table-hover table-striped">
                                                        <thead className="thead-dark">
                                                            <tr>
                                                                <th>{avatar[this.context.Language]}</th>
                                                                <th>{sku[this.context.Language]}</th>
                                                                <th>{p_name[this.context.Language]}</th>
                                                                <th>{qty[this.context.Language]}</th>
                                                                <th>{price[this.context.Language]}</th>
                                                                <th>{total[this.context.Language]}</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            {this.state.model.OrderItems ? this.state.model.OrderItems.map(e => {
                                                                return (
                                                                    <tr>
                                                                        <td><img src={e.Avatar} style={{ 'width': "150px", }} /></td>
                                                                        <td style={{ fontWeight:'bold' }}>{e.SkuCode}</td>
                                                                        <td style={{ fontWeight: 'bold' }}>{e.Name}</td>
                                                                        <td>{e.Quantity}</td>
                                                                        <td>{e.SkuPrice ? e.SkuPrice.formatMoney(2, ".", ",", "$") : "0"}</td>
                                                                        <td>{(e.SkuPrice * e.Quantity).formatMoney(2, ".", ",", "$")}</td>
                                                                    </tr>
                                                                    )
                                                            }) : ""}
                                                            <tr>
                                                                <td colSpan="5" style={{ textAlign: 'center' }}>Subtotal Amount</td>
                                                                <td>{convertMoney(this.state.model.SubTotalAmount)}</td>
                                                            </tr>
                                                           
                                                            <tr>
                                                                <td colSpan="5" style={{ textAlign: 'center' }}>Discount Amount</td>
                                                                <td>{convertMoney(this.state.model.DiscountAmount)}</td>
                                                            </tr>
                                                            <tr>
                                                                <td colSpan="5" style={{ textAlign: 'center' }}>Shipping Fee</td>
                                                                <td>{convertMoney(this.state.model.ShippingAmount)}</td>
                                                            </tr>
                                                            <tr>
                                                                <td colSpan="5" style={{ textAlign: 'center' }}>Taxes</td>
                                                                <td>{convertMoney(this.state.model.TotalTax)}</td>
                                                            </tr>
                                                            <tr>
                                                                <td colSpan="5" style={{ textAlign: 'center', fontWeight: 'bold' }}>Total Amount</td>
                                                                <td style={{ color: "red", fontWeight: 'bold' }}>{convertMoney(this.state.model.TotalAmount)}</td>
                                                            </tr>
                                                        </tbody>
                                                       
                                                    </table>
                                                </div>
                                            </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-4">
                    <div className="kt-portlet kt-portlet--mobile">
                      <div className="kt-portlet__head kt-portlet__head--lg">
                        <div className="kt-portlet__head-label">
                          <h3 className="kt-portlet__head-title">{bill_info[this.context.Language]}</h3>
                        </div>
                      </div>

                      <div className="kt-portlet__body">
                            <div className="form-group">
                            <label className="control-label">{o_status[this.context.Language]}</label>
                            <ReactSelect
                            isClearable={false}
                            isSearchable={false}
                            options={this.state.ex.OrderStatusList}
                            value={
                                this.state.ex.OrderStatusList != null
                                    ? this.state.ex.OrderStatusList.filter(
                                                            option =>
                                            option.value == this.state.model.OrderStatus
                                                        )
                                                        : ""
                                                }  
                            onChange={e => {
                             const value = e === null ? "" : e.value;
                                this.state.model.OrderStatus = value;
                              this.setState(this.state);
                            }}
                          />
                           </div>
                            <div className="form-group">
                            <label className="control-label">{p_status[this.context.Language]}</label>
                            <ReactSelect
                            isClearable={false}
                            isSearchable={false}
                            options={this.state.ex.PaymentStatusList}
                            value={
                                this.state.ex.PaymentStatusList != null
                                    ? this.state.ex.PaymentStatusList.filter(
                                                            option =>
                                            option.value == this.state.model.PaymentStatus
                                                        )
                                                        : ""
                                                }
                           // placeholder="Select Main Category"
                            onChange={e => {
                             const value = e === null ? "" : e.value;
                                this.state.model.PaymentStatus = value;
                              this.setState(this.state);
                            }}
                          />
                           </div>
                          <div className="form-group">
                            <label className="control-label">{p_method[this.context.Language]}</label>
                            <ReactSelect
                            isClearable={false}
                            isSearchable={false}
                            options={this.state.ex.PaymentMethodList}
                                                    value={
                                                        this.state.ex.PaymentMethodList != null
                                                            ? this.state.ex.PaymentMethodList.filter(
                                                                option =>
                                                                    option.value === this.state.model.OrderPaymentMethodNacGroup
                                                            )
                                                            : ""
                                                    }
                          //  placeholder="Select Main Category"
                              onChange={e => {
                                  const value = e === null ? "" : e.value;
                                this.state.model.OrderPaymentMethodNacGroup = value;
                              this.setState(this.state);
                            }}
                          />
                        </div>
                      </div>
                    </div>

                    <div className="kt-portlet kt-portlet--mobile">
                      <div className="kt-portlet__head kt-portlet__head--lg">
                        <div className="kt-portlet__head-label">
                         <h3 className="kt-portlet__head-title">{c_info[this.context.Language]}</h3>
                        </div>
                      </div>

                      <div className="kt-portlet__body">
                        <div className="form-group">
                          <label className="control-label">{c_name[this.context.Language]}</label>
                          <input
                            className="form-control"
                            placeholder={c_name[this.context.Language]}
                            maxlength="70"
                            value={this.state.model.FirstName}
                            onChange={e => {
                              this.state.model.FirstName = e.target.value;
                              this.setState(this.state);
                            }}
                          />
                        </div>
                        <div className="form-group">
                          <label className="control-label">
                            {c_phone[this.context.Language]}
                          </label>
                           <input
                            className="form-control"
                            placeholder={c_phone[this.context.Language]}
                            maxlength="70"
                            value={this.state.model.CustomerPhone}
                            onChange={e => {
                              this.state.model.CustomerPhone = e.target.value;
                              this.setState(this.state);
                            }}
                          />
                        </div>
                        <div className="form-group">
                          <label className="control-label">{c_email[this.context.Language]}</label>
                          <input
                            className="form-control"
                            placeholder={c_email[this.context.Language]}
                            value={this.state.model.CustomerEmail}
                            onChange={e => {
                              this.state.model.CustomerEmail = e.target.value;
                              this.setState(this.state);
                            }}
                          />
                        </div>
                         <div className="form-group">
                          <label className="control-label">{city[this.context.Language]}</label>
                          <ReactSelect
                            isClearable={true}
                            isSearchable={true}
                            options={this.state.ex.CityList}
                            value={
                                this.state.ex.CityList != null
                                    ? this.state.ex.CityList.filter(
                                                            option =>
                                            option.value === this.state.model.City
                                                        )
                                                        : ""
                                                }
                            placeholder={city[this.context.Language]}
                            onChange={e => {
                             const value = e === null ? "" : e.value;
                                this.state.model.City = value;
                                this.handleGetCity(value);
                                this.handleChangeDistrict(value);
                              this.setState(this.state);
                            }}
                          />
                        </div>
                         <div className="form-group flex-container flex-center">
                           <div className="form-group-item">
                                  <label className="control-label">{district[this.context.Language]}</label>
                                  <ReactSelect
                                    isClearable={true}
                                    isSearchable={true}
                                    options={this.state.ex.DistrictList}
                                    value={
                                        this.state.ex.DistrictList != null
                                            ? this.state.ex.DistrictList.filter(
                                                                    option =>
                                                    option.value === this.state.model.District
                                                                )
                                                                : ""
                                                        }
                                   placeholder={district[this.context.Language]}
                                    onChange={e => {
                                     const value = e === null ? "" : e.value;
                                        this.state.model.District = value;
                                        this.handleChangeWard(value);
                                      this.setState(this.state);
                                    }}
                                  />
                            </div>
                              <div className="form-group-item">
                             <label className="control-label">{ward[this.context.Language]}</label>
                                  <ReactSelect
                                    isClearable={true}
                                    isSearchable={true}
                                    options={this.state.ex.WardList}
                                    value={
                                        this.state.ex.WardList != null
                                            ? this.state.ex.WardList.filter(
                                                                    option =>
                                                    option.value === this.state.model.Ward
                                                                )
                                                                : ""
                                                        }
                                    placeholder={ward[this.context.Language]}
                                    onChange={e => {
                                     const value = e === null ? "" : e.value;
                                      this.state.model.Ward = value;
                                      this.setState(this.state);
                                    }}
                                  />
                            </div>
                        </div>
                        <div className="form-group">
                          <label className="control-label">{c_address[this.context.Language]}</label>
                          <input
                            className="form-control"
                            placeholder={c_address[this.context.Language]}
                            value={this.state.model.Street}
                            onChange={e => {
                              this.state.model.Street = e.target.value;
                              this.setState(this.state);
                            }}
                          />
                        </div>
                      </div>
                    </div>
                    <div className="kt-portlet kt-portlet--mobile">
                      <div className="kt-portlet__head kt-portlet__head--lg">
                                            <div className="kt-portlet__head-label">
                                                <h3 className="kt-portlet__head-title">{note[this.context.Language]}</h3>
                        </div>
                      </div>

                      <div className="kt-portlet__body">
                                            <div className="form-group">
                                                <textarea
                                                    className="form-control"
                                                    placeholder="Note"
                                                    value={this.state.model.Notes}
                                                    onChange={e => {
                                                        this.state.model.Notes = e.target.value;
                                                        this.setState(this.state); 
                                                    }}>
                                                    </textarea>
                           </div>
                       
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </React.Fragment>
        ) : (
          <div />
        )}
      </React.Fragment>
    );
  }
}
OrderAddUpdate.contextType = AppContext;
export default OrderAddUpdate;
