﻿import React, { Component } from "react";
import shippingByBillCmsModel from "./models/shippingbybill-cmsmodel";
import { Link } from "react-router-dom";
import uploadPlugin from "../../plugins/upload-plugin";
import {
    globalErrorMessage,
    error,
    back,
    save,
    success,
    add,
    remove,
    total,
    record,
    page,
    cDate,
    lDate,
    recordPerPage,
    status,
    se_status,
    waiting,
    paid,
    processing,
    s_delivery,
    complete,
    cancel,
    f_date,
    t_date
} from "../../constants/message";
import {
    ship_setting,
    a_list,
    add_shipping_by_district,
    update_shipping_by_district,
    add_shipping_by_bill,
    update_shipping_by_bill,
    name,
    shipping_fee,
    information,
    region,
    se_region,
    apply,
    ship_order,
    price_from,
    price_to,
    action,
    active,
    district,
    edit
} from "./models/orderstaticmessage";
import AppContext from "../../components/app-context";

//ckeditor
import CkEditor from "../../components/ckeditor";

var apiurl = "/cms/api/shippingbybill";
class shippingByBillAddUpdate extends Component {
  constructor(props,context) {
    super(props,context);
    var action = null;
    if (document.location.href.indexOf("/admin/shippingbybill/add") >= 0) {
      action = "add";
    } else if (
      document.location.href.indexOf("/admin/shippingbybill/update") >= 0
    ) {
      action = "update";
    }
    this.state = {
      model: new shippingByBillCmsModel(),
      ex: {
        Title: null,
        Action: action
      }
    };
    var that = this;

    if (action === "update") {
      //console.log(this.props);

        that.state.ex.Title = update_shipping_by_bill[this.context.Language];
      var id = that.props.match.params.id;
      KTApp.blockPage();
      $.ajax({
        url: apiurl + "/" + that.state.ex.Action,
        type: "GET",
        dataType: "json",
        contentType: "application/json",
        data: { id },
        success: response => {
          KTApp.unblockPage();
          toastr.clear();
          if (response.status == "success") {
            that.state.model = response.data;
            document.title = that.state.ex.Title;
            that.setState(that.state);
          } else {
              toastr["error"](response.message, error[this.context.Language]);
          }
        },
        error: function(er) {
          KTApp.unblockPage();
          toastr.clear();
            toastr["error"](globalErrorMessage[this.context.Language], error[this.context.Language]);
        }
      });
    } else {
        this.state.ex.Title = add_shipping_by_bill[this.context.Language];
      this.setState(this.state);
    }
  }
  componentWillMount() {
    $("#scriptloading").html(
      `<script src="/adminstatics/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>`
    );
  }

  componentDidMount() {
    document.title = this.state.ex.Title;
    $("#kt_aside_menu .kt-menu__item").removeClass("kt-menu__item--active");
      $("#kt_aside_menu .kt-menu__item[data-id='shipping-setting']").addClass(
          "kt-menu__item--active"
      );
      $("#kt_aside_menu .kt-menu__item[data-id='total-shipping']").addClass(
          "kt-menu__item--active"
      );
  }

  componentWillUnmount() {
    $("#scriptloading").html("");
  }

  // this.setState(this.state);
  submitForm() {
    var that = this;
    //var id = that.props.match.params.id;
    KTApp.blockPage();
    $.ajax({
      url: apiurl + "/" + that.state.ex.Action,
      type: "POST",
      dataType: "json",
      contentType: "application/json",
      data: JSON.stringify(that.state.model),
      success: response => {
        KTApp.unblockPage();
        if (response.status == "success") {
          swal.fire({
            title: success[this.context.Language],
            text: response.message,
            type: "success",
            onClose: () => {
              that.props.history.push("/admin/emptypage");
              that.props.history.replace({
                pathname: that.props.location.pathname
              });
            }
          });
        } else {
            toastr["error"](response.message, error[this.context.Language]);
        }
      },
      error: function(er) {
        KTApp.unblockPage();
          toastr["error"](globalErrorMessage[this.context.Language], error[this.context.Language]);
      }
    });
  }

  render() {
    return (
      <React.Fragment>
        {this.state && this.state.model ? (
          <React.Fragment>
            <div className="kt-subheader kt-grid__item" id="kt_subheader">
              <div className="kt-subheader__main">
                <div className="kt-subheader__breadcrumbs">
                  <Link
                    to="/admin/dashboard"
                    className="kt-subheader__breadcrumbs-home"
                  >
                    <i className="fa fa-home" />
                  </Link>
                  <span className="kt-subheader__breadcrumbs-separator" />
                  <span className="kt-subheader__breadcrumbs-link">
                                    {ship_setting[this.context.Language]}
                  </span>
                  <span className="kt-subheader__breadcrumbs-separator" />
                  <Link
                    to="/admin/shippingbybill"
                    className="kt-subheader__breadcrumbs-link"
                  >
                                    {ship_order[this.context.Language]}
                  </Link>
                  <span className="kt-subheader__breadcrumbs-separator" />
                  <span className="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
                    {this.state.ex.Title}
                  </span>
                </div>
              </div>
              <div className="kt-subheader__toolbar">
                <div className="kt-subheader__wrapper">
                  <Link
                    to="/admin/shippingbybill"
                    className="btn btn-secondary"
                  >
                                    <i className="fa fa-chevron-left" /> {back[this.context.Language]}
                  </Link>
                  <a
                    href="javascript:;"
                    className="btn btn-primary"
                    onClick={() => {
                      this.submitForm();
                    }}
                  >
                                    <i className="fa fa-save" /> {save[this.context.Language]}
                  </a>
                </div>
              </div>
            </div>
            <div
              className="kt-content kt-grid__item kt-grid__item--fluid"
              id="kt_content"
            >
              <div className="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                <div className="row">
                  <div className="col-lg-12">
                    <div className="kt-portlet kt-portlet--mobile">
                      <div className="kt-portlet__head kt-portlet__head--lg">
                        <div className="kt-portlet__head-label">
                          <h3 className="kt-portlet__head-title">
                                                    {information[this.context.Language]}
                          </h3>
                        </div>
                      </div>

                      <div className="kt-portlet__body">
                        <div className="form-group">
                                                <label className="control-label">{price_from[this.context.Language]}</label>
                          <input
                            className="form-control"
                            value={this.state.model.PriceFrom}
                            placeholder={price_from[this.context.Language]}
                            onChange={e => {
                              this.state.model.PriceFrom = e.target.value;
                              this.setState(this.state);
                            }}
                          />
                        </div>
                        <div className="form-group">
                          <label className="control-label">{price_to[this.context.Language]}</label>
                          <input
                            className="form-control"
                            value={this.state.model.PriceTo}
                            placeholder={price_to[this.context.Language]}
                            onChange={e => {
                              this.state.model.PriceTo = e.target.value;
                              this.setState(this.state);
                            }}
                          />
                        </div>
                        <div className="form-group">
                          <label className="control-label">{shipping_fee[this.context.Language]}</label>
                          <input
                            className="form-control"
                            value={this.state.model.Feeship}
                            placeholder={shipping_fee[this.context.Language]}
                            onChange={e => {
                              this.state.model.Feeship = e.target.value;
                              this.setState(this.state);
                            }}
                          />
                        </div>
                        <div className="form-group">
                          <label className="kt-checkbox kt-checkbox-brand">
                            <input
                              type="checkbox"
                              checked={this.state.model.IsActive}
                              value={this.state.model.IsActive}
                              onChange={e => {
                                this.state.model.IsActive = e.target.checked;
                                this.setState(this.state);
                              }}
                            />
                            <span />
                            {active[this.context.Language]}
                          </label>
                        </div>
                                     
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </React.Fragment>
        ) : (
          <div />
        )}
      </React.Fragment>
    );
  }
}
shippingByBillAddUpdate.contextType = AppContext;
export default shippingByBillAddUpdate;
