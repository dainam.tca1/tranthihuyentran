import moment from "moment";
import React, { Component } from "react";
import { Link } from "react-router-dom";
import {
  globalErrorMessage,
  error,
  back,
  save,
  success,
  add,
  remove,
  total,
  record,
  page,
  cDate,
  lDate,
  recordPerPage,
  status,
  se_status,
  waiting,
  paid,
  processing,
  s_delivery,
  complete,
  cancel,
  f_date,
  t_date,
  data_notfound,
  qty,
  price,
  are_you_sure,
  yes_de,
  no_ca
} from "../../constants/message";
import {
  orderpickup_setting,
  general_setting,
  session_length,
  date_off,
  se_date_off,
  add_day,
  de_day,
  day,
  spe_date,
  date,
  se_sp_date,
  open_time,
  close_time,
  day_off_seted,
  pl_se_special_date,
  special_date_seted,
  pl_se_begin_hour,
  pl_se_end_hour,
  max_order,
  mxpayment_type,
  active
} from "./models/orderstaticmessage";
import AppContext from "../../components/app-context";
import DateTimePicker from "../../components/datetimepicker";

const apiurl = "/cms/api/gposshoppingcartsetting";

class GPosShoppingCartSetting extends Component {
  constructor(props, context) {
    super(props, context);
    var that = this;
    that.state = {
      model: null,
      ex: {
        Title: orderpickup_setting[this.context.Language],
        DayOff: null,
        DaySpecial: {
          Day: null,
          Open: null,
          Close: null
        }
      }
    };
    that.setState(that.state);
    KTApp.blockPage();
    $.ajax({
      url: apiurl,
      type: "GET",
      dataType: "json",
      contentType: "application/json",
      success: response => {
        KTApp.unblockPage();
        toastr.clear();
        if (response.status == "success") {
          that.state.model = response.data;
          that.setState(that.state);
        } else {
          toastr["error"](response.message, error[this.context.Language]);
        }
      },
      error: function(er) {
        KTApp.unblockPage();
        toastr.clear();
        toastr["error"](
          globalErrorMessage[this.context.Language],
          error[this.context.Language]
        );
      }
    });
  }

  submitForm() {
    var that = this;
    KTApp.blockPage();
    $.ajax({
      url: apiurl + "/update",
      type: "POST",
      dataType: "json",
      contentType: "application/json",
      data: JSON.stringify(that.state.model),
      success: response => {
        KTApp.unblockPage();
        if (response.status == "success") {
          swal.fire({
            title: success[this.context.Language],
            text: response.message,
            type: "success",
            onClose: () => {
              that.props.history.push("/admin/emptypage");
              that.props.history.replace({
                pathname: that.props.location.pathname
              });
            }
          });
        } else {
          toastr["error"](response.message, error[this.context.Language]);
        }
      },
      error: function(er) {
        KTApp.unblockPage();
        toastr["error"](
          globalErrorMessage[this.context.Language],
          error[this.context.Language]
        );
      }
    });
  }

  /**
   * Thêm ngày off
   */
  addDayOff() {
    // Validate check null
    var DayOffEx = this.state.ex.DayOff;
    if (!DayOffEx) {
      toastr["error"](
        se_date_off[this.context.Language],
        error[this.context.Language]
      );
      return -1;
    }
    if (
      this.state.model.DayOff.filter(e => {
        return moment.utc(DayOffEx).isSame(e.Day);
      }).length > 0
    ) {
      toastr["error"](
        day_off_seted[this.context.Language],
        error[this.context.Language]
      );
      return -1;
    }
    this.state.model.DayOff.push({
      Day: moment.utc(DayOffEx).toDate()
    });
    this.setState(this.state);
  }

  /** Xóa ngày off */
  deleteDateOff() {
    var that = this;

    swal
      .fire({
        title: are_you_sure[this.context.Language],
        text: DATA_NOT_RECOVER,
        type: "warning",
        showCancelButton: true,
        confirmButtonText: yes_de[this.context.Language],
        cancelButtonText: no_ca[this.context.Language]
      })
      .then(function(result) {
        if (result.value) {
          that.state.model.DayOff = that.state.model.DayOff.filter(x => {
            return !x.IsChecked;
          });
          that.setState(that.state);
        }
      });
  }

  /** Thêm Special Date */
  addSpecialDate() {
    // Validate check null
    var day = this.state.ex.DaySpecial.Day;
    var open = this.state.ex.DaySpecial.Open;
    var close = this.state.ex.DaySpecial.Close;
    if (!day) {
      toastr["error"](
        pl_se_special_date[this.context.Language],
        error[this.context.Language]
      );
      return -1;
    }
    if (
      this.state.model.DaySpecial.filter(e => {
        return moment.utc(day).isSame(e.Day);
      }).length > 0
    ) {
      toastr["error"](
        special_date_seted[this.context.Language],
        error[this.context.Language]
      );
      return -1;
    }

    if (!open) {
      toastr["error"](
        pl_se_begin_hour[this.context.Language],
        error[this.context.Language]
      );
      return -1;
    }
    if (!close) {
      toastr["error"](
        pl_se_end_hour[this.context.Language],
        error[this.context.Language]
      );
      return -1;
    }

    this.state.model.DaySpecial.push({
      Day: moment.utc(day).toDate(),
      Open: open,
      Close: close,
      IsChecked: false
    });
    this.setState(this.state);
  }

  /**Xóa special date */
  deleteSpecialDate() {
    var that = this;

    swal
      .fire({
        title: are_you_sure[this.context.Language],
        text: DATA_NOT_RECOVER,
        type: "warning",
        showCancelButton: true,
        confirmButtonText: yes_de[this.context.Language],
        cancelButtonText: no_ca[this.context.Language]
      })
      .then(function(result) {
        if (result.value) {
          that.state.model.DaySpecial = that.state.model.DaySpecial.filter(
            x => {
              return !x.IsChecked;
            }
          );
          that.setState(that.state);
        }
      });
  }
  componentWillMount() {
    $("#cssloading").html(
      ` <link href="/adminstatics/global/plugins/flatpickr/css/flatpickr.min.css" rel="stylesheet" type="text/css" />`
    );
    $("#scriptloading").html(
      `<script src="/adminstatics/global/plugins/flatpickr/js/flatpickr.min.js" type="text/javascript"></script>`
    );
  }
  componentDidMount() {
    document.title = this.state.ex.Title;
    $("#kt_aside_menu .kt-menu__item").removeClass("kt-menu__item--active");
    $("#kt_aside_menu .kt-menu__item[data-id='orderpickup-setting']").addClass(
      "kt-menu__item--active"
    );
  }
  render() {
    return (
      <React.Fragment>
        {this.state && this.state.model ? (
          <React.Fragment>
            <div className="kt-subheader kt-grid__item" id="kt_subheader">
              <div className="kt-subheader__main">
                <div className="kt-subheader__breadcrumbs">
                  <Link
                    to="/admin/dashboard"
                    className="kt-subheader__breadcrumbs-home"
                  >
                    <i className="fa fa-home" />
                  </Link>
                  <span className="kt-subheader__breadcrumbs-separator" />

                  <span className="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
                    {this.state.ex.Title}
                  </span>
                </div>
              </div>
              <div className="kt-subheader__toolbar">
                <div className="kt-subheader__wrapper">
                  <a
                    href="javascript:;"
                    onClick={() => {
                      this.submitForm();
                    }}
                    className="btn btn-primary"
                  >
                    <i className="fa fa-save" /> {save[this.context.Language]}
                  </a>
                </div>
              </div>
            </div>
            <div
              className="kt-content kt-grid__item kt-grid__item--fluid"
              id="kt_content"
            >
              <div className="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                <div className="row">
                  <div className="col-lg-8">
                    <div className="kt-portlet kt-portlet--mobile">
                      <div className="kt-portlet__head kt-portlet__head--lg">
                        <div className="kt-portlet__head-label">
                          <h3 className="kt-portlet__head-title">
                            {general_setting[this.context.Language]}
                          </h3>
                        </div>
                      </div>

                      <div className="kt-portlet__body">
                        <div className="form-group">
                          <label className="control-label">
                            {session_length[this.context.Language]}{" "}
                          </label>
                          <input
                            type="number"
                            className="form-control"
                            value={this.state.model.SessionLength}
                            placeholder={session_length[this.context.Language]}
                            onChange={e => {
                              this.state.model.SessionLength = parseInt(
                                e.target.value
                              );
                              this.setState(this.state);
                            }}
                          />
                        </div>
                        <div className="form-group">
                          <label className="control-label">
                            {max_order[this.context.Language]}{" "}
                          </label>
                          <input
                            type="number"
                            className="form-control"
                            value={this.state.model.MaxOrderPerSession}
                            placeholder={max_order[this.context.Language]}
                            onChange={e => {
                              this.state.model.MaxOrderPerSession = parseInt(
                                e.target.value
                              );
                              this.setState(this.state);
                            }}
                          />
                        </div>
                        <div className="form-group">
                          <label className="control-label">
                            {mxpayment_type[this.context.Language]}{" "}
                          </label>
                                                <select
                                                    className="form-control"
                            value={this.state.model.MxPaymentType}
                            onChange={e => {
                              this.state.model.MxPaymentType = parseInt(
                                e.target.value
                              );
                              this.setState(this.state);
                            }}
                          >
                            <option value="0">Website Form</option>
                            <option value="1">MxMerchant Form</option>
                          </select>
                        </div>
                      </div>
                    </div>
                    <div className="kt-portlet kt-portlet--mobile">
                      <div className="kt-portlet__head kt-portlet__head--lg">
                        <div className="kt-portlet__head-label">
                          <h3 className="kt-portlet__head-title">
                            {date_off[this.context.Language]}{" "}
                          </h3>
                        </div>
                      </div>

                      <div className="kt-portlet__body">
                        <div className="form-group">
                          <label>{se_date_off[this.context.Language]}</label>
                          <DateTimePicker
                            value={this.state.ex.DayOff}
                            options={{
                              enableTime: false,
                              dateFormat: "m/d/Y"
                            }}
                            onChange={(selectedDates, dateStr, instance) => {
                              this.state.ex.DayOff = dateStr;
                              this.setState(this.state);
                            }}
                            placeholder={se_date_off[this.context.Language]}
                          />
                        </div>
                        <div className="form-group">
                          <a
                            href="javascript:void(0)"
                            className="btn btn-success"
                            onClick={e => this.addDayOff()}
                          >
                            {add_day[this.context.Language]}
                          </a>{" "}
                          <a
                            href="javascript:void(0)"
                            className="btn btn-danger"
                            onClick={e => this.deleteDateOff()}
                          >
                            {de_day[this.context.Language]}
                          </a>
                        </div>
                        <div className="dataTables_wrapper dt-bootstrap4">
                          <table className="table table-striped- table-bordered table-hover table-checkable dataTable">
                            <thead>
                              <tr>
                                <th className="stt">
                                  {" "}
                                  <label
                                    className="kt-checkbox kt-checkbox--single kt-checkbox--solid"
                                    onChange={ev => {
                                      this.state.model.DayOff = this.state.model.DayOff.map(
                                        e => {
                                          return {
                                            ...e,
                                            IsChecked: ev.target.checked
                                          };
                                        }
                                      );
                                      this.setState(this.state);
                                    }}
                                  >
                                    <input
                                      type="checkbox"
                                      className="kt-group-checkable"
                                    />
                                    <span />
                                  </label>
                                </th>
                                <th className="img">
                                  {" "}
                                  {day[this.context.Language]}
                                </th>
                              </tr>
                            </thead>
                            <tbody className="textlist">
                              {this.state.model.DayOff
                                ? this.state.model.DayOff.map(e => {
                                    return (
                                      <tr>
                                        <td>
                                          <label className="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                            <input
                                              type="checkbox"
                                              className="kt-group-checkable"
                                              value={e.element}
                                              checked={e.IsChecked}
                                              onChange={el => {
                                                e.IsChecked = !e.IsChecked;
                                                this.setState(this.state);
                                              }}
                                            />
                                            <span />
                                          </label>
                                        </td>
                                        <td>
                                          {moment
                                            .utc(e.Day)
                                            .format("MM/DD/YYYY")}
                                        </td>
                                      </tr>
                                    );
                                  })
                                : ""}
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                    <div className="kt-portlet kt-portlet--mobile">
                      <div className="kt-portlet__head kt-portlet__head--lg">
                        <div className="kt-portlet__head-label">
                          <h3 className="kt-portlet__head-title">
                            {spe_date[this.context.Language]}
                          </h3>
                        </div>
                      </div>

                      <div className="kt-portlet__body">
                        <div className="form-group">
                          <label className="control-label">
                            {date[this.context.Language]}
                          </label>
                          <DateTimePicker
                            value={this.state.ex.DaySpecial.Day}
                            options={{
                              enableTime: false,
                              dateFormat: "m/d/Y"
                            }}
                            onChange={(selectedDates, dateStr, instance) => {
                              this.state.ex.DaySpecial.Day = dateStr;
                              this.setState(this.state);
                            }}
                            placeholder={se_sp_date[this.context.Language]}
                          />
                        </div>
                        <div className="form-group">
                          <label className="control-label">
                            {open_time[this.context.Language]}
                          </label>
                          <DateTimePicker
                            value={this.state.ex.DaySpecial.Open}
                            options={{
                              enableTime: true,
                              noCalendar: true,
                              time_24hr: false,
                              dateFormat: "h:i K"
                            }}
                            onChange={(selectedDates, dateStr, instance) => {
                              this.state.ex.DaySpecial.Open = dateStr;
                              this.setState(this.state);
                            }}
                            placeholder={open_time[this.context.Language]}
                          />
                        </div>
                        <div className="form-group">
                          <label className="control-label">
                            {close_time[this.context.Language]}
                          </label>
                          <DateTimePicker
                            value={this.state.ex.DaySpecial.Close}
                            options={{
                              enableTime: true,
                              noCalendar: true,
                              time_24hr: false,
                              dateFormat: "h:i K"
                            }}
                            onChange={(selectedDates, dateStr, instance) => {
                              this.state.ex.DaySpecial.Close = dateStr;
                              this.setState(this.state);
                            }}
                            placeholder={close_time[this.context.Language]}
                          />
                        </div>
                        <div className="form-group">
                          <a
                            href="javascript:void(0)"
                            className="btn btn-success btn-customfile"
                            onClick={e => this.addSpecialDate()}
                          >
                            {add_day[this.context.Language]}
                          </a>{" "}
                          <a
                            href="javascript:void(0)"
                            className="btn btn-danger"
                            onClick={e => this.deleteSpecialDate()}
                          >
                            {de_day[this.context.Language]}
                          </a>
                        </div>
                        <div className="dataTables_wrapper dt-bootstrap4">
                          <table className="table table-striped- table-bordered table-hover table-checkable dataTable">
                            <thead>
                              <tr>
                                <th className="stt">
                                  <label
                                    className="kt-checkbox kt-checkbox--single kt-checkbox--solid"
                                    onChange={ev => {
                                      this.state.model.DaySpecial = this.state.model.DaySpecial.map(
                                        e => {
                                          return {
                                            ...e,
                                            IsChecked: ev.target.checked
                                          };
                                        }
                                      );
                                      this.setState(this.state);
                                    }}
                                  >
                                    <input
                                      className="kt-group-checkable"
                                      type="checkbox"
                                    />
                                    <span />
                                  </label>
                                </th>
                                <th className="img">Day</th>
                                <th>{open_time[this.context.Language]}</th>
                                <th>{close_time[this.context.Language]}</th>
                              </tr>
                            </thead>
                            <tbody className="textlist">
                              {this.state.model.DaySpecial
                                ? this.state.model.DaySpecial.map(e => {
                                    return (
                                      <tr>
                                        <td>
                                          <label className="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                            <input
                                              className="kt-group-checkable"
                                              type="checkbox"
                                              value={e.element}
                                              checked={e.IsChecked}
                                              onChange={el => {
                                                e.IsChecked = !e.IsChecked;
                                                this.setState(this.state);
                                              }}
                                            />
                                            <span />
                                          </label>
                                        </td>
                                        <td>
                                          {moment
                                            .utc(e.Day)
                                            .format("MM/DD/YYYY")}
                                        </td>
                                        <td>{e.Open}</td>
                                        <td>{e.Close}</td>
                                      </tr>
                                    );
                                  })
                                : ""}
                            </tbody>
                          </table>
                        </div>{" "}
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-4">
                    {this.state.model.DayWork
                      ? this.state.model.DayWork.map((e, i) => {
                          return (
                            <div
                              key={i}
                              className="kt-portlet kt-portlet--mobile"
                            >
                              <div className="kt-portlet__head kt-portlet__head--lg">
                                <div className="kt-portlet__head-label">
                                  <h3 className="kt-portlet__head-title">
                                    {e.Day}
                                  </h3>
                                </div>
                              </div>

                              <div className="kt-portlet__body">
                                <div className="form-group">
                                  <label className="control-label">
                                    {open_time[this.context.Language]}
                                  </label>
                                  <DateTimePicker
                                    value={e.Open}
                                    options={{
                                      enableTime: true,
                                      noCalendar: true,
                                      time_24hr: false,
                                      dateFormat: "h:i K"
                                    }}
                                    onChange={(
                                      selectedDates,
                                      dateStr,
                                      instance
                                    ) => {
                                      this.state.model.DayWork[
                                        i
                                      ].Open = dateStr;
                                      this.setState(this.state);
                                    }}
                                    placeholder={
                                      open_time[this.context.Language]
                                    }
                                  />
                                </div>
                                <div className="form-group">
                                  <label className="control-label">
                                    {close_time[this.context.Language]}
                                  </label>
                                  <DateTimePicker
                                    value={e.Close}
                                    options={{
                                      enableTime: true,
                                      noCalendar: true,
                                      time_24hr: false,
                                      dateFormat: "h:i K"
                                    }}
                                    onChange={(
                                      selectedDates,
                                      dateStr,
                                      instance
                                    ) => {
                                      this.state.model.DayWork[
                                        i
                                      ].Close = dateStr;
                                      this.setState(this.state);
                                    }}
                                    placeholder={
                                      close_time[this.context.Language]
                                    }
                                  />
                                </div>
                                <div className="form-group">
                                  <label className="kt-checkbox kt-checkbox--brand">
                                    <input
                                      type="checkbox"
                                      checked={e.IsActive}
                                      onChange={el => {
                                        e.IsActive = !e.IsActive;
                                        this.setState(this.state);
                                      }}
                                    />
                                    <span /> {active[this.context.Language]}
                                  </label>
                                </div>
                              </div>
                            </div>
                          );
                        })
                      : ""}
                  </div>
                </div>
              </div>
            </div>
          </React.Fragment>
        ) : (
          <div />
        )}
      </React.Fragment>
    );
  }
}
GPosShoppingCartSetting.contextType = AppContext;
export default GPosShoppingCartSetting;
