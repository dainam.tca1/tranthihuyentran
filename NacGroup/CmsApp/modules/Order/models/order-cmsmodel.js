class OrderCmsModel {
    constructor() {
        this._id = null;
        this.OrderItems = [];
        this.CreatedDate = new Date();
        this.TotalTax = 0;
        this.TotalAmount = 0;
        this.TotalSubAmount = 0;
        this.TotalShipping = 0;
        this.FirstName = null;
        this.LastName = null;
        this.CustomerId = null;
        this.CustomerEmail = null;
        this.CustomerPhone = null;
        this.Notes = null;
        this.City = null;
        this.District = null;
        this.Street = null;
        this.OrderStatusNacGroup = 0;
        this.OrderPaymentStatusNacGroup = 0;
        this.OrderPaymentMethodNacGroup = 0;
        this.OrderStatus = 0;
        this.PaymentStatus = 0;
        this.PaymentMehod = 0;
        this.ConfirmDate = new Date();
        this.Discount = null;
        this.UpdatedDate = null;
        this.Recipient = new AddressCmsModel();
        this.Billing = new AddressCmsModel();
        this.OrderMxMerchant = {
            AuthMessage: null,
            CardType: null,
            AuthCode: null,
            CardAccount: null,
            RecordNo: null,
            ServerDate: null,
            ServerTime: null
        };
    }
}
module.exports = OrderCmsModel;
class AddressCmsModel {
    constructor() {
        this.FirstName = null,
            this.LastName = null,
            this.BusinessName = null,
            this.Phone = null,
            this.Street1 = null,
            this.Street2 = null,
            this.Country = {
                Name: null,
                Value: null,
            },
            this.State = {
                Name: null,
                Value: null,
            },
            this.City = null,
            this.ZipCode = null
    }
}

