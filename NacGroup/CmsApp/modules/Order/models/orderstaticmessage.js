//#region order pickup setting
const orderpickup_setting = ["Order Pickup Setting", "Cài đặt đặt hàng"];
const general_setting = ["General Setting", "Cài đặt chung"];
const session_length = ["Session Length", "Khoảng cách giờ đặt"];
const date_off = ["Date Off", "Ngày nghỉ"];
const se_date_off = ["Select Date Off", "Chọn ngày nghỉ"];
const add_day = ["Add Day", "Thêm ngày"];
const de_day = ["Delete Day", "Xóa ngày"];
const day = ["Day", "Ngày"];
const spe_date = ["Special Date", "Ngày đặc biệt"];
const date = ["Date", "Ngày"];
const se_sp_date = ["Select Special Date", "Chọn ngày đặc biệt"];
const open_time = ["Open Time", "Giở mở cửa"];
const close_time = ["Close Time", "Giờ đóng cửa"];
const day_off_seted = ["Day off is selected", "Ngày nghỉ đã được chọn"];
const pl_se_special_date = ["Please select day special", "Hãy chọn ngày đặc biệt"];
const special_date_seted = ["Day Special is selected", "Ngày đặc biệt đã được chọn"];
const pl_se_begin_hour = ["Please select begin hour", "Hãy chọn giờ bắt đầu"];
const pl_se_end_hour = ["Please select end hour", "Hãy chọn giờ kết thúc"];
const max_order = ["Max Orders Per Session", "Tối đa đơn hàng trên 1 phiên"];
const mxpayment_type = ["Mx Payment Type", "Mx Payment Type"];
//#endregion
//#region couponlist
const promotion = ["Promotions", "Chương trình khuyến mãi"];
const c_list = ["Coupon List", "Danh sách mã giảm giá"];
const c_code = ["Coupon Code", "Mã giảm giá"];
const c_date = ["Coupon Date", "Ngày sử dụng mã giảm giá"];
const value = ["Value", "Giá trị"];
const c_value = ["Coupon value", "Giá trị mã giảm giá"];
const s_date = ["Start Date", "Ngày bắt đầu"];
const se_s_date = ["-- Select start date --", "-- Chọn ngày bắt đầu --"];
const se_e_date = ["-- Select end date --", "-- Chọn ngày kết thúc --"];
const e_date = ["Expired Date", "Ngày hết hạn"];
const isone = ["Is One Time Use", "Một lần sử dụng"];
const active = ["Active", "Kích hoạt"];
const iactive = ["In Active", "Hủy kích hoạt"];
const coupon_info = ["Coupon Information", "Thông tin mã giảm giá"];
const add_coupon = ["Add Coupon", "Thêm mã giảm giá"];
const update_coupon = ["Update Coupon", "Cập nhật mã giảm giá"];
const value_type = ["Value Type", "Loại giá trị"];
const percent = ["Percent", "Phần trăm"];
const money = ["Money", "Tiền"];
const discount = ["Discount", "Giảm"];
//#endregion
//#region orderlist
const o_his =["Order Histories", "Lịch sử đơn hàng"];
const o_num =["Order Number", "Mã đơn hàng"];
const o_date =["Order Date", "Ngày đặt"];
const c_name =["Customer Name", "Tên khách hàng"];
const c_phone = ["Customer Phone", "Số điện thoại"];
const user = ["User", "Thành viên"];
const view_more = ["View more", "Xem thêm"];
//#endregion
//#region orderview
const tranfer = ["Tranfer", "Chuyển khoản"];
const online = ["Online Payment", "Thanh toán online"];
const p_info = ["Product Information", "Thông tin sản phẩm"];
const avatar = ["Avatar", "Hình ảnh"];
const sku = ["Sku", "Mã sản phẩm"];
const p_name = ["Product Name", "Tên sản phẩm"];
const bill_info = ["Billing Information", "Thông tin đơn hàng"];
const o_status = ["Order Status", "Trạng thái đơn hàng"];
const p_status = ["Payment Status", "Trạng thái thanh toán"];
const p_method = ["Payment method", "Phương thức thanh toán"];
const c_info = ["Customer Information", "Thông tin khách hàng"];
const c_email = ["Customer Email", "Email"];
const city = ["Province / City", "Tỉnh / Thành phố"];
const district = ["District", "Quận"];
const ward = ["Ward", "Huyện"];
const c_address = ["Address", "Địa chỉ"];
const note = ["Notes", "Ghi chú"];
const o_view = ["Order detail view", "Xem chi tiết đơn hàng"];
const payment_status_t = ["Payment Status", "Trạng thái thanh toán"];
const u_date = ["Update Date", "Cập nhật ngày"];
//#endregion
//#region orderview
const ship_setting = ["Shipping Setting", "Cài đặt phí ship"];
const a_list = ["Areas List", "Danh sách khu vực"];
const add_shipping_by_district = ["Add Shipping Fee By District", "Thêm phí ship theo khu vực"];
const update_shipping_by_district = ["Update Shipping Fee By District", "Cập nhật phí ship theo khu vực"];
const add_shipping_by_bill = ["Add Shipping Fee By Bill", "Thêm phí ship tổng đơn"];
const update_shipping_by_bill = ["Update Shipping Fee By Bill", "Cập nhật phí ship tổng đơn"];
const name = ["Name", "Tên"];
const shipping_fee = ["Shipping Fee", "Phí ship"];
const information = ["Information", "Thông tin"];
const region = ["Region", "Khu vực"];
const se_region = ["Select region", "Chọn khu vực"];
const apply = ["Applies to all provinces", "Áp dụng tất cả tỉnh thành"];
const ship_order = ["Ship Fee Total Order Value", "Phí ship tổng giá trị đơn hàng"];
const price_from = ["Price From", "Từ giá"];
const price_to = ["Price To", "Đến giá"];
const action = ["Action", "Hành động"];
const edit = ["Edit", "Sửa"];
//#endregion
module.exports = {
   //#region coupon
    promotion,
    c_list,
    c_code,
    value,
    s_date,
    e_date,
    isone,
    active,
    iactive,
    coupon_info,
    add_coupon,
    update_coupon,
    value_type,
    percent,
    money,
    discount,
    c_value,
    c_date,
    se_e_date,
    se_s_date,
    //#endregion
    //#region orderlist
    o_his,
    o_num,
    o_date,
    c_name,
    c_phone,
    user,
    view_more,
    //#endregion
    //#region orderview
    tranfer,
    online,
    p_info,
    avatar,
    sku,
    p_name,
    bill_info,
    o_status,
    p_status,
    p_method,
    c_info,
    c_name,
    c_phone,
    c_email,
    city,
    district,
    ward,
    c_address,
    note,
    o_view,
    //#endregion
    //#region order pickup setting
    orderpickup_setting,
    general_setting,
    session_length,
    date_off,
    se_date_off,
    add_day,
    de_day,
    day,
    spe_date,
    date,
    se_sp_date,
    open_time,
    close_time,
    day_off_seted,
    pl_se_special_date,
    special_date_seted,
    pl_se_begin_hour,
    pl_se_end_hour,
    max_order,
    mxpayment_type,
    payment_status_t,
    u_date,
//#endregion
    //#region orderview
    ship_setting,
    a_list,
    add_shipping_by_district,
    update_shipping_by_district,
    add_shipping_by_bill,
    update_shipping_by_bill,
    name,
    shipping_fee,
    information,
    region,
    se_region,
    apply,
    ship_order,
    price_from,
    price_to,
    action,
    edit
//#endregion
};
