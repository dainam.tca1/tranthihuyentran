﻿class ShippingByAreaCmsModel {
    constructor() {
        this.Id = null;
        this.City = {
            Id: null,
            Name: null,
            Code: null,
            Level: null,
            DistrictList: []
        };
        this.Price = 0;
        this.IsDefault = false;
    } 
}
export default ShippingByAreaCmsModel;