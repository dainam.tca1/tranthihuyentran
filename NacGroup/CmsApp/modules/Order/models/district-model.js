﻿class DistrictModel {
    constructor() {
        this.Id = null;
        this.Name = null;
        this.Code = null;
        this.Level = null;
        this.Price = 0;
        this.IsActive = false;
    } 
}

export default DistrictModel;