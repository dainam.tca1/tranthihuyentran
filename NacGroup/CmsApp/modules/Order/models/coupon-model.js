﻿class CouponModel {
    constructor() {
        this.Id = null;
        this.CouponCode = null;
        this.StartDate = new Date();
        this.ExpiredDate = new Date();
        this.Value = 0;
        this.IsOneTimeUse = false;
        this.ValueType = 0;
        this.IsActive = false;
    } 
}

export default CouponModel;