import React, { Component } from "react";
import { DelayInput } from "react-delay-input";
import PagedList from "../../components/pagedlist";
import {
    globalErrorMessage,
    error,
    back,
    save,
    success,
    add,
    remove,
    total,
    record,
    page,
    cDate,
    lDate,
    recordPerPage,
    status,
    se_status,
    waiting,
    paid,
    processing,
    s_delivery,
    complete,
    cancel,
    f_date,
    t_date
} from "../../constants/message";
import {
    ship_setting,
    a_list,
    add_shipping_by_district,
    update_shipping_by_district,
    add_shipping_by_bill,
    update_shipping_by_bill,
    name,
    shipping_fee,
    information,
    region,
    se_region,
    apply,
    ship_order,
    price_from,
    price_to,
    action,
    active,
    district,
    edit
} from "./models/orderstaticmessage";
import AppContext from "../../components/app-context";
import { Link } from "react-router-dom";
import FilterListPlugin from "../../plugins/filter-list-plugin";
import RemoveListPlugin from "../../plugins/remove-list-plugin";
const apiurl = "/cms/api/shippingbybill";

class ShippingByBill extends Component {
  constructor(props,context) {
    super(props,context);
    var that = this;
    that.state = {
      ex: {
            Title: ship_order[this.context.Language],
        Param: {
          pagesize: null,
          categoryId: null
        }
      } 
    };
    that.setState(that.state);
    that.handleChangeDataRow = that.handleChangeDataRow.bind(that);
    that.toPage(1);
  }

  
  /**
   * Được gọi khi thay đổi các trường filter cập nhật tham số filter vào state
   * Filter dữ liệu theo tham số mới
   * @param {Event} event
   */
    toPage(index) {
        FilterListPlugin.filterdata(index, this, apiurl);
    }
  /**
   * Xóa data đã được chọn
   */
  handleDeleteDataRow() {
    RemoveListPlugin.removeList(`${apiurl}/removelist`, this);
  }
  handleChangeDataRow(event) {
    const target = event.target;
    const value = target.type === "checkbox" ? target.checked : target.value;
    const name = target.name;
    const index = target.getAttribute("index");
    var newobj = { ...this.state.model.Results[index] };
    newobj[name] = isNaN(value) ? value : parseInt(value);
    var that = this;
  }
  componentWillMount() {}
  componentDidMount() {
    document.title = this.state.ex.Title;
    $("#kt_aside_menu .kt-menu__item").removeClass("kt-menu__item--active");
    $("#kt_aside_menu .kt-menu__item[data-id='shipping-setting']").addClass(
      "kt-menu__item--active"
    );
      $("#kt_aside_menu .kt-menu__item[data-id='total-shipping']").addClass(
      "kt-menu__item--active"
    );
  }
    render() {
        const convertMoney = (price) => {
            return (
                this.context.CurrencyType === 0
                    ? (
                        price
                            ? price.formatMoney(
                                2,
                                ".",
                                ",",
                                "$"
                            )
                            : "0"
                    )
                    : (
                        price
                            ? price.formatMoney(
                                0,
                                ",",
                                "."
                            )
                            : "0"
                    )
            );
        }
    return (
      <React.Fragment>
        {this.state && this.state.model ? (
          <React.Fragment>
            <div className="kt-subheader kt-grid__item" id="kt_subheader">
              <div className="kt-subheader__main">
                <div className="kt-subheader__breadcrumbs">
                  <Link
                    to="/admin/dashboard"
                    className="kt-subheader__breadcrumbs-home"
                  >
                    <i className="fa fa-home" />
                  </Link>
                                <span className="kt-subheader__breadcrumbs-separator" />
                                <span className="kt-subheader__breadcrumbs-link">{ship_setting[this.context.Language]}</span>
                  <span className="kt-subheader__breadcrumbs-separator" />
                  <span className="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
                    {this.state.ex.Title}
                  </span>
                </div>
              </div>
              <div className="kt-subheader__toolbar">
                <div className="kt-subheader__wrapper">
                  <Link
                    to="/admin/shippingbybill/add"
                    className="btn btn-primary"
                  >
                                    <i className="fa fa-plus"></i>{add[this.context.Language]}
                  </Link>
                  <a
                    href="javascript:;"
                    onClick={e => this.handleDeleteDataRow()}
                    className="btn btn-danger"
                  >
                                    <i className="fa fa-trash-alt"></i> {remove[this.context.Language]}
                  </a>
                </div>
              </div>
            </div>
            <div
              className="kt-content kt-grid__item kt-grid__item--fluid"
              id="kt_content"
            >
              <div className="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                <div className="kt-portlet kt-portlet--mobile">
                  <div className="kt-portlet__head kt-portlet__head--lg">
                    <div className="kt-portlet__head-label">
                      <h3 className="kt-portlet__head-title">
                        {this.state.ex.Title}
                      </h3>
                    </div>
                  </div>

                  <div className="kt-portlet__body">
                    <div
                      id="kt_table_1_wrapper"
                      className="dataTables_wrapper dt-bootstrap4"
                    >
                      <div className="row">
                        <div className="col-sm-12">
                          <table className="table table-striped- table-bordered table-hover table-checkable dataTable no-footer dtr-inline table-responsive">
                            <thead>
                              <tr role="row">
                                <th
                                  className="dt-right sorting_disabled"
                                  rowspan="1"
                                  colspan="1"
                                  style={{ width: "1%" }}
                                  aria-label="Record ID"
                                >
                                  <label className="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                    <input
                                      type="checkbox"
                                      className="kt-group-checkable"
                                      onChange={ev => {
                                        this.state.model.Results = this.state.model.Results.map(
                                          e => {
                                            return {
                                              ...e,
                                              IsChecked: ev.target.checked
                                            };
                                          }
                                        );
                                        this.setState(this.state);
                                      }}
                                    />
                                    <span></span>
                                  </label>
                                </th>
                                
                                <th>{price_from[this.context.Language]}</th>
                                <th>{price_to[this.context.Language]}</th>
                                <th>{shipping_fee[this.context.Language]}</th>
                                <th>{status[this.context.Language]}</th>
                                 <th>{action[this.context.Language]}</th>
                              </tr>
                            </thead>
                            <tbody>
                              {this.state.model.Results.map((c, index) => {
                                return (
                                  <tr>
                                    <td className="dt-right" tabindex="0">
                                      <label className="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                        <input
                                          type="checkbox"
                                          className="kt-checkable"
                                          value={c._id}
                                          checked={c.IsChecked}
                                          onChange={e => {
                                            c.IsChecked = !c.IsChecked;
                                            this.setState(this.state);
                                          }}
                                        />
                                        <span></span>
                                      </label>
                                    </td>
                                 
                                        <td>{convertMoney(c.PriceFrom)}</td>
                                        <td>{convertMoney(c.PriceTo)}</td>
                                        <td>{convertMoney(c.Feeship)}</td>
                                        <td>{c.IsActive ? "Active" : "inActive"}</td>
                                        <td>
                                            <Link
                                                to={`/admin/shippingbybill/update/${c.Id}`}
                                            >
                                                {edit[this.context.Language]}
                                      </Link>
                                        </td>
                                  </tr>
                                );
                              })}
                            </tbody>
                          </table>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-sm-12 col-md-5">
                          <div
                            className="dataTables_info"
                            id="kt_table_1_info"
                            role="status"
                            aria-live="polite"
                          >
                            {total[this.context.Language]} {this.state.model.TotalItemCount} {record[this.context.Language]}
                          </div>
                        </div>
                        <div className="col-sm-12 col-md-7 dataTables_pager">
                          <PagedList
                            currentpage={this.state.model.CurrentPage}
                            pagesize={this.state.model.PageSize}
                            totalitemcount={this.state.model.TotalItemCount}
                            totalpagecount={this.state.model.TotalPageCount}
                            ajaxcallback={this.toPage.bind(this)}
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </React.Fragment>
        ) : (
          <div />
        )}
      </React.Fragment>
    );
  }
}
ShippingByBill.contextType = AppContext;
export default ShippingByBill;
