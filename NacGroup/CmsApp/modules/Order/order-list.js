import React, { Component } from "react";
import { Link } from "react-router-dom";
import { DelayInput } from "react-delay-input";
import PagedList from "../../components/pagedlist";
import moment from "moment";
import FilterListPlugin from "../../plugins/filter-list-plugin";
import RemoveListPlugin from "../../plugins/remove-list-plugin";
import DateTimePicker from "../../components/datetimepicker";
import { PAYMENT_STATUS, ORDER_STATUS } from "../../constants/enums";
const apiurl = "/cms/api/orderhistories";
import {
    globalErrorMessage,
    error,
    back,
    save,
    success,
    add,
    remove,
    total,
    record,
    page,
    cDate,
    lDate,
    recordPerPage,
    status,
    se_status,
    waiting,
    paid,
    processing,
    s_delivery,
    complete,
    cancel,
    f_date,
    t_date
} from "../../constants/message";
import {
    o_his,
    o_num,
    o_date,
    c_name,
    c_address,
    c_phone,
    payment_status_t,
    u_date,
    user,
    view_more
} from "./models/orderstaticmessage";
import AppContext from "../../components/app-context";
function payment_status(status) {
    var html = "";
    switch (status) {
        case PAYMENT_STATUS.AUTHORIZED:
            html += "AUTHORIZED";
            break;
        case PAYMENT_STATUS.CAPTURED:
            html += "CAPTURED";
            break;
        case PAYMENT_STATUS.DECILNED:
            html += "DECILNED";
            break;
        case PAYMENT_STATUS.ERROR:
            html += "ERROR";
            break;
        case PAYMENT_STATUS.REVIEW:
            html += "REVIEW";
            break;
        case PAYMENT_STATUS.UNKNOWN:
            html += "UNKNOWN";
            break;
        case PAYMENT_STATUS.VOIDED:
            html += "VOIDED";
            break;
    }
    return html;
}
class OrderList extends Component {
  //#region Khởi tạo
  constructor(props,context) {
    super(props,context);
    var that = this;
    that.state = {
      ex: {
        Title: o_his[this.context.Language],
        Param: {
          pagesize: null,
          ordernumber: null,
          customername: null,
          phone: null,
          email: null,
          status: null,
          fromdate: null,
          todate: null
        },
        OrderStatusList: [
            {
                Id: ORDER_STATUS.ON_HOLD,
                Text: "ON_HOLD"
          },       
            {
                Id: ORDER_STATUS.PENDING_PAYMENT,
                Text:"PENDING_PAYMENT"
          },
            {
                Id: ORDER_STATUS.PAYMENT_RECEIVED,
                Text: "PAYMENT_RECEIVED"
            },
            {
                Id: ORDER_STATUS.PACKAGING,
                Text: "PACKAGING"
            },  
            {
                Id: ORDER_STATUS.ORDER_SHIPPED,
                Text: "ORDER_SHIPPED"
            },
            {
                Id: ORDER_STATUS.ORDER_ARCHIVED,
                Text: "ORDER_ARCHIVED"
            },
            {
                Id: ORDER_STATUS.BACK_ORDER,
                Text: "BACK_ORDER"
            },
            {
                Id: ORDER_STATUS.CANCELED,
                Text: "CANCELED"
            },
            ],
          PaymentStatusList: [
              {
                  Id: PAYMENT_STATUS.CAPTURED,
                  Text: "CAPTURED"
                },
              {
                  Id: PAYMENT_STATUS.AUTHORIZED,
                  Text: "AUTHORIZED"
                },
              {
                  Id: PAYMENT_STATUS.DECILNED,
                  Text: "DECILNED"
                },
              {
                  Id: PAYMENT_STATUS.ERROR,
                  Text: "ERROR"
              },
              {
                  Id: PAYMENT_STATUS.REVIEW,
                  Text: "REVIEW"
              },
              {
                  Id: PAYMENT_STATUS.VOIDED,
                  Text: "VOIDED"
              },
              {
                  Id: PAYMENT_STATUS.UNKNOWN,
                  Text: "UNKNOWN"
              }
        ]
      }
    };
    that.setState(that.state);

    that.handleChangeFilter = that.handleChangeFilter.bind(that);
    that.handleChangeDataRow = that.handleChangeDataRow.bind(that);

    that.toPage(1);
  }
  //#endregion

  toPage(index) {
    FilterListPlugin.filterdata(index, this, apiurl);
  }
  /**
   * Được gọi khi thay đổi các trường filter cập nhật tham số filter vào state
   * Filter dữ liệu theo tham số mới
   * @param {Event} event
   */
  handleChangeFilter(event) {
    FilterListPlugin.handleChangeFilter(event, this);
    this.toPage(1);
  }

  /**
   * Xóa data đã được chọn
   */
    UpdateStatus(id,status) {
        var that = this;
        const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                confirmButton: 'btn btn-success',
                cancelButton: 'btn btn-danger'
            },
            buttonsStyling: false
        })
        swalWithBootstrapButtons.fire({
            title: 'Are you sure change status?',
            text: 'Do you want change status this order',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, I do'
        }).then((result) => {
            if (result.value) {
                KTApp.blockPage();
                $.ajax({
                    url: apiurl + "/updatestatus",
                    type: "POST",
                    dataType: "json",
                    contentType: "application/json",
                    data: JSON.stringify({ id: id, status: status }),
                    success: function (response) {
                        KTApp.unblockPage();
                        if (response.status == "success") {
                            swalWithBootstrapButtons.fire(
                                'Success!',
                                'Your order has been change status.',
                                'success'
                            )
                            that.state.model.Results.forEach(status => {
                                if (status.Id == id) {
                                    status.OrderStatus = response.data
                                }
                            });
                            that.setState(that.state);
                        } else {
                            swalWithBootstrapButtons.fire(
                                'Cancelled',
                                globalErrorMessage[that.context.Language],
                                'error'
                            )
                        }
                    },
                    error: function (er) {
                        $("#loading").hide();
                        swalWithBootstrapButtons.fire(
                            'Cancelled',
                            globalErrorMessage[that.context.Language],
                            'error'
                        )
                    }
                });
            }
        })
       
    }

  //Được gọi khi change giá trị của mỗi dòng dữ liệu
  handleChangeDataRow(event) {
    const target = event.target;
    const value = target.type === "checkbox" ? target.checked : target.value;
    const name = target.name;
    const index = target.getAttribute("index");
    var newobj = { ...this.state.model.Results[index] };
    newobj[name] = isNaN(value) ? value : parseInt(value);
    var that = this;
    KTApp.blockPage();

    $.ajax({
      url: `${apiurl}/updatecustomize`,
      type: "POST",
      dataType: "json",
      contentType: "application/json",
      data: JSON.stringify({
        model: newobj,
        name
      }),

      success: response => {
        KTApp.unblockPage();
        toastr.clear();
        if (response.status == "error") {
          toastr["error"](response.message, error[this.context.Language]);
        } else {
          that.state.model.Results[index][name] = newobj[name];
          that.setState(that.state);
          toastr["success"](response.message, success[this.context.Language]);
        }
      },
      error: function(er) {
        KTApp.unblockPage();
        toastr["error"](globalErrorMessage[this.context.Language], error[this.context.Language]);
      }
    });
  }

  componentWillMount() {
    $("#cssloading").html(
      ` <link href="/adminstatics/global/plugins/flatpickr/css/flatpickr.min.css" rel="stylesheet" type="text/css" />`
    );
    $("#scriptloading").html(
      `<script src="/adminstatics/global/plugins/flatpickr/js/flatpickr.min.js" type="text/javascript"></script>`
    );
  }

  componentWillUnmount() {
    $("#cssloading").html("");
    $("#scriptloading").html("");
  }
  componentDidMount() {
    //Active menu
    document.title = this.state.ex.Title;

    $("#kt_aside_menu .kt-menu__item").removeClass("kt-menu__item--active");
    $("#kt_aside_menu .kt-menu__item[data-id='order-history']").addClass(
      "kt-menu__item--active"
    );
  }
  render() {
    return (
      <React.Fragment>
        {this.state && this.state.model ? (
          <React.Fragment>
            <div className="kt-subheader kt-grid__item" id="kt_subheader">
              <div className="kt-subheader__main">
                <div className="kt-subheader__breadcrumbs">
                  <Link
                    to="/admin/dashboard"
                    className="kt-subheader__breadcrumbs-home"
                  >
                    <i className="fa fa-home" />
                  </Link>
                  <span className="kt-subheader__breadcrumbs-separator" />

                  <span className="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
                    {this.state.ex.Title}
                  </span>
                </div>
              </div>
              <div className="kt-subheader__toolbar">
                <div className="kt-subheader__wrapper">
                 
                </div>
              </div>
            </div>

            <div
              className="kt-content kt-grid__item kt-grid__item--fluid"
              id="kt_content"
            >
              <div className="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                <div className="row">
                  <div className="col-lg-12">
                    <div className="kt-portlet" data-ktportlet="true">
                      <div className="kt-portlet__head">
                        <div className="kt-portlet__head-label">
                          <h3 className="kt-portlet__head-title">
                            {" "}
                            {this.state.ex.Title}
                          </h3>
                        </div>
                      </div>

                      <div className="kt-portlet__body">
                        <form className="kt-form kt-form--fit">
                          <div className="row kt-margin-b-20">
                            <div className="col-lg-3 kt-margin-b-10">
                              <label>{o_num[this.context.Language]}:</label>
                              <DelayInput
                                delayTimeout={1000}
                                className="form-control kt-input"
                                placeholder={o_num[this.context.Language]}
                                value={this.state.ex.Param.ordernumber}
                                name="ordernumber"
                                onChange={this.handleChangeFilter}
                              />
                            </div>
                            <div className="col-lg-3 kt-margin-b-10">
                              <label>{c_name[this.context.Language]}:</label>
                              <DelayInput
                                delayTimeout={1000}
                                className="form-control kt-input"
                                placeholder={c_name[this.context.Language]}
                                value={this.state.ex.Param.customername}
                                name="customername"
                                onChange={this.handleChangeFilter}
                              />
                            </div>
                            <div className="col-lg-3  kt-margin-b-10">
                              <label>{c_phone[this.context.Language]}:</label>
                              <DelayInput
                                delayTimeout={1000}
                                className="form-control kt-input"
                                placeholder={c_phone[this.context.Language]}
                                value={this.state.ex.Param.phone}
                                name="phone"
                                onChange={this.handleChangeFilter}
                              />
                            </div>
                            <div className="col-lg-3 kt-margin-b-10">
                              <label>{f_date[this.context.Language]}:</label>
                              <DateTimePicker
                                value={this.state.ex.Param.fromdate}
                                options={{
                                  enableTime: false,
                                  dateFormat: "m/d/Y",
                                  allowInput: true
                                }}
                                onChange={(
                                  selectedDates,
                                  dateStr,
                                  instance
                                ) => {
                                  this.state.ex.Param.fromdate = dateStr;
                                  this.setState(this.state);
                                  this.toPage(1);
                                }}
                                placeholder={f_date[this.context.Language]}
                                datefrom="fromdate"
                              />
                            </div>
                            <div className="col-lg-3 kt-margin-b-10">
                              <label>{t_date[this.context.Language]}:</label>
                              <DateTimePicker
                                value={this.state.ex.Param.todate}
                                options={{
                                  enableTime: false,
                                  dateFormat: "m/d/Y",
                                  allowInput: true
                                }}
                                onChange={(
                                  selectedDates,
                                  dateStr,
                                  instance
                                ) => {
                                  this.state.ex.Param.todate = dateStr;
                                  this.setState(this.state);
                                  this.toPage(1);
                                }}
                                placeholder={t_date[this.context.Language]}
                                datefrom="todate"
                              />
                            </div>
                            <div className="col-lg-3 kt-margin-b-10">
                              <label>{status[this.context.Language]}:</label>
                              <select
                                className="form-control kt-input"
                                data-col-index="2"
                                value={this.state.ex.Param.status}
                                name="Status"
                                onChange={this.handleChangeFilter}
                              >
                                <option value="">{se_status[this.context.Language]}</option>
                                {this.state.ex.OrderStatusList.map((e, ix) => {
                                  return <option value={e.Id}>{e.Text}</option>;
                                })}
                              </select>
                            </div>
                            <div className="col-lg-3 kt-margin-b-10">
                              <label>{recordPerPage[this.context.Language]}:</label>
                              <select
                                className="form-control kt-input"
                                data-col-index="2"
                                value={this.state.ex.Param.pagesize}
                                name="pagesize"
                                onChange={this.handleChangeFilter}
                              >
                                <option value="10">10 {record[this.context.Language]} / {page[this.context.Language]}</option>
                                <option value="20">20 {record[this.context.Language]} / {page[this.context.Language]}</option>
                                <option value="50">50 {record[this.context.Language]} / {page[this.context.Language]}</option>
                                <option value="100">100 {record[this.context.Language]} / {page[this.context.Language]}</option>
                              </select>
                            </div>
                          </div>
                        </form>

                        <div className="kt-separator kt-separator--border-dashed" />
                        <div
                          id="kt_table_1_wrapper"
                          className="dataTables_wrapper dt-bootstrap4"
                        >
                          <div className="row">
                            <div className="col-sm-12">
                              <table className="table table-striped- table-bordered table-hover table-checkable responsive no-wrap dataTable dtr-inline collapsed">
                                <thead>
                                  <tr role="row">   
                                     <th>{o_date[this.context.Language]}</th>
                                    <th>{o_num[this.context.Language]}</th>
                                   
                                    <th>{payment_status_t[this.context.Language]}</th>
                                    <th>{status[this.context.Language]}</th>                                   
                                     <th>{u_date[this.context.Language]}</th> 
                                     <th>{view_more[this.context.Language]}</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  {this.state.model.Results.map((c, index) => {
                                    return (
                                      <React.Fragment>
                                            <tr>   
                                                <td>
                                                    {moment
                                                        .utc(c.CreatedDate)
                                                        .format("MM/DD/YYYY hh:mm:ss A")}
                                                </td>
                                                <td>
                                                    <Link to={`/admin/orderhistory/update/${c.Id}`}>#{c.OrderNumber}</Link>               
                                          </td>
                                        
                                         <td>
                                            <select
                                              className="form-control"
                                              value={c.PaymentStatus}
                                              name="PaymentStatus"
                                              index={index}
                                              onChange={
                                                this.handleChangeDataRow
                                              }
                                            >
                                              {this.state.ex.PaymentStatusList.map(
                                                (e, ix) => {
                                                  return (
                                                    <option value={e.Id}>
                                                      {e.Text}
                                                    </option>
                                                  );
                                                }
                                              )}
                                            </select>
                                                </td>
                                             
                                          <td>
                                            <select
                                              className="form-control"
                                              value={c.OrderStatus}
                                              name="OrderStatus"
                                              onChange={
                                                  e => { this.UpdateStatus(c.Id, e.target.value)}
                                              }
                                            >
                                              {this.state.ex.OrderStatusList.map(
                                                (e, ix) => {
                                                  return (
                                                    <option value={e.Id}>
                                                      {e.Text}
                                                    </option>
                                                  );
                                                }
                                              )}
                                            </select>
                                          </td>
                                          <td>
                                            {moment
                                              .utc(c.UpdatedDate)
                                              .format("MM/DD/YYYY hh:mm:ss A")}
                                          </td>
                                                <td>
                                                    <a
                                                        href="javascript:;"
                                                        onClick={e => {
                                                            $(`#detail-${c.Id}`).toggle();
                                                            $(e.target)
                                                                .parent()
                                                                .find("i")
                                                                .toggleClass("rotate");
                                                        }}
                                                    >
                                                        <i className="fa fa-chevron-down" />{" "}
                                                        View More
                                             </a>
                                                </td>
                                        </tr>
                                            <tr
                                                className="detail collapse show"
                                                id={`detail-${c.Id}`}
                                                style={{ display: "none" }}
                                            >
                                                <td colSpan="12">
                                                    <div className="row">
                                                        <div className="col-lg-4">
                                                            <div className="kt-portlet kt-portlet--height-fluid">             
                                                                <div className="kt-portlet__body">
                                                                    <div className="kt-widget kt-widget--user-profile-2">
                                                                       
                                                                        <div className="kt-widget__body">
                                                                            <div className="kt-widget__info">
                                                                                <h3 className="kt-widget__desc" style={{ "color": "#000", textTransform: "uppercase", textAlign: "center", fontWeight:"bold" }}>
                                                                                    Order
                                                                                </h3>
                                                                            </div>
                                                                            <div className="kt-widget__item">
                                                                                <div className="kt-widget__contact">
                                                                                    <span className="kt-widget__label">
                                                                                        Subtotal:
                                                                                    </span>
                                                                                    <a
                                                                                        href="#"
                                                                                        className="kt-widget__data"
                                                                                    >
                                                                                      
                                                                                        {this.context.CurrencyType === 0 ? (    
                                                                                                c.SubTotalAmount ? c.SubTotalAmount.formatMoney(
                                                                                                    2,
                                                                                                    ".",
                                                                                                    ",",
                                                                                                    "$"
                                                                                                ) : "0" 
                                                                                        ) : (
                                                                                                c.SubTotalAmount ? c.SubTotalAmount.formatMoney(
                                                                                                    0,
                                                                                                    ",",
                                                                                                    "."
                                                                                                ) : "0" 
                                                                                            )}
                                                                                    </a>
                                                                                </div>
                                                                                <div className="kt-widget__contact">
                                                                                    <span className="kt-widget__label">
                                                                                        Tax:
                                                                                    </span>
                                                                                    <a
                                                                                        href="#"
                                                                                        className="kt-widget__data"
                                                                                    >

                                                                                        {this.context.CurrencyType === 0 ? (
                                                                                            c.TotalTax ? c.TotalTax.formatMoney(
                                                                                                2,
                                                                                                ".",
                                                                                                ",",
                                                                                                "$"
                                                                                            ) : "0"
                                                                                        ) : (
                                                                                                c.TotalTax ? c.TotalTax.formatMoney(
                                                                                                    0,
                                                                                                    ",",
                                                                                                    "."
                                                                                                ) : "0"
                                                                                            )}
                                                                                    </a>
                                                                                </div>
                                                                                <div className="kt-widget__contact">
                                                                                    <span className="kt-widget__label">
                                                                                        Shipping Fee:
                                                                                    </span>
                                                                                    <a
                                                                                        href="#"
                                                                                        className="kt-widget__data"
                                                                                    >

                                                                                        {this.context.CurrencyType === 0 ? (
                                                                                            c.ShippingAmount ? c.ShippingAmount.formatMoney(
                                                                                                2,
                                                                                                ".",
                                                                                                ",",
                                                                                                "$"
                                                                                            ) : "0"
                                                                                        ) : (
                                                                                                c.ShippingAmount ? c.ShippingAmount.formatMoney(
                                                                                                    0,
                                                                                                    ",",
                                                                                                    "."
                                                                                                ) : "0"
                                                                                            )}
                                                                                    </a>
                                                                                </div>
                                                                                <div className="kt-widget__contact">
                                                                                    <span className="kt-widget__label">
                                                                                        Discount:
                                                                                    </span>
                                                                                    <a
                                                                                        href="#"
                                                                                        className="kt-widget__data"
                                                                                    >

                                                                                        {this.context.CurrencyType === 0 ? (
                                                                                            c.DiscountAmount ? c.DiscountAmount.formatMoney(
                                                                                                2,
                                                                                                ".",
                                                                                                ",",
                                                                                                "$"
                                                                                            ) : "0"
                                                                                        ) : (
                                                                                                c.DiscountAmount ? c.DiscountAmount.formatMoney(
                                                                                                    0,
                                                                                                    ",",
                                                                                                    "."
                                                                                                ) : "0"
                                                                                            )}
                                                                                    </a>
                                                                                </div>
                                                                                <div className="kt-widget__contact">
                                                                                    <span className="kt-widget__label">
                                                                                        Total:
                                                                                    </span>
                                                                                    <a
                                                                                        href="#"
                                                                                        className="kt-widget__data"
                                                                                    >

                                                                                        {this.context.CurrencyType === 0 ? (
                                                                                            c.TotalAmount ? c.TotalAmount.formatMoney(
                                                                                                2,
                                                                                                ".",
                                                                                                ",",
                                                                                                "$"
                                                                                            ) : "0"
                                                                                        ) : (
                                                                                                c.TotalAmount ? c.TotalAmount.formatMoney(
                                                                                                    0,
                                                                                                    ",",
                                                                                                    "."
                                                                                                ) : "0"
                                                                                            )}
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-lg-4">
                                                            <div className="kt-portlet kt-portlet--height-fluid">
                                                                <div className="kt-portlet__body">
                                                                    <div className="kt-widget kt-widget--user-profile-2">
                                                                       
                                                                        <div className="kt-widget__body">
                                                                            <div className="kt-widget__info">
                                                                                <h3 className="kt-widget__desc" style={{ "color": "#000", textTransform: "uppercase", textAlign: "center", fontWeight: "bold" }}>
                                                                                    Shipping
                                                                                </h3>
                                                                            </div>
                                                                            <div className="kt-widget__item">
                                                                                <div className="kt-widget__contact">
                                                                                    <span className="kt-widget__label">
                                                                                        Name:
                                                                                    </span>
                                                                                    <a
                                                                                        href="#"
                                                                                        className="kt-widget__data"
                                                                                    >
                                                                                        {`${c.Recipient.FirstName} ${c.Recipient.LastName}`}
                                                                                    </a>
                                                                                </div>
                                                                                <div className="kt-widget__contact">
                                                                                    <span className="kt-widget__label">
                                                                                        Phone:
                                                                                    </span>
                                                                                    <a
                                                                                        href="#"
                                                                                        className="kt-widget__data"
                                                                                    >

                                                                                        {c.Recipient.PhoneNumber}
                                                                                    </a>
                                                                                </div>
                                                                                <div className="kt-widget__contact">
                                                                                    <span className="kt-widget__label">
                                                                                        Address Line:
                                                                                    </span>
                                                                                    <a
                                                                                        href="#"
                                                                                        className="kt-widget__data"
                                                                                    >

                                                                                        {c.Recipient.Street1}
                                                                                    </a>
                                                                                </div>
                                                                                <div className="kt-widget__contact">
                                                                                    <span className="kt-widget__label">
                                                                                        City:
                                                                                    </span>
                                                                                    <a
                                                                                        href="#"
                                                                                        className="kt-widget__data"
                                                                                    >

                                                                                        {c.Recipient.City}
                                                                                    </a>
                                                                                </div>
                                                                                <div className="kt-widget__contact">
                                                                                    <span className="kt-widget__label">
                                                                                        State:
                                                                                    </span>
                                                                                    <a
                                                                                        href="#"
                                                                                        className="kt-widget__data"
                                                                                    >

                                                                                        {c.Recipient.State.Name}
                                                                                    </a>
                                                                                </div>
                                                                                <div className="kt-widget__contact">
                                                                                    <span className="kt-widget__label">
                                                                                        Zip Code:
                                                                                    </span>
                                                                                    <a
                                                                                        href="#"
                                                                                        className="kt-widget__data"
                                                                                    >

                                                                                        {c.Recipient.ZipCode}
                                                                                    </a>
                                                                                </div>
                                                                                <div className="kt-widget__contact">
                                                                                    <span className="kt-widget__label">
                                                                                        Country:
                                                                                    </span>
                                                                                    <a
                                                                                        href="#"
                                                                                        className="kt-widget__data"
                                                                                    >

                                                                                        {c.Recipient.Country.Name}
                                                                                    </a>
                                                                                </div>
                                                                               
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-lg-4">
                                                            <div className="kt-portlet kt-portlet--height-fluid">
                                                                <div className="kt-portlet__body">
                                                                    <div className="kt-widget kt-widget--user-profile-2">
                                                                     
                                                                        <div className="kt-widget__body">
                                                                            <div className="kt-widget__info">
                                                                                <h3 className="kt-widget__desc" style={{ "color": "#000", textTransform: "uppercase", textAlign: "center", fontWeight: "bold" }}>
                                                                                    Payment
                                                                                </h3>
                                                                            </div>
                                                                            <div className="kt-widget__item">
                                                                                <div className="kt-widget__contact">
                                                                                    <span className="kt-widget__label">
                                                                                        Name:
                                                                                    </span>
                                                                                    <a
                                                                                        href="#"
                                                                                        className="kt-widget__data"
                                                                                    >
                                                                                        {`${c.Billing.FirstName} ${c.Billing.LastName}`}
                                                                                    </a>
                                                                                </div>
                                                                                <div className="kt-widget__contact">
                                                                                    <span className="kt-widget__label">
                                                                                        Phone:
                                                                                    </span>
                                                                                    <a
                                                                                        href="#"
                                                                                        className="kt-widget__data"
                                                                                    >

                                                                                        {c.Billing.PhoneNumber}
                                                                                    </a>
                                                                                </div>
                                                                                <div className="kt-widget__contact">
                                                                                    <span className="kt-widget__label">
                                                                                        Address Line:
                                                                                    </span>
                                                                                    <a
                                                                                        href="#"
                                                                                        className="kt-widget__data"
                                                                                    >

                                                                                        {c.Billing.Street1}
                                                                                    </a>
                                                                                </div>
                                                                                <div className="kt-widget__contact">
                                                                                    <span className="kt-widget__label">
                                                                                        City:
                                                                                    </span>
                                                                                    <a
                                                                                        href="#"
                                                                                        className="kt-widget__data"
                                                                                    >

                                                                                        {c.Billing.City}
                                                                                    </a>
                                                                                </div>
                                                                                <div className="kt-widget__contact">
                                                                                    <span className="kt-widget__label">
                                                                                        State:
                                                                                    </span>
                                                                                    <a
                                                                                        href="#"
                                                                                        className="kt-widget__data"
                                                                                    >

                                                                                        {c.Billing.State.Name}
                                                                                    </a>
                                                                                </div>
                                                                                <div className="kt-widget__contact">
                                                                                    <span className="kt-widget__label">
                                                                                        Zip Code:
                                                                                    </span>
                                                                                    <a
                                                                                        href="#"
                                                                                        className="kt-widget__data"
                                                                                    >

                                                                                        {c.Billing.ZipCode}
                                                                                    </a>
                                                                                </div>
                                                                                <div className="kt-widget__contact">
                                                                                    <span className="kt-widget__label">
                                                                                        Country:
                                                                                    </span>
                                                                                    <a
                                                                                        href="#"
                                                                                        className="kt-widget__data"
                                                                                    >

                                                                                        {c.Billing.Country.Name}
                                                                                    </a>
                                                                                </div>
                                                                                
                                                                                <div className="kt-widget__contact">
                                                                                    <span className="kt-widget__label">
                                                                                        Payment Option:
                                                                                    </span>
                                                                                    <a
                                                                                        href="#"
                                                                                        className="kt-widget__data"
                                                                                    >

                                                                                        {c.PaymentMethod == 0 ? "COD" : "Credit Card" }
                                                                                    </a>
                                                                                </div>
                                                                                <div className="kt-widget__contact">
                                                                                    <span className="kt-widget__label">
                                                                                        Payment Status:
                                                                                    </span>
                                                                                    <a
                                                                                        href="#"
                                                                                        className="kt-widget__data"
                                                                                    >

                                                                                        {payment_status(c.PaymentStatus)}
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                      </React.Fragment>
                                    );
                                  })}
                                </tbody>
                              </table>
                            </div>
                          </div>
                          <div className="row">
                            <div className="col-sm-12 col-md-5">
                              <div
                                className="dataTables_info"
                                id="kt_table_1_info"
                                role="status"
                                aria-live="polite"
                              >
                                   {total[this.context.Language]} {this.state.model.TotalItemCount} {record[this.context.Language]}
                              </div>
                            </div>
                            <div className="col-sm-12 col-md-7 dataTables_pager">
                              <PagedList
                                currentpage={this.state.model.CurrentPage}
                                pagesize={this.state.model.PageSize}
                                totalitemcount={this.state.model.TotalItemCount}
                                totalpagecount={this.state.model.TotalPageCount}
                                ajaxcallback={this.toPage.bind(this)}
                              />
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </React.Fragment>
        ) : (
          <div />
        )}
      </React.Fragment>
    );
  }
}

OrderList.contextType = AppContext;
export default OrderList;
