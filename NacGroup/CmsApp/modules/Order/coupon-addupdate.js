import React, { Component } from "react";
import { Link } from "react-router-dom";
import CouponModel from "./models/coupon-model";
import { GLOBAL_ERROR_MESSAGE, ERROR, SUCCESS } from "../../constants/message";
import DateTimePicker from "../../components/datetimepicker";
import moment from "moment";

var apiurl = "/cms/api/coupon";
import {
    globalErrorMessage,
    error,
    back,
    save,
    success,
    add,
    remove,
    total,
    record,
    page,
    cDate,
    lDate,
    recordPerPage,
    status,
    se_status,
    waiting,
    paid,
    processing,
    s_delivery,
    complete,
    cancel,
    f_date,
    t_date
} from "../../constants/message";
import {
    o_his,
    o_num,
    o_date,
    c_name,
    c_address,
    c_phone,
    add_coupon,
    coupon_info,
    value,
    c_value,
    value_type,
    percent,
    money,
    isone,
    active,
    c_date,
    s_date,
    se_s_date,
    e_date,
    se_e_date,
    c_code,
    promotion,
    c_list,
    update_coupon
} from "./models/orderstaticmessage";
import AppContext from "../../components/app-context";
class CouponAddUpdate extends Component {
  constructor(props,context) {
    super(props,context);
    var that = this;

    //action để nhận biết hiện đang add hay update
    var action = null;
    if (document.location.href.indexOf("/admin/coupon/add") >= 0) {
      action = "add";
    } else if (document.location.href.indexOf("/admin/coupon/update") >= 0) {
      action = "update";
    }

    that.state = {
      model: new CouponModel(),
      ex: {
        Title: null,
        Action: action
      }
    };
    that.setState(that.state);

    if (action === "update") {
      var id = this.props.match.params.id;
      KTApp.blockPage();
      $.ajax({
        url: apiurl + "/getupdate",
        type: "GET",
        dataType: "json",
        contentType: "application/json",
        data: { id },
        success: response => {
          KTApp.unblockPage();
          if (response.status == "success") {
              that.state.model = response.data;
              that.state.ex.Title = update_coupon[this.context.Language];
            document.title = that.state.ex.Title;
            that.setState(that.state);
          } else {
              toastr["error"](response.message, error[this.context.Language]);
          }
        },
        error: function(er) {
            KTApp.unblockPage();
            toastr["error"](globalErrorMessage[this.context.Language], error[this.context.Language]);
        }
      });
    } else {
        that.state.ex.Title = add_coupon[this.context.Language];
      document.title = that.state.ex.Title;
      that.setState(that.state);
    }
  }

  submitForm() {
    var that = this;
    KTApp.blockPage();
    $.ajax({
      url: apiurl + "/" + that.state.ex.Action,
      type: "POST",
      dataType: "json",
      contentType: "application/json",
      data: JSON.stringify(that.state.model),
      success: response => {
        KTApp.unblockPage();
        if (response.status == "success") {
          swal.fire({
            title: success[this.context.Language],
            text: response.message,
            type: "success",
            onClose: () => {
              that.props.history.push("/admin/emptypage");
              that.props.history.replace({
                pathname: that.props.location.pathname
              });
            }
          });
        } else {
            toastr["error"](response.message, error[this.context.Language]);
        }
      },
      error: function(er) {
        KTApp.unblockPage();
          toastr["error"](globalErrorMessage[this.context.Language], error[this.context.Language]);
      }
    });
  }

  componentWillMount() {
    $("#cssloading").html(
      ` <link href="/adminstatics/global/plugins/flatpickr/css/flatpickr.min.css" rel="stylesheet" type="text/css" />`
    );
    $("#scriptloading").html(
      `<script src="/adminstatics/global/plugins/flatpickr/js/flatpickr.min.js" type="text/javascript"></script>`
    );
  }

  componentDidMount() {
    document.title = this.state.ex.Title;
    //Active menu
    $("#kt_aside_menu .kt-menu__item").removeClass("kt-menu__item--active");
    $("#kt_aside_menu .kt-menu__item[data-id='promotion']").addClass(
      "kt-menu__item--active"
    );
    $("#kt_aside_menu .kt-menu__item[data-id='coupon-list']").addClass(
      "kt-menu__item--active"
    );
  }

  componentWillUnmount() {
    $("#scriptloading").html("");
  }
  render() {
    return (
      <React.Fragment>
        {this.state && this.state.model ? (
          <React.Fragment>
            <div className="kt-subheader kt-grid__item" id="kt_subheader">
              <div className="kt-subheader__main">
                <div className="kt-subheader__breadcrumbs">
                  <Link
                    to="/admin/dashboard"
                    className="kt-subheader__breadcrumbs-home"
                  >
                    <i className="fa fa-home" />
                  </Link>
                  <span className="kt-subheader__breadcrumbs-separator" />
                  <span className="kt-subheader__breadcrumbs-link">
                                    {promotion[this.context.Language]}
                  </span>
                  <span className="kt-subheader__breadcrumbs-separator" />
                  <Link
                    to="/admin/coupon"
                    className="kt-subheader__breadcrumbs-link"
                  >
                                    {c_list[this.context.Language]}
                  </Link>
                  <span className="kt-subheader__breadcrumbs-separator" />
                  <span className="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
                    {this.state.ex.Title}
                  </span>
                </div>
              </div>
              <div className="kt-subheader__toolbar">
                <div className="kt-subheader__wrapper">
                  <Link to="/admin/coupon" className="btn btn-secondary">
                    <i className="fa fa-chevron-left" />  {back[this.context.Language]}
                  </Link>
                  <a
                    href="javascript:;"
                    className="btn btn-primary"
                    onClick={() => {
                      this.submitForm();
                    }}
                  >
                    <i className="fa fa-save"></i>  {save[this.context.Language]}
                  </a>
                </div>
              </div>
            </div>
            <div
              className="kt-content kt-grid__item kt-grid__item--fluid"
              id="kt_content"
            >
              <div className="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                <div className="row">
                  <div className="col-lg-8">
                    <div className="kt-portlet kt-portlet--mobile">
                      <div className="kt-portlet__head kt-portlet__head--lg">
                        <div className="kt-portlet__head-label">
                          <h3 className="kt-portlet__head-title">
                             {coupon_info[this.context.Language]}
                          </h3>
                        </div>
                      </div>

                      <div className="kt-portlet__body">
                        <div className="form-group">
                          <label className="control-label">{c_code[this.context.Language]}</label>
                          <input
                            className="form-control"
                            value={this.state.model.CouponCode}
                            placeholder={c_code[this.context.Language]}
                            onChange={e => {
                              this.state.model.CouponCode = e.target.value;
                              this.setState(this.state);
                            }}
                          />
                        </div>
                        <div className="form-group">
                          <label className="control-label">{value[this.context.Language]}</label>
                          <input
                            type="number"
                            className="form-control"
                            value={this.state.model.Value}
                            placeholder={c_value[this.context.Language]}
                            onChange={e => {
                              this.state.model.Value =
                                e.target.value && !isNaN(e.target.value)
                                  ? Number(e.target.value)
                                  : null;
                              this.setState(this.state);
                            }}
                          />
                        </div>
                        <div className="form-group">
                          <label className="control-label">{value_type[this.context.Language]}</label>
                          <select
                            className="form-control"
                            value={this.state.model.ValueType}
                            onChange={e => {
                              this.state.model.ValueType =
                                e.target.value && !isNaN(e.target.value)
                                  ? Number(e.target.value)
                                  : 0;
                              this.setState(this.state);
                            }}
                          >
                            <option value={0}>{percent[this.context.Language]} (%)</option>
                            <option value={1}>{money[this.context.Language]} ($)</option>
                          </select>
                        </div>
                        <div className="form-group">
                          <label className="kt-checkbox kt-checkbox-brand">
                            <input
                              type="checkbox"
                              checked={this.state.model.IsOneTimeUse}
                              value={this.state.model.IsOneTimeUse}
                              onChange={e => {
                                this.state.model.IsOneTimeUse =
                                  e.target.checked;
                                this.setState(this.state);
                              }}
                            />
                            {isone[this.context.Language]}
                            <span />
                          </label>
                        </div>
                        <div className="form-group">
                          <label className="kt-checkbox kt-checkbox-brand">
                            <input
                              type="checkbox"
                              checked={this.state.model.IsActive}
                              value={this.state.model.IsActive}
                              onChange={e => {
                                this.state.model.IsActive = e.target.checked;
                                this.setState(this.state);
                              }}
                            />
                           {active[this.context.Language]}
                            <span />
                          </label>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-4">
                    <div className="kt-portlet kt-portlet--mobile">
                      <div className="kt-portlet__head kt-portlet__head--lg">
                        <div className="kt-portlet__head-label">
                          <h3 className="kt-portlet__head-title">
                            {" "}
                            {c_date[this.context.Language]}
                          </h3>
                        </div>
                      </div>

                      <div className="kt-portlet__body">
                        <div className="form-group">
                          <label className="control-label"> {s_date[this.context.Language]}</label>
                          <span className="required"> * </span>
                          <div className="input-icon right">
                            <DateTimePicker
                              value={moment
                                .utc(this.state.model.StartDate)
                                .format("MM/DD/YYYY hh:mm A")}
                              options={{
                                enableTime: true,
                                dateFormat: "m/d/Y G:S K",
                                disableMobile: true
                              }}
                              onChange={(selectedDates, dateStr, instance) => {
                                this.state.model.StartDate = moment
                                  .utc(dateStr)
                                  .toDate();
                                this.setState(this.state);
                              }}
                              placeholder={se_s_date[this.context.Language]}
                            />
                          </div>
                        </div>
                        <div className="form-group">
                          <label className="control-label">{e_date[this.context.Language]}</label>
                          <span className="required"> * </span>
                          <div className="input-icon right">
                            <DateTimePicker
                              value={moment
                                .utc(this.state.model.ExpiredDate)
                                .format("MM/DD/YYYY hh:mm A")}
                              options={{
                                enableTime: true,
                                dateFormat: "m/d/Y G:S K",
                                disableMobile: true
                              }}
                              onChange={(selectedDates, dateStr, instance) => {
                                this.state.model.ExpiredDate = moment
                                  .utc(dateStr)
                                  .toDate();
                                this.setState(this.state);
                              }}
                              placeholder={se_e_date[this.context.Language]}
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </React.Fragment>
        ) : (
          <div />
        )}
      </React.Fragment>
    );
  }
}
CouponAddUpdate.contextType = AppContext;
export default CouponAddUpdate;
