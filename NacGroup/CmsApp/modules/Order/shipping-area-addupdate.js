﻿import React, { Component } from "react";
import ShippingByAreaCmsModel from "./models/shippingbyarea-cmsmodel";
import DistrictModel from "./models/district-model";
import { Link } from "react-router-dom";
import ReactSelect from "react-select";
import {
    globalErrorMessage,
    error,
    back,
    save,
    success,
    add,
    remove,
    total,
    record,
    page,
    cDate,
    lDate,
    recordPerPage,
    status,
    se_status,
    waiting,
    paid,
    processing,
    s_delivery,
    complete,
    cancel,
    f_date,
    t_date
} from "../../constants/message";
import {
    ship_setting,
    a_list,
    add_shipping_by_district,
    update_shipping_by_district,
    add_shipping_by_bill,
    update_shipping_by_bill,
    name,
    shipping_fee,
    information,
    region,
    se_region,
    apply,
    ship_order,
    price_from,
    price_to,
    action,
    active,
    district
} from "./models/orderstaticmessage";
import AppContext from "../../components/app-context";


const apiurl = "/cms/api/shippingbyarea";
class ShippingAreaAddUpdate extends Component {
    constructor(props, context) {
        super(props, context);
        var action = null;
        if (document.location.href.indexOf("/admin/feeshipbyarea/add") >= 0) {
            action = "add";
        } else if (
            document.location.href.indexOf("/admin/feeshipbyarea/update") >= 0
        ) {
            action = "update";
        }
        this.state = {
            model: new ShippingByAreaCmsModel(),
            ex: {
                Title: null,
                Action: action,
                City: null,
                CityListDefault: [],
                CityList: [],
                DistrictListDefault: [],
                DistrictList: []
            }
        };
        var that = this;


        if (action === "update") {
            //console.log(this.props);

            that.state.ex.Title = "Update Shipping Fee By District";
            var id = that.props.match.params.id;
            KTApp.blockPage();
            $.ajax({
                url: apiurl + "/" + that.state.ex.Action,
                type: "GET",
                dataType: "json",
                contentType: "application/json",
                data: { id },
                success: response => {
                    KTApp.unblockPage();
                    toastr.clear();
                    if (response.status == "success") {
                        that.state.model = response.data;
                        document.title = that.state.ex.Title;
                        that.setState(that.state);
                    } else {
                        toastr["error"](response.message, error[this.context.Language]);
                    }
                },
                error: function (er) {
                    KTApp.unblockPage();
                    toastr.clear();
                    toastr["error"](globalErrorMessage[this.context.Language], error[this.context.Language]);
                }
            });
        } else {
            this.state.ex.Title = add_shipping_by_district[this.context.Language];
            this.setState(this.state);
        }
    }
    handleChangeCity(value) {
        var that = this;
        KTApp.blockPage();
        if (value == null || value == "") {
            that.state.model.City.Id = null;
            that.state.model.City.Code = null;
            that.state.model.City.Name = null;
            that.state.model.City.Level = null;
            that.state.model.City.DistrictList = [];

            KTApp.unblockPage();
            return -1;
        };
        that.state.ex.City = that.state.ex.CityListDefault.find(e => { return e.Code == value });
        that.state.ex.DistrictList = that.state.ex.DistrictListDefault.filter(e => { return e.City.Code == value });

        if (that.state.ex.DistrictList.length > 0) {
            KTApp.unblockPage();
        }
        if (that.state.ex.City != null) {
            KTApp.unblockPage();
            that.state.model.City.Id = that.state.ex.City.Id;
            that.state.model.City.Code = that.state.ex.City.Code;
            that.state.model.City.Name = that.state.ex.City.Name;
            that.state.model.City.Level = that.state.ex.City.Level;
            that.state.model.City.DistrictList = that.state.ex.DistrictList ? that.state.ex.DistrictList.map(
                e => { return { Id: e.Id, Name: e.Name, Code: e.Code, Level: e.Level, Price: 0, IsActive: true } })
                : [];
        }
        that.setState(that.state);
    }
    componentWillMount() {
        var that = this;
        $("#scriptloading").html(
            `<script src="/adminstatics/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>`
        );
        $.get("/cms/api/shippingbyarea/getcity", function (response) {
            that.state.ex.CityListDefault = response;
            that.state.ex.CityList = response ? response.map(e => { return { label: e.Name, value: e.Code } }) : [];

            that.setState(that.state);
        });
        $.get("/cms/api/shippingbyarea/getdistrict", function (response) {
            that.state.ex.DistrictListDefault = response;
            that.setState(that.state);
        });
    }

    componentDidMount() {
        document.title = this.state.ex.Title;
        $("#kt_aside_menu .kt-menu__item").removeClass("kt-menu__item--active");
        $("#kt_aside_menu .kt-menu__item[data-id='shipping-setting']").addClass(
            "kt-menu__item--active"
        );
        $("#kt_aside_menu .kt-menu__item[data-id='feeship-bydistrict']").addClass(
            "kt-menu__item--active"
        );
    }

    componentWillUnmount() {
        $("#scriptloading").html("");
    }

    // this.setState(this.state);
    submitForm() {
        var that = this;
        //var id = that.props.match.params.id;

        KTApp.blockPage();
        $.ajax({
            url: apiurl + "/" + that.state.ex.Action,
            type: "POST",
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify(that.state.model),
            success: response => {
                KTApp.unblockPage();
                if (response.status == "success") {
                    swal.fire({
                        title: success[this.context.Language],
                        text: response.message,
                        type: "success",
                        onClose: () => {
                            that.props.history.push("/admin/emptypage");
                            that.props.history.replace({
                                pathname: that.props.location.pathname
                            });
                        }
                    });
                } else {
                    toastr["error"](response.message, error[this.context.Language]);
                }
            },
            error: function (er) {
                KTApp.unblockPage();
                toastr["error"](globalErrorMessage[this.context.Language], error[this.context.Language]);
            }
        });
    }

    render() {
        return (
            <React.Fragment>
                {this.state.ex && this.state.model ? (
                    <React.Fragment>
                        <div className="kt-subheader kt-grid__item" id="kt_subheader">
                            <div className="kt-subheader__main">
                                <div className="kt-subheader__breadcrumbs">
                                    <Link
                                        to="/admin/dashboard"
                                        className="kt-subheader__breadcrumbs-home"
                                    >
                                        <i className="fa fa-home" />
                                    </Link>
                                    <span className="kt-subheader__breadcrumbs-separator" />
                                    <span className="kt-subheader__breadcrumbs-link">
                                        {ship_setting[this.context.Language]}
                                    </span>
                                    <span className="kt-subheader__breadcrumbs-separator" />
                                    <span className="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
                                        {this.state.ex.Title}
                                    </span>
                                </div>
                            </div>
                            <div className="kt-subheader__toolbar">
                                <div className="kt-subheader__wrapper">
                                    <Link
                                        to="/admin/feeshipbyarea"
                                        className="btn btn-secondary"
                                    >
                                        <i className="fa fa-chevron-left" /> {back[this.context.Language]}
                                    </Link>
                                    <a
                                        href="javascript:;"
                                        className="btn btn-primary"
                                        onClick={() => {
                                            this.submitForm();
                                        }}
                                    >
                                        <i className="fa fa-save" /> {save[this.context.Language]}
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div
                            className="kt-content kt-grid__item kt-grid__item--fluid"
                            id="kt_content"
                        >
                            <div className="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                                <div className="row">
                                    <div className="col-lg-12">
                                        <div className="kt-portlet kt-portlet--mobile">
                                            <div className="kt-portlet__head kt-portlet__head--lg">
                                                <div className="kt-portlet__head-label">
                                                    <h3 className="kt-portlet__head-title">
                                                        {information[this.context.Language]}
                                                    </h3>
                                                </div>
                                            </div>

                                            <div className="kt-portlet__body">
                                                <div className="form-group">
                                                    <label className="control-label">{region[this.context.Language]}</label>
                                                    <ReactSelect
                                                        isClearable={true}
                                                        isSearchable={true}
                                                        isDisabled={this.state.ex.Action == "update" || this.state.model.IsDefault ? true : false}
                                                        options={this.state.ex.CityList}
                                                        value={
                                                            this.state.ex.CityList != null
                                                                ? this.state.ex.CityList.filter(
                                                                    option =>
                                                                        option.value === this.state.model.City.Code
                                                                )
                                                                : ""
                                                        }
                                                        placeholder={se_region[this.context.Language]}

                                                        onChange={e => {
                                                            const value = e === null ? "" : e.value;
                                                            this.state.model.City.Code = value;
                                                            this.handleChangeCity(value);
                                                            this.setState(this.state);
                                                        }}
                                                    />
                                                </div>
                                                <div className={this.state.ex.Action == "update" ? "kt-hidden" : "form-group"}>
                                                    <label className="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                                        <input
                                                            type="checkbox"
                                                            className="kt-checkable"
                                                            value={this.state.model.IsDefault}
                                                            checked={this.state.model.IsDefault}
                                                            onChange={e => {
                                                                this.state.model.IsDefault = e.target.checked;
                                                                this.setState(this.state);
                                                            }}

                                                        />
                                                        <span></span>
                                                    </label>
                                                    <span className="label" style={{ "position": "absolute", top: 0 }}>{apply[this.context.Language]}</span>
                                                </div>
                                                <div className="form-group">
                                                    <label className="control-label">{shipping_fee[this.context.Language]} *</label>
                                                    <input
                                                        className="form-control"
                                                        value={this.state.model.Price}
                                                        type="number"
                                                        min="0"
                                                        placeholder={shipping_fee[this.context.Language]}
                                                        onChange={e => {

                                                            this.state.model.Price = e.target.value;
                                                            this.setState(this.state);
                                                        }}
                                                        onBlur={e => {
                                                            if (this.state.model.City.DistrictList.length > 0) {
                                                                this.state.model.City.DistrictList.forEach(i => {
                                                                    i.Price = parseInt(e.target.value);
                                                                });

                                                                this.setState(this.state);
                                                            }
                                                        }}
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className={this.state.model.IsDefault ? `kt-hidden` : `col-lg-12`}>
                                        <div className="kt-portlet kt-portlet--mobile">
                                            <div className="kt-portlet__head kt-portlet__head--lg">
                                                <div className="kt-portlet__head-label">
                                                    <h3 className="kt-portlet__head-title">
                                                        {district[this.context.Language]}
                                                    </h3>
                                                </div>
                                            </div>
                                            <div className="kt-portlet__body">
                                                <div
                                                    id="kt_table_1_wrapper"
                                                    className="dataTables_wrapper dt-bootstrap4">
                                                    <div className="row">
                                                        <div className="col-sm-12">
                                                            <table className="table table-striped- table-bordered table-hover table-checkable dataTable no-footer dtr-inline">
                                                                <thead>
                                                                    <tr role="row">
                                                                        <th>{name[this.context.Language]}</th>
                                                                        <th>{shipping_fee[this.context.Language]}</th>
                                                                        <th>{active[this.context.Language]}</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    {this.state.model.City.DistrictList ? this.state.model.City.DistrictList.map((c, index) => {
                                                                        return (
                                                                            <tr>
                                                                                <td>
                                                                                    {c.Name}
                                                                                </td>
                                                                                <td>
                                                                                    <input
                                                                                        className="form-control"
                                                                                        value={c.Price}
                                                                                        type="text"
                                                                                        min="0"
                                                                                        placeholder="Phí ship"
                                                                                        onChange={e => {
                                                                                            c.Price = e.target.value;
                                                                                            this.setState(this.state);
                                                                                        }}
                                                                                    />
                                                                                </td>
                                                                                <td>
                                                                                    <label className="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                                                                        <input
                                                                                            type="checkbox"
                                                                                            className="kt-checkable"
                                                                                            value={c.IsActive}
                                                                                            checked={c.IsActive}
                                                                                            onChange={ev => {
                                                                                                c.IsActive = !c.IsActive;
                                                                                                this.setState(this.state);
                                                                                            }}
                                                                                        />
                                                                                        <span></span>
                                                                                    </label>
                                                                                </td>
                                                                            </tr>
                                                                        );
                                                                    }) : ""}
                                                                </tbody>
                                                            </table>

                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </React.Fragment>
                ) : (
                        <div />
                    )}
            </React.Fragment>
        );
    }
}
ShippingAreaAddUpdate.contextType = AppContext;
export default ShippingAreaAddUpdate;
