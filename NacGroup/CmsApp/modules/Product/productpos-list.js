import React, { Component } from "react";
import { Link } from "react-router-dom";
import uploadPlugin from "../../plugins/upload-plugin";
import CkEditor from "../../components/ckeditor";
import {
    globalErrorMessage,
    error,
    success,
    total,
    record,
    clearcache,
} from "../../constants/message";
import {
    product_list,
    name,
    avatar,
    description,
    pos_products,
    update_description,
} from "./models/productstaticmessage";
import AppContext from "../../components/app-context";
const apiurl = "/cms/api/productgpos";

class ProductPosList extends Component {
    constructor(props, context) {
    super(props,context);
    var that = this;
    that.state = {
      model: null,
      ex: {
          Title: product_list[this.context.Language]
      }
    };
    document.title = that.state.ex.Title;
    KTApp.blockPage();
    $.ajax({
      url: apiurl,
      type: "GET",
      dataType: "json",
      contentType: "application/json",
      success: response => {
        KTApp.unblockPage();
        toastr.clear();
        if (response.status == "success") {
          that.state.model = response.data;
          that.setState(that.state);
        } else {
          toastr["error"](response.message, error[this.context.Language]);
        }
      },
      error: function(er) {
        KTApp.unblockPage();
        toastr.clear();
          toastr["error"](globalErrorMessage[this.context.Language], error[this.context.Language]);
      }
    });
  }

  updateCustomize(product, name) {
    var that = this;
    KTApp.blockPage();
    $.ajax({
      url: `${apiurl}/UpdateCustomize`,
      type: "POST",
      dataType: "json",
      contentType: "application/json",
      data: JSON.stringify({
        model: product,
        name: name
      }),
      success: response => {
        KTApp.unblockPage();
        toastr.clear();
        if (response.status == "success") {
          that.setState(that.state);
          toastr["success"]("Update Sucessfully", success[this.context.Language]);
        } else {
          toastr["error"](response.message, error[this.context.Language]);
        }
      },
      error: function(er) {
        KTApp.unblockPage();
        toastr.clear();
        toastr["error"](globalErrorMessage[this.context.Language], error[this.context.Language]);
      }
    });
  }

  clearcache() {
    KTApp.blockPage();
    $.ajax({
      url: apiurl + "/clearcache",
      type: "POST",
      dataType: "json",
      contentType: "application/json",
      success: response => {
        location.reload();
      },
      error: function(er) {
        KTApp.unblockPage();
        toastr.clear();
        toastr["error"](globalErrorMessage[this.context.Language], error[this.context.Language]);
      }
    });
  }

  componentWillMount() {
    $("#scriptloading").html(
      `<script src="/adminstatics/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>`
    );
  }
  componentDidMount() {
    document.title = this.state.ex.Title;
    $("#kt_aside_menu .kt-menu__item").removeClass("kt-menu__item--active");
    $("#kt_aside_menu .kt-menu__item[data-id='san-pham-pos']").addClass(
      "kt-menu__item--active"
    );
    $(
      "#kt_aside_menu .kt-menu__item[data-id='danh-sach-san-pham-pos']"
    ).addClass("kt-menu__item--active");
  }

  componentWillUnmount() {
    $("#scriptloading").html("");
  }
  render() {
    return (
      <React.Fragment>
        {this.state && this.state.model ? (
          <React.Fragment>
            <div className="kt-subheader kt-grid__item" id="kt_subheader">
              <div className="kt-subheader__main">
                <div className="kt-subheader__breadcrumbs">
                  <Link
                    to="/admin/dashboard"
                    className="kt-subheader__breadcrumbs-home"
                  >
                    <i className="fa fa-home" />
                  </Link>
                  <span className="kt-subheader__breadcrumbs-separator" />
                  <span className="kt-subheader__breadcrumbs-link">
                    {pos_products[this.context.Language]}
                  </span>
                  <span className="kt-subheader__breadcrumbs-separator" />
                  <span className="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
                    {this.state.ex.Title}
                  </span>
                </div>
              </div>
              <div className="kt-subheader__toolbar">
                <a
                  href="javascript:;"
                  onClick={e => {
                    this.clearcache();
                  }}
                  className="btn btn-primary"
                >
                  <i className="fa fa-sync-alt"></i>{clearcache[this.context.Language]}
                </a>
              </div>
            </div>
            <div
              className="kt-content kt-grid__item kt-grid__item--fluid"
              id="kt_content"
            >
              <div className="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                <div className="row">
                  <div className="col-lg-12">
                    <div className="kt-portlet kt-portlet--mobile">
                      <div className="kt-portlet__head kt-portlet__head--lg">
                        <div className="kt-portlet__head-label">
                          <h3 className="kt-portlet__head-title">
                            {this.state.ex.Title}
                          </h3>
                        </div>
                      </div>

                      <div className="kt-portlet__body">
                        <div
                          id="kt_table_1_wrapper"
                          className="dataTables_wrapper dt-bootstrap4"
                        >
                          <div className="row">
                            <div className="col-sm-12">
                              <table className="table table-striped- table-bordered table-hover table-checkable dataTable no-footer dtr-inline table-responsive">
                                <thead>
                                  <tr role="row">
                                    <th style={{ width: "300px" }}>{name[this.context.Language]}</th>
                                    <th>{avatar[this.context.Language]}</th>
                                    <th>{description[this.context.Language]}</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  {this.state.model.map((pro, index) => {
                                    return (
                                      <tr>
                                        <td>{pro.Name}</td>
                                        <td>
                                          <div
                                            className="form-group"
                                            style={{ position: "relative" }}
                                          >
                                            <input
                                              type="file"
                                              className="form-control"
                                              style={{
                                                position: "absolute",
                                                zIndex: 99,
                                                top: 0,
                                                width: "100%",
                                                height: "100%",
                                                opacity: 0
                                              }}
                                              onChange={e => {
                                                var that = this;
                                                uploadPlugin.UpdateImage(
                                                  e,
                                                  files => {
                                                    pro.Avatar = files[0];
                                                    this.updateCustomize(
                                                      pro,
                                                      "Avatar"
                                                    );
                                                  }
                                                );
                                              }}
                                            />
                                            <img
                                              src={
                                                pro.Avatar
                                                  ? pro.Avatar
                                                  : "/adminstatics/global/img/no-image.png"
                                              }
                                              style={{ height: "100px" }}
                                            />
                                          </div>
                                        </td>
                                        <td>
                                          <div
                                            dangerouslySetInnerHTML={{
                                              __html: pro.Description
                                            }}
                                          />
                                          <button
                                            type="button"
                                            className="btn btn-primary"
                                            onClick={e => {
                                              $(e.target)
                                                .parent()
                                                .find(".ck")
                                                .slideToggle();
                                            }}
                                          >
                                            {update_description[this.context.Language]}
                                          </button>
                                          <div
                                            className="ck"
                                            style={{ display: "none" }}
                                          >
                                            <CkEditor
                                              value={pro.Description}
                                              onChange={e => {
                                                pro.Description = e;
                                                this.setState(this.state);
                                              }}
                                            />
                                            <button
                                              className="btn btn-success"
                                              onClick={e => {
                                                this.updateCustomize(
                                                  pro,
                                                  "Description"
                                                );
                                              }}
                                            >
                                              Save
                                            </button>
                                          </div>
                                        </td>
                                      </tr>
                                    );
                                  })}
                                </tbody>
                              </table>
                            </div>
                          </div>
                          <div className="row">
                            <div className="col-sm-12 col-md-5">
                              <div
                                className="dataTables_info"
                                id="kt_table_1_info"
                                role="status"
                                aria-live="polite"
                              >
                                {total[this.context.Language]} {this.state.model.length} {record[this.context.Language]}
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </React.Fragment>
        ) : (
          <div />
        )}
      </React.Fragment>
    );
  }
}
ProductPosList.contextType = AppContext;
export default ProductPosList;
