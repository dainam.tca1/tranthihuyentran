import React, { Component } from "react";
import { Link } from "react-router-dom";
import ProductCmsModel from "./models/product-cmsmodel";
import uploadPlugin from "../../plugins/upload-plugin";
import DateTimePicker from "../../components/datetimepicker";
import moment from "moment";
import {
    globalErrorMessage,
    error,
    success,
    back,
    add,
    save,
    close,
    remove,
    total,
    record,
    page,
    cDate,
    lDate,
    recordPerPage,
    order,
    sort,
    price,
    are_you_sure,
    data_recover,
    yes_de,
    no_ca
} from "../../constants/message";
import {
    brand,
    attr_name,
    attr_value,
    attr_group,
    sku_code,
    product,
    product_list,
    product_name,
    category,
    se_category,
    add_attr_group,
    infor,
    classify,
    main_cate,
    related_cate,
    se_main_cate,
    se_related_cate,
    se_brand,
    name,
    description,
    avatar,
    value,
    seo_title,
    seo_des,
    seo_key,
    add_product,
    update_product,
    stock_quantity,
    special_price,
    add_image,
    se_image,
    short_des,
    add_attr,
    se_attr_value,
    se_attr_name,
    attr_group_info,
    se_attr_group_info,
    specification,
    add_sku,
    update_more,
    active,
    action,
    ingredient,
    benefits,
    width,
    height,
    dimension_unit,
    weight,
    weight_unit,
    sale_tax,
    depth,
    from_date,
    to_date,
    is_inventory,
    length,
    shipping_category,
    label_category,
    sort_sku,
    se_product_type,
    product_type
} from "./models/productstaticmessage";
import AppContext from "../../components/app-context";
import ReactSelect from "react-select";

//ckeditor
import CkEditor from "../../components/ckeditor";

var apiurl = "/cms/api/product";
var apicateurl = "/cms/api/productcategory";
var apilabel = "/cms/api/label";
var apiattributegroupurl = "/cms/api/productattributegroup";
var apiattributeurl = "/cms/api/productattribute";
var apiattributevalueurl = "/cms/api/productattributevalue";
var apibrandurl = "/cms/api/brand";

class ProductAddUpdate extends Component {
    constructor(props, context) {
        super(props, context);
        var action = null;
        this.ProductListBox = React.createRef();
        if (document.location.href.indexOf("/admin/product/add") >= 0) {
            action = "add";
        } else if (document.location.href.indexOf("/admin/product/update") >= 0) {
            action = "update";
        }
        this.state = {
            model: new ProductCmsModel(),
            TempSkus: [],
            ex: {
                Title: null,
                Action: action,
                CategoryList: [],
                CategoryListDropdownList: [],
                MainCategoryId: null,
                RelatedCategoryId: [],
                ProductAttributeGroupList: [],
                ProductAttributeList: [],
                ProductAttributeValueList: [],
                ProductAttributeGroupListDropdown: [],
                ProductAttributeListDropdown: [],
                ProductAttributeValueListDropdown: [],
                LabelList: [],
                LabelListDropdown: [],
                SkuImageCurrentIndex: 0,
                BrandList: [],
                BrandListDropdown: [],
                ShippingCategoryList: [],
                ProductTypeList: [],
                WeightList: [],
                DimensionList: []
            }
        };
        this.addnewSku();
        var that = this;

        $.get(apicateurl, null, response => {
            KTApp.unblockPage();
            toastr.clear();
            if (response.status === "success") {
                that.state.ex.CategoryList = response.data.Results;
                that.state.ex.CategoryListDropdownList = response.data.Results.map(
                    e => {
                        return {
                            value: e.Id,
                            label: e.Name
                        };
                    }
                );
                $.get(apicateurl + "/getshippingcategorylist", response => {
                    that.state.ex.ShippingCategoryList = response;
                    that.setState(that.state);
                });
                $.get(apiurl + "/GetProductTypeList", response => {
                    that.state.ex.ProductTypeList = response;
                    that.setState(that.state);
                });
                $.get(apiurl + "/GetWeightList", response => {
                    that.state.ex.WeightList = response;
                    that.setState(that.state);
                });
                $.get(apiurl + "/GetDimensionList", response => {
                    that.state.ex.DimensionList = response;
                    that.setState(that.state);
                });
                that.setState(that.state);
            } else {
                toastr["error"](response.message, error[this.context.Language]);
            }
        }).fail(() => {
            KTApp.unblockPage();
            toastr.clear();
            toastr["error"](
                globalErrorMessage[this.context.Language],
                error[this.context.Language]
            );
        });

        $.get(apibrandurl, null, response => {
            KTApp.unblockPage();
            toastr.clear();
            if (response.status === "success") {
                that.state.ex.BrandList = response.data.Results;
                that.state.ex.BrandListDropdown = response.data.Results.map(e => {
                    return {
                        value: e.Id,
                        label: e.Name
                    };
                });
                that.setState(that.state);
            } else {
                toastr["error"](response.message, error[this.context.Language]);
            }
        }).fail(() => {
            KTApp.unblockPage();
            toastr.clear();
            toastr["error"](
                globalErrorMessage[this.context.Language],
                error[this.context.Language]
            );
        });
        $.get(apiattributegroupurl, null, response => {
            KTApp.unblockPage();
            toastr.clear();
            if (response.status === "success") {
                that.state.ex.ProductAttributeGroupList = response.data.Results;
                that.state.ex.ProductAttributeGroupListDropdown = response.data.Results.map(
                    e => {
                        return {
                            value: e.Id,
                            label: e.Name
                        };
                    }
                );
                that.setState(that.state);
            } else {
                toastr["error"](response.message, error[this.context.Language]);
            }
        }).fail(() => {
            KTApp.unblockPage();
            toastr.clear();
            toastr["error"](
                globalErrorMessage[this.context.Language],
                error[this.context.Language]
            );
        });

        $.get(apiattributeurl, null, response => {
            KTApp.unblockPage();
            toastr.clear();
            if (response.status === "success") {
                that.state.ex.ProductAttributeList = response.data.Results;
                that.state.ex.ProductAttributeListDropdown = response.data.Results.map(
                    e => {
                        return {
                            value: e.Id,
                            label: e.Name
                        };
                    }
                );
                that.setState(that.state);
            } else {
                toastr["error"](response.message, error[this.context.Language]);
            }
        }).fail(() => {
            KTApp.unblockPage();
            toastr.clear();
            toastr["error"](
                globalErrorMessage[this.context.Language],
                error[this.context.Language]
            );
        });

        $.get(apiattributevalueurl, null, response => {
            KTApp.unblockPage();
            toastr.clear();
            if (response.status === "success") {
                that.state.ex.ProductAttributeValueList = response.data.Results;
                that.state.ex.ProductAttributeValueListDropdown = response.data.Results.map(
                    e => {
                        return {
                            value: e.Id,
                            label: e.Value
                        };
                    }
                );
                that.setState(that.state);
            } else {
                toastr["error"](response.message, error[this.context.Language]);
            }
        }).fail(() => {
            KTApp.unblockPage();
            toastr.clear();
            toastr["error"](
                globalErrorMessage[this.context.Language],
                error[this.context.Language]
            );
        });
        $.get(apilabel, null, response => {
            KTApp.unblockPage();
            toastr.clear();
            if (response.status === "success") {
                that.state.ex.LabelList = response.data.Results;
                that.state.ex.LabelListDropdown = response.data.Results.map(e => {
                    return {
                        value: e.Id,
                        label: e.Name
                    };
                });
              
                that.setState(that.state);
            } else {
                toastr["error"](response.message, error[this.context.Language]);
            }
        }).fail(() => {
            KTApp.unblockPage();
            toastr.clear();
            toastr["error"](
                globalErrorMessage[this.context.Language],
                error[this.context.Language]
            );
        });
        switch (action) {
            case "update":
                var id = that.props.match.params.id;
                KTApp.blockPage();
                $.ajax({
                    url: apiurl + "/" + that.state.ex.Action,
                    type: "GET",
                    dataType: "json",
                    contentType: "application/json",
                    data: { id: id },
                    success: response => {
                        KTApp.unblockPage();
                        if (response.status === "success") {
                            that.state.model = response.data;
                            that.state.ex.Title = update_product[this.context.Language];
                            document.title = that.state.ex.Title;
                            that.state.ex.MainCategoryId = that.state.ex.CategoryListDropdownList.find(
                                m => m.value == that.state.model.MainCategory.Id
                            );
                            that.state.ex.RelatedCategoryId = that.state.ex.CategoryListDropdownList.filter(
                                m => {
                                    return (
                                        that.state.model.Categories.filter(c => {
                                            return (
                                                c.Id != that.state.model.MainCategory.Id &&
                                                c.Id == m.value
                                            );
                                        }).length > 0
                                    );
                                }
                            );
                            that.setState(that.state);
                        } else {
                            toastr["error"](response.message, error[this.context.Language]);
                        }
                    },
                    error: function (er) {
                        KTApp.unblockPage();
                        toastr["error"](
                            globalErrorMessage[this.context.Language],
                            error[this.context.Language]
                        );
                    }
                });
                break;
            case "add":
                that.state.ex.Title = add_product[this.context.Language];
                document.title = that.state.ex.Title;
                that.setState(that.state);
                break;
        }
    }

    onChangedProductCategory() {
        this.state.model.Categories = [];
        var that = this;
        if (this.state.ex.MainCategoryId) {
            var maincate = this.state.ex.CategoryList.find(
                e => e.Id === this.state.ex.MainCategoryId.value
            );
            if (maincate != null) {
                this.state.model.Categories.push({ ...maincate });
                this.state.model.MainCategory = { ...maincate };
            } else {
                this.state.model.MainCategory = null;
            }
        } else {
            this.state.model.MainCategory = null;
        }
        var relatedCategories = this.state.ex.CategoryList.filter(e => {
            return (
                that.state.ex.RelatedCategoryId.filter(x => x.value === e.Id).length > 0
            );
        });
        this.state.model.Categories = this.state.model.Categories.concat(
            relatedCategories
        );
    }

    onChangeProductName(e) {
        this.state.model.Name = e.target.value;
        this.state.model.MetaTitle = e.target.value;
        this.state.model.Url = e.target.value.cleanUnicode();
        this.state.model.Skus.forEach(sku => {
            this.refreshSkuName(sku);
        });
        this.setState(this.state);
    }

    refreshSkuName(sku) {
        var lastname = "";
        sku.Attributes.forEach((attr, index) => {
            if (attr.Values.length == 0) {
                return -1;
            }
            lastname += (index > 0 ? " - " : " ") + attr.Values[0].Value;
            return 1;
        });
        sku.Name = this.state.model.Name + lastname;
    }

    addnewSku() {
        this.state.model.Skus.push({
            SkuCode: null,
            Name: null,
            Price: 0,
            ListedPrice: 0,
            StockQuantity: 0,
            Sort: this.state.model.Skus.length,
            Attributes: [],
            ImageList: []
        });
    }
    // this.setState(this.state);
    submitForm() {
        var that = this;
        KTApp.blockPage();
        $.ajax({
            url: apiurl + "/" + that.state.ex.Action,
            type: "POST",
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify(that.state.model),
            success: response => {
                KTApp.unblockPage();
                if (response.status == "success") {
                    swal.fire({
                        title: success[this.context.Language],
                        text: response.message,
                        type: "success",
                        onClose: () => {
                            that.props.history.push("/admin/emptypage");
                            that.props.history.replace({
                                pathname: that.props.location.pathname
                            });
                        }
                    });
                } else {
                    toastr["error"](response.message, error[this.context.Language]);
                }
            },
            error: function (er) {
                KTApp.unblockPage();
                toastr["error"](
                    globalErrorMessage[this.context.Language],
                    error[this.context.Language]
                );
            }
        });
    }

    submitFormAfterSort() {
        var that = this;
        that.state.model.Skus = that.state.TempSkus;
        KTApp.blockPage();
        $.ajax({
            url: apiurl + "/" + that.state.ex.Action,
            type: "POST",
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify(that.state.model),
            success: response => {
                KTApp.unblockPage();
                if (response.status == "success") {
                    swal.fire({
                        title: success[this.context.Language],
                        text: response.message,
                        type: "success",
                        onClose: () => {
                            that.props.history.push("/admin/emptypage");
                            that.props.history.replace({
                                pathname: that.props.location.pathname
                            });
                        }
                    });
                } else {
                    toastr["error"](response.message, error[this.context.Language]);
                }
            },
            error: function (er) {
                KTApp.unblockPage();
                toastr["error"](
                    globalErrorMessage[this.context.Language],
                    error[this.context.Language]
                );
            }
        });
    }
    componentWillMount() {
        $("#cssloading").html(
            `<link href="/adminstatics/global/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
            <link href="/adminstatics/global/plugins/flatpickr/css/flatpickr.min.css" rel="stylesheet" type="text/css" />
`
        );
        $("#scriptloading").html(
            `<script src="/adminstatics/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
        <script src="/adminstatics/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
        <script src="/adminstatics/global/plugins/flatpickr/js/flatpickr.min.js" type="text/javascript"></script>
`
        );
    }

    componentDidMount() {
        document.title = this.state.ex.Title;
        $("#kt_aside_menu .kt-menu__item").removeClass("kt-menu__item--active");
        $("#kt_aside_menu .kt-menu__item[data-id='san-pham']").addClass(
            "kt-menu__item--active kt-menu__item--open"
        );
        $("#kt_aside_menu .kt-menu__item[data-id='danh-sach-san-pham']").addClass(
            "kt-menu__item--active"
        );
        var that = this;
        $(that.ProductListBox.current).sortable({
            placeholder: "sortable-placeholder",
            update: (event, ui) => {
                var sortedList = [];
                $(that.ProductListBox.current)
                    .children()
                    .each((index, ele) => {
                        sortedList.push(
                            that.state.TempSkus.find(s => s.SkuCode == $(ele).data("id"))
                        );
                        sortedList[sortedList.length - 1].Sort = index;
                        $($(ele).children()[0]).text(index);
                    });

                that.state.TempSkus = sortedList;
            }
        });
    }

    componentWillUnmount() {
        $("#cssloading").html("");
        $("#scriptloading").html("");
    }
    render() {
        return (
            <React.Fragment>
                {this.state && this.state.model ? (
                    <React.Fragment>
                        <div className="kt-subheader kt-grid__item" id="kt_subheader">
                            <div className="kt-subheader__main">
                                <div className="kt-subheader__breadcrumbs">
                                    <Link
                                        to="/admin/dashboard"
                                        className="kt-subheader__breadcrumbs-home"
                                    >
                                        <i className="fa fa-home" />
                                    </Link>
                                    <span className="kt-subheader__breadcrumbs-separator" />
                                    <span className="kt-subheader__breadcrumbs-link">
                                        {product[this.context.Language]}
                                    </span>
                                    <span className="kt-subheader__breadcrumbs-separator" />
                                    <Link
                                        to="/admin/product"
                                        className="kt-subheader__breadcrumbs-link"
                                    >
                                        {product_list[this.context.Language]}
                                    </Link>
                                    <span className="kt-subheader__breadcrumbs-separator" />
                                    <span className="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
                                        {this.state.ex.Title}
                                    </span>
                                </div>
                            </div>
                            <div className="kt-subheader__toolbar">
                                <div className="kt-subheader__wrapper">
                                    <Link to="/admin/product" className="btn btn-secondary">
                                        <i className="fa fa-chevron-left" />{" "}
                                        {back[this.context.Language]}
                                    </Link>
                                    <a
                                        href="javascript:;"
                                        className="btn btn-primary"
                                        onClick={() => {
                                            this.submitForm();
                                        }}
                                    >
                                        <i className="fa fa-save" /> {save[this.context.Language]}
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div
                            className="kt-content kt-grid__item kt-grid__item--fluid"
                            id="kt_content"
                        >
                            <div className="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                                <div className="row">
                                    <div className="col-lg-3">
                                        <div className="kt-portlet kt-portlet--mobile">
                                            <div className="kt-portlet__head kt-portlet__head--lg">
                                                <div className="kt-portlet__head-label">
                                                    <h3 className="kt-portlet__head-title">
                                                        {avatar[this.context.Language]}
                                                    </h3>
                                                </div>
                                            </div>

                                            <div className="kt-portlet__body">
                                                <div className="form-group item-avatar">
                                                    <input
                                                        type="file"
                                                        className="form-control"
                                                        onChange={e => {
                                                            var that = this;
                                                            uploadPlugin.UpdateImage(e, listfile => {
                                                                that.state.model.Avatar = listfile[0];
                                                                that.setState(that.state);
                                                            });
                                                        }}
                                                    />
                                                    <img
                                                        src={this.state.model.Avatar}
                                                        className="img-responsive"
                                                        style={{ maxWidth: "100%" }}
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-lg-6">
                                        <div className="kt-portlet kt-portlet--mobile">
                                            <div className="kt-portlet__head kt-portlet__head--lg">
                                                <div className="kt-portlet__head-label">
                                                    <h3 className="kt-portlet__head-title">
                                                        {infor[this.context.Language]}
                                                    </h3>
                                                </div>
                                            </div>

                                            <div className="kt-portlet__body">
                                                <div className="form-group">
                                                    <label className="control-label">
                                                        {product_name[this.context.Language]}
                                                    </label>
                                                    <input
                                                        className="form-control"
                                                        value={this.state.model.Name}
                                                        placeholder={product_name[this.context.Language]}
                                                        onChange={e => {
                                                            this.onChangeProductName(e);
                                                        }}
                                                    />
                                                </div>
                                                <div className="form-group">
                                                    <label className="kt-checkbox kt-checkbox-brand">
                                                        <input
                                                            type="checkbox"
                                                            checked={this.state.model.IsActive}
                                                            value={this.state.model.IsActive}
                                                            onChange={e => {
                                                                this.state.model.IsActive = e.target.checked;
                                                                this.setState(this.state);
                                                            }}
                                                        />
                                                        <span />
                                                        {active[this.context.Language]}
                                                    </label>
                                                </div>
                                               
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-lg-3">
                                        <div className="kt-portlet kt-portlet--mobile">
                                            <div className="kt-portlet__head kt-portlet__head--lg">
                                                <div className="kt-portlet__head-label">
                                                    <h3 className="kt-portlet__head-title">
                                                        {classify[this.context.Language]}
                                                    </h3>
                                                </div>
                                            </div>
                                            <div className="kt-portlet__body">
                                                <div className="form-group">
                                                    <label className="control-label">
                                                        {main_cate[this.context.Language]}
                                                    </label>
                                                    <ReactSelect
                                                        isClearable={true}
                                                        isSearchable={true}
                                                        options={this.state.ex.CategoryListDropdownList}
                                                        value={this.state.ex.MainCategoryId}
                                                        placeholder={se_main_cate[this.context.Language]}
                                                        onChange={e => {
                                                            this.state.ex.MainCategoryId = e ? e : e.value;
                                                            this.onChangedProductCategory();
                                                            this.setState(this.state);
                                                        }}
                                                    />
                                                </div>
                                                <div className="form-group">
                                                    <label className="control-label">
                                                        {related_cate[this.context.Language]}
                                                    </label>
                                                    <ReactSelect
                                                        isClearable={true}
                                                        isSearchable={true}
                                                        isMulti
                                                        options={this.state.ex.CategoryListDropdownList}
                                                        value={this.state.ex.RelatedCategoryId}
                                                        placeholder={se_related_cate[this.context.Language]}
                                                        onChange={e => {
                                                            this.state.ex.RelatedCategoryId = !e ? [] : e;
                                                            this.onChangedProductCategory();
                                                            this.setState(this.state);
                                                        }}
                                                    />
                                                </div>
                                                <div className="form-group">
                                                    <label className="control-label">
                                                        {brand[this.context.Language]}
                                                    </label>
                                                    <ReactSelect
                                                        isClearable={true}
                                                        isSearchable={true}
                                                        options={this.state.ex.BrandListDropdown}
                                                        value={
                                                            !this.state.model.Brand
                                                                ? null
                                                                : this.state.ex.BrandListDropdown.find(
                                                                    e => e.value == this.state.model.Brand.Id
                                                                )
                                                        }
                                                        placeholder={se_brand[this.context.Language]}
                                                        onChange={e => {
                                                            if (!e || e.value == "") {
                                                                this.state.model.Brand = null;
                                                            } else {
                                                                this.state.model.Brand = {
                                                                    ...this.state.ex.BrandList.find(
                                                                        br => br.Id == e.value
                                                                    )
                                                                };
                                                            }
                                                            this.setState(this.state);
                                                        }}
                                                    />
                                                </div>
                                                <div className="form-group">
                                                    <label className="control-label">
                                                        {product_type[this.context.Language]}
                                                    </label>

                                                    <ReactSelect
                                                        isClearable={true}
                                                        isSearchable={true}
                                                        options={
                                                            this.state.ex.ProductTypeList
                                                        }
                                                        value={
                                                            this.state.ex.ProductTypeList !=
                                                                null
                                                                ? this.state.ex.ProductTypeList.filter(
                                                                    option =>
                                                                        option.value ===
                                                                        this.state.model.ProductType
                                                                )
                                                                : ""
                                                        }
                                                        placeholder={
                                                            se_product_type[
                                                            this.context.Language
                                                            ]
                                                        }
                                                        onChange={e => {
                                                            var value =
                                                                e === null ? "" : e.value;
                                                            this.state.model.ProductType = value;
                                                            this.setState(this.state);
                                                        }}
                                                    />
                                                </div>
                                                <div className="form-group">
                                                    <label className="control-label">
                                                        {shipping_category[this.context.Language]}
                                                    </label>
                                                    <ReactSelect
                                                        isClearable={true}
                                                        isSearchable={true}
                                                        options={this.state.ex.ShippingCategoryList}
                                                        value={
                                                            !this.state.model.ShippingCategory
                                                                ? null
                                                                : this.state.ex.ShippingCategoryList.find(
                                                                    e =>
                                                                        e.value ==
                                                                        this.state.model.ShippingCategory
                                                                )
                                                        }
                                                        placeholder={
                                                            shipping_category[this.context.Language]
                                                        }
                                                        onChange={e => {
                                                            if (!e || e.value == "") {
                                                                this.state.model.ShippingCategory = null;
                                                            } else {
                                                                this.state.model.ShippingCategory = e.value;
                                                            }
                                                            this.setState(this.state);
                                                        }}
                                                    />
                                                </div>
                                                <div className="form-group">
                                                    <label className="control-label">
                                                        {label_category[this.context.Language]}
                                                    </label>
                                                    <ReactSelect
                                                        isClearable={true}
                                                        isSearchable={true}
                                                        options={this.state.ex.LabelListDropdown}
                                                        value={
                                                            !this.state.model.Label
                                                                ? null
                                                                : this.state.ex.LabelListDropdown.find(
                                                                    e => e.value == this.state.model.Label.Id
                                                                )
                                                        }
                                                        placeholder={label_category[this.context.Language]}
                                                        onChange={e => {
                                                            if (!e || e.value == "") {
                                                                this.state.model.Label = null;
                                                            } else {
                                                                this.state.model.Label = {
                                                                    ...this.state.ex.LabelList.find(
                                                                        br => br.Id == e.value
                                                                    )
                                                                };
                                                            }
                                                            this.setState(this.state);
                                                        }}
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-lg-12">
                                        <div className="kt-portlet kt-portlet--mobile">
                                            <div className="kt-portlet__head kt-portlet__head--lg">
                                                <div className="kt-portlet__head-label">
                                                    <h3 className="kt-portlet__head-title">Skus</h3>
                                                </div>
                                                <div className="kt-portlet__head-toolbar">
                                                    <div className="kt-portlet__head-group">
                                                        <a
                                                            href="javascript:;"
                                                            data-ktportlet-tool="toggle"
                                                            className="btn btn-sm btn-icon btn-brand btn-icon-md"
                                                        >
                                                            <i
                                                                className="la la-angle-down"
                                                                onClick={e => {
                                                                    $(e.target)
                                                                        .parent()
                                                                        .parent()
                                                                        .parent()
                                                                        .parent()
                                                                        .parent()
                                                                        .find(".kt-portlet__body")
                                                                        .toggle();
                                                                    $(e.target)
                                                                        .parent()
                                                                        .parent()
                                                                        .parent()
                                                                        .parent()
                                                                        .parent()
                                                                        .toggleClass("kt-portlet--collapse");
                                                                }}
                                                            />
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>

                                            <div className="kt-portlet__body">
                                                <div className="form-group">
                                                    <div className="form-group">
                                                        <a
                                                            href="javascript:"
                                                            className="btn btn-brand btn-elevate btn-pill"
                                                            onClick={() => {
                                                                this.addnewSku();
                                                                this.setState(this.state);
                                                            }}
                                                        >
                                                            <i className="fa fa-plus" />
                                                            {add_sku[this.context.Language]}
                                                        </a>
                                                        <a
                                                            href="javascript:"
                                                            className="btn btn-brand btn-elevate btn-pill btn-danger"
                                                            style={{ marginLeft: "20px" }}
                                                            onClick={() => {
                                                                this.state.TempSkus = this.state.model.Skus.filter(
                                                                    () => true
                                                                );
                                                                this.setState(this.state);

                                                                $("#list_sort_sku").modal("show");
                                                            }}
                                                        >
                                                            <i className="fa fa-sort" />
                                                            {sort_sku[this.context.Language]}
                                                        </a>
                                                    </div>
                                                    <div className="dataTables_wrapper dt-bootstrap4">
                                                        <table className="table table-striped- table-bordered table-hover table-checkable dataTable no-footer dtr-inline">
                                                            <thead>
                                                                <tr>
                                                                    <th>{avatar[this.context.Language]}</th>
                                                                    <th>{sort[this.context.Language]}</th>
                                                                    <th>{sku_code[this.context.Language]}</th>
                                                                    <th>{name[this.context.Language]}</th>
                                                                    <th>{price[this.context.Language]}</th>
                                                                    <th>
                                                                        {stock_quantity[this.context.Language]}
                                                                    </th>
                                                                    <th>{active[this.context.Language]}</th>
                                                                    <th>{action[this.context.Language]}</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody className="textlist">
                                                                {this.state.model.Skus.map((item, index) => {
                                                                    return (
                                                                        <React.Fragment>
                                                                            {" "}
                                                                            <tr>
                                                                                <td>
                                                                                    <img
                                                                                        height="60"
                                                                                        src={
                                                                                            item.ImageList.length > 0
                                                                                                ? item.ImageList[0].Src
                                                                                                : "/adminstatics/global/img/no-image.png"
                                                                                        }
                                                                                    />
                                                                                </td>
                                                                                <td>
                                                                                    <input
                                                                                        className="form-control"
                                                                                        placeholder={
                                                                                            sort[this.context.Language]
                                                                                        }
                                                                                        value={item.Sort}
                                                                                        onChange={e => {
                                                                                            item.Sort =
                                                                                                e.target.value != ""
                                                                                                    ? parseInt(e.target.value)
                                                                                                    : null;
                                                                                            this.state.model.Skus.sort(
                                                                                                (a, b) => {
                                                                                                    if (a.Sort > b.Sort) {
                                                                                                        return 1;
                                                                                                    }
                                                                                                    if (a.Sort < b.Sort) {
                                                                                                        return -1;
                                                                                                    }
                                                                                                    return 0;
                                                                                                }
                                                                                            );
                                                                                            this.setState(this.state);
                                                                                        }}
                                                                                        type="number"
                                                                                    />
                                                                                </td>
                                                                                <td>
                                                                                    <input
                                                                                        className="form-control"
                                                                                        placeholder={
                                                                                            sku_code[this.context.Language]
                                                                                        }
                                                                                        value={item.SkuCode}
                                                                                        onChange={e => {
                                                                                            item.SkuCode = e.target.value.toUpperCase();
                                                                                            this.setState(this.state);
                                                                                        }}
                                                                                    />
                                                                                </td>
                                                                                <td>{item.Name}</td>
                                                                                <td>
                                                                                    <input
                                                                                        className="form-control"
                                                                                        placeholder={
                                                                                            price[this.context.Language]
                                                                                        }
                                                                                        value={item.Price}
                                                                                        onChange={e => {
                                                                                            item.Price = e.target.value;
                                                                                            this.setState(this.state);
                                                                                        }}
                                                                                    />
                                                                                </td>
                                                                                <td>
                                                                                    <input
                                                                                        className="form-control"
                                                                                        placeholder={
                                                                                            stock_quantity[
                                                                                            this.context.Language
                                                                                            ]
                                                                                        }
                                                                                        value={item.StockQuantity}
                                                                                        onChange={e => {
                                                                                            item.StockQuantity =
                                                                                                e.target.value;
                                                                                            this.setState(this.state);
                                                                                        }}
                                                                                    />
                                                                                </td>
                                                                                <td>
                                                                                    <label className="kt-checkbox kt-checkbox-brand">
                                                                                        <input
                                                                                            type="checkbox"
                                                                                            checked={item.IsActive}
                                                                                            value={item.IsActive}
                                                                                            onChange={e => {
                                                                                                item.IsActive =
                                                                                                    e.target.checked;
                                                                                                this.setState(this.state);
                                                                                            }}
                                                                                        />
                                                                                        <span />
                                                                                        {active[this.context.Language]}
                                                                                    </label>
                                                                                </td>
                                                                                <td>
                                                                                    <div class="dropdown dropdown-inline show">
                                                                                        <button
                                                                                            type="button"
                                                                                            class="btn btn-brand btn-elevate-hover btn-icon btn-sm btn-icon-md btn-circle"
                                                                                            data-toggle="dropdown"
                                                                                            aria-haspopup="true"
                                                                                            aria-expanded="true"
                                                                                        >
                                                                                            <i class="flaticon-more-1" />
                                                                                        </button>
                                                                                        <div
                                                                                            class="dropdown-menu dropdown-menu-right"
                                                                                            x-placement="bottom-end"
                                                                                            style={{
                                                                                                position: "absolute",
                                                                                                transform:
                                                                                                    "translate3d(-150px, 32px, 0px)",
                                                                                                top: "0px",
                                                                                                left: "0px",
                                                                                                "will-change": "transform"
                                                                                            }}
                                                                                        >
                                                                                            <a
                                                                                                class="dropdown-item"
                                                                                                href="javascript:;"
                                                                                                onClick={e => {
                                                                                                    $(
                                                                                                        `#detail-${index}`
                                                                                                    ).toggle();
                                                                                                }}
                                                                                            >
                                                                                                <i className="fa fa-chevron-down" />{" "}
                                                                                                {
                                                                                                    update_more[
                                                                                                    this.context.Language
                                                                                                    ]
                                                                                                }
                                                                                            </a>

                                                                                            <div class="dropdown-divider" />
                                                                                            {this.state.model.Skus.length >
                                                                                                1 && (
                                                                                                    <a
                                                                                                        class="dropdown-item"
                                                                                                        href="javascript:;"
                                                                                                        onClick={() => {
                                                                                                            var that = this;
                                                                                                            swal
                                                                                                                .fire({
                                                                                                                    title:
                                                                                                                        are_you_sure[
                                                                                                                        this.context
                                                                                                                            .Language
                                                                                                                        ],
                                                                                                                    text:
                                                                                                                        data_recover[
                                                                                                                        this.context
                                                                                                                            .Language
                                                                                                                        ],
                                                                                                                    type: "warning",
                                                                                                                    showCancelButton: true,
                                                                                                                    confirmButtonText:
                                                                                                                        yes_de[
                                                                                                                        this.context
                                                                                                                            .Language
                                                                                                                        ],
                                                                                                                    cancelButtonText:
                                                                                                                        no_ca[
                                                                                                                        this.context
                                                                                                                            .Language
                                                                                                                        ]
                                                                                                                })
                                                                                                                .then(function (result) {
                                                                                                                    if (result.value) {
                                                                                                                        that.state.model.Skus.splice(
                                                                                                                            index,
                                                                                                                            1
                                                                                                                        );

                                                                                                                        that.setState(
                                                                                                                            that.state
                                                                                                                        );
                                                                                                                    }
                                                                                                                });
                                                                                                        }}
                                                                                                    >
                                                                                                        <i className="fa fa-trash" />{" "}
                                                                                                        {
                                                                                                            remove[
                                                                                                            this.context.Language
                                                                                                            ]
                                                                                                        }
                                                                                                    </a>
                                                                                                )}
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr
                                                                                className="detail collapse show"
                                                                                id={`detail-${index}`}
                                                                                style={{ display: "none" }}
                                                                            >
                                                                                <td colSpan="10">
                                                                                    <div className="row">
                                                                                        <div className="col-lg-6">
                                                                                            <div className="form-group">
                                                                                                <div className="row">
                                                                                                    {item.ImageList.map(
                                                                                                        (imgsku, imgskuindex) => {
                                                                                                            return (
                                                                                                                <div
                                                                                                                    className="col-sm-3"
                                                                                                                    style={{
                                                                                                                        position:
                                                                                                                            "relative",
                                                                                                                        marginBottom: "20px"
                                                                                                                    }}
                                                                                                                >
                                                                                                                    <img
                                                                                                                        src={imgsku.Src}
                                                                                                                        style={{
                                                                                                                            width: "100%"
                                                                                                                        }}
                                                                                                                    />
                                                                                                                    <a
                                                                                                                        style={{
                                                                                                                            position:
                                                                                                                                "absolute",
                                                                                                                            width:
                                                                                                                                "calc(100% - 20px)",
                                                                                                                            display: "block",
                                                                                                                            padding: "7px",
                                                                                                                            textAlign:
                                                                                                                                "center",
                                                                                                                            bottom: "40px",
                                                                                                                            left: "10px",
                                                                                                                            background:
                                                                                                                                "rgba(0, 0, 0, 0.51)",
                                                                                                                            color: "#fff"
                                                                                                                        }}
                                                                                                                        href="javascript:"
                                                                                                                        onClick={() => {
                                                                                                                            item.ImageList.splice(
                                                                                                                                imgskuindex,
                                                                                                                                1
                                                                                                                            );

                                                                                                                            this.setState(
                                                                                                                                this.state
                                                                                                                            );
                                                                                                                        }}
                                                                                                                    >
                                                                                                                        <i className="fa fa-trash" />
                                                                                                                    </a>
                                                                                                                    <input
                                                                                                                        className="form-control"
                                                                                                                        value={imgsku.Sort}
                                                                                                                        onChange={e => {
                                                                                                                            imgsku.Sort =
                                                                                                                                e.target
                                                                                                                                    .value != ""
                                                                                                                                    ? parseInt(
                                                                                                                                        e.target
                                                                                                                                            .value
                                                                                                                                    )
                                                                                                                                    : null;

                                                                                                                            item.ImageList.sort(
                                                                                                                                (a, b) => {
                                                                                                                                    if (
                                                                                                                                        a.Sort >
                                                                                                                                        b.Sort
                                                                                                                                    ) {
                                                                                                                                        return 1;
                                                                                                                                    }
                                                                                                                                    if (
                                                                                                                                        a.Sort <
                                                                                                                                        b.Sort
                                                                                                                                    ) {
                                                                                                                                        return -1;
                                                                                                                                    }
                                                                                                                                    return 0;
                                                                                                                                }
                                                                                                                            );

                                                                                                                            this.setState(
                                                                                                                                this.state
                                                                                                                            );
                                                                                                                        }}
                                                                                                                        type="number"
                                                                                                                    />
                                                                                                                </div>
                                                                                                            );
                                                                                                        }
                                                                                                    )}
                                                                                                </div>
                                                                                            </div>
                                                                                            <a
                                                                                                href="javascript:"
                                                                                                className="btn btn-primary"
                                                                                                onClick={() => {
                                                                                                    this.state.ex.SkuImageCurrentIndex = index;
                                                                                                    $(
                                                                                                        "#product-imagepopup"
                                                                                                    ).modal("show");
                                                                                                }}
                                                                                            >
                                                                                                <i className="fa fa-pen" />{" "}
                                                                                                Select Image
                                              </a>
                                                                                        </div>
                                                                                        <div className="col-lg-6">
                                                                                            <div className="form-group">
                                                                                                <div className="dataTables_wrapper dt-bootstrap4">
                                                                                                    <table className="table table-striped- table-bordered table-hover table-checkable dataTable no-footer dtr-inline">
                                                                                                        <thead>
                                                                                                            <tr>
                                                                                                                <th
                                                                                                                    style={{
                                                                                                                        width: "35%"
                                                                                                                    }}
                                                                                                                >
                                                                                                                    {
                                                                                                                        attr_name[
                                                                                                                        this.context
                                                                                                                            .Language
                                                                                                                        ]
                                                                                                                    }
                                                                                                                </th>
                                                                                                                <th
                                                                                                                    style={{
                                                                                                                        width: "50%"
                                                                                                                    }}
                                                                                                                >
                                                                                                                    {
                                                                                                                        attr_value[
                                                                                                                        this.context
                                                                                                                            .Language
                                                                                                                        ]
                                                                                                                    }
                                                                                                                </th>
                                                                                                                <th
                                                                                                                    style={{
                                                                                                                        width: "15%"
                                                                                                                    }}
                                                                                                                >
                                                                                                                    {
                                                                                                                        action[
                                                                                                                        this.context
                                                                                                                            .Language
                                                                                                                        ]
                                                                                                                    }
                                                                                                                </th>
                                                                                                            </tr>
                                                                                                        </thead>
                                                                                                        <tbody className="textlist">
                                                                                                            {item.Attributes.map(
                                                                                                                (attr, attrindex) => {
                                                                                                                    return (
                                                                                                                        <tr>
                                                                                                                            <td>
                                                                                                                                <ReactSelect
                                                                                                                                    isClearable={
                                                                                                                                        true
                                                                                                                                    }
                                                                                                                                    isSearchable={
                                                                                                                                        true
                                                                                                                                    }
                                                                                                                                    options={
                                                                                                                                        this.state
                                                                                                                                            .ex
                                                                                                                                            .ProductAttributeListDropdown
                                                                                                                                    }
                                                                                                                                    value={this.state.ex.ProductAttributeListDropdown.find(
                                                                                                                                        att =>
                                                                                                                                            att.value ===
                                                                                                                                            attr.Id
                                                                                                                                    )}
                                                                                                                                    placeholder="Select Attribute Name"
                                                                                                                                    onChange={e => {
                                                                                                                                        if (!e) {
                                                                                                                                            attr.Id = null;
                                                                                                                                        } else {
                                                                                                                                            var temp = this.state.ex.ProductAttributeList.find(
                                                                                                                                                att =>
                                                                                                                                                    att.Id ===
                                                                                                                                                    e.value
                                                                                                                                            );
                                                                                                                                            if (
                                                                                                                                                !temp
                                                                                                                                            ) {
                                                                                                                                                attr.Id = null;
                                                                                                                                            } else {
                                                                                                                                                attr.Id =
                                                                                                                                                    temp.Id;
                                                                                                                                                attr.Name =
                                                                                                                                                    temp.Name;
                                                                                                                                                attr.Slug =
                                                                                                                                                    temp.Slug;
                                                                                                                                            }
                                                                                                                                        }
                                                                                                                                        this.refreshSkuName(
                                                                                                                                            item
                                                                                                                                        );
                                                                                                                                        this.setState(
                                                                                                                                            this.state
                                                                                                                                        );
                                                                                                                                    }}
                                                                                                                                />
                                                                                                                            </td>
                                                                                                                            <td>
                                                                                                                                <ReactSelect
                                                                                                                                    isClearable={
                                                                                                                                        true
                                                                                                                                    }
                                                                                                                                    isSearchable={
                                                                                                                                        true
                                                                                                                                    }
                                                                                                                                    options={
                                                                                                                                        this.state
                                                                                                                                            .ex
                                                                                                                                            .ProductAttributeValueListDropdown
                                                                                                                                    }
                                                                                                                                    value={this.state.ex.ProductAttributeValueListDropdown.find(
                                                                                                                                        attv => {
                                                                                                                                            return (
                                                                                                                                                attr.Values.filter(
                                                                                                                                                    x =>
                                                                                                                                                        x.Id ===
                                                                                                                                                        attv.value
                                                                                                                                                )
                                                                                                                                                    .length >
                                                                                                                                                0
                                                                                                                                            );
                                                                                                                                        }
                                                                                                                                    )}
                                                                                                                                    placeholder="Select Attribute Value"
                                                                                                                                    onChange={e => {
                                                                                                                                        item.Attributes[
                                                                                                                                            attrindex
                                                                                                                                        ].Values = [];
                                                                                                                                        if (e) {
                                                                                                                                            item.Attributes[
                                                                                                                                                attrindex
                                                                                                                                            ].Values = [
                                                                                                                                                    {
                                                                                                                                                        ...this.state.ex.ProductAttributeValueList.find(
                                                                                                                                                            attv =>
                                                                                                                                                                e.value ===
                                                                                                                                                                attv.Id
                                                                                                                                                        )
                                                                                                                                                    }
                                                                                                                                                ];
                                                                                                                                        }
                                                                                                                                        this.refreshSkuName(
                                                                                                                                            item
                                                                                                                                        );
                                                                                                                                        this.setState(
                                                                                                                                            this.state
                                                                                                                                        );
                                                                                                                                    }}
                                                                                                                                />
                                                                                                                            </td>
                                                                                                                            <td>
                                                                                                                                <a
                                                                                                                                    className="btn btn-danger"
                                                                                                                                    href="javascript:"
                                                                                                                                    onClick={() => {
                                                                                                                                        item.Attributes.splice(
                                                                                                                                            attrindex,
                                                                                                                                            1
                                                                                                                                        );
                                                                                                                                        this.refreshSkuName(
                                                                                                                                            item
                                                                                                                                        );
                                                                                                                                        this.setState(
                                                                                                                                            this.state
                                                                                                                                        );
                                                                                                                                    }}
                                                                                                                                >
                                                                                                                                    <i className="fa fa-trash" />
                                                                                                                                </a>
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                    );
                                                                                                                }
                                                                                                            )}
                                                                                                        </tbody>
                                                                                                    </table>
                                                                                                </div>
                                                                                            </div>

                                                                                            {item.Attributes.length < 3 && (
                                                                                                <button
                                                                                                    className="btn btn-primary"
                                                                                                    onClick={() => {
                                                                                                        item.Attributes.push({
                                                                                                            Id: null,
                                                                                                            Name: null,
                                                                                                            Slug: null,
                                                                                                            Sort: 0,
                                                                                                            Values: []
                                                                                                        });
                                                                                                        this.setState(this.state);
                                                                                                    }}
                                                                                                >
                                                                                                    <i className="fa fa-plus" />{" "}
                                                                                                    {
                                                                                                        add_attr[
                                                                                                        this.context.Language
                                                                                                        ]
                                                                                                    }
                                                                                                </button>
                                                                                            )}
                                                                                        </div>
                                                                                    </div>
                                                                                    <div
                                                                                        className="row"
                                                                                        style={{ marginTop: "20px" }}
                                                                                    >
                                                                                        <div className="col-lg-3">
                                                                                            <div className="form-group">
                                                                                                <label className="control-label">
                                                                                                    {width[this.context.Language]}
                                                                                                </label>
                                                                                                <input
                                                                                                    className="form-control"
                                                                                                    value={item.Width}
                                                                                                    placeholder={
                                                                                                        width[this.context.Language]
                                                                                                    }
                                                                                                    onChange={e => {
                                                                                                        item.Width = e.target.value;
                                                                                                        this.setState(this.state);
                                                                                                    }}
                                                                                                />
                                                                                            </div>
                                                                                            <div className="form-group">
                                                                                                <label className="control-label">
                                                                                                    {
                                                                                                        height[
                                                                                                        this.context.Language
                                                                                                        ]
                                                                                                    }
                                                                                                </label>
                                                                                                <input
                                                                                                    className="form-control"
                                                                                                    value={item.Height}
                                                                                                    placeholder={
                                                                                                        height[
                                                                                                        this.context.Language
                                                                                                        ]
                                                                                                    }
                                                                                                    onChange={e => {
                                                                                                        item.Height =
                                                                                                            e.target.value;
                                                                                                        this.setState(this.state);
                                                                                                    }}
                                                                                                />
                                                                                            </div>
                                                                                            <div className="form-group">
                                                                                                <label className="control-label">
                                                                                                    {
                                                                                                        length[
                                                                                                        this.context.Language
                                                                                                        ]
                                                                                                    }
                                                                                                </label>
                                                                                                <input
                                                                                                    className="form-control"
                                                                                                    value={item.Length}
                                                                                                    placeholder={
                                                                                                        length[
                                                                                                        this.context.Language
                                                                                                        ]
                                                                                                    }
                                                                                                    onChange={e => {
                                                                                                        item.Length =
                                                                                                            e.target.value;
                                                                                                        this.setState(this.state);
                                                                                                    }}
                                                                                                />
                                                                                            </div>

                                                                                            <div className="form-group">
                                                                                                <label className="control-label">
                                                                                                    {
                                                                                                        dimension_unit[
                                                                                                        this.context.Language
                                                                                                        ]
                                                                                                    }
                                                                                                </label>
                                                                                                <ReactSelect
                                                                                                    isClearable={true}
                                                                                                    isSearchable={true}
                                                                                                    options={
                                                                                                        this.state.ex.DimensionList
                                                                                                    }
                                                                                                    value={
                                                                                                        this.state.ex
                                                                                                            .DimensionList != null
                                                                                                            ? this.state.ex.DimensionList.filter(
                                                                                                                option =>
                                                                                                                    option.value ===
                                                                                                                    item.DimensionUnit
                                                                                                            )
                                                                                                            : ""
                                                                                                    }
                                                                                                    placeholder={
                                                                                                        dimension_unit[
                                                                                                        this.context.Language
                                                                                                        ]
                                                                                                    }
                                                                                                    onChange={e => {
                                                                                                        var value =
                                                                                                            e === null ? "" : e.value;
                                                                                                        item.DimensionUnit = value;
                                                                                                        this.setState(this.state);
                                                                                                    }}
                                                                                                />
                                                                                            </div>
                                                                                        </div>
                                                                                        <div className="col-lg-3">
                                                                                            <div className="form-group">
                                                                                                <label className="control-label">
                                                                                                    {depth[this.context.Language]}
                                                                                                </label>
                                                                                                <input
                                                                                                    className="form-control"
                                                                                                    value={item.Depth}
                                                                                                    placeholder={
                                                                                                        depth[this.context.Language]
                                                                                                    }
                                                                                                    onChange={e => {
                                                                                                        item.Depth = e.target.value;
                                                                                                        this.setState(this.state);
                                                                                                    }}
                                                                                                />
                                                                                            </div>
                                                                                            <div className="form-group">
                                                                                                <label className="control-label">
                                                                                                    {
                                                                                                        weight[
                                                                                                        this.context.Language
                                                                                                        ]
                                                                                                    }
                                                                                                </label>
                                                                                                <input
                                                                                                    className="form-control"
                                                                                                    value={item.Weight}
                                                                                                    placeholder={
                                                                                                        weight[
                                                                                                        this.context.Language
                                                                                                        ]
                                                                                                    }
                                                                                                    onChange={e => {
                                                                                                        item.Weight =
                                                                                                            e.target.value;
                                                                                                        this.setState(this.state);
                                                                                                    }}
                                                                                                />
                                                                                            </div>
                                                                                            <div className="form-group">
                                                                                                <label className="control-label">
                                                                                                    {
                                                                                                        weight_unit[
                                                                                                        this.context.Language
                                                                                                        ]
                                                                                                    }
                                                                                                </label>
                                                                                                <ReactSelect
                                                                                                    isClearable={true}
                                                                                                    isSearchable={true}
                                                                                                    options={
                                                                                                        this.state.ex.WeightList
                                                                                                    }
                                                                                                    value={
                                                                                                        this.state.ex.WeightList !=
                                                                                                            null
                                                                                                            ? this.state.ex.WeightList.filter(
                                                                                                                option =>
                                                                                                                    option.value ===
                                                                                                                    item.WeightUnit
                                                                                                            )
                                                                                                            : ""
                                                                                                    }
                                                                                                    placeholder={
                                                                                                        weight_unit[
                                                                                                        this.context.Language
                                                                                                        ]
                                                                                                    }
                                                                                                    onChange={e => {
                                                                                                        var value =
                                                                                                            e === null ? "" : e.value;
                                                                                                        item.WeightUnit = value;
                                                                                                        this.setState(this.state);
                                                                                                    }}
                                                                                                />
                                                                                            </div>
                                                                                        </div>
                                                                                        <div className="col-lg-3">
                                                                                            <div className="form-group">
                                                                                                <label className="control-label">
                                                                                                    {
                                                                                                        sale_tax[
                                                                                                        this.context.Language
                                                                                                        ]
                                                                                                    }
                                                                                                </label>
                                                                                                <span className="required">
                                                                                                    {" "}
                                                                                                    *{" "}
                                                                                                </span>
                                                                                                <div className="input-icon right">
                                                                                                    <input
                                                                                                        type="text"
                                                                                                        value={item.SaleTaxRate}
                                                                                                        className="form-control"
                                                                                                        placeholder={
                                                                                                            sale_tax[
                                                                                                            this.context.Language
                                                                                                            ]
                                                                                                        }
                                                                                                        onChange={e => {
                                                                                                            item.SaleTaxRate =
                                                                                                                e.target.value;
                                                                                                            this.setState(this.state);
                                                                                                        }}
                                                                                                    />
                                                                                                </div>
                                                                                            </div>
                                                                                            <div className="form-group">
                                                                                                <label className="control-label">
                                                                                                    {
                                                                                                        is_inventory[
                                                                                                        this.context.Language
                                                                                                        ]
                                                                                                    }
                                                                                                </label>
                                                                                                <span className="required">
                                                                                                    {" "}
                                                                                                    *{" "}
                                                                                                </span>
                                                                                                <div className="input-icon right">
                                                                                                    <span className="kt-switch">
                                                                                                        <label>
                                                                                                            <input
                                                                                                                type="checkbox"
                                                                                                                checked={
                                                                                                                    item.IsInventory
                                                                                                                        ? `checked`
                                                                                                                        : ""
                                                                                                                }
                                                                                                                name=""
                                                                                                                onChange={e => {
                                                                                                                    item.IsInventory =
                                                                                                                        e.target.checked;
                                                                                                                    this.setState(
                                                                                                                        this.state
                                                                                                                    );
                                                                                                                }}
                                                                                                            />
                                                                                                            <span />
                                                                                                        </label>
                                                                                                    </span>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div className="col-lg-3">
                                                                                            <div className="form-group">
                                                                                                <label className="control-label">
                                                                                                    {
                                                                                                        special_price[
                                                                                                        this.context.Language
                                                                                                        ]
                                                                                                    }
                                                                                                </label>

                                                                                                <div className="input-icon right">
                                                                                                    <input
                                                                                                        type="text"
                                                                                                        value={item.SpecialPrice}
                                                                                                        className="form-control"
                                                                                                        placeholder={
                                                                                                            special_price[
                                                                                                            this.context.Language
                                                                                                            ]
                                                                                                        }
                                                                                                        onChange={e => {
                                                                                                            item.SpecialPrice =
                                                                                                                e.target.value;
                                                                                                            this.setState(this.state);
                                                                                                        }}
                                                                                                    />
                                                                                                </div>
                                                                                            </div>
                                                                                            <div className="form-group">
                                                                                                <label className="control-label">
                                                                                                    {
                                                                                                        from_date[
                                                                                                        this.context.Language
                                                                                                        ]
                                                                                                    }
                                                                                                </label>
                                                                                                <span className="required">
                                                                                                    {" "}
                                                                                                    *{" "}
                                                                                                </span>
                                                                                                <div className="input-icon right">
                                                                                                    <DateTimePicker
                                                                                                        value={moment
                                                                                                            .utc(item.FromDate)
                                                                                                            .format(
                                                                                                                "MM/DD/YYYY hh:mm A"
                                                                                                            )}
                                                                                                        options={{
                                                                                                            enableTime: true,
                                                                                                            dateFormat: "m/d/Y G:S K",
                                                                                                            disableMobile: true
                                                                                                        }}
                                                                                                        onChange={(
                                                                                                            selectedDates,
                                                                                                            dateStr,
                                                                                                            instance
                                                                                                        ) => {
                                                                                                            item.FromDate = moment
                                                                                                                .utc(dateStr)
                                                                                                                .toDate();
                                                                                                            this.setState(this.state);
                                                                                                        }}
                                                                                                        placeholder=""
                                                                                                    />
                                                                                                </div>
                                                                                            </div>
                                                                                            <div className="form-group">
                                                                                                <label className="control-label">
                                                                                                    {
                                                                                                        to_date[
                                                                                                        this.context.Language
                                                                                                        ]
                                                                                                    }
                                                                                                </label>
                                                                                                <span className="required">
                                                                                                    {" "}
                                                                                                    *{" "}
                                                                                                </span>
                                                                                                <div className="input-icon right">
                                                                                                    <DateTimePicker
                                                                                                        value={moment
                                                                                                            .utc(item.ToDate)
                                                                                                            .format(
                                                                                                                "MM/DD/YYYY hh:mm A"
                                                                                                            )}
                                                                                                        options={{
                                                                                                            enableTime: true,
                                                                                                            dateFormat: "m/d/Y G:S K",
                                                                                                            disableMobile: true
                                                                                                        }}
                                                                                                        onChange={(
                                                                                                            selectedDates,
                                                                                                            dateStr,
                                                                                                            instance
                                                                                                        ) => {
                                                                                                            item.ToDate = moment
                                                                                                                .utc(dateStr)
                                                                                                                .toDate();
                                                                                                            this.setState(this.state);
                                                                                                        }}
                                                                                                        placeholder=""
                                                                                                    />
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </React.Fragment>
                                                                    );
                                                                })}
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-lg-8">
                                        <div className="kt-portlet kt-portlet--mobile kt-portlet--collapse">
                                            <div className="kt-portlet__head kt-portlet__head--lg">
                                                <div className="kt-portlet__head-label">
                                                    <h3 className="kt-portlet__head-title">
                                                        {specification[this.context.Language]}
                                                    </h3>
                                                </div>
                                                <div className="kt-portlet__head-toolbar">
                                                    <div className="kt-portlet__head-group">
                                                        <a
                                                            href="javascript:;"
                                                            data-ktportlet-tool="toggle"
                                                            class="btn btn-sm btn-icon btn-brand btn-icon-md"
                                                            aria-describedby="tooltip_9mzud580e8"
                                                        >
                                                            <i class="la la-angle-down" />
                                                        </a>
                                                        <a
                                                            href="javascript:;"
                                                            data-ktportlet-tool="toggle"
                                                            className="btn btn-sm btn-icon btn-brand btn-icon-md"
                                                        >
                                                            <i
                                                                className="la la-angle-down"
                                                                onClick={e => {
                                                                    $(e.target)
                                                                        .parent()
                                                                        .parent()
                                                                        .parent()
                                                                        .parent()
                                                                        .parent()
                                                                        .find(".kt-portlet__body")
                                                                        .toggle();
                                                                    $(e.target)
                                                                        .parent()
                                                                        .parent()
                                                                        .parent()
                                                                        .parent()
                                                                        .parent()
                                                                        .toggleClass("kt-portlet--collapse");
                                                                }}
                                                            />
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>

                                            <div
                                                className="kt-portlet__body"
                                                style={{ display: "none" }}
                                            >
                                                <div className="form-group">
                                                    <div className="dataTables_wrapper dt-bootstrap4">
                                                        <table className="table table-striped- table-bordered table-hover table-checkable dataTable no-footer dtr-inline">
                                                            <thead>
                                                                <tr>
                                                                    <th>
                                                                        {attr_group_info[this.context.Language]}
                                                                    </th>
                                                                    <th
                                                                        style={{
                                                                            width: "100px"
                                                                        }}
                                                                    >
                                                                        {sort[this.context.Language]}
                                                                    </th>
                                                                    <th
                                                                        style={{
                                                                            width: "50px"
                                                                        }}
                                                                    >
                                                                        {action[this.context.Language]}
                                                                    </th>
                                                                </tr>
                                                            </thead>
                                                            <tbody className="textlist">
                                                                {this.state.model.AttributeGroups.map(
                                                                    (attrgroup, attrgroupindex) => {
                                                                        return (
                                                                            <tr>
                                                                                <td>
                                                                                    <div className="form-group">
                                                                                        <ReactSelect
                                                                                            isClearable={true}
                                                                                            isSearchable={true}
                                                                                            options={
                                                                                                this.state.ex
                                                                                                    .ProductAttributeGroupListDropdown
                                                                                            }
                                                                                            value={this.state.ex.ProductAttributeGroupListDropdown.find(
                                                                                                att =>
                                                                                                    att.value === attrgroup.Id
                                                                                            )}
                                                                                            placeholder={
                                                                                                se_attr_group_info[
                                                                                                this.context.Language
                                                                                                ]
                                                                                            }
                                                                                            onChange={e => {
                                                                                                if (!e) {
                                                                                                    attrgroup.Id = null;
                                                                                                } else {
                                                                                                    var temp = this.state.ex.ProductAttributeGroupList.find(
                                                                                                        attg => attg.Id === e.value
                                                                                                    );
                                                                                                    if (!temp) {
                                                                                                        attrgroup.Id = null;
                                                                                                    } else {
                                                                                                        attrgroup.Id = temp.Id;
                                                                                                        attrgroup.Name = temp.Name;
                                                                                                        attrgroup.Slug = temp.Slug;
                                                                                                    }
                                                                                                }
                                                                                                this.setState(this.state);
                                                                                            }}
                                                                                        />
                                                                                    </div>
                                                                                    <div className="form-group">
                                                                                        <div className="dataTables_wrapper dt-bootstrap4">
                                                                                            <table className="table table-striped- table-bordered table-hover table-checkable dataTable no-footer dtr-inline">
                                                                                                <thead>
                                                                                                    <tr>
                                                                                                        <th
                                                                                                            style={{
                                                                                                                width: "35%"
                                                                                                            }}
                                                                                                        >
                                                                                                            {
                                                                                                                attr_name[
                                                                                                                this.context.Language
                                                                                                                ]
                                                                                                            }
                                                                                                        </th>
                                                                                                        <th
                                                                                                            style={{
                                                                                                                width: "50%"
                                                                                                            }}
                                                                                                        >
                                                                                                            {
                                                                                                                attr_value[
                                                                                                                this.context.Language
                                                                                                                ]
                                                                                                            }
                                                                                                        </th>
                                                                                                        <th
                                                                                                            style={{
                                                                                                                width: "15%"
                                                                                                            }}
                                                                                                        >
                                                                                                            {
                                                                                                                action[
                                                                                                                this.context.Language
                                                                                                                ]
                                                                                                            }
                                                                                                        </th>
                                                                                                    </tr>
                                                                                                </thead>
                                                                                                <tbody className="textlist">
                                                                                                    {attrgroup.Attributes.map(
                                                                                                        (attr, attrindex) => {
                                                                                                            return (
                                                                                                                <tr>
                                                                                                                    <td>
                                                                                                                        <ReactSelect
                                                                                                                            isClearable={true}
                                                                                                                            isSearchable={
                                                                                                                                true
                                                                                                                            }
                                                                                                                            options={
                                                                                                                                this.state.ex
                                                                                                                                    .ProductAttributeListDropdown
                                                                                                                            }
                                                                                                                            value={this.state.ex.ProductAttributeListDropdown.find(
                                                                                                                                att =>
                                                                                                                                    att.value ===
                                                                                                                                    attr.Id
                                                                                                                            )}
                                                                                                                            placeholder={
                                                                                                                                se_attr_name[
                                                                                                                                this.context
                                                                                                                                    .Language
                                                                                                                                ]
                                                                                                                            }
                                                                                                                            onChange={e => {
                                                                                                                                if (!e) {
                                                                                                                                    attr.Id = null;
                                                                                                                                } else {
                                                                                                                                    var temp = this.state.ex.ProductAttributeList.find(
                                                                                                                                        att =>
                                                                                                                                            att.Id ===
                                                                                                                                            e.value
                                                                                                                                    );
                                                                                                                                    if (!temp) {
                                                                                                                                        attr.Id = null;
                                                                                                                                    } else {
                                                                                                                                        attr.Id =
                                                                                                                                            temp.Id;
                                                                                                                                        attr.Name =
                                                                                                                                            temp.Name;
                                                                                                                                        attr.Slug =
                                                                                                                                            temp.Slug;
                                                                                                                                    }
                                                                                                                                }
                                                                                                                                this.setState(
                                                                                                                                    this.state
                                                                                                                                );
                                                                                                                            }}
                                                                                                                        />
                                                                                                                    </td>
                                                                                                                    <td>
                                                                                                                        <ReactSelect
                                                                                                                            isClearable={true}
                                                                                                                            isSearchable={
                                                                                                                                true
                                                                                                                            }
                                                                                                                            isMulti
                                                                                                                            options={
                                                                                                                                this.state.ex
                                                                                                                                    .ProductAttributeValueListDropdown
                                                                                                                            }
                                                                                                                            value={this.state.ex.ProductAttributeValueListDropdown.filter(
                                                                                                                                attv => {
                                                                                                                                    return (
                                                                                                                                        attr.Values.filter(
                                                                                                                                            x =>
                                                                                                                                                x.Id ===
                                                                                                                                                attv.value
                                                                                                                                        ).length > 0
                                                                                                                                    );
                                                                                                                                }
                                                                                                                            )}
                                                                                                                            placeholder={
                                                                                                                                se_attr_value[
                                                                                                                                this.context
                                                                                                                                    .Language
                                                                                                                                ]
                                                                                                                            }
                                                                                                                            onChange={e => {
                                                                                                                                attr.Values = [];
                                                                                                                                if (e) {
                                                                                                                                    attr.Values = this.state.ex.ProductAttributeValueList.filter(
                                                                                                                                        attv => {
                                                                                                                                            return (
                                                                                                                                                e.filter(
                                                                                                                                                    x =>
                                                                                                                                                        attv.Id ===
                                                                                                                                                        x.value
                                                                                                                                                )
                                                                                                                                                    .length >
                                                                                                                                                0
                                                                                                                                            );
                                                                                                                                        }
                                                                                                                                    );
                                                                                                                                }

                                                                                                                                this.setState(
                                                                                                                                    this.state
                                                                                                                                );
                                                                                                                            }}
                                                                                                                        />
                                                                                                                    </td>
                                                                                                                    <td>
                                                                                                                        <a
                                                                                                                            className="btn btn-danger"
                                                                                                                            href="javascript:"
                                                                                                                            onClick={() => {
                                                                                                                                attrgroup.Attributes.splice(
                                                                                                                                    attrindex,
                                                                                                                                    1
                                                                                                                                );

                                                                                                                                this.setState(
                                                                                                                                    this.state
                                                                                                                                );
                                                                                                                            }}
                                                                                                                        >
                                                                                                                            <i className="fa fa-trash" />
                                                                                                                        </a>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            );
                                                                                                        }
                                                                                                    )}
                                                                                                </tbody>
                                                                                            </table>
                                                                                        </div>
                                                                                    </div>

                                                                                    <button
                                                                                        className="btn btn-primary"
                                                                                        onClick={() => {
                                                                                            attrgroup.Attributes.push({
                                                                                                Id: null,
                                                                                                Name: null,
                                                                                                Slug: null,
                                                                                                Sort: 0,
                                                                                                Values: []
                                                                                            });
                                                                                            this.setState(this.state);
                                                                                        }}
                                                                                    >
                                                                                        <i className="fa fa-plus" />
                                                                                        {add_attr[this.context.Language]}
                                                                                    </button>
                                                                                </td>
                                                                                <td>
                                                                                    <input
                                                                                        className="form-control"
                                                                                        placeholder="Sort"
                                                                                        value={attrgroup.Sort}
                                                                                        onChange={e => {
                                                                                            attrgroup.Sort =
                                                                                                e.target.value != ""
                                                                                                    ? parseInt(e.target.value)
                                                                                                    : null;
                                                                                            this.state.model.AttributeGroups.sort(
                                                                                                (a, b) => {
                                                                                                    if (a.Sort > b.Sort) {
                                                                                                        return 1;
                                                                                                    }
                                                                                                    if (a.Sort < b.Sort) {
                                                                                                        return -1;
                                                                                                    }
                                                                                                    return 0;
                                                                                                }
                                                                                            );
                                                                                            this.setState(this.state);
                                                                                        }}
                                                                                        type="number"
                                                                                    />
                                                                                </td>
                                                                                <td>
                                                                                    <a
                                                                                        className="btn btn-danger"
                                                                                        href="javascript:"
                                                                                        onClick={() => {
                                                                                            this.state.model.AttributeGroups.splice(
                                                                                                attrgroupindex,
                                                                                                1
                                                                                            );
                                                                                            this.setState(this.state);
                                                                                        }}
                                                                                    >
                                                                                        <i className="fa fa-trash" />
                                                                                    </a>
                                                                                </td>
                                                                            </tr>
                                                                        );
                                                                    }
                                                                )}
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>

                                                <button
                                                    className="btn btn-primary"
                                                    onClick={() => {
                                                        this.state.model.AttributeGroups.push({
                                                            Id: null,
                                                            Name: null,
                                                            Slug: null,
                                                            Sort:
                                                                this.state.model.AttributeGroups.length > 0
                                                                    ? this.state.model.AttributeGroups[
                                                                        this.state.model.AttributeGroups.length -
                                                                        1
                                                                    ].Sort + 1
                                                                    : 0,
                                                            Attributes: []
                                                        });
                                                        this.setState(this.state);
                                                    }}
                                                >
                                                    <i className="fa fa-plus" />{" "}
                                                    {add_attr_group[this.context.Language]}
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-lg-4">
                                        <div className="kt-portlet kt-portlet--mobile kt-portlet--collapse">
                                            <div className="kt-portlet__head kt-portlet__head--lg">
                                                <div className="kt-portlet__head-label">
                                                    <h3 className="kt-portlet__head-title">
                                                        {description[this.context.Language]}
                                                    </h3>
                                                </div>
                                                <div className="kt-portlet__head-toolbar">
                                                    <div className="kt-portlet__head-group">
                                                        <a
                                                            href="javascript:;"
                                                            data-ktportlet-tool="toggle"
                                                            className="btn btn-sm btn-icon btn-brand btn-icon-md"
                                                        >
                                                            <i
                                                                className="la la-angle-down"
                                                                onClick={e => {
                                                                    $(e.target)
                                                                        .parent()
                                                                        .parent()
                                                                        .parent()
                                                                        .parent()
                                                                        .parent()
                                                                        .find(".kt-portlet__body")
                                                                        .toggle();
                                                                    $(e.target)
                                                                        .parent()
                                                                        .parent()
                                                                        .parent()
                                                                        .parent()
                                                                        .parent()
                                                                        .toggleClass("kt-portlet--collapse");
                                                                }}
                                                            />
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>

                                            <div
                                                className="kt-portlet__body"
                                                style={{ display: "none" }}
                                            >
                                                <div className="form-group">
                                                    <label className="control-label">
                                                        {short_des[this.context.Language]}
                                                    </label>
                                                    <CkEditor
                                                        value={this.state.model.ShortDescription}
                                                        onChange={e => {
                                                            this.state.model.ShortDescription = e;
                                                            this.setState(this.state);
                                                        }}
                                                    />
                                                </div>
                                                <div className="form-group">
                                                    <label className="control-label">
                                                        {description[this.context.Language]}
                                                    </label>
                                                    <CkEditor
                                                        value={this.state.model.Description}
                                                        onChange={e => {
                                                            this.state.model.Description = e;
                                                            this.setState(this.state);
                                                        }}
                                                    />
                                                </div>
                                                <div className="form-group">
                                                    <label className="control-label">
                                                        {benefits[this.context.Language]}
                                                    </label>
                                                    <CkEditor
                                                        value={this.state.model.Benefits}
                                                        onChange={e => {
                                                            this.state.model.Benefits = e;
                                                            this.setState(this.state);
                                                        }}
                                                    />
                                                </div>
                                                <div className="form-group">
                                                    <label className="control-label">
                                                        {ingredient[this.context.Language]}
                                                    </label>
                                                    <CkEditor
                                                        value={this.state.model.Ingredient}
                                                        onChange={e => {
                                                            this.state.model.Ingredient = e;
                                                            this.setState(this.state);
                                                        }}
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="kt-portlet kt-portlet--mobile kt-portlet--collapse">
                                            <div className="kt-portlet__head kt-portlet__head--lg">
                                                <div className="kt-portlet__head-label">
                                                    <h3 className="kt-portlet__head-title">SEO</h3>
                                                </div>
                                                <div className="kt-portlet__head-toolbar">
                                                    <div className="kt-portlet__head-group">
                                                        <a
                                                            href="javascript:;"
                                                            data-ktportlet-tool="toggle"
                                                            className="btn btn-sm btn-icon btn-brand btn-icon-md"
                                                        >
                                                            <i
                                                                className="la la-angle-down"
                                                                onClick={e => {
                                                                    $(e.target)
                                                                        .parent()
                                                                        .parent()
                                                                        .parent()
                                                                        .parent()
                                                                        .parent()
                                                                        .find(".kt-portlet__body")
                                                                        .toggle();
                                                                    $(e.target)
                                                                        .parent()
                                                                        .parent()
                                                                        .parent()
                                                                        .parent()
                                                                        .parent()
                                                                        .toggleClass("kt-portlet--collapse");
                                                                }}
                                                            />
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>

                                            <div
                                                className="kt-portlet__body"
                                                style={{ display: "none" }}
                                            >
                                                <div className="form-group">
                                                    <label className="control-label">
                                                        {seo_title[this.context.Language]}
                                                    </label>
                                                    <input
                                                        className="form-control"
                                                        placeholder={seo_title[this.context.Language]}
                                                        maxlength="70"
                                                        value={this.state.model.MetaTitle}
                                                        onChange={e => {
                                                            this.state.model.MetaTitle = e.target.value;
                                                            this.setState(this.state);
                                                        }}
                                                    />
                                                </div>
                                                <div className="form-group">
                                                    <label className="control-label">
                                                        {seo_des[this.context.Language]}
                                                    </label>
                                                    <textarea
                                                        className="form-control"
                                                        placeholder={seo_des[this.context.Language]}
                                                        maxlength="160"
                                                        id="MetaDescription"
                                                        rows="5"
                                                        value={this.state.model.MetaDescription}
                                                        onChange={e => {
                                                            this.state.model.MetaDescription = e.target.value;
                                                            this.setState(this.state);
                                                        }}
                                                    />
                                                </div>
                                                <div className="form-group">
                                                    <label className="control-label">
                                                        {seo_key[this.context.Language]}
                                                    </label>
                                                    <input
                                                        className="form-control"
                                                        placeholder={seo_key[this.context.Language]}
                                                        value={this.state.model.MetaKeyword}
                                                        onChange={e => {
                                                            this.state.model.MetaKeyword = e.target.value;
                                                            this.setState(this.state);
                                                        }}
                                                    />
                                                </div>
                                                <div className="form-group">
                                                    <label className="control-label">Url</label>
                                                    <input
                                                        className="form-control"
                                                        placeholder="Url slug"
                                                        value={this.state.model.Url}
                                                        onChange={e => {
                                                            this.state.model.Url = e.target.value;
                                                            this.setState(this.state);
                                                        }}
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div
                            className="modal fade"
                            id="product-imagepopup"
                            ref={this.popup}
                            tabindex="-1"
                            role="dialog"
                            aria-labelledby="exampleModalLabel"
                            aria-hidden="true"
                        >
                            <div className="modal-dialog modal-xl" role="document">
                                <div className="modal-content">
                                    <div className="modal-header">
                                        <h5 className="modal-title" id="exampleModalLabel">
                                            {se_image[this.context.Language]}
                                        </h5>
                                        <button
                                            type="button"
                                            className="close"
                                            data-dismiss="modal"
                                            aria-label="Close"
                                        />
                                    </div>
                                    <div className="modal-body">
                                        <div className="form-group">
                                            <div className="row">
                                                {this.state.model.ProductImages.map(image => {
                                                    return (
                                                        <div
                                                            className="col-sm-2"
                                                            style={{ marginBottom: "20px" }}
                                                        >
                                                            <img
                                                                style={{
                                                                    width: "100%",
                                                                    cursor: "pointer",
                                                                    border: image.Selected
                                                                        ? "1px solid var(--dark)"
                                                                        : "none"
                                                                }}
                                                                src={image.Src}
                                                                onClick={() => {
                                                                    image.Selected = !image.Selected;
                                                                    this.setState(this.state);
                                                                }}
                                                            />
                                                        </div>
                                                    );

                                                })}
                                            </div>
                                        </div>
                                        <a
                                            href="javascript:"
                                            className="btn btn-primary btn-customfile"
                                        >
                                            <input
                                                type="file"
                                                multiple
                                                onChange={e => {
                                                    var that = this;

                                                    uploadPlugin.UpdateImage(e, files => {
                                                        files.forEach(file => {
                                                            var newmodel = {
                                                                Id: Math.random()
                                                                    .toString(36)
                                                                    .substr(2, 16),
                                                                Src: file,
                                                                Selected: true
                                                            };
                                                            that.state.model.ProductImages.push(newmodel);
                                                        });
                                                        this.setState(this.state);
                                                    });
                                                }}
                                            />
                                            <i className="fa fa-plus" />
                                            {add_image[this.context.Language]}
                                        </a>
                                        <a
                                            style={{ marginLeft: "20px" }}
                                            href="javascript:"
                                            className="btn btn-danger btn-customfile"
                                            onClick={e => {
                                                var that = this;
                                                if (
                                                    this.state.model.ProductImages == null ||
                                                    this.state.model.ProductImages.length == 0
                                                ) {
                                                    toastr["error"](
                                                        "Empty image list ",
                                                        error[this.context.Language]
                                                    );
                                                    return -1;
                                                }
                                                var image_selected = this.state.model.ProductImages.filter(
                                                    c => {
                                                        return c.Selected == true;
                                                    }
                                                );
                                                if (image_selected.length <= 0) {
                                                    toastr["error"](
                                                        "Please select Image ",
                                                        error[this.context.Language]
                                                    );
                                                    return -1;
                                                }
                                                swal
                                                    .fire({
                                                        title: are_you_sure[this.context.Language],
                                                        text: data_recover[this.context.Language],
                                                        type: "warning",
                                                        showCancelButton: true,
                                                        confirmButtonText: yes_de[this.context.Language],
                                                        cancelButtonText: no_ca[this.context.Language]
                                                    })
                                                    .then(function (result) {
                                                        if (result.value) {
                                                            that.state.model.ProductImages = that.state.model.ProductImages.filter(
                                                                c => {
                                                                    return c.Selected != true;
                                                                }
                                                            );

                                                            that.setState(that.state);
                                                        }
                                                    });
                                            }}
                                        >
                                            <i className="fa fa-minus" />
                                            Remove Image
                    </a>
                                    </div>
                                    <div className="modal-footer">
                                        <button
                                            type="button"
                                            className="btn btn-default"
                                            data-dismiss="modal"
                                        >
                                            {close[this.context.Language]}
                                        </button>
                                        <button
                                            type="button"
                                            className="btn btn-primary"
                                            onClick={() => {
                                                this.state.model.ProductImages.filter(im => {
                                                    return (
                                                        im.Selected &&
                                                        this.state.model.Skus[
                                                            this.state.ex.SkuImageCurrentIndex
                                                        ].ImageList.find(e => e.Id == im.Id) == null
                                                    );
                                                }).forEach(im => {
                                                    this.state.model.Skus[
                                                        this.state.ex.SkuImageCurrentIndex
                                                    ].ImageList.push({
                                                        Id: im.Id,
                                                        Src: im.Src,
                                                        Alt: im.Alt,
                                                        Sort:
                                                            this.state.model.Skus[
                                                                this.state.ex.SkuImageCurrentIndex
                                                            ].ImageList.length == 0
                                                                ? this.state.model.Skus[
                                                                    this.state.ex.SkuImageCurrentIndex
                                                                ].ImageList.length
                                                                : this.state.model.Skus[
                                                                    this.state.ex.SkuImageCurrentIndex
                                                                ].ImageList[
                                                                    this.state.model.Skus[
                                                                        this.state.ex.SkuImageCurrentIndex
                                                                    ].ImageList.length - 1
                                                                ].Sort + 1
                                                    });
                                                });
                                                this.state.model.ProductImages.forEach(img => {
                                                    img.Selected = false;
                                                    this.setState(this.state);
                                                })
                                                this.setState(this.state);
                                                this.state.model.ProductImages = [];
                                                $("#product-imagepopup").modal("hide");
                                            }}
                                        >
                                            {se_image[this.context.Language]}
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div
                            className="modal fade"
                            id="list_sort_sku"
                            ref={this.popup}
                            tabindex="-1"
                            role="dialog"
                            aria-labelledby="exampleModalLabel2"
                            aria-hidden="true"
                        >
                            <div className="modal-dialog modal-lg" role="document">
                                <div className="modal-content">
                                    <div className="modal-header">
                                        <h5 className="modal-title" id="exampleModalLabel2">
                                            Sort Product
                    </h5>
                                        <button
                                            type="button"
                                            className="close"
                                            data-dismiss="modal"
                                            aria-label="Close"
                                        />
                                    </div>
                                    <div className="modal-body">
                                        <div className="dataTables_wrapper dt-bootstrap4">
                                            <table className="table table-striped- table-bordered table-hover table-checkable dataTable no-footer dtr-inline">
                                                <thead>
                                                    <tr>
                                                        <th>STT</th>
                                                        <th>Sku Code</th>
                                                        <th>Sku Name</th>
                                                    </tr>
                                                </thead>

                                                <tbody ref={this.ProductListBox} className="textlist-2">
                                                    {this.state.TempSkus.map((ele, index) => {
                                                        return (
                                                            <tr data-id={ele.SkuCode}>
                                                                <td>{ele.Sort}</td>
                                                                <td>{ele.SkuCode}</td>
                                                                <td>{ele.Name}</td>
                                                            </tr>
                                                        );
                                                    })}
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div className="modal-footer">
                                        <button
                                            type="button"
                                            className="btn btn-default"
                                            data-dismiss="modal"
                                        >
                                            {close[this.context.Language]}
                                        </button>
                                        <button
                                            type="button"
                                            className="btn btn-primary"
                                            onClick={() => {
                                                $("#list_sort_sku").modal("hide");
                                                this.submitFormAfterSort();
                                            }}
                                        >
                                            Save
                    </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </React.Fragment>
                ) : (
                        <div />
                    )}
            </React.Fragment>
        );
    }
}
ProductAddUpdate.contextType = AppContext;
export default ProductAddUpdate;
