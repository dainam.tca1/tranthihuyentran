import React, { Component } from "react";
import { DelayInput } from "react-delay-input";
import PagedList from "../../components/pagedlist";
import VideoCmsModel from "../../modules/Product/models/video-cmsmodel";
import {
    globalErrorMessage,
    error,
    success,
    back,
    add,
    save,
    close,
    remove,
    total,
    record,
    page,
    cDate,
    lDate,
    recordPerPage,
    order
} from "../../constants/message";
import {
    
} from "./models/productstaticmessage";
import AppContext from "../../components/app-context";
import { Link } from "react-router-dom";
import FilterListPlugin from "../../plugins/filter-list-plugin";
import RemoveListPlugin from "../../plugins/remove-list-plugin";
const apiurl = "/cms/api/video";

class brandList extends Component {
  constructor(props,context) {
    super(props,context);
    var that = this;
    that.state = {
      ex: {
        Title: brand[this.context.Language],
        Param: {
          pagesize: null,
          categoryId: null
        }
      }
    };
    that.setState(that.state);
    that.handleChangeFilter = that.handleChangeFilter.bind(that);
    that.handleChangeDataRow = that.handleChangeDataRow.bind(that);
    that.toPage(1);
  }

  toPage(index) {
    FilterListPlugin.filterdata(index, this, apiurl);
  }
  /**
   * Được gọi khi thay đổi các trường filter cập nhật tham số filter vào state
   * Filter dữ liệu theo tham số mới
   * @param {Event} event
   */
  handleChangeFilter(event) {
    FilterListPlugin.handleChangeFilter(event, this);
    this.toPage(1);
  }

  /**
   * Xóa data đã được chọn
   */
  handleDeleteDataRow() {
    RemoveListPlugin.removeList(`${apiurl}/removelist`, this);
  }
  handleChangeDataRow(event) {
    const target = event.target;
    const value = target.type === "checkbox" ? target.checked : target.value;
    const name = target.name;
    const index = target.getAttribute("index");
    var newobj = { ...this.state.model.Results[index] };
    newobj[name] = isNaN(value) ? value : parseInt(value);
    var that = this;
    KTApp.blockPage();
    $.ajax({
      url: apiurl + "/updatecustomize",
      type: "POST",
      dataType: "json",
      contentType: "application/json",
      data: JSON.stringify({ model: newobj, name }),
      success: response => {
        KTApp.unblockPage();
        toastr.clear();
        if (response.status == "success") {
          that.state.model.Results[index][name] = newobj[name];
          that.setState(that.state);
            toastr["success"](response.message, success[this.context.Language]);
        } else {
          toastr["error"](response.message, error[this.context.Language]);
        }
      },
      error: function(er) {
        KTApp.unblockPage();
        toastr.clear();
          toastr["error"](globalErrorMessage[this.context.Language], error[this.context.Language]);
      }
    });
  }
  componentWillMount() {}
  componentDidMount() {
    document.title = this.state.ex.Title;
    $("#kt_aside_menu .kt-menu__item").removeClass("kt-menu__item--active");
    $("#kt_aside_menu .kt-menu__item[data-id='san-pham']").addClass(
      "kt-menu__item--active"
    );
    $("#kt_aside_menu .kt-menu__item[data-id='thuong-hieu-san-pham']").addClass(
      "kt-menu__item--active"
    );
  }
  render() {
    return (
      <React.Fragment>
        {this.state && this.state.model ? (
          <React.Fragment>
            <div className="kt-subheader kt-grid__item" id="kt_subheader">
              <div className="kt-subheader__main">
                <div className="kt-subheader__breadcrumbs">
                  <Link
                    to="/admin/dashboard"
                    className="kt-subheader__breadcrumbs-home"
                  >
                    <i className="fa fa-home" />
                  </Link>

                  <span className="kt-subheader__breadcrumbs-separator" />
                  <span className="kt-subheader__breadcrumbs-link">
                                    {product[this.context.Language]}
                  </span>
                  <span className="kt-subheader__breadcrumbs-separator" />
                  <span className="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
                    {this.state.ex.Title}
                  </span>
                </div>
              </div>
              <div className="kt-subheader__toolbar">
                <div className="kt-subheader__wrapper">
                                <a href="javascript:void(0)" className="btn btn-primary" onClick={e => $("#kt_modal_4").modal("show")}>
                     <i className="fa fa-plus" /> {add[this.context.Language]}
                  </a>
                  <a
                    href="javascript:;"
                    onClick={e => this.handleDeleteDataRow()}
                    className="btn btn-danger"
                  >
                                    <i className="fa fa-trash-alt" /> {remove[this.context.Language]}
                  </a>
                </div>
              </div>
            </div>
            <div
              className="kt-content kt-grid__item kt-grid__item--fluid"
              id="kt_content"
            >
              <div className="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                <div className="kt-portlet kt-portlet--mobile">
                  <div className="kt-portlet__head kt-portlet__head--lg">
                    <div className="kt-portlet__head-label">
                      <h3 className="kt-portlet__head-title">
                        {this.state.ex.Title}
                      </h3>
                    </div>
                  </div>

                  <div className="kt-portlet__body">
                    <div
                      id="kt_table_1_wrapper"
                      className="dataTables_wrapper dt-bootstrap4"
                    >
                      <div className="row">
                        <div className="col-sm-12">
                          <table className="table table-striped- table-bordered table-hover table-checkable dataTable no-footer dtr-inline table-responsive">
                            <thead>
                              <tr role="row">
                                <th
                                  className="dt-right sorting_disabled"
                                  rowspan="1"
                                  colspan="1"
                                  style={{ width: "1%" }}
                                  aria-label="Record ID"
                                >
                                  <label className="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                    <input
                                      type="checkbox"
                                      className="kt-group-checkable"
                                      onChange={ev => {
                                        this.state.model.Results = this.state.model.Results.map(
                                          e => {
                                            return {
                                              ...e,
                                              IsChecked: ev.target.checked
                                            };
                                          }
                                        );
                                        this.setState(this.state);
                                      }}
                                    />
                                    <span />
                                  </label>
                                </th>
                                <th>{name[this.context.Language]}</th>
                                <th>{order[this.context.Language]}</th>
                              </tr>
                            </thead>
                            <tbody>
                              {this.state.model.Results.map((c, index) => {
                                return (
                                  <tr>
                                    <td className="dt-right" tabindex="0">
                                      <label className="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                        <input
                                          type="checkbox"
                                          className="kt-checkable"
                                          value={c._id}
                                          checked={c.IsChecked}
                                          onChange={e => {
                                            c.IsChecked = !c.IsChecked;
                                            this.setState(this.state);
                                          }}
                                        />
                                        <span />
                                      </label>
                                    </td>
                                    <td>
                                      <Link to={`/admin/brand/update/${c.Id}`}>
                                        {c.Name}
                                      </Link>
                                    </td>
                                    <td>
                                      <DelayInput
                                        delayTimeout={2000}
                                        type="text"
                                        name="Sort"
                                        value={
                                          parseFloat(c.Sort) == ""
                                            ? "0"
                                            : c.Sort
                                        }
                                        id=""
                                        index={index}
                                        className="form-control"
                                        onChange={this.handleChangeDataRow}
                                      />
                                    </td>
                                  </tr>
                                );
                              })}
                            </tbody>
                          </table>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-sm-12 col-md-5">
                          <div
                            className="dataTables_info"
                            id="kt_table_1_info"
                            role="status"
                            aria-live="polite"
                          >
                            {total[this.context.Language]} {this.state.model.TotalItemCount} {record[this.context.Language]}
                          </div>
                        </div>
                        <div className="col-sm-12 col-md-7 dataTables_pager">
                          <PagedList
                            currentpage={this.state.model.CurrentPage}
                            pagesize={this.state.model.PageSize}
                            totalitemcount={this.state.model.TotalItemCount}
                            totalpagecount={this.state.model.TotalPageCount}
                            ajaxcallback={this.toPage.bind(this)}
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
           <div class="modal fade show" id="kt_modal_4" tabindex="-1" role="dialog" aria-labelledby="VideoAddUpdate" aria-modal="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title" id="VideoAddUpdate">New message</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            </button>
                        </div>
                        <div class="modal-body">
                            <form>
                                <div class="form-group">
                                    <label for="recipient-name" class="form-control-label">Recipient:</label>
                                    <input type="text" class="form-control" id="recipient-name" />
                                </div>
                                <div class="form-group">
                                    <label for="message-text" class="form-control-label">Message:</label>
                                    <textarea class="form-control" id="message-text"></textarea>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary">Send message</button>
                        </div>
                    </div>
                </div>
            </div>
          </React.Fragment>
        ) : (
          <div />
        )}
      </React.Fragment>
    );
  }
}
brandList.contextType = AppContext;
export default brandList;
