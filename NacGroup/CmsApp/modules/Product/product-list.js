import React, { Component } from "react";
import uploadPlugin from "../../plugins/upload-plugin";
import { DelayInput } from "react-delay-input";
import PagedList from "../../components/pagedlist";
import {
    globalErrorMessage,
    error,
    success,
    back,
    add,
    save,
    close,
    remove,
    total,
    record,
    page,
    cDate,
    lDate,
    recordPerPage,
    order,
    sort
} from "../../constants/message";
import {
    sku_code,
    product,
    product_list,
    product_name,
    category,
    se_category,
    infor,
    classify,
    main_cate,
    related_cate,
    se_main_cate,
    se_related_cate,
    se_brand,
    name,
    description,
    avatar,
    value,
    seo_title,
    seo_des,
    seo_key,
    add_product,
    update_product,
} from "./models/productstaticmessage";
import AppContext from "../../components/app-context";
import { Link } from "react-router-dom";
import moment from "moment";
import FilterListPlugin from "../../plugins/filter-list-plugin";
import RemoveListPlugin from "../../plugins/remove-list-plugin";
const apiurl = "/cms/api/product";

class productList extends Component {
    constructor(props, context) {
        super(props, context);
        var that = this;
        this.ProductListBox = React.createRef();
        that.state = {
            ex: {
                Title: product_list[this.context.Language],
                TempSkus: [],
                Param: {
                    name: null,
                    pagesize: null,
                    categoryid: null,
                    skucode: null
                },
                CategoryList: []
            }
        };
        $.get("/cms/api/productcategory", res => {
            that.state.ex.CategoryList = res.data.Results;
            that.setState(that.state);
        });

        that.handleChangeFilter = that.handleChangeFilter.bind(that);
        that.handleChangeDataRow = that.handleChangeDataRow.bind(that);
        this.toPage(1);
    }

    toPage(index) {
        FilterListPlugin.filterdata(index, this, apiurl);
    }
    /**
     * Được gọi khi thay đổi các trường filter cập nhật tham số filter vào state
     * Filter dữ liệu theo tham số mới
     * @param {Event} event
     */
    handleChangeFilter(event) {
        FilterListPlugin.handleChangeFilter(event, this);
        this.toPage(1);
    }

    /**
     * Xóa data đã được chọn
     */
    handleDeleteDataRow() {
        RemoveListPlugin.removeList(`${apiurl}/removelist`, this);
    }
    UpdateAllProduct() {
        var that = this;
        KTApp.blockPage();
        $.ajax({
            url: apiurl + "/updateall",
            type: "POST",
            dataType: "json",
            contentType: "application/json",    
            success: response => {
                KTApp.unblockPage();
                toastr.clear();
                if (response.status == "success") {
                    swal.fire({
                        title: success[this.context.Language],
                        text: response.message,
                        type: "success",
                        onClose: () => {
                            window.location.reload();
                        }
                    });
                    that.setState(that.state);
                    toastr["success"](response.message, success[this.context.Language]);
                } else {
                    toastr["error"](response.message, error[this.context.Language]);
                }
            },
            error: function (er) {
                KTApp.unblockPage();
                toastr.clear();
                toastr["error"](globalErrorMessage[this.context.Language], error[this.context.Language]);
            }
        });
    }
    hanđleUpdateSort() {
        var that = this;
        KTApp.blockPage();
        $.ajax({
            url: apiurl + "/updatesort",
            type: "POST",
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify(that.state.ex.TempSkus),
            success: response => {
                KTApp.unblockPage();
                toastr.clear();
                if (response.status == "success") {
                    $("#save-sort").removeClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light disabled");
                    swal.fire({
                        title: success[this.context.Language],
                        text: response.message,
                        type: "success",
                        onClose: () => {
                            window.location.reload();
                        }
                    });
                    that.setState(that.state);
                    toastr["success"](response.message, success[this.context.Language]);
                } else {
                    toastr["error"](response.message, error[this.context.Language]);
                }
            },
            error: function (er) {
                KTApp.unblockPage();
                toastr.clear();
                toastr["error"](globalErrorMessage[this.context.Language], error[this.context.Language]);
            }
        });
    }
    handleChangeDataByCategoryId() {
        var that = this;
        KTApp.blockPage();
        $.ajax({
            url: apiurl + "?categoryid=" + that.state.ex.Param.categoryid + "&pagesize=" + that.state.ex.Param.pagesize,
            type: "GET",
            success: response => {
                KTApp.unblockPage();
                toastr.clear();
                if (response.status == "success") {
                    that.state.ex.TempSkus = response.data.Results;
                    that.setState(that.state);
                    toastr["success"](response.message, success[this.context.Language]);
                } else {
                    toastr["error"](response.message, error[this.context.Language]);
                }
            },
            error: function (er) {
                KTApp.unblockPage();
                toastr.clear();
                toastr["error"](globalErrorMessage[this.context.Language], error[this.context.Language]);
            }
        });
    }
    handleChangeDataRow(event) {
        const target = event.target;
        const value = target.type === "checkbox" ? target.checked : target.value;
        const name = target.name;
        const index = target.getAttribute("index");
        var newobj = { ...this.state.model.Results[index] };
        newobj[name] = isNaN(value) ? value : parseInt(value);
        var that = this;
        KTApp.blockPage();
        $.ajax({
            url: apiurl + "/updatecustomize",
            type: "POST",
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify({ model: newobj, name }),
            success: response => {
                KTApp.unblockPage();
                toastr.clear();
                if (response.status == "success") {
                    that.state.model.Results[index][name] = newobj[name];
                    that.setState(that.state);
                    toastr["success"](response.message, success[this.context.Language]);
                } else {
                    toastr["error"](response.message, error[this.context.Language]);
                }
            },
            error: function (er) {
                KTApp.unblockPage();
                toastr.clear();
                toastr["error"](globalErrorMessage[this.context.Language], error[this.context.Language]);
            }
        });
    }
    componentWillMount() {
        $("#cssloading").html(
            `<link href="/adminstatics/global/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />`
        );
        $("#scriptloading").html(
            `<script src="/adminstatics/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>`
        );
    }
    componentDidMount() {
        //Sửa title trang
        document.title = this.state.ex.Title;
        //Active hight light menu
        $("#kt_aside_menu .kt-menu__item").removeClass("kt-menu__item--active");
        $("#kt_aside_menu .kt-menu__item[data-id='san-pham']").addClass(
            "kt-menu__item--active"
        );
        $("#kt_aside_menu .kt-menu__item[data-id='danh-sach-san-pham']").addClass(
            "kt-menu__item--active"
        );
        var that = this;
        $(that.ProductListBox.current).sortable({
            placeholder: "sortable-placeholder",
            update: (event, ui) => {
                var sortedList = [];
                $(that.ProductListBox.current)
                    .children()
                    .each((index, ele) => {
                        sortedList.push(
                            that.state.ex.TempSkus.find(s => s.Id == $(ele).data("id"))
                        );
                        sortedList[sortedList.length - 1].Sort = index;
                        $($(ele).children()[0]).text(index);
                    });

                that.state.ex.TempSkus = sortedList;
            }
        });
    }
    render() {
        return (
            <React.Fragment>
                {this.state && this.state.model ? (
                    <React.Fragment>
                        <div className="kt-subheader kt-grid__item" id="kt_subheader">
                            <div className="kt-subheader__main">
                                <div className="kt-subheader__breadcrumbs">
                                    <Link
                                        to="/admin/dashboard"
                                        className="kt-subheader__breadcrumbs-home"
                                    >
                                        <i className="fa fa-home" />
                                    </Link>
                                    <span className="kt-subheader__breadcrumbs-separator" />
                                    <span className="kt-subheader__breadcrumbs-link">
                                        {product[this.context.Language]}
                                    </span>
                                    <span className="kt-subheader__breadcrumbs-separator" />
                                    <span className="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
                                        {this.state.ex.Title}
                                    </span>
                                </div>
                            </div>
                            <div className="kt-subheader__toolbar">
                                <div className="kt-subheader__wrapper">
                                    <a
                                        href="javascript:"
                                        className="btn btn-primary btn-customfile"
                                        onClick={e => {

                                            //this.state.ex.TempSkus = this.state.model.Skus.filter(
                                            //    () => true
                                            //);
                                            //this.setState(this.state);
                                            $("#list_sort_sku").modal("show");
                                        }}
                                    >
                                        <i className="fa fa-sort" />
                                        Sort
                                    </a>
                                    <a
                                        href="javascript:"
                                        className="btn btn-primary btn-customfile"
                                    >
                                        <input
                                            type="file"

                                            onChange={e => {
                                                var that = this;

                                                uploadPlugin.ImportExcel(e, data => {


                                                });
                                            }}
                                        />
                                        <i className="fa fa-plus" />
                                        Import Product Excel
                                    </a>
                                    <a
                                        href="javascript:"
                                        className="btn btn-primary btn-customfile"
                                        onClick={
                                            e => this.UpdateAllProduct()
                                        }
                                    >
                                       
                                        <i className="fa fa-plus" />
                                        Update All Product
                                    </a>
                                    <Link to="/admin/product/add" className="btn btn-primary">
                                        <i className="fa fa-plus" /> {add[this.context.Language]}
                                    </Link>

                                    <a
                                        href="javascript:;"
                                        onClick={e => this.handleDeleteDataRow()}
                                        className="btn btn-danger"
                                    >
                                        <i className="fa fa-trash-alt" /> {remove[this.context.Language]}
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div
                            className="kt-content kt-grid__item kt-grid__item--fluid"
                            id="kt_content"
                        >
                            <div className="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                                <div className="kt-portlet kt-portlet--mobile">
                                    <div className="kt-portlet__head kt-portlet__head--lg">
                                        <div className="kt-portlet__head-label">
                                            <h3 className="kt-portlet__head-title">
                                                {this.state.ex.Title}
                                            </h3>
                                        </div>
                                    </div>

                                    <div className="kt-portlet__body">
                                        <form className="kt-form kt-form--fit">
                                            <div className="row kt-margin-b-20">
                                                <div className="col-lg-3">
                                                    <label>{product_name[this.context.Language]}:</label>
                                                    <DelayInput
                                                        delayTimeout={1000}
                                                        className="form-control kt-input"
                                                        placeholder={product_name[this.context.Language]}
                                                        value={this.state.ex.Param.name}
                                                        name="name"
                                                        onChange={this.handleChangeFilter}
                                                    />
                                                </div>
                                                <div className="col-lg-3">
                                                    <label>{sku_code[this.context.Language]}:</label>
                                                    <DelayInput
                                                        delayTimeout={1000}
                                                        className="form-control kt-input"
                                                        placeholder={sku_code[this.context.Language]}
                                                        value={this.state.ex.Param.skucode}
                                                        name="skucode"
                                                        onChange={this.handleChangeFilter}
                                                    />
                                                </div>
                                                <div className="col-lg-3">
                                                    <label>{category[this.context.Language]}:</label>
                                                    <select
                                                        className="form-control kt-input"
                                                        data-col-index="2"
                                                        value={this.state.ex.Param.categoryid}
                                                        name="categoryid"
                                                        onChange={this.handleChangeFilter}
                                                    >
                                                        <option value="">{se_category[this.context.Language]}</option>
                                                        {this.state.ex.CategoryList.map(cat => {
                                                            return <option value={cat.Id}>{cat.Name}</option>;
                                                        })}
                                                    </select>
                                                </div>
                                                <div className="col-lg-3">
                                                    <label>{recordPerPage[this.context.Language]}:</label>
                                                    <select
                                                        className="form-control kt-input"
                                                        data-col-index="2"
                                                        value={this.state.ex.Param.pagesize}
                                                        name="pagesize"
                                                        onChange={this.handleChangeFilter}
                                                    >
                                                        <option value="10">10 {record[this.context.Language]} / {page[this.context.Language]}</option>
                                                        <option value="20">20 {record[this.context.Language]} / {page[this.context.Language]}</option>
                                                        <option value="50">50 {record[this.context.Language]} / {page[this.context.Language]}</option>
                                                        <option value="100">100 {record[this.context.Language]} / {page[this.context.Language]}</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </form>

                                        <div className="kt-separator kt-separator--border-dashed" />

                                        <div
                                            id="kt_table_1_wrapper"
                                            className="dataTables_wrapper dt-bootstrap4"
                                        >
                                            <div className="row">
                                                <div className="col-sm-12">
                                                    <table className="table table-striped- table-bordered table-hover table-checkable dataTable no-footer dtr-inline table-responsive">
                                                        <thead>
                                                            <tr role="row">
                                                                <th
                                                                    className="dt-right sorting_disabled"
                                                                    rowspan="1"
                                                                    colspan="1"
                                                                    style={{ width: "1%" }}
                                                                    aria-label="Record ID"
                                                                >
                                                                    <label className="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                                                        <input
                                                                            type="checkbox"
                                                                            className="kt-group-checkable"
                                                                            onChange={ev => {
                                                                                this.state.model.Results = this.state.model.Results.map(
                                                                                    e => {
                                                                                        return {
                                                                                            ...e,
                                                                                            IsChecked: ev.target.checked
                                                                                        };
                                                                                    }
                                                                                );
                                                                                this.setState(this.state);
                                                                            }}
                                                                        />
                                                                        <span />
                                                                    </label>
                                                                </th>
                                                                <th>{avatar[this.context.Language]}</th>
                                                                <th>{name[this.context.Language]}</th>
                                                                <th>{category[this.context.Language]}</th>
                                                                <th>{sort[this.context.Language]}</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            {this.state.model.Results.map((c, index) => {
                                                                return (
                                                                    <tr>
                                                                        <td className="dt-right" tabindex="0">
                                                                            <label className="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                                                                <input
                                                                                    type="checkbox"
                                                                                    className="kt-checkable"
                                                                                    value={c._id}
                                                                                    checked={c.IsChecked}
                                                                                    onChange={e => {
                                                                                        c.IsChecked = !c.IsChecked;
                                                                                        this.setState(this.state);
                                                                                    }}
                                                                                />
                                                                                <span />
                                                                            </label>
                                                                        </td>
                                                                        <td>
                                                                            <Link
                                                                                to={`/admin/product/update/${c.Id}`}
                                                                            >
                                                                                <img
                                                                                    src={c.Avatar}
                                                                                    alt=""
                                                                                    className="ecs-img-thumbnail-m"
                                                                                    height="50"
                                                                                />
                                                                            </Link>
                                                                        </td>
                                                                        <td>
                                                                            <Link
                                                                                to={`/admin/product/update/${c.Id}`}
                                                                            >
                                                                                {c.Name}
                                                                            </Link>
                                                                        </td>
                                                                        <td>{c.MainCategory.Name}</td>

                                                                        <td>
                                                                            <DelayInput
                                                                                delayTimeout={2000}
                                                                                type="text"
                                                                                name="Sort"
                                                                                value={
                                                                                    parseFloat(c.Sort) == ""
                                                                                        ? "0"
                                                                                        : c.Sort
                                                                                }
                                                                                id=""
                                                                                index={index}
                                                                                className="form-control"
                                                                                onChange={this.handleChangeDataRow}
                                                                            />
                                                                        </td>
                                                                    </tr>
                                                                );
                                                            })}
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-sm-12 col-md-5">
                                                    <div
                                                        className="dataTables_info"
                                                        id="kt_table_1_info"
                                                        role="status"
                                                        aria-live="polite"
                                                    >
                                                        {total[this.context.Language]} {this.state.model.TotalItemCount} {record[this.context.Language]}
                                                    </div>
                                                </div>
                                                <div className="col-sm-12 col-md-7 dataTables_pager">
                                                    <PagedList
                                                        currentpage={this.state.model.CurrentPage}
                                                        pagesize={this.state.model.PageSize}
                                                        totalitemcount={this.state.model.TotalItemCount}
                                                        totalpagecount={this.state.model.TotalPageCount}
                                                        ajaxcallback={this.toPage.bind(this)}
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </React.Fragment>
                ) : (
                        <div />
                    )}
                <div
                    className="modal fade"
                    id="list_sort_sku"
                    tabindex="-1"
                    role="dialog"
                    aria-labelledby="exampleModalLabel2"
                    aria-hidden="true"
                >
                    <div className="modal-dialog modal-lg" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" id="exampleModalLabel2">
                                    Sort Product
                                </h5>
                                <button
                                    type="button"
                                    className="close"
                                    data-dismiss="modal"
                                    aria-label="Close"
                                />
                            </div>
                            <div className="modal-body">
                                <div className="row">
                                    <div className="col-lg-3">
                                        <label>{category[this.context.Language]}:</label>
                                        <select
                                            className="form-control kt-input"
                                            data-col-index="2"
                                            value={this.state.ex.Param.categoryid}
                                            name="categoryid"
                                            onChange={e => {
                                                this.state.ex.Param.categoryid = e.target.value;
                                                this.setState(this.state);
                                                this.handleChangeDataByCategoryId();
                                            }}
                                        >
                                            <option value="">{se_category[this.context.Language]}</option>
                                            {this.state.ex.CategoryList.map(cat => {
                                                return <option value={cat.Id}>{cat.Name}</option>;
                                            })}
                                        </select>
                                    </div>
                                    <div className="col-lg-3">
                                        <label>{recordPerPage[this.context.Language]}:</label>
                                        <select
                                            className="form-control kt-input"
                                            data-col-index="2"
                                            value={this.state.ex.Param.pagesize}
                                            name="pagesize"
                                            onChange={this.handleChangeFilter}
                                        >
                                            <option value="10">10 {record[this.context.Language]} / {page[this.context.Language]}</option>
                                            <option value="20">20 {record[this.context.Language]} / {page[this.context.Language]}</option>
                                            <option value="50">50 {record[this.context.Language]} / {page[this.context.Language]}</option>
                                            <option value="100">100 {record[this.context.Language]} / {page[this.context.Language]}</option>
                                        </select>
                                    </div>
                                </div>
                                <div className="dataTables_wrapper dt-bootstrap4">
                                    <table className="table table-striped- table-bordered table-hover table-checkable dataTable no-footer dtr-inline">
                                        <thead>
                                            <tr>
                                                <th>STT</th>
                                                <th>Sku Id</th>
                                                <th>Sku Name</th>
                                            </tr>
                                        </thead>

                                        <tbody ref={this.ProductListBox} className="textlist-2">
                                            {this.state.ex.TempSkus.map((ele, index) => {
                                                return (
                                                    <tr data-id={ele.Id}>
                                                        <td>{ele.Sort}</td>
                                                        <td>{ele.Id}</td>
                                                        <td>{ele.Name}</td>
                                                    </tr>
                                                );
                                            })}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div className="modal-footer">
                                <button
                                    type="button"
                                    className="btn btn-default"
                                    data-dismiss="modal"
                                >
                                    {close[this.context.Language]}
                                </button>
                                <button
                                    type="button"
                                    id="save-sort"
                                    className="btn btn-primary"
                                    onClick={() => {
                                        //$("#list_sort_sku").modal("hide");
                                        $("#save-sort").addClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light disabled");
                                        this.hanđleUpdateSort();
                                    }}
                                >
                                    Save
                    </button>
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}
productList.contextType = AppContext;
export default productList;
