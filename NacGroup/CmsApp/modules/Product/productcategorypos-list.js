import React, { Component } from "react";
import { Link } from "react-router-dom";
import uploadPlugin from "../../plugins/upload-plugin";
import {
    globalErrorMessage,
    error,
    success,
    total,
    record,
    clearcache,
} from "../../constants/message";
import {
    pos_products_cat_list,
    name,
    avatar,
    pos_products,
} from "./models/productstaticmessage";
import AppContext from "../../components/app-context";
const apiurl = "/cms/api/productcategorygpos";

class ProductCategoryPosList extends Component {
    constructor(props, context) {
    super(props, context);
    var that = this;
    that.state = {
      model: null,
      ex: {
        Title: pos_products_cat_list[this.context.Language]
      }
    };
    document.title = that.state.ex.Title;
    KTApp.blockPage();
    $.ajax({
      url: apiurl,
      type: "GET",
      dataType: "json",
      contentType: "application/json",
      success: response => {
        KTApp.unblockPage();
        toastr.clear();
        if (response.status === "success") {
          that.state.model = response.data;
          that.setState(that.state);
        } else {
          toastr["error"](response.message, error[this.context.Language]);
        }
      },
      error: function(er) {
        KTApp.blockPage();
        toastr.clear();
        toastr["error"](globalErrorMessage[this.context.Language], error[this.context.Language]);
      }
    });
  }

  updateAvatar(category) {
    var that = this;
    KTApp.blockPage();
    $.ajax({
      url: `${apiurl}/UpdateCustomize`,
      type: "POST",
      dataType: "json",
      contentType: "application/json",
      data: JSON.stringify({
        model: category,
        name: "Avatar"
      }),
      success: response => {
        KTApp.unblockPage();
        toastr.clear();
        if (response.status == "success") {
          that.setState(that.state);
        } else {
            toastr["error"](response.message, error[this.context.Language]);
        }
      },
      error: function(er) {
        KTApp.unblockPage();
        toastr.clear();
        toastr["error"](globalErrorMessage[this.context.Language], error[this.context.Language]);
      }
    });
  }

  componentWillMount() {}
  componentDidMount() {
    document.title = this.state.ex.Title;

    $("#kt_aside_menu .kt-menu__item").removeClass("kt-menu__item--active");
    $("#kt_aside_menu .kt-menu__item[data-id='san-pham-pos']").addClass(
      "kt-menu__item--active"
    );
    $(
      "#kt_aside_menu .kt-menu__item[data-id='danh-muc-san-pham-pos']"
    ).addClass("kt-menu__item--active");
  }
  render() {
    return (
      <React.Fragment>
        {this.state && this.state.model ? (
          <React.Fragment>
            <div className="kt-subheader kt-grid__item" id="kt_subheader">
              <div className="kt-subheader__main">
                <div className="kt-subheader__breadcrumbs">
                  <Link
                    to="/admin/dashboard"
                    className="kt-subheader__breadcrumbs-home"
                  >
                    <i className="fa fa-home" />
                  </Link>
                  <span className="kt-subheader__breadcrumbs-separator" />
                  <span className="kt-subheader__breadcrumbs-link">
                    {pos_products[this.context.Language]}
                  </span>
                  <span className="kt-subheader__breadcrumbs-separator" />
                  <span className="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
                    {this.state.ex.Title}
                  </span>
                </div>
              </div>
              <div className="kt-subheader__toolbar"></div>
            </div>
            <div
              className="kt-content kt-grid__item kt-grid__item--fluid"
              id="kt_content"
            >
              <div className="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                <div className="row">
                  <div className="col-lg-6">
                    <div className="kt-portlet kt-portlet--mobile">
                      <div className="kt-portlet__head kt-portlet__head--lg">
                        <div className="kt-portlet__head-label">
                          <h3 className="kt-portlet__head-title">
                            {this.state.ex.Title}
                          </h3>
                        </div>
                      </div>

                      <div className="kt-portlet__body">
                        <div
                          id="kt_table_1_wrapper"
                          className="dataTables_wrapper dt-bootstrap4"
                        >
                          <div className="row">
                            <div className="col-sm-12">
                              <table className="table table-striped- table-bordered table-hover table-checkable dataTable no-footer dtr-inline table-responsive">
                                <thead>
                                  <tr role="row">
                                    <th>{name[this.context.Language]}</th>
                                    <th>{avatar[this.context.Language]}</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  {this.state.model.map((cate, index) => {
                                    return (
                                      <tr>
                                        <td>{cate.Name}</td>
                                        <td>
                                          <div
                                            className="form-group"
                                            style={{ position: "relative" }}
                                          >
                                            <input
                                              type="file"
                                              className="form-control"
                                              style={{
                                                position: "absolute",
                                                zIndex: 99,
                                                top: 0,
                                                width: "100%",
                                                height: "100%",
                                                opacity: 0
                                              }}
                                              onChange={e => {
                                                uploadPlugin.UpdateImage(
                                                  e,
                                                  files => {
                                                    cate.Avatar = files[0];
                                                    this.updateAvatar(cate);
                                                  }
                                                );
                                              }}
                                            />
                                            <img
                                              src={
                                                cate.Avatar
                                                  ? cate.Avatar
                                                  : "/adminstatics/global/img/no-image.png"
                                              }
                                              style={{ height: "100px" }}
                                            />
                                          </div>
                                        </td>
                                      </tr>
                                    );
                                  })}
                                </tbody>
                              </table>
                            </div>
                          </div>
                          <div className="row">
                            <div className="col-sm-12 col-md-5">
                              <div
                                className="dataTables_info"
                                id="kt_table_1_info"
                                role="status"
                                aria-live="polite"
                              >
                              {total[this.context.Language]} {this.state.model.length} {record[this.context.Language]}
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </React.Fragment>
        ) : (
          <div />
        )}
      </React.Fragment>
    );
  }
}
ProductCategoryPosList.contextType = AppContext;
export default ProductCategoryPosList;
