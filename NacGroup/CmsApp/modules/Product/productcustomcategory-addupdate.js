import React, { Component } from "react";
import productCategoryCmsModel from "./models/productcategory-cmsmodel";
import { Link } from "react-router-dom";
import uploadPlugin from "../../plugins/upload-plugin";
import { GLOBAL_ERROR_MESSAGE, ERROR, SUCCESS } from "../../constants/message";

//ckeditor
import CkEditor from "../../components/ckeditor";

var apiurl = "/cms/api/productcustomcategory";
class productCategoryAddUpdate extends Component {
  constructor(props) {
    super(props);
    var action = null;
    if (document.location.href.indexOf("/admin/productcategory/add") >= 0) {
      action = "add";
    } else if (
      document.location.href.indexOf("/admin/productcategory/update") >= 0
    ) {
      action = "update";
    }
    this.state = {
      model: new productCategoryCmsModel(),
      ex: {
        Title: null,
        Action: action
      }
    };
    var that = this;

    if (action === "update") {
      //console.log(this.props);

      that.state.ex.Title = "Update Product Category";
      var id = that.props.match.params.id;
      KTApp.blockPage();
      $.ajax({
        url: apiurl + "/" + that.state.ex.Action,
        type: "GET",
        dataType: "json",
        contentType: "application/json",
        data: { id },
        success: response => {
          KTApp.unblockPage();
          toastr.clear();
          if (response.status == "success") {
            that.state.model = response.data;
            document.title = that.state.ex.Title;
            that.setState(that.state);
          } else {
            toastr["error"](response.message, ERROR);
          }
        },
        error: function(er) {
          KTApp.unblockPage();
          toastr.clear();
          toastr["error"](GLOBAL_ERROR_MESSAGE, ERROR);
        }
      });
    } else {
      this.state.ex.Title = "Add Product Category";
      this.setState(this.state);
    }
  }
  componentWillMount() {
    $("#scriptloading").html(
      `<script src="/adminstatics/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>`
    );
  }

  componentDidMount() {
    document.title = this.state.ex.Title;
    $("#kt_aside_menu .kt-menu__item").removeClass("kt-menu__item--active");
    $("#kt_aside_menu .kt-menu__item[data-id='san-pham']").addClass(
      "kt-menu__item--active"
    );
    $("#kt_aside_menu .kt-menu__item[data-id='danh-muc-san-pham']").addClass(
      "kt-menu__item--active"
    );
  }

  componentWillUnmount() {
    $("#scriptloading").html("");
  }

  // this.setState(this.state);
  submitForm() {
    var that = this;
    //var id = that.props.match.params.id;
    KTApp.blockPage();
    $.ajax({
      url: apiurl + "/" + that.state.ex.Action,
      type: "POST",
      dataType: "json",
      contentType: "application/json",
      data: JSON.stringify(that.state.model),
      success: response => {
        KTApp.unblockPage();
        if (response.status == "success") {
          swal.fire({
            title: SUCCESS,
            text: response.message,
            type: "success",
            onClose: () => {
              that.props.history.push("/admin/emptypage");
              that.props.history.replace({
                pathname: that.props.location.pathname
              });
            }
          });
        } else {
          toastr["error"](response.message, ERROR);
        }
      },
      error: function(er) {
        KTApp.unblockPage();
        toastr["error"](GLOBAL_ERROR_MESSAGE, ERROR);
      }
    });
  }

  render() {
    return (
      <React.Fragment>
        {this.state && this.state.model ? (
          <React.Fragment>
            <div className="kt-subheader kt-grid__item" id="kt_subheader">
              <div className="kt-subheader__main">
                <div className="kt-subheader__breadcrumbs">
                  <Link
                    to="/admin/dashboard"
                    className="kt-subheader__breadcrumbs-home"
                  >
                    <i className="fa fa-home" />
                  </Link>
                  <span className="kt-subheader__breadcrumbs-separator" />
                  <span className="kt-subheader__breadcrumbs-link">
                    Products
                  </span>
                  <span className="kt-subheader__breadcrumbs-separator" />
                  <Link
                    to="/admin/productcategory"
                    className="kt-subheader__breadcrumbs-link"
                  >
                    Product Categories
                  </Link>
                  <span className="kt-subheader__breadcrumbs-separator" />
                  <span className="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
                    {this.state.ex.Title}
                  </span>
                </div>
              </div>
              <div className="kt-subheader__toolbar">
                <div className="kt-subheader__wrapper">
                  <Link
                    to="/admin/productcategory"
                    className="btn btn-secondary"
                  >
                    <i className="fa fa-chevron-left" /> Back
                  </Link>
                  <a
                    href="javascript:;"
                    className="btn btn-primary"
                    onClick={() => {
                      this.submitForm();
                    }}
                  >
                    <i className="fa fa-save" /> Save
                  </a>
                </div>
              </div>
            </div>
            <div
              className="kt-content kt-grid__item kt-grid__item--fluid"
              id="kt_content"
            >
              <div className="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                <div className="row">
                  <div className="col-lg-8">
                    <div className="kt-portlet kt-portlet--mobile">
                      <div className="kt-portlet__head kt-portlet__head--lg">
                        <div className="kt-portlet__head-label">
                          <h3 className="kt-portlet__head-title">
                            Information
                          </h3>
                        </div>
                      </div>

                      <div className="kt-portlet__body">
                        <div className="form-group">
                          <label className="control-label">Name</label>
                          <input
                            className="form-control"
                            value={this.state.model.Name}
                            placeholder="Name"
                            onChange={e => {
                              this.state.model.Name = e.target.value;
                              this.state.model.MetaTitle = e.target.value;
                              this.state.model.Url = e.target.value.cleanUnicode();
                              this.setState(this.state);
                            }}
                          />
                        </div>
                        <div className="form-group">
                          <label className="control-label">Description</label>
                          <CkEditor
                            id="PageContent"
                            value={this.state.model.Description}
                            onChange={e => {
                              this.state.model.Description = e;
                              this.setState(this.state);
                            }}
                          />
                        </div>
                        <div className="form-group">
                          <label className="control-label">Order</label>
                          <input
                            type="number"
                            min="0"
                            max="1000"
                            value={this.state.model.Sort}
                            onChange={e => {
                              this.state.model.Sort = e.target.value;
                              this.setState(this.state);
                            }}
                            className="form-control"
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-4">
                    <div className="kt-portlet kt-portlet--mobile">
                      <div className="kt-portlet__head kt-portlet__head--lg">
                        <div className="kt-portlet__head-label">
                          <h3 className="kt-portlet__head-title">Avatar</h3>
                        </div>
                      </div>

                      <div className="kt-portlet__body">
                        <div className="form-group item-avatar">
                          <input
                            type="file"
                            className="form-control"
                            onChange={e => {
                              var that = this;
                              uploadPlugin.UpdateImage(e, listfile => {
                                that.state.model.Avatar = listfile[0];
                                that.setState(that.state);
                              });
                            }}
                          />
                          <img
                            src={this.state.model.Avatar}
                            className="img-responsive"
                            height="100"
                          />
                        </div>
                      </div>
                    </div>

                    <div className="kt-portlet kt-portlet--mobile">
                      <div className="kt-portlet__head kt-portlet__head--lg">
                        <div className="kt-portlet__head-label">
                          <h3 className="kt-portlet__head-title">SEO</h3>
                        </div>
                      </div>

                      <div className="kt-portlet__body">
                        <div className="form-group">
                          <label className="control-label">SEO Title</label>
                          <input
                            className="form-control"
                            placeholder="SEO Title"
                            maxlength="70"
                            value={this.state.model.MetaTitle}
                            onChange={e => {
                              this.state.model.MetaTitle = e.target.value;
                              this.setState(this.state);
                            }}
                          />
                        </div>
                        <div className="form-group">
                          <label className="control-label">
                            SEO Description
                          </label>
                          <textarea
                            className="form-control"
                            placeholder="SEO Description"
                            maxlength="160"
                            id="MetaDescription"
                            rows="5"
                            value={this.state.model.MetaDescription}
                            onChange={e => {
                              this.state.model.MetaDescription = e.target.value;
                              this.setState(this.state);
                            }}
                          />
                        </div>
                        <div className="form-group">
                          <label className="control-label">SEO Keyword</label>
                          <input
                            className="form-control"
                            placeholder="SEO Keyword"
                            value={this.state.model.MetaKeyword}
                            onChange={e => {
                              this.state.model.MetaKeyword = e.target.value;
                              this.setState(this.state);
                            }}
                          />
                        </div>
                        <div className="form-group">
                          <label className="control-label">Url</label>
                          <input
                            className="form-control"
                            placeholder="Url slug"
                            value={this.state.model.Url}
                            onChange={e => {
                              this.state.model.Url = e.target.value;
                              this.setState(this.state);
                            }}
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </React.Fragment>
        ) : (
          <div />
        )}
      </React.Fragment>
    );
  }
}

export default productCategoryAddUpdate;
