class brandCmsModel {
    constructor() {
        this.Id = null;
        this.Name = null;
        this.Description = null;
        this.MetaKeyword = null;
        this.MetaDescription = null;
        this.MetaTitle = null;
        this.Avatar = null;
        this.Url = null;
        this.Sort = 0;
    }
}

module.exports = brandCmsModel;
