class productCmsModel {
    constructor() {
        this.Id = null;
        this.Name = null;
        this.Avatar = null;
        this.ShortDescription = null;
        this.Description = null;
        this.MetaKeyword = null;
        this.MetaDescription = null;
        this.MetaTitle = null;
        this.Url = null;
        this.ShippingCategory = null;
        this.Sort = 0;
        this.Skus = [];
        this.AttributeGroups = [];
        this.SkuAttributes = [];
        this.Brand = null;
        this.Label = null;
        this.Categories = [];
        this.MainCategory = null;
        this.CreatedDate = new Date();
        this.UpdatedDate = new Date();
        this.IsActive = false;
        this.ProductImages = [];
        this.ProductType = 0;
        this.Benefits = null;
        this.Ingredient = null;
    }
}
module.exports = productCmsModel;
