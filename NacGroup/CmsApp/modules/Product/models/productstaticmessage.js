//#region product
const product = ["Products", "Sản phẩm"];
const add_product = ["Add product", "Thêm sản phẩm"];
const update_product = ["Update product", "Cập nhật sản phẩm"];
const sku_code = ["Sku Code", "Mã Sku"];
const product_list = ["Product List", "Danh sách sản phẩm"];
const product_name = ["Product Name", "Tên sản phẩm"];
const category = ["Category", "Danh mục"];
const se_category = ["-- Select Category --", "-- Chọn danh mục --"];
const infor = ["Information", "Thông tin cơ bản"];
const classify = ["Classify", "Phân loại"];
const main_cate = ["Main Category", "Danh mục chính"];
const related_cate= ["Related Category", "Danh mục liên quan"];
const se_main_cate = ["Select Main Category", "Chọn danh mục chính"];
const se_related_cate = ["Select Related Category", "Chọn danh mục liên quan"];
const se_brand = ["Select Brand", "Chọn thương hiệu"];
const name = ["Name", "Tên"];
const description = ["Description", "Mô tả"];
const avatar = ["Avatar", "Ảnh đại diện"];
const value = ["Value", "Giá trị"];
const seo_title = ["SEO Title", "Tiêu đề SEO"];
const seo_des = ["SEO Description", "Mô tả SEO"];
const seo_key = ["SEO Keyword", "Từ khóa SEO"];
const active = ["Active", "Kích hoạt"];
const action = ["Action", "Hành động"];
const stock_quantity = ["Stock Quantity", "Số lượng gốc"];
const special_price = ["Special Price", "Giá khuyến mãi"];
const add_sku = ["Add Sku", "Thêm Sku"];
const update_more = ["Update More", "Cập nhật thêm"];
const se_image = ["Select Image", "Chọn hình ảnh"];
const add_attr = ["Add Attribute", "Thêm thuộc tính"];
const specification = ["Specifications", "Thông số kĩ thuật"];
const attr_group_info = ["Attribute Group Information", "Thông tin nhóm thuộc tính"];
const se_attr_group_info = ["Select Attribute Group Information", "Chọn thông tin nhóm thuộc tính"];
const se_attr_name = ["Select Attribute Name", "Chọn tên thuộc tính"];
const se_attr_value = ["Select Attribute Value", "Chọn giá trị thuộc tính"];
const short_des = ["Short Description", "Mô tả ngắn"];
const add_image = ["Add Image", "Thêm hình ảnh"];
const ingredient = ["Ingredient", "Thành phần"];
const benefits = ["Benefits", "Lợi ích"];
const width = ["Width", "Chiều rộng"];
const height = ["Height", "Chiều dài"];
const depth = ["Depth", "Độ sâu"];
const dimension_unit = ["Dimension Unit", "Đơn vị kích thước"];
const weight = ["Weight", "Cân nặng"];
const weight_unit = ["Weight Unit", "Đơn vị cân nặng"];
const sale_tax = ["Sale Tax Rate", "Thuế suất bán hàng"];
const from_date = ["From Date", "Từ ngày"];
const to_date = ["To Date", "Đến ngày"];
const is_inventory = ["Is Inventory", "Tồn kho"];
const length = ["Length", "Chiều dài"];
const shipping_category = ["Shipping Category", "Danh mục"];
const label_category = ["Label", "Nhãn"];
const sort_sku = ["Sort Sku", "Sắp xếp Sku"];
const se_product_type = ["Select Product Type", "Chọn kiểu sản phẩm"];
const product_type = ["Product Type", "Kiểu sản phẩm"];
//#endregion
//#region attribute value
const attr_value = ["Attribute Value", "Giá trị thuộc tính"];
const add_attr_value = ["Add Attribute Value", "Thêm giá trị thuộc tình"];
//#endregion
//#region attribute name
const attr_name = ["Attribute Name", "Tên thuộc tính"];
const add_attr_name = ["Add Attribute Name", "Thêm tên thuộc tính"];
//#endregion
//#region attribute group
const attr_group = ["Attribute Group", "Nhóm thuộc tính"];
const add_attr_group = ["Add Attribute Group", "Thêm nhóm thuộc tính"];
//#endregion
//#region brand
const brand = ["Brands", "Thương hiệu"];
const add_brand = ["Add brands", "Thêm thương hiệu"];
const update_brand = ["Update brands", "Cập nhật thương hiệu"];
//#endregion
//#region product category
const product_cate = ["Product Categories", "Danh mục sản phẩm"];
const add_product_cate = ["Add Product Categories", "Thêm danh mục sản phẩm"];
const update_product_cate = ["Update Product Categories", "Cập nhật danh mục sản phẩm"];
//#endregion
//#region product Pos
const update_description = ["Update description", "Cập nhật mô tả"];
const pos_products = ["POS Products", "Sản phẩm POS"];
//#endregion
//#region product category Pos
const pos_products_cat_list = ["Product Category List", "Danh mục sản phẩm"];
//#endregion
//#region Comment
const comment_list = ["Comment - Rating List", "Danh sách bình luận - đánh giá sản phẩm"];
const comment = ["Comment - Rating", "Bình luận - Đánh giá"];
const product_have_comment = ["Product with comments - new reviews", "Sản phẩm có bình luận - đánh giá mới"];
const reviews = ["Reviews", "Đánh giá"];
const reply = ["Reply", "Trả lời"];
const hidden_web = ["Hidden on the website", "Ẩn trên web"];
const visible_web = ["Visible on the website", "Hiện trên web"];
const load_more = ["Load more", "Tải thêm"];
const cancel = ["Cancel", "Hủy bỏ"];
const spending = ["Spending", "Đang chờ"];
const done = ["Done", "Đã xử lí"];
//#endregion
//#region Comment
const label = ["Label", "Mã giảm giá"];
const color = ["Color hex", "Mã màu"];
const label_name = ["Label Name", "Tên nhãn"];
const add_label_name = ["Add Label Name", "Thêm tên nhãn"];
//#endregion
module.exports = {
    //#region product
    add_image,
    short_des,
    add_attr,
    se_attr_value,
    se_attr_name,
    attr_group_info,
    se_attr_group_info,
    specification,
    se_image,
    add_sku,
    update_more,
    active,
    action,
    stock_quantity,
    special_price,
    product,
    sku_code,
    product_list,
    product_name,
    category,
    se_category,
    infor,
    classify,
    main_cate,
    related_cate,
    se_main_cate,
    se_related_cate,
    se_brand,
    name,
    description,
    avatar,
    value,
    seo_title,
    seo_des,
    seo_key,
    add_product,
    update_product,
    ingredient,
    benefits,
    width,
    height,
    dimension_unit,
    weight,
    weight_unit,
    sale_tax,
    depth,
    from_date,
    to_date,
    is_inventory,
    length,
    shipping_category,
    label_category,
    sort_sku,
    product_type,
    //#endregion
    //#region attribute value
    attr_value,
    add_attr_value,
    //#endregion
    //#region attribute name
    attr_name,
    add_attr_name,
    //#endregion
    //#region attribute group
    attr_group,
    add_attr_group,
    //#endregion
    //#region brand
    brand,
    add_brand,
    update_brand,
    //#endregion
    //#region product category
    product_cate,
    add_product_cate,
    update_product_cate,
    //#endregion
    //#region Pos product
    pos_products,
    update_description,
    //#endregion
    //#region Pos product
    pos_products_cat_list,
    //#endregion
    //#region Comment
    comment_list,
    comment,
    product_have_comment,
    reviews,
    reply,
    hidden_web,
    visible_web,
    load_more,
    cancel,
    spending,
    done,
    se_product_type,
    //#endregion
    //#region label
    label,
    color,
    label_name,
    add_label_name
    //#endregion
};
