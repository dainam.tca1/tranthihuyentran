class videoCmsModel {
    constructor() {
        this.Id = null;
        this.Title = null;
        this.Description = null;
        this.MetaKeyword = null;
        this.MetaDescription = null;
        this.MetaTitle = null;
        this.Avatar = null;
        this.Url = null;
        this.Sort = 0;
        this.CreatedDate = new Date();
        this.UpdatedDate = new Date();
        this.Link = null;
    }
}

module.exports = videoCmsModel;
