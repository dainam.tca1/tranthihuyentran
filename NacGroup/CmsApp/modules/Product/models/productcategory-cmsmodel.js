class productCategoryCmsModel {
    constructor() {
        this.Id = null;
        this.Name = null;
        this.Description = null;
        this.Avatar = null;
        this.MetaKeyword = null;
        this.MetaDescription = null;
        this.MetaTitle = null;
        this.Url = null;
        this.Sort = 0;

        this.FilterList = [];
    }
}

module.exports = productCategoryCmsModel;
