class labelCmsModel {
    constructor() {
        this.Id = null;
        this.Name = null;
        this.Color = null;
        this.Slug = null;
        this.Sort = 0;
    }
}

module.exports = labelCmsModel;
