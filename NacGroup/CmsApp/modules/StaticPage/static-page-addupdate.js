import React, { Component } from "react";
import { Link } from "react-router-dom";
import StaticPageCmsModel from "./models/staticpage-cmsmodel";
import uploadPlugin from "../../plugins/upload-plugin";
import {
    globalErrorMessage,
    error,
    success,
    add,
    save,
    remove,
    total,
    record,
    page,
    cDate,
    lDate,
    back,
    recordPerPage
} from "../../constants/message";
import {
    staticTitle,
    staticList,
    page_name,
    name,
    addstatic,
    updatestatic,
    information,
    avatar,
    body,
    isStatic,
    isCustomer,
    seoTitle,
    seoDescription,
    seoKeyword
} from "./models/staticpagestaticmessage";
import AppContext from "../../components/app-context";
//ckeditor
import CkEditor from "../../components/ckeditor";

var apiurl = "/cms/api/staticpage";

class StaticPageAddUpdate extends Component {
  constructor(props,context) {
      super(props, context);
    var that = this;

    //action để nhận biết hiện đang add hay update
    var action = null;
    if (document.location.href.indexOf("/admin/staticpage/add") >= 0) {
      action = "add";
    } else if (
      document.location.href.indexOf("/admin/staticpage/update") >= 0
    ) {
      action = "update";
    }

    that.state = {
      model: new StaticPageCmsModel(),
      ex: {
        Title: null,
        Action: action
      }
    };
    that.setState(that.state);

    if (action === "update") {
      var id = this.props.match.params.id;
      KTApp.blockPage();
      $.ajax({
        url: apiurl + "/getupdate",
        type: "GET",
        dataType: "json",
        contentType: "application/json",
        data: { id },
        success: response => {
          KTApp.unblockPage();
          if (response.status == "success") {
              that.state.model = response.data;
              that.state.ex.Title = updatestatic[this.context.Language];
            document.title = that.state.ex.Title;
            that.setState(that.state);
          } else {
              toastr["error"](response.message, error[this.context.Language]);
          }
        },
        error: function(er) {
          KTApp.unblockPage();
            toastr["error"](globalErrorMessage[this.context.Language], error[this.context.Language]);
        }
      });
    } else {
        that.state.ex.Title = addstatic[this.context.Language];
      document.title = that.state.ex.Title;
      that.setState(that.state);
    }
  }

  submitForm() {
    var that = this;
    KTApp.blockPage();
    $.ajax({
      url: apiurl + "/" + that.state.ex.Action,
      type: "POST",
      dataType: "json",
      contentType: "application/json",
      data: JSON.stringify(that.state.model),
      success: response => {
        KTApp.unblockPage();
        if (response.status == "success") {
          swal.fire({
            title: success[this.context.Language],
            text: response.message,
            type: "success",
            onClose: () => {
              that.props.history.push("/admin/emptypage");
              that.props.history.replace({
                pathname: that.props.location.pathname,
                search: that.props.location.search
              });
            }
          });
        } else {
            toastr["error"](response.message, error[this.context.Language]);
        }
      },
      error: function(er) {
          KTApp.unblockPage();
          toastr["error"](globalErrorMessage[this.context.Language], error[this.context.Language]);
      }
    });
  }

  componentWillMount() {
    //Load script ckeditor lên
    $("#scriptloading").html(
      `<script src="/adminstatics/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>`
    );
  }

  componentDidMount() {
    //Active menu
    document.title = this.state.ex.Title;
    $("#kt_aside_menu .kt-menu__item").removeClass("kt-menu__item--active");
    $("#kt_aside_menu .kt-menu__item[data-id='trang-tinh']").addClass(
      "kt-menu__item--active"
    );
  }

  componentWillUnmount() {
    $("#scriptloading").html("");
  }
  render() {
    return (
      <React.Fragment>
        {this.state && this.state.model ? (
          <React.Fragment>
            <div className="kt-subheader kt-grid__item" id="kt_subheader">
              <div className="kt-subheader__main">
                <div className="kt-subheader__breadcrumbs">
                  <Link
                    to="/admin/dashboard"
                    className="kt-subheader__breadcrumbs-home"
                  >
                    <i className="fa fa-home" />
                  </Link>
                  <span className="kt-subheader__breadcrumbs-separator" />
                  <span className="kt-subheader__breadcrumbs-link">
                    {staticTitle[this.context.Language]}
                  </span>
                  <span className="kt-subheader__breadcrumbs-separator" />
                  <Link
                    to="/admin/staticpage"
                    className="kt-subheader__breadcrumbs-link"
                  >
                    {staticList[this.context.Language]}
                  </Link>
                  <span className="kt-subheader__breadcrumbs-separator" />
                  <span className="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
                    {this.state.ex.Title}
                  </span>
                </div>
              </div>
              <div className="kt-subheader__toolbar">
                <div className="kt-subheader__wrapper">
                  <Link to="/admin/staticpage" className="btn btn-secondary">
                    <i className="fa fa-chevron-left" />  {back[this.context.Language]}
                  </Link>
                  <a
                    href="javascript:;"
                    className="btn btn-primary"
                    onClick={() => {
                      this.submitForm();
                    }}
                  >
                    <i className="fa fa-save"></i> {save[this.context.Language]}
                  </a>
                </div>
              </div>
            </div>
            <div
              className="kt-content kt-grid__item kt-grid__item--fluid"
              id="kt_content"
            >
              <div className="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                <div className="row">
                  <div className="col-lg-8">
                    <div className="kt-portlet kt-portlet--mobile">
                      <div className="kt-portlet__head kt-portlet__head--lg">
                        <div className="kt-portlet__head-label">
                          <h3 className="kt-portlet__head-title">
                            {information[this.context.Language]}
                          </h3>
                        </div>
                      </div>

                      <div className="kt-portlet__body">
                        <div className="form-group">
                          <label className="control-label">{name[this.context.Language]}</label>
                          <input
                            className="form-control"
                            value={this.state.model.Name}
                            placeholder={name[this.context.Language]}
                            onChange={e => {
                              this.state.model.Name = e.target.value;
                              this.state.model.MetaTitle = e.target.value;
                              if (this.state.ex.Action == "add") {
                                this.state.model.Url = e.target.value.cleanUnicode();
                              }
                              this.setState(this.state);
                            }}
                          />
                        </div>
                        <div className="form-group">
                          <label className="control-label">{body[this.context.Language]}</label>
                          <CkEditor
                            id="PageContent"
                            value={this.state.model.PageContent}
                            onChange={e => {
                              this.state.model.PageContent = e;
                              this.setState(this.state);
                            }}
                          />
                        </div>
                        <div className="form-group">
                          <label className="kt-checkbox kt-checkbox-brand">
                            <input
                              type="checkbox"
                              checked={this.state.model.IsHtml}
                              value={this.state.model.IsHtml}
                              onChange={e => {
                                this.state.model.IsHtml = e.target.checked;
                                this.setState(this.state);
                              }}
                            />
                            <span />
                            {isStatic[this.context.Language]}
                          </label>
                        </div>
                          <div className="form-group">
                              <label className="kt-checkbox kt-checkbox-brand">
                                  <input
                                      type="checkbox"
                                      checked={this.state.model.IsCustomerPage}
                                      value={this.state.model.IsCustomerPage}
                                      onChange={e => {
                                          this.state.model.IsCustomerPage = e.target.checked;
                                                            this.setState(this.state);
                                                        }}
                                  />
                                  <span />
                                  {isCustomer[this.context.Language]}
                              </label>
                          </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-4">
                    <div className="kt-portlet kt-portlet--mobile">
                      <div className="kt-portlet__head kt-portlet__head--lg">
                        <div className="kt-portlet__head-label">
                          <h3 className="kt-portlet__head-title">{avatar[this.context.Language]}</h3>
                        </div>
                      </div>

                      <div className="kt-portlet__body">
                        <div className="form-group item-avatar">
                          <input
                            multiple
                            type="file"
                            className="form-control"
                            onChange={e => {
                              var that = this;
                              uploadPlugin.UpdateImage(e, listfile => {
                                that.state.model.Avatar = listfile[0];
                                that.setState(that.state);
                              });
                            }}
                          />
                          <img
                            src={this.state.model.Avatar}
                            className="img-responsive"
                            width="200"
                          />
                        </div>
                      </div>
                    </div>

                    <div className="kt-portlet kt-portlet--mobile">
                      <div className="kt-portlet__head kt-portlet__head--lg">
                        <div className="kt-portlet__head-label">
                          <h3 className="kt-portlet__head-title">SEO</h3>
                        </div>
                      </div>

                      <div className="kt-portlet__body">
                        <div className="form-group">
                          <label className="control-label">{seoTitle[this.context.Language]}</label>
                          <input
                            className="form-control"
                            placeholder={seoTitle[this.context.Language]}
                            maxlength="70"
                            value={this.state.model.MetaTitle}
                            onChange={e => {
                              this.state.model.MetaTitle = e.target.value;
                              this.setState(this.state);
                            }}
                          />
                        </div>
                        <div className="form-group">
                          <label className="control-label">
                            {seoDescription[this.context.Language]}
                          </label>
                          <textarea
                            className="form-control"
                            placeholder={seoDescription[this.context.Language]}
                            maxlength="160"
                            id="MetaDescription"
                            rows="5"
                            value={this.state.model.MetaDescription}
                            onChange={e => {
                              this.state.model.MetaDescription = e.target.value;
                              this.setState(this.state);
                            }}
                          />
                        </div>
                        <div className="form-group">
                          <label className="control-label">{seoKeyword[this.context.Language]}</label>
                          <input
                            className="form-control"
                            placeholder={seoKeyword[this.context.Language]}
                            value={this.state.model.MetaKeyword}
                            onChange={e => {
                              this.state.model.MetaKeyword = e.target.value;
                              this.setState(this.state);
                            }}
                          />
                        </div>
                        <div className="form-group">
                          <label className="control-label">Url</label>
                          <input
                            className="form-control"
                            placeholder="Url slug"
                            value={this.state.model.Url}
                            onChange={e => {
                              this.state.model.Url = e.target.value;
                              this.setState(this.state);
                            }}
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </React.Fragment>
        ) : (
          <div />
        )}
      </React.Fragment>
    );
  }
}
StaticPageAddUpdate.contextType = AppContext;
export default StaticPageAddUpdate;
