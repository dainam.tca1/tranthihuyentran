﻿class StaticPageCmsModel {
    constructor() {
        this.Id = null;
        this.Name = null;
        this.PageContent = null;
        this.MetaKeyword = null;
        this.MetaDescription = null;
        this.MetaTitle = null;
        this.Url = null;
        this.IsHtml = false;
        this.IsCustomerPage = false;
        this.Avatar = null;
    } 
}

export default StaticPageCmsModel;