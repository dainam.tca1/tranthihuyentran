const staticTitle = ["StaticPage", "Trang tĩnh"];
const staticList = ["StaticPage List", "Danh sách trang tĩnh"];
const page_name = ["Page Name", "Tên trang"];
const name = ["Name", "Tên trang"];
const addstatic = ["Add StaticPage", "Thêm mới trang tĩnh"];
const updatestatic = ["Update StaticPage", "Cập nhật trang tĩnh"];
const information = ["Information", "Thông tin"];
const avatar = ["Avatar", "Ảnh đại diện"];
const body = ["Body", "Nội dung"];
const isStatic = ["Static HTML Page", "Nội dung HTML"];
const isCustomer = ["Is Customer Page", "Trang có chỉnh sửa"];
const seoTitle = ["SEO Title", "Tiêu đề SEO"];
const seoDescription = ["SEO Description", "Mô tả SEO"];
const seoKeyword = ["SEO Keyword", "Từ khóa SEO"];
module.exports = {
    staticTitle,
    staticList,
    page_name,
    name,
    addstatic,
    updatestatic,
    information,
    avatar,
    body,
    isStatic,
    isCustomer,
    seoTitle,
    seoDescription,
    seoKeyword
};
