import React, { Component } from "react";
import { Link } from "react-router-dom";
import DeviceCmsModel from "./models/device-cmsmodel";
import Select from "react-select";
import makeAnimated from "react-select/animated";
import {
    globalErrorMessage,
    error,
    success,
    add,
    remove,
    total,
    record,
    page,
    cDate,
    lDate,
    recordPerPage,
} from "../../constants/message";
import {
    deviceList,
    deviceupdate,
    deviceadd,
    information,
    status,
    active,
    warning,
    inactive,
    code,
    cdate,
    edate,
    playing,
    devicename,
    name,
    playlist,
    playlistvideo,
    plname,
    total_video,
    action,
    play_all,
    pl_mess,
    yes,
    cancel,
    videolist,
    vname
} from "./models/digitalstaticmessage";
import AppContext from "../../components/app-context";

import { DEVICE_STATUS } from "../../constants/enums";
var apiurl = "/cms/api/digitalsignage/devices";
import moment from "moment";
class DevicesAddUpdate extends Component {
  constructor(props,context) {
      super(props, context);
    var that = this;

    //action để nhận biết hiện đang add hay update
    var action = null;
    if (document.location.href.indexOf("/admin/digital/devices/add") >= 0) {
      action = "add";
    } else if (
      document.location.href.indexOf("/admin/digital/devices/update") >= 0
    ) {
      action = "update";
    }

    that.state = {
      model: new DeviceCmsModel(),
      ex: {
        Title: null,
        Action: action,
        Type:{
          Play:null
        },
        PlayList: [],
          StatusList: [
          {value: DEVICE_STATUS.Inactive, label: inactive[this.context.Language] },
          {value:DEVICE_STATUS.Warning, label: warning[this.context.Language]},
          {value:DEVICE_STATUS.Active, label: active[this.context.Language]}
        ]
      }
    };
      that.setState(that.state);
      $.get(
          "/cms/api/digitalsignage/playlists/getplaylist",
          (response) => {
              that.state.ex.PlayList = response.data ? response.data.map(e => {
                  return {value: e.Id, label: e.Title}
              }) : "";
              that.setState(that.state);
          }
      );
    var id = this.props.match.params.id;
    switch (action) {
      case "update":
          KTApp.blockPage();
        $.ajax({
          url: apiurl +"/"+ "getupdate",
          type: "GET",
          dataType: "json",
          contentType: "application/json",
          data: { id },
          success: response => {
              KTApp.unblockPage();
            if (response.status === "success") {
              that.state.model = response.data;
              that.state.ex.Title = deviceupdate[this.context.Language];
              document.title = that.state.ex.Title;
              that.setState(that.state);
            } else {
                toastr["error"](response.message, error[this.context.Language]);
            }
          },
            error: function (er) {
                KTApp.unblockPage();
                toastr["error"](globalErrorMessage[this.context.Language], error[this.context.Language]);
          }
        });
        break;
      case "add":
            that.state.ex.Title = deviceadd[this.context.Language];
        break;
    }
  }

  submitForm() {
    var that = this;
    KTApp.blockPage();
    $.ajax({
      url: apiurl + "/updatemanual",
      type: "POST",
      dataType: "json",
      contentType: "application/json",
      data: JSON.stringify(that.state.model),
      success: response => {
          KTApp.unblockPage();
        if (response.status === "success") {
            Swal.fire({
                type: 'success',
                title: success[this.context.Language],
                text: response.message,
                showConfirmButton: false,
                timer: 1500,
            }).then(() => {
                that.props.history.push("/admin/emptypage");
                that.props.history.replace({
                    pathname: that.props.location.pathname
                });
            });
        } else {
          toastr["error"](response.message, error[this.context.Language]);
        }
      }
    });
  }
  componentWillMount() {
    //Load script ckeditor lên
    $("#scriptloading").html(
      //`<script src="/statics/templates/admin/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>`
    );
  }

  componentDidMount() {
    //Active menu
    document.title = this.state.ex.Title;
    $("#kt_aside_menu .kt-menu__item").removeClass("kt-menu__item--active");
      $("#kt_aside_menu .kt-menu__item[data-id='digital-signage']").addClass(
          "kt-menu__item--active"
      );
      $("#kt_aside_menu .kt-menu__item[data-id='device-list']").addClass(
          "kt-menu__item--active"
      );
  }

  componentWillUnmount() {
    $("#scriptloading").html("");
  }
  render() {
    return (
      <React.Fragment>
        {this.state && this.state.model ? (
                <React.Fragment>
                    <div className="kt-subheader kt-grid__item" id="kt_subheader">
                        <div className="kt-subheader__main">
                            <div className="kt-subheader__breadcrumbs"><a className="kt-subheader__breadcrumbs-home" href="/admin/dashboard"><i className="fa fa-home"></i></a><span className="kt-subheader__breadcrumbs-separator"></span><span className="kt-subheader__breadcrumbs-link">Digital Signage</span><span className="kt-subheader__breadcrumbs-separator"></span><span className="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">{this.state.ex.Title}</span></div>
                        </div>
                        <div className="kt-subheader__toolbar">
                            <div className="kt-subheader__wrapper"><Link className="btn btn-secondary" to={`/admin/digital/devices`}><i className="fa fa-chevron-left" />Back</Link><a href="javascript:;" className="btn btn-primary" onClick={() => { this.submitForm(); }}><i className="fa fa-save"></i> Save</a></div>
                        </div>
                    </div>
                    <div className="kt-content kt-grid__item kt-grid__item--fluid" id="kt_content">
                        <div className="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                            <div className="row">
                                <div className="col-lg-12">
                                    <div className="kt-portlet kt-portlet--mobile">
                                        <div className="kt-portlet__head kt-portlet__head--lg">
                                            <div className="kt-portlet__head-label">
                                                <h3 className="kt-portlet__head-title">{information[this.context.Language]}</h3>
                                            </div>
                                        </div>
                                        <div className="kt-portlet__body">
                                            <div className="form-group">
                                                <label className="control-label">{name[this.context.Language]}</label>
                                                <input
                                                    className="form-control kt-input"
                                                    value={this.state.model.Name}
                                                    placeholder={name[this.context.Language]}
                                                    onChange={e => {
                                                        this.state.model.Name = e.target.value;
                                                        this.setState(this.state);
                                                    }}
                                                />
                                            </div>
                                            <div className={this.state.ex.Action === "update" ? "form-group" : "hidden"}>
                                                <label className="control-label">{code[this.context.Language]}</label>
                                                <input
                                                    className="form-control kt-input"
                                                    value={this.state.model.Code}
                                                    placeholder="Code"
                                                    onChange={e => {
                                                        this.state.model.Code = e.target.value;
                                                        this.setState(this.state);
                                                    }}
                                                    readOnly={true}
                                                />
                                            </div>
                                            <div className="form-group">
                                                <label className="control-label">{cdate[this.context.Language]}</label>
                                                <input
                                                    className="form-control kt-input"
                                                    value={moment.utc(this.state.model.CreatedDate).format("MM/DD/Y")}
                                                    placeholder={cdate[this.context.Language]}
                                                    onChange={e => {
                                                        this.state.model.CreatedDate = e.target.value;
                                                        this.setState(this.state);
                                                    }}
                                                    readOnly={true}
                                                />
                                            </div>
                                            <div className="form-group">
                                                <label className="control-label">{edate[this.context.Language]}</label>
                                                <input
                                                    className="form-control kt-input"
                                                    value={moment.utc(this.state.model.ExpDate).format("MM/DD/Y")}
                                                    placeholder={edate[this.context.Language]}
                                                    onChange={e => {
                                                        this.state.model.ExpDate = e.target.value;
                                                        this.setState(this.state);
                                                    }}
                                                    readOnly={true}
                                                />
                                            </div>
                                            <div className="form-group">
                                                <label className="control-label">{playlist[this.context.Language]}</label>
                                                <Select
                                                    className="basic-single"
                                                    classNamePrefix="select"
                                                    value={
                                                        this.state.ex.PlayList != null
                                                            ? this.state.ex.PlayList.filter(
                                                                option =>
                                                                    option.value === this.state.model.PlayListId
                                                            )
                                                            : ""
                                                    }
                                                    components={makeAnimated()}
                                                    isSearchable={true}
                                                    isClearable={true}
                                                    name="PlayList"
                                                    placeholder={playlist[this.context.Language]}
                                                    options={this.state.ex.PlayList}
                                                    onChange={e => {
                                                        var value = e === null ? "" : e.value;
                                                        this.state.model.PlayListId = value;
                                                        this.setState(this.state);
                                                    }}
                                                />
                                            </div>
                                            <div className={this.state.ex.Action === "add" ? "hidden" : "form-group"}>
                                                <label className="control-label">{status[this.context.Language]}</label>
                                                <Select
                                                    className="basic-single"
                                                    classNamePrefix="select"
                                                    value={
                                                        this.state.ex.StatusList != null
                                                            ? this.state.ex.StatusList.filter(
                                                                option =>
                                                                    option.value === this.state.model.Status
                                                            )
                                                            : ""
                                                    }
                                                    components={makeAnimated()}
                                                    isSearchable={true}
                                                    isClearable={true}
                                                    name="PlayList"
                                                    isDisabled={true}
                                                    placeholder={status[this.context.Language]}
                                                    options={this.state.ex.StatusList}
                                                    onChange={e => {
                                                        var value = e === null ? "" : e.value;
                                                        this.state.model.Status = value;
                                                        this.setState(this.state);
                                                    }}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
          </React.Fragment>
        ) : (
          <div />
        )}
      </React.Fragment>
    );
  }
}
DevicesAddUpdate.contextType = AppContext;
export default DevicesAddUpdate;
