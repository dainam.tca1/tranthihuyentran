import React, { Component } from "react";
import { Link } from "react-router-dom";
import {DelayInput} from "react-delay-input";
import PlayListVideoCmsModel from "./models/playlist-cmsmodel";
import SuggestVideo from "../../components/suggest-video";
import {
  GLOBAL_ERROR_MESSAGE,
  ERROR,
  SUCCESS,
    NOTI,
    DATA_NOT_RECOVER
} from "../../constants/message";

var apiurl = "/cms/api/digitalsignage/playlists";

class PlayListVideoAddUpdate extends Component {
  constructor(props) {
    super(props);
    var that = this;

    //action để nhận biết hiện đang add hay update
    var action = null;
      if (document.location.href.indexOf("/admin/digital/playlistvideo/add") >= 0) {
      action = "add";
    } else if (
        document.location.href.indexOf("/admin/digital/playlistvideo/update") >= 0
    ) {
      action = "update";
    }

    that.state = {
      model: new PlayListVideoCmsModel(),
      ex: {
        Title: null,
        Action: action,
        Type:{
          Play:null
        },
        OrderList: [],
      }
    };
    that.setState(that.state);
    switch (action) {
      case "update":
        var id = this.props.match.params.id;
        KTApp.blockPage();
        $.ajax({
          url: apiurl +"/getupdate",
          type: "GET",
          dataType: "json",
          contentType: "application/json",
          data: { id },
          success: response => {
              KTApp.unblockPage();
            if (response.status === "success") {
              that.state.model = response.data;
              that.state.ex.Title = "Update PlayList Video";
              document.title = that.state.ex.Title;
              that.setState(that.state);
            } else {
              toastr["error"](response.message, ERROR);
            }
          },
          error: function(er) {
              KTApp.unblockPage();
            toastr["error"](GLOBAL_ERROR_MESSAGE, ERROR);
          }
        });
        break;
      case "add":
        that.state.ex.Title = "Add PlayList Video";
        document.title = that.state.ex.Title;
        break;
    }
  }

  submitForm() {
    var that = this;
    KTApp.blockPage();
    $.ajax({
      url: apiurl +"/"+ that.state.ex.Action,
      type: "POST",
      dataType: "json",
      contentType: "application/json",
      data: JSON.stringify(that.state.model),
      success: response => {
          KTApp.unblockPage();
        if (response.status === "success") {
            Swal.fire({
                type: 'success',
                title: SUCCESS,
                text: response.message,
                showConfirmButton: false,
                timer: 1500,
            }).then(() => {
                that.props.history.push("/admin/emptypage");
                that.props.history.replace({
                    pathname: that.props.location.pathname
                });
            });
        } else {
          toastr["error"](response.message, ERROR);
        }
      }
    });
  }
  handleGetSuggestVideoList(e) {
    $("#suggest-modal").modal("hide");
    this.state.model.VideoThumbnailList = e;
    this.state.model.VideoThumbnailList = this.state.model.VideoThumbnailList.map(
        e => {
          return { ...e, IsChecked: false };
        }
    );
    this.setState(this.state.model.VideoThumbnailList);
  }
  handleClickSuggestVideo() {
    $("#suggest-modal").modal("show");
  }
  handleDeleteSuggestVideo() {
      var that = this;
      Swal.fire({
          title: NOTI,
          text: DATA_NOT_RECOVER,
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
          var model = that.state.model.VideoThumbnailList;
          that.state.model.VideoThumbnailList = model.filter(e => {
              return e.IsChecked === false;
          });
          that.setState(that.state);
          return true;
      })
  }
  // sap xep thu tu Video play List
  reorder() {
    var that = this;
    that.state.model.VideoThumbnailList.sort(function(a, b) {
      return parseInt(a.Sort) > parseInt(b.Sort ) ? 1 : parseInt(b.Sort ) > parseInt(a.Sort ) ? -1 : 0;
    });
  }
  componentWillMount() {
    //Load script ckeditor lên
    $("#scriptloading").html(
      //`<script src="/statics/templates/admin/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>`
    );
  }

  componentDidMount() {
    //Active menu
    document.title = this.state.ex.Title;
    $("#kt_aside_menu .kt-menu__item").removeClass("kt-menu__item--active");
    $("#kt_aside_menu .kt-menu__item[data-id='digital-signage']").addClass(
        "kt-menu__item--active"
    );
    $("#kt_aside_menu .kt-menu__item[data-id='play-list']").addClass(
        "kt-menu__item--active"
    );
  }

  componentWillUnmount() {
    $("#scriptloading").html("");
  }
  render() {
    return (
      <React.Fragment>
        {this.state && this.state.model ? (
          <React.Fragment>
              <div className="kt-subheader kt-grid__item" id="kt_subheader">
                  <div className="kt-subheader__main">
                      <div className="kt-subheader__breadcrumbs"><Link className="kt-subheader__breadcrumbs-home" to="/admin/dashboard"><i className="fa fa-home"></i></Link><span className="kt-subheader__breadcrumbs-separator"></span><span className="kt-subheader__breadcrumbs-link">Digital Signage</span><span className="kt-subheader__breadcrumbs-separator"></span><span className="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">{this.state.ex.Title}</span></div>
                  </div>
                  <div className="kt-subheader__toolbar">
                            <div className="kt-subheader__wrapper">
                                <Link to="/admin/digital/playlistvideo/add" className="btn btn-primary">
                                    <i className="fa fa-plus"></i> Add
                                </Link>
                                <Link to="/admin/digital/playlistvideo" className="btn btn-secondary">
                                    <i className="fa fa-chevron-left" /> Back
                                </Link>
                                <a
                                    href="javascript:;"
                                    className="btn btn-danger"
                                    onClick={() => {
                                        this.submitForm();
                                    }}
                                >
                                    <i className="fa fa-save"></i> Save
                                </a>
                            </div>
                  </div>
                    </div>
                    <div className="kt-content kt-grid__item kt-grid__item--fluid" id="kt_content">
                        <div className="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                            <div className="row">
                                <div className="col-lg-12">
                                    <div className="kt-portlet kt-portlet--mobile">
                                        <div className="kt-portlet__head kt-portlet__head--lg">
                                            <div className="kt-portlet__head-label">
                                                <h3 className="kt-portlet__head-title">Infomation</h3>
                                            </div>
                                        </div>
                                        <div className="kt-portlet__body">
                                            <div className="form-group">
                                                <label className="control-label">Name</label>
                                                <input
                                                    className="form-control  kt-input"
                                                    value={this.state.model.Title}
                                                    placeholder="Title"
                                                    onChange={e => {
                                                        this.state.model.Title = e.target.value;
                                                        this.setState(this.state);
                                                    }}
                                                />
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-lg-12">
                                    <div className="kt-portlet kt-portlet--mobile">
                                        <div className="kt-portlet__head kt-portlet__head--lg">
                                            <div className="kt-portlet__head-label">
                                                <h3 className="kt-portlet__head-title">PLAYLIST VIDEO</h3>
                                            </div>
                                        </div>
                                        <div className="kt-portlet__body">
                                            <div className="form-group">
                                                <button
                                                    type="button"
                                                    className="btn btn-primary"
                                                    onClick={e => {
                                                        this.handleClickSuggestVideo();
                                                    }}
                                                    style={{ marginRight: "10px" }}
                                                >
                                                    <i className="fa fa-plus"></i> Add Video  
                                                </button>
                                                <a
                                                    href="javascript:"
                                                    className="btn btn-danger"
                                                    onClick={e => this.handleDeleteSuggestVideo()}
                                                >
                                                    <i className="fa fa-trash-alt"></i>
                                                    Remove Video
                                                </a>
                                            </div>
                                            <div className="form-group">
                                                <div className="kt-separator kt-separator--border-dashed"></div>
                                                <div id="kt_table_1_wrapper" className="dataTables_wrapper dt-bootstrap4">
                                                    <div className="row">
                                                        <div className="col-sm-12">
                                                            <table className="table table-striped- table-bordered table-hover table-checkable dataTable no-footer dtr-inline table-responsive">
                                                                <thead>
                                                                    <tr role="row">
                                                                        <th
                                                                            className="dt-right sorting_disabled"
                                                                            rowSpan="1"
                                                                            colSpan="1"
                                                                            style={{ width: "1%" }}
                                                                            aria-label="Record ID"
                                                                        >
                                                                            <label className="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                                                                <input
                                                                                    type="checkbox"
                                                                                    className="kt-group-checkable"
                                                                                    onChange={ev => {
                                                                                        this.state.model.VideoThumbnailList = this.state.model.VideoThumbnailList.map(
                                                                                            e => {
                                                                                                return {
                                                                                                    ...e,
                                                                                                    IsChecked: ev.target.checked
                                                                                                };
                                                                                            }
                                                                                        );
                                                                                        this.setState(this.state);
                                                                                    }}
                                                                                />
                                                                                <span></span>
                                                                            </label>
                                                                        </th>
                                                                        <th style={{ width: "70px" }}>STT</th>
                                                                        <th style={{ width: "170px" }}>Avatar</th>
                                                                        <th>Title</th>
                                                                        <th>Url</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    {this.state.model.VideoThumbnailList ? this.state.model.VideoThumbnailList.map(c => {
                                                                        return (
                                                                            <tr>
                                                                                <td className="dt-right" tabIndex="0">
                                                                                    <label className="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                                                                        <input
                                                                                            type="checkbox"
                                                                                            className="kt-checkable"
                                                                                            value={c._id}
                                                                                            checked={c.IsChecked}
                                                                                            onChange={e => {
                                                                                                c.IsChecked = !c.IsChecked;
                                                                                                this.setState(this.state);
                                                                                            }}
                                                                                        />
                                                                                        <span></span>
                                                                                    </label>
                                                                                </td>
                                                                                <td>
                                                                                    <DelayInput
                                                                                        delayTimeout={1000}
                                                                                        className="form-control  kt-input"
                                                                                        type="text"
                                                                                        value={
                                                                                            parseFloat(c.Sort) == "" ? "0" : parseFloat(c.Sort)
                                                                                        }
                                                                                        onChange={
                                                                                            e => {
                                                                                                var that = this;
                                                                                                c.Sort = e.target.value;
                                                                                                that.reorder();
                                                                                                //   that.setState(this.state)

                                                                                            }
                                                                                        }
                                                                                    />
                                                                                </td>
                                                                                <td>
                                                                                    <Link to={`/admin/digital/videos/update/${c.Id}`}>
                                                                                        <img style={{ width: '200px' }} src={c.Avatar} alt="" />
                                                                                    </Link>
                                                                                </td>
                                                                                <td>
                                                                                    <Link to={`/admin/digital/video/update/${c.Id}`}>
                                                                                        {c.Title}
                                                                                    </Link>
                                                                                </td>
                                                                                <td><a href={c.Url}>{c.Url}</a></td>
                                                                            </tr>
                                                                        );
                                                                    }) : ""}

                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            <SuggestVideo
                videoThumbnail={this.state.model.VideoThumbnailList}
                onSelected={e => {
                  this.handleGetSuggestVideoList(e);
                }}
                idDigi = {this.props.match.params.idDigital ? this.props.match.params.idDigital : this.props.match.params.id}
            />
          </React.Fragment>
        ) : (
          <div />
        )}
      </React.Fragment>
    );
  }
}

export default PlayListVideoAddUpdate;
