import React, { Component } from "react";
import { DelayInput } from "react-delay-input";
import PagedList from "../../components/pagedlist";
import { Link } from "react-router-dom";
import FilterListPlugin from "../../plugins/filter-list-plugin";
import DeviceCmsModel from "./models/device-cmsmodel";
import RemoveListPlugin from "../../plugins/remove-list-plugin";
import { DEVICE_STATUS } from "../../constants/enums";
import Select from "react-select";
import makeAnimated from "react-select/animated";
import Switch from "../../components/switch-button";
import {
    globalErrorMessage,
    error,
    success,
    add,
    remove,
    total,
    record,
    page,
    cDate,
    savechange,
    lDate,
    recordPerPage,
} from "../../constants/message";
import {
    deviceList,
    deviceupdate,
    information,
    status,
    active,
    warning,
    inactive,
    code,
    cdate,
    edate,
    playing,
    devicename,
    name,
    playlist,
    playlistvideo,
    plname,
    total_video,
    action,
    play_all,
    pl_mess,
    yes,
    cancel,
    videolist,
    vname
} from "./models/digitalstaticmessage";
import AppContext from "../../components/app-context";
import moment from "moment";
const apiurl = "/cms/api/digitalsignage/devices";
class DevicesList extends Component {
  constructor(props,context) {
      super(props, context);

    //Khởi tạo state
    var that = this;
    that.state = {
      ex: {
        Title: deviceList[this.context.Language],
        Param: {
          //Danh sách tham số để filter
          pagesize: null,
          name: null,
          order: null,
          ip: null,
          status: null
            },
          Device: new DeviceCmsModel(),
        StatusList: [
            { value: DEVICE_STATUS.Inactive, label: inactive[this.context.Language] },
            { value: DEVICE_STATUS.Warning, label: warning[this.context.Language] },
            { value: DEVICE_STATUS.Active, label: active[this.context.Language] }
        ]
      }
    };
    that.setState(that.state);

    //Cấu hình event trên component
    that.handleChangeFilter = that.handleChangeFilter.bind(that);
    that.handleChangeDataRow = that.handleChangeDataRow.bind(that);
    //Gọi hàm filter để đổ dữ liệu ra
      that.toPage(1);
      $.get(
          "/cms/api/digitalsignage/playlists/getplaylist",
          { id: this.props.match.params.idDigital },
          (response) => {
              that.state.ex.PlayList = response.data ? response.data.map(e => {
                  return { value: e.Id, label: e.Title }
              }) : "";
              that.setState(that.state);
          }
      );
  }

  toPage(index) {
    FilterListPlugin.filterdata(index, this, apiurl);
  }
  /**
   * Được gọi khi thay đổi các trường filter cập nhật tham số filter vào state
   * Filter dữ liệu theo tham số mới
   * @param {Event} event
   */
  handleChangeFilter(event) {
    FilterListPlugin.handleChangeFilter(event, this);
    this.toPage(1);
  }

  /**
   * Xóa data đã được chọn
   */
  handleDeleteDataRow() {
    RemoveListPlugin.removeList(`${apiurl}/removelist`, this);
    }
  handleSaveDataDevice() {
      var that = this;
      KTApp.blockPage();
      $.ajax({
          url: apiurl + "/update",
          type: "POST",
          dataType: "json",
          contentType: "application/json",
          data: JSON.stringify({ model: that.state.ex.Device }),
          success: response => {
            KTApp.unblockPage();
              toastr.clear();
              if (response.status == "success") {
                  that.state.model.Results = that.state.model.Results
                      ? that.state.model.Results.map(e => {
                          if (e.Id == that.state.ex.Device.Id) {
                              e = that.state.ex.Device;
                              that.setState(that.state);
                          }
                          return e;
                      })
                      : "";
                  Swal.fire({
                      type: 'success',
                      title: SUCCESS,
                      text: response.message,
                      showConfirmButton: false,
                      timer: 1500
                  }).then(() => {
                      that.props.history.push("/admin/emptypage");
                      that.props.history.replace({
                          pathname: that.props.location.pathname
                      });
                  });
                  $("#responsive").modal("hide");
                  that.setState(that.state);
                 // toastr["success"](response.message, SUCCESS);
              } else {
                  toastr["error"](response.message, error[this.context.Language]);
              }
          },
          error: function (er) {
            KTApp.unblockPage();
              toastr.clear();
              toastr["error"](globalErrorMessage[this.context.Language], error[this.context.Language]);
          }
      });
  }
  handleGetDeviceById(id) {
      var that = this;
      KTApp.blockPage();
      $.ajax({
          url: apiurl + "/getupdate",
          type: "GET",
          data: { id: id },
          success: response => {
            KTApp.unblockPage();
              toastr.clear();
              if (response.status == "success") {
                  that.state.ex.Device = response.data;
                  that.setState(this.state);
                  $("#responsive").modal("show");

              } else {
                  toastr["error"](response.message, error[this.context.Language]);
              }
          }
      });
  }
  handleChangeDataRow(event) {
    const target = event.target;
    const value = target.type === "checkbox" ? target.checked : target.value;
    const name = target.name;
    const index = target.getAttribute("index");
    var newobj = { ...this.state.model.Results[index] };
    newobj[name] = isNaN(value) ? value : parseInt(value);
    var that = this;
    KTApp.blockPage();
    $.ajax({
      url: apiurl + "/updatecustomize",
      type: "POST",
      dataType: "json",
      contentType: "application/json",
      data: JSON.stringify({ model: newobj, name }),
      success: response => {
        KTApp.unblockPage();
        toastr.clear();
        if (response.status === "success") {
          that.state.model.Results[index][name] = newobj[name];
          that.setState(that.state);
            toastr["success"](response.message, success[this.context.Language]);
        } else {
            toastr["error"](response.message, error[this.context.Language]);
        }
      }
    });
  }
  componentWillMount() {}
  componentDidMount() {
    document.title = this.state.ex.Title;
    $("#kt_aside_menu .kt-menu__item").removeClass("kt-menu__item--active");
    $("#kt_aside_menu .kt-menu__item[data-id='digital-signage']").addClass(
      "kt-menu__item--active"
    );
    $("#kt_aside_menu .kt-menu__item[data-id='device-list']").addClass(
      "kt-menu__item--active"
    );
   
  }
  render() {
    return (
      <React.Fragment>
        {this.state && this.state.model ? (
                <React.Fragment>
                    <div className="kt-subheader kt-grid__item" id="kt_subheader">
                        <div className="kt-subheader__main">
                            <div className="kt-subheader__breadcrumbs"><a className="kt-subheader__breadcrumbs-home" href="/admin/dashboard"><i className="fa fa-home"></i></a><span className="kt-subheader__breadcrumbs-separator"></span><span className="kt-subheader__breadcrumbs-link">Digital Signage</span><span className="kt-subheader__breadcrumbs-separator"></span><span className="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">{this.state.ex.Title}</span></div>
                        </div>
                        <div className="kt-subheader__toolbar">
                            <div className="kt-subheader__wrapper"></div>
                        </div>
                    </div>
                    <div className="kt-content kt-grid__item kt-grid__item--fluid" id="kt_content">
                        <div className="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                            <div className="kt-portlet kt-portlet--mobile">
                                <div className="kt-portlet__head kt-portlet__head--lg">
                                    <div className="kt-portlet__head-label">
                                        <h3 class="kt-portlet__head-title">{this.state.ex.Title}</h3>
                                    </div>
                                </div>
                                <div className="kt-portlet__body">
                                    <form className="kt-form kt-form--fit">
                                        <div className="row kt-margin-b-20">
                                          
                                        </div>
                                    </form>
                                    <div className="kt-separator kt-separator--border-dashed"></div>
                                    <div id="kt_table_1_wrapper" className="dataTables_wrapper dt-bootstrap4">
                                        <div className="row">
                                            {this.state.model.Results ? this.state.model.Results.map(c => {
                                                return (
                                                    <div className="col-lg-3 col-xs-12 col-sm-12" style={{
                                                        marginTop: "20px",padding:"10px"
                                                    }}>
                                                        <div className="card" style={{ padding: "10px"}}>
                                                            <div className="portlet-title">
                                                                <div className="caption" style={{padding:"5px 0"}}>
                                                                    <i className="icon-share font-dark hide" />
                                                                    <span className="caption-subject font-dark bold uppercase" style={{textTransform:"uppercase",fontSize:"20px"}}>
                                                                        <Link to={`/admin/digital/devices/update/${c.Id
                                                                            }`}>
                                                                            {c.Name}
                                                                        </Link>
                                                                    </span>
                                                                </div>
                                                                <div className="actions">
                                                                    <span style={{ position: "absolute", top: "-10px",left:0}} className={`badge badge-${c.Status == DEVICE_STATUS.Active ? `success`: `danger`}`}>{c.Status == DEVICE_STATUS.Active ? "Active" : "In Active"}</span>
                                                                    <a
                                                                        style={{ position: "absolute", top: "-10px", right:"-10px" }}
                                                                        className=" btn-circle btn-icon-only btn-default"
                                                                        data-toggle="modal"
                                                                        href="#responsive"
                                                                        onClick={e => {
                                                                            this.handleGetDeviceById(c.Id);
                                                                        }}
                                                                    >
                                                                        <i className="fa fa-cog fa-2x" />
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div className="portlet-body">
                                                                <div className="row">
                                                                    <div className="col-md-6 ">
                                                                        {code[this.context.Language]}:
                                                </div>
                                                                    <div className="col-md-6 text-right">
                                                                        <span style={{ "fontWeight": "bold" }}>  {c
                                                                            .Code} </span>
                                                                    </div>
                                                                </div>
                                                                <div className="row">
                                                                    <div className="col-md-6 ">
                                                                        {cdate[this.context.Language]} : 
                                                    </div>
                                                                    <div className="col-md-6 text-right">
                                                                        <span style={{ "fontWeight": "bold" }}>  {
                                                                            moment
                                                                                .utc(c.CreatedDate)
                                                                                .format("M/DD/Y")} </span>
                                                                    </div>
                                                                </div>
                                                                <div className="row">
                                                                    <div className="col-md-6">
                                                                        {edate[this.context.Language]} :
                                                    </div>
                                                                    <div className="col-md-6 text-right">
                                                                        <span style={{ "fontWeight": "bold" }}> {moment
                                                                            .utc(c.ExpDate)
                                                                            .format("M/DD/Y")} </span>
                                                                    </div>
                                                                </div>
                                                                <div className="row">
                                                                    <div className="col-md-6">
                                                                        {playing[this.context.Language]} :
                                                    </div>
                                                                    <div className="col-md-6 text-right">
                                                                        <span style={{ "fontWeight": "bold" }}>  {c
                                                                            .PlayListName}  </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                );
                                            }):""}
                                          
                                        </div>
                                        <div className="row">
                                            <div className="col-sm-12 col-md-5">
                                                <div className="dataTables_info" id="kt_table_1_info" role="status" aria-live="polite">{total[this.context.Language]} {this.state.model.TotalItemCount} {record[this.context.Language]}</div>
                                            </div>
                                            <div className="col-sm-12 col-md-7 dataTables_pager">
                                                <PagedList
                                                    currentpage={this.state.model.CurrentPage}
                                                    pagesize={this.state.model.PageSize}
                                                    totalitemcount={this.state.model.TotalItemCount}
                                                    totalpagecount={this.state.model.TotalPageCount}
                                                    ajaxcallback={this.toPage.bind(this)}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div
                            id="responsive"
                            className="modal fade modalstyle"
                            role="dialog"
                        >
                            <div className="modal-dialog" style={{ maxWidth: "1000px" }}>
                                <div className="modal-content">
                                    <div className="modal-header">
                                        <h5 className="modal-title" id="exampleModalLabel">Device Name: {this.state.ex.Device.Name}</h5>
                                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                    </div>
                                    <div className="modal-body">
                                        <div id="kt_table_1_wrapper" className="dataTables_wrapper dt-bootstrap4">
                                            <div className="row">
                                                <div className="col-sm-12">
                                                    <div className="form-group">
                                                        <label className="control-label">{devicename[this.context.Language]}</label>
                                                        <input
                                                            type="text"
                                                            value={this.state.ex.Device.Name}
                                                            placeholder={devicename[this.context.Language]}
                                                            onChange={e => {
                                                                this.state.ex.Device.Name = e.target.value;
                                                                this.setState(this.state);
                                                            }}
                                                            className="form-control"
                                                        />
                                                    </div>
                                                    <div className="form-group">
                                                        <label className="control-label">{playlist[this.context.Language]}</label>
                                                        <Select
                                                            className="basic-single"
                                                            classNamePrefix="select"
                                                            value={
                                                                this.state.ex.PlayList != null
                                                                    ? this.state.ex.PlayList.filter(
                                                                        option =>
                                                                            option.value === this.state.ex.Device.PlayListId
                                                                    )
                                                                    : ""
                                                            }
                                                            components={makeAnimated()}
                                                            isSearchable={true}
                                                            isClearable={true}
                                                            name="PlayList"
                                                            placeholder={playlist[this.context.Language]}
                                                            options={this.state.ex.PlayList}
                                                            onChange={e => {
                                                                var value = e === null ? "" : e.value;
                                                                this.state.ex.Device.PlayListId = value;
                                                                this.setState(this.state);
                                                            }}
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                    <div className="modal-footer">
                                        <button
                                            type="button"
                                            className="btn btn-danger  btn-outline"
                                            data-dismiss="modal"
                                        >
                                            Close
                                        </button>
                                        <button
                                            type="button"
                                            className="btn btn-primary"
                                            onClick={e => this.handleSaveDataDevice()}
                                        >
                                            {savechange[this.context.Language]}
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
          </React.Fragment>
        ) : (
          <div />
        )}
      </React.Fragment>
    );
  }
}
DevicesList.contextType = AppContext;
export default DevicesList;
