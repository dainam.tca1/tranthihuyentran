import React, { Component } from "react";
import VideoCmsModel from "./models/video-cmsmodel";
import {
  GLOBAL_ERROR_MESSAGE,
  ERROR,
  SUCCESS,
    NOTI,
    DATA_NOT_RECOVER
} from "../../constants/message";
import uploadPlugin from "../../plugins/upload-plugin";

var apiurl = "/cms/api/digitalsignage/videos";

class ManagerVideoAddUpdate extends Component {
  constructor(props) {
    super(props);
    var that = this;

    //action để nhận biết hiện đang add hay update
    var action = null;
    if (document.location.href.indexOf("/admin/digital/video/add") >= 0) {
      action = "add";
    } else if (
        document.location.href.indexOf("/admin/digital/video/update") >= 0
    ) {
      action = "update";
    }

    that.state = {
      model: new VideoCmsModel(),
      ex: {
        Title: null,
        Action: action,
        Type:{
          Play:null
        },
        OrderList: [],
      }
    };
    that.setState(that.state);
    switch (action) {
      case "update":
        var id = this.props.match.params.id;
        KTApp.blockPage();
        $.ajax({
          url: apiurl +"/video/"+ "getupdate",
          type: "GET",
          dataType: "json",
          contentType: "application/json",
          data: { id },
          success: response => {
              KTApp.unblockPage();
            if (response.status === "success") {
              that.state.model = response.data;
              that.state.ex.Title = "Update Video";
              document.title = that.state.ex.Title;
              that.setState(that.state);
            } else {
              toastr["error"](response.message, ERROR);
            }
          },
          error: function(er) {
              KTApp.unblockPage();
            toastr["error"](GLOBAL_ERROR_MESSAGE, ERROR);
          }
        });
        break;
      case "add":
        that.state.ex.Title = "Add Video";
        document.title = that.state.ex.Title;
        break;
    }
  }

  submitForm() {
    var that = this;
    KTApp.blockPage();
    $.ajax({
      url: apiurl + "/video/" + that.state.ex.Action,
      type: "POST",
      dataType: "json",
      contentType: "application/json",
      data: JSON.stringify(that.state.model),
      success: response => {
          KTApp.unblockPage();
        if (response.status === "success") {
          alertify.alert(SUCCESS, response.message, () => {
            that.props.history.push("/admin/emptypage");
            that.props.history.replace({
              pathname: that.props.location.pathname
            });
          });
        } else {
          toastr["error"](response.message, ERROR);
        }
      }
    });
  }

  componentWillMount() {
    //Load script ckeditor lên
    $("#scriptloading").html(
      //`<script src="/statics/templates/admin/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>`
    );
  }

  componentDidMount() {
    //Active menu
    document.title = this.state.ex.Title;
    $("#kt_aside_menu .kt-menu__item").removeClass("kt-menu__item--active");
      $("#kt_aside_menu .kt-menu__item[data-id='digital-signage']").addClass(
          "kt-menu__item--active"
      );
      $("#kt_aside_menu .kt-menu__item[data-id='video-list']").addClass(
          "kt-menu__item--active"
      );
  }

  componentWillUnmount() {
    $("#scriptloading").html("");
  }
  render() {
    return (
      <React.Fragment>
        {this.state && this.state.model ? (
          <React.Fragment>
            <div className="page-bar">
              <ul className="page-breadcrumb">
                <li>
                                <a href="javascript:">Digital Signage</a>
                  <i className="fa fa-circle" />
                </li>
                <li>
                  <span>{this.state.ex.Title}</span>
                </li>
              </ul>
              <div className="page-toolbar">
                <div className="btn-group pull-right">
                </div>
              </div>
            </div>
            <h3 className="page-title">{this.state.ex.Title}</h3>
            <div className="row">
              <div className="col-md-8">
                <div className="portlet box green-meadow">
                  <div className="portlet-title">
                    <div className="caption">
                      <i className=" icon-layers font-white" />
                      <span className="caption-subject font-white sbold uppercase">
                        INFO Video
                      </span>
                    </div>
                    <div className="tools">
                      <a href="" className="collapse" />
                    </div>
                  </div>
                  <div className="portlet-body">
                    <div className="form-body">
                      <div className="form-group">
                        <label className="control-label">Title</label>
                        <span className="required"> * </span>
                        <div className="input-icon right">
                          <i className="fa" />
                          <input
                              className="form-control"
                              value={this.state.model.Title}
                              placeholder="Title"
                              onChange={e => {
                                this.state.model.Title = e.target.value;
                                this.setState(this.state);
                              }}
                          />
                        </div>
                      </div>
                      <div className="form-group">
                        <label className="control-label">Url</label>
                        <span className="required"> * </span>
                        <div className="input-icon right">
                          <i className="fa" />
                          <input
                              className="form-control"
                              value={this.state.model.Url}
                              placeholder="Url"
                              onChange={e => {
                                this.state.model.Url = e.target.value;
                                this.setState(this.state);
                              }}
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-md-4">
                <div className="portlet box green-meadow">
                  <div className="portlet-title">
                    <div className="caption">
                      <i className=" icon-layers font-white" />
                      <span className="caption-subject font-white sbold uppercase">
                        Avatar
                      </span>
                    </div>
                    <div className="tools">
                      <a href="" className="collapse">
                        {" "}
                      </a>
                    </div>
                  </div>
                  <div className="portlet-body">
                    <div className="form-body">
                      <div className="form-group item-avatar">
                        <input
                            multiple
                            type="file"
                            className="form-control"
                            onChange={e => {
                              var that = this;
                              uploadPlugin.UpdateImage(e, listfile => {
                                that.state.model.Avatar = listfile[0];
                                that.setState(that.state);
                              });
                            }}
                        />
                        <img
                            src={this.state.model.Avatar}
                            className="img-responsive"
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </React.Fragment>
        ) : (
          <div />
        )}
      </React.Fragment>
    );
  }
}

export default ManagerVideoAddUpdate;
