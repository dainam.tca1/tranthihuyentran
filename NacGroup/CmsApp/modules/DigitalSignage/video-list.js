import React, { Component } from "react";
import { DelayInput } from "react-delay-input";
import PagedList from "../../components/pagedlist";
import FilterListPlugin from "../../plugins/filter-list-plugin";
import RemoveListPlugin from "../../plugins/remove-list-plugin";
import VideoCmsModel from "./models/video-cmsmodel";
import {
    globalErrorMessage,
    error,
    success,
    add,
    remove,
    total,
    record,
    page,
    cDate,
    lDate,
    recordPerPage,
    noti
} from "../../constants/message";
import {
    deviceList,
    deviceupdate,
    information,
    status,
    active,
    warning,
    inactive,
    code,
    cdate,
    edate,
    playing,
    devicename,
    name,
    playlist,
    playlistvideo,
    plname,
    total_video,
    action,
    play_all,
    pl_mess,
    yes,
    cancel,
    videolist,
    vname
} from "./models/digitalstaticmessage";
import AppContext from "../../components/app-context";
const apiurl = "/cms/api/digitalsignage/videos";
class ManagerVideoList extends Component {
  constructor(props,context) {
    super(props,context);

    //Khởi tạo state
    var that = this;
    that.state = {
      ex: {
        Title: videolist[this.context.Language],
        Param: {
          //Danh sách tham số để filter
          pagesize: null,
          name: null,
          url: null,
           },
        Video: new VideoCmsModel(),
      }
    };
    that.setState(that.state);

    //Cấu hình event trên component
    that.handleChangeFilter = that.handleChangeFilter.bind(that);
    that.handleChangeDataRow = that.handleChangeDataRow.bind(that);
    //Gọi hàm filter để đổ dữ liệu ra
    that.toPage(1);
  }

  toPage(index) {
    FilterListPlugin.filterdata(index, this, apiurl);
  }

  /**
   * Được gọi khi thay đổi các trường filter cập nhật tham số filter vào state
   * Filter dữ liệu theo tham số mới
   * @param {Event} event
   */
  handleChangeFilter(event) {
    FilterListPlugin.handleChangeFilter(event, this);
    this.toPage(1);
  }

  /**
   * Xóa data đã được chọn
   */
  handleDeleteDataRow() {
    RemoveListPlugin.removeList(`${apiurl}/removelist`, this);
  }
  handleChangeDataRow(event) {
    const target = event.target;
    const value = target.type === "checkbox" ? target.checked : target.value;
    const name = target.name;
    const index = target.getAttribute("index");
    var newobj = { ...this.state.model.Results[index] };
    newobj[name] = isNaN(value) ? value : parseInt(value);
    var that = this;
    KTApp.blockPage();
    $.ajax({
      url: apiurl + "/updatecustomize",
      type: "POST",
      dataType: "json",
      contentType: "application/json",
      data: JSON.stringify({ model: newobj, name }),
      success: response => {
          KTApp.unblockPage();
        toastr.clear();
        if (response.status === "success") {
          that.state.model.Results[index][name] = newobj[name];
          that.setState(that.state);
            toastr["success"](response.message, success[this.context.Language],);
        } else {
            toastr["error"](response.message, error[this.context.Language],);
        }
      },
      error: function(er) {
          KTApp.unblockPage();
          toastr.clear();
          toastr["error"](globalErrorMessage[this.context.Language], error[this.context.Language]);
      }
    });
  }
  componentWillMount() {}
  componentDidMount() {
    document.title = this.state.ex.Title;
    $("#kt_aside_menu .kt-menu__item").removeClass("kt-menu__item--active");
    $("#kt_aside_menu .kt-menu__item[data-id='digital-signage']").addClass(
        "kt-menu__item--active"
    );
    $("#kt_aside_menu .kt-menu__item[data-id='video-list']").addClass(
        "kt-menu__item--active"
    );
  }
  render() {
    return (
      <React.Fragment>
        {this.state && this.state.model ? (
          <React.Fragment>
              <div className="kt-subheader kt-grid__item" id="kt_subheader">
                  <div className="kt-subheader__main">
                      <div className="kt-subheader__breadcrumbs"><a className="kt-subheader__breadcrumbs-home" href="/admin/dashboard"><i className="fa fa-home"></i></a><span className="kt-subheader__breadcrumbs-separator"></span><span className="kt-subheader__breadcrumbs-link">Digital Signage</span><span className="kt-subheader__breadcrumbs-separator"></span><span className="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">{this.state.ex.Title}</span></div>
                  </div>
                  <div className="kt-subheader__toolbar">
                      <div className="kt-subheader__wrapper"></div>
                  </div>
                    </div>
                    <div className="kt-content kt-grid__item kt-grid__item--fluid" id="kt_content">
                        <div className="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                            <div className="kt-portlet kt-portlet--mobile">
                                <div className="kt-portlet__head kt-portlet__head--lg">
                                    <div className="kt-portlet__head-label">
                                        <h3 className="kt-portlet__head-title">{this.state.ex.Title}</h3>
                                    </div>
                                </div>
                                <div className="kt-portlet__body">
                                    <form className="kt-form kt-form--fit">
                                        <div className="row kt-margin-b-20">
                                            <div className="col-lg-3">
                                                <label>{name[this.context.Language]}:</label>
                                                <DelayInput
                                                    delayTimeout={1000}
                                                    className="form-control kt-input"
                                                    placeholder={name[this.context.Language]}
                                                    value={this.state.ex.Param.name}
                                                    name="name"
                                                    onChange={this.handleChangeFilter}
                                                />
                                            </div>
                                            <div className="col-lg-3">
                                                <label>Url:</label>
                                                <DelayInput
                                                    delayTimeout={1000}
                                                    className="form-control kt-input"
                                                    placeholder="Url"
                                                    value={this.state.ex.Param.url}
                                                    name="url"
                                                    onChange={this.handleChangeFilter}
                                                />
                                            </div>
                                            <div className="col-lg-3">
                                                <label>{recordPerPage[this.context.Language]}:</label>
                                                <select
                                                    className="form-control kt-input"
                                                    value={this.state.ex.Param.pagesize}
                                                    name="pagesize"
                                                    onChange={this.handleChangeFilter}
                                                >
                                                    <option value="10">10 {record[this.context.Language]} / {page[this.context.Language]}</option>
                                                    <option value="20">20 {record[this.context.Language]} / {page[this.context.Language]}</option>
                                                    <option value="50">50 {record[this.context.Language]} / {page[this.context.Language]}</option>
                                                    <option value="100">100 {record[this.context.Language]} / {page[this.context.Language]}s</option>
                                                </select>
                                            </div>
                                        </div>
                                    </form>
                                    <div className="kt-separator kt-separator--border-dashed"></div>
                                    <div id="kt_table_1_wrapper" className="dataTables_wrapper dt-bootstrap4">
                                        <div className="row">
                                            {this.state.model.Results ? this.state.model.Results.map(c => {
                                                return (
                                                    <div className="col-lg-3 col-md-4 col-sm-6 col-xs-12" style={{ marginTop: "20px" }}>
                                                        <div className="card">
                                                            <div className="kt-bg-metal">
                                                                <img src={c.Avatar} style={{ width: "100%",height:"200px" }} />
                                                            </div>
                                                            <div className="card-body">
                                                                <h5 className="card-title">
                                                                    {c.Title}
                                                                </h5>
                                                                <p className="card-text">{c.Url}</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                )
                                            }) : ""}
                                        </div>
                                        <div className="row">
                                            <div className="col-sm-12 col-md-5">
                                                <div className="dataTables_info" id="kt_table_1_info" role="status" aria-live="polite">{total[this.context.Language]} {this.state.model.TotalItemCount} {record[this.context.Language]}</div>
                                            </div>
                                            <div className="col-sm-12 col-md-7 dataTables_pager">
                                                <PagedList
                                                    currentpage={this.state.model.CurrentPage}
                                                    pagesize={this.state.model.PageSize}
                                                    totalitemcount={this.state.model.TotalItemCount}
                                                    totalpagecount={this.state.model.TotalPageCount}
                                                    ajaxcallback={this.toPage.bind(this)}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
          </React.Fragment>
        ) : (
          <div />
        )}
      </React.Fragment>
    );
  }
}
ManagerVideoList.contextType = AppContext;
export default ManagerVideoList;
