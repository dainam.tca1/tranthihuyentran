﻿class PlayListCmsModel {
    constructor() {
        this.Id = null;
        this.Title = null;
        this.VideoThumbnailList = [];
        this.IdDigital = null;
    } 
}

export default PlayListCmsModel;