﻿class DeviceCmsModel {
    constructor() {
        this.Id = null;
        this.Code = null;
        this.PlayListId = null;
        this.CreatedDate = null;
        this.ExpDate = null;
        this.Status = 0;
        this.IdDigital = null;
        this.Finger = null;
        this.Name = null;
    } 
}

export default DeviceCmsModel;