//#region device
const deviceList = ["DeviceList", "Danh sách thiết bị"];
const deviceupdate = ["Update Devices", "Cập nhật thiết bị"];
const deviceadd = ["Add Devices", "Thêm mới thiết bị"];
const active = ["Active", "Kích hoạt"];
const inactive = ["InActive", "Chưa kích hoạt"];
const warning = ["Warning", "Cảnh báo"];
const code = ["Code Play", "Mã chạy video"];
const cdate = ["Created Date", "Ngày tạo"];
const edate = ["Exp Date", "Ngày hết hạn"];
const playing = ["Playing", "Tên danh sách video"];
const devicename = ["Device Name", "Tên thiết bị"];
const name = ["Name", "Tên"];
const playlist = ["Playlist video", "Danh sách phát video"];
const information = ["Information", "Thông tin"];
const status = ["Status", "Trạng thái"];
//#endregion
//#region playlist
const playlistvideo = ["Playlist video", "Danh sách phát video"];
const plname = ["Name", "Tên"];
const total_video = ["Total video", "Tổng cộng video"];
const action = ["Action", "Hành động"];
const play_all = ["Play All", "Chạy tất cả"];
const pl_mess = ["Do you want all devices to run 1 playlist", "Bạn có muốn tất cả thiết bị cùng chạy 1 danh sách phát video"]
const yes = ["Yes, you want!", "Vâng tôi muốn"];
const cancel = ["Cancel", "Hủy bỏ"]
//#endregion

//#region video
const videolist = ["Video List", "Danh sách video"];
const vname = ["Name", "Tên video"];
//#endregion





module.exports = {
    deviceList,
    deviceupdate,
    deviceadd,
    information,
    status,
    active,
    warning,
    inactive,
    code,
    cdate,
    edate,
    playing,
    devicename,
    name,
    playlist,
    playlistvideo,
    plname,
    total_video,
    action,
    play_all,
    pl_mess,
    yes,
    cancel,
    videolist,
    vname
};
