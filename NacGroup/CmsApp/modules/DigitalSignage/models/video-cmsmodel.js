﻿class VideoCmsModel {
    constructor() {
        this.Id = null;
        this.Title = null;
        this.Url = null;
        this.Avatar = null;
    } 
}

export default VideoCmsModel;