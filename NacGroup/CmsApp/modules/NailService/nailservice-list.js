import React, { Component } from "react";
import { DelayInput } from "react-delay-input";
import PagedList from "../../components/pagedlist";
import { Link } from "react-router-dom";
import moment from "moment";
import FilterListPlugin from "../../plugins/filter-list-plugin";
import RemoveListPlugin from "../../plugins/remove-list-plugin";
const apiurl = "/cms/api/nailservice";
const apicateurl = "/cms/api/nailservicecategory";
import {
    globalErrorMessage,
    error,
    back,
    save,
    success,
    add,
    remove,
    total,
    record,
    page,
    cDate,
    lDate,
    recordPerPage
} from "../../constants/message";
import {
    service,
    serviceList,
    service_name,
    category,
    se_category,
    price,
    service_price,
    service_time,
    isPlus,
    sort,
    avatar,
    name,
    cdate,
    udate,
    info,
    order,
    service_categories,
    parent_categories,
    se_parent,
    des,
    seoTitle,
    seoDescription,
    seoKeyword
} from "./models/nailservicestaticmessage";
import AppContext from "../../components/app-context";

class NailServiceList extends Component {
  constructor(props,context) {
    super(props,context);
    var that = this;
    that.state = {
        ex: {
        Title: serviceList[this.context.Language],
        Param: {
          name: null,
          pagesize: null,
          categoryid: null
        },
        CategoryList: []
      }
    };
    that.setState(that.state);
    that.handleChangeFilter = that.handleChangeFilter.bind(that);
    that.handleChangeDataRow = that.handleChangeDataRow.bind(that);

    KTApp.blockPage();
    $.get(apicateurl, null, response => {
      KTApp.unblockPage();
      toastr.clear();
      if (response.status === "success") {
        that.state.ex.CategoryList = response.data.Results;
        that.setState(that.state);
      } else {
        toastr["error"](response.message, error[this.context.Language]);
      }
    }).fail(() => {
      KTApp.unblockPage();
      toastr.clear();
        toastr["error"](globalErrorMessage[this.context.Language], error[this.context.Language]);
    });

    that.toPage(1);
  }

  filter(url) {
    var that = this;
    KTApp.blockPage();
    $.ajax({
      url: url,
      type: "GET",
      dataType: "json",
      contentType: "application/json",
      success: response => {
        KTApp.unblockPage();
        that.state.model = response.data;
        that.state.model.Results = that.state.model.Results.map(e => {
          return { ...e, IsChecked: false };
        });
        that.setState(that.state);
      },
      error: function(er) {
        KTApp.unblockPage();
      }
    });
  }
  toPage(index) {
    var paramStr = "";
    if (this.state.ex.Param != null) {
      for (var key in this.state.ex.Param) {
        if (this.state.ex.Param[key] == null) continue;
        if (paramStr != "") {
          paramStr += "&";
        }
        paramStr += key + "=" + encodeURIComponent(this.state.ex.Param[key]);
      }
    }
    var url = apiurl + "?" + paramStr + (paramStr ? "&" : "") + "page=" + index;
    this.filter(url);
  }

  handleChangeFilter(event) {
    FilterListPlugin.handleChangeFilter(event, this);
    this.toPage(1);
  }
  handleDeleteDataRow() {
    RemoveListPlugin.removeList(`${apiurl}/removelist`, this);
  }
  handleChangeDataRow(event) {
    const target = event.target;
    const value = target.type === "checkbox" ? target.checked : target.value;
    const name = target.name;
    const index = target.getAttribute("index");
    var newobj = { ...this.state.model.Results[index] };
    newobj[name] = isNaN(value) ? value : parseInt(value);
    var that = this;
    KTApp.blockPage();
    $.ajax({
      url: apiurl + "/updatecustomize",
      type: "POST",
      dataType: "json",
      contentType: "application/json",
      data: JSON.stringify({ model: newobj, name }),
      success: response => {
        KTApp.unblockPage();
        toastr.clear();
        if (response.status == "success") {
          that.state.model.Results[index][name] = newobj[name];
          that.setState(that.state);
          toastr["success"](response.message, success[this.context.Language]);
        } else {
          toastr["error"](response.message, error[this.context.Language]);
        }
      },
      error: function(er) {
        KTApp.unblockPage();
        toastr.clear();
        toastr["error"](globalErrorMessage[this.context.Language], error[this.context.Language]);
      }
    });
  }
  componentWillMount() {}
  componentDidMount() {
    document.title = this.state.ex.Title;
    $("#kt_aside_menu .kt-menu__item").removeClass("kt-menu__item--active");
    $("#kt_aside_menu .kt-menu__item[data-id='dich-vu']").addClass(
      "kt-menu__item--active kt-menu__item--open"
    );
    $("#kt_aside_menu .kt-menu__item[data-id='danh-sach-dich-vu']").addClass(
      "kt-menu__item--active"
      );
      $(".nac-nav-ul li").removeClass("active");
      $(".nac-nav-ul li[data-id='nail-service']").addClass("active");
  }
  render() {
    return (
      <React.Fragment>
        {this.state && this.state.model ? (
          <React.Fragment>
            <div className="kt-subheader kt-grid__item" id="kt_subheader">
              <div className="kt-subheader__main">
                <div className="kt-subheader__breadcrumbs">
                  <Link
                    to={document.location.href.indexOf("/appointment/nailservice") >= 0 ? "/appointment/dashboard" : "/admin/dashboard"}
                    className="kt-subheader__breadcrumbs-home"
                  >
                    <i className="fa fa-home" />
                  </Link>
                  <span className="kt-subheader__breadcrumbs-separator" />
                  <span className="kt-subheader__breadcrumbs-link">
                     {service[this.context.Language]}
                  </span>
                  <span className="kt-subheader__breadcrumbs-separator" />
                  <span className="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
                    {this.state.ex.Title}
                  </span>
                </div>
              </div>
              <div className="kt-subheader__toolbar">
                <div className="kt-subheader__wrapper">
                                <Link
                                    to={document.location.href.indexOf("/appointment/nailservice") >= 0 ? "/appointment/nailservice/add" : "/admin/nailservice/add"}
                                    className="btn btn-primary">
                    <i className="fa fa-plus"></i>  {add[this.context.Language]}
                  </Link>
                  <a
                    href="javascript:;"
                    onClick={e => this.handleDeleteDataRow()}
                    className="btn btn-danger"
                  >
                    <i className="fa fa-trash-alt"></i> {remove[this.context.Language]}
                  </a>
                </div>
              </div>
            </div>
            <div
              className="kt-content kt-grid__item kt-grid__item--fluid"
              id="kt_content"
            >
              <div className="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                <div className="kt-portlet kt-portlet--mobile">
                  <div className="kt-portlet__head kt-portlet__head--lg">
                    <div className="kt-portlet__head-label">
                      <h3 className="kt-portlet__head-title">
                        {this.state.ex.Title}
                      </h3>
                    </div>
                  </div>

                  <div className="kt-portlet__body">
                    <form className="kt-form kt-form--fit">
                      <div className="row kt-margin-b-20">
                        <div className="col-lg-3">
                          <label>{service_name[this.context.Language]}:</label>
                          <DelayInput
                            delayTimeout={1000}
                            className="form-control kt-input"
                            placeholder={service_name[this.context.Language]}
                            value={this.state.ex.Param.name}
                            name="name"
                            onChange={this.handleChangeFilter}
                          />
                        </div>
                        <div className="col-lg-3">
                          <label>{category[this.context.Language]}:</label>
                          <select
                            className="form-control kt-input"
                            value={this.state.ex.Param.categoryid}
                            name="categoryid"
                            onChange={this.handleChangeFilter}
                          >
                            <option value="">{se_category[this.context.Language]}</option>
                            {this.state.ex.CategoryList
                              ? this.state.ex.CategoryList.map(e => {
                                  return <option value={e.Id}>{e.Name}</option>;
                                })
                              : ""}
                          </select>
                        </div>
                        <div className="col-lg-3">
                          <label>{recordPerPage[this.context.Language]}:</label>
                          <select
                            className="form-control kt-input"
                            data-col-index="2"
                            value={this.state.ex.Param.pagesize}
                            name="pagesize"
                            onChange={this.handleChangeFilter}
                          >
                            <option value="10">10 {record[this.context.Language]} / {page[this.context.Language]}</option>
                            <option value="20">20 {record[this.context.Language]} / {page[this.context.Language]}</option>
                            <option value="50">50 {record[this.context.Language]} / {page[this.context.Language]}</option>
                             <option value="100">100 {record[this.context.Language]} / {page[this.context.Language]}</option>
                          </select>
                        </div>
                      </div>
                    </form>

                    <div className="kt-separator kt-separator--border-dashed"></div>

                    <div
                      id="kt_table_1_wrapper"
                      className="dataTables_wrapper dt-bootstrap4"
                    >
                      <div className="row">
                        <div className="col-sm-12">
                          <table className="table table-striped- table-bordered table-hover table-checkable dataTable no-footer dtr-inline table-responsive">
                            <thead>
                              <tr role="row">
                                <th
                                  className="dt-right sorting_disabled"
                                  rowspan="1"
                                  colspan="1"
                                  style={{ width: "1%" }}
                                  aria-label="Record ID"
                                >
                                  <label className="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                    <input
                                      type="checkbox"
                                      className="kt-group-checkable"
                                      onChange={ev => {
                                        this.state.model.Results = this.state.model.Results.map(
                                          e => {
                                            return {
                                              ...e,
                                              IsChecked: ev.target.checked
                                            };
                                          }
                                        );
                                        this.setState(this.state);
                                      }}
                                    />
                                    <span></span>
                                  </label>
                                 </th>
                                 {document.location.href.indexOf("/appointment/nailservice") >= 0 ? "" : (<th>{avatar[this.context.Language]}</th>) }
                                <th>{name[this.context.Language]}</th>
                                <th>{category[this.context.Language]}</th>
                              <th>{price[this.context.Language]}</th>
                              {document.location.href.indexOf("/appointment/nailservice") >= 0 ? "" : (<th>{cdate[this.context.Language]}</th>)}
                              {document.location.href.indexOf("/appointment/nailservice") >= 0 ? "" : (<th>{udate[this.context.Language]}</th>)}
                              <th>{sort[this.context.Language]}</th>
                              </tr>
                            </thead>
                            <tbody>
                              {this.state.model.Results.map((c, index) => {
                                return (
                                  <tr>
                                    <td className="dt-right" tabindex="0">
                                      <label className="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                        <input
                                          type="checkbox"
                                          className="kt-checkable"
                                          value={c._id}
                                          checked={c.IsChecked}
                                          onChange={e => {
                                            c.IsChecked = !c.IsChecked;
                                            this.setState(this.state);
                                          }}
                                        />
                                        <span></span>
                                      </label>
                                        </td>
                                        {document.location.href.indexOf("/appointment/nailservice") >= 0 ?
                                            "" : (
                                                <td>
                                                    <Link
                                                        to={`/admin/nailservice/update/${c.Id}`}
                                                    >
                                                        <img
                                                            src={c.Avatar}
                                                            alt=""
                                                            className="ecs-img-thumbnail-m"
                                                        />
                                                    </Link>
                                                </td>
                                            )}
                                        <td>
                                         
                                            {document.location.href.indexOf("/appointment/nailservice") >= 0 ?
                                                (
                                                    <Link
                                                        to={`/appointment/nailservice/update/${c.Id}`}
                                                    >
                                                        {c.Name}
                                                    </Link>

                                                ) : (
                                                    <Link
                                                        to={`/admin/nailservice/update/${c.Id}`}
                                                    >
                                                        {c.Name}
                                                    </Link>
                                                )}         
                                    </td>
                                    <td>{c.CategoryList[0].Name}</td>
                                    <td>
                                      {c.Price.formatMoney(2, ".", ",", "$")}
                                      {c.PricePlus ? "+" : ""}
                                        </td>
                                        {document.location.href.indexOf("/appointment/nailservice") >= 0 ? "" : (
                                            <td>
                                                {moment
                                                    .utc(c.CreatedDate)
                                                    .format("MM/DD/YYYY hh:mm:ss A")}
                                            </td>
                                        )}      
                                        {document.location.href.indexOf("/appointment/nailservice") >= 0 ? "" : (
                                            <td>
                                                {c.UpdatedDate
                                                    ? moment
                                                        .utc(c.UpdatedDate)
                                                        .format("MM/DD/YYYY hh:mm:ss A")
                                                    : ""}
                                            </td>
                                        )}      
                                    
                                    <td>
                                      <DelayInput
                                        delayTimeout={2000}
                                        type="number"
                                        name="Sort"
                                        value={
                                          parseFloat(c.Sort) == ""
                                            ? "0"
                                            : c.Sort
                                        }
                                        id=""
                                        index={index}
                                        className="form-control"
                                        onChange={this.handleChangeDataRow}
                                      />
                                    </td>
                                  </tr>
                                );
                              })}
                            </tbody>
                          </table>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-sm-12 col-md-5">
                          <div
                            className="dataTables_info"
                            id="kt_table_1_info"
                            role="status"
                            aria-live="polite"
                          >
                            {total[this.context.Language]} {this.state.model.TotalItemCount} {record[this.context.Language]}
                          </div>
                        </div>
                        <div className="col-sm-12 col-md-7 dataTables_pager">
                          <PagedList
                            currentpage={this.state.model.CurrentPage}
                            pagesize={this.state.model.PageSize}
                            totalitemcount={this.state.model.TotalItemCount}
                            totalpagecount={this.state.model.TotalPageCount}
                            ajaxcallback={this.toPage.bind(this)}
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </React.Fragment>
        ) : (
          <div />
        )}
      </React.Fragment>
    );
  }
}
NailServiceList.contextType = AppContext;
export default NailServiceList;
