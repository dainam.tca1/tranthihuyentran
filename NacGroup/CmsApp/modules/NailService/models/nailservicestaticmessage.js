const service = ["Services", "Dịch vụ"];
const serviceList = ["Service List", "Danh sách dịch vụ"];
const service_name = ["Service Name", "Tên dịch vụ"];
const category = ["Category", "Danh mục"];
const se_category = ["-- Select categories --", "-- Chọn danh mục -- "];
const price = ["Price", "Giá"];
const service_price = ["Service Price", "Giá dịch vụ"];
const service_time = ["Service Time", "Thời gian dịch vụ"];
const isPlus = ["Show '+' After Price", "Hiện thị '+' sau giá"];
const sort = ["Sort", "Sắp xếp"];
const avatar = ["Avatar", "Hình đại diện"];
const name = ["Name", "Tên"];
const cdate = ["Created Date", "Ngày tạo"];
const udate = ["Updated Date", "Ngày cập nhật"];
const info = ["Information", "Thông tin"];
const order = ["Order", "Sắp xếp"];
const service_categories = ["Service Categories", "Danh mục dịch vụ"];
const parent_categories = ["Parent Categories", "Danh mục cha"];
const se_parent = ["-- Select Parent Categories -- ", "-- Chọn danh mục cha --"];
const seoTitle = ["SEO Title", "Tiêu đề SEO"];
const seoDescription = ["SEO Description", "Mô tả SEO"];
const seoKeyword = ["SEO Keyword", "Từ khóa SEO"];
const des = ["Description", "Mô tả"];
const addService = ["Add Service", "Thêm mới dịch vụ"];
const updateService = ["Update Service", "Cập nhật dịch vụ"];
const addServiceCate = ["Add Service Categories", "Thêm mới danh mục dịch vụ"];
const updateServiceCate = ["Update Service Categories", "Cập nhật danh mục dịch vụ"];
module.exports = {
    addService,
    updateService,
    addServiceCate,
    updateServiceCate,
    service,
    serviceList,
    service_name,
    category,
    se_category,
    price,
    service_price,
    service_time,
    isPlus,
    sort,
    avatar,
    name,
    cdate,
    udate,
    info,
    order,
    service_categories,
    parent_categories,
    se_parent,
    des,
    seoTitle,
    seoDescription,
    seoKeyword
};
