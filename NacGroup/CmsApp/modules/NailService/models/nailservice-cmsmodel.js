class NailServiceCmsModel {
  constructor() {
    this.Id = null;
    this.Name = null;
    this.Description = null;
    this.Avatar = null;
    this.Price = 0;
    this.PricePlus = false;
    this.Time = 0;
    this.PromotionPrice = 0;
    this.CategoryList = [];
    this.MetaKeyword = null;
    this.MetaDescription = null;
    this.MetaTitle = null;
    this.Url = null;
    this.CreatedDate = new Date();
    this.UpdatedDate = new Date();
    this.Sort = 0;
  }
}
module.exports = NailServiceCmsModel;
