﻿class GalleryCmsModel {
    constructor() {
        this.Id = null;
        this.Title = null;
        this.ShortDescription = null;
        this.PageContent = null;
        this.MetaKeyword = null;
        this.MetaDescription = null;
        this.MetaTitle = null;
        this.Url = null;
        this.IsDefault = false;
        this.CreatedDate = new Date();
        this.UpdatedDate = new Date();
        this.Avatar = null;
        this.ListAlbum = [];
        this.Sort = 0;
    } 
}

export default GalleryCmsModel;