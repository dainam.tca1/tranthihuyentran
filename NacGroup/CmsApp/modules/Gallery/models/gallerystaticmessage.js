const gallery = ["Galleries", "Bộ sưu tập"];
const addgallery = ["Add Gallery", "Thêm mới bộ sưu tập"];
const updategallery = ["Update Gallery", "Cập nhật bộ sưu tập"];
const galleryList = ["Gallery List", "Danh sách bộ sưu tập"];
const gallery_name = ["Gallery Name", "Tên bộ sưu tập"];
const avatar = ["Avatar", "Hình đại diện"];
const name = ["Name", "Tên"];
const cdate = ["Created Date", "Ngày tạo"];
const lupdate = ["Last Update", "Cập nhật lần cuối"];
const order = ["Order", "Sắp xếp"];
const info = ["Information", "Thông tin"];
const short_des = ["Short Description", "Mô tả ngắn"];
const body = ["Body", "Nội dung"];
const isDefault = ["Default Index", "Mặc định"];
const ab_gallery = ["Album Gallery", "Bộ sưu tập album"];
const se_image = ["Select Image", "Chọn hình ảnh"];
const seoTitle = ["SEO Title", "Tiêu đề SEO"];
const seoDescription = ["SEO Description", "Mô tả SEO"];
const seoKeyword = ["SEO Keyword", "Từ khóa SEO"];
const sure = ["Are you sure", "Bạn có chắc chắn không"];
const message = ["Data won't be recovered?", "Dữ liệu không thê phục hồi"];
const yes = ["Yes, delete it", "Vâng, xóa nó đi"];
const no = ["No, cancel", "Không, hủy bỏ"];
const alt = ["Alt", "Mô tả"];
const de_image = ["Delete Image", "Xóa hình ảnh"];
const ch_image = ["Choose Image", "Chọn hình ảnh"];
module.exports = {
    alt,
    de_image,
    ch_image,
    sure,
    message,
    yes,
    no,
    gallery,
    addgallery,
    updategallery,
    galleryList,
    gallery_name,
    avatar,
    name,
    cdate,
    lupdate,
    order,
    info,
    short_des,
    body,
    isDefault,
    ab_gallery,
    se_image,
    seoTitle,
    seoDescription,
    seoKeyword
};
