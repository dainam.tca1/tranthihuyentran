import React, { Component } from "react";
import { Link } from "react-router-dom";
import { DelayInput } from "react-delay-input";
import GalleryCmsModel from "./models/gallery-cmsmodel";
import uploadPlugin from "../../plugins/upload-plugin";
import {
    globalErrorMessage,
    error,
    back,
    save,
    success,
    add,
    remove,
    total,
    record,
    page,
    cDate,
    lDate,
    recordPerPage
} from "../../constants/message";
import {
    alt,
    de_image,
    ch_image,
    sure,
    message,
    yes,
    no,
    gallery,
    addgallery,
    updategallery,
    galleryList,
    gallery_name,
    avatar,
    name,
    cdate,
    lupdate,
    order,
    info,
    short_des,
    body,
    isDefault,
    ab_gallery,
    se_image,
    seoTitle,
    seoDescription,
    seoKeyword
} from "./models/gallerystaticmessage";
import AppContext from "../../components/app-context";
import ImageModel from "../../dtos/ImageModel";
//ckeditor
import CkEditor from "../../components/ckeditor";

var apiurl = "/cms/api/gallery";

class GalleryAddUpdate extends Component {
  constructor(props,context) {
      super(props, context);
    var that = this;

    //action để nhận biết hiện đang add hay update
    var action = null;
    if (document.location.href.indexOf("/admin/gallery/add") >= 0) {
      action = "add";
    } else if (document.location.href.indexOf("/admin/gallery/update") >= 0) {
      action = "update";
    }

    that.state = {
      model: new GalleryCmsModel(),
      ex: {
        Title: null,
        Action: action
      }
    };
    that.setState(that.state);

    if (action === "update") {
      var id = this.props.match.params.id;
      KTApp.blockPage();
      $.ajax({
        url: apiurl + "/getupdate",
        type: "GET",
        dataType: "json",
        contentType: "application/json",
        data: { id },
        success: response => {
          KTApp.unblockPage();
          if (response.status == "success") {
              that.state.model = response.data;
              that.state.ex.Title = updategallery[this.context.Language];
            document.title = that.state.ex.Title;
            that.setState(that.state);
          } else {
            toastr["error"](response.message, error[this.context.Language]);
          }
        },
        error: function(er) {
            KTApp.unblockPage();
            toastr["error"](globalErrorMessage[this.context.Language], error[this.context.Language]);
        }
      });
    } else {
        that.state.ex.Title = addgallery[this.context.Language];
      document.title = that.state.ex.Title;
      that.setState(that.state);
    }
  }

  submitForm() {
    var that = this;
    KTApp.blockPage();
    $.ajax({
      url: apiurl + "/" + that.state.ex.Action,
      type: "POST",
      dataType: "json",
      contentType: "application/json",
      data: JSON.stringify(that.state.model),
      success: response => {
        KTApp.unblockPage();
        if (response.status == "success") {
          swal.fire({
            title: success[this.context.Language],
            text: response.message,
            type: "success",
            onClose: () => {
              that.props.history.push("/admin/emptypage");
              that.props.history.replace({
                pathname: that.props.location.pathname,
                search: that.props.location.search
              });
            }
          });
        } else {
          toastr["error"](response.message, error[this.context.Language]);
        }
      },
      error: function(er) {
        KTApp.unblockPage();
          toastr["error"](globalErrorMessage[this.context.Language], error[this.context.Language]);
      }
    });
  }
  handleDeleteImage() {
    var that = this;

    swal
      .fire({
        title: sure[this.context.Language],
        text: message[this.context.Language],
        type: "warning",
        showCancelButton: true,
        confirmButtonText: yes[this.context.Language],
        cancelButtonText: no[this.context.Language]
      })
      .then(function(result) {
        if (result.value) {
          var model = that.state.model.ListAlbum;
          var result = model.filter(x => {
            return x.IsChecked === false;
          });
          that.state.model.ListAlbum = result;
          that.setState(that.state);
          return true;
        }
      });
  }
  reorder() {
    var that = this;
    that.state.model.ListAlbum.sort(function(a, b) {
      return a.Sort > b.Sort ? 1 : b.Sort > a.Sort ? -1 : 0;
    });
    toastr["success"](DATA_UPDATE_SUCCESSFULL, SUCCESS);
    this.setState(that.state.model);
  }
  componentWillMount() {
    //Load script ckeditor lên
    $("#scriptloading").html(
      `<script src="/adminstatics/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>`
    );
  }

  componentDidMount() {
    //Active menu
    $("#kt_aside_menu .kt-menu__item").removeClass("kt-menu__item--active");
    $("#kt_aside_menu .kt-menu__item[data-id='gallery']").addClass(
      "kt-menu__item--active"
    );
  }

  componentWillUnmount() {
    $("#scriptloading").html("");
  }
  render() {
    return (
      <React.Fragment>
        {this.state && this.state.model ? (
          <React.Fragment>
            <div className="kt-subheader kt-grid__item" id="kt_subheader">
              <div className="kt-subheader__main">
                <div className="kt-subheader__breadcrumbs">
                  <Link
                    to="/admin/dashboard"
                    className="kt-subheader__breadcrumbs-home"
                  >
                    <i className="fa fa-home" />
                  </Link>

                  <span className="kt-subheader__breadcrumbs-separator" />
                  <Link
                    to="/admin/gallery"
                    className="kt-subheader__breadcrumbs-link"
                  >
                     {gallery[this.context.Language]}
                  </Link>
                  <span className="kt-subheader__breadcrumbs-separator" />
                  <span className="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
                    {this.state.ex.Title}
                  </span>
                </div>
              </div>
              <div className="kt-subheader__toolbar">
                <div className="kt-subheader__wrapper">
                  <Link to="/admin/gallery" className="btn btn-secondary">
                    <i className="fa fa-chevron-left" />   {back[this.context.Language]}
                  </Link>
                  <a
                    href="javascript:;"
                    className="btn btn-primary"
                    onClick={() => {
                      this.submitForm();
                    }}
                  >
                    <i className="fa fa-save"></i>   {save[this.context.Language]}
                  </a>
                </div>
              </div>
            </div>
            <div
              className="kt-content kt-grid__item kt-grid__item--fluid"
              id="kt_content"
            >
              <div className="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                <div className="row">
                  <div className="col-lg-8">
                    <div className="kt-portlet kt-portlet--mobile">
                      <div className="kt-portlet__head kt-portlet__head--lg">
                        <div className="kt-portlet__head-label">
                          <h3 className="kt-portlet__head-title">
                              {info[this.context.Language]}
                          </h3>
                        </div>
                      </div>

                      <div className="kt-portlet__body">
                        <div className="form-group">
                          <label className="control-label">{name[this.context.Language]}</label>
                          <input
                            className="form-control"
                            value={this.state.model.Title}
                            placeholder={name[this.context.Language]}
                            onChange={e => {
                              this.state.model.Title = e.target.value;
                              this.state.model.MetaTitle = e.target.value;
                              if (this.state.ex.Action == "add") {
                                this.state.model.Url = e.target.value.cleanUnicode();
                              }
                              this.setState(this.state);
                            }}
                          />
                        </div>
                        <div className="form-group">
                          <label className="control-label">
                            {short_des[this.context.Language]}
                          </label>
                          <textarea
                            className="form-control"
                            value={this.state.model.ShortDescription}
                            placeholder={short_des[this.context.Language]}
                            onChange={e => {
                              this.state.model.ShortDescription =
                                e.target.value;
                              this.setState(this.state);
                            }}
                          ></textarea>
                        </div>
                        <div className="form-group">
                          <label className="control-label">{body[this.context.Language]}</label>
                          <CkEditor
                            id="PageContent"
                            value={this.state.model.PageContent}
                            onChange={e => {
                              this.state.model.PageContent = e;
                              this.setState(this.state);
                            }}
                          />
                        </div>
                        <div className="form-group">
                          <label className="kt-checkbox kt-checkbox-brand">
                            <input
                              type="checkbox"
                              checked={this.state.model.IsDefault}
                              value={this.state.model.IsDefault}
                              onChange={e => {
                                this.state.model.IsDefault = e.target.checked;
                                this.setState(this.state);
                              }}
                            />
                            <span />
                            {isDefault[this.context.Language]}
                          </label>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-4">
                    <div className="kt-portlet kt-portlet--mobile">
                      <div className="kt-portlet__head kt-portlet__head--lg">
                        <div className="kt-portlet__head-label">
                          <h3 className="kt-portlet__head-title">{avatar[this.context.Language]}</h3>
                        </div>
                      </div>

                      <div className="kt-portlet__body">
                        <div className="form-group item-avatar">
                          <input
                            multiple
                            type="file"
                            className="form-control"
                            onChange={e => {
                              var that = this;
                              uploadPlugin.UpdateImage(e, listfile => {
                                that.state.model.Avatar = listfile[0];
                                that.setState(that.state);
                              });
                            }}
                          />
                          <img
                            src={this.state.model.Avatar}
                            className="img-responsive"
                            width="200"
                          />
                        </div>
                      </div>
                    </div>
                    <div className="kt-portlet kt-portlet--mobile">
                      <div className="kt-portlet__head kt-portlet__head--lg">
                        <div className="kt-portlet__head-label">
                          <h3 className="kt-portlet__head-title">SEO</h3>
                        </div>
                      </div>

                      <div className="kt-portlet__body">
                        <div className="form-group">
                          <label className="control-label">{seoTitle[this.context.Language]}</label>
                          <input
                            className="form-control"
                            placeholder={seoTitle[this.context.Language]}
                            maxlength="70"
                            value={this.state.model.MetaTitle}
                            onChange={e => {
                              this.state.model.MetaTitle = e.target.value;
                              this.setState(this.state);
                            }}
                          />
                        </div>
                        <div className="form-group">
                          <label className="control-label">
                            {seoDescription[this.context.Language]}
                          </label>
                          <textarea
                            className="form-control"
                            placeholder={seoDescription[this.context.Language]}
                            maxlength="160"
                            id="MetaDescription"
                            rows="5"
                            value={this.state.model.MetaDescription}
                            onChange={e => {
                              this.state.model.MetaDescription = e.target.value;
                              this.setState(this.state);
                            }}
                          />
                        </div>
                        <div className="form-group">
                          <label className="control-label">{seoKeyword[this.context.Language]}</label>
                          <input
                            className="form-control"
                            placeholder={seoKeyword[this.context.Language]}
                            value={this.state.model.MetaKeyword}
                            onChange={e => {
                              this.state.model.MetaKeyword = e.target.value;
                              this.setState(this.state);
                            }}
                          />
                        </div>
                        <div className="form-group">
                          <label className="control-label">Url</label>
                          <input
                            className="form-control"
                            placeholder="Url slug"
                            value={this.state.model.Url}
                            onChange={e => {
                              this.state.model.Url = e.target.value;
                              this.setState(this.state);
                            }}
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-lg-12">
                    <div className="kt-portlet kt-portlet--mobile">
                      <div className="kt-portlet__head kt-portlet__head--lg">
                        <div className="kt-portlet__head-label">
                          <h3 className="kt-portlet__head-title"> 
                            {ab_gallery[this.context.Language]}
                          </h3>
                        </div>
                      </div>

                      <div className="kt-portlet__body">
                        <div className="form-group">
                          <label>{se_image[this.context.Language]}</label>
                          <div></div>
                          <div className="custom-file">
                            <input
                              type="file"
                              className="custom-file-input"
                              type="file"
                              multiple="multiple"
                              id="imageList"
                              onChange={e => {
                                var that = this;
                                uploadPlugin.UpdateImage(e, listfile => {
                                  listfile.forEach((img, i) => {
                                    var dorder =
                                      that.state.model.ListAlbum.length > 0
                                        ? that.state.model.ListAlbum[
                                            that.state.model.ListAlbum.length -
                                              1
                                          ].Sort + 1
                                        : 0;
                                    var imageitem = new ImageModel();
                                    imageitem.Src = img;
                                    imageitem.Sort = dorder;
                                    imageitem.IsChecked = false;
                                    that.state.model.ListAlbum.push(imageitem);
                                  });
                                  that.setState(that.state);
                                });
                              }}
                            />
                            <label
                              className="custom-file-label"
                              for="imageList"
                            >
                              {ch_image[this.context.Language]}
                            </label>
                          </div>
                        </div>
                        <div className="form-group">
                          <a
                            href="javascript:;"
                            className="btn btn-danger"
                            onClick={e => this.handleDeleteImage()}
                          >
                            {de_image[this.context.Language]}
                          </a>
                        </div>

                        <div className="dataTables_wrapper dt-bootstrap4">
                          <table className="table table-striped- table-bordered table-hover table-checkable dataTable no-footer dtr-inline table-responsive">
                            <thead>
                              <tr>
                                <th className="stt">
                                  <label
                                    className="kt-checkbox kt-checkbox--single kt-checkbox--solid"
                                    onChange={ev => {
                                      this.state.model.ListAlbum = this.state.model.ListAlbum.map(
                                        e => {
                                          return {
                                            ...e,
                                            IsChecked: ev.target.checked
                                          };
                                        }
                                      );
                                      this.setState(this.state);
                                    }}
                                  >
                                    <input type="checkbox" />
                                    <span />
                                  </label>
                                </th>
                                <th className="sort" />
                                <th className="img">{avatar[this.context.Language]}</th>
                                <th>{alt[this.context.Language]}</th>
                                <th>Url</th>
                              </tr>
                            </thead>
                            <tbody className="textlist">
                              {this.state.model.ListAlbum
                                ? this.state.model.ListAlbum.map(
                                    (img, index) => {
                                      return (
                                        <tr>
                                          <td>
                                            {" "}
                                            <label className="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                              <input
                                                type="checkbox"
                                                value={img.Src}
                                                checked={img.IsChecked}
                                                onChange={e => {
                                                  img.IsChecked = !img.IsChecked;
                                                  this.setState(this.state);
                                                }}
                                              />
                                              <span />
                                            </label>
                                          </td>
                                          <td>
                                            <DelayInput
                                              delayTimeout={1000}
                                              type="text"
                                              value={
                                                parseFloat(img.Sort) == ""
                                                  ? "0"
                                                  : parseFloat(img.Sort)
                                              }
                                              className="form-control"
                                              onChange={e => {
                                                var that = this;
                                                img.Sort = e.target.value;
                                                that.reorder();
                                              }}
                                            />
                                          </td>
                                          <td>
                                            <div
                                              className="form-group"
                                              style={{ position: "relative" }}
                                            >
                                              <input
                                                type="file"
                                                className="form-control"
                                                style={{
                                                  position: "absolute",
                                                  zIndex: 99,
                                                  top: 0,
                                                  width: "100%",
                                                  height: "100%",
                                                  opacity: 0
                                                }}
                                                onChange={e => {
                                                  var that = this;
                                                  uploadPlugin.UpdateImage(
                                                    e,
                                                    files => {
                                                      img.Src = files[0];
                                                      that.setState(that.state);
                                                    }
                                                  );
                                                }}
                                              />
                                              <img
                                                src={img.Src}
                                                style={{ height: "100px" }}
                                              />
                                            </div>
                                          </td>
                                          <td>
                                            <input
                                              type="text"
                                              class="form-control"
                                              placeholder="Image description"
                                              value={img.Alt}
                                              onChange={e => {
                                                var that = this;
                                                img.Alt = e.target.value;
                                                that.setState(that.state);
                                              }}
                                            />
                                          </td>
                                          <td>
                                            <input
                                              type="text"
                                              class="form-control"
                                              placeholder="Image Url"
                                              value={img.Url}
                                              onChange={e => {
                                                var that = this;
                                                img.Url = e.target.value;
                                                that.setState(that.state);
                                              }}
                                            />
                                          </td>
                                        </tr>
                                      );
                                    }
                                  )
                                : ""}
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </React.Fragment>
        ) : (
          <div />
        )}
      </React.Fragment>
    );
  }
}
GalleryAddUpdate.contextType = AppContext;
export default GalleryAddUpdate;
