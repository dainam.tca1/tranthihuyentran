import React, { Component } from "react";
import { GLOBAL_ERROR_MESSAGE, ERROR } from "../../constants/message";
import { Link } from "react-router-dom";
import moment from "moment";
const apiurl = "/cms/api/appointment/appointmentcalendar";
import {
  globalErrorMessage,
  error,
  success,
  add,
  remove,
  total,
  record,
  page,
  cDate,
  lDate,
  back,
  save,
  recordPerPage,
  phone,
  name,
  status,
  se_status,
  are_you_sure,
  yes_de,
  no_ca
} from "../../constants/message";
import {
  booking_report,
  booking_list,
  booking_detail,
  c_name,
  c_phone,
  c_email,
  booking_request_time,
  news,
  done,
  confirmed,
  cancel,
  service,
  ser_name,
  qty,
  price,
  total_amount,
  c_info,
  booking_info,
  booking_type,
  personal,
  group,
  number_of_people,
  staff_name
} from "./models/bookingstaticmessage";
import AppContext from "../../components/app-context";
class BookingView extends Component {
  constructor(props, context) {
    super(props, context);
    var that = this;
    that.state = {
      ex: {
        Title: booking_detail[this.context.Language],
        Param: {
          name: null,
          pagesize: null,
          categoryId: null
        }
      }
    };
    that.setState(that.state);
    KTApp.blockPage();
    var id = that.props.match.params.id;
    $.ajax({
      url: apiurl + "/GetUpdate",
      type: "GET",
      dataType: "json",
      data: { id },
      contentType: "application/json",
      success: response => {
        KTApp.unblockPage();
        toastr.clear();
        that.state.model = response;
        that.setState(that.state);
      },
      error: function(er) {
        KTApp.unblockPage();
        toastr.clear();
        toastr["error"](
          globalErrorMessage[this.context.Language],
          error[this.context.Language]
        );
      }
    });
  }

  renderStatus() {
    switch (this.state.model.Status) {
      case 0:
        return news[this.context.Language];
      case 1:
        return confirmed[this.context.Language];
      case 2:
        return done[this.context.Language];
      case 3:
        return cancel[this.context.Language];
      default:
        return null;
    }
  }

  componentWillMount() {}
  componentDidMount() {
    document.title = this.state.ex.Title;
    $("#kt_aside_menu .kt-menu__item").removeClass("kt-menu__item--active");
    $("#kt_aside_menu .kt-menu__item[data-id='booking']").addClass(
      "kt-menu__item--active"
    );
    $(".nac-nav-ul li").removeClass("active");
    $(".nac-nav-ul li[data-id='appointment-list']").addClass("active");
  }
  render() {
    return this.state && this.state.model ? (
      <React.Fragment>
        <div className="kt-subheader kt-grid__item" id="kt_subheader">
          <div className="kt-subheader__main">
            <div className="kt-subheader__breadcrumbs">
              <Link
                to={
                  document.location.href.indexOf("/appointment/view") >= 0
                    ? "/appointment/dashboard"
                    : "/admin/dashboard"
                }
                className="kt-subheader__breadcrumbs-home"
              >
                <i className="fa fa-home" />
              </Link>
              <span className="kt-subheader__breadcrumbs-separator" />
              <Link
                to={
                  document.location.href.indexOf("/appointment/view") >= 0
                    ? "/appointment/list"
                    : "/admin/booking"
                }
                className="kt-subheader__breadcrumbs-link"
              >
                {booking_report[this.context.Language]}
              </Link>
              <span className="kt-subheader__breadcrumbs-separator" />
              <span className="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
                {this.state.ex.Title}
              </span>
            </div>
          </div>
          <div className="kt-subheader__toolbar">
            <div className="kt-subheader__wrapper">
              <Link to="/admin/booking" className="btn btn-secondary">
                <i className="fa fa-chevron-left" />{" "}
                {back[this.context.Language]}
              </Link>
            </div>
          </div>
        </div>

        <div
          className="kt-content kt-grid__item kt-grid__item--fluid"
          id="kt_content"
        >
          <div className="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
            <div className="row">
              <div className="col-lg-6">
                <div className="kt-portlet kt-portlet--mobile">
                  <div className="kt-portlet__head kt-portlet__head--lg">
                    <div className="kt-portlet__head-label">
                      <h3 className="kt-portlet__head-title">
                        {service[this.context.Language]}
                      </h3>
                    </div>
                  </div>
                  <div className="kt-portlet__body">
                    <table className="table table-striped- table-bordered table-hover table-checkable dataTable no-footer dtr-inline">
                      <thead>
                        <tr>
                          <th>{ser_name[this.context.Language]}</th>
                          <th>{total[this.context.Language]}</th>
                        </tr>
                      </thead>
                      <tbody>
                        {this.state.model.AppointmentItems
                          ? this.state.model.AppointmentItems.map(e => {
                              return (
                                <tr>
                                  <td>
                                    <Link
                                      to={`/admin/nailservice/update/${
                                        e.SkuId
                                      }`}
                                    >
                                      {e.Name}
                                    </Link>
                                  </td>
                                  <td>{e.Duration} minutes</td>
                                </tr>
                              );
                            })
                          : ""}
                        <tr>
                          <td className="text-center">Total</td>
                          <td className="bold font-red">
                            <strong>
                              {this.state.model.TotalDuringTime} minutes
                            </strong>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
                <div className="kt-portlet kt-portlet--mobile">
                  <div className="kt-portlet__head kt-portlet__head--lg">
                    <div className="kt-portlet__head-label">
                      <h3 className="kt-portlet__head-title">
                        {c_info[this.context.Language]}
                      </h3>
                    </div>
                  </div>
                  <div className="kt-portlet__body">
                    <div className="kt-widget4">
                      <div className="kt-widget4__item">
                        <span className="kt-widget4__icon">
                          <i className="flaticon2-user" />
                        </span>
                        <a
                          href="#"
                          className="kt-widget4__title kt-widget4__title--light"
                        >
                          {`${this.state.model.FirstName} ${
                            this.state.model.LastName
                          }`}
                        </a>
                      </div>
                      <div className="kt-widget4__item">
                        <span className="kt-widget4__icon">
                          <i className="flaticon2-phone" />
                        </span>
                        <a
                          href="#"
                          className="kt-widget4__title kt-widget4__title--light"
                        >
                          {this.state.model.CustomerPhone}
                        </a>
                      </div>
                      <div className="kt-widget4__item">
                        <span className="kt-widget4__icon">
                          <i className="flaticon2-envelope" />
                        </span>
                        <a
                          href="#"
                          className="kt-widget4__title kt-widget4__title--light"
                        >
                          {this.state.model.CustomerEmail}
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="col-lg-6">
                <div className="kt-portlet kt-portlet--mobile">
                  <div className="kt-portlet__head kt-portlet__head--lg">
                    <div className="kt-portlet__head-label">
                      <h3 className="kt-portlet__head-title">
                        {booking_info[this.context.Language]}
                      </h3>
                    </div>
                  </div>
                  <div className="kt-portlet__body">
                    <div className="kt-widget4">
                      <div className="kt-widget4__item">
                        <span className="kt-widget4__icon">
                          <i className="flaticon2-calendar-6" />
                        </span>
                        <a
                          href="#"
                          className="kt-widget4__title kt-widget4__title--light"
                        >
                          {cDate[this.context.Language]} :
                          {moment
                            .utc(this.state.model.CreatedDate)
                            .format("MM/DD/YYYY h:mm A")}
                        </a>
                      </div>
                      <div className="kt-widget4__item">
                        <span className="kt-widget4__icon">
                          <i className="flaticon2-calendar-5" />
                        </span>
                        <a
                          href="#"
                          className="kt-widget4__title kt-widget4__title--light"
                        >
                          {booking_request_time[this.context.Language]}:{" "}
                          <strong>
                            {moment
                              .utc(
                                this.state.model.AppointmentItems[0]
                                  .StartDateTime
                              )
                              .format("MM/DD/YYYY h:mm A")}
                          </strong>
                        </a>
                      </div>
                      <div className="kt-widget4__item">
                        <span className="kt-widget4__icon">
                          <i className="flaticon2-contract" />
                        </span>
                        <a
                          href="#"
                          className="kt-widget4__title kt-widget4__title--light"
                        >
                          {booking_type[this.context.Language]} :{" "}
                          {this.state.model.AppointmentType == 0
                            ? personal[this.context.Language]
                            : group[this.context.Language]}
                        </a>
                      </div>
                      <div className="kt-widget4__item">
                        <span className="kt-widget4__icon">
                          <i className="flaticon-users" />
                        </span>
                        <a
                          href="#"
                          className="kt-widget4__title kt-widget4__title--light"
                        >
                          {number_of_people[this.context.Language]} :{" "}
                          {this.state.model.NumberOfGuest}
                        </a>
                      </div>
                      <div className="kt-widget4__item">
                        <span className="kt-widget4__icon">
                          <i className="flaticon2-user-outline-symbol" />
                        </span>
                        <a
                          href="#"
                          className="kt-widget4__title kt-widget4__title--light"
                        >
                          {staff_name[this.context.Language]} :{" "}
                          {this.state.model.SelectedStaff
                            ? this.state.model.SelectedStaff.Name
                            : "Default"}
                        </a>
                      </div>

                      <div className="kt-widget4__item">
                        <span className="kt-widget4__icon">
                          <i className="flaticon2-contract" />
                        </span>
                        <a
                          href="#"
                          className="kt-widget4__title kt-widget4__title--light"
                        >
                          {status[this.context.Language]} :{" "}
                          {this.renderStatus()}
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    ) : (
      <div />
    );
  }
}
BookingView.contextType = AppContext;
export default BookingView;
