import React, { Component } from "react";

import { Link } from "react-router-dom";
import { DelayInput } from "react-delay-input";
import PagedList from "../../components/pagedlist";
import moment from "moment";
import FilterListPlugin from "../../plugins/filter-list-plugin";
import RemoveListPlugin from "../../plugins/remove-list-plugin";
const apiurl = "/cms/api/appointment/appointmentcalendar";
import {
  globalErrorMessage,
  error,
  success,
  add,
  remove,
  total,
  record,
  page,
  cDate,
  lDate,
  back,
  save,
  recordPerPage,
  phone,
  name,
  status,
  se_status,
  are_you_sure,
  yes_de,
  no_ca
} from "../../constants/message";
import {
  booking_report,
  booking_list,
  c_name,
  c_phone,
  c_email,
  booking_request_time,
  news,
  done,
  confirmed,
  cancel,
  service,
  ser_name,
  qty,
  price,
  total_amount,
  c_info,
  booking_info,
  booking_type,
  personal,
  group,
  number_of_people,
  staff_name
} from "./models/bookingstaticmessage";
import AppContext from "../../components/app-context";
class BookingList extends Component {
  constructor(props, context) {
    super(props, context);
    var that = this;
    that.state = {
      ex: {
        Title: booking_report[this.context.Language],
        Param: {
          name: null,
          phone: null,
          status: null,
          pagesize: null
        },
        StatusList: [
          {
            Id: 0,
            Text: news[this.context.Language]
          },
          {
            Id: 1,
            Text: confirmed[this.context.Language]
          },
          {
            Id: 2,
            Text: done[this.context.Language]
          },
          {
            Id: 3,
            Text: cancel[this.context.Language]
          }
        ]
      }
    };
    that.setState(that.state);
    that.handleChangeFilter = that.handleChangeFilter.bind(that);
    that.handleChangeDataRow = that.handleChangeDataRow.bind(that);
    that.toPage(1);
  }

  toPage(index) {
    FilterListPlugin.filterdata(index, this, apiurl);
  }
  /**
   * Được gọi khi thay đổi các trường filter cập nhật tham số filter vào state
   * Filter dữ liệu theo tham số mới
   * @param {Event} event
   */
  handleChangeFilter(event) {
    FilterListPlugin.handleChangeFilter(event, this);
    this.toPage(1);
  }
  handleChangeDataRow(event) {
    const target = event.target;
    const value = target.type === "checkbox" ? target.checked : target.value;
    const name = target.name;
    const index = target.getAttribute("index");
    var newobj = { ...this.state.model.Results[index] };
    newobj[name] = isNaN(value) ? value : parseInt(value);
    var that = this;
    KTApp.blockPage();

    $.ajax({
      url: `${apiurl}/updatecustomize`,
      type: "POST",
      dataType: "json",
      contentType: "application/json",
      data: JSON.stringify({
        model: newobj,
        name
      }),

      success: response => {
        KTApp.unblockPage();
        toastr.clear();
        if (response.status == "error") {
          toastr["error"](response.message, error[this.context.Language]);
        } else {
          that.state.model.Results[index][name] = newobj[name];
          that.setState(that.state);
          toastr["success"](response.message, success[this.context.Language]);
        }
      },
      error: function(er) {
        KTApp.unblockPage();
        toastr["error"](
          globalErrorMessage[this.context.Language],
          error[this.context.Language]
        );
      }
    });
  }
  handleDeleteDataRow() {
    RemoveListPlugin.removeList(`${apiurl}/removelist`, this);
  }
  componentWillMount() {}
  componentDidMount() {
    document.title = this.state.ex.Title;
    $("#kt_aside_menu .kt-menu__item").removeClass("kt-menu__item--active");
    $("#kt_aside_menu .kt-menu__item[data-id='booking']").addClass(
      "kt-menu__item--active"
    );
    $(".nac-nav-ul li").removeClass("active");
    $(".nac-nav-ul li[data-id='appointment-list']").addClass("active");
  }
  render() {
    return this.state && this.state.model ? (
      <React.Fragment>
        <div className="kt-subheader kt-grid__item" id="kt_subheader">
          <div className="kt-subheader__main">
            <div className="kt-subheader__breadcrumbs">
              <Link
                to={
                  document.location.href.indexOf("/appointment/list") >= 0
                    ? "/appointment/dashboard"
                    : "/admin/dashboard"
                }
                className="kt-subheader__breadcrumbs-home"
              >
                <i className="fa fa-home" />
              </Link>
              <span className="kt-subheader__breadcrumbs-separator" />

              <span className="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
                {this.state.ex.Title}
              </span>
            </div>
          </div>
          <div className="kt-subheader__toolbar">
            <div className="kt-subheader__wrapper">
              <a
                href="javascript:;"
                onClick={e => this.handleDeleteDataRow()}
                className="btn btn-danger"
              >
                <i className="fa fa-trash-alt" />
                {remove[this.context.Language]}
              </a>
            </div>
          </div>
        </div>

        <div
          className="kt-content kt-grid__item kt-grid__item--fluid"
          id="kt_content"
        >
          <div className="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
            <div className="kt-portlet kt-portlet--mobile">
              <div className="kt-portlet__head kt-portlet__head--lg">
                <div className="kt-portlet__head-label">
                  <h3 className="kt-portlet__head-title">
                    {booking_list[this.context.Language]}
                  </h3>
                </div>
              </div>

              <div className="kt-portlet__body">
                <form className="kt-form kt-form--fit">
                  <div className="row kt-margin-b-20">
                    <div className="col-lg-3">
                      <label>{c_name[this.context.Language]}:</label>
                      <DelayInput
                        delayTimeout={1000}
                        className="form-control kt-input"
                        placeholder={c_name[this.context.Language]}
                        value={this.state.ex.Param.name}
                        name="name"
                        onChange={this.handleChangeFilter}
                      />
                    </div>
                    <div className="col-lg-3">
                      <label>{c_phone[this.context.Language]}:</label>
                      <DelayInput
                        delayTimeout={1000}
                        className="form-control kt-input"
                        placeholder={c_phone[this.context.Language]}
                        value={this.state.ex.Param.phone}
                        name="phone"
                        onChange={this.handleChangeFilter}
                      />
                    </div>
                    {document.location.href.indexOf("/appointment/list") >=
                    0 ? (
                      ""
                    ) : (
                      <div className="col-lg-3">
                        <label>{status[this.context.Language]}:</label>
                        <select
                          className="form-control kt-input"
                          data-col-index="2"
                          value={this.state.ex.Param.status}
                          name="status"
                          onChange={this.handleChangeFilter}
                        >
                          <option value="">
                            {se_status[this.context.Language]}
                          </option>
                          {this.state.ex.StatusList.map((e, ix) => {
                            return <option value={e.Id}>{e.Text}</option>;
                          })}
                        </select>
                      </div>
                    )}

                    <div className="col-lg-3">
                      <label>{recordPerPage[this.context.Language]}:</label>
                      <select
                        className="form-control kt-input"
                        data-col-index="2"
                        value={this.state.ex.Param.pagesize}
                        name="pagesize"
                        onChange={this.handleChangeFilter}
                      >
                        <option value="10">
                          10 {record[this.context.Language]} /{" "}
                          {page[this.context.Language]}
                        </option>
                        <option value="20">
                          20 {record[this.context.Language]} /{" "}
                          {page[this.context.Language]}
                        </option>
                        <option value="50">
                          50 {record[this.context.Language]} /{" "}
                          {page[this.context.Language]}
                        </option>
                        <option value="100">
                          100 {record[this.context.Language]} /{" "}
                          {page[this.context.Language]}
                        </option>
                      </select>
                    </div>
                  </div>
                </form>

                <div className="kt-separator kt-separator--border-dashed" />

                <div
                  id="kt_table_1_wrapper"
                  className="dataTables_wrapper dt-bootstrap4"
                >
                  <div className="row">
                    <div className="col-sm-12">
                      <table className="table table-striped- table-bordered table-hover table-checkable dataTable no-footer dtr-inline">
                        <thead>
                          <tr role="row">
                            <th
                              className="dt-right sorting_disabled"
                              rowspan="1"
                              colspan="1"
                              style={{ width: "1%" }}
                              aria-label="Record ID"
                            >
                              <label className="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                <input
                                  type="checkbox"
                                  className="kt-group-checkable"
                                  onChange={ev => {
                                    this.state.model.Results = this.state.model.Results.map(
                                      e => {
                                        return {
                                          ...e,
                                          IsChecked: ev.target.checked
                                        };
                                      }
                                    );
                                    this.setState(this.state);
                                  }}
                                />
                                <span />
                              </label>
                            </th>
                            <th>ID</th>
                            <th>{c_name[this.context.Language]}</th>
                            <th>{c_phone[this.context.Language]}</th>
                            <th>{c_email[this.context.Language]}</th>
                            <th>{cDate[this.context.Language]}</th>
                            {document.location.href.indexOf(
                              "/appointment/list"
                            ) >= 0 ? (
                              ""
                            ) : (
                              <th>
                                {booking_request_time[this.context.Language]}
                              </th>
                            )}
                            {document.location.href.indexOf(
                              "/appointment/list"
                            ) >= 0 ? (
                              ""
                            ) : (
                              <th>{status[this.context.Language]}</th>
                            )}
                          </tr>
                        </thead>
                        <tbody>
                          {this.state.model.Results.map((c, index) => {
                            return (
                              <tr>
                                <td className="dt-right" tabindex="0">
                                  <label className="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                    <input
                                      type="checkbox"
                                      className="kt-checkable"
                                      value={c._id}
                                      checked={c.IsChecked}
                                      onChange={e => {
                                        c.IsChecked = !c.IsChecked;
                                        this.setState(this.state);
                                      }}
                                    />
                                    <span />
                                  </label>
                                </td>
                                <td>
                                  <Link to={`/admin/booking/view/${c.Id}`}>
                                    #{c.Id}
                                  </Link>
                                </td>
                                <td>{`${c.FirstName} ${c.LastName}`}</td>
                                <td>{c.CustomerPhone}</td>
                                <td>{c.CustomerEmail}</td>
                                <td>
                                  {moment
                                    .utc(c.CreatedDate)
                                    .format("MM/DD/YYYY h:mm A")}
                                </td>
                                <td>
                                  {moment
                                    .utc(c.AppointmentItems[0].StartDateTime)
                                    .format("MM/DD/YYYY h:mm A")}
                                </td>
                                <td>
                                  <select
                                    className="form-control"
                                    value={c.Status}
                                    name="Status"
                                    index={index}
                                    onChange={this.handleChangeDataRow}
                                  >
                                    {this.state.ex.StatusList.map((e, ix) => {
                                      return (
                                        <option value={e.Id}>{e.Text}</option>
                                      );
                                    })}
                                  </select>
                                </td>
                              </tr>
                            );
                          })}
                        </tbody>
                      </table>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-sm-12 col-md-5">
                      <div
                        className="dataTables_info"
                        id="kt_table_1_info"
                        role="status"
                        aria-live="polite"
                      >
                        {total[this.context.Language]}{" "}
                        {this.state.model.TotalItemCount}{" "}
                        {record[this.context.Language]}
                      </div>
                    </div>
                    <div className="col-sm-12 col-md-7 dataTables_pager">
                      <PagedList
                        currentpage={this.state.model.CurrentPage}
                        pagesize={this.state.model.PageSize}
                        totalitemcount={this.state.model.TotalItemCount}
                        totalpagecount={this.state.model.TotalPageCount}
                        ajaxcallback={this.toPage.bind(this)}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    ) : (
      <div />
    );
  }
}
BookingList.contextType = AppContext;
export default BookingList;
