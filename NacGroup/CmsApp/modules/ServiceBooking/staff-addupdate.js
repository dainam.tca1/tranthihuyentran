import React, { Component } from "react";
import StaffCmsModel from "./models/staff-cmsmodel";
import { Link } from "react-router-dom";
import uploadPlugin from "../../plugins/upload-plugin";
import { GENDERTYPE } from "../../constants/enums";
import DateTimePicker from "../../components/datetimepicker";
import moment from "moment";
var apiUrl = "/cms/api/servicebookingstaff";
var apiService = "/api/nailservices";
var apiServiceCategory = "/api/nailservicecategories";
import {
  globalErrorMessage,
  error,
  success,
  add,
  remove,
  total,
  record,
  page,
  cDate,
  lDate,
  back,
  save,
  recordPerPage,
  data_recover
} from "../../constants/message";
import {
  staff,
  staff_name,
  addStaff,
  updateStaff,
  major,
  staff_info,
  female,
  male,
  sex,
  option,
  active,
  date_off,
  se_date_off,
  add_day,
  de_day,
  day,
  spe_date,
  date,
  se_sp_date,
  open_time,
  close_time,
  day_off_seted,
  pl_se_special_date,
  special_date_seted,
  pl_se_begin_hour,
  pl_se_end_hour
} from "./models/staffstaticmessage";
import AppContext from "../../components/app-context";
class StaffAddUpdate extends Component {
  constructor(props, context) {
    super(props, context);
    var action = null;
    if (
      document.location.href.indexOf("/admin/servicebookingstaff/add") >= 0 ||
      document.location.href.indexOf("/appointment/servicebookingstaff/add") >=
        0
    ) {
      action = "add";
    } else if (
      document.location.href.indexOf("/admin/servicebookingstaff/update") >=
        0 ||
      document.location.href.indexOf(
        "/appointment/servicebookingstaff/update"
      ) >= 0
    ) {
      action = "update";
    }
    this.state = {
      model: null,
      ex: {
        Title:
          action === "update"
            ? updateStaff[this.context.Language]
            : addStaff[this.context.Language],
        Action: action,
        AllServiceList: null,
        AllCategoryList: null,
        DayOff: null,
        DaySpecial: {
          Day: null,
          Open: null,
          Close: null
        }
      }
    };
    document.title = this.state.ex.Title;

    this.getAllServiceList();
    this.getAllServiceCategoryList();

    if (this.state.ex.Action === "add") {
      this.state.model = new StaffCmsModel();
    } else if (this.state.ex.Action === "update") {
      this.getupdate();
    }
  }

  submitForm() {
    var that = this;
    that.setState(that.state);
    KTApp.blockPage();
    $.ajax({
      url: apiUrl + "/" + that.state.ex.Action,
      type: "POST",
      dataType: "json",
      contentType: "application/json",
      data: JSON.stringify(that.state.model),
      success: response => {
        KTApp.unblockPage();
        if (response.status == "success") {
          swal.fire({
            title: success[this.context.Language],
            text: response.message,
            type: "success",
            onClose: () => {
              that.props.history.push("/admin/emptypage");
              that.props.history.replace({
                pathname: that.props.location.pathname
              });
            }
          });
        } else {
          toastr["error"](response.message, error[this.context.Language]);
        }
      },
      error: function(er) {
        KTApp.unblockPage();
        toastr["error"](
          globalErrorMessage[this.context.Language],
          error[this.context.Language]
        );
      }
    });
  }

  getupdate() {
    var that = this;
    var id = that.props.match.params.id;
    KTApp.blockPage();
    $.ajax({
      url: apiUrl + "/" + that.state.ex.Action,
      type: "GET",
      dataType: "json",
      contentType: "application/json",
      data: { id: id },
      success: response => {
        KTApp.unblockPage();
        if (response.status === "success") {
          that.state.model = response.data;
          that.setState(that.state);
        } else {
          toastr["error"](response.message, error[this.context.Language]);
        }
      },
      error: function(er) {
        KTApp.unblockPage();
        toastr["error"](
          globalErrorMessage[this.context.Language],
          error[this.context.Language]
        );
      }
    });
  }

  getAllServiceList() {
    var that = this;

    KTApp.blockPage();
    $.ajax({
      url: apiService,
      type: "GET",
      dataType: "json",
      contentType: "application/json",
      success: response => {
        KTApp.unblockPage();
        that.state.ex.AllServiceList = response.map(e => {
          return {
            ...e,
            IsChecked: false
          };
        });
        that.setState(that.state);
      },
      error: function(er) {
        KTApp.unblockPage();
        toastr["error"](
          globalErrorMessage[this.context.Language],
          error[this.context.Language]
        );
      }
    });
  }

  getAllServiceCategoryList() {
    var that = this;

    KTApp.blockPage();
    $.ajax({
      url: apiServiceCategory,
      type: "GET",
      dataType: "json",
      contentType: "application/json",
      success: response => {
        KTApp.unblockPage();
        that.state.ex.AllCategoryList = response.map(e => {
          return {
            ...e,
            IsChecked: false
          };
        });
        that.setState(that.state);
      },
      error: function(er) {
        KTApp.unblockPage();
        toastr["error"](
          globalErrorMessage[this.context.Language],
          error[this.context.Language]
        );
      }
    });
  }
  /**
   * Th�m ng�y off
   */
  addDayOff() {
    // Validate check null
    var DayOffEx = this.state.ex.DayOff;
    if (!DayOffEx) {
      toastr["error"](
        se_date_off[this.context.Language],
        error[this.context.Language]
      );
      return -1;
    }
    if (
      this.state.model.DayOff.filter(e => {
        return moment.utc(DayOffEx, "MM/DD/YYYY").isSame(e.Day);
      }).length > 0
    ) {
      toastr["error"](
        day_off_seted[this.context.Language],
        error[this.context.Language]
      );
      return -1;
    }
    this.state.model.DayOff.push({
      Day: moment.utc(DayOffEx, "MM/DD/YYYY").toDate()
    });
    this.setState(this.state);
  }

  /** X�a ng�y off */
  deleteDateOff() {
    var that = this;

    swal
      .fire({
        title: are_you_sure[this.context.Language],
        text: data_recover[this.context.Language],
        type: "warning",
        showCancelButton: true,
        confirmButtonText: yes_de[this.context.Language],
        cancelButtonText: no_ca[this.context.Language]
      })
      .then(function(result) {
        if (result.value) {
          that.state.model.DayOff = that.state.model.DayOff.filter(x => {
            return !x.IsChecked;
          });
          that.setState(that.state);
        }
      });
  }

  /** Th�m Special Date */
  addSpecialDate() {
    // Validate check null
    var day = this.state.ex.DaySpecial.Day;
    var open = this.state.ex.DaySpecial.Open;
    var close = this.state.ex.DaySpecial.Close;
    if (!day) {
      toastr["error"](
        pl_se_special_date[this.context.Language],
        error[this.context.Language]
      );
      return -1;
    }
    if (
      this.state.model.DaySpecial.filter(e => {
        return moment.utc(day).isSame(e.Day);
      }).length > 0
    ) {
      toastr["error"](
        special_date_seted[this.context.Language],
        error[this.context.Language]
      );
      return -1;
    }

    if (!open) {
      toastr["error"](
        pl_se_begin_hour[this.context.Language],
        error[this.context.Language]
      );
      return -1;
    }
    if (!close) {
      toastr["error"](
        pl_se_end_hour[this.context.Language],
        error[this.context.Language]
      );
      return -1;
    }

    this.state.model.DaySpecial.push({
      Day: moment.utc(day).toDate(),
      Open: open,
      Close: close,
      IsChecked: false
    });
    this.setState(this.state);
  }

  /**X�a special date */
  deleteSpecialDate() {
    var that = this;

    swal
      .fire({
        title: are_you_sure[this.context.Language],
        text: d,
        type: "warning",
        showCancelButton: true,
        confirmButtonText: yes_de[this.context.Language],
        cancelButtonText: no_ca[this.context.Language]
      })
      .then(function(result) {
        if (result.value) {
          that.state.model.DaySpecial = that.state.model.DaySpecial.filter(
            x => {
              return !x.IsChecked;
            }
          );
          that.setState(that.state);
        }
      });
  }
  componentWillMount() {
    $("#cssloading").html(
      `<link href="/adminstatics/global/plugins/flatpickr/css/flatpickr.min.css" rel="stylesheet" type="text/css" />`
    );
    $("#scriptloading").html(
      `<script src="/adminstatics/global/plugins/flatpickr/js/flatpickr.min.js" type="text/javascript"></script>`
    );
  }

  componentDidMount() {
    $("#kt_aside_menu .kt-menu__item").removeClass("kt-menu__item--active");
    $("#kt_aside_menu .kt-menu__item[data-id='staff']").addClass(
      "kt-menu__item--active"
    );
    $(".nac-nav-ul li").removeClass("active");
    $(".nac-nav-ul li[data-id='staff-list']").addClass("active");
  }

  componentWillUnmount() {
    $("#scriptloading").html("");
  }

  componentDidUpdate(prevProps, prevState, snapshot) {}
  render() {
    return (
      <React.Fragment>
        {this.state &&
        this.state.model &&
        this.state.ex.AllCategoryList &&
        this.state.ex.AllServiceList ? (
          <React.Fragment>
            <div className="kt-subheader kt-grid__item" id="kt_subheader">
              <div className="kt-subheader__main">
                <div className="kt-subheader__breadcrumbs">
                  <Link
                    to={
                      document.location.href.indexOf(
                        "/appointment/servicebookingstaff/" +
                          this.state.ex.Action
                      ) >= 0
                        ? "/appointment/dashboard"
                        : "/admin/dashboard"
                    }
                    className="kt-subheader__breadcrumbs-home"
                  >
                    <i className="fa fa-home" />
                  </Link>
                  <span className="kt-subheader__breadcrumbs-separator" />
                  <Link
                    to={
                      document.location.href.indexOf(
                        "/appointment/servicebookingstaff/" +
                          this.state.ex.Action
                      ) >= 0
                        ? "/appointment/servicebookingstaff"
                        : "/admin/servicebookingstaff"
                    }
                    className="kt-subheader__breadcrumbs-link"
                  >
                    {staff[this.context.Language]}
                  </Link>
                  <span className="kt-subheader__breadcrumbs-separator" />
                  <span className="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
                    {this.state.ex.Title}
                  </span>
                </div>
              </div>
              <div className="kt-subheader__toolbar">
                <div className="kt-subheader__wrapper">
                  <Link
                    to={
                      document.location.href.indexOf(
                        "/appointment/servicebookingstaff/" +
                          this.state.ex.Action
                      ) >= 0
                        ? "/appointment/servicebookingstaff"
                        : "/admin/servicebookingstaff"
                    }
                    className="btn btn-secondary"
                  >
                    <i className="fa fa-chevron-left" />{" "}
                    {back[this.context.Language]}
                  </Link>
                  <a
                    href="javascript:;"
                    className="btn btn-primary"
                    onClick={() => {
                      this.submitForm();
                    }}
                  >
                    <i className="fa fa-save" />
                    {save[this.context.Language]}
                  </a>
                </div>
              </div>
            </div>

            <div
              className="kt-content kt-grid__item kt-grid__item--fluid"
              id="kt_content"
            >
              <div className="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                <div className="row">
                  <div className="col-lg-4">
                    <div className="kt-portlet kt-portlet--mobile">
                      <div className="kt-portlet__head kt-portlet__head--lg">
                        <div className="kt-portlet__head-label">
                          <h3 className="kt-portlet__head-title">
                            {major[this.context.Language]}
                          </h3>
                        </div>
                      </div>
                      <div className="kt-portlet__body">
                        <div className="form-group">
                          <ul style={{ "list-style": "none" }}>
                            {this.state.ex.AllCategoryList.map(cate => {
                              var servicelist = this.state.ex.AllServiceList.filter(
                                sers => {
                                  return sers.CategoryList.find(
                                    x => x.Id === cate.Id
                                  );
                                }
                              );
                              return (
                                <li>
                                  <label className="kt-checkbox kt-checkbox--brand">
                                    <input
                                      type="checkbox"
                                      onChange={e => {
                                        this.state.model.ServiceList = this.state.model.ServiceList.filter(
                                          x =>
                                            !servicelist.find(m => m.Id === x)
                                        );
                                        this.setState(this.state);
                                        if (e.target.checked) {
                                          this.state.model.ServiceList = this.state.model.ServiceList.concat(
                                            servicelist.map(x => x.Id)
                                          );
                                        }
                                        this.setState(this.state);
                                      }}
                                    />
                                    <span> </span>
                                    {cate.Name}
                                  </label>
                                  <ul style={{ "list-style": "none" }}>
                                    {servicelist.map(ser => {
                                      return (
                                        <li>
                                          {" "}
                                          <label className="kt-checkbox kt-checkbox--brand">
                                            <input
                                              type="checkbox"
                                              checked={
                                                this.state.model.ServiceList.find(
                                                  e => e === ser.Id
                                                ) != null
                                              }
                                              onChange={e => {
                                                if (e.target.checked) {
                                                  this.state.model.ServiceList.push(
                                                    ser.Id
                                                  );
                                                } else {
                                                  this.state.model.ServiceList = this.state.model.ServiceList.filter(
                                                    i => i !== ser.Id
                                                  );
                                                }
                                                this.setState(this.state);
                                              }}
                                            />
                                            <span> </span>
                                            {ser.Name}
                                          </label>
                                        </li>
                                      );
                                    })}
                                  </ul>
                                </li>
                              );
                            })}
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-4">
                    <div className="kt-portlet kt-portlet--mobile">
                      <div className="kt-portlet__head kt-portlet__head--lg">
                        <div className="kt-portlet__head-label">
                          <h3 className="kt-portlet__head-title">
                            {staff_info[this.context.Language]}
                          </h3>
                        </div>
                      </div>
                      <div className="kt-portlet__body">
                        <div className="form-group item-avatar">
                          <input
                            multiple
                            type="file"
                            className="form-control"
                            onChange={e => {
                              var that = this;
                              uploadPlugin.UpdateImage(e, listfile => {
                                that.state.model.Avatar = listfile[0];
                                that.setState(that.state);
                              });
                            }}
                          />
                          <img src={this.state.model.Avatar} height="100" />
                        </div>

                        <div className="form-group">
                          <label className="control-label">
                            {staff_name[this.context.Language]}
                          </label>
                          <span className="required">*</span>
                          <div className="input-icon right">
                            <i className="fa" />
                            <input
                              className="form-control"
                              placeholder="Staff Name"
                              maxLength="70"
                              value={this.state.model.Name}
                              onChange={e => {
                                this.state.model.Name = e.target.value;
                                this.setState(this.state);
                              }}
                            />
                          </div>
                        </div>
                        <div className="form-group">
                          <label className="control-label">
                            {sex[this.context.Language]}
                          </label>
                          <span className="required">
                            ({option[this.context.Language]})
                          </span>
                          <div className="kt-radio-inline">
                            <label className="kt-radio kt-radio--brand">
                              <input
                                type="radio"
                                checked={
                                  this.state.model.Gender == GENDERTYPE.Male
                                }
                                onChange={e => {
                                  (this.state.model.Gender = GENDERTYPE.Male),
                                    this.setState(this.state);
                                }}
                              />
                              {male[this.context.Language]}
                              <span> </span>
                            </label>
                            <label className="kt-radio kt-radio--brand">
                              <input
                                type="radio"
                                checked={
                                  this.state.model.Gender == GENDERTYPE.Female
                                }
                                onChange={e => {
                                  (this.state.model.Gender = GENDERTYPE.Female),
                                    this.setState(this.state);
                                }}
                              />
                              {female[this.context.Language]}
                              <span> </span>
                            </label>
                          </div>
                        </div>
                        <div className="form-group">
                          <label className="control-label">
                            {active[this.context.Language]}
                          </label>
                          <div className="kt-radio-inline">
                            <label className="kt-radio kt-radio--brand">
                              <input
                                type="checkbox"
                                checked={this.state.model.IsActive}
                                onChange={e => {
                                  (this.state.model.IsActive = !this.state.model
                                    .IsActive),
                                    this.setState(this.state);
                                }}
                              />
                              {active[this.context.Language]}
                              <span> </span>
                            </label>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="kt-portlet kt-portlet--mobile">
                      <div className="kt-portlet__head kt-portlet__head--lg">
                        <div className="kt-portlet__head-label">
                          <h3 className="kt-portlet__head-title">
                            {date_off[this.context.Language]}
                          </h3>
                        </div>
                      </div>
                      <div className="kt-portlet__body">
                        <div className="form-group">
                          <label>{se_date_off[this.context.Language]}</label>
                          <DateTimePicker
                            value={this.state.ex.DayOff}
                            options={{
                              enableTime: false,
                              dateFormat: "m/d/Y"
                            }}
                            onChange={(selectedDates, dateStr, instance) => {
                              this.state.ex.DayOff = dateStr;
                              this.setState(this.state);
                            }}
                            placeholder={se_date_off[this.context.Language]}
                          />
                        </div>
                        <div className="form-group">
                          <a
                            href="javascript:void(0)"
                            className="btn btn-success"
                            onClick={e => this.addDayOff()}
                          >
                            {add_day[this.context.Language]}
                          </a>{" "}
                          <a
                            href="javascript:void(0)"
                            className="btn btn-danger"
                            onClick={e => this.deleteDateOff()}
                          >
                            {de_day[this.context.Language]}
                          </a>
                        </div>
                        <div className="dataTables_wrapper dt-bootstrap4">
                          <table className="table table-striped- table-bordered table-hover table-checkable dataTable">
                            <thead>
                              <tr>
                                <th className="stt">
                                  {" "}
                                  <label
                                    className="kt-checkbox kt-checkbox--single kt-checkbox--solid"
                                    onChange={ev => {
                                      this.state.model.DayOff = this.state.model.DayOff.map(
                                        e => {
                                          return {
                                            ...e,
                                            IsChecked: ev.target.checked
                                          };
                                        }
                                      );
                                      this.setState(this.state);
                                    }}
                                  >
                                    <input
                                      type="checkbox"
                                      className="kt-group-checkable"
                                    />
                                    <span />
                                  </label>
                                </th>
                                <th className="img">
                                  {day[this.context.Language]}
                                </th>
                              </tr>
                            </thead>
                            <tbody className="textlist">
                              {this.state.model.DayOff
                                ? this.state.model.DayOff.map(e => {
                                    return (
                                      <tr>
                                        <td>
                                          <label className="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                            <input
                                              type="checkbox"
                                              className="kt-group-checkable"
                                              value={e.element}
                                              checked={e.IsChecked}
                                              onChange={el => {
                                                e.IsChecked = !e.IsChecked;
                                                this.setState(this.state);
                                              }}
                                            />
                                            <span />
                                          </label>
                                        </td>
                                        <td>
                                          {moment
                                            .utc(e.Day)
                                            .format("MM/DD/YYYY")}
                                        </td>
                                      </tr>
                                    );
                                  })
                                : ""}
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                    <div className="kt-portlet kt-portlet--mobile">
                      <div className="kt-portlet__head kt-portlet__head--lg">
                        <div className="kt-portlet__head-label">
                          <h3 className="kt-portlet__head-title">
                            {spe_date[this.context.Language]}
                          </h3>
                        </div>
                      </div>

                      <div className="kt-portlet__body">
                        <div className="form-group">
                          <label className="control-label">
                            {date[this.context.Language]}
                          </label>
                          <DateTimePicker
                            value={this.state.ex.DaySpecial.Day}
                            options={{
                              enableTime: false,
                              dateFormat: "m/d/Y"
                            }}
                            onChange={(selectedDates, dateStr, instance) => {
                              this.state.ex.DaySpecial.Day = dateStr;
                              this.setState(this.state);
                            }}
                            placeholder={se_sp_date[this.context.Language]}
                          />
                        </div>
                        <div className="form-group">
                          <label className="control-label">
                            {open_time[this.context.Language]}
                          </label>
                          <DateTimePicker
                            value={this.state.ex.DaySpecial.Open}
                            options={{
                              enableTime: true,
                              noCalendar: true,
                              time_24hr: false,
                              dateFormat: "h:i K"
                            }}
                            onChange={(selectedDates, dateStr, instance) => {
                              this.state.ex.DaySpecial.Open = dateStr;
                              this.setState(this.state);
                            }}
                            placeholder={open_time[this.context.Language]}
                          />
                        </div>
                        <div className="form-group">
                          <label className="control-label">
                            {close_time[this.context.Language]}
                          </label>
                          <DateTimePicker
                            value={this.state.ex.DaySpecial.Close}
                            options={{
                              enableTime: true,
                              noCalendar: true,
                              time_24hr: false,
                              dateFormat: "h:i K"
                            }}
                            onChange={(selectedDates, dateStr, instance) => {
                              this.state.ex.DaySpecial.Close = dateStr;
                              this.setState(this.state);
                            }}
                            placeholder={close_time[this.context.Language]}
                          />
                        </div>
                        <div className="form-group">
                          <a
                            href="javascript:void(0)"
                            className="btn btn-success btn-customfile"
                            onClick={e => this.addSpecialDate()}
                          >
                            {add_day[this.context.Language]}
                          </a>{" "}
                          <a
                            href="javascript:void(0)"
                            className="btn btn-danger"
                            onClick={e => this.deleteSpecialDate()}
                          >
                            {de_day[this.context.Language]}
                          </a>
                        </div>
                        <div className="dataTables_wrapper dt-bootstrap4">
                          <table className="table table-striped- table-bordered table-hover table-checkable dataTable">
                            <thead>
                              <tr>
                                <th className="stt">
                                  <label
                                    className="kt-checkbox kt-checkbox--single kt-checkbox--solid"
                                    onChange={ev => {
                                      this.state.model.DaySpecial = this.state.model.DaySpecial.map(
                                        e => {
                                          return {
                                            ...e,
                                            IsChecked: ev.target.checked
                                          };
                                        }
                                      );
                                      this.setState(this.state);
                                    }}
                                  >
                                    <input
                                      className="kt-group-checkable"
                                      type="checkbox"
                                    />
                                    <span />
                                  </label>
                                </th>
                                <th className="img">
                                  {" "}
                                  {day[this.context.Language]}
                                </th>
                                <th> {open_time[this.context.Language]}</th>
                                <th> {close_time[this.context.Language]}</th>
                              </tr>
                            </thead>
                            <tbody className="textlist">
                              {this.state.model.DaySpecial
                                ? this.state.model.DaySpecial.map(e => {
                                    return (
                                      <tr>
                                        <td>
                                          <label className="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                            <input
                                              className="kt-group-checkable"
                                              type="checkbox"
                                              value={e.element}
                                              checked={e.IsChecked}
                                              onChange={el => {
                                                e.IsChecked = !e.IsChecked;
                                                this.setState(this.state);
                                              }}
                                            />
                                            <span />
                                          </label>
                                        </td>
                                        <td>
                                          {moment
                                            .utc(e.Day)
                                            .format("MM/DD/YYYY")}
                                        </td>
                                        <td>{e.Open}</td>
                                        <td>{e.Close}</td>
                                      </tr>
                                    );
                                  })
                                : ""}
                            </tbody>
                          </table>
                        </div>{" "}
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-4">
                    {this.state.model.DayWork
                      ? this.state.model.DayWork.map((e, i) => {
                          return (
                            <div
                              key={i}
                              className="kt-portlet kt-portlet--mobile"
                            >
                              <div className="kt-portlet__head kt-portlet__head--lg">
                                <div className="kt-portlet__head-label">
                                  <h3 className="kt-portlet__head-title">
                                    {e.Day}
                                  </h3>
                                </div>
                              </div>

                              <div className="kt-portlet__body">
                                <div className="form-group">
                                  <label className="control-label">
                                    {open_time[this.context.Language]}
                                  </label>
                                  <DateTimePicker
                                    value={e.Open}
                                    options={{
                                      enableTime: true,
                                      noCalendar: true,
                                      time_24hr: false,
                                      dateFormat: "h:i K"
                                    }}
                                    onChange={(
                                      selectedDates,
                                      dateStr,
                                      instance
                                    ) => {
                                      this.state.model.DayWork[
                                        i
                                      ].Open = dateStr;
                                      this.setState(this.state);
                                    }}
                                    placeholder={
                                      open_time[this.context.Language]
                                    }
                                  />
                                </div>
                                <div className="form-group">
                                  <label className="control-label">
                                    {close_time[this.context.Language]}
                                  </label>
                                  <DateTimePicker
                                    value={e.Close}
                                    options={{
                                      enableTime: true,
                                      noCalendar: true,
                                      time_24hr: false,
                                      dateFormat: "h:i K"
                                    }}
                                    onChange={(
                                      selectedDates,
                                      dateStr,
                                      instance
                                    ) => {
                                      this.state.model.DayWork[
                                        i
                                      ].Close = dateStr;
                                      this.setState(this.state);
                                    }}
                                    placeholder={
                                      close_time[this.context.Language]
                                    }
                                  />
                                </div>
                                <div className="form-group">
                                  <label className="kt-checkbox kt-checkbox--brand">
                                    <input
                                      type="checkbox"
                                      checked={e.IsActive}
                                      onChange={el => {
                                        e.IsActive = !e.IsActive;
                                        this.setState(this.state);
                                      }}
                                    />
                                    <span /> {active[this.context.Language]}
                                  </label>
                                </div>
                              </div>
                            </div>
                          );
                        })
                      : ""}
                  </div>
                </div>
              </div>
            </div>
          </React.Fragment>
        ) : (
          <div />
        )}
      </React.Fragment>
    );
  }
}
StaffAddUpdate.contextType = AppContext;
export default StaffAddUpdate;
