import React, { Component } from "react";
import { DelayInput } from "react-delay-input";
import PagedList from "../../components/pagedlist";
import { Link } from "react-router-dom";
import moment from "moment";
import FilterListPlugin from "../../plugins/filter-list-plugin";
import RemoveListPlugin from "../../plugins/remove-list-plugin";
const apiurl = "/cms/api/servicebookingstaffgpos";
import {
    globalErrorMessage,
    error,
    success,
    add,
    remove,
    total,
    record,
    page,
    cDate,
    lDate,
    back,
    save,
    recordPerPage,
    phone,
    name,
    status
} from "../../constants/message";
import {
   
    avatar,
    sort,
    staff_name,
    addStaff,
    updateStaff,
    major,
    staff_info,
    female,
    male,
    sex,
    option,
    staff,
    gposStaff
} from "./models/staffstaticmessage";
import AppContext from "../../components/app-context";
class GPosStaffList extends Component {
  constructor(props,context) {
    super(props,context);
    var that = this;
    that.state = {
      ex: {
        Title: gposStaff[this.context.Language],
        Param: {
          name: null,
          pagesize: null
        }
      }
    };
    that.setState(that.state);
    that.handleChangeFilter = that.handleChangeFilter.bind(that);
    that.handleChangeDataRow = that.handleChangeDataRow.bind(that);

    that.toPage(1);
  }

  filter(url) {
    var that = this;
    KTApp.blockPage();
    $.ajax({
      url: url,
      type: "GET",
      dataType: "json",
      contentType: "application/json",
      success: response => {
        KTApp.unblockPage();
        that.state.model = response.data;
        that.setState(that.state);
      },
      error: function(er) {
        KTApp.unblockPage();
      }
    });
  }
  toPage(index) {
    var paramStr = "";
    if (this.state.ex.Param != null) {
      for (var key in this.state.ex.Param) {
        if (this.state.ex.Param[key] == null) continue;
        if (paramStr != "") {
          paramStr += "&";
        }
        paramStr += key + "=" + encodeURIComponent(this.state.ex.Param[key]);
      }
    }
    var url = apiurl + "?" + paramStr + (paramStr ? "&" : "") + "page=" + index;
    this.filter(url);
  }

  handleChangeFilter(event) {
    FilterListPlugin.handleChangeFilter(event, this);
    this.toPage(1);
  }
  handleDeleteDataRow() {
    RemoveListPlugin.removeList(`${apiurl}/removelist`, this);
  }
  handleChangeDataRow(event) {
    const target = event.target;
    const value = target.type === "checkbox" ? target.checked : target.value;
    const name = target.name;
    const index = target.getAttribute("index");
    var newobj = { ...this.state.model.Results[index] };
    newobj[name] = isNaN(value) ? value : parseInt(value);
    var that = this;
    KTApp.blockPage();
    $.ajax({
      url: apiurl + "/updatecustomize",
      type: "POST",
      dataType: "json",
      contentType: "application/json",
      data: JSON.stringify({ model: newobj, name }),
      success: response => {
        KTApp.unblockPage();
        toastr.clear();
        if (response.status == "success") {
          that.state.model.Results[index][name] = newobj[name];
          that.setState(that.state);
          toastr["success"](response.message, success[this.context.Language]);
        } else {
          toastr["error"](response.message, error[this.context.Language]);
        }
      },
      error: function(er) {
        KTApp.unblockPage();
        toastr.clear();
          toastr["error"](globalErrorMessage[this.context.Language], error[this.context.Language]);
      }
    });
  }
  componentWillMount() {}
  componentDidMount() {
    document.title = this.state.ex.Title;
    $("#kt_aside_menu .kt-menu__item").removeClass("kt-menu__item--active");
      $("#kt_aside_menu .kt-menu__item[data-id='gpos-staff']").addClass(
      "kt-menu__item--active"
    );
  }
  render() {
    return (
      <React.Fragment>
        {this.state && this.state.model ? (
          <React.Fragment>
            <div className="kt-subheader kt-grid__item" id="kt_subheader">
              <div className="kt-subheader__main">
                <div className="kt-subheader__breadcrumbs">
                  <Link
                    to="/admin/dashboard"
                    className="kt-subheader__breadcrumbs-home"
                  >
                    <i className="fa fa-home" />
                  </Link>

                  <span className="kt-subheader__breadcrumbs-separator" />
                  <span className="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
                    {this.state.ex.Title}
                  </span>
                </div>
              </div>
              <div className="kt-subheader__toolbar">
                <div className="kt-subheader__wrapper">
                
                </div>
              </div>
            </div>
            <div
              className="kt-content kt-grid__item kt-grid__item--fluid"
              id="kt_content"
            >
              <div className="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                <div className="kt-portlet kt-portlet--mobile">
                  <div className="kt-portlet__head kt-portlet__head--lg">
                    <div className="kt-portlet__head-label">
                      <h3 className="kt-portlet__head-title">
                        {this.state.ex.Title}
                      </h3>
                    </div>
                  </div>

                  <div className="kt-portlet__body">
                    <form className="kt-form kt-form--fit">
                      <div className="row kt-margin-b-20">
                        <div className="col-lg-3">
                          <label>{staff_name[this.context.Language]}:</label>
                          <DelayInput
                            delayTimeout={1000}
                            className="form-control kt-input"
                            placeholder={staff_name[this.context.Language]}
                            value={this.state.ex.Param.name}
                            name="name"
                            onChange={this.handleChangeFilter}
                          />
                        </div>
                      
                        <div className="col-lg-3">
                          <label>{recordPerPage[this.context.Language]}:</label>
                          <select
                            className="form-control kt-input"
                            data-col-index="2"
                            value={this.state.ex.Param.pagesize}
                            name="pagesize"
                            onChange={this.handleChangeFilter}
                          >
                            <option value="10">10 {record[this.context.Language]}/ {page[this.context.Language]}</option>
                            <option value="20">20 {record[this.context.Language]} / {page[this.context.Language]}</option>
                            <option value="50">50 {record[this.context.Language]} / {page[this.context.Language]}</option>
                            <option value="100">100 {record[this.context.Language]} / {page[this.context.Language]}</option>
                          </select>
                        </div>
                      </div>
                    </form>

                    <div className="kt-separator kt-separator--border-dashed"></div>

                    <div
                      id="kt_table_1_wrapper"
                      className="dataTables_wrapper dt-bootstrap4"
                    >
                      <div className="row">
                        <div className="col-sm-12">
                          <table className="table table-striped- table-bordered table-hover table-checkable dataTable no-footer dtr-inline">
                            <thead>
                              <tr role="row"> 
                                <th>Id</th>
                                <th>{name[this.context.Language]}</th>
                                <th>{phone[this.context.Language]}</th>
                                <th>{status[this.context.Language]}</th>
                               
                              </tr>
                            </thead>
                            <tbody>
                              {this.state.model.map((c, index) => {
                                  return (
                                      <React.Fragment>
                                          <tr>
                                              <td>
                                                  {c.Id}
                                              </td>
                                              <td>
                                                  <a
                                                      href="javascript:;"
                                                      onClick={e => {
                                                          $(`#detail-${c.Id}`).toggle();
                                                          $(e.target)
                                                              .parent()
                                                              .find("i")
                                                              .toggleClass("rotate");
                                                      }}
                                                  >
                                                      <i className="fa fa-chevron-down" />{" "}
                                                      {c.Name}
                                                  </a>
                                              </td>
                                              <td>
                                                  {c.Phone}
                                              </td>
                                              <td>
                                                  {c.Active ? "Active" : "InActive"}
                                              </td>
                                             
                                          </tr>
                                          <tr
                                              className="detail collapse show"
                                              id={`detail-${c.Id}`}
                                              style={{ display: "none" }}
                                          >
                                              <td colSpan="7">
                                                  <div className="row">
                                                      <div className="col-lg-6">
                                                          <div class="kt-portlet kt-portlet--height-fluid">
                                                              <div class="kt-portlet__head kt-portlet__head--noborder">
                                                                  <div class="kt-portlet__head-label">
                                                                      <h3 class="kt-portlet__head-title">
                                                                          Working Day
                                                                      </h3>
                                                                  </div>
                                                              </div>
                                                              <div class="kt-portlet__body">
                                                                  <div class="kt-widget kt-widget--user-profile-2">
                                                                      <div class="kt-widget__body">
                                                                          <div class="kt-widget__item row">
                                                                              {c.WorkingDaysObj ? c.WorkingDaysObj.map(d => {
                                                                                  return (
                                                                                      <React.Fragment>
                                                                                          <div class="kt-widget__contact col-lg-4">
                                                                                              <span class="kt-widget__label">
                                                                                                  Day:
                                                                                            </span>
                                                                                              <a
                                                                                                  href="#"
                                                                                                  class="kt-widget__data"
                                                                                              >
                                                                                                  {d.Day}
                                                                                              </a>
                                                                                          </div>
                                                                                          <div class="kt-widget__contact col-lg-4">
                                                                                              <span class="kt-widget__label">
                                                                                                  Start:
                                                                                            </span>
                                                                                              <a
                                                                                                  href="#"
                                                                                                  class="kt-widget__data"
                                                                                              >
                                                                                                  {d.Start}
                                                                                              </a>
                                                                                          </div>
                                                                                          <div class="kt-widget__contact col-lg-4">
                                                                                              <span class="kt-widget__label">
                                                                                                  End:
                                                                                            </span>
                                                                                              <a
                                                                                                  href="#"
                                                                                                  class="kt-widget__data"
                                                                                              >
                                                                                                  {d.End}
                                                                                              </a>
                                                                                          </div>
                                                                                      </React.Fragment>
                                                                                      )
                                                                              }):""}

                                                                          </div>
                                                                      </div>
                                                                  </div>
                                                              </div>
                                                          </div>
                                                      </div>
                                                  </div>
                                              </td>
                                          </tr>
                                      </React.Fragment>
                                );
                              })}
                            </tbody>
                          </table>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-sm-12 col-md-5">
                          <div
                            className="dataTables_info"
                            id="kt_table_1_info"
                            role="status"
                            aria-live="polite"
                          >
                            {total[this.context.Language]} {this.state.model.TotalItemCount} {record[this.context.Language]}
                          </div>
                        </div>
                        <div className="col-sm-12 col-md-7 dataTables_pager">
                          <PagedList
                            currentpage={this.state.model.CurrentPage}
                            pagesize={this.state.model.PageSize}
                            totalitemcount={this.state.model.TotalItemCount}
                            totalpagecount={this.state.model.TotalPageCount}
                            ajaxcallback={this.toPage.bind(this)}
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </React.Fragment>
        ) : (
          <div />
        )}
      </React.Fragment>
    );
  }
}
GPosStaffList.contextType = AppContext;
export default GPosStaffList;
