const staff = ["Staff", "Nhân viên"];
const staff_list = ["Staff List", "Danh sách nhân viên"];
const staff_name = ["Staff Name", "Tên nhân viên"];
const avatar = ["Avatar", "Ảnh nhân viên"];
const name = ["Name", "Tên nhân viên"];
const sort = ["Sort", "Sắp xếp"];
const addStaff = ["Add Staff", "Thêm nhân viên"];
const updateStaff = ["Update Staff", "Cập nhật nhân viên"];
const major = ["Majors", "Nghiệp vụ"];
const staff_info = ["Staff Information", "Thông tin nhân viên"];
const female = ["Female", "Nữ"];
const male = ["Male", "Nam"];
const sex = ["Sex", "Giới tính"];
const option = ["Optional", "Tùy chọn"];
const gposStaff = ["GPos Staff", "Nhân viên Gpos"];
const active = ["Is Active", "Kích hoạt"];
const date_off = ["Date Off", "Ngày nghỉ"];
const se_date_off = ["Select Date Off", "Chọn ngày nghỉ"];
const add_day = ["Add Day", "Thêm ngày"];
const de_day = ["Delete Day", "Xóa ngày"];
const day = ["Day", "Ngày"];
const spe_date = ["Special Date", "Ngày đặc biệt"];
const date = ["Date", "Ngày"];
const se_sp_date = ["Select Special Date", "Chọn ngày đặc biệt"];
const open_time = ["Open Time", "Giở mở cửa"];
const close_time = ["Close Time", "Giờ đóng cửa"];
const mon = ["Monday", "Thứ hai"];
const tue = ["Tuesday", "Thứ ba"];
const wed = ["Wednesday", "Thứ tư"];
const thu = ["Thursday", "Thứ năm"];
const fri = ["Friday", "Thứ sáu"];
const sat = ["Saturday", "Thứ bảy"];
const sun = ["Sunday", "Chủ nhật"];
const day_off_seted = ["Day off is selected", "Ngày nghỉ đã được chọn"];
const pl_se_special_date = ["Please select day special", "Hãy chọn ngày đặc biệt"];
const special_date_seted = ["Day Special is selected", "Ngày đặc biệt đã được chọn"];
const pl_se_begin_hour = ["Please select begin hour", "Hãy chọn giờ bắt đầu"];
const pl_se_end_hour = ["Please select end hour", "Hãy chọn giờ kết thúc"];
module.exports = {
    staff_list,
    avatar,
    name,
    sort,
    staff_name,
    addStaff,
    updateStaff,
    major,
    staff_info,
    female,
    male,
    sex,
    option,
    staff,
    gposStaff,
    active,
    date_off,
    se_date_off,
    add_day,
    de_day,
    day,
    spe_date,
    date,
    se_sp_date,
    open_time,
    close_time,
    day_off_seted,
    pl_se_special_date,
    special_date_seted,
    pl_se_begin_hour,
    pl_se_end_hour,
};
