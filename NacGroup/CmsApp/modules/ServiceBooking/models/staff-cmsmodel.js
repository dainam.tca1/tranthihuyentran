class StaffCmsModel {
  constructor() {
    this.Id = null;
    this.Name = null;
    this.Avatar = null;
    this.Gender = 0;
    this.ServiceList = [];
    this.Sort = 0;
    this.DayOff = [];
    this.DaySpecial = [];
    this.DayWork = [];
    this.IsActive = true;
  }
}
module.exports = StaffCmsModel;
