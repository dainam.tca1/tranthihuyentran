//#region booking setting
const booking_setting = ["Booking Setting", "Cài đặt đặt lịch"];
const general_setting = ["General Setting", "Cài đặt chung"];
const session_length = ["Session Length", "Khoảng cách giờ đặt"];
const active = ["Is Active", "Kích hoạt"];
const date_off = ["Date Off", "Ngày nghỉ"];
const se_date_off = ["Select Date Off", "Chọn ngày nghỉ"];
const add_day = ["Add Day", "Thêm ngày"];
const de_day = ["Delete Day", "Xóa ngày"];
const day = ["Day", "Ngày"];
const spe_date = ["Special Date", "Ngày đặc biệt"];
const date = ["Date", "Ngày"];
const se_sp_date = ["Select Special Date", "Chọn ngày đặc biệt"];
const open_time = ["Open Time", "Giở mở cửa"];
const close_time = ["Close Time", "Giờ đóng cửa"];
const mon = ["Monday", "Thứ hai"];
const tue = ["Tuesday", "Thứ ba"];
const wed = ["Wednesday", "Thứ tư"];
const thu = ["Thursday", "Thứ năm"];
const fri = ["Friday", "Thứ sáu"];
const sat = ["Saturday", "Thứ bảy"];
const sun = ["Sunday", "Chủ nhật"];
const day_off_seted = ["Day off is selected", "Ngày nghỉ đã được chọn"];
const pl_se_special_date = ["Please select day special", "Hãy chọn ngày đặc biệt"];
const special_date_seted = ["Day Special is selected", "Ngày đặc biệt đã được chọn"];
const pl_se_begin_hour = ["Please select begin hour", "Hãy chọn giờ bắt đầu"];
const pl_se_end_hour = ["Please select end hour", "Hãy chọn giờ kết thúc"];
//#endregion
//#region bookinglist
const booking_report = ["Booking Reports", "Báo cáo đặt lịch"];
const booking_list = ["Booking List", "Danh sách đặt lịch"];
const c_name = ["Customer Name", "Tên khách hàng"];
const c_phone = ["Customer Phone", "Số điện thoại"];
const c_email = ["Customer Email", "Email"];
const booking_request_time = ["Booking Request Time", "Thời gian đặt lịch"];
const news = ["New", "Mới"];
const done = ["Done", "Đã hoàn thành"];
const confirmed = ["Confirmed", "Đã xác nhận"];
const cancel = ["Cancel", "Hủy"];
const service = ["Services", "Dịch vụ"];
const ser_name = ["Service Name", "Tên dịch vụ"];
const qty = ["Quantity", "Số lượng"];
const price = ["Price", "Giá"];
const total_amount = ["Total Amount", "Tổng cộng"];
const c_info = ["Customer Information", "Thông tin khách hàng"];
const booking_info = ["Booking Information", "Thông tin đặt lịch"];
const booking_type = ["Booking Type", "Loại đặt lịch"];
const personal = ["Personal", "Cá nhân"];
const group = ["Group", "Nhóm"];
const number_of_people = ["Number of People", "Số lượng người"];
const staff_name = ["Staff Name", "Tên nhân viên"];
const booking_detail = ["Booking Detail", "Chi tiết đặt lịch"];
//#endregion
module.exports = {
    booking_setting,
    general_setting,
    session_length,
    active,
    date_off,
    se_date_off,
    add_day,
    de_day,
    day,
    spe_date,
    date,
    se_sp_date,
    open_time,
    close_time,
    mon,
    tue,
    wed,
    thu,
    fri,
    sat,
    sun,
    day_off_seted,
    pl_se_special_date,
    special_date_seted,
    pl_se_begin_hour,
    pl_se_end_hour,
    booking_report,
    booking_list,
    c_name,
    c_phone,
    c_email,
    booking_request_time,
    news,
    done,
    confirmed,
    cancel,
    service,
    ser_name,
    qty,
    price,
    total_amount,
    c_info,
    booking_info,
    booking_type,
    personal,
    group,
    number_of_people,
    staff_name,
    booking_detail
};
