import React, { Component } from "react";
import { DelayInput } from "react-delay-input";
import BlogCommentCmsModel from "./models/blog-comment-cmsmodel";
import PagedList from "../../components/pagedlist";
import moment from "moment";
import {
    globalErrorMessage,
    error,
    success,
    back,
    add,
    save,
    close,
    remove,
    total,
    record,
    page,
    cDate,
    lDate,
    recordPerPage,
    order
} from "../../constants/message";
import {
    comment_list,
    comment,
    blog_have_comment,
    reviews,
    reply,
    hidden_web,
    visible_web,
    load_more,
    cancel,
    spending,
    done
} from "./models/blogstaticmessage";
import AppContext from "../../components/app-context";
import { Link } from "react-router-dom";
import FilterListPlugin from "../../plugins/filter-list-plugin";
import RemoveListPlugin from "../../plugins/remove-list-plugin";
const apiurl = "/cms/api/blogcomment";

function comment_status(val) {
    var html = "";
    switch (val) {
        case 0:
            html += "Cancel";
            break;
        case 1:
            html += "Spending";
            break;
        case 2:
            html += "Done";
            break;
            
    }
    return html;
}

class BlogCommentList extends Component {
  constructor(props,context) {
    super(props,context);
    var that = this;
    that.state = {
        models: new BlogCommentCmsModel(),
      ex: {
        Title: comment_list[this.context.Language],
        Param: {
          pagesize: 10,
          categoryId: null
          },
          Content:null,
        BlogList: [],
        CommentListRight: [],
        CommentListLeft: [],
      }
    };
    that.setState(that.state);
    
    that.handleChangeFilter = that.handleChangeFilter.bind(that);
    that.handleChangeDataRow = that.handleChangeDataRow.bind(that);
      that.toPage(1);
      $.get("/cms/api/blog?pagesize=100", (response) => {
          that.state.ex.BlogList = response.data.Results;
          that.state.ex.CommentListLeft = that.state.model.Results && that.state.model ? that.state.model.Results.map(e => {
              var sku = that.state.ex.BlogList.find(c => { return c.Id == e.BlogId });
              return { ...sku }
          }) : []; 
          that.state.ex.CommentListLeft = that.getUnique(that.state.ex.CommentListLeft, "Id");
          that.setState(that.state);
      })
                                                                                
    }

  getUnique(arr, comp) {

    const unique = arr
        .map(e => e[comp])

        // store the keys of the unique objects
        .map((e, i, final) => final.indexOf(e) === i && i)

        // eliminate the dead keys & store unique objects
        .filter(e => arr[e]).map(e => arr[e]);

    return unique;
}

  toPage(index) {
    FilterListPlugin.filterdata(index, this, apiurl);
  }
  /**
   * Được gọi khi thay đổi các trường filter cập nhật tham số filter vào state
   * Filter dữ liệu theo tham số mới
   * @param {Event} event
   */
  handleChangeFilter(event) {
    FilterListPlugin.handleChangeFilter(event, this);
    this.toPage(1);
  }

  /**
   * Xóa data đã được chọn
   */
  handleDeleteDataRow() {
    RemoveListPlugin.removeList(`${apiurl}/removelist`, this);
  }
  handleChangeDataRow(event) {
    const target = event.target;
    const value = target.type === "checkbox" ? target.checked : target.value;
    const name = target.name;
    const index = target.getAttribute("index");
    var newobj = { ...this.state.model.Results[index] };
    newobj[name] = isNaN(value) ? value : parseInt(value);
    var that = this;
    KTApp.blockPage();
    $.ajax({
      url: apiurl + "/updatecustomize",
      type: "POST",
      dataType: "json",
      contentType: "application/json",
      data: JSON.stringify({ model: newobj, name }),
      success: response => {
        KTApp.unblockPage();
        toastr.clear();
        if (response.status == "success") {
          that.state.model.Results[index][name] = newobj[name];
          that.setState(that.state);
            toastr["success"](response.message, success[this.context.Language]);
        } else {
          toastr["error"](response.message, error[this.context.Language]);
        }
      },
      error: function(er) {
        KTApp.unblockPage();
        toastr.clear();
          toastr["error"](globalErrorMessage[this.context.Language], error[this.context.Language]);
      }
    });
    }
    handleVisibleForm(id) {
        $(`#box-reply-comment-${id}`).slideToggle();
    }
    handleGetCommentbyBlogId(id) {
        var that = this;
        that.state.ex.CommentListRight = that.state.model.Results ? that.state.model.Results.filter(e => { return e.BlogId == id }) : "";
        $(`#kt-nav-left .kt-nav__item`).removeClass("active");
        $(`#kt-nav-left .kt-nav__item[data-product-id='${id}']`).addClass("active");
        that.setState(that.state);
    }
    handleChangeStatus(obj) {
        var that = this;
        delete obj.IsChecked;
        KTApp.blockPage();
        $.ajax({
            url: apiurl + "/update",
            type: "POST",
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify(obj),
            success: response => {
                KTApp.unblockPage();
                toastr.clear();
                if (response.status == "success") {
                
                    that.state.ex.CommentListRight.forEach(e => {
                        if (e.Id == obj.Id) e.ActiveStatus = !e.ActiveStatus;
                    })
                    that.setState(that.state);
                    toastr["success"](response.message, success[this.context.Language]);
                } else {
                    toastr["error"](response.message, error[this.context.Language]);
                }
            },
            error: function (er) {
                KTApp.unblockPage();
                toastr.clear();
                toastr["error"](globalErrorMessage[this.context.Language], error[this.context.Language]);
            }
        });
    }
    handleChangeStatusComment(status,obj) {
        var that = this;
        obj.CommentStatus = status;
        delete obj.IsChecked;
        KTApp.blockPage();
        $.ajax({
            url: apiurl + "/update",
            type: "POST",
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify(obj),
            success: response => {
                KTApp.unblockPage();
                toastr.clear();
                if (response.status == "success") {
                    that.state.ex.CommentListRight = that.state.model.Results ? that.state.model.Results.filter(e => { return e.BlogId == obj.BlogId }) : "";
                    that.setState(that.state);
                    toastr["success"](response.message, success[this.context.Language]);
                } else {
                    toastr["error"](response.message, error[this.context.Language]);
                }
            },
            error: function (er) {
                KTApp.unblockPage();
                toastr.clear();
                toastr["error"](globalErrorMessage[this.context.Language], error[this.context.Language]);
            }
        });
    }
    handleReply(obj) {
        var that = this;
        obj.ParentId = obj.Id;
        obj.Id = null;
        obj.Name = "Admin";
        obj.Body = that.state.ex.Content;
        delete obj.IsChecked;
        KTApp.blockPage();
        $.ajax({
            url: apiurl + "/add",
            type: "POST",
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify(obj),
            success: response => {
                KTApp.unblockPage();
                toastr.clear();
                if (response.status == "success") {
                    that.props.history.push("/admin/emptypage");
                    that.props.history.replace({
                        pathname: that.props.location.pathname,
                        search: that.props.location.search
                    });
                    toastr["success"](response.message, success[this.context.Language]);
                } else {
                    toastr["error"](response.message, error[this.context.Language]);
                }
            },
            error: function (er) {
                KTApp.unblockPage();
                toastr.clear();
                toastr["error"](globalErrorMessage[this.context.Language], error[this.context.Language]);
            }
        });
    }
    handleChangeLoadMore() {
        KTApp.blockPage();
        this.state.ex.Param.pagesize += 10;
        $.get(`/cms/api/blogcomment?pagesize=${this.state.ex.Param.pagesize}`, (response) => {
            KTApp.unblockPage();
            toastr["success"](response.message, success[this.context.Language]);
            that.state.ex.CommentListLeft = response.data.Results ? response.data.Results.map(e => {
                var sku = that.state.ex.BlogList.find(c => { return c.Id == e.BlogId });
                return { ...sku }
            }) : [];   
            that.state.ex.CommentListLeft = that.getUnique(that.state.ex.CommentListLeft, "Id");
            that.setState(that.state);
        })
    }
    componentWillMount() {

    }
  componentDidMount() {
    document.title = this.state.ex.Title;
    $("#kt_aside_menu .kt-menu__item").removeClass("kt-menu__item--active");
    $("#kt_aside_menu .kt-menu__item[data-id='comment-rating']").addClass(
      "kt-menu__item--active"
    );
    $("#kt_aside_menu .kt-menu__item[data-id='comment']").addClass(
      "kt-menu__item--active"
    );
  }
  render() {
    return (
      <React.Fragment>
        {this.state.ex && this.state.model ? (
          <React.Fragment>
            <div className="kt-subheader kt-grid__item" id="kt_subheader">
              <div className="kt-subheader__main">
                <div className="kt-subheader__breadcrumbs">
                  <Link
                    to="/admin/dashboard"
                    className="kt-subheader__breadcrumbs-home"
                  >
                    <i className="fa fa-home" />
                  </Link>

                  <span className="kt-subheader__breadcrumbs-separator" />
                  <span className="kt-subheader__breadcrumbs-link">
                      {comment[this.context.Language]}
                  </span>
                  <span className="kt-subheader__breadcrumbs-separator" />
                  <span className="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
                    {this.state.ex.Title}
                  </span>
                </div>
              </div>
              <div className="kt-subheader__toolbar">
                
              </div>
            </div>
            <div
              className="kt-content kt-grid__item kt-grid__item--fluid"
              id="kt_content"
            >
              <div className="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                <div className="kt-portlet kt-portlet--mobile">
                  <div className="kt-portlet__head kt-portlet__head--lg">
                    <div className="kt-portlet__head-label">
                      <h3 className="kt-portlet__head-title">
                        {this.state.ex.Title}
                      </h3>
                    </div>
                  </div>

                  <div className="kt-portlet__body">
                    <div
                      id="kt_table_1_wrapper"
                      className="dataTables_wrapper dt-bootstrap4"
                    >
                      <div className="row">
                        <div className="col-sm-4">
                                                <div className="kt-portlet kt-portlet--head--noborder kt-portlet--height-fluid">
                                                    <div className="kt-portlet__head">                                     
                                                        <div className="kt-portlet__head-label">   
                                                            {blog_have_comment[this.context.Language]}
                                                        </div>
                                                    </div>
                                                    <div className="kt-portlet__body p-0">
                                                        <div className="kt-section">
                                                            <div className="kt-section__content kt-section__content--fit">
                                                                <ul className="kt-nav kt-nav--bold kt-nav--md-space kt-nav--v3" id="kt-nav-left" role="tablist">
                                                                    {this.state.ex.CommentListLeft.length > 0 ? this.state.ex.CommentListLeft.map(c => {
                                                                        return (
                                                                            <li className="kt-nav__item" data-product-id={c.Id}>
                                                                                <a href="javascript:void(0)" onClick={e => this.handleGetCommentbyBlogId(c.Id)} data-toggle="tab" className="kt-nav__link">
                                                                                    <div className="kt-widget19__userpic">
                                                                                        <img className="kt-widget3__img" style={{ borderRadius: "50%", width: "40px", height: "40px", marginRight: "10px" }} src={c.Avatar} />
                                                                                    </div>
                                                                                    <span className="kt-nav__link-text">{c.Title}</span>
                                                                                </a>

                                                                            </li>
                                                                            )
                                                                    }): ""}
                                                            
                                                                </ul>
                                                                {this.state.ex.CommentListLeft.length > this.state.ex.Param.pagesize ? (
                                                                    <div style={{ textAlign: "center" }}>
                                                                        <a href="javascript:;" onClick={e => {
                                                                            this.handleChangeLoadMore()
                                                                        }} className="btn border"><i className="la la-rotate-left"></i>{load_more[this.context.Language]}</a>
                                                                    </div>
                                                                ): ""}
                                                               
                                                            </div>
                                                        </div>
                                                       
                                                    </div>
                                                </div>
                         </div>
                                            <div className="col-sm-8">
                                                <div className="kt-portlet kt-portlet--height-fluid">
                                                    <div className="kt-portlet__head">
                                                        <div className="kt-portlet__head-label">
                                                            <h3 className="kt-portlet__head-title">
                                                                {reviews[this.context.Language]}
                                                            </h3>
                                                        </div>  
                                                    </div>
                                                    <div className="kt-portlet__body">
                                                        <div className="kt-widget3">
                                                            {this.state.ex.CommentListRight ? this.state.ex.CommentListRight.map(c => {
                                                                if (c.ParentId == null || c.ParentId == "") {
                                                                    return (
                                                                        <React.Fragment>
                                                                            <div className="kt-widget3__item">
                                                                                <div className="kt-widget3__header">
                                                                                    <div className="kt-widget3__user-img">
                                                                                        <img className="kt-widget3__img" style={{ width: "4.2rem" }} src="https://keenthemes.com/metronic/themes/metronic/theme/default/demo1/dist/assets/media/users/user1.jpg" />
                                                                                    </div>
                                                                                    <div className="kt-widget3__info">
                                                                                        <a href="#" className="kt-widget3__username">{c.Name}</a>
                                                                                        <br />
                                                                                        <span className="kt-widget3__email">{c.Email}</span> - <span className="kt-widget3__phone">{c.Phone}</span>
                                                                                        <br />
                                                                                        <span className="kt-widget3__time">{moment.utc(c.CreatedDate).format("MM/DD/YYYY hh:mm A")}</span>
                                                                                    </div>
                                                                                    <span className="kt-widget3__status kt-font-info">
                                                                                        <select onChange={e => { this.handleChangeStatusComment(e.target.value, c) }} value={c.CommentStatus}>
                                                                                            <option value="0">{cancel[this.context.Language]}</option>
                                                                                            <option value="1">{spending[this.context.Language]}</option>
                                                                                            <option value="2">{done[this.context.Language]}</option>
                                                                                        </select>
                                                                                    </span>
                                                                                </div>
                                                                                <div className="kt-widget3__body p-2">
                                                                                    <p className="kt-widget3__text">
                                                                                        {c.Body}
                                                                                    </p>
                                                                                    <a href="javascript:void(0)"
                                                                                        className="btn border" onClick={e => this.handleVisibleForm(c.Id)}><i className="la la-reply"></i> {reply[this.context.Language]}</a>
                                                                                    <a href="javascript:void(0)" className="btn border" onClick={e => this.handleChangeStatus(c)} >
                                                                                        <i className={`la la-${c.ActiveStatus == true ? 'eye-slash' : 'eye'}`}></i>
                                                                                        {c.ActiveStatus == true ? `${hidden_web[this.context.Language]}` : `${visible_web[this.context.Language]}`}
                                                                                    </a>
                                                                                    {this.state.ex.CommentListRight ? this.state.ex.CommentListRight.map(d => {
                                                                                        if (d.ParentId == c.Id) {
                                                                                            return (
                                                                                                <React.Fragment>
                                                                                                    <div className="reply-row mt-2" style={{ "background": "#eaeaea","padding":"5px"}}>
                                                                                                        <div className="kt-widget3__item">
                                                                                                            <div className="kt-widget3__header">
                                                                                                                <div className="kt-widget3__user-img">
                                                                                                                    <img className="kt-widget3__img" style={{ width: "4.2rem" }} src="https://keenthemes.com/metronic/themes/metronic/theme/default/demo1/dist/assets/media/users/user1.jpg" />
                                                                                                                </div>
                                                                                                                <div className="kt-widget3__info">
                                                                                                                    <a href="#" className="kt-widget3__username">{d.Name} - {d.Name == "Admin" ? "QTV":"Customer"}</a>
                                                                                                                    <br />
                                                                                                                    <span className="kt-widget3__time">{moment.utc(d.CreatedDate).format("MM/DD/YYYY hh:mm A")}</span>
                                                                                                                </div>
                                                                                                                <span className="kt-widget3__status kt-font-info">
                                                                                                                    
                                                                                                                </span>
                                                                                                            </div>
                                                                                                            <div className="kt-widget3__body p-2">
                                                                                                                <p className="kt-widget3__text" style={{color:"#333"}}>
                                                                                                                    {d.Body}
                                                                                                                </p>
                                                                                                            </div>
                                                                                                          
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </React.Fragment>
                                                                                            )
                                                                                        }

                                                                                    }) : ""}

                                                                                    <div className="" id={`box-reply-comment-${c.Id}`} style={{ "padding": "10px", backgroundColor: "#f7f8fa", "display": "none" }}>
                                                                                        <textarea className="form-control" id="Content" onChange={e => { this.state.ex.Content = e.target.value; this.setState(this.state); }}>{`@${c.Name}: `}</textarea>
                                                                                        <a href="javascript:void(0)" onClick={e => this.handleReply(c)} className="btn border bg-white" style={{ marginTop: "10px" }}><i className="la la-send"></i> {reply[this.context.Language]}</a>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </React.Fragment>
                                                                    )
                                                                }
                                                            }) : ""}
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                        </div>
                      </div>
                    
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </React.Fragment>
        ) : (
          <div />
        )}
      </React.Fragment>
    );
  }
}
BlogCommentList.contextType = AppContext;
export default BlogCommentList;
