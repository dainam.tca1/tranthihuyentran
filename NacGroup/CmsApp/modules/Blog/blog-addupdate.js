import React, { Component } from "react";
import BlogCmsModel from "./models/blog-cmsmodel";
import TagCmsModel from "./models/tags-cmsmodel";
import { DelayInput } from "react-delay-input";
import { Link } from "react-router-dom";
import uploadPlugin from "../../plugins/upload-plugin";
import ReactSelect from "react-select/creatable";
import {
    globalErrorMessage,
    error,
    success,
    back,
    save
} from "../../constants/message";
import {
    updatePromotionNews,
    updateNews,
    addPromotionNews,
    addNews,
    promotions,
    blogs,
    promotionList,
    blogList,
    information,
    title,
    shortDescription,
    body,
    order,
    avatar,
    category,
    chooseACategory,
    seo,
    seoTitle,
    seoDescription,
    seoKeyword,
    tag,
} from "./models/blogstaticmessage";
import AppContext from "../../components/app-context";

//ckeditor
import CkEditor from "../../components/ckeditor";

var apiurl = "/cms/api/blog";
var apiblogcateurl = "/cms/api/blogcategory";
var apitagurl = "/cms/api/tag";

class BlogAddUpdate extends Component {
    constructor(props, context) {
        super(props, context);
        var action = null;
        if (document.location.href.indexOf("/admin/blog/add") >= 0) {
            action = "add";
        } else if (document.location.href.indexOf("/admin/blog/update") >= 0) {
            action = "update";
        }
        this.state = {
            model: new BlogCmsModel(),
            ex: {
                Title: null,
                Action: action,
                CategoryList: [],
                isPromotion: false,
                TagListUsedTagsToggle: false,
                TagListUsedTags: [],
                TagList: [],
                TagListToggle: false,
                Tag: null,
            }
        };
        this.setState(this.state);
        var that = this;
        this.TagInput = React.createRef();
        //this.handleGetTag = this.handleGetTag.bind(this);
        if (document.location.href.indexOf("type=promotion") >= 0) {
            that.state.ex.isPromotion = true;
            that.setState(that.state);
        }
        $.get(apiblogcateurl, null, response => {
            KTApp.unblockPage();
            toastr.clear();
            if (response.status == "success") {
                that.state.ex.CategoryList = response.data.Results;
                $.get(apitagurl, { "pagesize": 1000 }, response => {
                    that.state.ex.TagListUsedTags = response.data.Results;
                    that.state.ex.TagListUsedTagsToggle = false;
                    that.setState(that.state);
                })
                if (that.state.ex.isPromotion) {
                    var promotionCategory = that.state.ex.CategoryList.find(
                        m => m.Name === "Promotion"
                    );
                    that.state.model.CategoryId =
                        promotionCategory != null ? promotionCategory.Id : null;
                }

                that.setState(that.state);
            } else {
                toastr["error"](response.message, error[that.context.Language]);
            }
        }).fail(() => {
            KTApp.unblockPage();
            toastr.clear();
            toastr["error"](
                globalErrorMessage[that.context.Language],
                error[that.context.Language]
            );
        });
        switch (action) {
            case "update":
                if (that.state.ex.isPromotion === true) {
                    that.state.ex.Title = updatePromotionNews[that.context.Language];
                } else {
                    that.state.ex.Title = updateNews[that.context.Language];
                }
                var id = that.props.match.params.id;
                KTApp.blockPage();
                $.ajax({
                    url: apiurl + "/" + that.state.ex.Action,
                    type: "GET",
                    dataType: "json",
                    contentType: "application/json",
                    data: { id: id },
                    success: response => {
                        KTApp.unblockPage();
                        if (response.status == "success") {
                            that.state.model = response.data;
                            document.title = that.state.ex.Title;
                            that.setState(that.state);
                        } else {
                            toastr["error"](response.message, error[that.context.Language]);
                        }
                    },
                    error: function (er) {
                        KTApp.unblockPage();
                        toastr["error"](
                            globalErrorMessage[that.context.Language],
                            error[that.context.Language]
                        );
                    }
                });
                break;
            case "add":
                if (that.state.ex.isPromotion === true) {
                    that.state.ex.Title = addPromotionNews[that.context.Language];
                } else {
                    that.state.ex.Title = addNews[that.context.Language];
                }
                document.title = that.state.ex.Title;
                that.setState(that.state);
                break;
        }
    }
    // this.setState(this.state);
    handleGetTag() {
        var that = this;
        $(this.TagInput.current).addClass("kt-spinner");
        $.ajax({
            url: apitagurl,
            type: "GET",
            data: { "name": that.state.ex.Tag },
            success: response => {
                $(this.TagInput.current).removeClass("kt-spinner");
                that.state.ex.TagList = response.data.Results;
                if (this.state.ex.TagList.length > 0) {
                    this.state.ex.TagListToggle = true
                } else {
                    this.state.ex.TagListToggle = false
                }
                that.setState(that.state);

            },
            error: function (er) {
                KTApp.unblockPage();
                toastr["error"](
                    globalErrorMessage[that.context.Language],
                    error[that.context.Language]
                );
            }
        });
    }
    submitForm() {
        var that = this;
        KTApp.blockPage();
        $.ajax({
            url: apiurl + "/" + that.state.ex.Action,
            type: "POST",
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify(that.state.model),
            success: response => {
                KTApp.unblockPage();
                if (response.status == "success") {
                    swal.fire({
                        title: success[that.context.Language],
                        text: response.message,
                        type: "success",
                        onClose: () => {
                            that.props.history.push("/admin/emptypage");
                            that.props.history.replace({
                                pathname: that.props.location.pathname,
                                search: that.props.location.search
                            });
                        }
                    });
                } else {
                    toastr["error"](response.message, error[that.context.Language]);
                }
            },
            error: function (er) {
                KTApp.unblockPage();
                toastr["error"](
                    globalErrorMessage[that.context.Language],
                    error[that.context.Language]
                );
            }
        });
    }
    handleRemoveTag(slug) {
        var that = this;
        that.state.model.Tags = that.state.model.Tags.filter(tag => { return tag.Slug != slug })
        that.setState(that.state);
    }
    handleClearTag() {
        this.state.ex.Tag = "";
        this.setState(this.state);
    }
    onChangedTag(temp) {
        var that = this;
        var old_temp = this.state.model.Tags.filter(tag => { return tag.Slug == temp.Slug });

        if (old_temp.length > 0) {
            that.handleClearTag();
            return -1;
        }
        that.state.model.Tags.push(temp);
        that.setState(that.state);
        this.handleClearTag();
    }
    componentWillMount() {
        $("#scriptloading").html(
            `<script src="/adminstatics/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>`
        );
    }

    componentDidMount() {
        document.title = this.state.ex.Title;
        $(".page-sidebar-menu .nav-item").removeClass("active");
        if (this.state.ex.isPromotion === true) {
            $("#kt_aside_menu .kt-menu__item").removeClass("kt-menu__item--active");
            $("#kt_aside_menu .kt-menu__item[data-id='promotion']").addClass(
                "kt-menu__item--active"
            );
            $("#kt_aside_menu .kt-menu__item[data-id='promotion-list']").addClass(
                "kt-menu__item--active"
            );
        } else {
            $("#kt_aside_menu .kt-menu__item").removeClass("kt-menu__item--active");
            $("#kt_aside_menu .kt-menu__item[data-id='tin-tuc']").addClass(
                "kt-menu__item--active"
            );
            $("#kt_aside_menu .kt-menu__item[data-id='danh-sach-tin-tuc']").addClass(
                "kt-menu__item--active"
            );
        }
    }

    componentWillUnmount() {
        $("#scriptloading").html("");
    }
    render() {
        return (
            <React.Fragment>
                {this.state.ex && this.state.model ? (
                    <React.Fragment>
                        <div className="kt-subheader kt-grid__item" id="kt_subheader">
                            <div className="kt-subheader__main">
                                <div className="kt-subheader__breadcrumbs">
                                    <Link
                                        to="/admin/dashboard"
                                        className="kt-subheader__breadcrumbs-home"
                                    >
                                        <i className="fa fa-home" />
                                    </Link>
                                    <span className="kt-subheader__breadcrumbs-separator" />
                                    <span className="kt-subheader__breadcrumbs-link">
                                        {this.state.ex.isPromotion
                                            ? promotions[this.context.Language]
                                            : blogs[this.context.Language]}
                                    </span>
                                    <span className="kt-subheader__breadcrumbs-separator" />
                                    <Link
                                        to={
                                            this.state.ex.isPromotion
                                                ? "/admin/promotion"
                                                : "/admin/blog"
                                        }
                                        className="kt-subheader__breadcrumbs-link"
                                    >
                                        {this.state.ex.isPromotion
                                            ? promotionList[this.context.Language]
                                            : blogList[this.context.Language]}
                                    </Link>
                                    <span className="kt-subheader__breadcrumbs-separator" />
                                    <span className="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
                                        {this.state.ex.Title}
                                    </span>
                                </div>
                            </div>
                            <div className="kt-subheader__toolbar">
                                <div className="kt-subheader__wrapper">
                                    <Link
                                        to={
                                            this.state.ex.isPromotion
                                                ? "/admin/promotion"
                                                : "/admin/blog"
                                        }
                                        className="btn btn-secondary"
                                    >
                                        <i className="fa fa-chevron-left" />{" "}
                                        {back[this.context.Language]}
                                    </Link>
                                    <a
                                        href="javascript:;"
                                        className="btn btn-primary"
                                        onClick={() => {
                                            this.submitForm();
                                        }}
                                    >
                                        <i className="fa fa-save" /> {save[this.context.Language]}
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div
                            className="kt-content kt-grid__item kt-grid__item--fluid"
                            id="kt_content"
                        >
                            <div className="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                                <div className="row">
                                    <div className="col-lg-8">
                                        <div className="kt-portlet kt-portlet--mobile">
                                            <div className="kt-portlet__head kt-portlet__head--lg">
                                                <div className="kt-portlet__head-label">
                                                    <h3 className="kt-portlet__head-title">
                                                        {information[this.context.Language]}
                                                    </h3>
                                                </div>
                                            </div>

                                            <div className="kt-portlet__body">
                                                <div class="form-group">
                                                    <label class="control-label">
                                                        {" "}
                                                        {title[this.context.Language]}
                                                    </label>
                                                    <input
                                                        class="form-control"
                                                        value={this.state.model.Title}
                                                        placeholder={title[this.context.Language]}
                                                        onChange={e => {
                                                            this.state.model.Title = e.target.value;
                                                            this.state.model.MetaTitle = e.target.value;
                                                            this.state.model.Url = e.target.value.cleanUnicode();
                                                            this.setState(this.state);
                                                        }}
                                                    />
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label">
                                                        {" "}
                                                        {shortDescription[this.context.Language]}
                                                    </label>
                                                    <textarea
                                                        name=""
                                                        id=""
                                                        class="form-control"
                                                        cols="30"
                                                        rows="10"
                                                        value={this.state.model.ShortDescription}
                                                        placeholder={
                                                            shortDescription[this.context.Language]
                                                        }
                                                        onChange={e => {
                                                            this.state.model.ShortDescription =
                                                                e.target.value;
                                                            this.setState(this.state);
                                                        }}
                                                    />
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label">
                                                        {" "}
                                                        {body[this.context.Language]}
                                                    </label>
                                                    <CkEditor
                                                        id="PageContent"
                                                        value={this.state.model.PageContent}
                                                        onChange={e => {
                                                            this.state.model.PageContent = e;
                                                            this.setState(this.state);
                                                        }}
                                                    />
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label">
                                                        {" "}
                                                        {order[this.context.Language]}
                                                    </label>
                                                    <input
                                                        type="number"
                                                        className="form-control"
                                                        value={this.state.model.Sort}
                                                        onChange={e => {
                                                            this.state.model.Sort = e.target.value;
                                                            this.setState(this.state);
                                                        }}
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-lg-4">
                                        <div className="kt-portlet kt-portlet--mobile">
                                            <div className="kt-portlet__head kt-portlet__head--lg">
                                                <div className="kt-portlet__head-label">
                                                    <h3 className="kt-portlet__head-title">
                                                        {" "}
                                                        {avatar[this.context.Language]}
                                                    </h3>
                                                </div>
                                            </div>

                                            <div className="kt-portlet__body">
                                                <div className="form-group item-avatar">
                                                    <input           
                                                        type="file"
                                                        className="form-control"
                                                        onChange={e => {
                                                            var that = this;
                                                            uploadPlugin.UpdateImage(e, listfile => {
                                                                that.state.model.Avatar = listfile[0];
                                                                that.setState(that.state);
                                                            });
                                                        }}
                                                    />
                                                    <img
                                                        src={this.state.model.Avatar}
                                                        className="img-responsive"
                                                        width="200"
                                                    />
                                                </div>
                                                <div className="form-group item-avatar">
                                                    <label className="control-label">
                                                       Thumbnail FB (600 * 315)
                                                    </label>
                                                    <input
                                                        type="file"
                                                        className="form-control"
                                                        onChange={e => {
                                                            var that = this;
                                                            uploadPlugin.UpdateImage(e, listfile => {
                                                                that.state.model.ThumbnailFB = listfile[0];
                                                                that.setState(that.state);
                                                            });
                                                        }}
                                                    />
                                                    <img
                                                        src={this.state.model.ThumbnailFB}
                                                        className="img-responsive"
                                                        width="200"
                                                    />
                                                </div>
                                            </div>
                                        </div>

                                        <div className="kt-portlet kt-portlet--mobile">
                                            <div className="kt-portlet__head kt-portlet__head--lg">
                                                <div className="kt-portlet__head-label">
                                                    <h3 className="kt-portlet__head-title">
                                                        {" "}
                                                        {category[this.context.Language]}
                                                    </h3>
                                                </div>
                                            </div>

                                            <div className="kt-portlet__body">
                                                <div className="form-group">
                                                    <label className="control-label">
                                                        {category[this.context.Language]}
                                                    </label>
                                                    <select
                                                        disabled={this.state.ex.isPromotion}
                                                        id=""
                                                        value={this.state.model.CategoryId}
                                                        onChange={e => {
                                                            this.state.model.CategoryId = e.target.value;
                                                            this.setState(this.state);
                                                        }}
                                                        className="form-control"
                                                    >
                                                        <option value={null}>
                                                            {chooseACategory[this.context.Language]}
                                                        </option>

                                                        {this.state.ex.CategoryList ? (
                                                            this.state.ex.CategoryList.map((c, index) => {
                                                                return <option value={c.Id}>{c.Name}</option>;
                                                            })
                                                        ) : (
                                                                <div />
                                                            )}
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="kt-portlet kt-portlet--mobile">
                                            <div className="kt-portlet__head kt-portlet__head--lg">
                                                <div className="kt-portlet__head-label">
                                                    <h3 className="kt-portlet__head-title">
                                                        {" "}
                                                        {tag[this.context.Language]}
                                                    </h3>
                                                </div>
                                            </div>

                                            <div className="kt-portlet__body">
                                                <div className="form-group">
                                                    <label className="control-label">
                                                        {tag[this.context.Language]}
                                                    </label>

                                                    <div ref={this.TagInput} className="kt-spinner--sm kt-spinner--success kt-spinner--right kt-spinner--input">
                                                        <DelayInput
                                                            className="form-control"
                                                            value={this.state.ex.Tag}
                                                            onChange={e => {
                                                                this.state.ex.Tag = e.target.value;
                                                                this.setState(this.state);
                                                                this.handleGetTag();

                                                            }}
                                                            onFocus={e => {
                                                                var that = this;
                                                                //console.log(this.state.ex.TagList);

                                                            }}
                                                            onKeyPress={e => {
                                                                if (e.key === 'Enter' || e.key === 'Tab') {
                                                                    e.preventDefault();
                                                                    var that = this;
                                                                    var temp = new TagCmsModel();
                                                                    temp.Id = null;
                                                                    temp.Name = e.target.value;
                                                                    temp.Slug = e.target.value.cleanUnicode();
                                                                    temp.CreatedDate = new Date();
                                                                    that.onChangedTag(temp);
                                                                }
                                                            }}
                                                        />
                                                    </div>
                                                    <div className={this.state.ex.TagListToggle ? `nac-tag` : `kt-hidden`}>
                                                        <ul className="tag">
                                                            {this.state.ex.TagList ? this.state.ex.TagList.map(c => {
                                                                return <li><a
                                                                    href="javascript:;" onClick={e => {
                                                                        var that = this;
                                                                        that.onChangedTag(c);                                                                                                                                       
                                                                        that.state.ex.TagList = [];
                                                                        that.state.ex.TagListToggle = false;
                                                                        that.handleClearTag();
                                                                        that.setState(that.state);
                                                                    }} >{c.Name}</a></li>
                                                            }) : ""}
                                                        </ul>
                                                    </div>

                                                </div>
                                                <div className="form-group">
                                                    <ul class="tagchecklist" role="list">
                                                        {this.state.model.Tags ? this.state.model.Tags.map(c => {
                                                            return (
                                                                <li>
                                                                    <span
                                                                        className="checklist"
                                                                        onClick={e => {
                                                                            this.handleRemoveTag(c.Slug);
                                                                        }}><i className="fa fa-window-close"></i></span>
                                                                    <span>{c.Name}</span>
                                                                </li>
                                                            )

                                                        }) : ""}
                                                    </ul>
                                                </div>
                                                <div className="form-group">
                                                    <label className="control-label">
                                                        <a href="javascript:;" style={{ textDecoration:"underline" }} onClick={e => { this.state.ex.TagListUsedTagsToggle = !this.state.ex.TagListUsedTagsToggle; this.setState(this.state); }}>Choose from the most used tags</a>
                                                    </label>
                                                    <div className={this.state.ex.TagListUsedTagsToggle ? `tag-list` : `kt-hidden`}>
                                                        {this.state.ex.TagListUsedTags ? this.state.ex.TagListUsedTags.map(c => {
                                                            if (c.Count >= 2) {
                                                                return (
                                                                    <a href="javascript:;"
                                                                        onClick={e => {
                                                                            var that = this;
                                                                            that.onChangedTag(c);
                                                                            that.setState(that.state);
                                                                        }} style={{ fontSize: (2 * 12)+"px" }}>{c.Name}</a>
                                                                )
                                                            } else {
                                                                return (
                                                                    <a href="javascript:;"
                                                                        onClick={e => {
                                                                            var that = this;
                                                                            that.onChangedTag(c);
                                                                            that.setState(that.state);
                                                                        }} style={{ fontSize: "12px" }}>{c.Name}</a>
                                                                )
                                                            }
                                                            
                                                        }) : ""}
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        <div className="kt-portlet kt-portlet--mobile">
                                            <div className="kt-portlet__head kt-portlet__head--lg">
                                                <div className="kt-portlet__head-label">
                                                    <h3 className="kt-portlet__head-title">
                                                        {seo[this.context.Language]}
                                                    </h3>
                                                </div>
                                            </div>

                                            <div className="kt-portlet__body">
                                                <div className="form-group">
                                                    <label className="control-label">
                                                        {seoTitle[this.context.Language]}
                                                    </label>
                                                    <input
                                                        className="form-control"
                                                        placeholder={seoTitle[this.context.Language]}
                                                        maxlength="70"
                                                        value={this.state.model.MetaTitle}
                                                        onChange={e => {
                                                            this.state.model.MetaTitle = e.target.value;
                                                            this.setState(this.state);
                                                        }}
                                                    />
                                                </div>
                                                <div className="form-group">
                                                    <label className="control-label">
                                                        {seoDescription[this.context.Language]}
                                                    </label>
                                                    <textarea
                                                        className="form-control"
                                                        placeholder={seoDescription[this.context.Language]}
                                                        maxlength="160"
                                                        id="MetaDescription"
                                                        rows="5"
                                                        value={this.state.model.MetaDescription}
                                                        onChange={e => {
                                                            this.state.model.MetaDescription = e.target.value;
                                                            this.setState(this.state);
                                                        }}
                                                    />
                                                </div>
                                                <div className="form-group">
                                                    <label className="control-label">
                                                        {seoKeyword[this.context.Language]}
                                                    </label>
                                                    <input
                                                        className="form-control"
                                                        placeholder={seoKeyword[this.context.Language]}
                                                        value={this.state.model.MetaKeyword}
                                                        onChange={e => {
                                                            this.state.model.MetaKeyword = e.target.value;
                                                            this.setState(this.state);
                                                        }}
                                                    />
                                                </div>
                                                <div className="form-group">
                                                    <label className="control-label">Url</label>
                                                    <input
                                                        className="form-control"
                                                        placeholder="Url slug"
                                                        value={this.state.model.Url}
                                                        onChange={e => {
                                                            this.state.model.Url = e.target.value;
                                                            this.setState(this.state);
                                                        }}
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </React.Fragment>
                ) : (
                        <div />
                    )}
            </React.Fragment>
        );
    }
}
BlogAddUpdate.contextType = AppContext;
export default BlogAddUpdate;
