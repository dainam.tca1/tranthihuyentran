class BlogCommentCmsModel {
    constructor() {
        this.Id = null;
        this.Name = null;
        this.Email = null;
        this.Phone = null;
        this.Body = null;
        this.Rating = 0; // decimal
        this.Gender = 0;
        this.CreatedDate = new Date();
        this.UpdatedDate = new Date();
        this.ActiveStatus = false; // An hien True / False
        this.CommentStatus = 0; // 0 Cancel 1 Spending 2 Done 
        this.UserId = null; 
        this.BlogId = null;
        this.ParentId = null;
    }
}

module.exports = BlogCommentCmsModel;
