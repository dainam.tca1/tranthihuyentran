const updatePromotionNews = ["Update Promotion News", "Cập nhật tin khuyến mãi"];
const updateNews = ["Update News", "Cập nhật tin tức"];
const updateBlogCategory = ["Update Blog Categories", "Cập nhật danh mục tin tức"];
const addPromotionNews = ["Add Promotion News", "Thêm tin khuyến mãi"];
const addNews = ["Add News", "Thêm tin tức"];
const addCateNews = ["Add Blog Categories", "Thêm danh mục tin tức"];
const promotions = ["Promotions", "Tin khuyến mãi"];
const blogs = ["Blogs", "Tin tức"];
const blogcategory = ["Blog Categories", "Danh mục tin tức"];
const promotionList = ["Promotion List", "Danh sách tin khuyến mãi"];
const blogList = ["Blog List", "Danh sách tin tức"];
const information = ["Information", "Thông tin cơ bản"];
const title = ["Title", "Tiêu đề"];
const name = ["Name", "Tên danh mục"];
const shortDescription = ["Short Description", "Mô tả ngắn"];
const description = ["Description", "Mô tả"];
const body = ["Body", "Nội dung"];
const order = ["Order", "Sắp xếp"];
const avatar = ["Avatar", "Ảnh đại diện"];
const category = ["Category", "Chuyên mục"];
const parentCategory = ["Parent Category", "Chuyên mục cha"];
const chooseACategory = ["-- Choose a category --", "-- Chọn chuyên mục --"];
const chooseAParentCategory = ["-- Choose Parent Category --", "-- Chọn chuyên mục cha --"];
const seo = ["SEO", "Cấu hình SEO" ];
const seoTitle = ["SEO Title", "Tiêu đề SEO"];
const seoDescription = ["SEO Description", "Mô tả SEO"];
const seoKeyword = ["SEO Keyword", "Từ khóa SEO"];
const tag = ["Tags", "Tags"];
//#region Comment
const comment_list = ["Comment", "Danh sách bình luận"];
const comment = ["Comment - Rating", "Bình luận - Đánh giá"];
const blog_have_comment = ["Blog with comments - new reviews", "Bài viết có bình luận - đánh giá mới"];
const reviews = ["Reviews", "Đánh giá"];
const reply = ["Reply", "Trả lời"];
const hidden_web = ["Hidden on the website", "Ẩn trên web"];
const visible_web = ["Visible on the website", "Hiện trên web"];
const load_more = ["Load more", "Tải thêm"];
const cancel = ["Cancel", "Hủy bỏ"];
const spending = ["Spending", "Đang chờ"];
const done = ["Done", "Đã xử lí"];
//#endregion
module.exports = {
    updatePromotionNews,
    updateNews,
    addPromotionNews,
    addNews,
    promotions,
    blogs,
    promotionList,
    blogList,
    information,
    title,
    shortDescription,
    body,
    order,
    avatar,
    category,
    chooseACategory,
    seo,
    seoTitle,
    seoDescription,
    seoKeyword,
    description,
    chooseAParentCategory,
    name,
    parentCategory,
    blogcategory,
    addCateNews,
    updateBlogCategory,
    tag,
     //#region Comment
    comment_list,
    comment,
    blog_have_comment,
    reviews,
    reply,
    hidden_web,
    visible_web,
    load_more,
    cancel,
    spending,
    done,
    //#endregion
};
