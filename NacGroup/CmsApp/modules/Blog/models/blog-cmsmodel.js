class BlogCmsModel {
    constructor() {
        this.Id = null;
        this.Title = null;
        this.ShortDescription = null;
        this.CategoryId = null;
        this.CategoryName = null;
        this.CategoryUrl = null;
        this.CategoryAvatar = null;
        this.PageContent = null;
        this.MetaKeyword = null;
        this.MetaDescription = null;
        this.MetaTitle = null;
        this.Url = null;
        this.Avatar = null;
        this.CreatedDate = new Date();
        this.UpdatedDate = new Date();
        this.UserProfileId = null;
        this.Tags = [];
        this.ThumbnailFB = null;
        //this.EcsUserName = null;
        this.ViewCount = 0;
        this.Sort = 0;
    }
}
module.exports = BlogCmsModel;

