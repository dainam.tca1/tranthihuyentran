class TagsCmsModel {
    constructor() {
        this.Id = null;
        this.Name = null;
        this.Description = null;
        this.Slug = null;
        this.CreatedDate = new Date();
        this.Count = 0;
    }
}
module.exports = TagsCmsModel;