class BlogCategoryCmsModel {
    constructor() {
        this.Id = null;
        this.Name = null;
        this.Description = null;
        this.Avatar = null;
        this.ParentId = null;
        this.CategoryLevel = 0;
        this.MetaKeyword = null;
        this.MetaDescription = null;
        this.MetaTitle = null;
        this.Url = null;
        this.Sort = 0;
    }
}

module.exports = BlogCategoryCmsModel;
