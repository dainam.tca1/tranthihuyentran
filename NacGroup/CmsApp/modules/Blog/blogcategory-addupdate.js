import React, { Component } from "react";
import BlogCategoryCmsModel from "./models/blogcategory-cmsmodel";
import { Link } from "react-router-dom";
import uploadPlugin from "../../plugins/upload-plugin";
import {
    globalErrorMessage,
    error,
    success,
    back,
    save
} from "../../constants/message";
import {
    addNews,
    blogs,
    information,
    order,
    avatar,
    seo,
    seoTitle,
    seoDescription,
    seoKeyword,
    description,
    chooseAParentCategory,
    name,
    parentCategory,
    blogcategory,
    addCateNews,
    updateBlogCategory
} from "./models/blogstaticmessage";
import AppContext from "../../components/app-context";

//ckeditor
import CkEditor from "../../components/ckeditor";

var apiurl = "/cms/api/blogcategory";
class BlogCategoryAddUpdate extends Component {
  constructor(props, context) {
      super(props, context);
    var action = null;
    if (document.location.href.indexOf("/admin/blogcategory/add") >= 0) {
      action = "add";
    } else if (
      document.location.href.indexOf("/admin/blogcategory/update") >= 0
    ) {
      action = "update";
    }
    this.state = {
      model: new BlogCategoryCmsModel(),
      ex: {
        Title: null,
        Action: action,
        CategoryList: []
      }
    };
    var that = this;
    $.get(apiurl, null, response => {
      KTApp.unblockPage();
      toastr.clear();
      if (response.status == "success") {
        that.state.ex.CategoryList = response.data.Results;
        that.setState(that.state);
      } else {
          toastr["error"](response.message, error[that.context.Language]);
      }
    }).fail(() => {
      KTApp.unblockPage();
      toastr.clear();
        toastr["error"](
            globalErrorMessage[that.context.Language],
            error[that.context.Language]
        );
    });

    if (action === "update") {
      that.state.ex.Title = updateBlogCategory[that.context.Language];
      var id = that.props.match.params.id;
      KTApp.blockPage();
      $.ajax({
        url: apiurl + "/" + that.state.ex.Action,
        type: "GET",
        dataType: "json",
        contentType: "application/json",
        data: { id },
        success: response => {
          KTApp.unblockPage();
          toastr.clear();
          if (response.status == "success") {
            that.state.model = response.data; 
            document.title = that.state.ex.Title;
            that.setState(that.state);
          } else {
              toastr["error"](response.message, error[that.context.Language]);
          }
        },
        error: function(er) {
          KTApp.unblockPage();
          toastr.clear();
          toastr["error"](
              globalErrorMessage[that.context.Language],
              error[that.context.Language]
            );
        }
      });
    } else {
      this.state.ex.Title = addCateNews[that.context.Language];
      this.setState(this.state);
    }
  }
  componentWillMount() {
    $("#scriptloading").html(
      `<script src="/adminstatics/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>`
    );
  }

  componentDidMount() {
    document.title = this.state.ex.Title;
    $("#kt_aside_menu .kt-menu__item").removeClass("kt-menu__item--active");
    $("#kt_aside_menu .kt-menu__item[data-id='tin-tuc']").addClass(
      "kt-menu__item--active"
    );
    $("#kt_aside_menu .kt-menu__item[data-id='danh-muc-tin-tuc']").addClass(
      "kt-menu__item--active"
    );
  }

  componentWillUnmount() {
    $("#scriptloading").html("");
  }

  // this.setState(this.state);
  submitForm() {
    var that = this;
    //var id = that.props.match.params.id;
    KTApp.blockPage();
    $.ajax({
      url: apiurl + "/" + that.state.ex.Action,
      type: "POST",
      dataType: "json",
      contentType: "application/json",
      data: JSON.stringify(that.state.model),
      success: response => {
        KTApp.unblockPage();
        if (response.status == "success") {
          swal.fire({
            title: success[that.context.Language],
            text: response.message,
            type: "success",
            onClose: () => {
              that.props.history.push("/admin/emptypage");
              that.props.history.replace({
                pathname: that.props.location.pathname
              });
            }
          });
        } else {
            toastr["error"](response.message, error[that.context.Language]);
        }
      },
      error: function(er) {
        KTApp.unblockPage();
          toastr["error"](
              globalErrorMessage[that.context.Language],
              error[that.context.Language]
          );
      }
    });
  }

  render() {
    return (
      <React.Fragment>
        {this.state && this.state.model ? (
          <React.Fragment>
            <div className="kt-subheader kt-grid__item" id="kt_subheader">
              <div className="kt-subheader__main">
                <div className="kt-subheader__breadcrumbs">
                  <Link
                    to="/admin/dashboard"
                    className="kt-subheader__breadcrumbs-home"
                  >
                    <i className="fa fa-home" />
                  </Link>
                  <span className="kt-subheader__breadcrumbs-separator" />
                  <span className="kt-subheader__breadcrumbs-link"> {blogs[this.context.Language]}</span>
                  <span className="kt-subheader__breadcrumbs-separator" />
                  <Link
                    to="/admin/blogcategory"
                    className="kt-subheader__breadcrumbs-link"
                  >
                     {blogcategory[this.context.Language]}
                  </Link>
                  <span className="kt-subheader__breadcrumbs-separator" />
                  <span className="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
                    {this.state.ex.Title}
                  </span>
                </div>
              </div>
              <div className="kt-subheader__toolbar">
                <div className="kt-subheader__wrapper">
                                <Link to="/admin/blogcategory" className="btn btn-secondary">
                                    <i className="fa fa-chevron-left" /> {back[this.context.Language]}
                  </Link>
                  <a
                    href="javascript:;"
                    className="btn btn-primary"
                    onClick={() => {
                      this.submitForm();
                    }}
                  >
                    <i className="fa fa-save"></i> {save[this.context.Language]}
                  </a>
                </div>
              </div>
            </div>
            <div
              className="kt-content kt-grid__item kt-grid__item--fluid"
              id="kt_content"
            >
              <div className="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                <div className="row">
                  <div className="col-lg-8">
                    <div className="kt-portlet kt-portlet--mobile">
                      <div className="kt-portlet__head kt-portlet__head--lg">
                        <div className="kt-portlet__head-label">
                          <h3 className="kt-portlet__head-title">
                             {information[this.context.Language]}
                          </h3>
                        </div>
                      </div>

                      <div className="kt-portlet__body">
                        <div className="form-group">
                          <label className="control-label">{name[this.context.Language]}</label>
                          <input
                            className="form-control"
                            value={this.state.model.Name}
                            placeholder={name[this.context.Language]}
                            onChange={e => {
                              this.state.model.Name = e.target.value;
                              this.state.model.MetaTitle = e.target.value;
                              this.state.model.Url = e.target.value.cleanUnicode();
                              this.setState(this.state);
                            }}
                          />
                        </div>
                        <div className="form-group">
                          <label className="control-label">
                            {parentCategory[this.context.Language]}
                          </label>
                          <select
                            id=""
                            value={this.state.model.ParentId}
                            onChange={e => {
                              this.state.model.ParentId = e.target.value;
                              this.setState(this.state);
                            }}
                            className="form-control"
                          >
                            <option value="">
                             {chooseAParentCategory[this.context.Language]}
                            </option>
                            {this.state.ex.CategoryList ? (
                              this.state.ex.CategoryList.map(c => {
                                return <option value={c.Id}>{c.Name}</option>;
                              })
                            ) : (
                              <div />
                            )}
                          </select>
                        </div>
                        <div className="form-group">
                          <label className="control-label">{description[this.context.Language]}</label>
                          <CkEditor
                            id="PageContent"
                            value={this.state.model.Description}
                            onChange={e => {
                              this.state.model.Description = e;
                              this.setState(this.state);
                            }}
                          />
                        </div>
                        <div className="form-group">
                          <label className="control-label">{order[this.context.Language]}</label>
                          <input
                            type="number"
                            min="0"
                            max="1000"
                            value={this.state.model.Sort}
                            onChange={e => {
                              this.state.model.Sort = e.target.value;
                              this.setState(this.state);
                            }}
                            className="form-control"
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-4">
                    <div className="kt-portlet kt-portlet--mobile">
                      <div className="kt-portlet__head kt-portlet__head--lg">
                        <div className="kt-portlet__head-label">
                          <h3 className="kt-portlet__head-title">{avatar[this.context.Language]}</h3>
                        </div>
                      </div>

                      <div className="kt-portlet__body">
                        <div className="form-group item-avatar">
                          <input
                            multiple
                            type="file"
                            className="form-control"
                            onChange={e => {
                              var that = this;
                              uploadPlugin.UpdateImage(e, listfile => {
                                that.state.model.Avatar = listfile[0];
                                that.setState(that.state);
                              });
                            }}
                          />
                          <img
                            src={this.state.model.Avatar}
                            className="img-responsive"
                            width="200"
                          />
                        </div>
                      </div>
                    </div>

                    <div className="kt-portlet kt-portlet--mobile">
                      <div className="kt-portlet__head kt-portlet__head--lg">
                        <div className="kt-portlet__head-label">
                                                <h3 className="kt-portlet__head-title">
                                                    {seo[this.context.Language]}
                                                </h3>
                        </div>
                      </div>

                      <div className="kt-portlet__body">
                        <div className="form-group">
                           <label className="control-label">
                              {seoTitle[this.context.Language]}
                           </label>
                          <input
                            className="form-control"
                            placeholder= {seoTitle[this.context.Language]}
                            maxlength="70"
                            value={this.state.model.MetaTitle}
                            onChange={e => {
                              this.state.model.MetaTitle = e.target.value;
                              this.setState(this.state);
                            }}
                          />
                        </div>
                        <div className="form-group">
                          <label className="control-label">
                            {seoDescription[this.context.Language]}
                          </label>
                          <textarea
                            className="form-control"
                            placeholder= {seoDescription[this.context.Language]}
                            maxlength="160"
                            id="MetaDescription"
                            rows="5"
                            value={this.state.model.MetaDescription}
                            onChange={e => {
                              this.state.model.MetaDescription = e.target.value;
                              this.setState(this.state);
                            }}
                          />
                        </div>
                        <div className="form-group">
                         <label className="control-label">{seoKeyword[this.context.Language]}</label>
                          <input
                            className="form-control"
                            placeholder={seoKeyword[this.context.Language]}
                            value={this.state.model.MetaKeyword}
                            onChange={e => {
                              this.state.model.MetaKeyword = e.target.value;
                              this.setState(this.state);
                            }}
                          />
                        </div>
                        <div className="form-group">
                          <label className="control-label">Url</label>
                          <input
                            className="form-control"
                            placeholder="Url slug"
                            value={this.state.model.Url}
                            onChange={e => {
                              this.state.model.Url = e.target.value;
                              this.setState(this.state);
                            }}
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </React.Fragment>
        ) : (
          <div />
        )}
      </React.Fragment>
    );
  }
}
BlogCategoryAddUpdate.contextType = AppContext;
export default BlogCategoryAddUpdate;
