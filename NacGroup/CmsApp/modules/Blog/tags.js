import React, { Component } from "react";
import { DelayInput } from "react-delay-input";
import PagedList from "../../components/pagedlist";
import TagCmsModel from "./models/tags-cmsmodel";
import {
    error,
    success,
    add,
    remove,
    total,
    record,
    save,
    close,
    recordPerPage,
} from "../../constants/message";
import AppContext from "../../components/app-context";
import { Link } from "react-router-dom";
import FilterListPlugin from "../../plugins/filter-list-plugin";
import RemoveListPlugin from "../../plugins/remove-list-plugin";
const apiurl = "/cms/api/tag";

class Tags extends Component {
    constructor(props, context) {
        super(props, context);
        var that = this;
        that.state = {
            tmpModel: new TagCmsModel(),
            newModel: new TagCmsModel(),
            ex: {
                Title: "Tags",
                Param: {
                    pagesize: null,
                    categoryId: null
                }
            }
        };
        this.NameInput = React.createRef();
        this.DesInput = React.createRef();
        this.popup = React.createRef();
        that.setState(that.state);
        that.handleChangeFilter = that.handleChangeFilter.bind(that);
        that.handleChangeDataRow = that.handleChangeDataRow.bind(that);
        that.toPage(1);
    }

    toPage(index) {
        FilterListPlugin.filterdata(index, this, apiurl);
    }
    /**
     * Được gọi khi thay đổi các trường filter cập nhật tham số filter vào state
     * Filter dữ liệu theo tham số mới
     * @param {Event} event
     */
    handleChangeFilter(event) {
        FilterListPlugin.handleChangeFilter(event, this);
        this.toPage(1);
    }

    /**
     * Xóa data đã được chọn
     */
    handleDeleteDataRow() {
        RemoveListPlugin.removeList(`${apiurl}/removelist`, this);
    }
    handleUpdate() {
        var that = this;
        KTApp.blockPage();
        $.ajax({
            url: apiurl + "/update",
            type: "POST",
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify(this.state.newModel),
            success: response => {
                KTApp.unblockPage();
                toastr.clear();
                if (response.status == "success") {
                   
                    that.state.model.Results.forEach(tag => {
                        if (tag.Id == this.state.newModel.Id) {
                            tag.Id = this.state.newModel.Id;
                            tag.Name = this.state.newModel.Name;
                            tag.Description = this.state.newModel.Description;
                            tag.Slug = this.state.newModel.Slug;
                            tag.Count = this.state.newModel.Count;
                            that.setState(that.state);
                        }  
                    })  
                    that.setState(that.state);
                    $(this.NameInput.current).val("");
                    $(this.DesInput.current).val("");

                    toastr["success"](response.message, success[that.context.Language]);
                } else {
                    toastr["error"](response.message, error[that.context.Language]);
                }
            },
            error: function (er) {
                KTApp.unblockPage();
                toastr.clear();
                toastr["error"](GLOBAL_ERROR_MESSAGE, error[that.context.Language]);
            }
        });
    }
    handleSubmit() {
        var that = this;
        KTApp.blockPage();
        $.ajax({
            url: apiurl + "/add",
            type: "POST",
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify(this.state.tmpModel),
            success: response => {
                KTApp.unblockPage();
                toastr.clear();
                if (response.status == "success") {
                    that.state.tmpModel.Id = response.data;
                    that.state.model.Results.push(that.state.tmpModel);
                    that.state.model.TotalItemCount = that.state.model.Results.length;
                    that.setState(that.state);
                    $(this.NameInput.current).val("");
                    $(this.DesInput.current).val("");
                    that.state.tmpModel = {
                        Id: "",
                        Name: "",
                        Description: "",
                        Slug: "",
                        Count:0
                    }
                   
                    toastr["success"](response.message, success[that.context.Language]);
                } else {
                    toastr["error"](response.message, error[that.context.Language]);
                }
            },
            error: function (er) {
                KTApp.unblockPage();
                toastr.clear();
                toastr["error"](GLOBAL_ERROR_MESSAGE, error[that.context.Language]);
            }
        });
    }
    handleChangeDataRow(event) {
        const target = event.target;
        const value = target.type === "checkbox" ? target.checked : target.value;
        const name = target.name;
        const index = target.getAttribute("index");
        var newobj = { ...this.state.model.Results[index] };
        newobj[name] = isNaN(value) ? value : parseInt(value);
        var that = this;
        KTApp.blockPage();
        $.ajax({
            url: apiurl + "/updatecustomize",
            type: "POST",
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify({ model: newobj, name }),
            success: response => {
                KTApp.unblockPage();
                toastr.clear();
                if (response.status == "success") {
                    that.state.model.Results[index][name] = newobj[name];
                    that.setState(that.state);
                    toastr["success"](response.message, success[that.context.Language]);
                } else {
                    toastr["error"](response.message, error[that.context.Language]);
                }
            },
            error: function (er) {
                KTApp.unblockPage();
                toastr.clear();
                toastr["error"](GLOBAL_ERROR_MESSAGE, error[that.context.Language]);
            }
        });
    }
    componentWillMount() { }
    componentDidMount() {
        document.title = this.state.ex.Title;
        $("#kt_aside_menu .kt-menu__item").removeClass("kt-menu__item--active");
        $("#kt_aside_menu .kt-menu__item[data-id='tin-tuc']").addClass(
            "kt-menu__item--active"
        );
        $("#kt_aside_menu .kt-menu__item[data-id='tags']").addClass(
            "kt-menu__item--active"
        );
    }
    render() {
        return (
            <React.Fragment>
                {this.state && this.state.model && this.state.tmpModel && this.state.newModel  ? (
                    <React.Fragment>
                        <div className="kt-subheader kt-grid__item" id="kt_subheader">
                            <div className="kt-subheader__main">
                                <div className="kt-subheader__breadcrumbs">
                                    <Link
                                        to="/admin/dashboard"
                                        className="kt-subheader__breadcrumbs-home"
                                    >
                                        <i className="fa fa-home" />
                                    </Link>
                                    <span className="kt-subheader__breadcrumbs-separator" />
                                    <span className="kt-subheader__breadcrumbs-link">Tags</span>
                                    <span className="kt-subheader__breadcrumbs-separator" />
                                    <span className="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
                                        {this.state.ex.Title}

                                    </span>
                                </div>
                            </div>
                            <div className="kt-subheader__toolbar">
                                <div className="kt-subheader__wrapper">                                  
                                    <a
                                        href="javascript:;"
                                        onClick={e => this.handleDeleteDataRow()}
                                        className="btn btn-danger"
                                    >
                                        <i className="fa fa-trash-alt"></i> {remove[this.context.Language]}
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div
                            className="kt-content kt-grid__item kt-grid__item--fluid"
                            id="kt_content"
                        >
                            <div className="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                                <div className="kt-portlet kt-portlet--mobile">
                                    <div className="kt-portlet__head kt-portlet__head--lg">
                                        <div className="kt-portlet__head-label">
                                            <h3 className="kt-portlet__head-title">
                                                {this.state.ex.Title}
                                            </h3>
                                        </div>
                                    </div>

                                    <div className="kt-portlet__body">
                                        <div className="row">
                                            <div className="col-md-4 col-sm-12">
                                                <div className="form-group">
                                                    <label className="control-label">
                                                        Name
                                                        </label>
                                                    <input
                                                        ref={this.NameInput}
                                                        className="form-control"
                                                        placeholder="Name"
                                                        maxlength="70"
                                                        value={this.state.tmpModel.Name}
                                                        onChange={e => {
                                                            this.state.tmpModel.Name = e.target.value;
                                                            this.state.tmpModel.Slug = e.target.value.cleanUnicode();
                                                            this.setState(this.state);
                                                        }}
                                                    />
                                                </div>
                                                <div className="form-group">
                                                    <label className="control-label">
                                                        Description
                                                        </label>
                                                    <input
                                                        ref={this.DesInput}
                                                        className="form-control"
                                                        placeholder="Description"
                                                        maxlength="70"
                                                        value={this.state.tmpModel.Description}
                                                        onChange={e => {
                                                            this.state.tmpModel.Description = e.target.value;
                                                            this.setState(this.state);
                                                        }}
                                                    />
                                                </div>
                                                <div className="form-group">
                                                    <a
                                                        href="javascript:;"
                                                        onClick={e => this.handleSubmit()}
                                                        className="btn btn-success"
                                                    >
                                                        {save[this.context.Language]}
                                                    </a>
                                                </div>
                                            </div>
                                            <div className="col-md-8 col-sm-12">
                                                <div
                                                    id="kt_table_1_wrapper"
                                                    className="dataTables_wrapper dt-bootstrap4"
                                                >
                                                    <div className="row">
                                                        <div className="col-sm-12">
                                                            <table className="table table-striped- table-bordered table-hover table-checkable dataTable no-footer dtr-inline table-responsive">
                                                                <thead>
                                                                    <tr role="row">
                                                                        <th
                                                                            className="dt-right sorting_disabled"
                                                                            rowspan="1"
                                                                            colspan="1"
                                                                            style={{ width: "1%" }}
                                                                            aria-label="Record ID"
                                                                        >
                                                                            <label className="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                                                                <input
                                                                                    type="checkbox"
                                                                                    className="kt-group-checkable"
                                                                                    onChange={ev => {
                                                                                        this.state.model.Results = this.state.model.Results.map(
                                                                                            e => {
                                                                                                return {
                                                                                                    ...e,
                                                                                                    IsChecked: ev.target.checked
                                                                                                };
                                                                                            }
                                                                                        );
                                                                                        this.setState(this.state);
                                                                                    }}
                                                                                />
                                                                                <span></span>
                                                                            </label>
                                                                        </th>
                                                                        <th>Name</th>
                                                                        <th>Description</th>
                                                                        <th>Slug</th>
                                                                        <th>Count</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    {this.state.model.Results.map((c, index) => {
                                                                        return (
                                                                            <tr>
                                                                                <td className="dt-right" tabindex="0">
                                                                                    <label className="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                                                                        <input
                                                                                            type="checkbox"
                                                                                            className="kt-checkable"
                                                                                            value={c._id}
                                                                                            checked={c.IsChecked}
                                                                                            onChange={e => {
                                                                                                c.IsChecked = !c.IsChecked;
                                                                                                this.setState(this.state);
                                                                                            }}
                                                                                        />
                                                                                        <span></span>
                                                                                    </label>
                                                                                </td>
                                                                                <td>
                                                                                    <a href="javascript:;" onClick={e => {
                                                                                        this.state.newModel = this.state.model.Results.filter(t => { return t.Id == c.Id })[0];
                                                                                        this.setState(this.state);
                                                                                        $(this.popup.current).modal("show");
                                                                                        
                                                                                    }}>{c.Name}</a>
                                                                                </td>
                                                                                <td>
                                                                                    {c.Description}
                                                                                </td>
                                                                                <td>
                                                                                    {c.Slug}
                                                                                </td>
                                                                                <td>
                                                                                    {c.Count}
                                                                                </td>

                                                                            </tr>
                                                                        );
                                                                    })}
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <div className="row">
                                                        <div className="col-sm-12 col-md-5">
                                                            <div
                                                                className="dataTables_info"
                                                                id="kt_table_1_info"
                                                                role="status"
                                                                aria-live="polite"
                                                            >
                                                                {total[this.context.Language]} {this.state.model.TotalItemCount} {record[this.context.Language]}
                                                            </div>
                                                        </div>
                                                        <div className="col-sm-12 col-md-7 dataTables_pager">
                                                            <PagedList
                                                                currentpage={this.state.model.CurrentPage}
                                                                pagesize={this.state.model.PageSize}
                                                                totalitemcount={this.state.model.TotalItemCount}
                                                                totalpagecount={this.state.model.TotalPageCount}
                                                                ajaxcallback={this.toPage.bind(this)}
                                                            />
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div
                            className="modal fade"                 
                            ref={this.popup}
                            tabindex="-1"
                            role="dialog"
                            aria-labelledby="exampleModalLabel2"
                            aria-hidden="true"
                        >
                            <div className="modal-dialog modal-xs" role="document">
                                <div className="modal-content">
                                    <div className="modal-header">
                                        <h5 className="modal-title" id="exampleModalLabel2">
                                            Update Tag
                                        </h5>
                                        <button
                                            type="button"
                                            className="close"
                                            data-dismiss="modal"
                                            aria-label="Close"
                                        />
                                    </div>
                                    <div className="modal-body">
                                        <div className="form-group">
                                            <label className="control-label">
                                                Name
                                            </label>
                                            <input
                                                ref={this.NameInput}
                                                className="form-control"
                                                placeholder="Name"
                                                maxlength="70"
                                                value={this.state.newModel.Name}
                                                onChange={e => {
                                                    this.state.newModel.Name = e.target.value;
                                                    this.state.newModel.Slug = e.target.value.cleanUnicode();
                                                    this.setState(this.state);
                                                }}
                                            />
                                        </div>
                                        <div className="form-group">
                                            <label className="control-label">
                                                Description
                                             </label>
                                            <input
                                                ref={this.DesInput}
                                                className="form-control"
                                                placeholder="Description"
                                                maxlength="70"
                                                value={this.state.newModel.Description}
                                                onChange={e => {
                                                    this.state.newModel.Description = e.target.value;
                                                    this.setState(this.state);
                                                }}
                                            />
                                        </div>
                                        
                                    </div>
                                    <div className="modal-footer">
                                        <button
                                            type="button"
                                            className="btn btn-default"
                                            data-dismiss="modal"
                                        >
                                            {close[this.context.Language]}
                                        </button>
                                        <button
                                            type="button"
                                            className="btn btn-primary"
                                            onClick={() => {
                                                this.handleUpdate();
                                                $(this.popup.current).modal("hide");
                                                
                                            }}
                                        >
                                            Save
                                    </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </React.Fragment>
                ) : (
                        <div />
                    )}
            </React.Fragment>
        );
    }
}
Tags.contextType = AppContext;
export default Tags;
