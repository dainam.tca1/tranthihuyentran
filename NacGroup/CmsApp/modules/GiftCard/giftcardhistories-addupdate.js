import React, { Component } from "react";
import { Link } from "react-router-dom";
import { DelayInput } from "react-delay-input";
import GiftCardHistories from "./models/giftcardhistories-model";
import uploadPlugin from "../../plugins/upload-plugin";
import {
  GLOBAL_ERROR_MESSAGE,
  ERROR,
  SUCCESS,
  DATA_UPDATE_SUCCESSFULL,
  NOTI,
  DATA_NOT_RECOVER
} from "../../constants/message";
import imageModel from "./models/image-model";
//ckeditor

var apiurl = "/cms/api/giftcardhistories";

class GiftCardHistoriesAddUpdate extends Component {
  constructor(props) {
    super(props);
    var that = this;

    //action để nhận biết hiện đang add hay update
    var action = null;
      if (document.location.href.indexOf("/admin/giftcardhistories/add") >= 0) {
      action = "add";
    } else if (
        document.location.href.indexOf("/admin/giftcardhistories/view") >= 0
    ) {
      action = "update";
    }

    that.state = {
        model: new GiftCardHistories(),
      ex: {
        Title: null,
        Action: action
      }
    };
    that.setState(that.state);

    if (action === "update") {
      var id = this.props.match.params.id;
      KTApp.blockPage();
      $.ajax({
        url: apiurl + "/getupdate",
        type: "GET",
        dataType: "json",
        contentType: "application/json",
        data: { id },
        success: response => {
          KTApp.unblockPage();
          if (response.status == "success") {
            that.state.model = response.data;
            that.state.ex.Title = "Update Gift Card Template";
            document.title = that.state.ex.Title;
            that.setState(that.state);
          } else {
            toastr["error"](response.message, ERROR);
          }
        },
        error: function(er) {
          KTApp.unblockPage();
          toastr["error"](GLOBAL_ERROR_MESSAGE, ERROR);
        }
      });
    } else {
      that.state.ex.Title = "Add Gift Card Template";
      document.title = that.state.ex.Title;
      that.setState(that.state);
    }
  }

  submitForm() {
    var that = this;
    KTApp.blockPage();
    $.ajax({
      url: apiurl + "/" + that.state.ex.Action,
      type: "POST",
      dataType: "json",
      contentType: "application/json",
      data: JSON.stringify(that.state.model),
      success: response => {
        KTApp.unblockPage();
        if (response.status == "success") {
          swal.fire({
            title: SUCCESS,
            text: response.message,
            type: "success",
            onClose: () => {
              that.props.history.push("/admin/emptypage");
              that.props.history.replace({
                pathname: that.props.location.pathname
              });
            }
          });
        } else {
          toastr["error"](response.message, ERROR);
        }
      },
      error: function(er) {
        KTApp.unblockPage();
        toastr["error"](GLOBAL_ERROR_MESSAGE, ERROR);
      }
    });
  }
  handleDeleteImage() {
    var that = this;

    swal
      .fire({
        title: "Are you sure?",
        text: DATA_NOT_RECOVER,
        type: "warning",
        showCancelButton: true,
        confirmButtonText: "Yes, delete it",
        cancelButtonText: "No, cancel"
      })
      .then(function(result) {
        if (result.value) {
          var model = that.state.model.Templates;
          var result = model.filter(x => {
            return !x.IsChecked;
          });
          that.state.model.Templates = result;
          that.setState(that.state);
          return true;
        }
      });
  }
  reorder() {
    var that = this;
    that.state.model.Templates.sort(function(a, b) {
      return a.Sort > b.Sort ? 1 : b.Sort > a.Sort ? -1 : 0;
    });
    toastr["success"](DATA_UPDATE_SUCCESSFULL, SUCCESS);
    this.setState(that.state.model);
  }
  componentWillMount() {}

  componentDidMount() {
    //Active menu
    $("#kt_aside_menu .kt-menu__item").removeClass("kt-menu__item--active");
    $(
      "#kt_aside_menu .kt-menu__item[data-id='giftcard-list']"
    ).addClass("kt-menu__item--active");
  }

  componentWillUnmount() {}
  render() {
    return (
      <React.Fragment>
        {this.state && this.state.model ? (
          <React.Fragment>
            <div className="kt-subheader kt-grid__item" id="kt_subheader">
              <div className="kt-subheader__main">
                <div className="kt-subheader__breadcrumbs">
                  <Link
                    to="/admin/dashboard"
                    className="kt-subheader__breadcrumbs-home"
                  >
                    <i className="fa fa-home" />
                  </Link>
                  <span className="kt-subheader__breadcrumbs-separator" />
                  <Link
                    to="/admin/giftcardtemplate"
                    className="kt-subheader__breadcrumbs-link"
                  >
                    Gift Card Template List
                  </Link>
                  <span className="kt-subheader__breadcrumbs-separator" />
                  <span className="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
                    {this.state.ex.Title}
                  </span>
                </div>
              </div>
              <div className="kt-subheader__toolbar">
                <div className="kt-subheader__wrapper">
                  <Link
                    to="/admin/giftcardtemplate"
                    className="btn btn-secondary"
                  >
                    <i className="fa fa-chevron-left" /> Back
                  </Link>
                  <a
                    href="javascript:;"
                    className="btn btn-primary"
                    onClick={() => {
                      this.submitForm();
                    }}
                  >
                    <i className="fa fa-save"></i> Save
                  </a>
                </div>
              </div>
            </div>
            <div
              className="kt-content kt-grid__item kt-grid__item--fluid"
              id="kt_content"
            >
              <div className="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                <div className="row">
                  <div className="col-lg-12">
                    <div className="kt-portlet kt-portlet--mobile">
                      <div className="kt-portlet__head kt-portlet__head--lg">
                        <div className="kt-portlet__head-label">
                          <h3 className="kt-portlet__head-title">
                            Information
                          </h3>
                        </div>
                      </div>

                      <div className="kt-portlet__body">
                        <div className="form-group">
                          <label className="control-label">Name</label>
                          <input
                            className="form-control"
                            value={this.state.model.Name}
                            placeholder="Title"
                            onChange={e => {
                              this.state.model.Name = e.target.value;
                              if (this.state.ex.Action == "add") {
                                this.state.model.Slug = e.target.value.cleanUnicode();
                              }
                              this.setState(this.state);
                            }}
                          />
                        </div>
                        <div className="form-group">
                          <label className="control-label">Slug</label>
                          <input
                            className="form-control"
                            placeholder="Url slug"
                            value={this.state.model.Slug}
                            onChange={e => {
                              this.state.model.Slug = e.target.value;
                              this.setState(this.state);
                            }}
                          />
                        </div>
                        <div className="form-group">
                          <label className="control-label">Sort</label>
                          <input
                            type="number"
                            className="form-control"
                            placeholder="Sort"
                            value={this.state.model.Sort}
                            onChange={e => {
                              this.state.model.Sort = parseInt(e.target.value);
                              this.setState(this.state);
                            }}
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-12">
                    <div className="kt-portlet kt-portlet--mobile">
                      <div className="kt-portlet__head kt-portlet__head--lg">
                        <div className="kt-portlet__head-label">
                          <h3 className="kt-portlet__head-title">
                            Template List
                          </h3>
                        </div>
                      </div>

                      <div className="kt-portlet__body">
                        <div className="form-group">
                          <label>Select Image</label>
                          <div></div>
                          <div className="custom-file">
                            <input
                              type="file"
                              className="custom-file-input"
                              type="file"
                              multiple="multiple"
                              id="imageList"
                              onChange={e => {
                                var that = this;
                                uploadPlugin.UpdateImage(e, listfile => {
                                  listfile.forEach((img, i) => {
                                    var dorder =
                                      that.state.model.Templates.length > 0
                                        ? that.state.model.Templates[
                                            that.state.model.Templates.length -
                                              1
                                          ].Sort + 1
                                        : 0;
                                    var imageitem = new imageModel();
                                    imageitem.Src = img;
                                    imageitem.Sort = dorder;
                                    imageitem.IsChecked = false;
                                    that.state.model.Templates.push(imageitem);
                                  });
                                  that.setState(that.state);
                                });
                              }}
                            />
                            <label
                              className="custom-file-label"
                              for="imageList"
                            >
                              Choose file
                            </label>
                          </div>
                        </div>
                        <div className="form-group">
                          <a
                            href="javascript:;"
                            className="btn btn-danger"
                            onClick={e => this.handleDeleteImage()}
                          >
                            Delete Image
                          </a>
                        </div>
                        <div className="dataTables_wrapper dt-bootstrap4">
                          <table className="table table-striped- table-bordered table-hover table-checkable dataTable no-footer dtr-inline table-responsive">
                            <thead>
                              <tr>
                                <th className="stt">
                                  <label
                                    className="kt-checkbox kt-checkbox--single kt-checkbox--solid"
                                    onChange={ev => {
                                      this.state.model.Templates = this.state.model.Templates.map(
                                        e => {
                                          return {
                                            ...e,
                                            IsChecked: ev.target.checked
                                          };
                                        }
                                      );
                                      this.setState(this.state);
                                    }}
                                  >
                                    <input type="checkbox" />
                                    <span />
                                  </label>
                                </th>
                                <th className="sort">Sort</th>
                                <th className="img">Image</th>
                                <th>Alt</th>
                                <th>Url</th>
                              </tr>
                            </thead>
                            <tbody className="textlist">
                              {this.state.model.Templates
                                ? this.state.model.Templates.map(
                                    (img, index) => {
                                      return (
                                        <tr>
                                          <td>
                                            {" "}
                                            <label className="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                              <input
                                                type="checkbox"
                                                value={img.Src}
                                                checked={img.IsChecked}
                                                onChange={e => {
                                                  img.IsChecked = !img.IsChecked;
                                                  this.setState(this.state);
                                                }}
                                              />
                                              <span />
                                            </label>
                                          </td>
                                          <td>
                                            <DelayInput
                                              delayTimeout={1000}
                                              type="text"
                                              value={
                                                parseFloat(img.Sort) == ""
                                                  ? "0"
                                                  : parseFloat(img.Sort)
                                              }
                                              className="form-control"
                                              onChange={e => {
                                                var that = this;
                                                img.Sort = e.target.value;
                                                that.reorder();
                                              }}
                                            />
                                          </td>
                                          <td>
                                            <div
                                              className="form-group"
                                              style={{ position: "relative" }}
                                            >
                                              <input
                                                type="file"
                                                className="form-control"
                                                style={{
                                                  position: "absolute",
                                                  zIndex: 99,
                                                  top: 0,
                                                  width: "100%",
                                                  height: "100%",
                                                  opacity: 0
                                                }}
                                                onChange={e => {
                                                  var that = this;
                                                  uploadPlugin.UpdateImage(
                                                    e,
                                                    files => {
                                                      img.Src = files[0];
                                                      that.setState(that.state);
                                                    }
                                                  );
                                                }}
                                              />
                                              <img
                                                src={img.Src}
                                                style={{ height: "100px" }}
                                              />
                                            </div>
                                          </td>
                                          <td>
                                            <input
                                              type="text"
                                              class="form-control"
                                              placeholder="Image description"
                                              value={img.Alt}
                                              onChange={e => {
                                                var that = this;
                                                img.Alt = e.target.value;
                                                that.setState(that.state);
                                              }}
                                            />
                                          </td>
                                          <td>
                                            <input
                                              type="text"
                                              class="form-control"
                                              placeholder="Image Url"
                                              value={img.Url}
                                              onChange={e => {
                                                var that = this;
                                                img.Url = e.target.value;
                                                that.setState(that.state);
                                              }}
                                            />
                                          </td>
                                        </tr>
                                      );
                                    }
                                  )
                                : ""}
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </React.Fragment>
        ) : (
          <div />
        )}
      </React.Fragment>
    );
  }
}

export default GiftCardHistoriesAddUpdate;
