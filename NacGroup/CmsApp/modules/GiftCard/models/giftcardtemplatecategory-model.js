﻿class GiftCardTemplateCategoryModel {
    constructor() {
        this.Id = null;
        this.Name = null;
        this.Slug = null;
        this.Templates = [];
        this.Sort = 0;
    } 
}

export default GiftCardTemplateCategoryModel;