
//#region giftcard template
const giftcard_template = ["Gift Card Template List", "Danh sách biểu mẫu thẻ quà tặng"];
const template_name = ["Template Name", "Tên biểu mẫu"];
const avatar = ["Avatar", "Hình ảnh"];
const name = ["Name", "Tên"];
const add_giftcard = ["Add Gift Card Template", "Thêm biểu mẫu thẻ quà tặng"];
const update_giftcard = ["Update Gift Card Template", "Cập nhật biểu mẫu thẻ quà tặng"];
const information = ["Information", "Thông tin cơ bản"];
const template_list = ["Template List", "Danh sách biểu mẫu"];
const se_image = ["Select Image", "Chọn hình ảnh"]; 
const de_image = ["Delete Image", "Xóa hình ảnh"];
const giftcard_coupon_list = ["Gift Card Coupon List", "Danh sách phiếu quà tặng"];
const promotions = ["Promotions", "Chương trình khuyến mãi"];
const coupon_code = ["Coupon Code", "Mã giảm giá"];
const status_code = ["Status", "Trạng Thái"];
const inactive = ["Is InActive", "Chưa Kích Hoạt"];
const active = ["Active", "Đang Kích Hoạt"];
const isactive = ["Is Active", "Kích Hoạt"];
const records_one_page = ["Records / Page", "Kết quả / trang"];
const value = ["Value", "Giá Trị"];
const select = ["Select", "Chọn"];
const coupon_date = ["Coupon Date", "Ngày Áp Dụng"];
const start_date = ["Start Date", "Ngày bắt đầu"];
const expired_date = ["Expired Date", "Ngày hết hạn"];
const is_times_use = ["Times Use", "Số lần sử dụng"];
const one_time_use = ["One Time Use", "Một lần sử dụng"];
const add_coupon =["Add Coupon", "Thêm mã giảm giá"];
const update_coupon =["Update Coupon", "Cập nhật mã giảm giá"];
const giftcard_iformation = ["Gift card Coupon Information", "Thông tin phiếu quà tặng"];
const giftcard_coupon_code = ["Gift Card Coupon Code", "Mã Giảm Giá"];
const value_type = ["Value Type", "Loại giá trị"];
const amount_type = ["Amount Type", "Loại giảm giá"];
const percent = ["Percent (%)", "Phần Trăm (%)"];
const money = ["Money ($)", "Tiền (VNĐ)"];
const amount_less = ["Amount Less (-)", "Giảm tiền (-)"];
const amount_more = ["Amount More (+)", "Thêm tiền (+)"];
const amount_min = ["Amount min", "Số tiền tối thiểu"];
const amount_max = ["Amount Max", "Số tiền tối đa"];
const coupon_value = ["Coupon Value", "Giá trị phiếu giảm giá"];
//#endregion

module.exports = {
    //#region giftcard template
    giftcard_template,
    template_name,
    avatar,
    name,
    add_giftcard,
    update_giftcard,
    information,
    template_list,
    se_image,
    de_image,
    //~giftcard coupon~
    giftcard_coupon_list,
    promotions,
    coupon_code,
    status_code,
    inactive,
    active,
    isactive,
    records_one_page,
    value,
    start_date,
    expired_date,
    is_times_use,
    one_time_use,
    //add coupon
    select,
    coupon_date,
    update_coupon,
    add_coupon,
    giftcard_iformation,
    giftcard_coupon_code,
    value_type,
    amount_type,
    percent,
    money,
    amount_less,
    amount_more,
    amount_min,
    amount_max,
    coupon_value,
    //#endregion 
};
