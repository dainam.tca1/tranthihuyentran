﻿class GiftCardTemplateCategoryModel {
    constructor() {
        this.Id = null;
        this.FirstName = null;
        this.LastName = null;
        this.CustomerPhone = null;
        this.CustomerEmail = null;
        this.CustomerId = null;
        this.Message = null;
        this.TotalAmount = null;
        this.RecipientFirstName = null;
        this.RecipientLastName = null;
        this.RecipientPhone = null;
        this.RecipientEmail = null;
        this.GiftCardCode = null;
        this.GiftCardTemplateImage = null;
        this.GiftCardBarCode = null;
        this.CreatedDate = new Date();
        this.UpdatedDate = new Date();
        this.CardHolderName = null;
        this.PaymentInfomation = {
            AuthMessage : null,
            CardType : null,
            AuthCode : null,
            AcctNo : null,
            RecordId : null,
        };
        this.AddressInformation = {
            Street : null,
            City : null,
            State : null,
            ZipCode : null,
        };
    } 
}

export default GiftCardTemplateCategoryModel;