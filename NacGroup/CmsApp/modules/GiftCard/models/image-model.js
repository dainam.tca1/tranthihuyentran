﻿class ImageModel {
    constructor() {
        this.Src = null;
        this.Alt = null;
        this.Url = null;
        this.Sort = 0;
        this.IsChecked = false;
    }
}
module.exports = ImageModel;