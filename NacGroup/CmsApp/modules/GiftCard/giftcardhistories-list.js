import React, { Component } from "react";
import { DelayInput } from "react-delay-input";
import PagedList from "../../components/pagedlist";
import { Link } from "react-router-dom";
import FilterListPlugin from "../../plugins/filter-list-plugin";
import RemoveListPlugin from "../../plugins/remove-list-plugin";
import DateTimePicker from "../../components/datetimepicker";
import { GLOBAL_ERROR_MESSAGE, ERROR, SUCCESS } from "../../constants/message";
import moment from "moment";

const apiurl = "/cms/api/giftcardhistories";

class GiftCardHistoriesList extends Component {
  constructor() {
    super();

    //Khởi tạo state
    var that = this;
    that.state = {
      ex: {
        Title: "Gift Card Histories",
        Param: {
          //Danh sách tham số để filter
          pagesize: null,
          name: null,
          code: null,
          fromdate: null,
          todate: null
        }
      }
    };
    that.setState(that.state);

    //Cấu hình event trên component
    that.handleChangeFilter = that.handleChangeFilter.bind(that);
    that.handleChangeDataRow = that.handleChangeDataRow.bind(that);
    //Gọi hàm filter để đổ dữ liệu ra
    that.toPage(1);
  }

  toPage(index) {
    FilterListPlugin.filterdata(index, this, apiurl);
  }

  /**
   * Được gọi khi thay đổi các trường filter cập nhật tham số filter vào state
   * Filter dữ liệu theo tham số mới
   * @param {any} event -- Event
   */
  handleChangeFilter(event) {
    FilterListPlugin.handleChangeFilter(event, this);
    this.toPage(1);
  }

  /**
   * Xóa data đã được chọn
   */
  handleDeleteDataRow() {
    RemoveListPlugin.removeList(`${apiurl}/removelist`, this);
  }
  handleChangeDataRow(event) {
    const target = event.target;
    const value = target.type === "checkbox" ? target.checked : target.value;
    const name = target.name;
    const index = target.getAttribute("index");
    var newobj = { ...this.state.model.Results[index] };
    newobj[name] = isNaN(value) ? value : parseInt(value);
    var that = this;
    KTApp.blockPage();
    $.ajax({
      url: apiurl + "/updatecustomize",
      type: "POST",
      dataType: "json",
      contentType: "application/json",
      data: JSON.stringify({ model: newobj, name }),
      success: response => {
        KTApp.unblockPage();
        toastr.clear();
        if (response.status == "success") {
          that.state.model.Results[index][name] = newobj[name];
          that.setState(that.state);
          toastr["success"](response.message, SUCCESS);
        } else {
          toastr["error"](response.message, ERROR);
        }
      },
      error: function(er) {
        KTApp.unblockPage();
        toastr.clear();
        toastr["error"](GLOBAL_ERROR_MESSAGE, ERROR);
      }
    });
  }
  componentWillMount() {
    $("#cssloading").html(
      ` <link href="/adminstatics/global/plugins/flatpickr/css/flatpickr.min.css" rel="stylesheet" type="text/css" />`
    );
    $("#scriptloading").html(
      `<script src="/adminstatics/global/plugins/flatpickr/js/flatpickr.min.js" type="text/javascript"></script>`
    );
  }

  componentWillUnmount() {
    $("#cssloading").html("");
    $("#scriptloading").html("");
  }
  componentDidMount() {
    document.title = this.state.ex.Title;

    $("#kt_aside_menu .kt-menu__item").removeClass("kt-menu__item--active");
    $("#kt_aside_menu .kt-menu__item[data-id='giftcard-list']").addClass(
      "kt-menu__item--active"
    );
  }

  render() {
    return (
      <React.Fragment>
        {this.state && this.state.model ? (
          <React.Fragment>
            <div className="kt-subheader kt-grid__item" id="kt_subheader">
              <div className="kt-subheader__main">
                <div className="kt-subheader__breadcrumbs">
                  <Link
                    to="/admin/dashboard"
                    className="kt-subheader__breadcrumbs-home"
                  >
                    <i className="fa fa-home" />
                  </Link>
                  <span className="kt-subheader__breadcrumbs-separator" />
                  <span className="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
                    {this.state.ex.Title}
                  </span>
                </div>
              </div>
              <div className="kt-subheader__toolbar">
                <div className="kt-subheader__wrapper">
                  <a
                    href="javascript:;"
                    onClick={e => this.handleDeleteDataRow()}
                    className="btn btn-danger"
                  >
                    <i className="fa fa-trash-alt" /> Remove
                  </a>
                </div>
              </div>
            </div>
            <div
              className="kt-content kt-grid__item kt-grid__item--fluid"
              id="kt_content"
            >
              <div className="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                <div className="kt-portlet kt-portlet--mobile">
                  <div className="kt-portlet__head kt-portlet__head--lg">
                    <div className="kt-portlet__head-label">
                      <h3 className="kt-portlet__head-title">
                        {this.state.ex.Title}
                      </h3>
                    </div>
                  </div>

                  <div className="kt-portlet__body">
                    <form className="kt-form kt-form--fit">
                      <div className="row kt-margin-b-20">
                        <div className="col-lg-3">
                          <label>GiftCard Code:</label>
                          <DelayInput
                            delayTimeout={1000}
                            className="form-control kt-input"
                            placeholder="GiftCard Code"
                            value={this.state.ex.Param.code}
                            name="code"
                            onChange={this.handleChangeFilter}
                          />
                        </div>
                        <div className="col-lg-3 kt-margin-b-10">
                          <label>From Date:</label>
                          <DateTimePicker
                            value={this.state.ex.Param.fromdate}
                            options={{
                              enableTime: false,
                              dateFormat: "m/d/Y",
                              allowInput: true
                            }}
                            onChange={(selectedDates, dateStr, instance) => {
                              this.state.ex.Param.fromdate = dateStr;
                              this.setState(this.state);
                              this.toPage(1);
                            }}
                            placeholder="From date"
                            datefrom="fromdate"
                          />
                        </div>
                        <div className="col-lg-3 kt-margin-b-10">
                          <label>To Date:</label>
                          <DateTimePicker
                            value={this.state.ex.Param.todate}
                            options={{
                              enableTime: false,
                              dateFormat: "m/d/Y",
                              allowInput: true
                            }}
                            onChange={(selectedDates, dateStr, instance) => {
                              this.state.ex.Param.todate = dateStr;
                              this.setState(this.state);
                              this.toPage(1);
                            }}
                            placeholder="To date"
                            datefrom="todate"
                          />
                        </div>
                        <div className="col-lg-3">
                          <label>Records Per Page:</label>
                          <select
                            className="form-control kt-input"
                            data-col-index="2"
                            value={this.state.ex.Param.pagesize}
                            name="pagesize"
                            onChange={this.handleChangeFilter}
                          >
                            <option value="10">10 Records / Page</option>
                            <option value="20">20 Records / Page</option>
                            <option value="50">50 Records / Page</option>
                            <option value="100">100 Records / Page</option>
                          </select>
                        </div>
                      </div>
                    </form>

                    <div className="kt-separator kt-separator--border-dashed" />

                    <div
                      id="kt_table_1_wrapper"
                      className="dataTables_wrapper dt-bootstrap4"
                    >
                      <div className="row">
                        <div className="col-sm-12">
                          <table className="table table-striped- table-bordered table-hover table-checkable dataTable no-footer dtr-inline table-responsive">
                            <thead>
                              <tr role="row">
                                <th
                                  className="dt-right sorting_disabled"
                                  rowspan="1"
                                  colspan="1"
                                  style={{ width: "1%" }}
                                  aria-label="Record ID"
                                >
                                  <label className="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                    <input
                                      type="checkbox"
                                      className="kt-group-checkable"
                                      onChange={ev => {
                                        this.state.model.Results = this.state.model.Results.map(
                                          e => {
                                            return {
                                              ...e,
                                              IsChecked: ev.target.checked
                                            };
                                          }
                                        );
                                        this.setState(this.state);
                                      }}
                                    />
                                    <span />
                                  </label>
                                </th>
                                <th>GiftCard Code</th>
                                <th>Customer Name</th>
                                <th>Created Date</th>
                                <th>GiftCardTemplateImage</th>
                                <th>Payment Status</th>
                              </tr>
                            </thead>
                            <tbody>
                              {this.state.model.Results.map((c, index) => {
                                return (
                                  <React.Fragment>
                                    <tr>
                                      <td className="dt-right" tabindex="0">
                                        <label className="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                          <input
                                            type="checkbox"
                                            className="kt-checkable"
                                            value={c.Id}
                                            checked={c.IsChecked}
                                            onChange={e => {
                                              c.IsChecked = !c.IsChecked;
                                              this.setState(this.state);
                                            }}
                                          />
                                          <span />
                                        </label>
                                      </td>
                                      <td>
                                        <a
                                          href="javascript:;"
                                          onClick={e => {
                                            $(`#detail-${c.Id}`).toggle();
                                            $(e.target)
                                              .parent()
                                              .find("i")
                                              .toggleClass("rotate");
                                          }}
                                        >
                                          <i className="fa fa-chevron-down" />{" "}
                                          {c.GiftCardCode}
                                        </a>
                                      </td>
                                      <td>
                                        {c.FirstName} {c.LastName}
                                      </td>
                                      <td>
                                        {moment
                                          .utc(c.CreatedDate)
                                          .format("MM/DD/YYYY hh:mm A")}
                                      </td>
                                      <td>
                                        <img
                                          src={c.GiftCardBarCode}
                                          style={{ width: "200px" }}
                                        />
                                      </td>
                                      <td>
                                        {c.PaymentStatus == 0 ? (
                                          <label className="kt-badge kt-badge--inline kt-badge--light">
                                            Spending
                                          </label>
                                        ) : (
                                          <label className="kt-badge kt-badge--inline kt-badge--success">
                                            Paid
                                          </label>
                                        )}
                                      </td>
                                    </tr>
                                    <tr
                                      className="detail collapse show"
                                      id={`detail-${c.Id}`}
                                      style={{ display: "none" }}
                                    >
                                      <td colSpan="7">
                                        <div className="row">
                                          <div className="col-lg-6">
                                            <div class="kt-portlet kt-portlet--height-fluid">
                                              <div class="kt-portlet__head kt-portlet__head--noborder">
                                                <div class="kt-portlet__head-label">
                                                  <h3 class="kt-portlet__head-title" />
                                                </div>
                                              </div>
                                              <div class="kt-portlet__body">
                                                <div class="kt-widget kt-widget--user-profile-2">
                                                  <div class="kt-widget__head">
                                                    <div class="kt-widget__media">
                                                      <div class="kt-widget__pic kt-widget__pic--success kt-font-success kt-font-boldest">
                                                        {c.FirstName[0]}
                                                      </div>
                                                    </div>
                                                    <div class="kt-widget__info">
                                                      <a
                                                        href="#"
                                                        class="kt-widget__username"
                                                      >
                                                        {c.FirstName}{" "}
                                                        {c.LastName}
                                                      </a>
                                                      <span class="kt-widget__desc">
                                                        {c.GiftCardCode}
                                                      </span>
                                                    </div>
                                                  </div>
                                                  <div class="kt-widget__body">
                                                    <div class="kt-widget__section">
                                                      {c.Message}
                                                    </div>
                                                    <div class="kt-widget__item">
                                                      <div class="kt-widget__contact">
                                                        <span class="kt-widget__label">
                                                          Customer Email:
                                                        </span>
                                                        <a
                                                          href="#"
                                                          class="kt-widget__data"
                                                        >
                                                          {c.CustomerEmail}
                                                        </a>
                                                      </div>
                                                      <div class="kt-widget__contact">
                                                        <span class="kt-widget__label">
                                                          Customer Phone:
                                                        </span>
                                                        <a
                                                          href="#"
                                                          class="kt-widget__data"
                                                        >
                                                          {c.CustomerPhone}
                                                        </a>
                                                      </div>
                                                      <div class="kt-widget__contact">
                                                        <span class="kt-widget__label">
                                                          Created Date:
                                                        </span>
                                                        <a
                                                          href="#"
                                                          class="kt-widget__data"
                                                        >
                                                          {moment
                                                            .utc(c.CreatedDate)
                                                            .format(
                                                              "MM/DD/YYYY hh:mm:ss A"
                                                            )}
                                                        </a>
                                                      </div>
                                                      <div class="kt-widget__contact">
                                                        <span class="kt-widget__label">
                                                          Recipient Name:
                                                        </span>
                                                        <a
                                                          href="#"
                                                          class="kt-widget__data"
                                                        >
                                                          {c.RecipientFirstName}{" "}
                                                          {c.RecipientLastName}
                                                        </a>
                                                      </div>
                                                      <div class="kt-widget__contact">
                                                        <span class="kt-widget__label">
                                                          Recipient Phone:
                                                        </span>
                                                        <a
                                                          href="#"
                                                          class="kt-widget__data"
                                                        >
                                                          {c.RecipientPhone}
                                                        </a>
                                                      </div>

                                                      <div class="kt-widget__contact">
                                                        <span class="kt-widget__label">
                                                          Recipient Email:
                                                        </span>
                                                        <a
                                                          href="#"
                                                          class="kt-widget__data"
                                                        >
                                                          {c.RecipientEmail}
                                                        </a>
                                                      </div>
                                                      <div class="kt-widget__contact">
                                                        <span class="kt-widget__label">
                                                          Total Amount:
                                                        </span>
                                                        <a
                                                          href="#"
                                                          class="kt-widget__data"
                                                        >
                                                          ${c.TotalAmount}
                                                        </a>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </td>
                                    </tr>
                                  </React.Fragment>
                                );
                              })}
                            </tbody>
                          </table>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-sm-12 col-md-5">
                          <div
                            className="dataTables_info"
                            id="kt_table_1_info"
                            role="status"
                            aria-live="polite"
                          >
                            Total {this.state.model.TotalItemCount} records
                          </div>
                        </div>
                        <div className="col-sm-12 col-md-7 dataTables_pager">
                          <PagedList
                            currentpage={this.state.model.CurrentPage}
                            pagesize={this.state.model.PageSize}
                            totalitemcount={this.state.model.TotalItemCount}
                            totalpagecount={this.state.model.TotalPageCount}
                            ajaxcallback={this.toPage.bind(this)}
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </React.Fragment>
        ) : (
          <div />
        )}
      </React.Fragment>
    );
  }
}

export default GiftCardHistoriesList;
