import React, { Component } from "react";
import { DelayInput } from "react-delay-input";
import PagedList from "../../components/pagedlist";
import { Link } from "react-router-dom";
import FilterListPlugin from "../../plugins/filter-list-plugin";
import RemoveListPlugin from "../../plugins/remove-list-plugin";
import moment from "moment";
import {
    globalErrorMessage,
    error,
    success,
    add,
    remove,
    total,
    record,
    page,
    cDate,
    lDate,
    recordPerPage
} from "../../constants/message";
import {
    giftcard_coupon_list,
    coupon_code,
    promotions,
    status_code,
    inactive,
    active,
    records_one_page,
    value,
    start_date,
    expired_date,
    is_times_use,
    one_time_use,
} from "./models/giftcardstaticmessage";
import AppContext from "../../components/app-context";
const apiurl = "/cms/api/giftcardcoupon";

class GiftCardCouponList extends Component {
  constructor(props,context) {
      super(props, context);

    //Khởi tạo state
    var that = this;
    that.state = {
      ex: {
        Title: giftcard_coupon_list[this.context.Language],
        Param: {
          //Danh sách tham số để filter
          pagesize: null,
          code: null,
          isactive: null
        }
      }
    };
    that.setState(that.state);

    //Cấu hình event trên component
    that.handleChangeFilter = that.handleChangeFilter.bind(that);

    //Gọi hàm filter để đổ dữ liệu ra
    that.toPage(1);
  }

  componentWillMount() {}
  componentDidMount() {
    document.title = this.state.ex.Title;

    $("#kt_aside_menu .kt-menu__item").removeClass("kt-menu__item--active");
    $("#kt_aside_menu .kt-menu__item[data-id='promotion']").addClass(
      "kt-menu__item--active"
    );
    $("#kt_aside_menu .kt-menu__item[data-id='giftcard-coupon-list']").addClass(
      "kt-menu__item--active"
    );
  }

  toPage(index) {
    FilterListPlugin.filterdata(index, this, apiurl);
  }

  /**
   * Được gọi khi thay đổi các trường filter cập nhật tham số filter vào state
   * Filter dữ liệu theo tham số mới
   * @param {any} event -- Event
   */
  handleChangeFilter(event) {
    FilterListPlugin.handleChangeFilter(event, this);
    this.toPage(1);
  }

  /**
   * Xóa data đã được chọn
   */
  handleDeleteDataRow() {
    RemoveListPlugin.removeList(`${apiurl}/removelist`, this);
  }

  render() {
    return (
      <React.Fragment>
        {this.state && this.state.model ? (
          <React.Fragment>
            <div className="kt-subheader kt-grid__item" id="kt_subheader">
              <div className="kt-subheader__main">
                <div className="kt-subheader__breadcrumbs">
                  <Link
                    to="/admin/dashboard"
                    className="kt-subheader__breadcrumbs-home"
                  >
                    <i className="fa fa-home" />
                  </Link>
                  <span className="kt-subheader__breadcrumbs-separator" />
                  <span className="kt-subheader__breadcrumbs-link">
                    {promotions[this.context.Language]}
                  </span>
                  <span className="kt-subheader__breadcrumbs-separator" />
                  <span className="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
                    {this.state.ex.Title}
                  </span>
                </div>
              </div>
              <div className="kt-subheader__toolbar">
                <div className="kt-subheader__wrapper">
                  <Link
                    to="/admin/giftcardcoupon/add"
                    className="btn btn-primary"
                  >
                    <i className="fa fa-plus"></i> {add[this.context.Language]}
                  </Link>
                  <a
                    href="javascript:;"
                    onClick={e => this.handleDeleteDataRow()}
                    className="btn btn-danger"
                  >
                    <i className="fa fa-trash-alt"></i> {remove[this.context.Language]}
                  </a>
                </div>
              </div>
            </div>
            <div
              className="kt-content kt-grid__item kt-grid__item--fluid"
              id="kt_content"
            >
              <div className="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                <div className="kt-portlet kt-portlet--mobile">
                  <div className="kt-portlet__head kt-portlet__head--lg">
                    <div className="kt-portlet__head-label">
                      <h3 className="kt-portlet__head-title">
                        {this.state.ex.Title}
                      </h3>
                    </div>
                  </div>

                  <div className="kt-portlet__body">
                    <form className="kt-form kt-form--fit">
                      <div className="row kt-margin-b-20">
                        <div className="col-lg-3">
                          <label>{coupon_code[this.context.Language]}:</label>
                          <DelayInput
                            delayTimeout={1000}
                            className="form-control kt-input"
                            placeholder={coupon_code[this.context.Language]}
                            value={this.state.ex.Param.code}
                            name="code"
                            onChange={this.handleChangeFilter}
                          />
                        </div>
                        <div className="col-lg-3">
                          <label>{status_code[this.context.Language]}:</label>
                          <select
                            className="form-control kt-input"
                            value={this.state.ex.Param.isactive}
                            name="isactive"
                            onChange={this.handleChangeFilter}
                          >
                            <option value={null}>---{status_code[this.context.Language]}---</option>
                            <option value={false}>{inactive[this.context.Language]}</option>
                            <option value={true}>{active[this.context.Language]}</option>
                          </select>
                        </div>
                        <div className="col-lg-3">
                          <label>{recordPerPage[this.context.Language]}</label>
                          <select
                            className="form-control kt-input"
                            data-col-index="2"
                            value={this.state.ex.Param.pagesize}
                            name="pagesize"
                            onChange={this.handleChangeFilter}
                          >
                            <option value="10">10 {records_one_page[this.context.Language]}</option>
                            <option value="20">20 {records_one_page[this.context.Language]}</option>
                            <option value="50">50 {records_one_page[this.context.Language]}</option>
                            <option value="100">100 {records_one_page[this.context.Language]}</option>
                          </select>
                        </div>
                      </div>
                    </form>

                    <div className="kt-separator kt-separator--border-dashed"></div>

                    <div
                      id="kt_table_1_wrapper"
                      className="dataTables_wrapper dt-bootstrap4"
                    >
                      <div className="row">
                        <div className="col-sm-12">
                          <table className="table table-striped- table-bordered table-hover table-checkable dataTable no-footer dtr-inline table-responsive">
                            <thead>
                              <tr role="row">
                                <th
                                  className="dt-right sorting_disabled"
                                  rowspan="1"
                                  colspan="1"
                                  style={{ width: "1%" }}
                                  aria-label="Record ID"
                                >
                                  <label className="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                    <input
                                      type="checkbox"
                                      className="kt-group-checkable"
                                      onChange={ev => {
                                        this.state.model.Results = this.state.model.Results.map(
                                          e => {
                                            return {
                                              ...e,
                                              IsChecked: ev.target.checked
                                            };
                                          }
                                        );
                                        this.setState(this.state);
                                      }}
                                    />
                                    <span></span>
                                  </label>
                                </th>
                                <th>{coupon_code[this.context.Language]}</th>
                                <th>{value[this.context.Language]}</th>
                                <th>{start_date[this.context.Language]}</th>
                                <th>{expired_date[this.context.Language]}</th>
                                <th>{is_times_use[this.context.Language]}</th>
                                <th>{status_code[this.context.Language]}</th>
                              </tr>
                            </thead>
                            <tbody>
                              {this.state.model.Results.map((c, index) => {
                                return (
                                  <tr>
                                    <td className="dt-right" tabindex="0">
                                      <label className="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                        <input
                                          type="checkbox"
                                          className="kt-checkable"
                                          value={c._id}
                                          checked={c.IsChecked}
                                          onChange={e => {
                                            c.IsChecked = !c.IsChecked;
                                            this.setState(this.state);
                                          }}
                                        />
                                        <span></span>
                                      </label>
                                    </td>
                                    <td>
                                      <Link
                                        to={`/admin/giftcardcoupon/update/${c.Id}`}
                                      >
                                        {c.CouponCode}
                                      </Link>
                                    </td>
                                    <td>
                                      Discount {c.ValueType === 1 ? "$" : ""}{" "}
                                      {c.Value} {c.ValueType === 0 ? "%" : ""}
                                    </td>
                                    <td>
                                      {moment
                                        .utc(c.StartDate)
                                        .format("MM/DD/YYYY hh:mm A")}
                                    </td>
                                    <td>
                                      {moment
                                        .utc(c.ExpiredDate)
                                        .format("MM/DD/YYYY hh:mm A")}
                                    </td>
                                    <td>
                                      {c.IsOneTimeUse && (
                                        <label className="kt-badge kt-badge--inline kt-badge--warning">
                                          {one_time_use[this.context.Language]}
                                        </label>
                                      )}
                                    </td>
                                    <td>
                                      {c.IsActive ? (
                                        <label className="kt-badge kt-badge--inline kt-badge--success">
                                          {active[this.context.Language]}
                                        </label>
                                      ) : (
                                        <label className="kt-badge kt-badge--inline kt-badge--danger">
                                          {inactive[this.context.Language]}
                                        </label>
                                      )}
                                    </td>
                                  </tr>
                                );
                              })}
                            </tbody>
                          </table>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-sm-12 col-md-5">
                          <div
                            className="dataTables_info"
                            id="kt_table_1_info"
                            role="status"
                            aria-live="polite"
                          >
                            {total[this.context.Language]} {this.state.model.TotalItemCount} {record[this.context.Language]}
                          </div>
                        </div>
                        <div className="col-sm-12 col-md-7 dataTables_pager">
                          <PagedList
                            currentpage={this.state.model.CurrentPage}
                            pagesize={this.state.model.PageSize}
                            totalitemcount={this.state.model.TotalItemCount}
                            totalpagecount={this.state.model.TotalPageCount}
                            ajaxcallback={this.toPage.bind(this)}
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </React.Fragment>
        ) : (
          <div />
        )}
      </React.Fragment>
    );
  }
}
GiftCardCouponList.contextType = AppContext;
export default GiftCardCouponList;
