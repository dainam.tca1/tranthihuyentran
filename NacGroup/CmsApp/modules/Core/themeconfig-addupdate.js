import React, { Component } from "react";
import { Link } from "react-router-dom";
import {
    globalErrorMessage,
    error,
    success,
    back,
    add,
    remove,
    total,
    record,
    page,
    cDate,
    lDate,
    recordPerPage,
    are_you_sure,
    yes_de,
    no_ca,
    close,
    sort,
    save
} from "../../constants/message";
import {
    theme_config,
    theme,
    add_theme,
    update_theme,
    theme_name,
    name,
    key
} from "./models/corestaticmessage";
import AppContext from "../../components/app-context";
import { ThemeConfigModel } from "./models/thememodel";

import ThemeConfigItem from "./theme-config-item";
var apiurl = "/cms/api/themeconfig";

class ThemeConfigAddUpdate extends Component {
  constructor(props,context) {
    super(props,context);
    var that = this;

    //action để nhận biết hiện đang add hay update
    var action = null;
    if (document.location.href.indexOf("/admin/themeconfig/add") >= 0) {
      action = "add";
    } else if (
      document.location.href.indexOf("/admin/themeconfig/update") >= 0
    ) {
      action = "update";
    }

    that.state = {
      model: new ThemeConfigModel(),
      ex: {
        Title: null,
        Action: action
      }
    };
    that.setState(that.state);

    if (action === "update") {
      var id = this.props.match.params.id;
      KTApp.blockPage();
      $.ajax({
        url: "/cms/api/themeconfig/getupdate",
        type: "GET",
        dataType: "json",
        contentType: "application/json",
        data: { id },
        success: response => {
          KTApp.unblockPage();
          if (response.status === "success") {
              that.state.model = response.data;
              that.state.ex.Title = update_theme[this.context.Language];
            document.title = that.state.ex.Title;
            that.setState(that.state);
          } else {
              toastr["error"](response.message, error[this.context.Language]);
          }
        },
        error: function(er) {
          KTApp.unblockPage();
            toastr["error"](globalErrorMessage[this.context.Language], error[this.context.Language]);
        }
      });
    } else {
        that.state.ex.Title = add_theme[this.context.Language];
      document.title = that.state.ex.Title;
      that.setState(that.state);
    }
  }

  componentWillMount() {
    //Load script ckeditor lên
    $("#scriptloading").html(
      `<script src="/adminstatics/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>`
    );
  }

  componentDidMount() {
    //Active menu
    $("#kt_aside_menu .kt-menu__item").removeClass("kt-menu__item--active");
    $("#kt_aside_menu .kt-menu__item[data-id='theme']").addClass(
      "kt-menu__item--active"
    );
    $("#kt_aside_menu .kt-menu__item[data-id='theme-config']").addClass(
      "kt-menu__item--active"
    );
  }

  componentWillUnmount() {
    $("#scriptloading").html("");
  }

  submitForm() {
    var that = this;
    KTApp.blockPage();
    $.ajax({
      url: apiurl + "/" + that.state.ex.Action,
      type: "POST",
      dataType: "json",
      contentType: "application/json",
      data: JSON.stringify(that.state.model),
      success: response => {
        KTApp.unblockPage();
        if (response.status === "success") {
          swal.fire({
            title: success[this.context.Language],
            text: response.message,
            type: "success",
            onClose: () => {
              that.props.history.push("/admin/emptypage");
              that.props.history.replace({
                pathname: that.props.location.pathname,
                search: that.props.location.search
              });
            }
          });
        } else {
            toastr["error"](response.message, error[this.context.Language]);
        }
      },
      error: function(er) {
        KTApp.unblockPage();
          toastr["error"](globalErrorMessage[this.context.Language], error[this.context.Language]);
      }
    });
  }

  render() {
    return (
      <React.Fragment>
        {this.state && this.state.model ? (
          <React.Fragment>
            <div className="kt-subheader kt-grid__item" id="kt_subheader">
              <div className="kt-subheader__main">
                <div className="kt-subheader__breadcrumbs">
                  <Link
                    to="/admin/dashboard"
                    className="kt-subheader__breadcrumbs-home"
                  >
                    <i className="fa fa-home" />
                  </Link>
                  <span className="kt-subheader__breadcrumbs-separator" />
                  <span className="kt-subheader__breadcrumbs-link">{theme[this.context.Language]}</span>
                  <span className="kt-subheader__breadcrumbs-separator" />
                  <Link
                    to="/admin/themeconfig"
                    className="kt-subheader__breadcrumbs-link"
                  >
                   {theme_config[this.context.Language]}
                  </Link>
                  <span className="kt-subheader__breadcrumbs-separator" />
                  <span className="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
                    {this.state.ex.Title}
                  </span>
                </div>
              </div>
              <div className="kt-subheader__toolbar">
                <div className="kt-subheader__wrapper">
                  <Link to="/admin/themeconfig" className="btn btn-secondary">
                    <i className="fa fa-chevron-left" />  {back[this.context.Language]}
                  </Link>
                  <a
                    href="javascript:;"
                    className="btn btn-primary"
                    onClick={() => {
                      this.submitForm();
                    }}
                  >
                    <i className="fa fa-save"></i> {save[this.context.Language]}
                  </a>
                </div>
              </div>
            </div>
            <div
              className="kt-content kt-grid__item kt-grid__item--fluid"
              id="kt_content"
            >
              <div className="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                <div className="row">
                  <div className="col-lg-12">
                    <ThemeConfigItem
                      model={this.state.model}
                      onChange={newmodel => {
                        this.state.model = newmodel;
                        this.setState(this.state);
                      }}
                      isShowKeyInput={this.state.ex.Action === "add"}
                    />
                  </div>
                </div>
              </div>
            </div>
          </React.Fragment>
        ) : (
          <div />
        )}
      </React.Fragment>
    );
  }
}
ThemeConfigAddUpdate.contextType = AppContext;
export default ThemeConfigAddUpdate;
