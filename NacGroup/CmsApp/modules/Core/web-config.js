import React, { Component } from "react";
import { Link } from "react-router-dom";
import Select from "react-select";
import {
    globalErrorMessage,
    error,
    success,
    back,
    add,
    remove,
    total,
    record,
    page,
    cDate,
    lDate,
    recordPerPage,
    are_you_sure,
    yes_de,
    no_ca,
    close,
    sort,
    save
} from "../../constants/message";
import {
    website_config1,
    web_info,
    seo_config,
    website_config2,
    time_zone,
    home_meta_title,
    home_meta_des,
    home_meta_key,
    datetime_format,
    date_format,
    cus_css,
    cus_js,
    gcapt_pub,
    gcapt_pri,
    web_name,
    web_url,
    web_email,
    web_email_noti,
    web_phone,
    web_google,
    web_address,
    web_facebook,
    web_ins,
    web_youtube,
    web_twitter,
    web_pin,
    web_lis,
} from "./models/corestaticmessage";
import AppContext from "../../components/app-context";
const apiurl = "/cms/api/websiteconfig";

class WebsiteConfig extends Component {
  constructor(props,context) {
    super(props,context);
    var that = this;
    this.state = {
      model: null,
      ex: {
          Title: website_config1[this.context.Language],
        TimeZoneList: null
      }
    };
    KTApp.blockPage();

    $.ajax({
      url: apiurl + "/gettimezonelist",
      type: "GET",
      dataType: "json",
      contentType: "application/json",
      success: response => {
        KTApp.unblockPage();
        toastr.clear();
        if (response.status === "success") {
          that.state.ex.TimeZoneList = response.data.map(e => {
            return { value: e.Id, label: e.DisplayName + " (" + e.Id + ")" };
          });
          that.setState(that.state);
        } else {
          toastr["error"](response.message, error[this.context.Language]);
        }
      },
      error: function(er) {
        KTApp.unblockPage();
        toastr.clear();
        toastr["error"](globalErrorMessage[this.context.Language], error[this.context.Language]);
      }
    });

    $.ajax({
      url: apiurl,
      type: "GET",
      dataType: "json",
      contentType: "application/json",
      success: response => {
        KTApp.unblockPage();
        toastr.clear();
        if (response.status === "success") {
          that.state.model = response.data;
          that.setState(that.state);
        } else {
            toastr["error"](response.message, error[this.context.Language]);
        }
      },
      error: function(er) {
        KTApp.unblockPage();
        toastr.clear();
          toastr["error"](globalErrorMessage[this.context.Language], error[this.context.Language]);
      }
    });
  }
  componentWillMount() {}
  componentDidMount() {
    document.title = this.state.ex.Title;
    $("#kt_aside_menu .kt-menu__item").removeClass("kt-menu__item--active");
    $("#kt_aside_menu .kt-menu__item[data-id='cau-hinh-website']").addClass(
      "kt-menu__item--active"
    );
  }
  submitForm() {
    var that = this;
    KTApp.blockPage();

    $.ajax({
      url: apiurl + "/update",
      type: "POST",
      dataType: "json",
      contentType: "application/json",
      data: JSON.stringify(that.state.model),
      success: response => {
        KTApp.unblockPage();
        if (response.status == "success") {
          swal.fire({
            title: success[this.context.Language],
            text: response.message,
            type: "success",
            onClose: () => {
              that.props.history.push("/admin/emptypage");
              that.props.history.replace({
                pathname: that.props.location.pathname,
                search: that.props.location.search
              });
            }
          });
        } else {
          console.log(response.data);
            toastr["error"](response.message, error[this.context.Language]);
        }
      },
      error: function(er) {
        KTApp.unblockPage();
          toastr["error"](globalErrorMessage[this.context.Language], error[this.context.Language]);
      }
    });
  }

  render() {
    return (
      <React.Fragment>
        {this.state && this.state.model && this.state.ex.TimeZoneList ? (
          <React.Fragment>
            <div className="kt-subheader kt-grid__item" id="kt_subheader">
              <div className="kt-subheader__main">
                <div className="kt-subheader__breadcrumbs">
                  <Link
                    to="/admin/dashboard"
                    className="kt-subheader__breadcrumbs-home"
                  >
                    <i className="fa fa-home" />
                  </Link>

                  <span className="kt-subheader__breadcrumbs-separator" />
                  <span className="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
                    {this.state.ex.Title}
                  </span>
                </div>
              </div>
              <div className="kt-subheader__toolbar">
                <div className="kt-subheader__wrapper">
                  <Link to="/admin/staticpage" className="btn btn-secondary">
                                    <i className="fa fa-chevron-left" /> {back[this.context.Language]}
                  </Link>
                  <a
                    href="javascript:;"
                    className="btn btn-primary"
                    onClick={() => {
                      this.submitForm();
                    }}
                  >
                                    <i className="fa fa-save"></i> {save[this.context.Language]}
                  </a>
                </div>
              </div>
            </div>
            <div
              className="kt-content kt-grid__item kt-grid__item--fluid"
              id="kt_content"
            >
              <div className="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                <div className="row">
                  <div className="col-lg-6">
                    <div className="kt-portlet kt-portlet--mobile">
                      <div className="kt-portlet__head kt-portlet__head--lg">
                        <div className="kt-portlet__head-label">
                          <h3 className="kt-portlet__head-title">{seo_config[this.context.Language]}</h3>
                        </div>
                      </div>

                      <div className="kt-portlet__body">
                        <div className="form-group">
                          <label className="control-label">
                            {home_meta_title[this.context.Language]}
                          </label>{" "}
                          <span class="required" />
                          <div className="input-icon right">
                            <i className="fa" />
                            <input
                              placeholder= {home_meta_title[this.context.Language]}
                              className="form-control"
                              value={this.state.model.SeoConfig.MetaTitle}
                              onChange={e => {
                                this.state.model.SeoConfig.MetaTitle =
                                  e.target.value;
                                this.setState(this.state);
                              }}
                            />
                          </div>
                        </div>
                        <div className="form-group">
                          <label className="control-label">
                             {home_meta_des[this.context.Language]}
                          </label>
                          <input
                            placeholder={home_meta_des[this.context.Language]}
                            className="form-control"
                            value={this.state.model.SeoConfig.MetaDescription}
                            onChange={e => {
                              this.state.model.SeoConfig.MetaDescription =
                                e.target.value;
                              this.setState(this.state);
                            }}
                          />
                        </div>
                        <div className="form-group">
                          <label className="control-label">
                            {home_meta_key[this.context.Language]}
                          </label>
                          <input
                            placeholder= {home_meta_key[this.context.Language]}
                            className="form-control"
                            value={this.state.model.SeoConfig.MetaKeyword}
                            onChange={e => {
                              this.state.model.SeoConfig.MetaKeyword =
                                e.target.value;
                              this.setState(this.state);
                            }}
                          />
                        </div>
                        <div className="form-group">
                          <label className="control-label">
                            Google Analytic
                          </label>{" "}
                          <input
                            placeholder="Google Analytic"
                            className="form-control"
                            value={this.state.model.SeoConfig.GoogleAnalytic}
                            onChange={e => {
                              this.state.model.SeoConfig.GoogleAnalytic =
                                e.target.value;
                              this.setState(this.state);
                            }}
                          />
                        </div>
                        <div className="form-group">
                          <label className="control-label">
                            Webmaster Tool
                          </label>{" "}
                          <input
                            placeholder="Webmaster Tool"
                            className="form-control"
                            value={this.state.model.SeoConfig.WebmasterTool}
                            onChange={e => {
                              this.state.model.SeoConfig.WebmasterTool =
                                e.target.value;
                              this.setState(this.state);
                            }}
                          />
                        </div>
                      </div>
                    </div>
                    <div className="kt-portlet kt-portlet--mobile">
                      <div className="kt-portlet__head kt-portlet__head--lg">
                        <div className="kt-portlet__head-label">
                          <h3 className="kt-portlet__head-title">
                             {website_config2[this.context.Language]}
                          </h3>
                        </div>
                      </div>

                      <div className="kt-portlet__body">
                        <div className="form-group">
                           <label className="control-label">
                                {time_zone[this.context.Language]}
                            </label>
                          <Select
                            value={this.state.ex.TimeZoneList.filter(option => {
                              return (
                                option.value ===
                                this.state.model.WebsiteConfig.TimeZone
                              );
                            })}
                            isSearchable={true}
                            options={this.state.ex.TimeZoneList}
                            onChange={e => {
                              var value = e === null ? "" : e.value;
                              this.state.model.WebsiteConfig.TimeZone = value;
                              this.setState(this.state);
                            }}
                          />
                        </div>
                        <div className="form-group">
                          <label className="control-label">
                            {datetime_format[this.context.Language]}
                          </label>{" "}
                          <input
                            placeholder= {datetime_format[this.context.Language]}
                            className="form-control"
                            value={
                              this.state.model.WebsiteConfig
                                .DateTimeCShapeFormat
                            }
                            onChange={e => {
                              this.state.model.WebsiteConfig.DateTimeCShapeFormat =
                                e.target.value;
                              this.setState(this.state);
                            }}
                          />
                        </div>
                        <div className="form-group">
                          <label className="control-label">
                             {date_format[this.context.Language]}
                          </label>{" "}
                          <input
                            placeholder=  {date_format[this.context.Language]}
                            className="form-control"
                            value={
                              this.state.model.WebsiteConfig.DateCShapeFormat
                            }
                            onChange={e => {
                              this.state.model.WebsiteConfig.DateCShapeFormat =
                                e.target.value;
                              this.setState(this.state);
                            }}
                          />
                        </div>
                        <div className="form-group">
                          <label className="control-label">{cus_css[this.context.Language]}</label>{" "}
                          <input
                            placeholder={cus_css[this.context.Language]}
                            className="form-control"
                            value={this.state.model.WebsiteConfig.CustomizeCss}
                            onChange={e => {
                              this.state.model.WebsiteConfig.CustomizeCss =
                                e.target.value;
                              this.setState(this.state);
                            }}
                          />
                        </div>
                        <div className="form-group">
                          <label className="control-label">{cus_js[this.context.Language]}</label>{" "}
                          <input
                            placeholder={cus_js[this.context.Language]}
                            className="form-control"
                            value={this.state.model.WebsiteConfig.CustomizeJs}
                            onChange={e => {
                              this.state.model.WebsiteConfig.CustomizeJs =
                                e.target.value;
                              this.setState(this.state);
                            }}
                          />
                        </div>
                        <div className="form-group">
                          <label className="control-label">
                            {gcapt_pub[this.context.Language]}
                          </label>{" "}
                          <input
                            placeholder={gcapt_pub[this.context.Language]}
                            className="form-control"
                            value={
                              this.state.model.WebsiteConfig.GcaptchaPublic
                            }
                            onChange={e => {
                              this.state.model.WebsiteConfig.GcaptchaPublic =
                                e.target.value;
                              this.setState(this.state);
                            }}
                          />
                        </div>
                        <div className="form-group">
                          <label className="control-label">
                            {gcapt_pri[this.context.Language]}
                          </label>{" "}
                          <input
                            placeholder={gcapt_pri[this.context.Language]}
                            className="form-control"
                            value={
                              this.state.model.WebsiteConfig.GcaptchaPrivate
                            }
                            onChange={e => {
                              this.state.model.WebsiteConfig.GcaptchaPrivate =
                                e.target.value;
                              this.setState(this.state);
                            }}
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-6">
                    <div className="kt-portlet kt-portlet--mobile">
                      <div className="kt-portlet__head kt-portlet__head--lg">
                        <div className="kt-portlet__head-label">
                          <h3 className="kt-portlet__head-title">
                            {web_info[this.context.Language]}
                          </h3>
                        </div>
                      </div>

                      <div className="kt-portlet__body">
                        <div className="form-group">
                          <label className="control-label"> {web_name[this.context.Language]}</label>{" "}
                          <input
                            placeholder={web_name[this.context.Language]}
                            className="form-control"
                            value={this.state.model.WebInfomation.WebsiteName}
                            onChange={e => {
                              this.state.model.WebInfomation.WebsiteName =
                                e.target.value;
                              this.setState(this.state);
                            }}
                          />
                        </div>
                        <div className="form-group">
                          <label className="control-label">{web_url[this.context.Language]}</label>{" "}
                          <input
                            placeholder={web_url[this.context.Language]}
                            className="form-control"
                            value={this.state.model.WebInfomation.WebsiteURL}
                            onChange={e => {
                              this.state.model.WebInfomation.WebsiteURL =
                                e.target.value;
                              this.setState(this.state);
                            }}
                          />
                        </div>
                        <div className="form-group">
                          <label className="control-label">{web_email[this.context.Language]}</label>{" "}
                          <input
                            placeholder={web_email[this.context.Language]}
                            className="form-control"
                            value={this.state.model.WebInfomation.WebsiteEmail}
                            onChange={e => {
                              this.state.model.WebInfomation.WebsiteEmail =
                                e.target.value;
                              this.setState(this.state);
                            }}
                          />
                        </div>
                        <div className="form-group">
                          <label className="control-label">
                            {web_email_noti[this.context.Language]}
                          </label>{" "}
                          <input
                            placeholder={web_email_noti[this.context.Language]}
                            className="form-control"
                            value={
                              this.state.model.WebInfomation.WebsiteNotification
                            }
                            onChange={e => {
                              this.state.model.WebInfomation.WebsiteNotification =
                                e.target.value;
                              this.setState(this.state);
                            }}
                          />
                        </div>
                        <div className="form-group">
                          <label className="control-label">{web_phone[this.context.Language]}</label>{" "}
                          <input
                            placeholder={web_phone[this.context.Language]}
                            className="form-control"
                            value={this.state.model.WebInfomation.WebsitePhone}
                            onChange={e => {
                              this.state.model.WebInfomation.WebsitePhone =
                                e.target.value;
                              this.setState(this.state);
                            }}
                          />
                        </div>
                        <div className="form-group">
                          <label className="control-label">
                            {web_address[this.context.Language]}
                          </label>{" "}
                          <textarea
                            placeholder={web_address[this.context.Language]}
                            className="form-control"
                            value={
                              this.state.model.WebInfomation.WebsiteAddress
                            }
                            onChange={e => {
                              this.state.model.WebInfomation.WebsiteAddress =
                                e.target.value;
                              this.setState(this.state);
                            }}
                          />
                        </div>
                        <div className="form-group">
                          <label className="control-label">
                            {web_google[this.context.Language]}
                          </label>{" "}
                          <textarea
                            placeholder= {web_google[this.context.Language]}
                            className="form-control"
                            value={
                              this.state.model.WebInfomation.WebsiteGoogleMap
                            }
                            onChange={e => {
                              this.state.model.WebInfomation.WebsiteGoogleMap =
                                e.target.value;
                              this.setState(this.state);
                            }}
                          />
                        </div>
                        <div className="form-group">
                          <label className="control-label">
                             {web_facebook[this.context.Language]}
                          </label>{" "}
                          <input
                            placeholder={web_facebook[this.context.Language]}
                            className="form-control"
                            value={
                              this.state.model.WebInfomation.WebsiteFacebook
                            }
                            onChange={e => {
                              this.state.model.WebInfomation.WebsiteFacebook =
                                e.target.value;
                              this.setState(this.state);
                            }}
                          />
                        </div>
                        <div className="form-group">
                          <label className="control-label">
                            {web_ins[this.context.Language]}
                          </label>{" "}
                          <input
                            placeholder={web_ins[this.context.Language]}
                            className="form-control"
                            value={
                              this.state.model.WebInfomation.WebsiteInstagram
                            }
                            onChange={e => {
                              this.state.model.WebInfomation.WebsiteInstagram =
                                e.target.value;
                              this.setState(this.state);
                            }}
                          />
                        </div>
                        <div className="form-group">
                          <label className="control-label">
                            {web_youtube[this.context.Language]}
                          </label>{" "}
                          <input
                            placeholder={web_youtube[this.context.Language]}
                            className="form-control"
                            value={
                              this.state.model.WebInfomation.WebsiteYoutube
                            }
                            onChange={e => {
                              this.state.model.WebInfomation.WebsiteYoutube =
                                e.target.value;
                              this.setState(this.state);
                            }}
                          />
                        </div>
                        <div className="form-group">
                          <label className="control-label">
                            {web_twitter[this.context.Language]}
                          </label>{" "}
                          <input
                            placeholder= {web_twitter[this.context.Language]}
                            className="form-control"
                            value={
                              this.state.model.WebInfomation.WebsiteTwitter
                            }
                            onChange={e => {
                              this.state.model.WebInfomation.WebsiteTwitter =
                                e.target.value;
                              this.setState(this.state);
                            }}
                          />
                        </div>
                        <div className="form-group">
                          <label className="control-label">
                             {web_pin[this.context.Language]}
                          </label>{" "}
                          <input
                            placeholder={web_pin[this.context.Language]}
                            className="form-control"
                            value={
                              this.state.model.WebInfomation.WebsitePinterest
                            }
                            onChange={e => {
                              this.state.model.WebInfomation.WebsitePinterest =
                                e.target.value;
                              this.setState(this.state);
                            }}
                          />
                        </div>
                        <div className="form-group">
                          <label className="control-label">
                            {web_lis[this.context.Language]}
                          </label>{" "}
                          <input
                            placeholder={web_lis[this.context.Language]}
                            className="form-control"
                            value={
                              this.state.model.WebInfomation.WebsiteLicense
                            }
                            onChange={e => {
                              this.state.model.WebInfomation.WebsiteLicense =
                                e.target.value;
                              this.setState(this.state);
                            }}
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </React.Fragment>
        ) : (
          <div />
        )}
      </React.Fragment>
    );
  }
}
WebsiteConfig.contextType = AppContext;
export default WebsiteConfig;
