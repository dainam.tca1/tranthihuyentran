//#region menuconfig
const menu_config = ["Menu Configuration", "Cấu hình menu"];
const add_item = ["Add Item", "Thêm"];
const edit = ["Edit", "Sửa"];
const add_menu = ["Add Menu", "Thêm menu"];
const add_menu_item = ["Add Menu Item", "Thêm menu mới"];
const update_menu_item = ["Update Menu Item", "Cập nhật menu item"];
const text = ["Text", "Chữ"];
const menu_text = ["Menu Text", "Chữ menu"];
//#endregion
//#region website config
const website_config1 = ["Website Configuration", "Cấu hình Website"];
const web_info = ["Website Information", "Thông tin cơ bản"];
const seo_config = ["SEO Config", "Cấu hình SEO"];
const website_config2 = ["Website Config", "Cấu hình website"];
const time_zone = ["Time Zone", "Múi giờ"];
const home_meta_title = ["Home Meta Title", "Tiêu đề trang chủ"];
const home_meta_des = ["Home Meta Description", "Mô tả ngắn trang chủ"];
const home_meta_key = ["Home Meta Keyword", "Mô tả từ khóa trang chủ"];
const datetime_format = ["DateTime CShape Format", "Định dạng C# ngày giờ"];
const date_format = ["Date CShape Format", "Định dạng C# ngày"];
const cus_css = ["Customize Css", "Tùy chỉnh Css"];
const cus_js = ["Customize Js", "Tùy chỉnh Js"];
const gcapt_pub = ["Gcaptcha Public", "Công cộng Gcaptcha"];
const gcapt_pri = ["Gcaptcha Private", "Riêng tư Gcaptcha "];
const web_name = ["Website Name", "Tên trang web"];
const web_url = ["Website URL", "Url Trang web"];
const web_email = ["Website Email", "Email"];
const web_email_noti = ["Website Email Notification", "Email thông báo"];
const web_phone = ["Website Phone", "Số điện thoại website"];
const web_google = ["Website GoogleMap", "Bản đồ website"];
const web_address = ["Website Address", "Địa chỉ website"];
const web_facebook = ["Website Facebook", "MXH facebook"];
const web_ins = ["Website Instagram", "MXH Instagram"];
const web_youtube = ["Website Youtube", "MXH youtube"];
const web_twitter = ["Website Twitter", "MXH twitter"];
const web_pin = ["Website Pinterest", "MXH Pinterest"];
const web_lis = ["Website License", "Giấy phép trang web"];
//#endregion
//#region key config
const key_config = ["Key Configuration", "Cấu hình khóa"];
const key = ["Key", "Khóa"];
const config_key = ["CONFIG KEY (FOR TECHNICAL ONLY)", "Cấu hình khóa (dành cho kỹ thuật)"];
const add_key = ["Add Key", "Thêm khóa"];
const update_key = ["Update Key", "Cập nhật khóa"];
const key_value = ["Value", "Giá trị"];
//#endregion

//#region theme config
const theme_config = ["Theme Configuration", "Cấu hình giao diện"];
const theme = ["Themes", "Giao diện"];
const add_theme = ["Add theme config page", "Thêm cấu hình trang giao diện"];
const update_theme = ["Update theme config page", "Cập nhật cấu hình trang giao diện"];
const theme_name = ["Theme name group", "Tên nhóm giao diện"];
const name = ["Name", "Tên"];
const class1 = ["Class", "Lớp"];
const group =["Group", "Nhóm"];
const image =["Image", "Hình ảnh"];
const image_list =["Image List", "Danh sách hình ảnh"];
const text_image_list = ["Text Image List", "Danh sách hình ảnh & chữ"];
const template = ["Template", "Bản mẫu"];
const de_group = ["Delete Group", "Xóa nhóm"];
const text_style = ["Text style", "Kiểu chữ"];
const normal = ["Normal", "Bình thường"];
const link1 = ["Link", "Đường dẫn"];
const please_enter_item_name = ["Please enter the Item name", "Hãy điền tên item"];
//#endregion
//#region App
const hi = ["Hi", "Xin Chào"];
const change_pw_btn = ["Change Password", "Đổi Mật Khẩu"];
const myprofile = ["My Profile", "Tài khoản của tôi"];
const setting_account = ["Account settings", "Cài đặt tài khoản"];
const sign_out = ["Sign Out", "Đăng Xuất"];
//#endregion
//#region Nav
const dashboard = ["Dashboard", "Trang Chủ"];
const contact = ["Contacts", "Liên Hệ"];
const services = ["Services", "Dịch Vụ"];
const service_ls = ["Service List", "Danh Sách Dịch Vụ"];
const service_cat = ["Service Categories", "Danh Mục Dịch Vụ"];
const pos_product = ["POS Products", "Sản Phẩm POS"];
const products = ["Products", "Sản Phẩm"];
const product_ls = ["Product List", "Danh Sách Sản Phẩm"];
const product_cat = ["Product Categories", "Danh Mục Sản Phẩm"];
const product_cus_cat = ["Product Custom Categories", "Danh Mục Sản Phẩm Tùy Chỉnh"];
const product_brands = ["Brand", "Nhãn Hiệu"];
const product_att_group = ["Attribute Group", "Nhóm Thuộc Tính"];
const product_att_name = ["Attribute Name", "Tên Thuộc Tính"];
const product_att_value = ["Attribute Value", "Giá Trị Thuộc Tính"];
const label_ls = ["Label List", "Danh Sách Nhãn"];
const order_history = ["Order History", "Lịch Sử Mua Hàng"];
const dgt_signage = ["Digital Signage", "Digital Signage"];
const device_ls = ["Devices", "Danh Sách Thiết Bị"];
const play_ls = ["Playlist", "Danh Sách Phát"];
const video_ls = ["Videos", "Danh Sách Video"];
const promotion = ["Promotions", "Khuyến Mãi"];
const promotion_news = ["Promotion News", "Tin Tức Khuyến Mãi"];
const coupon_code = ["Coupon Code", "Mã Giảm Giá"];
const giftcard_coupon = ["Gift Card Coupons", "Phiếu Quà Tặng"];
const blog = ["Blog", "Tin Tức"];
const blog_ls = ["Blog List", "Danh Sách Tin Tức"];
const blog_cat = ["Blog Categories", "Danh Mục Tin Tức"];
const gallery = ["Galleries", "Thư Viện Ảnh"];
const static_pages = ["Static Pages", "Trang Tĩnh"];
const staff = ["Staffs", "Nhân Viên"];
const gpos_staff = ["GPos Staffs", "Nhân Viên GPos"];
const conf_title = ["CONFIGURATION", "CẤU HÌNH VÀ CÀI ĐẶT"];
const website_conf = ["Website Configuration", "Cấu Hình Website"];
const booking_setting = ["Booking Setting", "Thiết Lập Đặt Lịch"];
const booking_report = ["Booking Reports", "Báo Cáo Đặt Lịch"];
const order_pickup_setting = ["Order Pick-up Setting", "Cài Đặt Đơn Hàng"];
const giftcard_his = ["Gift Card Histories", "Lịch Sử Phiếu Quà Tặng"];
const giftcard_temp_setting = ["Gift Card Template Setting", "Cài Đặt Phiếu Quà Tặng Mẫu"];
const menu_conf = ["Menu Config", "Cấu Hình Menu"];
const ship_setting = ["Shipping Setting", "Cấu Hình Vận Chuyển"];
const ship_by_dist = ["Ship Fee By District", "Phí Ship Theo Quận Huyện"];
const ship_by_total_bill = ["Ship Fee Total Order Value", "Phí Ship Theo Tổng Giá Trị Đơn Hàng"];
const admin_manager = ["Admin Manager", "Quản lí Tài Khoản Quản Trị"];
const key_configuration = ["Key configuration", "Cấu Hình Key"];
const comment = ["Comment - Rating", "Bình luận và đánh giá"];
const video = ["Video Youtube", "Video Youtube"];
const tags = ["Tags", "Tags"];
//#endregion
//#region Aminmanager
const admin_list = ["Admin User List", "Danh Sách Admin"];
const admin_name = ["Name", "Tên"];
const admin_phone = ["Phone", "Số điện thoại"];
const admin_email = ["Email", "Email"];
const add_user_admin = ["Add User Admin", "Thêm tài khoản Admin"];
const password = ["Password", "Mật Khẩu"];
const confirm_password = ["Confirm Password", "Nhập lại mật Khẩu"];
//#endregion
module.exports = {
    //#region menuconfig
    menu_config,
    add_item,
    edit,
    add_menu,
    add_menu_item,
    text,
    menu_text,
    update_menu_item,
    //#endregion
    //#region web config
    website_config1,
    web_info,
    seo_config,
    website_config2,
    time_zone,
    home_meta_title,
    home_meta_des,
    home_meta_key,
    datetime_format,
    date_format,
    cus_css,
    cus_js,
    gcapt_pub,
    gcapt_pri,
    web_name,
    web_url,
    web_email,
    web_email_noti,
    web_phone,
    web_google,
    web_address,
    web_facebook,
    web_ins,
    web_youtube,
    web_twitter,
    web_pin,
    web_lis,
    //#endregion
    //#region key
    key_config,
    key,
    config_key,
    add_key,
    update_key,
    key_value,
    //#endregion
    //#region theme
    theme_config,
    theme,
    add_theme,
    update_theme,
    theme_name,
    name,
    class1,
    group,
    image,
    image_list,
    text_image_list,
    template,
    de_group,
    please_enter_item_name,
    text_style,
    normal,
    link1,
    //#endregion
    //#region App
    hi,
    change_pw_btn,
    myprofile,
    setting_account,
    sign_out,
    //#endregion
    //#region nav
    dashboard,
    contact,
    services,
    service_ls,
    service_cat,
    pos_product,
    products,
    product_ls,
    product_cat,
    product_cus_cat,
    product_brands,
    product_att_group,
    product_att_name,
    product_att_value,
    label_ls,
    order_history,
    dgt_signage,
    device_ls,
    play_ls,
    video_ls,
    promotion,
    promotion_news,
    coupon_code,
    giftcard_coupon,
    blog,
    blog_ls,
    blog_cat,
    gallery,
    static_pages,
    staff,
    gpos_staff,
    conf_title,
    website_conf,
    booking_setting,
    booking_report,
    order_pickup_setting,
    giftcard_his,
    giftcard_temp_setting,
    menu_conf,
    ship_setting,
    ship_by_dist,
    ship_by_total_bill,
    admin_manager,
    key_configuration,
    comment,
    video,
    tags,
    //#endregion
    //#region admin manager
    admin_list,
    admin_name,
    admin_phone,
    admin_email,
    add_user_admin,
    password,
    confirm_password,
    //#endregion
};
