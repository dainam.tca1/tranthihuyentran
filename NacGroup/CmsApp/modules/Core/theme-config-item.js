import React, { Component } from "react";
import {
  ThemeConfigImageItem,
  ThemeConfigTextItem,
  ThemeConfigModel,
  ThemeConfigTextImageList,
  ThemeConfigImageList
} from "./models/thememodel";
import {
    globalErrorMessage,
    error,
    success,
    back,
    add,
    remove,
    total,
    record,
    page,
    cDate,
    lDate,
    recordPerPage,
    are_you_sure,
    yes_de,
    no_ca,
    close,
    sort,
    save,
    cancel,
    data_recover
} from "../../constants/message";
import {
    theme_config,
    theme,
    add_theme,
    update_theme,
    theme_name,
    name,
    class1,
    group,
    image,
    image_list,
    text_image_list,
    template,
    add_item,
    de_group,
    please_enter_item_name,
    text_style,
    normal,
    link1,
    text,
} from "./models/corestaticmessage";
import AppContext from "../../components/app-context";

//const {
//  THEMETYPE,
//  THEME_TEXT_TYPE
//} = require("../../../../../libs/constants/enums");

import ThemeImageItem from "./theme-image-item";
import ThemeTextItem from "./theme-text-item";
import ThemeImageList from "./theme-image-list";
import ThemeTextImageList from "./theme-text-image-list";

class ThemeConfigItem extends Component {
  constructor(props,context) {
    super(props,context);
    this.state = {
      AddUpdateItem: {
        Name: null,
        Type: 0,
        TextType: 0,
        TextTemplate: null,
        ImageTemplate: null
      }
    };
    this.popup = React.createRef();
  }
  AddItem() {
      if (!this.state.AddUpdateItem.Name) {
          toastr["error"](please_enter_item_name[this.context.Language], error[this.context.Language]);
      return -1;
    }
    var newmodel = { ...this.props.model };
    if (!newmodel.Data) {
      newmodel.Data = [];
    }
    var addmodel = {};

    switch (this.state.AddUpdateItem.Type) {
      case 0:
        addmodel = new ThemeConfigModel();
        addmodel.Name = this.state.AddUpdateItem.Name;
        addmodel.ThemeType = this.state.AddUpdateItem.Type;
        break;
      case 1:
        addmodel = new ThemeConfigImageItem();
        addmodel.Name = this.state.AddUpdateItem.Name;
        addmodel.ThemeType = this.state.AddUpdateItem.Type;
        break;
      case 2:
        addmodel = new ThemeConfigTextItem();
        addmodel.Name = this.state.AddUpdateItem.Name;
        addmodel.TextType = this.state.AddUpdateItem.TextType;
        addmodel.ThemeType = this.state.AddUpdateItem.Type;
        break;
      case 3:
        addmodel = new ThemeConfigImageList();
        addmodel.Name = this.state.AddUpdateItem.Name;
        addmodel.ThemeType = this.state.AddUpdateItem.Type;
        break;
      case 4:
        $(this.popup.current).modal("show");
        return 1;
    }
    newmodel.Data.push(addmodel);
    this.props.onChange(newmodel);
  }

  componentDidMount() {}

  render() {
    return (
      <div className="kt-portlet kt-portlet--mobile kt-portlet--bordered">
        <div className="kt-portlet__head kt-portlet__head--lg kt-bg-primary">
          <div className="kt-portlet__head-label">
            <h3 className="kt-portlet__head-title font-bold">
              <input
                value={this.props.model.Name}
                style={{ color: "#000" }}
                placeholder={theme_name[this.context.Language]}
                onChange={e => {
                  this.props.model.Name = e.target.value;
                  this.props.onChange(this.props.model);
                }}
              />
              {this.props.isShowKeyInput && (
                <input
                  value={this.props.model.Slug}
                  style={{ color: "#000" }}
                  placeholder="THEME_NAMEKEY"
                  onChange={e => {
                    this.props.model.Slug = e.target.value;
                    this.props.onChange(this.props.model);
                  }}
                />
              )}
            </h3>
          </div>
          <div className="kt-portlet__head-toolbar">
            <div className="kt-portlet__head-group">
              <a
                href="javascript:;"
                onClick={e => {
                  $(e.target)
                    .parent()
                    .parent()
                    .parent()
                    .parent()
                    .parent()
                    .find(".kt-portlet__body")
                    .toggle();
                  $(e.target)
                    .parent()
                    .parent()
                    .parent()
                    .parent()
                    .parent()
                    .toggleClass("kt-portlet--collapse");
                }}
                data-ktportlet-tool="toggle"
                className="btn btn-outline-light btn-sm btn-icon btn-icon-md"
              >
                <i className="la la-angle-down"></i>
              </a>
            </div>
          </div>
        </div>

        <div className="kt-portlet__body">
          <div className="row">
            <div className="col-md-3 form-group">
              <div className="input-group">
                <div className="input-group-prepend">
                  <span className="input-group-text">{name[this.context.Language]}</span>
                </div>
                <input
                  className="form-control"
                  value={this.state.AddUpdateItem.Name}
                  onChange={e => {
                    this.state.AddUpdateItem.Name = e.target.value;
                    this.setState(this.state);
                  }}
                />
              </div>
            </div>
            <div className="col-md-3 form-group">
              <div className="input-group">
                <div className="input-group-prepend">
                  <span className="input-group-text">{name[this.context.Language]}</span>
                </div>
                <select
                  className="form-control"
                  value={this.state.AddUpdateItem.Type}
                  onChange={e => {
                    this.state.AddUpdateItem.Type = parseInt(e.target.value);
                    this.setState(this.state);
                  }}
                >
                  <option value={0}>{group[this.context.Language]}</option>
                  <option value={1}>{image[this.context.Language]}</option>
                  <option value={2}>{text[this.context.Language]}</option>
                  <option value={3}>{image_list[this.context.Language]}</option>
                  <option value={4}>{text_image_list[this.context.Language]}</option>
                </select>
              </div>
            </div>

            {this.state.AddUpdateItem.Type == 2 && (
              <div className="col-md-3 form-group">
                <div className="input-group">
                                <div className="input-group-prepend">
                                    <span className="input-group-text">{text_style[this.context.Language]}</span>
                  </div>
                  <select
                    value={this.state.AddUpdateItem.TextType}
                    className="form-control"
                    onChange={e => {
                      this.state.AddUpdateItem.TextType = parseInt(
                        e.target.value
                      );
                      this.setState(this.state);
                    }}
                  >
                    <option value={0}>{normal[this.context.Language]}</option>
                    <option value={1}>Html</option>
                    <option value={2}>{link1[this.context.Language]}</option>
                  </select>
                </div>
              </div>
            )}

            <div className="col-md-3 form-group">
              <a
                href="javascript:"
                className="btn btn-primary"
                onClick={e => {
                  this.AddItem();
                }}
              >
                {add_item[this.context.Language]}
              </a>{" "}
              {this.props.isShowDelete && (
                <a
                  href="javascript:"
                  className="btn btn-danger"
                  onClick={e => {
                    var that = this;

                    swal
                        .fire({
                            title: are_you_sure[this.context.Language],
                            text: data_recover[this.context.Language],
                        type: "warning",
                        showCancelButton: true,
                            confirmButtonText: yes_de[this.context.Language],
                            cancelButtonText: no_ca[this.context.Language]
                      })
                      .then(function(result) {
                        if (result.value) {
                          that.props.onRemove(that.props.index);
                          return true;
                        }
                      });
                  }}
                            >
                                {de_group[this.context.Language]}
                </a>
              )}
            </div>
          </div>
          <div className="datalist">
            {this.props.model.Data &&
              this.props.model.Data.map((e, index) => {
                if (e.ThemeType == 0) {
                  return (
                    <ThemeConfigItem
                      model={e}
                      index={index}
                      isShowDelete={true}
                      isShowKeyInput={false}
                      onRemove={ix => {
                        this.props.model.Data.splice(ix, 1);
                        this.props.onChange(this.props.model);
                      }}
                      onChange={newmodel => {
                        e = newmodel;
                        this.props.onChange(this.props.model);
                      }}
                    />
                  );
                } else if (e.ThemeType == 1) {
                  return (
                    <ThemeImageItem
                      model={e}
                      index={index}
                      onRemove={ix => {
                        this.props.model.Data.splice(ix, 1);
                        this.props.onChange(this.props.model);
                      }}
                      onChange={newmodel => {
                        e = newmodel;
                        this.props.onChange(this.props.model);
                      }}
                    />
                  );
                } else if (e.ThemeType == 2) {
                  return (
                    <ThemeTextItem
                      model={e}
                      index={index}
                      onRemove={ix => {
                        this.props.model.Data.splice(ix, 1);
                        this.props.onChange(this.props.model);
                      }}
                      onChange={newmodel => {
                        e = newmodel;
                        this.props.onChange(this.props.model);
                      }}
                    />
                  );
                } else if (e.ThemeType == 3) {
                  return (
                    <ThemeImageList
                      model={e}
                      index={index}
                      onRemove={ix => {
                        this.props.model.Data.splice(ix, 1);
                        this.props.onChange(this.props.model);
                      }}
                      onChange={newmodel => {
                        this.props.model.Data[index] = newmodel;
                        this.props.onChange(this.props.model);
                      }}
                    />
                  );
                } else if (e.ThemeType == 4) {
                  return (
                    <ThemeTextImageList
                      model={e}
                      index={index}
                      onRemove={ix => {
                        this.props.model.Data.splice(ix, 1);
                        this.props.onChange(this.props.model);
                      }}
                      onChange={newmodel => {
                        this.props.model.Data[index] = newmodel;
                        this.props.onChange(this.props.model);
                      }}
                    />
                  );
                }
              })}
          </div>
        </div>

        <div
          class="modal fade"
          id="textimagepopup"
          ref={this.popup}
          tabindex="-1"
          role="dialog"
          aria-labelledby="exampleModalLabel"
          aria-hidden="true"
        >
          <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                   {template[this.context.Language]}
                </h5>
                <button
                  type="button"
                  class="close"
                  data-dismiss="modal"
                  aria-label="Close"
                ></button>
              </div>
              <div class="modal-body">
                <div className="form-group">
                  <input
                    placeholder="Name;Name.."
                    className="form-control"
                    value={this.state.AddUpdateItem.ImageTemplate}
                    onChange={e => {
                      this.state.AddUpdateItem.ImageTemplate = e.target.value;
                      this.setState(this.state);
                    }}
                  />
                </div>
                <div className="form-group">
                  <input
                    placeholder="Name:0-1-2;Name:0-1-2.."
                    className="form-control"
                    value={this.state.AddUpdateItem.TextTemplate}
                    onChange={e => {
                      this.state.AddUpdateItem.TextTemplate = e.target.value;
                      this.setState(this.state);
                    }}
                  />
                </div>
              </div>
              <div class="modal-footer">
                <button
                  type="button"
                  className="btn btn-secondary"
                  data-dismiss="modal"
                >
                                {cancel[this.context.Language]}
                </button>
                <button
                  type="button"
                  className="btn btn-primary"
                  onClick={e => {
                    if (
                      !this.state.AddUpdateItem.ImageTemplate &&
                      !this.state.AddUpdateItem.TextTemplate
                    ) {
                      toastr["error"](
                          please_enter_item_name[this.context.Language],
                          error[this.context.Language]
                      );
                      return -1;
                    }
                    var imglst = this.state.AddUpdateItem.ImageTemplate
                      ? this.state.AddUpdateItem.ImageTemplate.split(";")
                      : [];
                    var txtlst = this.state.AddUpdateItem.TextTemplate
                      ? this.state.AddUpdateItem.TextTemplate.split(";")
                      : [];

                    var txttemplate = [];
                    if (txtlst.length > 0) {
                      txtlst.forEach(element => {
                        var splits = element.split(":");
                        if (splits.length != 2 || isNaN(splits[1])) return;
                        txttemplate.push({
                          Name: splits[0],
                          TextType: parseInt(splits[1])
                        });
                      });
                    }

                    var addmodel = new ThemeConfigTextImageList();
                    addmodel.Name = this.state.AddUpdateItem.Name;
                    addmodel.Template.ImageList = imglst;
                    addmodel.Template.TextList = txttemplate;
                    this.props.model.Data.push(addmodel);
                    this.props.onChange(this.props.model);
                    $(this.popup.current).modal("hide");
                  }}
                >
                  {add_item[this.context.Language]}
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
ThemeConfigItem.contextType = AppContext;
export default ThemeConfigItem;
