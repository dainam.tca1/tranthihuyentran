import React, { Component } from "react";
import { Link } from "react-router-dom";
import {
    globalErrorMessage,
    error,
    success,
    add,
    remove,
    total,
    record,
    page,
    cDate,
    lDate,
    recordPerPage,
    are_you_sure,
    yes_de,
    no_ca,
    close,
    sort,
    save,
    data_recover
} from "../../constants/message";
import {
    menu_config,
    add_item,
    edit,
    add_menu,
    add_menu_item,
    text,
    menu_text,
    update_menu_item
} from "./models/corestaticmessage";
import AppContext from "../../components/app-context";
import MenuConfigList from "./menu-config-list";
const apiurl = "/cms/api/menuconfig";

class MenuConfig extends Component {
  constructor(props,context) {
      super(props, context);
    var that = this;
    this.state = {
      model: null,
      ex: {
          Title: menu_config[this.context.Language],
        AddModel: {},
        UpdateModel: {}
      }
    };
    KTApp.blockPage();

    $.ajax({
      url: apiurl,
      type: "GET",
      dataType: "json",
      contentType: "application/json",
      success: response => {
        KTApp.unblockPage();
        toastr.clear();
        if (response.status === "success") {
          that.state.model = response.data;
          that.setState(that.state);
        } else {
            toastr["error"](response.message, error[this.context.Language]);
        }
      },
      error: function(er) {
        KTApp.unblockPage();
        toastr.clear();
          toastr["error"](globalErrorMessage[this.context.Language], error[this.context.Language]);
      }
    });
  }
  componentWillMount() {
    $("#cssloading").html(
      `<link href="/adminstatics/global/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />`
    );
    $("#scriptloading").html(
      `<script src="/adminstatics/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>`
    );
  }
  componentDidMount() {
    document.title = this.state.ex.Title;
    $("#kt_aside_menu .kt-menu__item").removeClass("kt-menu__item--active");
    $("#kt_aside_menu .kt-menu__item[data-id='menu-config']").addClass(
      "kt-menu__item--active"
    );
  }
  addMenuItem() {
    $("#add-modal").modal("hide");
    var that = this;
    KTApp.blockPage();
    $.ajax({
      url: apiurl + "/add",
      type: "POST",
      dataType: "json",
      contentType: "application/json",
      data: JSON.stringify(that.state.ex.AddModel),
      success: response => {
        KTApp.unblockPage();
        if (response.status == "success") {
          swal.fire({
              title: success[that.context.Language],
            text: response.message,
            type: "success",
            onClose: () => {
              that.props.history.push("/admin/emptypage");
              that.props.history.replace({
                pathname: that.props.location.pathname,
                search: that.props.location.search
              });
            }
          });
        } else {
        
            toastr["error"](response.message, error[that.context.Language]);
        }
      },
      error: function(er) {
        KTApp.unblockPage();
          toastr["error"](globalErrorMessage[that.context.Language], error[that.context.Language]);
      }
    });
  }

  updateMenuItem() {
    $("#update-modal").modal("hide");
    var that = this;
    KTApp.blockPage();
    $.ajax({
      url: apiurl + "/update",
      type: "POST",
      dataType: "json",
      contentType: "application/json",
      data: JSON.stringify(that.state.ex.UpdateModel),
      success: response => {
        KTApp.unblockPage();
        if (response.status == "success") {
          swal.fire({
            title: success[that.context.Language],
            text: response.message,
            type: "success",
            onClose: () => {
              that.props.history.push("/admin/emptypage");
              that.props.history.replace({
                pathname: that.props.location.pathname,
                search: that.props.location.search
              });
            }
          });
        } else {
            toastr["error"](response.message, error[that.context.Language]);
        }
      },
      error: function(er) {
        KTApp.unblockPage();
          toastr["error"](globalErrorMessage[that.context.Language], error[that.context.Language]);
      }
    });
  }

  remove(id) {
    var that = this;
    swal
        .fire({
            title: are_you_sure[that.context.Language],
            text: data_recover[that.context.Language],
        type: "warning",
        showCancelButton: true,
            confirmButtonText: yes_de[that.context.Language],
            cancelButtonText: no_ca[that.context.Language]
      })
      .then(function(result) {
        if (result.value) {
          KTApp.blockPage();
          $.ajax({
            url: apiurl + "/remove",
            type: "POST",
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify(id),
            success: response => {
              KTApp.unblockPage();
                toastr.clear();
              
              if (response.status == "error") {
                  toastr["error"](response.message, error[that.context.Language]);
              } else {
                 
                  toastr["success"](response.message, success[that.context.Language]);
                that.props.history.push("/admin/emptypage");
                that.props.history.replace({
                  pathname: that.props.location.pathname
                });
              }
            },
            error: er => {
              KTApp.unblockPage();
              toastr.clear();
                toastr["error"](globalErrorMessage[that.context.Language], error[that.context.Language]);
            }
          });

          return true;
        }
      });
  }

  render() {
    return (
      <React.Fragment>
        {this.state && this.state.model ? (
          <React.Fragment>
            <div className="kt-subheader kt-grid__item" id="kt_subheader">
              <div className="kt-subheader__main">
                <div className="kt-subheader__breadcrumbs">
                  <Link
                    to="/admin/dashboard"
                    className="kt-subheader__breadcrumbs-home"
                  >
                    <i className="fa fa-home" />
                  </Link>

                  <span className="kt-subheader__breadcrumbs-separator" />
                  <span className="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
                    {this.state.ex.Title}
                  </span>
                </div>
              </div>
              <div className="kt-subheader__toolbar">
                <div className="kt-subheader__wrapper">
                  <a
                    href="javascript:;"
                    className="btn btn-primary"
                    onClick={e => {
                      this.state.ex.AddModel = {
                        ParentId: null,
                        MenuLevel: 0,
                        Sort: this.state.model.length
                      };
                      this.setState(this.state);
                      $("#add-modal").modal("show");
                    }}
                  >
                                    <i className="fa fa-plus" /> {add_menu[this.context.Language]}
                  </a>
                </div>
              </div>
            </div>
            <div
              className="kt-content kt-grid__item kt-grid__item--fluid"
              id="kt_content"
            >
              <div className="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                <div className="row">
                  {this.state.model.map(menu => {
                    return (
                      <div className="col-lg-6">
                        <div className="kt-portlet kt-portlet--mobile">
                          <div className="kt-portlet__head kt-portlet__head--lg">
                            <div className="kt-portlet__head-label">
                              <h3 className="kt-portlet__head-title">
                                {menu.Text}
                              </h3>
                            </div>
                            <div className="kt-portlet__head-toolbar">
                              <div className="kt-portlet__head-group">
                                <a
                                  href="javascript:;"
                                  className="btn btn-primary btn-sm"
                                  onClick={e => {
                                    this.state.ex.AddModel = {
                                      ParentId: menu.Id,
                                      MenuLevel: menu.MenuLevel,
                                      Sort: menu.Children.length
                                    };
                                    this.setState(this.state);
                                    $("#add-modal").modal("show");
                                  }}
                                >
                                  <i className="fa fa-plus" /> {add_item[this.context.Language]}
                                </a>{" "}
                                <a
                                  href="javascript:;"
                                  className="btn btn-danger btn-sm"
                                  onClick={() => {
                                    this.remove(menu.Id);
                                  }}
                                >
                                  <i className="fa fa-trash" /> {remove[this.context.Language]}
                                </a>{" "}
                                <a
                                  href="javascript:;"
                                  className="btn btn-primary btn-sm"
                                  onClick={e => {
                                    this.state.ex.UpdateModel = menu;
                                    this.setState(this.state);
                                    $("#update-modal").modal("show");
                                  }}
                                >
                                                <i className="fa fa-cog" /> {edit[this.context.Language]}
                                </a>{" "}
                                <a
                                  href="javascript:;"
                                  onClick={e => {
                                    $(e.target)
                                      .parent()
                                      .parent()
                                      .parent()
                                      .parent()
                                      .parent()
                                      .find(".kt-portlet__body")
                                      .toggle();
                                    $(e.target)
                                      .parent()
                                      .parent()
                                      .parent()
                                      .parent()
                                      .parent()
                                      .toggleClass("kt-portlet--collapse");
                                  }}
                                  data-ktportlet-tool="toggle"
                                  className="btn btn-sm btn-icon btn-default btn-icon-md"
                                >
                                  <i className="la la-angle-down"></i>
                                </a>
                              </div>
                            </div>
                          </div>

                          <div className="kt-portlet__body menu-config-component">
                            <MenuConfigList
                              value={menu.Children}
                              onChange={() => {
                                this.props.history.push("/admin/emptypage");
                                this.props.history.replace({
                                  pathname: this.props.location.pathname
                                });
                              }}
                              onclickedAdd={addModel => {
                                this.state.ex.AddModel = addModel;
                                this.setState(this.state);
                                $("#add-modal").modal("show");
                              }}
                              onclickedUpdate={updateModel => {
                                this.state.ex.UpdateModel = updateModel;
                                this.setState(this.state);
                                $("#update-modal").modal("show");
                              }}
                            />{" "}
                          </div>
                        </div>
                      </div>
                    );
                  })}
                </div>
              </div>
            </div>

            <div
              className="modal fade"
              id="add-modal"
              ref={this.popup}
              tabindex="-1"
              role="dialog"
              aria-labelledby="exampleModalLabel"
              aria-hidden="true"
            >
              <div className="modal-dialog modal-sm" role="document">
                <div className="modal-content">
                  <div className="modal-header">
                    <h5 className="modal-title" id="exampleModalLabel">
                     {add_menu_item[this.context.Language]}
                    </h5>
                    <button
                      type="button"
                      className="close"
                      data-dismiss="modal"
                      aria-label="Close"
                    ></button>
                  </div>
                  <div className="modal-body">
                    <div className="form-group">
                      <label className="control-label">{text[this.context.Language]}</label>
                      <input
                        placeholder={menu_text[this.context.Language]}
                        className="form-control"
                        value={this.state.ex.AddModel.Text}
                        onChange={e => {
                          this.state.ex.AddModel.Text = e.target.value;
                          this.setState(this.state);
                        }}
                      />
                    </div>
                    <div className="form-group">
                      <label className="control-label">Url</label>
                      <input
                        placeholder="Url"
                        className="form-control"
                        value={this.state.ex.AddModel.Url}
                        onChange={e => {
                          this.state.ex.AddModel.Url = e.target.value;
                          this.setState(this.state);
                        }}
                      />
                    </div>
                    <div className="form-group">
                      <label className="control-label">{sort[this.context.Language]}</label>
                      <input
                        type="number"
                        placeholder={sort[this.context.Language]}
                        className="form-control"
                        value={this.state.ex.AddModel.Sort}
                        onChange={e => {
                          this.state.ex.AddModel.Sort = e.target.value;
                          this.setState(this.state);
                        }}
                      />
                    </div>
                  </div>
                  <div className="modal-footer">
                    <button
                      type="button"
                      className="btn btn-default"
                      data-dismiss="modal"
                    >
                      {close[this.context.Language]}
                    </button>
                    <button
                      type="button"
                      className="btn btn-primary"
                      onClick={e => {
                        this.addMenuItem();
                      }}
                    >
                      {save[this.context.Language]}
                    </button>{" "}
                  </div>
                </div>
              </div>
            </div>
            <div
              className="modal fade"
              id="update-modal"
              ref={this.popup}
              tabindex="-1"
              role="dialog"
              aria-labelledby="exampleModalLabel"
              aria-hidden="true"
            >
              <div className="modal-dialog modal-sm" role="document">
                <div className="modal-content">
                  <div className="modal-header">
                    <h5 className="modal-title" id="exampleModalLabel">
                      {update_menu_item[this.context.Language]}
                    </h5>
                    <button
                      type="button"
                      className="close"
                      data-dismiss="modal"
                      aria-label="Close"
                    ></button>
                  </div>
                  <div className="modal-body">
                    <div className="form-group">
                      <label className="control-label">{text[this.context.Language]}</label>
                      <span className="required" />
                      <div className="input-icon right">
                        <i className="fa" />
                        <input
                          placeholder={menu_text[this.context.Language]}
                          className="form-control"
                          value={this.state.ex.UpdateModel.Text}
                          onChange={e => {
                            this.state.ex.UpdateModel.Text = e.target.value;
                            this.setState(this.state);
                          }}
                        />
                      </div>
                    </div>
                    <div className="form-group">
                      <label className="control-label">Url</label>
                      <input
                        placeholder="Url"
                        className="form-control"
                        value={this.state.ex.UpdateModel.Url}
                        onChange={e => {
                          this.state.ex.UpdateModel.Url = e.target.value;
                          this.setState(this.state);
                        }}
                      />
                    </div>
                    <div className="form-group">
                      <label className="control-label">{sort[this.context.Language]}</label>
                      <input
                        type="number"
                        placeholder={sort[this.context.Language]}
                        className="form-control"
                        value={this.state.ex.UpdateModel.Sort}
                        onChange={e => {
                          this.state.ex.UpdateModel.Sort = e.target.value;
                          this.setState(this.state);
                        }}
                      />
                    </div>
                  </div>
                  <div className="modal-footer">
                    <button
                      type="button"
                      className="btn btn-default"
                      data-dismiss="modal"
                    >
                      {close[this.context.Language]}
                    </button>
                    <button
                      type="button"
                      className="btn btn-primary"
                      onClick={e => {
                        this.updateMenuItem();
                      }}
                    >
                                        {save[this.context.Language]}
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </React.Fragment>
        ) : (
          <div />
        )}
      </React.Fragment>
    );
  }
}
MenuConfig.contextType = AppContext;
export default MenuConfig;
