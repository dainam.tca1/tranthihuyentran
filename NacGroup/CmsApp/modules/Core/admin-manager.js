import React, { Component } from "react";
import { DelayInput } from "react-delay-input";
import PagedList from "../../components/pagedlist";
import adminManagerCmsModel from "./models/adminmanager-cmsmodel";
import {
  GLOBAL_ERROR_MESSAGE,
  ERROR,
  SUCCESS,
  DATA_NOT_RECOVER
} from "../../constants/message";
import { Link } from "react-router-dom";
import FilterListPlugin from "../../plugins/filter-list-plugin";
import RemoveListPlugin from "../../plugins/remove-list-plugin";
import {
    globalErrorMessage,
    error,
    success,
    add,
    remove,
    save,
    total,
    record,
    close,
} from "../../constants/message";
import {
    admin_list,
    admin_name,
    admin_phone,
    admin_email,
    add_user_admin,
    password,
    confirm_password,
} from "./models/corestaticmessage";
import AppContext from "../../components/app-context";
const apiurl = "/cms/api/admin-mannager";

class AdminManager extends Component {
    constructor(props, context) {
    super(props,context);
    var that = this;
    that.state = {
      ex: {
        Title: admin_list[this.context.Language],
        Param: {
          pagesize: null,
          categoryId: null
        },
        CurrentAddModel: new adminManagerCmsModel()
      }
    };
    that.setState(that.state);
    that.handleChangeFilter = that.handleChangeFilter.bind(that);
      that.handleChangeDataRow = that.handleChangeDataRow.bind(that);
      that.toPage(1);
  }

    toPage(index) {
        FilterListPlugin.filterdata(index, this, apiurl);
    }
  /**
   * Được gọi khi thay đổi các trường filter cập nhật tham số filter vào state
   * Filter dữ liệu theo tham số mới
   * @param {Event} event
   */
  handleChangeFilter(event) {
    FilterListPlugin.handleChangeFilter(event, this);
    this.toPage(1);
  }

  /**
   * Xóa data đã được chọn
   */
  handleDeleteDataRow() {
    RemoveListPlugin.removeList(`${apiurl}/removelist`, this);
  }
  handleChangeDataRow(event) {
    const target = event.target;
    const value = target.type === "checkbox" ? target.checked : target.value;
    const name = target.name;
    const index = target.getAttribute("index");
    var newobj = { ...this.state.model.Results[index] };
    newobj[name] = isNaN(value) ? value : parseInt(value);
    var that = this;
    KTApp.blockPage();
    $.ajax({
      url: apiurl + "/updatecustomize",
      type: "POST",
      dataType: "json",
      contentType: "application/json",
      data: JSON.stringify({ model: newobj, name }),
      success: response => {
        KTApp.unblockPage();
        toastr.clear();
        if (response.status == "success") {
          that.state.model.Results[index][name] = newobj[name];
          that.setState(that.state);
            toastr["success"](response.message, success[this.context.Language]);
        } else {
            toastr["error"](response.message, error[this.context.Language]);
        }
      },
      error: function(er) {
        KTApp.unblockPage();
        toastr.clear();
          toastr["error"](globalErrorMessage[this.context.Language], error[this.context.Language]);
      }
    });
  }

  handleSubmitForm() {
        var that = this;
        KTApp.blockPage();
        $.ajax({
            url:  apiurl + "/AddAdminUser",
            type: "POST",
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify(that.state.ex.CurrentAddModel),
            success: response => {
                KTApp.unblockPage();
                toastr.clear();
                if (response.status == "success") {
                    toastr["success"](response.message, success[this.context.Language]);
                    $("#add-attribute-popup").modal("hide");
                    that.toPage(1);
                } else {
                    toastr["error"](response.message, error[this.context.Language]);
                }
            },
            error: function (er) {
                KTApp.unblockPage();
                toastr.clear();
                toastr["error"](globalErrorMessage[this.context.Language], error[this.context.Language]);
            }
        });
    }
  componentWillMount() { }
  componentDidMount() {
    document.title = this.state.ex.Title;
    $("#kt_aside_menu .kt-menu__item").removeClass("kt-menu__item--active");
    $("#kt_aside_menu .kt-menu__item[data-id='danh-sach-admin']").addClass(
      "kt-menu__item--active"
    );
  }
  render() {
    return (
      <React.Fragment>
            {this.state && this.state.model ?(
          <React.Fragment>
            <div className="kt-subheader kt-grid__item" id="kt_subheader">
              <div className="kt-subheader__main">
                <div className="kt-subheader__breadcrumbs">
                  <Link
                    to="/admin/dashboard"
                    className="kt-subheader__breadcrumbs-home"
                  >
                    <i className="fa fa-home" />
                  </Link>

                  <span className="kt-subheader__breadcrumbs-separator" />
                  <span className="kt-subheader__breadcrumbs-link">
                    Admin
                  </span>
                  <span className="kt-subheader__breadcrumbs-separator" />
                  <span className="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
                    {this.state.ex.Title}
                  </span>
                </div>
              </div>
              <div className="kt-subheader__toolbar">
                <div className="kt-subheader__wrapper">
                  <a
                    href="javascript:"
                    className="btn btn-primary"
                    onClick={() => {
                        this.state.ex.CurrentAddModel = new adminManagerCmsModel();
                      $("#add-attribute-popup input").val("");
                      this.setState(this.state);
                      $("#add-attribute-popup").modal("show");
                    }}
                  >
                    <i className="fa fa-plus" /> {add[this.context.Language]}
                  </a>
                  <a
                    href="javascript:"
                    onClick={e => this.handleDeleteDataRow()}
                    className="btn btn-danger"
                  >
                    <i className="fa fa-trash-alt" />{remove[this.context.Language]}
                  </a>
                </div>
              </div>
            </div>
            <div
              className="kt-content kt-grid__item kt-grid__item--fluid"
              id="kt_content"
            >
              <div className="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                <div className="kt-portlet kt-portlet--mobile">
                  <div className="kt-portlet__head kt-portlet__head--lg">
                    <div className="kt-portlet__head-label">
                      <h3 className="kt-portlet__head-title">
                        {this.state.ex.Title}
                      </h3>
                    </div>
                  </div>

                  <div className="kt-portlet__body">
                    <div
                      id="kt_table_1_wrapper"
                      className="dataTables_wrapper dt-bootstrap4"
                    >
                      <div className="row">
                        <div className="col-sm-12">
                          <table className="table table-striped- table-bordered table-hover table-checkable dataTable no-footer dtr-inline table-responsive">
                            <thead>
                              <tr role="row">
                                <th
                                  className="dt-right sorting_disabled"
                                  rowspan="1"
                                  colspan="1"
                                  style={{ width: "1%" }}
                                  aria-label="Record ID"
                                >
                                  <label className="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                    <input
                                      type="checkbox"
                                      className="kt-group-checkable"
                                      onChange={ev => {
                                        this.state.model.Results = this.state.model.Results.map(
                                          e => {
                                            return {
                                              ...e,
                                              IsChecked: ev.target.checked
                                            };
                                          }
                                        );
                                        this.setState(this.state);
                                      }}
                                    />
                                    <span />
                                  </label>
                                </th>
                                <th>{admin_name[this.context.Language]}</th>
                                <th>{admin_phone[this.context.Language]}</th>
                                <th>{admin_email[this.context.Language]}</th>
                              </tr>
                            </thead>
                            <tbody>
                              {this.state.model.Results.map((c, index) => {
                                return (
                                  <tr>
                                    <td className="dt-right" tabindex="0">
                                      <label className="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                        <input
                                          type="checkbox"
                                          className="kt-checkable"
                                          value={c._id}
                                          checked={c.IsChecked}
                                          onChange={e => {
                                            c.IsChecked = !c.IsChecked;
                                            this.setState(this.state);
                                          }}
                                        />
                                        <span />
                                      </label>
                                    </td>
                                    <td>{c.FirstName}</td>
                                    <td>{c.Phone}</td>
                                    <td>{c.Email}</td>
                                  </tr>
                                );
                              })}
                            </tbody>

                          </table>
                        </div>
                      </div>
                 
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div
              class="modal fade"
              id="add-attribute-popup"
              ref={this.popup}
              tabindex="-1"
              role="dialog"
              aria-labelledby="exampleModalLabel"
              aria-hidden="true"
            >
              <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                      {add_user_admin[this.context.Language]}
                    </h5>
                    <button
                      type="button"
                      class="close"
                      data-dismiss="modal"
                      aria-label="Close"
                    />
                  </div>
                  <div class="modal-body">
                    <div className="form-group">
                      <label className="control-label">{admin_name[this.context.Language]}</label>
                      <input
                        className="form-control"
                        placeholder={admin_name[this.context.Language]}
                        value={this.state.ex.CurrentAddModel.FirstName}
                        onChange={e => {
                          this.state.ex.CurrentAddModel.FirstName = e.target.value;
                        }}
                      />
                    </div>
                    <div className="form-group">
                      <label className="control-label">{admin_phone[this.context.Language]}</label>
                      <input
                        className="form-control"
                        placeholder={admin_phone[this.context.Language]}
                        value={this.state.ex.CurrentAddModel.Phone}
                        onChange={e => {
                          this.state.ex.CurrentAddModel.Phone = e.target.value;
                        }}
                      />
                    </div>
                    <div className="form-group">
                      <label className="control-label">{admin_email[this.context.Language]}</label>
                      <input
                        className="form-control"
                        placeholder={admin_email[this.context.Language]}
                        value={this.state.ex.CurrentAddModel.Email}
                        onChange={e => {
                          this.state.ex.CurrentAddModel.Email = e.target.value;
                        }}
                      />
                    </div>
                    <div className="form-group">
                      <label className="control-label">{password[this.context.Language]}</label>
                      <input
                        type="password"
                        className="form-control"
                        placeholder={password[this.context.Language]}
                        value={this.state.ex.CurrentAddModel.Password}
                        onChange={e => {
                          this.state.ex.CurrentAddModel.Password = e.target.value;
                        }}
                      />
                    </div>
                    <div className="form-group">
                      <label className="control-label">{confirm_password[this.context.Language]}</label>
                      <input
                        type="password"
                        className="form-control"
                        placeholder={confirm_password[this.context.Language]}
                        value={this.state.ex.CurrentAddModel.ConfirmPassword}
                        onChange={e => {
                          this.state.ex.CurrentAddModel.ConfirmPassword = e.target.value;
                        }}
                      />
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button
                      type="button"
                      className="btn btn-default"
                      data-dismiss="modal"
                    >
                      {close[this.context.Language]}
                    </button>
                    <button
                      type="button"
                      className="btn btn-primary"
                      onClick={e => {
                          this.handleSubmitForm();
                      }}
                    >
                      {save[this.context.Language]}
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </React.Fragment>
        ) : (
          <div />
        )}
      </React.Fragment>
    );
  }
}
AdminManager.contextType = AppContext;
export default AdminManager;
