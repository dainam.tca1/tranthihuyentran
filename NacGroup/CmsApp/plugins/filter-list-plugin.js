import { ERROR, DATA_NOTFOUND } from "../constants/message";

/**
 * Plugin dùng trong trang danh sách data, gọi ajax để filter lại danh sách data
 */
class FilterListPlugin {
  /**
   * Gọi ajax filter danh sách trang tĩnh khi đã có URL
   * @param {String} url - Url để gọi ajax
   * @param {Object} cpn - Truyền this của component vào
   */
  static filter(url, cpn) {
    var that = cpn;
    KTApp.blockPage();
    $.ajax({
      url: url,
      type: "GET",
      dataType: "json",
      contentType: "application/json",
      success: response => {
        KTApp.unblockPage();
        if (response.status == "error") {
          toastr["error"](DATA_NOTFOUND, ERROR);
          return -1;
        }
        that.state.model = response.data;
        that.state.model.Results = that.state.model.Results.map(e => {
          return { ...e, IsChecked: false };
        });
        that.setState(that.state);
      },
      error: function(er) {
        KTApp.unblockPage();
        toastr["error"](DATA_NOTFOUND, ERROR);
      }
    });
  }

  /**
   * Lấy tham số và từ các field và tạo url
   * @param {Number} index -- Trang cần lấy
   * @param {Object} cpn -- Truyền this của component vào
   * @param {String} apiurl -- Đường dẫn api
   */
  static toPage(index, cpn, apiurl) {
    var that = cpn;
    var paramStr = "";
    if (that.state.ex.Param != null) {
      for (var key in that.state.ex.Param) {
        if (
          !that.state.ex.Param[key] ||
          that.state.ex.Param[key].toString() === ""
        )
          continue;
        if (paramStr !== "") {
          paramStr += "&";
        }
        paramStr += key + "=" + encodeURIComponent(that.state.ex.Param[key]);
      }
    }
    var url = apiurl + "?" + paramStr + (paramStr ? "&" : "") + "page=" + index;
    return url;
  }

  /**
   * Filter danh sách data
   * @param {Number} index - Trang cần lấy
   * @param {Object} cpn - Truyền this của component vào
   * @param {String} apiurl - Đường dẫn api
   */
  static filterdata(index, cpn, apiurl) {
    var url = this.toPage(index, cpn, apiurl);
    this.filter(url, cpn);
  }

  /**
   * Được gọi khi thay đổi các trường filter cập nhật tham số filter vào state
   * Filter dữ liệu theo tham số mới
   * @param {Event} event
   */
  static handleChangeFilter(event, cpn) {
    var that = cpn;
    const target = event.target;
    const value = target.type === "checkbox" ? target.checked : target.value;
    const name = target.name;
    that.state.ex.Param[name] = value;
    that.setState(that.state);
  }
}

export default FilterListPlugin;
