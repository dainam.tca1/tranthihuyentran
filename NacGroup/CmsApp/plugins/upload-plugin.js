import {
    GLOBAL_ERROR_MESSAGE,
    ERROR,
    SUCCESS
} from "../constants/message";

class UploadPlugin {
    static UpdateImage(event, cback, des) {
        var data = new FormData();
        var files = event.target.files;
        if (files.length > 0) {
            for (var i = 0; i < files.length; i++) {
                data.append("banner", files[i]);
            }
        }
        
        data.append("path", des);
        // console.log(data.des);
        $(event.target).val("");
        $.ajax({
            url: "/cms/api/cmsplugin/UploadImage",
            type: "POST",
            processData: false,
            contentType: false,
            data: data,
            success: function (res) {
                if (res.status === "error") {
                    toastr["error"](res.message, ERROR);
                    return 1;
                }
                cback(res.data);
                toastr["success"](res.message, SUCCESS);
                return 1;
            },
            error: function (er) {
                toastr["error"](GLOBAL_ERROR_MESSAGE, ERROR);
            }
        });
        return 1;
    }
    static ImportExcel(event, cback) {
        var fileExtension = ['xls', 'xlsx'];
        var filename = event.target.files;
        if (filename.length == 0) {
            toastr["error"]("Please select a file.", ERROR);
            return false;
        }
        else {   
            var extension = filename[0].name.replace(/^.*\./, '');
            if ($.inArray(extension, fileExtension) == -1) {
                toastr["error"]("Please select only excel files.", ERROR);
                return false;
            }
        }
        var data = new FormData();
        var files = event.target.files;
        if (files.length > 0) {
            for (var i = 0; i < files.length; i++) {
                data.append("excel", files[i]);
            }
        }
   

        //data.append("path", des);
        //$(event.target).val("");
        KTApp.blockPage();
        $.ajax({
            url: "/cms/api/product/ImportExcel",
            type: "POST",
            processData: false,
            contentType: false,
            data: data,
            success: function (res) {
                if (res.status == "error") {
                    toastr["error"](res.message, ERROR);
                    return 1;
                }
                KTApp.unblockPage();
                cback(res.data);
                
                toastr["success"](res.message, SUCCESS);
                window.location.reload();
                return 1;
            },
            error: function (er) {
                toastr["error"](GLOBAL_ERROR_MESSAGE, ERROR);
            }
        });
        return 1;
    }
}

export default UploadPlugin;
