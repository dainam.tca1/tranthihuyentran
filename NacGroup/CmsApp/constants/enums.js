const THEME_TEXT_TYPE = {
  Normal: 0,
  Html: 1,
  Link: 2
};

const THEMETYPE = {
  Group: 0,
  Image: 1,
  Text: 2,
  ImageList: 3,
  TextImageList: 4
};

const BOOKINGTYPE = {
  Individual: 0,
  Group: 1
};

const GENDERTYPE = {
  Undefine: 0,
  Male: 1,
  Female: 2
};
const DEVICE_STATUS = {
    Inactive: 0,
    Active: 1,
    Warning: 2
};
const PAYMENT_STATUS = {
    CAPTURED: 0,
    AUTHORIZED: 1,
    DECILNED: 2,
    ERROR: 3,
    REVIEW: 4,
    VOIDED: 5,
    UNKNOWN: 6
}
const ORDER_STATUS = {   
    ON_HOLD: 0, 
    PENDING_PAYMENT: 1,
    PAYMENT_RECEIVED: 2,
    PACKAGING: 3,
    ORDER_SHIPPED: 4,
    ORDER_ARCHIVED: 5,
    BACK_ORDER: 6,
    CANCELED: 7,
}
module.exports = {
    THEMETYPE,
    THEME_TEXT_TYPE,
    BOOKINGTYPE,
    GENDERTYPE,
    DEVICE_STATUS,
    PAYMENT_STATUS,
    ORDER_STATUS
};
