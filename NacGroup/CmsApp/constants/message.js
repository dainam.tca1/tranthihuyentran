//#region Thông báo
const GLOBAL_ERROR_MESSAGE = "Opps! System has some error. Please try again!";
const globalErrorMessage = [
  "Opps! System has some error",
  "Rất tiếc hệ thống xảy ra lỗi"
];


const PARAMETER_INVALID = "Parameters is invalable";
const DATA_UPDATE_SUCCESSFULL = "Update successfully";
const DATA_UPDATE_IMG = "Upload image successfully";
const DATA_NOTFOUND = "Data is not found";
const DATA_NOT_RECOVER = "Data won't be recovered?";
const DATA_NOT_SELECT = "Please select data need to remove";
const DATA_LOADING_SUCCESSFULL = "Success";
const DATA_CHANGE_PLAYLIST = "Do you want all devices to run 1 play list";
//#endregion
//#region validate CategoryBlog
const VALID_TITLE_CATE = "Vui lòng nhập tên chuyên mục";
const VALID_URL = "Vui lòng nhập URL";
const VALID_SELECT_CATE = "Vui lòng chọn danh mục";
const VALID_URL_EXIST = "URL đã tồn tại vui lòng thử lại";
const VALID_PRICE_SERVICE = "Vui lòng nhập giá dịch vụ";
const VALID_TITLE_SERVICE = "Vui lòng nhập tên dịch vụ";
const VALID_TIME_SERVICE = "Vui lòng nhập thời gian dịch vụ";
const VALID_TIME = "Thời gian phải là số";
const VALID_PRICE = "Tiền phải là số";
const VALID_NAME = "Vui lòng bạn nhập họ và tên";
const VALID_SELECT_SERVICE = "Vui lòng chọn nghiệp vụ nhân viên";
//#endregion
//#region Text UI
const ERROR = "Error";
const SUCCESS = "Success";
const error = ["Error", "Lỗi"];
const success = ["Success", "Thành công"];
const NOTI = "Notification";
const noti = ["Notification","Thông báo"];
const back = ["Back", "Quay lại"];
const save = ["Save", "Lưu lại"];
const total = ["Total", "Tổng cộng"];
const record = ["Records", "Kết quả"];
const add = ["Add", "Thêm"];
const remove = ["Remove", "Xóa"];
const page = ["Page", "trang"];
const cDate = ["Create Date", "Ngày tạo"];
const lDate = ["Last Update", "Lần cập nhật cuối"];
const recordPerPage = ["Records Per Page", "Kết quả trên trang"];
const close = ["Close", "Đóng"];
const savechange = ["Save Changes", "Lưu thay đổi"];
const phone = ["Phone", "Số điện thoại"];
const name = ["Name", "Tên"];
const status = ["Status", "Trạng thái"];
const se_status = ["--- Select Status ---", "--- Chọn trạng thái ---"];
const order = ["Order", "Thứ tự"];
const sort = ["Sort", "Sắp xếp"];
const are_you_sure = ["Are you sure?", "Bạn có chắc chắn không"];
const yes_de = ["Yes, delete it", "Vâng, xóa nó đi"];
const no_ca = ["No, cancel", "Không, hủy bỏ"];
const waiting = ["Waiting", "Đang đợi"];
const paid = ["Paid", "Thanh toán"];
const processing = ["Processing", "Đang xử lí"];
const s_delivery = ["Start Delivery", "Chuẩn bị giao"];
const complete = ["Complete", "Hoàn thành"];
const cancel = ["Cancel", "Hủy bỏ"];
const f_date = ["From Date", "Từ ngày"];
const t_date = ["To Date", "Đến ngày"];
const data_notfound = ["Data Not Found", "Không tìm thấy dữ liệu"];
const data_recover = ["Data Not Recover", "Dữ liệu không thể phục hồi"];
const qty = ["Quantity", "Số lượng"];
const price = ["Price", "Giá"];
const data_update_success = ["Data Update Successfully", "Cập nhật dữ liệu thành công"];
const clearcache = [" Clear Cache", "Xóa bộ nhớ cache"];
//#endregion

//#region Message giỏ hàng
const QUANTITY_MORETHAN_ZERO = "Quantity must be greater than zero";
//#endregion
module.exports = {
    globalErrorMessage,
    VALID_NAME,
    VALID_SELECT_SERVICE,
    VALID_TIME,
    VALID_PRICE,
    VALID_TITLE_SERVICE,
    VALID_PRICE_SERVICE,
    VALID_SELECT_CATE,
    VALID_TIME_SERVICE,
    GLOBAL_ERROR_MESSAGE,
    PARAMETER_INVALID,
    DATA_NOT_RECOVER,
    DATA_NOT_SELECT,
    DATA_UPDATE_SUCCESSFULL,
    DATA_UPDATE_IMG,
    DATA_LOADING_SUCCESSFULL,
    DATA_CHANGE_PLAYLIST,
    VALID_URL_EXIST,
    VALID_URL,
    VALID_TITLE_CATE,
    DATA_NOTFOUND,
    ERROR,
    SUCCESS,
    NOTI,
    success,
    error,
    QUANTITY_MORETHAN_ZERO,
    back,
    save,
    total,
    record,
    add,
    remove,
    page,
    cDate,
    lDate,
    recordPerPage,
    close,
    savechange,
    clearcache,
    noti,
    phone,
    name,
    status,
    order,
    sort,
    are_you_sure,
    yes_de,
    no_ca,
    se_status,
    waiting,
    paid,
    processing,
    s_delivery,
    complete,
    cancel,
    f_date,
    t_date,
    data_notfound,
    qty,
    price,
    data_recover,
    data_update_success
};
