﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Loader;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApplicationParts;
using Microsoft.Extensions.DependencyInjection;
using NacGroup.Module.Core.Interfaces;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Core.Services;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;

namespace NacGroup.Extensions
{
    public static class ServiceCollectionExtensions
    {
        private static readonly IModuleConfigurationManager ModulesConfig = new ModuleConfigurationManager();
        private static string moduleManifestName = "module.json";
        public static IServiceCollection RegisterModules(this IServiceCollection services, IHostingEnvironment hostingEnvironment)
        {
            var modulesFolder = Path.Combine(hostingEnvironment.ContentRootPath, "Modules");
            foreach (var module in ModulesConfig.GetModules(hostingEnvironment.ContentRootPath))
            {
                var moduleFolder = new DirectoryInfo(Path.Combine(modulesFolder, module.Id));
                var moduleManifestPath = Path.Combine(moduleFolder.FullName, moduleManifestName);
                if (!File.Exists(moduleManifestPath))
                {
                    throw new Exception($"The manifest for the module '{moduleFolder.Name}' is not found.");
                }

                using (var reader = new StreamReader(moduleManifestPath))
                {
                    string content = reader.ReadToEnd();
                    dynamic moduleMetadata = JsonConvert.DeserializeObject(content);
                    module.Name = moduleMetadata.name;
                    module.CopyFolders = ((JArray)moduleMetadata.copyFolders).ToObject<string[]>();
                    module.Id = moduleMetadata.id;
                    module.IsBundledWithHost = Convert.ToBoolean(moduleMetadata.isBundledWithHost);
                }
                if (module.IsBundledWithHost)
                {
                    module.Assembly = Assembly.Load(new AssemblyName(module.Id));
                }
                else
                {
                    TryLoadModuleAssembly(moduleFolder.FullName, module);
                    if (module.Assembly == null)
                    {
                        throw new Exception($"Cannot find main assembly for module {module.Id}");
                    }
                }
              
                ModuleManager.Modules.Add(module);
                RegisterModuleInitializerServices(module, ref services);
            }
            return services;
        }

        /// <summary>
        /// Đăng ký ModuleInitializer trong mỗi module
        /// </summary>
        /// <param name="module"></param>
        /// <param name="services"></param>
        private static void RegisterModuleInitializerServices(ModuleInfo module, ref IServiceCollection services)
        {
            var moduleInitializerType = module.Assembly.GetTypes()
                    .FirstOrDefault(t => typeof(IModuleInitializer).IsAssignableFrom(t));
            if ((moduleInitializerType != null) && (moduleInitializerType != typeof(IModuleInitializer)))
            {
                services.AddSingleton(typeof(IModuleInitializer), moduleInitializerType);
            }
        }

        private static void TryLoadModuleAssembly(string moduleFolderPath, ModuleInfo module)
        {
            const string binariesFolderName = "bin";
            var binariesFolderPath = Path.Combine(moduleFolderPath, binariesFolderName);
            var binariesFolder = new DirectoryInfo(binariesFolderPath);

            if (Directory.Exists(binariesFolderPath))
            {
                foreach (var file in binariesFolder.GetFileSystemInfos("*.dll", SearchOption.AllDirectories))
                {
                    Assembly assembly;
                    try
                    {
                        assembly = AssemblyLoadContext.Default.LoadFromAssemblyPath(file.FullName);
                    }
                    catch (FileLoadException)
                    {
                        // Get loaded assembly. This assembly might be loaded
                        assembly = Assembly.Load(new AssemblyName(Path.GetFileNameWithoutExtension(file.Name)));

                        if (assembly == null)
                        {
                            throw;
                        }

                        //string loadedAssemblyVersion = FileVersionInfo.GetVersionInfo(assembly.Location).FileVersion;
                        //string tryToLoadAssemblyVersion = FileVersionInfo.GetVersionInfo(file.FullName).FileVersion;

                        //// Or log the exception somewhere and don't add the module to list so that it will not be initialized
                        //if (tryToLoadAssemblyVersion != loadedAssemblyVersion)
                        //{
                        //    throw new Exception($"Cannot load {file.FullName} {tryToLoadAssemblyVersion} because {assembly.Location} {loadedAssemblyVersion} has been loaded");
                        //}
                    }

                    if (Path.GetFileNameWithoutExtension(assembly.ManifestModule.Name) == module.Id)
                    {
                        module.Assembly = assembly;
                    }
                }
            }
        }

        public static IServiceCollection AddCustomizedMvc(this IServiceCollection services, IList<ModuleInfo> modules)
        {
            var mvcbuilder = services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
                .AddJsonOptions(options => options.SerializerSettings.ContractResolver = new DefaultContractResolver());

            foreach (var module in modules)
            {
                AddApplicationPart(mvcbuilder, module.Assembly);
            }

            return services;
        }

        private static void AddApplicationPart(IMvcBuilder mvcBuilder, Assembly assembly)
        {
            var partFactory = ApplicationPartFactory.GetApplicationPartFactory(assembly);
            foreach (var part in partFactory.GetApplicationParts(assembly))
            {
                mvcBuilder.PartManager.ApplicationParts.Add(part);
            }

            var relatedAssemblies = RelatedAssemblyAttribute.GetRelatedAssemblies(assembly, throwOnError: false);
            foreach (var relatedAssembly in relatedAssemblies)
            {
                partFactory = ApplicationPartFactory.GetApplicationPartFactory(relatedAssembly);
                foreach (var part in partFactory.GetApplicationParts(relatedAssembly))
                {
                    mvcBuilder.PartManager.ApplicationParts.Add(part);
                }
            }
        }

        public static void CopyModulesToHost(string contentRootPath, string buildConfigurationName = "Debug")
        {
            var modulelist = ModulesConfig.GetModules(contentRootPath);
            var workFolder = @"../"; //Thu mục Solutions
            var moduleBuildPath = Path.Combine(workFolder, "NacGroup/Modules");

            #region Tạo thư mục Modules trong project host
            if (Directory.Exists(moduleBuildPath))
            {
                try
                {
                    Directory.Delete(moduleBuildPath, true);
                    Directory.CreateDirectory(moduleBuildPath);
                }
                catch
                {
                    //ignored
                }
            }
            else
            {
                Directory.CreateDirectory(moduleBuildPath);
            }

            #endregion


            var moduleDevPath = Path.Combine(workFolder, "Modules");

            foreach (var module in modulelist)
            {
                var modulefolder = Path.Combine(moduleDevPath, module.Id);

                #region Mở file moduleConfig trong project module để get danh sách thư mục cần copy code
                var moduleConfigJsonFilePath = Path.Combine(modulefolder, moduleManifestName);
                using (var reader = new StreamReader(moduleConfigJsonFilePath))
                {
                    string content = reader.ReadToEnd();
                    dynamic moduleMetadata = JsonConvert.DeserializeObject(content);
                    module.CopyFolders = ((JArray)moduleMetadata.copyFolders).ToObject<string[]>();
                    module.IsBundledWithHost = Convert.ToBoolean(moduleMetadata.isBundledWithHost);
                }    
                #endregion

                #region Tạo thư mục ở project host
                var modulePath = Path.Combine(moduleBuildPath, module.Id);
                Directory.CreateDirectory(modulePath);
                #endregion

                #region Copy file module.json
                FileInfo modulejsonfile = new FileInfo(moduleConfigJsonFilePath);
                string temppath = Path.Combine(modulePath, modulejsonfile.Name);
                modulejsonfile.CopyTo(temppath, true);
                #endregion


                foreach (var folder in module.CopyFolders)
                {
                    var copyPath = folder.Trim();
                    var sourcePath = Path.Combine(modulefolder, copyPath);
                    if (!Directory.Exists(sourcePath))
                        continue;
                    var desPath = string.Empty;

                    switch (copyPath.ToLower())
                    {
                        case "bin":
                            if (module.IsBundledWithHost) break;
                            var binsourceFolder = Path.Combine(sourcePath, buildConfigurationName, "netcoreapp2.2");
                            var files = Directory.GetFiles(binsourceFolder, module.Id + "*.dll", SearchOption.AllDirectories);
                            var binPath = Path.Combine(modulePath, "bin");
                            Directory.CreateDirectory(binPath);

                            foreach (var item in files)
                            {
                                desPath = Path.Combine(binPath, Path.GetFileName(item));
                                File.Copy(item, desPath, true);
                            }
                            break;

                        case "wwwroot":
                            desPath = Path.Combine(workFolder, "NacGroup/wwwroot");
                            DirectoryCopy(sourcePath, desPath, true);
                            break;

                        default:
                            desPath = Path.Combine(modulePath, copyPath);
                            DirectoryCopy(sourcePath, desPath, true);
                            break;
                    }
                }
            }

        }

        private static void DirectoryCopy(string sourceDirName, string destDirName, bool copySubDirs)
        {
            // Get the subdirectories for the specified directory.
            DirectoryInfo dir = new DirectoryInfo(sourceDirName);

            if (!dir.Exists)
            {
                throw new DirectoryNotFoundException(
                    "Source directory does not exist or could not be found: "
                    + sourceDirName);
            }

            DirectoryInfo[] dirs = dir.GetDirectories();
            // If the destination directory doesn't exist, create it.
            if (!Directory.Exists(destDirName))
            {
                Directory.CreateDirectory(destDirName);
            }

            // Get the files in the directory and copy them to the new location.
            FileInfo[] files = dir.GetFiles();
            foreach (FileInfo file in files)
            {
                string temppath = Path.Combine(destDirName, file.Name);
                file.CopyTo(temppath, true);
            }

            // If copying subdirectories, copy them and their contents to new location.
            if (copySubDirs)
            {
                foreach (DirectoryInfo subdir in dirs)
                {
                    string temppath = Path.Combine(destDirName, subdir.Name);
                    DirectoryCopy(subdir.FullName, temppath, copySubDirs);
                }
            }
        }

    }

}
