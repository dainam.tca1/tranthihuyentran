﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Razor;
using NacGroup.Module.Core.Services;
using Microsoft.Extensions.DependencyInjection;
using NacGroup.Module.Core.Interfaces;

namespace NacGroup.Extensions
{
    public class ThemeableViewLocationExpander : IViewLocationExpander
    {
        private const string ThemeKey = "WebsiteTheme";
        private const string TenantKey = "TernantName";
        public IEnumerable<string> ExpandViewLocations(ViewLocationExpanderContext context, IEnumerable<string> viewLocations)
        {
            if (!string.IsNullOrWhiteSpace(context.Values[ThemeKey]))
            {
                var moduleViewLocations = new string[]
                {
                    $"/Themes/{context.Values[ThemeKey]}/Areas/{{2}}/Views/{{1}}/{{0}}.cshtml",
                    $"/Themes/{context.Values[ThemeKey]}/Areas/{{2}}/Views/Shared/{{0}}.cshtml",
                    $"/Themes/{context.Values[ThemeKey]}/Views/{{1}}/{{0}}.cshtml",
                    $"/Themes/{context.Values[ThemeKey]}/Views/Shared/{{0}}.cshtml"
                };
                viewLocations = moduleViewLocations.Concat(viewLocations);
            }
            if (!string.IsNullOrWhiteSpace(context.Values[TenantKey]))
            {
                var moduleViewLocations = new string[]
                {
                    $"/Tenants/{context.Values[TenantKey]}/Areas/{{2}}/Views/{{1}}/{{0}}.cshtml",
                    $"/Tenants/{context.Values[TenantKey]}/Areas/{{2}}/Views/Shared/{{0}}.cshtml",
                    $"/Tenants/{context.Values[TenantKey]}/Views/{{1}}/{{0}}.cshtml",
                    $"/Tenants/{context.Values[TenantKey]}/Views/Shared/{{0}}.cshtml"
                };
                viewLocations = moduleViewLocations.Concat(viewLocations);
            }

            return viewLocations;
        }

        public void PopulateValues(ViewLocationExpanderContext context)
        {
            var setting = context.ActionContext.HttpContext.RequestServices.GetService<ITenantProvider>();
            var ternant = setting.GetTenant();
            context.Values[ThemeKey] = ternant.ActiveTheme;
            context.Values[TenantKey] = ternant.TenantName;
            //var controllerName = context.ActionContext.ActionDescriptor.DisplayName;
            //if (controllerName == null) // in case of render view to string
            //{
            //    return;
            //}

            //context.ActionContext.HttpContext.Request.Cookies.TryGetValue(ThemeKey, out string previewingTheme);
            //if (!string.IsNullOrWhiteSpace(previewingTheme))
            //{
            //    context.Values[ThemeKey] = previewingTheme;
            //}
            //else
            //{
            //    var config = context.ActionContext.HttpContext.RequestServices.GetService<IConfiguration>();
            //    context.Values[ThemeKey] = config[ThemeKey];
            //}
        }
    }
}
