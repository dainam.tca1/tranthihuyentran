﻿using System;
using System.Globalization;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

namespace NacGroup.Infrastructure.Shared
{
    public static class StringHelper
    {


        private static string ComputeHash<T>(object instance, T cryptoServiceProvider) where T : HashAlgorithm, new()
        {
            using (MemoryStream memoryStream = new MemoryStream())
            {
                var formatter = new BinaryFormatter();
                formatter.Serialize(memoryStream, instance);
                cryptoServiceProvider.ComputeHash(memoryStream.ToArray());
                return Convert.ToBase64String(cryptoServiceProvider.Hash);
            }
        }

        public static string GetHash<T>(this object instance) where T : HashAlgorithm, new()
        {
            T cryptoServiceProvider = new T();
            return ComputeHash(instance, cryptoServiceProvider);
        }

        /// <summary>
        /// Lấy mã MD5 của 1 chuỗi
        /// </summary>
        /// <param name="instance"></param>
        /// <returns></returns>
        public static string GetMd5Hash(this object instance) => instance.GetHash<MD5CryptoServiceProvider>();

        /// <summary>
        /// Chuyển một số về dạng tiền tệ
        /// </summary>
        /// <param name="number">Giá trị cần chuyển (int, decimal, double...)</param>
        /// <returns>String dạng tiền tệ</returns>
        public static string ToCurrency(this object number, int currencyType = 0)
        {
            return string.IsNullOrEmpty(number.FormatNumber("{0:c}",currencyType)) ? "0" : number.FormatNumber("{0:c}", currencyType);
        }
  

        private static string FormatNumber(this object value, string format, int currencyType)
        {
            if (!value.IsNumericType())
                throw new ArgumentException("\"" + value + "\" is not a number.");

            var cultureInfo = (CultureInfo)CultureInfo.GetCultureInfo("en-US").Clone();

            switch (currencyType)
            {
                case 0:
                    cultureInfo = (CultureInfo)CultureInfo.GetCultureInfo("en-US").Clone();
                    cultureInfo.NumberFormat.CurrencyDecimalDigits = 2;
                    cultureInfo.NumberFormat.CurrencyPositivePattern = 0;
                    cultureInfo.NumberFormat.CurrencyNegativePattern = 1;
                    break;
                case 1:
                    cultureInfo = (CultureInfo)CultureInfo.GetCultureInfo("vi-VN").Clone();
                    cultureInfo.NumberFormat.CurrencyDecimalDigits = 0;
                    cultureInfo.NumberFormat.CurrencyPositivePattern = 3;
                    cultureInfo.NumberFormat.CurrencyNegativePattern = 8;
                    break;
            }

            var result = string.Format(cultureInfo, format, value);
            if (result.StartsWith("0"))
                return string.Empty;
            return result;
        }


        public static string ToNewsDateFormat(this DateTime value)
        {
            var timespan = DateTime.Now - value;
            if (timespan.TotalSeconds < 60 && timespan.TotalSeconds > 0)
                return $"{(int)(timespan.TotalSeconds)} giây trước";
            if (timespan.TotalMinutes < 60)
                return $"{(int)timespan.TotalMinutes} phút trước";
            if (timespan.TotalHours < 24)
                return $"{(int)timespan.TotalHours} giờ trước";
            if (timespan.TotalDays < 4)
                return $"{(int)timespan.TotalDays} ngày trước";
            return value.ToString("dd/MM/yyyy");
        }

        #region Validate string
        /// <summary>
        /// Kiểm tra một object có phải là kiểu số hay không
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        public static bool IsNumericType(this object o)
        {
            switch (Type.GetTypeCode(o.GetType()))
            {
                case TypeCode.Byte:
                case TypeCode.SByte:
                case TypeCode.UInt16:
                case TypeCode.UInt32:
                case TypeCode.UInt64:
                case TypeCode.Int16:
                case TypeCode.Int32:
                case TypeCode.Int64:
                case TypeCode.Decimal:
                case TypeCode.Double:
                case TypeCode.Single:
                    return true;
                default:
                    return false;
            }
        }

        public static bool IsPhoneNumber(this string phone)
        {
            return true;
            //if (string.IsNullOrEmpty(phone))
            //    return false;
            //const string phonePattern = @"^((09(\d){8})|(01(\d){9})|(03(\d){8})|(07(\d){8})|(05(\d){8})|(08(\d){8}))$";
            //return Regex.IsMatch(phone.Trim(), phonePattern);
        }

        public static bool IsEmail(this string email)
        {
            if (string.IsNullOrEmpty(email))
                return false;
            const string sMailPattern = @"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*";
            return Regex.IsMatch(email.Trim(), sMailPattern);
        }

        public static bool IsValidCustomerName(this string name)
        {
            if (string.IsNullOrEmpty(name))
                return false;

            name = name.Trim();
            var requirePattern = @"^([^\\x00-\\x7F]|[_a-zA-Z]|[\s])([_a-zA-Z0-9]|[\s]|[^\\x00-\\x7F])+$";
            if (!Regex.IsMatch(name, requirePattern))
                return false;

            var pattern = "^([^\\x00-\\x7F]|[\\w_\\ \\.\\+\\-]){2,50}$";
            return Regex.IsMatch(name, pattern);
        }


        #endregion

        /// <summary>
        /// Lấy dạng URL của một chuỗi bất kỳ
        /// </summary>
        /// <param name="phrase">Chuỗi cần chuyển thành URl</param>
        /// <returns>Chuối dạng URL</returns>
        public static string ToUrl(this string title, bool remapToAscii = true, int maxlength = 150)
        {
            if (title == null)
            {
                return string.Empty;
            }

            int length = title.Length;
            bool prevdash = false;
            StringBuilder stringBuilder = new StringBuilder(length);
            char c;

            for (int i = 0; i < length; ++i)
            {
                c = title[i];
                if ((c >= 'a' && c <= 'z') || (c >= '0' && c <= '9'))
                {
                    stringBuilder.Append(c);
                    prevdash = false;
                }
                else if (c >= 'A' && c <= 'Z')
                {
                    // tricky way to convert to lower-case
                    stringBuilder.Append((char)(c | 32));
                    prevdash = false;
                }
                else if ((c == ' ') || (c == ',') || (c == '.') || (c == '/') ||
                  (c == '\\') || (c == '-') || (c == '_') || (c == '='))
                {
                    if (!prevdash && (stringBuilder.Length > 0))
                    {
                        stringBuilder.Append('-');
                        prevdash = true;
                    }
                }
                else if (c >= 128)
                {
                    int previousLength = stringBuilder.Length;

                    if (remapToAscii)
                    {
                        stringBuilder.Append(RemapInternationalCharToAscii(c));
                    }
                    else
                    {
                        stringBuilder.Append(c);
                    }

                    if (previousLength != stringBuilder.Length)
                    {
                        prevdash = false;
                    }
                }

                if (i == maxlength)
                {
                    break;
                }
            }

            if (prevdash)
            {
                return stringBuilder.ToString().Substring(0, stringBuilder.Length - 1);
            }
            else
            {
                return stringBuilder.ToString();
            }
        }

        /// <summary>
        /// Remaps the international character to their equivalent ASCII characters. See
        /// http://meta.stackexchange.com/questions/7435/non-us-ascii-characters-dropped-from-full-profile-url/7696#7696
        /// </summary>
        /// <param name="character">The character to remap to its ASCII equivalent.</param>
        /// <returns>The remapped character</returns>
        private static string RemapInternationalCharToAscii(char character)
        {
            string s = character.ToString().ToLowerInvariant();
            if ("àåáâäãåąā".Contains(s))
            {
                return "a";
            }
            else if ("èéêëę".Contains(s))
            {
                return "e";
            }
            else if ("ìíîïı".Contains(s))
            {
                return "i";
            }
            else if ("òóôõöøő".Contains(s))
            {
                return "o";
            }
            else if ("ùúûüŭů".Contains(s))
            {
                return "u";
            }
            else if ("çćčĉ".Contains(s))
            {
                return "c";
            }
            else if ("żźž".Contains(s))
            {
                return "z";
            }
            else if ("śşšŝ".Contains(s))
            {
                return "s";
            }
            else if ("ñń".Contains(s))
            {
                return "n";
            }
            else if ("ýÿ".Contains(s))
            {
                return "y";
            }
            else if ("ğĝ".Contains(s))
            {
                return "g";
            }
            else if (character == 'ř')
            {
                return "r";
            }
            else if (character == 'ł')
            {
                return "l";
            }
            else if ("đð".Contains(s))
            {
                return "d";
            }
            else if (character == 'ß')
            {
                return "ss";
            }
            else if (character == 'Þ')
            {
                return "th";
            }
            else if (character == 'ĥ')
            {
                return "h";
            }
            else if (character == 'ĵ')
            {
                return "j";
            }
            else
            {
                return string.Empty;
            }
        }

        public static string Renameimg(this string phrase)
        {
            string str = phrase.Replace(",", " ").ToUnsignedVietnamese().RemoveAccent().ToLower();

            str = Regex.Replace(str, @"[^a-z0-9\s-/?:\.]", ""); // invalid chars           
            str = Regex.Replace(str, @"\s+", " ").Trim(); // convert multiple spaces into one space   
            //str = str.Substring(0, str.Length <= 150 ? str.Length : 150).Trim(); // cut and trim it   
            str = Regex.Replace(str, @"\s", "-"); // hyphens   
            str = str.Replace("?", "");

            return str;
        }

      


        /// <summary>
        /// Viết hoa các chữ cái đầu của mỗi từ trong một văn bản
        /// </summary>
        /// <param name="value">Chuỗi cần viết hoa</param>
        /// <returns>Chuỗi đã viết hoa</returns>
        public static string ToUpperWords(this string value)
        {
            char[] array = value.ToCharArray();
            // Handle the first letter in the string.
            if (array.Length >= 1)
            {
                if (char.IsLower(array[0]))
                {
                    array[0] = char.ToUpper(array[0]);
                }
            }
            // Scan through the letters, checking for spaces.
            // ... Uppercase the lowercase letters following spaces.
            for (int i = 1; i < array.Length; i++)
            {
                if (array[i - 1] == ' ')
                {
                    if (char.IsLower(array[i]))
                    {
                        array[i] = char.ToUpper(array[i]);
                    }
                }
            }
            return new string(array);
        }

        /// <summary>
        /// Chuyển mãi về chuẩn ASCII để đảm bảo loại tất cả các dấu đặt biệt
        /// </summary>
        /// <param name="txt">Chuỗi cần chuyển</param>
        /// <returns>Chuỗi đã mã hóa</returns>
        public static string RemoveAccent(this string txt)
        {
            byte[] bytes = Encoding.GetEncoding("Cyrillic").GetBytes(txt);
            return Encoding.ASCII.GetString(bytes).Replace("-", " ").Replace("/", " ");
        }

        /// <summary>
        /// Bỏ dấu tiếng Việt của một chuỗi bất kỳ
        /// </summary>
        /// <param name="vietnamese">Chuỗi tiếng Việt cần bỏ dấu</param>
        /// <returns>Chuỗi đã khử dấu</returns>
        public static string ToUnsignedVietnamese(this string vietnamese)
        {
            if (vietnamese == null) return string.Empty;
            Regex regex = new Regex("\\p{IsCombiningDiacriticalMarks}+");
            string temp = vietnamese.Normalize(NormalizationForm.FormD);
            return regex.Replace(temp, string.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D');
        }

        /// <summary>
        /// Lấy tên từ họ và tên (tiếng việt)
        /// </summary>
        /// <param name="vietnamese">họ và tên</param>
        /// <returns>Chuỗi đã khử dấu</returns>
        public static string ToFirstName(this string vietnamese)
        {
            var a = vietnamese.Trim().Split(' ');
            return a[a.Length - 1];
        }


        public static string Base64Encode(this string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        public static string Base64Decode(this string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }

        /// <summary>
        /// Tạo ngẫu nhiên một mật khẩu
        /// </summary>
        /// <param name="type">
        /// <para>0: Mật khẩu chỉ bao gồm số</para>
        /// <para>1: Mật khẩu bao gồm ký tự và số</para>
        /// </param>
        /// <param name="length">Độ dài mật khẩu</param>
        /// <returns></returns>
        public static string GeneratePassword(int type = 0, int length = 4)
        {
            string alphabets = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            string small_alphabets = "abcdefghijklmnopqrstuvwxyz";
            string numbers = "1234567890";

            string characters = numbers;
            if (type == 1)
            {
                characters += alphabets + small_alphabets + numbers;
            }
            string otp = string.Empty;
            for (int i = 0; i < length; i++)
            {
                string character;
                do
                {
                    int index = new Random().Next(0, characters.Length);
                    character = characters.ToCharArray()[index].ToString();
                } while (otp.IndexOf(character, StringComparison.Ordinal) != -1);
                otp += character;
            }
            return otp;
        }

        public static string GenerateNumber(int length = 6)
        {
            var random = new  Random();
            return Math.Floor(Math.Pow(10, length - 1)+ 9 * Math.Pow(10, length - 1) * random.NextDouble()).ToString(CultureInfo.InvariantCulture);
        }

        public static string HmacSha1(string key, string dataToSign)
        {
            byte[] secretBytes = Encoding.UTF8.GetBytes(key);
            var hmac = new HMACSHA1(secretBytes);

            byte[] dataBytes = Encoding.UTF8.GetBytes(dataToSign);
            byte[] calcHash = hmac.ComputeHash(dataBytes);
            String calcHashString = Convert.ToBase64String(calcHash);
            return calcHashString;
        }

        public static string ToPhoneFormat(this string str)
        {
            if (str.Length != 10)
            {
                return str;
            }
            var number = str.Substring(0, 3) + "-" + str.Substring(3, 3) + "-" + str.Substring(6,4);
            return number;
        }
    }

}
