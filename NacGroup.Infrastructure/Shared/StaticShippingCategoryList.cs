﻿namespace NacGroup.Infrastructure.Shared
{
    public static class StaticShippingCategoryList
    {
        public static FieldString[] ShippingCategoryList = {
            new FieldString { label = "Fashion", value = "fashion" },
            new FieldString { label = "Mobile", value = "mobiles" },
            new FieldString { label = "Tablets", value = "tablets" },
            new FieldString { label = "Computers Laptops", value = "computers_laptops" },
            new FieldString { label = "Cameras", value = "cameras" },
            new FieldString { label = "Accessory No Battery", value = "accessory_no_battery" },
            new FieldString { label = "Accessory Battery", value = "accessory_battery" },
            new FieldString { label = "Health Beauty", value = "health_beauty" },
            new FieldString { label = "Watches", value = "watches" },
            new FieldString { label = "Home Appliances", value = "home_appliances" },
            new FieldString { label = "Home Decor", value = "home_decor" },
            new FieldString { label = "Toys", value = "toys" },
            new FieldString { label = "Sport", value = "sport" },
            new FieldString { label = "Luggage", value = "luggage" },
            new FieldString { label = "Audio Video", value = "audio_video" },
            new FieldString { label = "Documents", value = "documents" },
            new FieldString { label = "Jewelry", value = "jewelry" },
            new FieldString { label = "Dry Food Supplements", value = "dry_food_supplements" },
            new FieldString { label = "Books Collectionables", value = "books_collectionables" },
            new FieldString { label = "Pet Accessory", value = "pet_accessory" },
            new FieldString { label = "Gaming", value = "gaming" },
        };
    }

    public static class ProductType
    {
        public static FieldInt[] ProductTypeList = {
            new  FieldInt { label = "Simple", value = (int)ProductTypeDefine.Simple },
            new  FieldInt { label = "Collection", value = (int)ProductTypeDefine.Collection},
        };
    }
    public static class Dimension
    {
        public static FieldInt[] DimensionList = {
            new  FieldInt { label = "Inch", value =  (int)DimensionUnit.Inch},
            new  FieldInt { label = "Feet", value = (int)DimensionUnit.Feet },
            new  FieldInt { label = "Milimeter", value = (int)DimensionUnit.Milimeter },
            new  FieldInt { label = "Centimeter", value = (int)DimensionUnit.Milimeter },
            new  FieldInt { label = "Meter", value = (int)DimensionUnit.Meter},
        };
        public static FieldInt[] WeightList = {
            new  FieldInt { label = "Oz", value =  (int)WeightUnit.Oz},
            new  FieldInt { label = "Pound", value = (int)WeightUnit.Pound },
            new  FieldInt { label = "Gram", value = (int)WeightUnit.Gram },
            new  FieldInt { label = "Kilogram", value = (int)WeightUnit.Kilogram},
        };
    }

    public enum ProductTypeDefine
    {
        Simple,
        Collection
    }

    public enum DimensionUnit
    {
        Inch,
        Feet,
        Milimeter,
        Centimeter,
        Meter
    }
    public enum WeightUnit
    {
        Oz,
        Pound,
        Gram,
        Kilogram
    }
    public class FieldString
    {
        public string label { get; set; }
        public string value { get; set; }
    }

    public class FieldInt
    {
        public string label { get; set; }
        public int value { get; set; }
    }

}
