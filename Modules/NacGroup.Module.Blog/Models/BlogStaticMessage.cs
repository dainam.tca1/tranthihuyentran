﻿using System.Collections.Generic;

namespace NacGroup.Module.Blog.Models
{
    public static class BlogStaticMessage
    {
        public static List<string> PleaseRemoveAllBlogInCurrentCategory = new List<string>
        {
            "Please remove all blogs in current category",
            "Vui lòng xóa tất cả bài viết trong chuyên mục hiện tại"
        }; 
        public static List<string> PleaseRemoveChildrenCategory = new List<string>
        {
            "Please remove all children of current category",
            "Vui lòng xóa chuyên mục con của chuyên mục hiện tại"
        };
        public static List<string> CategoryNameIsRequired = new List<string>
        {
            "Category name is required",
            "Vui lòng nhập tên chuyên mục"
        };
        public static List<string> CategoryUrlIsRequired = new List<string>
        {
            "Category url is required",
            "Vui lòng nhập url chuyên mục"
        };
        public static List<string> CategoryUrlIsExist = new List<string>
        {
            "Category url is exist",
            "Url chuyên mục đã tồn tại"
        };
        public static List<string> ParentCategoryNotFound = new List<string>
        {
            "Parent category is not found",
            "Không tìm thấy chuyên mục cha"
        };
        public static List<string> ParentCategoryLevelMustLowerCurrentCategory = new List<string>
        {
            "Parent category level must lower than current category",
            "Chuyên mục cha phải có cấp thấp hơn chuyên mục hiện tại"
        };
        public static List<string> ParentCategoryLevelMustDifferentCurrentCategory = new List<string>
        {
            "Parent category level must different from current category",
            "Chuyên mục cha phải có cấp khác với chuyên mục hiện tại"
        };
        public static List<string> BlogTitleRequired = new List<string>
        {
            "Blog title is required",
            "Vui lòng nhập tiêu đề bài viết"
        };
        public static List<string> BlogUrlRequired = new List<string>
        {
            "Blog url is required",
            "Vui lòng nhập url bài viết"
        };
        public static List<string> BlogCategoryRequired = new List<string>
        {
            "Blog category is required",
            "Vui lòng chọn category"
        };
        public static List<string> BlogUrlIsExist = new List<string>
        {
            "Blog Url is exist",
            "Url bài viết đã tồn tại"
        };
       
    }
}
