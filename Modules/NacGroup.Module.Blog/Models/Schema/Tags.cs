﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using NacGroup.Module.Core.Models;

namespace NacGroup.Module.Blog.Models.Schema
{
    [BsonIgnoreExtraElements]
    public class Tags: Entity
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("Name")]
        public string Name { get; set; }

        [BsonElement("Description")]
        public string Description { get; set; }

        [BsonElement("Slug")]
        public string Slug { get; set; }

        [BsonElement("Count")]
        public int Count { get; set; }

        [BsonElement("CreatedDate")]
        public DateTime CreatedDate { get; set; }

        public object this[string propertyName]
        {
            get => this.GetType().GetProperty(propertyName).GetValue(this, null);
            set => this.GetType().GetProperty(propertyName).SetValue(this, value, null);
        }

    }

    
}
