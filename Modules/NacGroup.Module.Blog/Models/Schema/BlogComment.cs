﻿using System;
using System.Collections.Generic;
using System.Linq;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using NacGroup.Module.Core.Models;

namespace NacGroup.Module.Blog.Models.Schema
{
    [BsonIgnoreExtraElements]
    public class BlogComment : Entity, ITrackingUpdate
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("Name")]
        public string Name { get; set; }

        [BsonElement("Email")]
        public string Email { get; set; }

        [BsonElement("Phone")]
        public string Phone { get; set; }

        [BsonElement("Body")]
        public string Body { get; set; }

        [BsonElement("Rating")]
        public decimal Rating { get; set; }

        [BsonElement("ActiveStatus")]
        public bool ActiveStatus { get; set; }

        [BsonElement("CommentStatus")]
        public CommentStatus CommentStatus { get; set; }

        [BsonElement("UserId")]
        public string UserId { get; set; }

        [BsonElement("BlogId")]
        public string BlogId { get; set; }

        [BsonElement("ParentId")]
        public string ParentId { get; set; }

        [BsonElement("CreatedDate")]
        public DateTime CreatedDate { get; set; }
        [BsonElement("UpdatedDate")]
        public DateTime UpdatedDate { get; set; }

        public object this[string propertyName]
        {
            get => this.GetType().GetProperty(propertyName)?.GetValue(this, null);
            set => this.GetType().GetProperty(propertyName)?.SetValue(this, value, null);
        }

    }

   public enum CommentStatus
    {
        Cancel,
        Spending,
        Done,
    }

}
