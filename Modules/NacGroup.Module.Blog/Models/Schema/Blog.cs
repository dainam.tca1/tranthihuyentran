﻿using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using NacGroup.Module.Core.Models;
using NacGroup.Infrastructure.Shared;

namespace NacGroup.Module.Blog.Models.Schema
{
    [BsonIgnoreExtraElements]
    public class Blog: Entity,ITrackingUpdate
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("Title")]
        public string Title { get; set; }

        [BsonElement("ShortDescription")]
        public string ShortDescription { get; set; }

        [BsonElement("CategoryId")]
        public string CategoryId { get; set; }

        [BsonElement("CategoryName")]
        public string CategoryName { get; set; }

        [BsonElement("CategoryUrl")]
        public string CategoryUrl { get; set; }

        [BsonElement("CategoryAvatar")]
        public string CategoryAvatar { get; set; }

        [BsonElement("PageContent")]
        public string PageContent { get; set; }

        [BsonElement("ThumbnailFB")]
        public string ThumbnailFB { get; set; }

        [BsonElement("MetaKeyword")]
        public string MetaKeyword { get; set; }

        [BsonElement("MetaDescription")]
        public string MetaDescription { get; set; }

        [BsonElement("MetaTitle")]
        public string MetaTitle { get; set; }

        [BsonElement("Url")]
        public string Url { get; set; }

        [BsonElement("CreatedDate")]
        public DateTime CreatedDate { get; set; }

        [BsonElement("Avatar")]
        public string Avatar { get; set; }

        [BsonElement("UpdatedDate")]
        public DateTime UpdatedDate { get; set; }

        [BsonElement("UserProfileId")]
        public string UserProfileId { get; set; }

        [BsonElement("EcsUserName")]
        public string EcsUserName { get; set; }

        [BsonElement("ViewCount")]
        public int ViewCount { get; set; }

        [BsonElement("Sort")]
        public int Sort { get; set; }


        [BsonElement("Tags")]
        public List<Tags> Tags { get; set; }

        public object this[string propertyName]
        {
            get => this.GetType().GetProperty(propertyName).GetValue(this, null);
            set => this.GetType().GetProperty(propertyName).SetValue(this, value, null);
        }

    }

    
}
