﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace NacGroup.Module.Blog.Models.Schema
{
    [BsonIgnoreExtraElements]
    public class BlogCategory : Core.Models.Entity
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("Name")]
        public string Name { get; set; }

        [BsonElement("Description")]
        public string Description { get; set; }

        [BsonElement("Avatar")]
        public string Avatar { get; set; }

        [BsonElement("ParentId")]
        public string ParentId { get; set; }

        [BsonElement("CategoryLevel")]
        public int CategoryLevel { get; set; }

        [BsonElement("MetaKeyword")]
        public string MetaKeyword { get; set; }

        [BsonElement("MetaDescription")]
        public string MetaDescription { get; set; }

        [BsonElement("MetaTitle")]
        public string MetaTitle { get; set; }

        [BsonElement("Url")]
        public string Url { get; set; }

        [BsonElement("Sort")]
        public int Sort { get; set; }

        public object this[string propertyName]
        {
            get => this.GetType().GetProperty(propertyName).GetValue(this, null);
            set => this.GetType().GetProperty(propertyName).SetValue(this, value, null);
        }
    }
}
