﻿using MediatR;
using Microsoft.AspNetCore.Mvc.RazorPages;
using NacGroup.Module.Blog.Data;
using NacGroup.Module.Blog.Models.Querys;
using NacGroup.Module.Core.Data;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Core.Models.Querys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NacGroup.Module.Blog.Services
{

    public interface IBlogWebService
    {
        Models.Schema.Blog GetByUrl(string url);
        void UpdateViewCount(string id, int viewCount);
        PagedResult<Models.Schema.Blog> GetByCategoryUrl(string categoryurl, int page, int pagesize);
        PagedResult<Models.Schema.Blog> GetByTagSlug(string slug, int page, int pagesize);
        PagedResult<Models.Schema.Blog> Filter(string title, int page, int pagesize);
    }

    public class BlogWebService : IBlogWebService, IRequestHandler<BlogGetListQuery, PagedResult<BlogData>>
    {
        private readonly IBlogRepository _blogRepository;
        private readonly IUnitOfWork _unitOfWork;

        public BlogWebService(IBlogRepository blogRepository, IUnitOfWork unitOfWork)
        {
            _blogRepository = blogRepository;
            _unitOfWork = unitOfWork;
        }

        public Models.Schema.Blog GetByUrl(string url)
        {
            return _blogRepository.GetSingle(m => m.Url == url);
        }
        public PagedResult<Models.Schema.Blog> Filter(string title, int page, int pagesize)
        {
            return _blogRepository.Filter(title, null, page, pagesize);
        }
        public PagedResult<Models.Schema.Blog> GetByTagSlug(string slug, int page, int pagesize)
        {
            var blog_all = _blogRepository.Get(m => m.Tags.Any(x => x.Slug == slug));
            return new PagedResult<Models.Schema.Blog>(blog_all, page, pagesize, blog_all.Count());
        }
        public PagedResult<Models.Schema.Blog> GetByCategoryUrl(string categoryurl, int page, int pagesize)
        {
            return _blogRepository.Get(page, pagesize, m => m.CategoryUrl == categoryurl);
        }

        public void UpdateViewCount(string id, int viewCount)
        {
            try
            {
                var blog = _blogRepository.GetById(id);
                if (blog == null)
                    return;
                blog.ViewCount = viewCount;
                _blogRepository.Update(id, blog);
                _unitOfWork.Commit();

            }
            catch
            {
                // ignored
            }
        }

        public Task<PagedResult<BlogData>> Handle(BlogGetListQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var blog_all = _blogRepository.Get(request.Page, request.PageSize);
                return Task.FromResult(new PagedResult<BlogData>(blog_all.Results.Select(m =>
                new BlogData
                {
                    Avatar = m.Avatar,
                    Id = m.Id,
                    Title = m.Title,
                    ShortDescription = m.ShortDescription,
                    CategoryId = m.CategoryId,
                    CategoryName = m.CategoryName,
                    CategoryUrl = m.CategoryUrl,
                    CategoryAvatar = m.CategoryAvatar,
                    PageContent = m.PageContent,
                    ThumbnailFB = m.ThumbnailFB,
                    MetaDescription = m.MetaDescription,
                    MetaKeyword = m.MetaKeyword,
                    MetaTitle = m.MetaTitle,
                    Url = m.Url,
                    CreatedDate = m.CreatedDate,
                    UpdatedDate = m.UpdatedDate,
                    Sort = m.Sort,
                    ViewCount = m.ViewCount
                }).ToList()
                    , blog_all.CurrentPage, blog_all.PageSize, blog_all.TotalItemCount));
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}
