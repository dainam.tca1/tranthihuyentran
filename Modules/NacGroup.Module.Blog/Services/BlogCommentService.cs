﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using NacGroup.Module.Core.Data;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Core.Models.DomainEvents;
using NacGroup.Module.Blog.Data;
using NacGroup.Module.Blog.Models;
using NacGroup.Module.Blog.Models.Schema;
using Newtonsoft.Json;
using NacGroup.Module.Core.Services;
using Newtonsoft.Json.Linq;

namespace NacGroup.Module.Blog.Services
{
    public interface IBlogCommentService
    {
        PagedResult<BlogComment> Filter(string blogId, int page, int pagesize);
        PagedResult<BlogComment> FilterComment(int page, int pagesize);
        ApiResponseModel Add(BlogComment model, string captcha);
        ApiResponseModel AddReply(BlogComment model);
        ApiResponseModel AddAdmin(BlogComment model);
        BlogComment GetById(string id);
        ApiResponseModel Update(BlogComment model);
        ApiResponseModel UpdateCustomize(BlogComment model, string name);
    }
    public class BlogCommentService : IBlogCommentService
    {
        private readonly IBlogCommentRepository _blogCommentRepository;
        private readonly IBlogRepository _blogRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly GlobalConfiguration _globalConfiguration;
        public BlogCommentService(
            IBlogCommentRepository blogCommentRepository,
            IBlogRepository blogRepository,
            IUnitOfWork unitOfWork,
            IGettingGlobalConfigService gettingGlobalConfigService)
        {
            _blogCommentRepository = blogCommentRepository;
            _blogRepository = blogRepository;
            _unitOfWork = unitOfWork;
            _globalConfiguration = gettingGlobalConfigService.GetConfiguration();

        }

        public PagedResult<BlogComment> Filter(string blogId, int page, int pagesize)
        {
            return _blogCommentRepository.Filter(blogId, page, pagesize);
        }

        public PagedResult<BlogComment> FilterComment(int page, int pagesize)
        {
            return _blogCommentRepository.Filter(page, pagesize);
        }

        public ApiResponseModel Add(BlogComment model, string captcha)
        {
            try
            {
                #region Validate
                if (string.IsNullOrEmpty(model.Body)) return new ApiResponseModel("error", "Mời bạn nhập nội dung bình luận");
                if (model.Body.Length <= 80) return new ApiResponseModel("error", "Nội dung bình luận phải tối thiểu 80 kí tự");
               
                if (string.IsNullOrEmpty(model.Name)) return new ApiResponseModel("error","Mời bạn nhập họ và tên");
                if (string.IsNullOrEmpty(model.Phone)) return new ApiResponseModel("error", "Mời bạn nhập số điện thoại");
                //var privateKey = _globalConfiguration.GlobalWebSetting["WebsiteConfig"]["GcaptchaPrivate"];

                //string URI = "https://www.google.com/recaptcha/api/siteverify";
                //string myParameters = "secret=" + privateKey + "&response=" + captcha;

                //using (System.Net.WebClient wc = new System.Net.WebClient())
                //{
                //    wc.Headers[System.Net.HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                //    string HtmlResult = wc.UploadString(URI, myParameters);
                //    var json = JObject.Parse(HtmlResult);
                //    if (!json["success"].Value<bool>())
                //    {
                //        return new ApiResponseModel("error", "Please confirm captcha");
                //    }
                //}
                #endregion
                model.CommentStatus = CommentStatus.Spending;
                var result = _blogCommentRepository.Add(model);
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);

                result = _unitOfWork.Commit();
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);

                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
            }
            catch (Exception e)
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }

        }
        public ApiResponseModel AddReply(BlogComment model)
        {
            try
            {
                #region Validate   
                if (string.IsNullOrEmpty(model.Body)) return new ApiResponseModel("error", "Vui lòng nhập bình luận");
                if (model.Body.Length <= 80) return new ApiResponseModel("error", "Bình luận nhập tối thiểu 80 kí tự");
                if (string.IsNullOrEmpty(model.Name)) return new ApiResponseModel("error", "Mời bạn nhập họ và tên");
                if (string.IsNullOrEmpty(model.Phone)) return new ApiResponseModel("error", "Mời bạn nhập số điện thoại");

                #endregion
                model.CommentStatus = CommentStatus.Spending;
                var result = _blogCommentRepository.Add(model);
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);

                result = _unitOfWork.Commit();
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);

                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
            }
            catch (Exception e)
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }

        }

        public ApiResponseModel AddAdmin(BlogComment model)
        {
            try
            {
                #region Validate

                if (string.IsNullOrEmpty(model.Body)) return new ApiResponseModel("error", "Content Is Required");
                #endregion

                var result = _blogCommentRepository.Add(model);
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);

                result = _unitOfWork.Commit();
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);

                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
            }
            catch (Exception e)
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }

        }

        public BlogComment GetById(string id)
        {
            return _blogCommentRepository.GetById(id);
        }



        public ApiResponseModel Update(BlogComment model)
        {
            try
            {
                #region Validate
                

                #endregion
                var temp = _blogCommentRepository.GetById(model.Id);
                if (temp == null) return new ApiResponseModel("error", StaticMessage.DataNotFound);
                model.ActiveStatus = !model.ActiveStatus;
                model.UpdatedDate = DateTime.Now;
                var result = _blogCommentRepository.Update(temp.Id,model);

                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);

                result = _unitOfWork.Commit();
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);

                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
            }
            catch (Exception e)
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }

        }

        public ApiResponseModel UpdateCustomize(BlogComment model, string name)
        {
            try
            {
                if (model == null || string.IsNullOrEmpty(name)) return new ApiResponseModel("error", StaticMessage.DataNotFound);

                var temp = _blogCommentRepository.GetById(model.Id);
                if (temp == null) return new ApiResponseModel("error", StaticMessage.DataNotFound);

                temp[name] = model[name];
                var result = _blogCommentRepository.Update(model.Id, temp);
                if (result < 0) return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);

                result = _unitOfWork.Commit();
                if (result < 0) return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }

    }
}
