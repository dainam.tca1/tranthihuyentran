﻿using System.Collections.Generic;
using System.Linq;
using MediatR;
using NacGroup.Infrastructure;
using NacGroup.Module.Blog.Data;
using NacGroup.Module.Blog.Models;
using NacGroup.Module.Blog.Models.Schema;
using NacGroup.Module.Core.Data;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Core.Models.DomainEvents;

namespace NacGroup.Module.Blog.Services
{
    public interface IBlogCategoryService
    {
        ApiResponseModel Delete(List<string> idlst, LanguageType lang);
        ApiResponseModel Delete(string id, LanguageType lang);
        BlogCategory GetById(string id);
        ApiResponseModel Add(BlogCategory model, LanguageType lang);
        ApiResponseModel Update(BlogCategory model, LanguageType lang);
        PagedResult<BlogCategory> GetAndSortByLevel();
     
        ApiResponseModel UpdateCustomize(BlogCategory model, string name, LanguageType lang);
        BlogCategory GetByUrl(string url);
    }

    public class BlogCategoryService : IBlogCategoryService
    {

        private readonly IBlogCategoryRepository _blogCategoryRepository;
        private readonly IBlogRepository _blogRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMediator _mediator;

        public BlogCategoryService(IBlogCategoryRepository blogCategoryRepository, IBlogRepository blogRepository, IUnitOfWork unitOfWork, IMediator mediator)
        {
            _blogCategoryRepository = blogCategoryRepository;
            _blogRepository = blogRepository;
            _unitOfWork = unitOfWork;
            _mediator = mediator;
        }

        public ApiResponseModel Delete(List<string> idlst, LanguageType lang)
        {
            if (idlst == null)
                return new ApiResponseModel("error", CoreStaticMessage.SelectDataToRemove[(int)lang]);
            foreach (var item in idlst)
            {
                var re = Delete(item, lang);
                if (re.status == "error") return re;
            }
            return new ApiResponseModel("success", CoreStaticMessage.DataUpdateSuccessfully[(int)lang]);
        }
        public BlogCategory GetByUrl(string url)
        {
            if (string.IsNullOrEmpty(url)) return null;
            var blogCate = _blogCategoryRepository.GetSingle(m => m.Url == url);
            if(blogCate == null) return null;
            return blogCate;
        }
        public ApiResponseModel Delete(string id,LanguageType lang)
        {
            var blogOfCurrentCate = _blogRepository.GetSingle(m => m.CategoryId == id);
            if (blogOfCurrentCate != null) return new ApiResponseModel("error", BlogStaticMessage.PleaseRemoveAllBlogInCurrentCategory[(int)lang]);
            if(_blogCategoryRepository.GetChildrenCategories(id).Any())
                return new ApiResponseModel("error", BlogStaticMessage.PleaseRemoveChildrenCategory[(int)lang]);


            var resultcode = _blogCategoryRepository.Delete(id);
            if (resultcode < 0)
                return new ApiResponseModel("error", CoreStaticMessage.GlobalErrorMessage[(int)lang]);
            resultcode = _unitOfWork.Commit();
            if (resultcode < 0)
                return new ApiResponseModel("error", CoreStaticMessage.GlobalErrorMessage[(int)lang]);
            return new ApiResponseModel("success", CoreStaticMessage.DataUpdateSuccessfully[(int)lang]);
        }

        public BlogCategory GetById(string id)
        {
            return _blogCategoryRepository.GetById(id);
        }

        public ApiResponseModel Add(BlogCategory model, LanguageType lang)
        {
            #region Validate
            if (string.IsNullOrEmpty(model.Name)) return new ApiResponseModel("error", BlogStaticMessage.CategoryNameIsRequired[(int)lang]);
            if (string.IsNullOrEmpty(model.Url)) return new ApiResponseModel("error", BlogStaticMessage.CategoryUrlIsRequired[(int)lang]);
            var oldcate = _blogCategoryRepository.GetSingle(m => m.Url == model.Url);
            if (oldcate != null)
                return new ApiResponseModel("error", BlogStaticMessage.CategoryUrlIsExist[(int)lang]);

            //Parent category
            BlogCategory parentcate = null;
            if (!string.IsNullOrEmpty(model.ParentId))
            {
                parentcate = _blogCategoryRepository.GetById(model.ParentId);
                if (parentcate == null) return new ApiResponseModel("error", BlogStaticMessage.ParentCategoryNotFound[(int)lang]);
            }
            #endregion

            try
            {
                model.CategoryLevel = parentcate != null ? parentcate.CategoryLevel + 1 : 0;
                var result = _blogCategoryRepository.Add(model);
                if (result < 0)
                    return new ApiResponseModel("error", CoreStaticMessage.GlobalErrorMessage[(int)lang]);

                result = _unitOfWork.Commit();
                if (result < 0)
                    return new ApiResponseModel("error", CoreStaticMessage.GlobalErrorMessage[(int)lang]);
                return new ApiResponseModel("success", CoreStaticMessage.DataUpdateSuccessfully[(int)lang]);
            }
            catch
            {
                return new ApiResponseModel("error", CoreStaticMessage.GlobalErrorMessage[(int)lang]);
            }
        }

        public ApiResponseModel Update(BlogCategory model, LanguageType lang)
        {
            #region Validate
            if (string.IsNullOrEmpty(model.Name)) return new ApiResponseModel("error", BlogStaticMessage.CategoryNameIsRequired[(int)lang]);
            if (string.IsNullOrEmpty(model.Url)) return new ApiResponseModel("error", BlogStaticMessage.CategoryUrlIsRequired[(int)lang]);

            var temp = _blogCategoryRepository.GetById(model.Id);
            if (temp == null)
                return new ApiResponseModel("error", CoreStaticMessage.DataNotFound);

            var oldcate = _blogCategoryRepository.GetSingle(m => m.Url == model.Url);
            if (oldcate != null && temp.Id != oldcate.Id)
                return new ApiResponseModel("error", BlogStaticMessage.CategoryUrlIsExist[(int)lang]);

            BlogCategory parentcate = null;
            if (!string.IsNullOrEmpty(model.ParentId))
            {
                parentcate = _blogCategoryRepository.GetById(model.ParentId);
                if (parentcate == null) return new ApiResponseModel("error", BlogStaticMessage.ParentCategoryNotFound[(int)lang]);

                if (model.ParentId != temp.ParentId)
                {
                    if (parentcate.CategoryLevel > temp.CategoryLevel) return new ApiResponseModel("error", BlogStaticMessage.ParentCategoryLevelMustLowerCurrentCategory[(int)lang]);
                }

                if (model.ParentId == temp.Id) return new ApiResponseModel("error", BlogStaticMessage.ParentCategoryLevelMustDifferentCurrentCategory[(int)lang]);

            }
            #endregion

            try
            {
                model.CategoryLevel = parentcate != null ? parentcate.CategoryLevel + 1 : 0;
                var isChangeChildLevel = temp.CategoryLevel != model.CategoryLevel;
                var isChangeDataInBlogTable = temp.Name != model.Name || temp.Url != model.Url || temp.Avatar != model.Avatar;
                temp.Avatar = model.Avatar;
                temp.MetaDescription = model.MetaDescription;
                temp.MetaKeyword = model.MetaKeyword;
                temp.MetaTitle = model.MetaTitle;
                temp.Sort = model.Sort;
                temp.CategoryLevel = model.CategoryLevel;
                temp.Url = model.Url;
                temp.Description = model.Description;
                temp.Name = model.Name;
                temp.ParentId = string.IsNullOrEmpty(model.ParentId) ? null : model.ParentId;
                var result = _blogCategoryRepository.Update(model.Id, temp);
                if (result < 0)
                    return new ApiResponseModel("error", CoreStaticMessage.GlobalErrorMessage[(int)lang]);

                #region Kiểm tra các table reference tới table này
                if (isChangeChildLevel)
                {
                    UpdateChildCategoryLevel(temp);
                }
                if (isChangeDataInBlogTable)
                {
                    _mediator.Publish(new BlogCategoryChangedDomainEvent(temp.Id, temp.Name, temp.Avatar, temp.Url));
                }

                #endregion

                result = _unitOfWork.Commit();
                if (result < 0)
                    return new ApiResponseModel("error", CoreStaticMessage.GlobalErrorMessage[(int)lang]);

                return new ApiResponseModel("success", CoreStaticMessage.DataUpdateSuccessfully[(int)lang]);
            }
            catch
            {
                return new ApiResponseModel("error", CoreStaticMessage.GlobalErrorMessage[(int)lang]);
            }
        }

        private void UpdateChildCategoryLevel(BlogCategory category)
        {
            var childrentlist = _blogCategoryRepository.GetChildrenCategories(category.Id);
            if (childrentlist == null || !childrentlist.Any())
                return;

            foreach (var item in childrentlist)
            {
                item.CategoryLevel = category.CategoryLevel + 1;
                _blogCategoryRepository.Update(item.Id, item);
                UpdateChildCategoryLevel(item);
            }
        }

        public PagedResult<BlogCategory> GetAndSortByLevel()
        {
            var catlst = _blogCategoryRepository.Get().ToList();
            //Chuyển về dạng tree
            catlst = catlst.OrderBy(x => x.Sort).ThenBy(x => x.Name).ToList();
            var stack = new Stack<BlogCategory>();

            // Grab all the items without parents
            foreach (var section in catlst.Where(x => x.ParentId == null).Reverse())
            {
                stack.Push(section);
                catlst.Remove(section);
            }

            var output = new List<BlogCategory>();
            while (stack.Any())
            {
                var currentSection = stack.Pop();

                var children = catlst.Where(x => x.ParentId == currentSection.Id).OrderBy(m => m.Sort).Reverse();

                foreach (var section in children)
                {
                    stack.Push(section);
                    catlst.Remove(section);
                }
                output.Add(currentSection);
            }

            return new PagedResult<BlogCategory>(output, 1, output.Count, output.Count);
        }

        public ApiResponseModel UpdateCustomize(BlogCategory model, string name, LanguageType lang)
        {
            try
            {
                if (model == null || string.IsNullOrEmpty(name)) return new ApiResponseModel("error", CoreStaticMessage.DataNotFound[(int)lang]);

                var temp = _blogCategoryRepository.GetById(model.Id);
                if (temp == null) return new ApiResponseModel("error", CoreStaticMessage.DataNotFound[(int)lang]);

                temp[name] = model[name];
                var result = _blogCategoryRepository.Update(model.Id, temp);
                if (result < 0) return new ApiResponseModel("error", CoreStaticMessage.GlobalErrorMessage[(int)lang]);

                result = _unitOfWork.Commit();
                if (result < 0)
                    return new ApiResponseModel("error", CoreStaticMessage.GlobalErrorMessage[(int)lang]);
                return new ApiResponseModel("success", CoreStaticMessage.DataUpdateSuccessfully[(int)lang]);
            }
            catch
            {
                return new ApiResponseModel("error", CoreStaticMessage.GlobalErrorMessage[(int)lang]);
            }
        }
    }
}
