﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using NacGroup.Infrastructure;
using NacGroup.Module.Blog.Data;
using NacGroup.Module.Blog.Models;
using NacGroup.Module.Blog.Models.Schema;
using NacGroup.Module.Core.Data;
using NacGroup.Module.Core.Interfaces;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Core.Models.DomainEvents;

namespace NacGroup.Module.Blog.Services
{
    public interface ITagsCmsService
    {
        ApiResponseModel Delete(List<string> idlst);
        ApiResponseModel Delete(string id);
        Tags GetById(string id);
        Tags GetBySlug(string slug);
        ApiResponseModel Add(Tags model);
        PagedResult<Tags> Filter(string name, int page, int pagesize);
        ApiResponseModel Update(Tags model);
      //  ApiResponseModel UpdateCustomize(Tags model, string name);
    }

    public class TagsCmsService : ITagsCmsService 
    {
        private readonly ITagsRepository _tagsRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMediator _mediator;
        public TagsCmsService(ITagsRepository tagsRepository,IMediator mediator, IUnitOfWork unitOfWork)
        {
            _mediator = mediator;
            _tagsRepository = tagsRepository;
            _unitOfWork = unitOfWork;
        }

        public Tags GetBySlug(string slug)
        {
            return _tagsRepository.GetSingle(m => m.Slug == slug);
        }
        public ApiResponseModel Delete(List<string> idlst)
        {
            if (idlst == null)
                return new ApiResponseModel("error", StaticMessage.SelectDataToRemove);
            foreach (var item in idlst)
            {
                Delete(item);
            }
            return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
        }

        public ApiResponseModel Delete(string id)
        {
            var resultcode = _tagsRepository.Delete(id);
            if (resultcode < 0)
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            resultcode = _unitOfWork.Commit();
            _mediator.Publish(new TagChangedDomainEvent(id, null, null, null, Method.Delete));
            if (resultcode < 0)
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
        }

        public Tags GetById(string id)
        {
            return _tagsRepository.GetById(id);
        }

        public ApiResponseModel Add(Tags model)
        {
            #region Validate
            if (string.IsNullOrEmpty(model.Name)) return new ApiResponseModel("error", "Name is required");
            if (string.IsNullOrEmpty(model.Slug)) return new ApiResponseModel("error", "Slug is required");

            var oldcate = _tagsRepository.GetSingle(m => m.Id == model.Id);
            if (oldcate != null)
                return new ApiResponseModel("error", "Slug is exist");

            #endregion

            try
            {

                var result = _tagsRepository.Add(model);
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);

                result = _unitOfWork.Commit();
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                var old_tag = _tagsRepository.GetSingle(m => m.Slug == model.Slug);
                if (old_tag == null) return new ApiResponseModel("error",StaticMessage.DataNotFound);

                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully, old_tag.Id);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }

        public PagedResult<Tags> Filter(string name, int page, int pagesize)
        {
            return _tagsRepository.Filter(name, page, pagesize);
        }

        public ApiResponseModel Update(Tags model)
        {
            #region Validate
            if (string.IsNullOrEmpty(model.Name)) return new ApiResponseModel("error", "Name is required");
            if (string.IsNullOrEmpty(model.Slug)) return new ApiResponseModel("error", "Slug is required");
            var temp = _tagsRepository.GetSingle(m => m.Id == model.Id);
            if(temp == null) return new ApiResponseModel("error", StaticMessage.DataNotFound);

            var isChangeDataInBlogTable = temp.Name != model.Name || temp.Slug != model.Slug || temp.Description != model.Description;
            #endregion

            try
            {
                #region Kiểm tra các table reference tới table này
             
                if (isChangeDataInBlogTable)
                {
                    _mediator.Publish(new TagChangedDomainEvent(temp.Id, temp.Name, temp.Slug,temp.Description,Method.Update));
                }

                #endregion
                var result = _tagsRepository.Update(temp.Id, model);
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);

                result = _unitOfWork.Commit();
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }

        


    }
  
}
