﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using NacGroup.Infrastructure;
using NacGroup.Module.Blog.Data;
using NacGroup.Module.Blog.Models;
using NacGroup.Module.Blog.Models.Schema;
using NacGroup.Module.Core.Data;
using NacGroup.Module.Core.Interfaces;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Core.Models.DomainEvents;
using System.Linq;

namespace NacGroup.Module.Blog.Services
{
    public interface IBlogCmsService
    {
        ApiResponseModel Delete(List<string> idlst, LanguageType lang);
        ApiResponseModel Delete(string id, LanguageType lang);
        Models.Schema.Blog GetById(string id);
        ApiResponseModel Add(Models.Schema.Blog model, LanguageType lang);
        PagedResult<Models.Schema.Blog> Filter(string title, string categoryId, int page, int pagesize);
        PagedResult<Models.Schema.Blog> Filter(string title, string categoryId,string categoryname, int page, int pagesize);
        ApiResponseModel Update(Models.Schema.Blog model, LanguageType lang);
        ApiResponseModel UpdateCustomize(Models.Schema.Blog model, string name, LanguageType lang);
    }

    public class BlogCmsService : IBlogCmsService, INotificationHandler<BlogCategoryChangedDomainEvent>, INotificationHandler<TagChangedDomainEvent>
    {
        private readonly IBlogRepository _blogRepository;
        private readonly IBlogCategoryRepository _blogCategoryRepository;
        private readonly ITagsRepository _tagsRepository;
        private readonly IUnitOfWork _unitOfWork;
        public BlogCmsService(IBlogRepository blogRepository, IBlogCategoryRepository blogCategoryRepository,ITagsRepository tagsRepository, IUnitOfWork unitOfWork)
        {
            _tagsRepository = tagsRepository;
            _blogRepository = blogRepository;
            _blogCategoryRepository = blogCategoryRepository;
            _unitOfWork = unitOfWork;
        }


        public ApiResponseModel Delete(List<string> idlst, LanguageType lang)
        {
            if (idlst == null)
                return new ApiResponseModel("error", CoreStaticMessage.SelectDataToRemove[(int)lang]);
            foreach (var item in idlst)
            {
                Delete(item,lang);
            }
            return new ApiResponseModel("success", CoreStaticMessage.DataUpdateSuccessfully[(int)lang]);
        }

        public ApiResponseModel Delete(string id, LanguageType lang)
        {
            var resultcode = _blogRepository.Delete(id);
            if (resultcode < 0)
                return new ApiResponseModel("error", CoreStaticMessage.GlobalErrorMessage[(int)lang]);
            resultcode = _unitOfWork.Commit();
            if (resultcode < 0)
                return new ApiResponseModel("error", CoreStaticMessage.GlobalErrorMessage[(int)lang]);
            return new ApiResponseModel("success", CoreStaticMessage.DataUpdateSuccessfully[(int)lang]);
        }

        public Models.Schema.Blog GetById(string id)
        {
            return _blogRepository.GetById(id);
        }

        public ApiResponseModel Add(Models.Schema.Blog model, LanguageType lang)
        {
            #region Validate
            if (string.IsNullOrEmpty(model.Title)) return new ApiResponseModel("error", BlogStaticMessage.BlogTitleRequired[(int)lang]);
            if (string.IsNullOrEmpty(model.Url)) return new ApiResponseModel("error", BlogStaticMessage.BlogUrlRequired[(int)lang]);

            if (string.IsNullOrEmpty(model.CategoryId)) return new ApiResponseModel("error", BlogStaticMessage.BlogCategoryRequired[(int)lang]);
            var cate = _blogCategoryRepository.GetById(model.CategoryId);
            if (cate == null) return new ApiResponseModel("error", BlogStaticMessage.BlogCategoryRequired[(int)lang]);

            foreach (var tag in model.Tags)
            {
                var old_tag = _tagsRepository.GetSingle(m => m.Slug == tag.Slug);
                if (old_tag == null)
                {
                    var addTag = _tagsRepository.Add(tag);
                    if(addTag < 0) return new ApiResponseModel("error", CoreStaticMessage.GlobalErrorMessage[(int)lang]);
                    addTag = _unitOfWork.Commit();
                    if (addTag < 0) return new ApiResponseModel("error", CoreStaticMessage.GlobalErrorMessage[(int)lang]);
                }
            }

            var oldcate = _blogRepository.GetSingle(m => m.Url == model.Url);
            if (oldcate != null)
                return new ApiResponseModel("error", BlogStaticMessage.BlogUrlIsExist[(int)lang]);

            #endregion

            try
            {
                var tag_List = new List<Tags>();
                foreach (var tag in model.Tags)
                {
                    var tagObject = new Tags { };
                    var old_tag = _tagsRepository.GetSingle(m => m.Slug == tag.Slug);           
                    if (old_tag != null)
                    {
                        tagObject.Id = old_tag.Id;
                        tagObject.Name = old_tag.Name;
                        tagObject.Slug = old_tag.Slug;                 
                        tagObject.CreatedDate = DateTime.Now;
                        tag_List.Add(tagObject);
                        //var count_Tag = _blogRepository.Get().Where(m => m.Tags.Where(t => t.Id == tagObject.Id).Any());

                        //tagObject.Count = count_Tag;
                        //var updateTag = _tagsRepository.Update(tagObject.Id, tagObject);
                        //if (updateTag < 0) return new ApiResponseModel("error", CoreStaticMessage.GlobalErrorMessage[(int)lang]);
                        //updateTag = _unitOfWork.Commit();
                        //if (updateTag < 0) return new ApiResponseModel("error", CoreStaticMessage.GlobalErrorMessage[(int)lang]);
                    }
                    
                }
                model.Tags = tag_List;
                model.CategoryName = cate.Name;
                model.CategoryUrl = cate.Url;
                model.CategoryAvatar = cate.Avatar;
                var result = _blogRepository.Add(model);
                if (result < 0)
                    return new ApiResponseModel("error", CoreStaticMessage.GlobalErrorMessage[(int)lang]);

                result = _unitOfWork.Commit();
                if (result < 0)
                    return new ApiResponseModel("error", CoreStaticMessage.GlobalErrorMessage[(int)lang]);

                return new ApiResponseModel("success", CoreStaticMessage.DataUpdateSuccessfully[(int)lang]);
            }
            catch
            {
                return new ApiResponseModel("error", CoreStaticMessage.GlobalErrorMessage[(int)lang]);
            }
        }

        public PagedResult<Models.Schema.Blog> Filter(string title, string categoryId, int page, int pagesize)
        {
            return _blogRepository.Filter(title, categoryId, page, pagesize);
        }
        public PagedResult<Models.Schema.Blog> Filter(string title, string categoryId, string categoryname, int page, int pagesize)
        {
            return _blogRepository.Filter(title, categoryId, categoryname, page, pagesize);
        }
        public ApiResponseModel Update(Models.Schema.Blog model, LanguageType lang)
        {
            #region Validate
            if (string.IsNullOrEmpty(model.Title)) return new ApiResponseModel("error", BlogStaticMessage.BlogTitleRequired[(int)lang]);
            if (string.IsNullOrEmpty(model.Url)) return new ApiResponseModel("error", BlogStaticMessage.BlogUrlRequired[(int)lang]);

            var temp = _blogRepository.GetById(model.Id);
            if (temp == null)
                return new ApiResponseModel("error", CoreStaticMessage.DataNotFound[(int)lang]);

            if (string.IsNullOrEmpty(model.CategoryId)) return new ApiResponseModel("error", BlogStaticMessage.BlogCategoryRequired[(int)lang]);
            var cate = _blogCategoryRepository.GetById(model.CategoryId);
            if (cate == null) return new ApiResponseModel("error", BlogStaticMessage.BlogCategoryRequired[(int)lang]);

            var oldcate = _blogRepository.GetSingle(m => m.Url == model.Url);
            if (oldcate != null && temp.Id != oldcate.Id)
                return new ApiResponseModel("error", BlogStaticMessage.BlogUrlIsExist[(int)lang]);
            foreach (var tag in model.Tags)
            {
                var old_tag = _tagsRepository.GetSingle(m => m.Slug == tag.Slug);
                if (old_tag == null)
                {
                    var addTag = _tagsRepository.Add(tag);
                    if (addTag < 0) return new ApiResponseModel("error", CoreStaticMessage.GlobalErrorMessage[(int)lang]);
                    addTag = _unitOfWork.Commit();
                    if (addTag < 0) return new ApiResponseModel("error", CoreStaticMessage.GlobalErrorMessage[(int)lang]);
                }
            }
            #endregion

            try
            {
                temp.Avatar = model.Avatar;
                temp.MetaDescription = model.MetaDescription;
                temp.MetaKeyword = model.MetaKeyword;
                temp.MetaTitle = model.MetaTitle;
                temp.PageContent = model.PageContent;
                temp.ShortDescription = model.ShortDescription;
                temp.Sort = model.Sort;
                temp.Title = model.Title;
                temp.Url = model.Url;
                temp.ThumbnailFB = model.ThumbnailFB;
                var tag_List = new List<Tags>();
                foreach (var tag in model.Tags)
                {
                    var tagObject = new Tags { };
                    var old_tag = _tagsRepository.GetSingle(m => m.Slug == tag.Slug);
                    
                    if (old_tag != null)
                    {

                        tagObject.Id = old_tag.Id;
                        tagObject.Name = old_tag.Name;
                        tagObject.Slug = old_tag.Slug;
                        tagObject.CreatedDate = DateTime.Now;
                        tag_List.Add(tagObject);
                        //var count_Tag = _blogRepository.Get().Where(m => m.Tags.Where(t => t.Id == tagObject.Id).Any()).Count();
                        //tagObject.Count = count_Tag;
                        //var updateTag = _tagsRepository.Update(tagObject.Id,tagObject);
                        //if (updateTag < 0) return new ApiResponseModel("error", CoreStaticMessage.GlobalErrorMessage[(int)lang]);
                        //updateTag = _unitOfWork.Commit();
                        //if (updateTag < 0) return new ApiResponseModel("error", CoreStaticMessage.GlobalErrorMessage[(int)lang]);


                    }

                }
                temp.Tags = tag_List;
                if (temp.CategoryId != model.CategoryId)
                {
                    temp.CategoryId = model.CategoryId;
                    temp.CategoryAvatar = cate.Avatar;
                    temp.CategoryName = cate.Name;
                    temp.CategoryUrl = cate.Url;
                }

                var result = _blogRepository.Update(model.Id, temp);
                if (result < 0)
                    return new ApiResponseModel("error", CoreStaticMessage.GlobalErrorMessage[(int)lang]);
                result = _unitOfWork.Commit();
                if (result < 0)
                    return new ApiResponseModel("error", CoreStaticMessage.GlobalErrorMessage[(int)lang]);
                return new ApiResponseModel("success", CoreStaticMessage.DataUpdateSuccessfully[(int)lang]);
            }
            catch
            {
                return new ApiResponseModel("error", CoreStaticMessage.GlobalErrorMessage[(int)lang]);
            }
        }

        public ApiResponseModel UpdateCustomize(Models.Schema.Blog model, string name, LanguageType lang)
        {
            try
            {
                if (model == null || string.IsNullOrEmpty(name)) return new ApiResponseModel("error", CoreStaticMessage.DataNotFound[(int)lang]);

                var temp = _blogRepository.GetById(model.Id);
                if (temp == null) return new ApiResponseModel("error", CoreStaticMessage.DataNotFound[(int)lang]);

                temp[name] = model[name];
                var result = _blogRepository.Update(model.Id, temp);
                if (result < 0) return new ApiResponseModel("error", CoreStaticMessage.GlobalErrorMessage[(int)lang]);

                result = _unitOfWork.Commit();
                if (result < 0) return new ApiResponseModel("error", CoreStaticMessage.GlobalErrorMessage[(int)lang]);
                return new ApiResponseModel("success", CoreStaticMessage.DataUpdateSuccessfully[(int)lang]);
            }
            catch
            {
                return new ApiResponseModel("error", CoreStaticMessage.GlobalErrorMessage[(int)lang]);
            }
        }

        /// <summary>
        /// Được gọi khi category name/url/avatar updated
        /// </summary>
        /// <param name="notification"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task Handle(BlogCategoryChangedDomainEvent notification, CancellationToken cancellationToken)
        {
            try
            {
                var blogList = _blogRepository.Get(m => m.CategoryId == notification.Id);
                foreach (var blog in blogList)
                {
                    blog.CategoryAvatar = notification.Avatar;
                    blog.CategoryName = notification.Name;
                    blog.CategoryUrl = notification.Url;
                    _blogRepository.Update(blog.Id, blog);
                }
                return Task.CompletedTask;
            }
            catch
            {
                return Task.CompletedTask;
            }
        }

        /// <summary>
        /// Được gọi khi tag updated
        /// </summary>
        /// <param name="notification"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task Handle(TagChangedDomainEvent notification, CancellationToken cancellationToken)
        {
            try
            {
                var blogList = _blogRepository.Get(m => m.Tags.Any(x => x.Id == notification.Id));
                switch (notification.Method)
                {
                    case Method.Update:
                       
                        var tagTemp = new Tags
                        {
                            Id = notification.Id,
                            Name = notification.Name,
                            Description = notification.Description,
                            Slug = notification.Slug,

                        };
                        foreach (var blog in blogList)
                        {
                            var tag = blog.Tags.FirstOrDefault(m => m.Id == tagTemp.Id);
                            tag = tagTemp;
                            var update = _blogRepository.Update(blog.Id, blog);
                            update = _unitOfWork.Commit();

                        }
                        break;
                    case Method.Delete:
                       
                        foreach (var blog in blogList)
                        {
                            var tag = blog.Tags.Where(m => m.Id != notification.Id).ToList();
                            blog.Tags = tag;
                            var update = _blogRepository.Update(blog.Id, blog);
                           
                            update = _unitOfWork.Commit();
                        }
                        break;
                }
                return Task.CompletedTask;
            }
            catch
            {
                return Task.CompletedTask;
            }
        }
    }
}
