﻿using Microsoft.AspNetCore.Mvc;
using NacGroup.Module.Blog.Services;
using NacGroup.Module.Core.Models;

namespace NacGroup.Module.Blog.RestApi
{
    [Route("api/blogs")]
    [ApiController]
    public class BlogWebApiController : ControllerBase
    {
        private readonly IBlogCmsService _blogService;

        public BlogWebApiController(IBlogCmsService blogService)
        {
            _blogService = blogService;
        }

        [HttpGet]
        public ActionResult<PagedResult<Models.Schema.Blog>> Get(int page = 1, int pagesize = 10)
        {
            return _blogService.Filter(null, null, page, pagesize);
        }
    }
}