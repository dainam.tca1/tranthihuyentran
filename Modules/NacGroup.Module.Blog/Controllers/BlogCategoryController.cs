﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using NacGroup.Infrastructure.Shared;
using NacGroup.Module.Blog.Models.Querys;
using NacGroup.Module.Blog.Models.Schema;
using NacGroup.Module.Blog.Services;
using NacGroup.Module.Core.Models;

namespace NacGroup.Module.Blog.Controllers
{
    public class BlogCategoryController : Controller
    {
        private readonly IBlogWebService _blogWebService;
        private readonly IBlogCategoryService _blogCategoryService;
        private readonly ITagsCmsService _tagsCmsService;

        public BlogCategoryController(ITagsCmsService tagsCmsService, IBlogWebService blogWebService, IBlogCategoryService blogCategoryService)
        {
            _blogWebService = blogWebService;
            _tagsCmsService = tagsCmsService;
            _blogCategoryService = blogCategoryService;

        }

        [Route("danh-muc/{url?}", Name = "BlogCategory", Order = 0)]
        public IActionResult Index(string url, int page = 1, int pagesize = 10)
        {
            var pwd = "admin@gmail.comadmin2020".ToLower().GetMd5Hash();
            var blogcates = _blogCategoryService.GetAndSortByLevel();
            ViewData["BlogCate"] = blogcates.Results.FirstOrDefault(m => m.Url == url);
            if (ViewData["BlogCate"] == null)
                return NotFound();
            var blog = _blogWebService.GetByCategoryUrl(url, page, pagesize);
            if (blog == null)
                return NotFound();

            ViewData["cateurl"] = Url.Action("Index", "BlogCategoryWeb", new { url }) + "?";

            return View(blog);
        }

       

        [Route("tim-kiem-bai-viet")]
        public IActionResult Search(string keyword, int page = 1, int pagesize = 10)
        {
            var blogList = _blogWebService.Filter(keyword, page, pagesize);
            ViewData["keyword"] = keyword;
            return View("_FilterSearch", blogList);
        }

        [Route("blog-topic/{slug}", Name = "tag")]
        public IActionResult Tag(string slug, int page = 1, int pagesize = 10)
        {

            var blog = _blogWebService.GetByTagSlug(slug, page, pagesize);
            ViewData["BlogCate"] = new BlogCategory { };
            if (ViewData["BlogCate"] == null)
                return NotFound();
            if (blog == null)
                return NotFound();
            var tag = _tagsCmsService.GetBySlug(slug);
            if (tag == null)
            {
                return NotFound();
            }
            ViewData["Tag"] = tag.Name;
            return View("Index", blog);
        }
    }
}