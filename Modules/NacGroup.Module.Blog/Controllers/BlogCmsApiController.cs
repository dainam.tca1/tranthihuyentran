﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NacGroup.Infrastructure;
using NacGroup.Module.Blog.Services;
using NacGroup.Module.Core.Interfaces;
using NacGroup.Module.Core.Models;
using Newtonsoft.Json.Linq;

namespace NacGroup.Module.Blog.Controllers
{
    [Authorize(Roles = "Admin", Policy = "TenantAuthorize")]
    [Route("cms/api/blog/{action=Index}")]
    public class BlogCmsApiController : Controller
    {
        private readonly IBlogCmsService _blogService;
        private readonly LanguageType _lang;

        public BlogCmsApiController(IBlogCmsService blogService, ITenantProvider tenantProvider)
        {
           
            _blogService = blogService;
            _lang = tenantProvider.GetTenant().DefaultLanguage;
        }

        [HttpGet]
        public ActionResult<ApiResponseModel> Index(string title, string categoryid, string categoryname, int page = 1,int pagesize = 10)
        {
           return new ApiResponseModel("success", _blogService.Filter(title, categoryid, categoryname, page,pagesize));
        }
            

        [HttpPost]
        public ActionResult<ApiResponseModel> RemoveList([FromBody] List<string> idList)
        {
            var lang = string.IsNullOrEmpty(HttpContext.Request.Cookies["cmslang"]) ? _lang : (LanguageType)int.Parse(HttpContext.Request.Cookies["cmslang"]);

            return _blogService.Delete(idList,lang);
        }

        [HttpPost]
        public ActionResult<ApiResponseModel> Add([FromBody] Models.Schema.Blog model)
        {
            var lang = string.IsNullOrEmpty(HttpContext.Request.Cookies["cmslang"]) ? _lang : (LanguageType)int.Parse(HttpContext.Request.Cookies["cmslang"]);

            return _blogService.Add(model,lang);
        }

        public ActionResult<ApiResponseModel> Update(string id)
        {
            var lang = string.IsNullOrEmpty(HttpContext.Request.Cookies["cmslang"]) ? _lang : (LanguageType)int.Parse(HttpContext.Request.Cookies["cmslang"]);

            var item = _blogService.GetById(id);
            if (item == null)
                return new ApiResponseModel("error", CoreStaticMessage.DataNotFound[(int)lang]);

            return new ApiResponseModel("success", item);
        }
        [HttpPost]
        public ActionResult<ApiResponseModel> Update([FromBody] Models.Schema.Blog model)
        {
            var lang = string.IsNullOrEmpty(HttpContext.Request.Cookies["cmslang"]) ? _lang : (LanguageType)int.Parse(HttpContext.Request.Cookies["cmslang"]);

            return _blogService.Update(model,lang);
        }

        [HttpPost]
        public ActionResult<ApiResponseModel> UpdateCustomize([FromBody]JObject data)
        {
            var lang = string.IsNullOrEmpty(HttpContext.Request.Cookies["cmslang"]) ? _lang : (LanguageType)int.Parse(HttpContext.Request.Cookies["cmslang"]);
            var model = data["model"].ToObject<Models.Schema.Blog>();
            var name = data["name"].Value<string>();
            return _blogService.UpdateCustomize(model, name,lang);
        }

    }
}
