﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Blog.Models.Schema;
using NacGroup.Module.Blog.Services;
using Newtonsoft.Json.Linq;

namespace NacGroup.Module.Blog.Controllers
{
    [Authorize(Roles = "Admin", Policy = "TenantAuthorize")]
    [Route("cms/api/blogcomment/{action=Index}")]
    public class CommentCmsApiController : Controller
    {
        private readonly IBlogCommentService _blogCommentService;

        public CommentCmsApiController(IBlogCommentService blogCommentService)
        {
            _blogCommentService = blogCommentService;
        }


        [HttpGet]
        public ActionResult<ApiResponseModel> Index(int page , int pageSize = 10)
        {
           
            var paged = _blogCommentService.FilterComment(page, pageSize);
            return new ApiResponseModel("success", paged);
        }


        [HttpPost]
        public ActionResult<ApiResponseModel> Add([FromBody] BlogComment model)
        {
            return _blogCommentService.AddAdmin(model);
        }

        public ActionResult<ApiResponseModel> Update(string id)
        {
            var item = _blogCommentService.GetById(id);
            return item == null ? new ApiResponseModel("error", StaticMessage.DataNotFound) : new ApiResponseModel("success", item);
        }

        [HttpPost]
        public ActionResult<ApiResponseModel> Update([FromBody] BlogComment model)
        {
            return _blogCommentService.Update(model);
        }

        [HttpPost]
        public ActionResult<ApiResponseModel> UpdateCustomize([FromBody]JObject data)
        {
            var model = data["model"].ToObject<BlogComment>();
            var name = data["name"].Value<string>();
            return _blogCommentService.UpdateCustomize(model, name);
        }
    }
}