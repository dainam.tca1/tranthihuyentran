﻿using NacGroup.Module.Blog.Services;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Blog.Models.Schema;

namespace NacGroup.Module.Blog.Controllers
{
    [Route("commentblog/{action=Index}")]
    public class BlogCommentController : Controller
    {
        private readonly IBlogCommentService _blogcommentService;

        public BlogCommentController(IBlogCommentService commentService)
        {
            _blogcommentService = commentService;
        }


        [HttpGet]
        public ActionResult<ApiResponseModel> Index(string id,int page, int pagesize)
        {
            var comment = _blogcommentService.Filter(id,page,pagesize);
            if (comment.Results.Count() <= 0)
                return new ApiResponseModel("error", StaticMessage.DataNotFound);


            return new ApiResponseModel("success", comment.Results);
        }

        [HttpPost]
        public ActionResult<ApiResponseModel> Comment([Bind("Name,Email,Phone,Body,BlogId")] BlogComment model)
        {

            var result = _blogcommentService.Add(model, Request.Form["g-recaptcha-response"]);
            if (result.status == "error")
            {
                return new ApiResponseModel("error", result.message);
            }
            return new ApiResponseModel("success", result.message);
        }

        [HttpPost]
        public ActionResult<ApiResponseModel> Reply([Bind("Name,Email,Phone,Body,BlogId,ParentId")] BlogComment model)
        {

            var result = _blogcommentService.AddReply(model);
            if (result.status == "error")
            {
                return new ApiResponseModel("error", result.message);
            }
            return new ApiResponseModel("success", result.message);
        }






    }
}