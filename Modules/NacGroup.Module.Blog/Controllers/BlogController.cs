﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using NacGroup.Module.Blog.Services;
using NacGroup.Module.Core.Extensions;
using NacGroup.Module.Core.Models;
using System.Linq;

namespace NacGroup.Module.Blog.Controllers
{
    public class BlogController : Controller
    {
        private readonly IBlogWebService _blogWebService;
        private readonly IBlogCategoryService _blogCategoryService;
        public BlogController(IBlogWebService blogWebService, IBlogCategoryService blogCategoryService)
        {
            _blogWebService = blogWebService;
            _blogCategoryService = blogCategoryService;
        }

        [Route("{cateUrl}/{url}/{param}/{param1}/{param2}", Name ="Blog")]
        public IActionResult Index(string cateUrl, string url)
        {
            if (string.IsNullOrEmpty(url)) return NotFound();
            if (string.IsNullOrEmpty(cateUrl)) return NotFound();
            var blogCate = _blogCategoryService.GetByUrl(cateUrl);
            if(blogCate == null ) return NotFound();
            var blog = _blogWebService.GetByUrl(url);
            if (blog == null)
                return NotFound();
           
            ViewData["BlogRecent"] = new PagedResult<Models.Schema.Blog>(_blogWebService.GetByCategoryUrl(blog.CategoryUrl, 1, 10).Results.Where(m => m.Url != url).ToList(),1,10, _blogWebService.GetByCategoryUrl(blog.CategoryUrl, 1, 10).Results.Where(m => m.Url != url).ToList().Count());
            var newstraffic = HttpContext.Session.Get<List<string>>("NewsTraffic");
            if (newstraffic == null)
                newstraffic = new List<string>();

            if (!newstraffic.Contains(blog.Id))
            {
                blog.ViewCount++;
                _blogWebService.UpdateViewCount(blog.Id, blog.ViewCount);
 
                newstraffic.Add(blog.Id);
                HttpContext.Session.Set<List<string>>("NewsTraffic", newstraffic);
            }

            return View(blog);
        }
    }
}