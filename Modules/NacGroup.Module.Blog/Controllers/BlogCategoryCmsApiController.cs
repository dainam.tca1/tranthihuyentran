﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NacGroup.Infrastructure;
using NacGroup.Module.Blog.Models.Schema;
using NacGroup.Module.Blog.Services;
using NacGroup.Module.Core.Interfaces;
using NacGroup.Module.Core.Models;
using Newtonsoft.Json.Linq;

namespace NacGroup.Module.Blog.Controllers
{
    [Authorize(Roles = "Admin", Policy = "TenantAuthorize")]
    [Route("cms/api/blogcategory/{action=Index}")]
    public class BlogCategoryCmsApiController : Controller
    {
        private readonly IBlogCategoryService _blogCategoryService;
        private readonly LanguageType _lang;

        public BlogCategoryCmsApiController(IBlogCategoryService blogCategoryService, ITenantProvider tenantProvider)
        {
            _blogCategoryService = blogCategoryService;
            _lang = tenantProvider.GetTenant().DefaultLanguage;
        }

        [HttpGet]
        public ActionResult<ApiResponseModel> Index()
        {
            var paged = _blogCategoryService.GetAndSortByLevel();
            paged.Results = paged.Results.Select(m =>
            {
                m.Name = m.Name.Insert(0, new string('-', 4 * m.CategoryLevel));
                return m;
            }).ToList();
            return new ApiResponseModel("success", paged);
        }


        [HttpPost]
        public ActionResult<ApiResponseModel> RemoveList([FromBody] List<string> idList)
        {
            var lang = string.IsNullOrEmpty(HttpContext.Request.Cookies["cmslang"]) ? _lang : (LanguageType)int.Parse(HttpContext.Request.Cookies["cmslang"]);
            return _blogCategoryService.Delete(idList, lang);
        }

        [HttpPost]
        public ActionResult<ApiResponseModel> Add([FromBody] BlogCategory model)
        {
            var lang = string.IsNullOrEmpty(HttpContext.Request.Cookies["cmslang"]) ? _lang : (LanguageType)int.Parse(HttpContext.Request.Cookies["cmslang"]);
            return _blogCategoryService.Add(model,lang);
        }

        public ActionResult<ApiResponseModel> Update(string id)
        {
            var lang = string.IsNullOrEmpty(HttpContext.Request.Cookies["cmslang"]) ? _lang : (LanguageType)int.Parse(HttpContext.Request.Cookies["cmslang"]);
            var item = _blogCategoryService.GetById(id);
            if (item == null)
                return new ApiResponseModel("error", CoreStaticMessage.DataNotFound[(int)lang]);

            return new ApiResponseModel("success", item);
        }
        [HttpPost]
        public ActionResult<ApiResponseModel> Update([FromBody] BlogCategory model)
        {
            var lang = string.IsNullOrEmpty(HttpContext.Request.Cookies["cmslang"]) ? _lang : (LanguageType)int.Parse(HttpContext.Request.Cookies["cmslang"]);
            return _blogCategoryService.Update(model, lang);
        }

        [HttpPost]
        public ActionResult<ApiResponseModel> UpdateCustomize([FromBody]JObject data)
        {
            var lang = string.IsNullOrEmpty(HttpContext.Request.Cookies["cmslang"]) ? _lang : (LanguageType)int.Parse(HttpContext.Request.Cookies["cmslang"]);
            var model = data["model"].ToObject<BlogCategory>();
            var name = data["name"].Value<string>();
            return _blogCategoryService.UpdateCustomize(model, name,lang);
        }
    }
}
