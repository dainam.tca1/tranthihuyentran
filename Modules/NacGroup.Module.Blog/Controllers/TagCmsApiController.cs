﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NacGroup.Infrastructure;
using NacGroup.Module.Blog.Models.Schema;
using NacGroup.Module.Blog.Services;
using NacGroup.Module.Core.Interfaces;
using NacGroup.Module.Core.Models;
using Newtonsoft.Json.Linq;

namespace NacGroup.Module.Blog.Controllers
{
    [Authorize(Roles = "Admin", Policy = "TenantAuthorize")]
    [Route("cms/api/tag/{action=Index}")]
    public class TagsCmsApiController : Controller
    {
        private readonly ITagsCmsService _tagsService;
        private readonly LanguageType _lang;

        public TagsCmsApiController(ITagsCmsService tagsService, ITenantProvider tenantProvider)
        {
           
            _tagsService = tagsService;
            
        }

        [HttpGet]
        public ActionResult<ApiResponseModel> Index(string name, int page = 1,int pagesize = 10)
        {
           return new ApiResponseModel("success", _tagsService.Filter(name, page,pagesize));
        }
            

        [HttpPost]
        public ActionResult<ApiResponseModel> RemoveList([FromBody] List<string> idList)
        {
          
            return _tagsService.Delete(idList);
        }

        [HttpPost]
        public ActionResult<ApiResponseModel> Add([FromBody] Tags model)
        {
         
            return _tagsService.Add(model);
        }

        public ActionResult<ApiResponseModel> Update(string id)
        {
         
            var item = _tagsService.GetById(id);
            if (item == null)
                return new ApiResponseModel("error", StaticMessage.DataNotFound);

            return new ApiResponseModel("success", item);
        }
        [HttpPost]
        public ActionResult<ApiResponseModel> Update([FromBody] Tags model)
        {
           
            return _tagsService.Update(model);
        }

        //[HttpPost]
        //public ActionResult<ApiResponseModel> UpdateCustomize([FromBody]JObject data)
        //{
        //    var lang = string.IsNullOrEmpty(HttpContext.Request.Cookies["cmslang"]) ? _lang : (LanguageType)int.Parse(HttpContext.Request.Cookies["cmslang"]);
        //    var model = data["model"].ToObject<Models.Schema.Blog>();
        //    var name = data["name"].Value<string>();
        //    return _blogService.UpdateCustomize(model, name,lang);
        //}

    }
}
