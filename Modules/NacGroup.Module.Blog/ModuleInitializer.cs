﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using NacGroup.Module.Blog.Data;
using NacGroup.Module.Blog.Services;
using NacGroup.Module.Core.Interfaces;

namespace NacGroup.Module.Blog
{
    public class ModuleInitializer : IModuleInitializer
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddTransient<IBlogRepository, BlogRepository>();
            services.AddTransient<IBlogCategoryRepository, BlogCategoryRepository>();

            services.AddTransient<ITagsRepository, TagsRepository>();
            services.AddTransient<ITagsCmsService, TagsCmsService>();

            services.AddTransient<IBlogCommentService, BlogCommentService>();
            services.AddTransient<IBlogCommentRepository, BlogCommentRepository>();
            services.AddTransient<IBlogCmsService, BlogCmsService>();
            services.AddTransient<IBlogWebService, BlogWebService>();
            services.AddTransient<IBlogCategoryService, BlogCategoryService>();

        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {

        }
    }
}
