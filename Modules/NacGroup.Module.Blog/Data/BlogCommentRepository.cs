﻿using System.Linq;
using MongoDB.Driver;
using NacGroup.Module.Core.Data;
using NacGroup.Module.Core.Interfaces;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Blog.Models.Schema;

namespace NacGroup.Module.Blog.Data
{
    public interface IBlogCommentRepository : IRepository<BlogComment>
    {
        PagedResult<BlogComment> Filter(string blogtId, int page, int pagesize);
        PagedResult<BlogComment> Filter(int page, int pagesize);
    }

    public class BlogCommentRepository : MongoRepository<BlogComment>, IBlogCommentRepository, IMigrationRepository
    {
        public BlogCommentRepository(IMongoContext context) : base(context)
        {

        }
        public void Migration()
        {
         
        }

        public void Seed()
        {

        }

        public PagedResult<BlogComment> Filter(string blogId, int page,int pagesize)
        {
            var filter = Builders<BlogComment>.Filter;
            var filterdefine = filter.Empty;
            if (!string.IsNullOrEmpty(blogId))
            {
                
                filterdefine &= filter.Where(m => m.BlogId == blogId);
            }

            var all = DbSet.Find(filterdefine);
            var totalItem = DbSet.CountDocuments(filterdefine);
            all = all.Skip((page - 1) * pagesize).Limit(pagesize).SortByDescending( m => m.CreatedDate);
            return new PagedResult<BlogComment>(all.ToList(), page, pagesize, (int)totalItem);
        }
        public PagedResult<BlogComment> Filter(int page, int pagesize)
        {
            var filter = Builders<BlogComment>.Filter;
            var filterdefine = filter.Empty;
            var all = DbSet.Find(filterdefine);
            var totalItem = DbSet.CountDocuments(filterdefine);
            all = all.Skip((page - 1) * pagesize).Limit(pagesize).SortByDescending(m => m.UpdatedDate);
            return new PagedResult<BlogComment>(all.ToList(), page, pagesize, (int)totalItem);
        }
    }
}
