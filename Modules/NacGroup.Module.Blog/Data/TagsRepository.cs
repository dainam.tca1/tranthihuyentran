﻿using System;
using System.Linq.Expressions;
using MongoDB.Driver;
using NacGroup.Infrastructure.Shared;
using NacGroup.Module.Blog.Models.Schema;
using NacGroup.Module.Core.Data;
using NacGroup.Module.Core.Interfaces;
using NacGroup.Module.Core.Models;

namespace NacGroup.Module.Blog.Data
{
    public interface ITagsRepository : IRepository<Tags>
    {
        PagedResult<Tags> Filter(string name, int page, int pagesize);
      
    }

    public class TagsRepository : MongoRepository<Tags>, ITagsRepository, IMigrationRepository
    {
        public TagsRepository(IMongoContext context) : base(context)
        {
   
        }

        public void Migration()
        {
            DbSet.Indexes.CreateOne(model: new CreateIndexModel<Tags>("{Slug:1}", new CreateIndexOptions { Unique = true }));
        }


        public void Seed()
        {
            
        }
        public PagedResult<Tags> Filter(string name, int page, int pagesize)
        {
            var filter = Builders<Tags>.Filter;
            var filterdefine = filter.Empty;
            if (!string.IsNullOrEmpty(name))
            {
                var nameturl = name.ToUrl();
                filterdefine &= filter.Where(m => m.Slug.Contains(nameturl));
            }
            
           
            var totalItem = DbSet.CountDocuments(filterdefine);
            var all = DbSet.Find(filterdefine).SortBy(m => m.CreatedDate).ThenByDescending(m => m.CreatedDate).Skip((page - 1) * pagesize).Limit(pagesize);
            return new PagedResult<Tags>(all.ToList(), page, pagesize, (int)totalItem);
        }

        
       
    }
}
