﻿using System;
using System.Linq.Expressions;
using MongoDB.Driver;
using NacGroup.Infrastructure.Shared;
using NacGroup.Module.Core.Data;
using NacGroup.Module.Core.Interfaces;
using NacGroup.Module.Core.Models;

namespace NacGroup.Module.Blog.Data
{
    public interface IBlogRepository : IRepository<Models.Schema.Blog>
    {
        PagedResult<Models.Schema.Blog> Filter(string title, string categoryId, int page, int pagesize);
        PagedResult<Models.Schema.Blog> Filter(string title, string categoryId,string categoryname, int page, int pagesize);
        new PagedResult<Models.Schema.Blog> Get(int page, int pagesize,
            Expression<Func<Models.Schema.Blog, bool>> predicate);
    }

    public class BlogRepository : MongoRepository<Models.Schema.Blog>, IBlogRepository, IMigrationRepository
    {
        public BlogRepository(IMongoContext context) : base(context)
        {
   
        }

        public void Migration()
        {
            DbSet.Indexes.CreateOne(model: new CreateIndexModel<Models.Schema.Blog>("{Url:1}", new CreateIndexOptions { Unique = true }));
            DbSet.Indexes.CreateOne(model: new CreateIndexModel<Models.Schema.Blog>("{CategoryUrl:1}"));
        }


        public void Seed()
        {
            
        }
        public PagedResult<Models.Schema.Blog> Filter(string title,string categoryId, int page, int pagesize)
        {
            var filter = Builders<Models.Schema.Blog>.Filter;
            var filterdefine = filter.Empty;
            if (!string.IsNullOrEmpty(title))
            {
                //var titurl = title.ToUrl();
                filterdefine &= filter.Where(m => m.Title.ToLower().Contains(title.ToLower()));
            }
            if (!string.IsNullOrEmpty(categoryId))
            {
                filterdefine &= filter.Where(m => m.CategoryId == categoryId);
            }
           
            var totalItem = DbSet.CountDocuments(filterdefine);
            var all = DbSet.Find(filterdefine).SortBy(m => m.Sort).ThenByDescending(m => m.CreatedDate).Skip((page - 1) * pagesize).Limit(pagesize);
            return new PagedResult<Models.Schema.Blog>(all.ToList(), page, pagesize, (int)totalItem);
        }

        public PagedResult<Models.Schema.Blog> Filter(string title, string categoryId, string categoryname, int page, int pagesize)
        {
            var filter = Builders<Models.Schema.Blog>.Filter;
            var filterdefine = filter.Empty;
            if (!string.IsNullOrEmpty(title))
            {
                var titurl = title.ToUrl();
                filterdefine &= filter.Where(m => m.Url.Contains(titurl));
            }
            if (!string.IsNullOrEmpty(categoryId))
            {
                filterdefine &= filter.Where(m => m.CategoryId == categoryId);
            }
            if (!string.IsNullOrEmpty(categoryname))
            {
                filterdefine &= filter.Where(m => m.CategoryName == categoryname);
            }
            var totalItem = DbSet.CountDocuments(filterdefine);
            var all = DbSet.Find(filterdefine).SortBy(m => m.Sort).ThenByDescending(m => m.CreatedDate).Skip((page - 1) * pagesize).Limit(pagesize);
            return new PagedResult<Models.Schema.Blog>(all.ToList(), page, pagesize,(int)totalItem);
        }

        public override PagedResult<Models.Schema.Blog> Get(int page, int pagesize,
            Expression<Func<Models.Schema.Blog, bool>> predicate)
        {
            var totalItem = DbSet.CountDocuments(predicate);
            var all = DbSet.Find(predicate).SortBy(m => m.Sort).ThenByDescending(m => m.CreatedDate).Skip((page - 1) * pagesize).Limit(pagesize);
            return new PagedResult<Models.Schema.Blog>(all.ToList(), page, pagesize, (int)totalItem);
        }
    }
}
