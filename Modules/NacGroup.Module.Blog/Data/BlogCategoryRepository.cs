﻿using System.Collections.Generic;
using MongoDB.Driver;
using NacGroup.Module.Blog.Models.Schema;
using NacGroup.Module.Core.Data;
using NacGroup.Module.Core.Interfaces;

namespace NacGroup.Module.Blog.Data
{
    public interface IBlogCategoryRepository : IRepository<BlogCategory>
    {
        IEnumerable<BlogCategory> GetChildrenCategories(string parentId);
    }

    public class BlogCategoryRepository : MongoRepository<BlogCategory>, IBlogCategoryRepository, IMigrationRepository
    {
        public BlogCategoryRepository(IMongoContext context) : base(context)
        {
   
        }
        public void Migration()
        {
            DbSet.Indexes.CreateOne(model: new CreateIndexModel<BlogCategory>("{Url:1}", new CreateIndexOptions { Unique = true }));
            DbSet.Indexes.CreateOne(model: new CreateIndexModel<BlogCategory>("{ParentId:1}"));
        }

        public void Seed()
        {

        }

        public IEnumerable<BlogCategory> GetChildrenCategories(string parentId)
        {
            return Get(m => m.ParentId == parentId);
        }
    }
}
