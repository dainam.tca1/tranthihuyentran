﻿using System.Collections.Generic;
using NacGroup.Module.Core.Data;
using NacGroup.Module.Core.Models;
using NacGroup.Module.StaticPage.Data;

namespace NacGroup.Module.StaticPage.Services
{
    public interface IStaticPageService
    {
        PagedResult<Models.Schema.StaticPage> Get(int page, int pagesize);
        ApiResponseModel Delete(List<string> idlst);
        ApiResponseModel Delete(string id);
        Models.Schema.StaticPage GetById(string id);
        ApiResponseModel Add(Models.Schema.StaticPage model);

        PagedResult<Models.Schema.StaticPage> Filter(string name, int page, int pagesize);
        ApiResponseModel Update(Models.Schema.StaticPage model);
    }

    public class StaticPageService : IStaticPageService
    {
        private readonly IStaticPageRepository _staticPageRepository;
        private readonly IUnitOfWork _unitOfWork;

        public StaticPageService(IStaticPageRepository staticPageRepository, IUnitOfWork unitOfWork)
        {
            _staticPageRepository = staticPageRepository;
            _unitOfWork = unitOfWork;
        }

        public PagedResult<Models.Schema.StaticPage> Get(int page, int pagesize) =>
        _staticPageRepository.Get(page, pagesize);

        public ApiResponseModel Delete(List<string> idlst)
        {
            if (idlst == null)
                return new ApiResponseModel("error", StaticMessage.SelectDataToRemove);
            foreach (var item in idlst)
            {
                Delete(item);
            }
            return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
        }

        public ApiResponseModel Delete(string id)
        {
            var resultcode = _staticPageRepository.Delete(id);
            if (resultcode < 0)
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            resultcode = _unitOfWork.Commit();
            if (resultcode < 0)
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
        }

        public Models.Schema.StaticPage GetById(string id)
        {
            return _staticPageRepository.GetById(id);
        }

        public ApiResponseModel Add(Models.Schema.StaticPage model)
        {
            if (string.IsNullOrEmpty(model.Name)) return new ApiResponseModel("error", "Page name is required");
            if (string.IsNullOrEmpty(model.Url)) return new ApiResponseModel("error", "Page url is required");

            var oldcate = _staticPageRepository.GetByUrl(model.Url);
            if (oldcate != null)
                return new ApiResponseModel("error", "Url is exist");
            try
            {
                var result = _staticPageRepository.Add(model);
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                result = _unitOfWork.Commit();
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }

        public PagedResult<Models.Schema.StaticPage> Filter(string name, int page, int pagesize)
        {
            return _staticPageRepository.Filter(name, page, pagesize);
        }

        public ApiResponseModel Update(Models.Schema.StaticPage model)
        {
            if (string.IsNullOrEmpty(model.Name)) return new ApiResponseModel("error", "Page name is required");
            if (string.IsNullOrEmpty(model.Url)) return new ApiResponseModel("error", "Page url is required");

            var temp = _staticPageRepository.GetById(model.Id);
            if (temp == null)
                return new ApiResponseModel("error", StaticMessage.DataNotFound);

            var oldcate = _staticPageRepository.GetByUrl(model.Url);
            if (oldcate != null && temp.Id != oldcate.Id)
                return new ApiResponseModel("error", "Url is exist");
            try
            {

                temp.Avatar = model.Avatar;
                temp.IsHtml = model.IsHtml;
                temp.IsCustomerPage = model.IsCustomerPage;
                temp.MetaDescription = model.MetaDescription;
                temp.MetaKeyword = model.MetaKeyword;
                temp.MetaTitle = model.MetaTitle;
                temp.Name = model.Name;
                temp.PageContent = model.PageContent;
                temp.Url = model.Url;
                var result = _staticPageRepository.Update(model.Id, temp);
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                result = _unitOfWork.Commit();
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }

        //public ApiResponseModel CreateStaticPage(Models.StaticPage model)
        //{
        //    #region Validate
        //    try
        //    {
        //        var oldmodel = _staticpages.Find(m => m.Url == model.Url).FirstOrDefault();
        //        if (oldmodel != null)
        //        {
        //            return new ApiResponseModel("error", "Url is exist");
        //        }
        //        _staticpages.InsertOne(model);
        //        return new ApiResponseModel("success", "Update data successfully");
        //    }
        //    catch (System.Exception e)
        //    {
        //        return new ApiResponseModel("error", "System Error");
        //    }

        //    #endregion
        //}

    }
}
