﻿using NacGroup.Module.StaticPage.Data;

namespace NacGroup.Module.StaticPage.Services
{
    public interface IStaticPageWebService
    {
        Models.Schema.StaticPage GetByUrl(string url);

    }

    public class StaticPageWebService : IStaticPageWebService
    {
        private readonly IStaticPageRepository _staticPageRepository;

        public StaticPageWebService(IStaticPageRepository staticPageRepository)
        {
            _staticPageRepository = staticPageRepository;
        }

        public Models.Schema.StaticPage GetByUrl(string url)
        {
            return _staticPageRepository.GetSingle(m => m.Url == url);
        }
    }
}
