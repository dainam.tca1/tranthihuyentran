﻿using MongoDB.Driver;
using NacGroup.Infrastructure.Shared;
using NacGroup.Module.Core.Data;
using NacGroup.Module.Core.Interfaces;
using NacGroup.Module.Core.Models;

namespace NacGroup.Module.StaticPage.Data
{
    public interface IStaticPageRepository : IRepository<Models.Schema.StaticPage>
    {
        Models.Schema.StaticPage GetByUrl(string url);

        PagedResult<Models.Schema.StaticPage> Filter(string title, int page, int pagesize);

    }

    public class StaticPageRepository : MongoRepository<Models.Schema.StaticPage>, IStaticPageRepository, IMigrationRepository
    {
        public StaticPageRepository(IMongoContext context) : base(context)
        {
   
        }

        public void Migration()
        {
            DbSet.Indexes.CreateOne(model: new CreateIndexModel<Models.Schema.StaticPage>("{Url:1}", new CreateIndexOptions { Unique = true }));
        }

        public void Seed()
        {
            
        }

        public PagedResult<Models.Schema.StaticPage> Filter(string title, int page, int pagesize)
        {
            var filter = Builders<Models.Schema.StaticPage>.Filter;
            var filterdefine = filter.Empty;
            if (!string.IsNullOrEmpty(title))
            {
                var titurl = title.ToUrl();
                filterdefine = filterdefine & filter.Where(m => m.Url.Contains(titurl));
            }
            var totalItem = DbSet.CountDocuments(filterdefine);
            var all = DbSet.Find(filterdefine).Skip((page - 1) * pagesize).Limit(pagesize);
            return new PagedResult<Models.Schema.StaticPage>(all.ToList(), page, pagesize, (int)totalItem);
        }

        public Models.Schema.StaticPage GetByUrl(string url)
        {
            var all = DbSet.Find(m => m.Url == url);
            return all.FirstOrDefault();
        }

        
    }
}
