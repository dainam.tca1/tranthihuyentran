﻿using System;
using System.IO;
using Microsoft.AspNetCore.Mvc;
using NacGroup.Infrastructure.Shared;
using NacGroup.Module.StaticPage.Services;

namespace NacGroup.Module.StaticPage.Controllers
{
    public class StaticPageController : Controller
    {
        private readonly IStaticPageWebService _staticPageWebService;

        public StaticPageController(IStaticPageWebService staticPageWebService)
        {
            _staticPageWebService = staticPageWebService;
        }

        [Route("page/{url}")]
        public IActionResult Index(string url)
        {
           
            if (string.IsNullOrEmpty(url))
                throw new Exception();
            var staticpage = _staticPageWebService.GetByUrl(url);
            if (staticpage == null)
                throw new Exception();
            if (staticpage.IsCustomerPage == true)
            {
                return View(staticpage.Url, staticpage);
            }
            return View(staticpage);
        }
    }
}