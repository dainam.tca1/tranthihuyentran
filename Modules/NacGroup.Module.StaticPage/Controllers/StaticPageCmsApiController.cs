﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NacGroup.Module.Core.Models;
using NacGroup.Module.StaticPage.Services;

namespace NacGroup.Module.StaticPage.Controllers
{
    [Authorize(Roles = "Admin", Policy = "TenantAuthorize")]
    [Route("cms/api/staticpage/{action=Index}")]
    public class StaticPageCmsApiController : Controller
    {
        private readonly IStaticPageService _staticPageService;

        public StaticPageCmsApiController(IStaticPageService staticPageService)
        {
            _staticPageService = staticPageService;
        }

        [HttpGet]
        public ActionResult<ApiResponseModel> Index(string name,int page = 1, int pagesize = 10) =>
            new ApiResponseModel("success", _staticPageService.Filter(name,page, pagesize));

        [HttpPost]
        public ActionResult<ApiResponseModel> RemoveList([FromBody] List<string> idList)
        {
            return _staticPageService.Delete(idList);
        }

        [HttpPost]
        public ActionResult<ApiResponseModel> Add([FromBody] Models.Schema.StaticPage model)
        {
            return _staticPageService.Add(model);
        }

        public ActionResult<ApiResponseModel> GetUpdate(string id)
        {
            var item = _staticPageService.GetById(id);
            if (item == null)
                return new ApiResponseModel("error", StaticMessage.DataNotFound);

            return new ApiResponseModel("success", item);
        }
        [HttpPost]
        public ActionResult<ApiResponseModel> Update([FromBody] Models.Schema.StaticPage model)
        {
            return _staticPageService.Update(model);
        }
    }
}
