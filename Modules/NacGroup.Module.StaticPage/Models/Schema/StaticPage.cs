﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using NacGroup.Module.Core.Models;

namespace NacGroup.Module.StaticPage.Models.Schema
{
    [BsonIgnoreExtraElements]
    public class StaticPage : Entity
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("Name")]
        public string Name { get; set; }

        [BsonElement("PageContent")]
        public string PageContent { get; set; }

        [BsonElement("MetaKeyword")]
        public string MetaKeyword { get; set; }

        [BsonElement("MetaDescription")]
        public string MetaDescription { get; set; }

        [BsonElement("MetaTitle")]
        public string MetaTitle { get; set; }

        [BsonElement("Url")]
        public string Url { get; set; }

        [BsonElement("IsCustomerPage")]
        public bool IsCustomerPage { get; set; }

        [BsonElement("IsHtml")]
        public bool IsHtml { get; set; }

        [BsonElement("Avatar")]
        public string Avatar { get; set; }

    }
}
