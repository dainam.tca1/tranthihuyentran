﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using NacGroup.Module.Core.Interfaces;
using NacGroup.Module.StaticPage.Data;
using NacGroup.Module.StaticPage.Services;

namespace NacGroup.Module.StaticPage
{
    public class ModuleInitializer : IModuleInitializer
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddTransient<IStaticPageRepository, StaticPageRepository>();
            services.AddTransient<IStaticPageWebService, StaticPageWebService>();
            services.AddTransient<IStaticPageService, StaticPageService>();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
        }
    }
}
