﻿
namespace NacGroup.Module.Contact.Models.Dtos
{

    public class ContactIndexDto
    {
        public string FullName { get; set; }
        private string _email;
        public string Email
        {
            get => _email;
            set => _email = value?.ToLower();
        }
        public string PhoneNumber { get; set; }
        public string Address { get; set; }
        public string Note { get; set; }
    }
}
