﻿
namespace NacGroup.Module.Contact.Models.Schema
{
   
    public class ContactDto 
    {
        public string FullName { get; set; }
        private string _email;
        public string Email
        {
            get => _email;
            set => _email = value?.ToLower();
        }
        public string Passport { get; set; }
        public string PhoneNumber { get; set; }
        public string City { get; set; }
        public string Salary { get; set; }
        public string PhoneNumberHome { get; set; }
        public string AmountToBorrow { get; set; }
        public string InCome { get; set; }
    }
}
