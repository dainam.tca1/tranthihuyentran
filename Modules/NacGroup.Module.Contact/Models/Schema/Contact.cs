﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using NacGroup.Module.Core.Models;

namespace NacGroup.Module.Contact.Models.Schema
{
    [BsonIgnoreExtraElements]
    public class Contact : Entity,ITrackingUpdate
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("Name")]
        public string Name { get; set; }

        private string _email;

        [BsonElement("Email")]
        public string Email
        {
            get => _email;
            set => _email = value?.ToLower();
        }


        [BsonElement("PhoneNumber")]
        public string PhoneNumber { get; set; }

        [BsonElement("Title")]
        public string Title { get; set; }

        [BsonElement("Body")]
        public string Body { get; set; }

        [BsonElement("CreatedDate")]
        public DateTime CreatedDate { get; set; }

        [BsonElement("UpdatedDate")]
        public DateTime UpdatedDate { get; set; }

        [BsonElement("Alovay247")]
        public AloVay247 AloVay247{ get; set; }

        [BsonElement("Status")]
        public ContactStatus Status { get; set; }
        [BsonElement("ClientGenId")]
        public Guid ClientGenId { get; set; }

        public object this[string propertyName]
        {
            get => this.GetType().GetProperty(propertyName).GetValue(this, null);
            set => this.GetType().GetProperty(propertyName).SetValue(this, value, null);
        }
    }

    public class AloVay247
    {
        public string Passport { get; set; }
        public string City { get; set; }
        public string Salary { get; set; }
        public string PhoneNumberHome { get; set; }
        public string AmountToBorrow { get; set; }
        public string InCome { get; set; }
    }
    public enum ContactStatus
    {
        NeverSeen,
        Seen,
        Done
    }
}
