﻿using Microsoft.AspNetCore.Mvc;
using NacGroup.Module.Contact.Models.Dtos;
using NacGroup.Module.Contact.Models.Schema;
using NacGroup.Module.Contact.Services;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Core.Services;


namespace NacGroup.Module.Contact.Controllers
{
    [Route("contact-us/{action=Index}")]
    public class ContactController : Controller
    {
        private readonly IContactWebService _contactWebService;
        private readonly GlobalConfiguration _globalConfiguration;

        public ContactController(IContactWebService contactWebService, IGettingGlobalConfigService gettingGlobalConfigService)
        {
            _contactWebService = contactWebService;
            _globalConfiguration = gettingGlobalConfigService.GetConfiguration();
        }
        public IActionResult Index()
        {
            return View();
        }

       
        [HttpPost]
        public ActionResult<ApiResponseModel> SubmitForm([Bind("Name,Email,PhoneNumber,Title,Body")] Models.Schema.Contact model)
        {
            var result = _contactWebService.Add(model, Request.Form["g-recaptcha-response"]);
            if (result.status == "error")
            {
                return new ApiResponseModel("error", result.message);
            }
          
            return new ApiResponseModel("success","Contact Successfully");
        }

        [HttpPost]
        public ActionResult<ApiResponseModel> Contact([Bind("FullName,Email,PhoneNumber,Address,Note")] ContactIndexDto model)
        {
            var result = _contactWebService.Add(model, Request.Form["g-recaptcha-response"]);
            if (result.status == "error")
            {
                return new ApiResponseModel("error", result.message);
            }

            return new ApiResponseModel("success", "Contact Successfully");
        }


        [HttpPost]
        public ActionResult<ApiResponseModel> SubmitContactProduct([Bind("PhoneNumber,Title,Body")] Models.Schema.Contact model)
        {
            var result = _contactWebService.AddContactProduct(model);
            if (result.status == "error")
            {
                return new ApiResponseModel("error", result.message);
            }

            return new ApiResponseModel("success", "Contact Successfully");
        }

        [HttpPost]
        public ActionResult<ApiResponseModel> RegisterInterest([Bind("FullName,PhoneNumber,Email,Passport,PhoneNumberHome,Salary,City,AmountToBorrow,InCome")] ContactDto model)
        {
            var result = _contactWebService.AddContactRegister(model, Request.Form["g-recaptcha-response"]);
            if (result.status == "error") return new ApiResponseModel("error", result.message);
            return new ApiResponseModel("success", "Contact Successfully");
        }
    }
}