﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NacGroup.Module.Contact.Services;
using NacGroup.Module.Core.Models;
using Newtonsoft.Json.Linq;

namespace NacGroup.Module.Contact.Controllers
{
    [Authorize(Roles = "Admin", Policy = "TenantAuthorize")]
    [Route("cms/api/contact/{action=Index}")]
    public class ContactCmsApiController : Controller
    {
        private readonly IContactService _contactService;


        public ContactCmsApiController(IContactService contactService)
        {
            _contactService = contactService;

        }

        [HttpGet]
        public ActionResult<ApiResponseModel> Index(string name, string email, string phone, int? status, string fromdate, string todate, int page = 1, int pagesize = 10)
        {
            return new ApiResponseModel("success",
                _contactService.Filter(name, email, phone, status, fromdate, todate, page, pagesize));
        }

        [HttpGet]
        public ActionResult<ApiResponseModel> Get()
        {
            return new ApiResponseModel("success",
                _contactService.Get());
        }

        [HttpPost]
        public ActionResult<ApiResponseModel> RemoveList([FromBody] List<string> idList)
        {
            return _contactService.Delete(idList);
        }

        public ActionResult<ApiResponseModel> Update(string id)
        {
            var contact = _contactService.GetById(id);
            if (contact == null) return new ApiResponseModel("error", StaticMessage.DataNotFound);
            return new ApiResponseModel("success", StaticMessage.DataLoadingSuccessfull, contact);
        }

        [HttpPost]
        public ActionResult<ApiResponseModel> UpdateCustomize([FromBody]JObject data)
        {
            var model = data["model"].ToObject<Models.Schema.Contact>();
            var name = data["name"].Value<string>();
            return _contactService.UpdateCustomize(model, name);
        }

    }
}