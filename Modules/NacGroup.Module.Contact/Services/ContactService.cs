﻿using System;
using System.Collections.Generic;
using NacGroup.Module.Contact.Data;
using NacGroup.Module.Core.Data;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Core.Services;
using Newtonsoft.Json.Linq;
using System.Linq;

namespace NacGroup.Module.Contact.Services
{
    public interface IContactService
    {
        ApiResponseModel Delete(List<string> idlst);
        ApiResponseModel Delete(string id);
        Models.Schema.Contact GetById(string id);
        PagedResult<Models.Schema.Contact> Filter(string name, string email, string phone, int? status, string fromdate, string todate, int page, int pagesize);
        ApiResponseModel UpdateCustomize(Models.Schema.Contact model, string name);

        List<Models.Schema.Contact> Get();
    }

    public class ContactService : IContactService
    {
        private readonly IContactRepository _contactRepository;
        private readonly string _dateFormat;
        private readonly IUnitOfWork _unitOfWork;
        private readonly GlobalConfiguration _globalConfiguration;
        public ContactService(IContactRepository contactRepository, IUnitOfWork unitOfWork, IGettingGlobalConfigService gettingGlobalConfigService)
        {
            _contactRepository = contactRepository;
            _unitOfWork = unitOfWork;
            _globalConfiguration = gettingGlobalConfigService.GetConfiguration();
            _dateFormat = _globalConfiguration.GlobalWebSetting["WebsiteConfig"]["DateCShapeFormat"].Value<string>();
        }


        public ApiResponseModel Delete(List<string> idlst)
        {
            if (idlst == null)
                return new ApiResponseModel("error", StaticMessage.SelectDataToRemove);
            foreach (var item in idlst)
            {
                Delete(item);
            }
            return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
        }

        public List<Models.Schema.Contact> Get()
        {
            return _contactRepository.Get().ToList();
        }

        public ApiResponseModel Delete(string id)
        {
            var resultcode = _contactRepository.Delete(id);
            if (resultcode < 0)
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            resultcode = _unitOfWork.Commit();
            if (resultcode < 0)
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
        }

        public Models.Schema.Contact GetById(string id)
        {
            return _contactRepository.GetById(id);
        }

        public PagedResult<Models.Schema.Contact> Filter(string name, string email, string phone, int? status, string fromdate, string todate, int page, int pagesize)
        {
            DateTime fromDate = DateTime.MinValue, toDate = DateTime.MaxValue;
            if (!string.IsNullOrEmpty(fromdate))
            {
                var isDate = DateTime.TryParseExact(fromdate, _dateFormat, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out fromDate);
                if (!isDate) return new PagedResult<Models.Schema.Contact>();
            }
            if (!string.IsNullOrEmpty(todate))
            {
                var isDate = DateTime.TryParseExact(todate, _dateFormat, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out toDate);
                if (!isDate) return new PagedResult<Models.Schema.Contact>();
            }

            return _contactRepository.Filter(name, email, phone, status, fromDate, toDate, page, pagesize);
        }

        public ApiResponseModel UpdateCustomize(Models.Schema.Contact model, string name)
        {
            try
            {
                if (model == null || string.IsNullOrEmpty(name)) return new ApiResponseModel("error", StaticMessage.DataNotFound);

                var temp = _contactRepository.GetById(model.Id);
                if (temp == null) return new ApiResponseModel("error", StaticMessage.DataNotFound);

                temp[name] = model[name];
                var result = _contactRepository.Update(model.Id, temp);
                if (result < 0) return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);

                result = _unitOfWork.Commit();
                if (result < 0) return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }
    }
}
