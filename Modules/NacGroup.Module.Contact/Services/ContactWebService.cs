﻿using System;
using MongoDB.Bson;
using NacGroup.Infrastructure.Shared;
using NacGroup.Module.Contact.Data;
using NacGroup.Module.Contact.Models.Dtos;
using NacGroup.Module.Contact.Models.Schema;
using NacGroup.Module.Core.Data;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Core.Services;
using Newtonsoft.Json.Linq;

namespace NacGroup.Module.Contact.Services
{
    public interface IContactWebService
    {
        ApiResponseModel Add(Models.Schema.Contact model, string captcha);
        ApiResponseModel Add(ContactIndexDto contactIndexDto, string captcha);
        ApiResponseModel AddContactProduct(Models.Schema.Contact model);
        ApiResponseModel AddContactRegister(ContactDto contactDto, string captcha);
    }

    public class ContactWebService : IContactWebService
    {
        private readonly IContactRepository _contactRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly ISendMailService _sendMailService;
        private readonly IViewRenderService _viewRenderService;
        private readonly GlobalConfiguration _globalConfiguration;
        public ContactWebService(IContactRepository contactRepository, IUnitOfWork unitOfWork, ISendMailService sendMailService, IViewRenderService viewRenderService, IGettingGlobalConfigService gettingGlobalConfigService)
        {
            _contactRepository = contactRepository;
            _unitOfWork = unitOfWork;
            _sendMailService = sendMailService;
            _viewRenderService = viewRenderService;
            _globalConfiguration = gettingGlobalConfigService.GetConfiguration();
        }
        public ApiResponseModel AddContactRegister(ContactDto contactDto, string captcha)
        {
            try
            {
                #region Validate
                if (contactDto == null)
                    return new ApiResponseModel("error", StaticMessage.ParameterInvalid);
                if (string.IsNullOrEmpty(contactDto.FullName))
                    return new ApiResponseModel("error", "Vui lòng nhập đầy đủ họ và tên");
                if (string.IsNullOrEmpty(contactDto.PhoneNumber))
                    return new ApiResponseModel("error", "Vui lòng nhập số điện thoại");
                if (string.IsNullOrEmpty(contactDto.City))
                    return new ApiResponseModel("error", "Vui lòng nhập tỉnh thành đang cư trú");
                //if (string.IsNullOrEmpty(contactDto.Salary))
                //    return new ApiResponseModel("error", "Vui lòng nhập thu nhập hàng tháng");
                if (string.IsNullOrEmpty(contactDto.InCome))
                    return new ApiResponseModel("error", "Vui lòng chọn nguồn thu nhập");
                //if (string.IsNullOrEmpty(contactDto.PhoneNumberHome))
                //    return new ApiResponseModel("error", "Vui lòng nhập số điện thoại nhà hoặc công ty");
                //if (string.IsNullOrEmpty(contactDto.Email))
                //    return new ApiResponseModel("error", "Vui lòng nhập địa chỉ email liên hệ");
                //if (!contactDto.Email.IsEmail())
                //    return new ApiResponseModel("error", "Địa chỉ email không hợp lệ");
                //if (string.IsNullOrEmpty(contactDto.Passport))
                //    return new ApiResponseModel("error", "Vui lòng nhập số CMND");
                if (string.IsNullOrEmpty(contactDto.AmountToBorrow))
                    return new ApiResponseModel("error", "Vui lòng nhập số tiền cần vay");
                if (string.IsNullOrEmpty(captcha)) return new ApiResponseModel("error", "Vui lòng xác nhận tôi không phải là robot ");
                var privateKey = _globalConfiguration.GlobalWebSetting["WebsiteConfig"]["GcaptchaPrivate"];

                string URI = "https://www.google.com/recaptcha/api/siteverify";
                string myParameters = "secret=" + privateKey + "&response=" + captcha;

                using (System.Net.WebClient wc = new System.Net.WebClient())
                {
                    wc.Headers[System.Net.HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                    string HtmlResult = wc.UploadString(URI, myParameters);
                    var json = JObject.Parse(HtmlResult);
                    if (!json["success"].Value<bool>())
                    {
                        return new ApiResponseModel("error", "Please confirm captcha");
                    }
                }
                var contactId = Guid.NewGuid();
                while (_contactRepository.GetSingle(m => m.ClientGenId == contactId) != null)
                {
                    contactId = Guid.NewGuid();
                }
                #endregion

                var temp = new Models.Schema.Contact
                {
                    PhoneNumber = contactDto.PhoneNumber,
                    Status = ContactStatus.NeverSeen,
                    Title = "Đăng kí vay tiêu dùng",
                    Name = contactDto.FullName,
                    Email = contactDto.Email,
                    AloVay247 = new AloVay247
                    {
                        Passport = contactDto.Passport,
                        PhoneNumberHome = contactDto.PhoneNumberHome,
                        City = contactDto.City,
                        Salary = contactDto.Salary,
                        AmountToBorrow = contactDto.AmountToBorrow,
                        InCome = contactDto.InCome
                    },
                    ClientGenId = contactId
                };

                var result = _contactRepository.Add(temp);
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                result = _unitOfWork.Commit();
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);

                var mailmessage = _viewRenderService.RenderToStringAsync("_ContactMail", new
                {
                    WebsiteName = _globalConfiguration.GlobalWebSetting["WebInfomation"]["WebsiteName"].ToString(),
                    model = temp
                }).Result;

                _sendMailService.SendMail(new EmailModel(_globalConfiguration.SendMailUserName,
                    _globalConfiguration.SendMailPassword, _globalConfiguration.GlobalWebSetting["WebInfomation"]["WebsiteNotification"].ToString(),
                    $"Liên hệ mới từ {_globalConfiguration.GlobalWebSetting["WebInfomation"]["WebsiteName"]}",
                    mailmessage, _globalConfiguration.GlobalWebSetting["WebInfomation"]["WebsiteName"].ToString()));

                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully, contactId);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }
        public ApiResponseModel Add(Models.Schema.Contact model, string captcha)
        {
            try
            {
                #region Validate
                if (model == null)
                    return new ApiResponseModel("error", StaticMessage.ParameterInvalid);
                if (string.IsNullOrEmpty(model.Name))
                    return new ApiResponseModel("error", "Họ và tên bắt buộc");

                if (string.IsNullOrEmpty(model.Email))
                    return new ApiResponseModel("error", "Địa chỉ email bắt buộc");
                if (!model.Email.IsEmail())
                    return new ApiResponseModel("error", "Địa chỉ email không phù hợp");
                if (string.IsNullOrEmpty(model.PhoneNumber))
                    return new ApiResponseModel("error", "Số điện thoại bắt buộc");
                if (string.IsNullOrEmpty(model.Title))
                    return new ApiResponseModel("error", "Tiêu đề bắt buộc");
                if (string.IsNullOrEmpty(model.Body))
                    return new ApiResponseModel("error", "Nôi dung liên hệ bắt buộc");
                if (model.Body.Length > 1000)
                    return new ApiResponseModel("error", "Nội dung nhỏ hơn 1000 kí tự");
                if (string.IsNullOrEmpty(captcha)) return new ApiResponseModel("error", "Vui lòng xác nhận tôi không phải là robot ");
                var privateKey = _globalConfiguration.GlobalWebSetting["WebsiteConfig"]["GcaptchaPrivate"];

                string URI = "https://www.google.com/recaptcha/api/siteverify";
                string myParameters = "secret=" + privateKey + "&response=" + captcha;

                using (System.Net.WebClient wc = new System.Net.WebClient())
                {
                    wc.Headers[System.Net.HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                    string HtmlResult = wc.UploadString(URI, myParameters);
                    var json = JObject.Parse(HtmlResult);
                    if (!json["success"].Value<bool>())
                    {
                        return new ApiResponseModel("error", "Please confirm captcha");
                    }
                }
                var contactId = Guid.NewGuid();
                while (_contactRepository.GetSingle(m => m.ClientGenId == contactId) != null)
                {
                    contactId = Guid.NewGuid();
                }
                #endregion

                var temp = new Models.Schema.Contact
                {
                    Id = model.Id,
                    Name = model.Name,
                    Body = model.Body,
                    Email = model.Email.ToLower(),
                    PhoneNumber = model.PhoneNumber,
                    Status = model.Status,
                    Title = model.Title,
                    ClientGenId = contactId
                };

                var result = _contactRepository.Add(temp);
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                result = _unitOfWork.Commit();
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);

                var mailmessage = _viewRenderService.RenderToStringAsync("_ContactMail", new
                {
                    WebsiteName = _globalConfiguration.GlobalWebSetting["WebInfomation"]["WebsiteName"].ToString(),
                    model = temp
                }).Result;

                _sendMailService.SendMail(new EmailModel(_globalConfiguration.SendMailUserName,
                    _globalConfiguration.SendMailPassword, _globalConfiguration.GlobalWebSetting["WebInfomation"]["WebsiteNotification"].ToString(),
                    $"Welcome to {_globalConfiguration.GlobalWebSetting["WebInfomation"]["WebsiteName"]}",
                    mailmessage, _globalConfiguration.GlobalWebSetting["WebInfomation"]["WebsiteName"].ToString()));

                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully, contactId);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }

        }
        public ApiResponseModel Add(ContactIndexDto contactIndexDto, string captcha)
        {
            try
            {
                #region Validate
                if (contactIndexDto == null)
                    return new ApiResponseModel("error", StaticMessage.ParameterInvalid);
                if (string.IsNullOrEmpty(contactIndexDto.FullName))
                    return new ApiResponseModel("error", "Họ và tên bắt buộc");
                if (string.IsNullOrEmpty(contactIndexDto.PhoneNumber))
                    return new ApiResponseModel("error", "Số điện thoại bắt buộc");
                if (string.IsNullOrEmpty(contactIndexDto.Email))
                    return new ApiResponseModel("error", "Địa chỉ email bắt buộc");
                if (!contactIndexDto.Email.IsEmail())
                    return new ApiResponseModel("error", "Địa chỉ email không phù hợp");
                if (!string.IsNullOrEmpty(contactIndexDto.Address) && contactIndexDto.Address.Length > 1000)
                    return new ApiResponseModel("error", "Địa chỉ nhỏ hơn 1000 kí tự");
                if (!string.IsNullOrEmpty(contactIndexDto.Note) && contactIndexDto.Note.Length > 1000)
                    return new ApiResponseModel("error", "Nội dung nhỏ hơn 1000 kí tự");
                if (string.IsNullOrEmpty(captcha)) return new ApiResponseModel("error", "Vui lòng xác nhận tôi không phải là robot ");
                var privateKey = _globalConfiguration.GlobalWebSetting["WebsiteConfig"]["GcaptchaPrivate"];

                string URI = "https://www.google.com/recaptcha/api/siteverify";
                string myParameters = "secret=" + privateKey + "&response=" + captcha;

                using (System.Net.WebClient wc = new System.Net.WebClient())
                {
                    wc.Headers[System.Net.HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                    string HtmlResult = wc.UploadString(URI, myParameters);
                    var json = JObject.Parse(HtmlResult);
                    if (!json["success"].Value<bool>())
                    {
                        return new ApiResponseModel("error", "Please confirm captcha");
                    }
                }
                var contactId = Guid.NewGuid();
                while (_contactRepository.GetSingle(m => m.ClientGenId == contactId) != null)
                {
                    contactId = Guid.NewGuid();
                }
                #endregion

                var temp = new Models.Schema.Contact
                {
                    Name = contactIndexDto.FullName,
                    Body = $"Địa chỉ: {contactIndexDto.Address} | Yêu Cầu : {contactIndexDto.Note}",
                    Email = contactIndexDto.Email.ToLower(),
                    PhoneNumber = contactIndexDto.PhoneNumber,
                    Status = ContactStatus.NeverSeen,
                    Title = "Liên hệ",
                    ClientGenId = contactId,
                };

                var result = _contactRepository.Add(temp);
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                result = _unitOfWork.Commit();
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);

                var mailmessage = _viewRenderService.RenderToStringAsync("_ContactMailT", new
                {
                    WebsiteName = _globalConfiguration.GlobalWebSetting["WebInfomation"]["WebsiteName"].ToString(),
                    model = temp
                }).Result;

                _sendMailService.SendMail(new EmailModel(_globalConfiguration.SendMailUserName,
                    _globalConfiguration.SendMailPassword, _globalConfiguration.GlobalWebSetting["WebInfomation"]["WebsiteNotification"].ToString(),
                    $"Liên hệ mới từ {_globalConfiguration.GlobalWebSetting["WebInfomation"]["WebsiteName"]}",
                    mailmessage, _globalConfiguration.GlobalWebSetting["WebInfomation"]["WebsiteName"].ToString()));

                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully, contactId);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }

        }
        public ApiResponseModel AddContactProduct(Models.Schema.Contact model)
        {
            try
            {
                #region Validate
                if (model == null)
                    return new ApiResponseModel("error", StaticMessage.ParameterInvalid);
                if (string.IsNullOrEmpty(model.PhoneNumber))
                    return new ApiResponseModel("error", "Phone number is required");
                if (string.IsNullOrEmpty(model.Title))
                    return new ApiResponseModel("error", "Title is required");
                if (string.IsNullOrEmpty(model.Body))
                    return new ApiResponseModel("error", "Message is required");
                var contactId = Guid.NewGuid();
                while (_contactRepository.GetSingle(m => m.ClientGenId == contactId) != null)
                {
                    contactId = Guid.NewGuid();
                }
                #endregion

                var temp = new Models.Schema.Contact
                {
                    Id = model.Id,
                    Body = model.Body,
                    PhoneNumber = model.PhoneNumber,
                    Status = model.Status,
                    Title = model.Title,
                    ClientGenId = contactId
                };

                var result = _contactRepository.Add(temp);
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                result = _unitOfWork.Commit();
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);

                var mailmessage = _viewRenderService.RenderToStringAsync("_ContactMail", new
                {
                    WebsiteName = _globalConfiguration.GlobalWebSetting["WebInfomation"]["WebsiteName"].ToString(),
                    model = temp
                }).Result;

                _sendMailService.SendMail(new EmailModel(_globalConfiguration.SendMailUserName,
                    _globalConfiguration.SendMailPassword, _globalConfiguration.GlobalWebSetting["WebInfomation"]["WebsiteNotification"].ToString(),
                    $"Welcome to {_globalConfiguration.GlobalWebSetting["WebInfomation"]["WebsiteName"]}",
                    mailmessage, _globalConfiguration.GlobalWebSetting["WebInfomation"]["WebsiteName"].ToString()));

                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully, contactId);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }

        }
    }
}
