﻿using System;
using MongoDB.Driver;
using NacGroup.Module.Contact.Models.Schema;
using NacGroup.Module.Core.Data;
using NacGroup.Module.Core.Interfaces;
using NacGroup.Module.Core.Models;

namespace NacGroup.Module.Contact.Data
{
    public interface IContactRepository : IRepository<Models.Schema.Contact>
    {

        PagedResult<Models.Schema.Contact> Filter(string name, string email, string phone, int? status, DateTime fromdate, DateTime todate, int page, int pagesize);

    }

    public class ContactRepository : MongoRepository<Models.Schema.Contact>, IContactRepository, IMigrationRepository
    {
        public ContactRepository(IMongoContext context) : base(context)
        {
   
        }



        public PagedResult<Models.Schema.Contact> Filter(string name, string email, string phone, int? status, DateTime fromdate, DateTime todate, int page, int pagesize)
        {
            var filter = Builders<Models.Schema.Contact>.Filter;
            var filterdefine = filter.Empty;
            if (!string.IsNullOrEmpty(name))
            {
                filterdefine &= filter.Where(m => m.Name.Contains(name));
            }
            if (!string.IsNullOrEmpty(email))
            {
                filterdefine &= filter.Where(m => m.Email.Contains(email));
            }
            if (!string.IsNullOrEmpty(phone))
            {
                filterdefine &= filter.Where(m => m.PhoneNumber.Contains(phone));
            }

            if (status.HasValue)
            {
                filterdefine &= filter.Where(m => m.Status == (ContactStatus)status.Value);
            }

            if (fromdate != DateTime.MinValue)
            {
                filterdefine &= filter.Where(m => m.CreatedDate >= fromdate);
            }
            if (todate != DateTime.MaxValue)
            {
                filterdefine &= filter.Where(m => m.CreatedDate <= todate);
            }
            var totalItem = DbSet.CountDocuments(filterdefine);
            var all = DbSet.Find(filterdefine).Skip((page - 1) * pagesize).Limit(pagesize);
            return new PagedResult<Models.Schema.Contact>(all.ToList(), page, pagesize, (int)totalItem);
        }

        public void Migration()
        {
            DbSet.Indexes.CreateOne(model: new CreateIndexModel<Models.Schema.Contact>("{ClientGenId:1}", new CreateIndexOptions { Unique = true }));
        }

        public void Seed()
        {
            // ignore
        }
    }
}
