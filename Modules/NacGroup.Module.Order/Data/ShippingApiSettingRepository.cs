﻿using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using NacGroup.Module.Core.Data;
using NacGroup.Module.Core.Interfaces;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Core.Models.Schema;
using NacGroup.Module.Order.Models.Schema;
using Newtonsoft.Json;

namespace NacGroup.Module.Order.Data
{
    public interface IShippingApiSettingRepository : IRepository<AppSetting>
    {
        AppSetting GetSetting();
    }

    public class ShippingApiSettingRepository : MongoRepository<AppSetting>, IShippingApiSettingRepository, IMigrationRepository
    {
        public ShippingApiSettingRepository(IMongoContext context) : base(context)
        {

        }

        public AppSetting GetSetting()
        {
            return GetSingle(m => m.Key == ConstKey.ShippingApiSettingKey);
        }

        public void Migration()
        {

        }

        public void Seed()
        {
            var shippingKeySetting = GetSingle(m => m.Key == ConstKey.ShippingApiSettingKey);
            if (shippingKeySetting == null)
            {
                shippingKeySetting = new AppSetting
                {
                    Key = ConstKey.ShippingApiSettingKey,
                    Value = BsonSerializer.Deserialize<BsonDocument>(JsonConvert.SerializeObject(new
                    {
                       Type = ShippingType.EasyShip,
                       Used = false
                    }))
                };
                Add(shippingKeySetting);
                Context.SaveChange();
            }
            
        }

    }
}
