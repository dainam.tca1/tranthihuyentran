﻿using MongoDB.Driver;
using NacGroup.Module.Core.Data;
using NacGroup.Module.Core.Interfaces;
using NacGroup.Module.Order.Models.Schema;


namespace NacGroup.Module.Order.Data
{
    public interface IShippingFeeByAreaRepository : IRepository<ShippingFeeByDistrict>
    {

    }
    public class ShippingFeeByAreaRepository : MongoRepository<ShippingFeeByDistrict>, IShippingFeeByAreaRepository, IMigrationRepository
    {
        public ShippingFeeByAreaRepository(IMongoContext context) : base(context)
        {
        }

        public void Migration()
        {
           // DbSet.Indexes.CreateOne(model: new CreateIndexModel<ShippingFeeByDistrict>("{PriceFrom:1}"));

        }

        public void Seed()
        {
           
        }
       

    }
}
