﻿using MongoDB.Driver;
using NacGroup.Module.Core.Data;
using NacGroup.Module.Core.Interfaces;
using NacGroup.Module.Order.Models.Schema;

namespace NacGroup.Module.Order.Data
{
    public interface IShippingServiceDistrictRepository : IRepository<DistrictCollection>
    {
        DistrictCollection GetByCode(string code);

    }
    public class ShippingServiceDistrictRepository : MongoRepository<DistrictCollection>, IShippingServiceDistrictRepository
    {
        public ShippingServiceDistrictRepository(IMongoContext context) : base(context)
        {
        }

        public void Seed()
        {

        }
        public DistrictCollection GetByCode(string code)
        {
            return GetSingle(m => m.Code == code);

        }

    }
}
