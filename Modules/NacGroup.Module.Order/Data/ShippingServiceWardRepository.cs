﻿using MongoDB.Driver;
using NacGroup.Module.Core.Data;
using NacGroup.Module.Core.Interfaces;
using NacGroup.Module.Order.Models.Schema;

namespace NacGroup.Module.Order.Data
{
    public interface IShippingServiceWardRepository : IRepository<WardCollection>
    {

    }
    public class ShippingServiceWardRepository : MongoRepository<WardCollection>, IShippingServiceWardRepository
    {
        public ShippingServiceWardRepository(IMongoContext context) : base(context)
        {
        }

        public void Seed()
        {

        }

    }
}
