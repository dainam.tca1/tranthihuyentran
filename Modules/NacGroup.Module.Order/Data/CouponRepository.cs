﻿using MongoDB.Driver;
using NacGroup.Module.Core.Data;
using NacGroup.Module.Core.Interfaces;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Order.Models.Schema;

namespace NacGroup.Module.Order.Data
{
    public interface ICouponRepository : IRepository<Coupon>
    {
        PagedResult<Coupon> Filter(string couponCode, bool? isActive, int page, int pagesize);
        Coupon GetByCode(string code);
    }

    public class CouponRepository : MongoRepository<Coupon>, ICouponRepository, IMigrationRepository
    {
        public CouponRepository(IMongoContext context) : base(context)
        {
        }

        public void Seed()
        {

        }

        public void Migration()
        {
            DbSet.Indexes.CreateOne(model: new CreateIndexModel<Coupon>("{CouponCode:1}", new CreateIndexOptions { Unique = true }));
        }


        public PagedResult<Coupon> Filter(string couponCode, bool? isActive, int page, int pagesize)
        {
            var filter = Builders<Coupon>.Filter;
            var filterdefine = filter.Empty;
            if (!string.IsNullOrEmpty(couponCode))
            {
                filterdefine &= filter.Where(m => m.CouponCode.Contains(couponCode));
            }

            if (isActive.HasValue)
            {
                filterdefine &= filter.Where(m => m.IsActive == isActive.Value);
            }

            var totalItem = DbSet.CountDocuments(filterdefine);
            var all = DbSet.Find(filterdefine).Skip((page - 1) * pagesize).Limit(pagesize);
            return new PagedResult<Coupon>(all.ToList(), page, pagesize, (int)totalItem);
        }

        public Coupon GetByCode(string code)
        {
            return GetSingle(m => m.CouponCode == code);
        }
    }
}
