﻿using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using NacGroup.Module.Core.Data;
using NacGroup.Module.Core.Interfaces;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Core.Models.Schema;
using NacGroup.Module.Order.Models;
using Newtonsoft.Json;

namespace NacGroup.Module.Order.Data
{
    public interface IEasyShipApiSettingRepository : IRepository<AppSetting>
    {
        AppSetting GetSetting();
    }

    public class EasyShipApiSettingRepository : MongoRepository<AppSetting>, IEasyShipApiSettingRepository, IMigrationRepository
    {
        public EasyShipApiSettingRepository(IMongoContext context) : base(context)
        {

        }

        public AppSetting GetSetting()
        {
            return GetSingle(m => m.Key == ConstKey.EasyShipApiSettingKey);
        }

        public void Migration()
        {

        }

        public void Seed()
        {
            var easySetting = GetSingle(m => m.Key == ConstKey.EasyShipApiSettingKey);
            if (easySetting == null)
            {
                easySetting = new AppSetting
                {
                    Key = ConstKey.EasyShipApiSettingKey,
                    Value = BsonSerializer.Deserialize<BsonDocument>(JsonConvert.SerializeObject(new
                    {
                        Test = new EasyShipApiConfig
                        { 
                            ApiRateUrl = "https://api.easyship.com/rate/v1/rates",
                            ApiShipmentUrl = "https://api.easyship.com/shipment/v1/shipments",
                            AccessToken = "sand_90phEUYUtY7MzICmv+UWjgjCu71hFCyvkDch8MMMpzM="
                        },
                        Live = new EasyShipApiConfig
                        {
                            ApiRateUrl = "https://api.easyship.com/rate/v1/rates",
                            ApiShipmentUrl = "https://api.easyship.com/shipment/v1/shipments",
                            AccessToken = "prod_ktNcjglIUB2viuhMNDZAnFOQYGfP+M1Zvqy7tHZaXnE="
                        }
                    }))
                };
                Add(easySetting);
                Context.SaveChange();
            }
            
        }

    }
}
