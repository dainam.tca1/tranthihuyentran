﻿using MongoDB.Driver;
using NacGroup.Module.Core.Data;
using NacGroup.Module.Core.Interfaces;
using NacGroup.Module.Order.Models.Schema;

namespace NacGroup.Module.Order.Data
{
    public interface IOrderUserProfileRepository : IRepository<OrderUserProfile>
    {

    }

    public class OrderUserProfileRepository : MongoRepository<OrderUserProfile>, IOrderUserProfileRepository, IMigrationRepository
    {
        public OrderUserProfileRepository(IMongoContext context) : base(context)
        {
        }

        public void Seed()
        {

        }

        public void Migration()
        {
            DbSet.Indexes.CreateOne(model: new CreateIndexModel<OrderUserProfile>("{UserProfileId:1}", new CreateIndexOptions { Unique = true }));
        }


    }
}
