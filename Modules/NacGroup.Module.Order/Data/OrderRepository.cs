﻿using MongoDB.Driver;
using NacGroup.Module.Core.Data;
using NacGroup.Module.Core.Interfaces;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Order.Models;
using NacGroup.Module.Order.Models.Schema;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NacGroup.Module.Order.Data
{
    public interface IOrderRepository : IRepository<Models.Schema.Order>
    {
        PagedResult<Models.Schema.Order> GetOrderByCustomerId(string cusid, int page, int pagesize);
        PagedResult<Models.Schema.Order> Filter(string ordernumber, string customername, string phone, int? status, DateTime fromdate, DateTime todate, int page, int pagesize);

        int GetToTalOrderCount();
        int GetToTalOrderCount(DateTime timefrom, DateTime timeto, OrderStatus? orderStatus);
        decimal GetTotalRevenue(OrderStatus? orderStatus);
        decimal GetTotalRevenue(DateTime timefrom, DateTime timeto, OrderStatus? orderStatus);
        List<OrderItem> GetListOrder(DateTime timefrom, DateTime timeto, OrderStatus? orderStatus, int page, int pagesize);
    }


    public class OrderRepository : MongoRepository<Models.Schema.Order>, IOrderRepository, IMigrationRepository
    {
        public OrderRepository(IMongoContext context) : base(context)
        {
        }

        public void Seed()
        {

        }

        public void Migration()
        {
            DbSet.Indexes.CreateOne(model: new CreateIndexModel<Models.Schema.Order>("{CustomerId:1}"));
        }

        public decimal GetTotalRevenue(OrderStatus? orderstatus)
        {
            if (!orderstatus.HasValue)
            {
                return Get().Select(m => m.TotalAmount).DefaultIfEmpty(0).Sum();
            }
            else
            {
                return Get(m => m.OrderStatus == OrderStatus.ORDER_ARCHIVED).Select(m => m.TotalAmount).DefaultIfEmpty(0).Sum();
            }
        }

        public int GetToTalOrderCount()
        {
            return Get().Count();
        }

        public int GetToTalOrderCount(DateTime timefrom, DateTime timeto, OrderStatus? orderStatus)
        {
            if (!orderStatus.HasValue)
            {
                return Get(m => m.CreatedDate >= timefrom && m.CreatedDate <= timeto).Count();
            }
            else
            {
                return Get(m => m.OrderStatus == orderStatus.Value && m.CreatedDate >= timefrom && m.CreatedDate <= timeto).Count();
            }
        }

        public decimal GetTotalRevenue(DateTime timefrom, DateTime timeto, OrderStatus? orderStatus)
        {
            if (!orderStatus.HasValue)
            {
                return Get(m => m.CreatedDate >= timefrom && m.CreatedDate <= timeto).Sum(m => m.TotalAmount);
            }
            else
            {
                return Get(m => m.OrderStatus == orderStatus.Value && m.CreatedDate >= timefrom && m.CreatedDate <= timeto).Sum(m => m.TotalAmount);
            }

        }

        public List<OrderItem> GetListOrder(DateTime timefrom, DateTime timeto, OrderStatus? orderStatus, int page, int pagesize)
        {
            if (!orderStatus.HasValue)
            {
                var order_list = Get(m => m.CreatedDate >= timefrom && m.CreatedDate <= timeto).ToList();
                var sku_list = new List<OrderItem>();
                if (order_list.Any())
                {
                    foreach(var order in order_list)
                    {
                        if (order.OrderItems.Any())
                        {
                            foreach (var sku in order.OrderItems)
                            {
                                sku_list.Add(sku);
                            }
                        }
                    }
                }
                var result = sku_list.GroupBy(m => m.SkuCode).Select(g => new OrderItem
                {
                    SkuCode = g.Key,
                    Quantity = g.ToList().Sum(m => m.Quantity),
                    Name = g.FirstOrDefault()?.Name,
                    Avatar = g.FirstOrDefault()?.Avatar

                }).Take(5).ToList();

                return result;
            }
            else
            {
                var order_list = Get(m => m.OrderStatus == orderStatus.Value && m.CreatedDate >= timefrom && m.CreatedDate <= timeto).ToList();
                var sku_list = new List<OrderItem>();
                if (order_list.Any())
                {
                    foreach (var order in order_list)
                    {
                        if (order.OrderItems.Any())
                        {
                            foreach (var sku in order.OrderItems)
                            {
                                sku_list.Add(sku);
                            }
                        }
                    }
                }
                var result = sku_list.GroupBy(m => m.SkuCode).Select(g => new OrderItem
                {
                    SkuCode = g.Key,
                    Quantity = g.ToList().Sum(m => m.Quantity),
                    Name = g.FirstOrDefault()?.Name,

                }).Take(5).ToList();

                return result;
            }
        }

        public PagedResult<Models.Schema.Order> GetOrderByCustomerId(string cusid, int page, int pagesize)
        {
            var filter = Builders<Models.Schema.Order>.Filter;
            var filterdefine = filter.Empty;
            if (!string.IsNullOrEmpty(cusid))
            {
                filterdefine &= filter.Where(m => m.CustomerId == cusid);
            }

            var totalItem = DbSet.CountDocuments(filterdefine);
            var all = DbSet.Find(filterdefine).Skip((page - 1) * pagesize).Limit(pagesize).SortByDescending(m => m.CreatedDate);
            return new PagedResult<Models.Schema.Order>(all.ToList(), page, pagesize, (int)totalItem);
        }

        public PagedResult<Models.Schema.Order> Filter(string ordernumber, string customername, string phone, int? status, DateTime fromdate, DateTime todate, int page, int pagesize)
        {
            var filter = Builders<Models.Schema.Order>.Filter;
            var filterdefine = filter.Empty;
            if (!string.IsNullOrEmpty(customername))
            {
                filterdefine &= filter.Where(m => m.FirstName.Contains(customername) || m.LastName.Contains(customername));
            }
            if (!string.IsNullOrEmpty(ordernumber))
            {
                filterdefine &= filter.Where(m => m.OrderNumber.Contains(ordernumber));
            }
            if (!string.IsNullOrEmpty(phone))
            {
                filterdefine &= filter.Where(m => m.CustomerPhone.Contains(phone));
            }

            if (status.HasValue)
            {
                filterdefine &= filter.Where(m => m.OrderStatusNacGroup == (OrderStatusNacGroup)status.Value);
            }

            if (fromdate != DateTime.MinValue)
            {
                filterdefine &= filter.Where(m => m.CreatedDate >= fromdate);
            }
            if (todate != DateTime.MaxValue)
            {
                filterdefine &= filter.Where(m => m.CreatedDate <= todate);
            }
            var totalItem = DbSet.CountDocuments(filterdefine);
            var all = DbSet.Find(filterdefine).SortByDescending(m => m.CreatedDate).Skip((page - 1) * pagesize).Limit(pagesize);
            return new PagedResult<Models.Schema.Order>(all.ToList(), page, pagesize, (int)totalItem);
        }


    }
}
