﻿using MongoDB.Driver;
using NacGroup.Module.Core.Data;
using NacGroup.Module.Core.Interfaces;
using NacGroup.Module.Order.Models.Schema;

namespace NacGroup.Module.Order.Data
{
    public interface IShippingServiceRepository : IRepository<CityCollection>
    {
        CityCollection GetByCode(string code);
    }
    public class ShippingServiceRepository : MongoRepository<CityCollection>, IShippingServiceRepository
    {
        public ShippingServiceRepository(IMongoContext context) : base(context)
        {
        }

        public void Seed()
        {

        }
        public CityCollection GetByCode(string code)
        {
            return GetSingle(m => m.Code == code);
        }

    }
}
