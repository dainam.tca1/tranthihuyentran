﻿using MongoDB.Driver;
using NacGroup.Module.Core.Data;
using NacGroup.Module.Core.Interfaces;
using NacGroup.Module.Order.Models.Schema;


namespace NacGroup.Module.Order.Data
{
    public interface IShippingFeeByTotalBillRepository : IRepository<ShippingFeeByTotalBill>
    {

    }
    public class ShippingFeeByTotalBillRepository : MongoRepository<ShippingFeeByTotalBill>, IShippingFeeByTotalBillRepository, IMigrationRepository
    {
        public ShippingFeeByTotalBillRepository(IMongoContext context) : base(context)
        {
        }

        public void Migration()
        {
            DbSet.Indexes.CreateOne(model: new CreateIndexModel<ShippingFeeByTotalBill>("{PriceFrom:1}"));

        }

        public void Seed()
        {
            if (DbSet.EstimatedDocumentCount() == 0)
            {
                Add(new ShippingFeeByTotalBill
                {
                    PriceFrom = 0,
                    PriceTo = 0,
                    Feeship = 0,
                    IsActive = false,
                });
            }
        }
       

    }
}
