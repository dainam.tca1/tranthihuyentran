﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using NacGroup.Module.Core.Data;
using NacGroup.Module.Core.Interfaces;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Core.Models.Schema;
using NacGroup.Module.Order.Models;
using Newtonsoft.Json;

namespace NacGroup.Module.Order.Data
{
    public interface IOrderPickUpSettingRepository : IRepository<AppSetting>
    {
        AppSetting GetSetting();
    }

    public class OrderPickUpSettingRepository : MongoRepository<AppSetting>, IOrderPickUpSettingRepository, IMigrationRepository
    {
        public OrderPickUpSettingRepository(IMongoContext context) : base(context)
        {
        }

        public void Seed()
        {
            var appSetting = GetSetting();
            if (appSetting == null)
            {
                var bookingSettingObject = OrderPickUpSetting.Create();
                bookingSettingObject.SessionLength = 5;
                bookingSettingObject.MaxOrderPerSession = 3;
                foreach (var day in bookingSettingObject.DayWork)
                {
                    day.Open = "10:00 AM";
                    day.Close = "8:00 PM";
                    day.IsActive = true;
                }
                
                
                var shoppingCartSetting = new AppSetting
                {
                    Key = ConstKey.OrderPickUpSettingKey,
                    Value = BsonSerializer.Deserialize<BsonDocument>(JsonConvert.SerializeObject(bookingSettingObject))
                };
                Add(shoppingCartSetting);
                Context.SaveChange();
            }
        }

        public void Migration()
        {
        }

        public AppSetting GetSetting()
        {
            return GetSingle(m => m.Key == ConstKey.OrderPickUpSettingKey);
        }
    }
}
