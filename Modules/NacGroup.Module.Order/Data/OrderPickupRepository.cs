﻿using MongoDB.Driver;
using NacGroup.Module.Core.Data;
using NacGroup.Module.Core.Interfaces;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Order.Models.Schema;
using System;
namespace NacGroup.Module.Order.Data
{
    public interface IOrderPickupRepository : IRepository<OrderPickup>
    {
        PagedResult<OrderPickup> Filter(string customername, string phone, DateTime fromdate, DateTime todate, int page, int pagesize);
    }
    

    public class OrderPickupRepository : MongoRepository<OrderPickup>, IOrderPickupRepository, IMigrationRepository
    {
        public OrderPickupRepository(IMongoContext context) : base(context)
        {
        }

        public void Seed()
        {

        }

        public void Migration()
        {
            DbSet.Indexes.CreateOne(model: new CreateIndexModel<OrderPickup>("{CustomerId:1}"));
        }

        public PagedResult<OrderPickup> Filter(string customername, string phone, DateTime fromdate, DateTime todate, int page, int pagesize)
        {
            var filter = Builders<OrderPickup>.Filter;
            var filterdefine = filter.Empty;
            if (!string.IsNullOrEmpty(customername))
            {
                filterdefine &= filter.Where(m => m.FirstName.Contains(customername) || m.LastName.Contains(customername));
            }
            
            if (!string.IsNullOrEmpty(phone))
            {
                filterdefine &= filter.Where(m => m.CustomerPhone.Contains(phone));
            }

            if (fromdate != DateTime.MinValue)
            {
                filterdefine &= filter.Where(m => m.CreatedDate >= fromdate);
            }
            if (todate != DateTime.MaxValue)
            {
                filterdefine &= filter.Where(m => m.CreatedDate <= todate);
            }
            var totalItem = DbSet.CountDocuments(filterdefine);
            var all = DbSet.Find(filterdefine).Skip((page - 1) * pagesize).Limit(pagesize);
            return new PagedResult<OrderPickup>(all.ToList(), page, pagesize, (int)totalItem);
        }


    }
}
