﻿using System.Collections.Generic;
using NacGroup.Module.Core.Data;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Order.Data;
using NacGroup.Module.Order.Models.Schema;

namespace NacGroup.Module.Order.Services
{
    public interface ICouponService
    {
        PagedResult<Coupon> Get(string couponCode,bool? isActive,int page, int pagesize);
        ApiResponseModel Delete(List<string> idlst);
        ApiResponseModel Delete(string id);
        Coupon GetById(string id);
        Coupon GetByCode(string code);
        ApiResponseModel Add(Coupon model);
        ApiResponseModel Update(Coupon model);
    }

    public class CouponService : ICouponService
    {
        private readonly ICouponRepository _couponRepository;
        private readonly IUnitOfWork _unitOfWork;

        public CouponService(IUnitOfWork unitOfWork, ICouponRepository couponRepository)
        {
            _unitOfWork = unitOfWork;
            _couponRepository = couponRepository;
        }


        public PagedResult<Coupon> Get(string couponCode, bool? isActive, int page, int pagesize)
        {
            return _couponRepository.Filter(couponCode, isActive, page, pagesize);
        }

        public ApiResponseModel Delete(List<string> idlst)
        {
            if (idlst == null)
                return new ApiResponseModel("error", StaticMessage.SelectDataToRemove);
            foreach (var item in idlst)
            {
                Delete(item);
            }
            return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
        }

        public ApiResponseModel Delete(string id)
        {
            var resultcode = _couponRepository.Delete(id);
            if (resultcode < 0)
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            resultcode = _unitOfWork.Commit();
            if (resultcode < 0)
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
        }

        public Coupon GetById(string id)
        {
            return _couponRepository.GetById(id);
        }

        public Coupon GetByCode(string code)
        {
            return _couponRepository.GetByCode(code?.ToUpper());
        }

        public ApiResponseModel Add(Coupon model)
        {
            if(model == null)  return new ApiResponseModel("error", StaticMessage.ParameterInvalid);  
            if (string.IsNullOrEmpty(model.CouponCode)) return new ApiResponseModel("error", "Coupon code is required");
            if (model.Value <=0) return new ApiResponseModel("error", "Coupon value must be more than zero");

            var oldCoupon = _couponRepository.GetByCode(model.CouponCode);
            if (oldCoupon != null)
                return new ApiResponseModel("error", "Coupon code is exist");
            try
            {
                model.CouponCode = model.CouponCode.ToUpper();
                var result = _couponRepository.Add(model);
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                result = _unitOfWork.Commit();
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }


        public ApiResponseModel Update(Coupon model)
        {
            if (model == null) return new ApiResponseModel("error", StaticMessage.ParameterInvalid);
            if (string.IsNullOrEmpty(model.CouponCode)) return new ApiResponseModel("error", "Coupon code is required");
            if (model.Value <= 0) return new ApiResponseModel("error", "Coupon value must be more than zero");

            var temp = _couponRepository.GetById(model.Id);
            if (temp == null)
                return new ApiResponseModel("error", StaticMessage.DataNotFound);

            var oldCoupon = _couponRepository.GetByCode(model.CouponCode);
            if (oldCoupon != null && temp.Id != oldCoupon.Id)
                return new ApiResponseModel("error", "Coupon code is exist");
           
            try
            {

                temp.CouponCode = model.CouponCode.ToUpper();
                temp.IsActive = model.IsActive;
                temp.Value = model.Value;
                temp.StartDate = model.StartDate;
                temp.ExpiredDate = model.ExpiredDate;
                temp.IsOneTimeUse = model.IsOneTimeUse;
                temp.ValueType = model.ValueType;
               
                var result = _couponRepository.Update(model.Id, temp);
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                result = _unitOfWork.Commit();
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }

    }
}
