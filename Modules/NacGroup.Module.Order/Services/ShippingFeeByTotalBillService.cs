﻿using System.Collections.Generic;
using System.Linq;
using NacGroup.Module.Core.Data;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Order.Data;
using NacGroup.Module.Order.Models.Schema;

namespace NacGroup.Module.Order.Services
{
    public interface IShippingFeeByTotalBillService
    {
        List<ShippingFeeByTotalBill> GetAll();
        ApiResponseModel Delete(List<string> idList);
        ApiResponseModel Delete(string id);
        ApiResponseModel Add(ShippingFeeByTotalBill model);
        ShippingFeeByTotalBill GetById(string id);
        ApiResponseModel Update(ShippingFeeByTotalBill model);
    }
    public class ShippingFeeByTotalBillService : IShippingFeeByTotalBillService
    {
        private readonly IShippingFeeByTotalBillRepository _shippingFeeByTotalBillRepository;
        private readonly IUnitOfWork _unitOfWork;

        public ShippingFeeByTotalBillService(IShippingFeeByTotalBillRepository shippingFeeByTotalBillRepository, IUnitOfWork unitOfWork)
        {
            _shippingFeeByTotalBillRepository = shippingFeeByTotalBillRepository;
            _unitOfWork = unitOfWork;
        }

        public List<ShippingFeeByTotalBill> GetAll()
        {
            return _shippingFeeByTotalBillRepository.Get().ToList();
        }

        public ApiResponseModel Delete(List<string> idList)
        {
            if (idList == null)
                return new ApiResponseModel("error", StaticMessage.SelectDataToRemove);
            foreach (var item in idList)
            {
                var result = Delete(item);
                if (result.status == "error") return result;
            }
            return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
        }

        public ApiResponseModel Delete(string id)
        {
            var resultcode = _shippingFeeByTotalBillRepository.Delete(id);
            if (resultcode < 0)
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            resultcode = _unitOfWork.Commit();
            if (resultcode < 0)
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
        }

        public ApiResponseModel Add(ShippingFeeByTotalBill model)
        {
            try
            {

                var result = _shippingFeeByTotalBillRepository.Add(model);
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);

                result = _unitOfWork.Commit();
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);

                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }

        public ShippingFeeByTotalBill GetById(string id)
        {
            return _shippingFeeByTotalBillRepository.GetById(id);
        }

        public ApiResponseModel Update(ShippingFeeByTotalBill model)
        {
            #region Validate
            var temp = _shippingFeeByTotalBillRepository.GetById(model.Id);
            if (temp == null)
                return new ApiResponseModel("error", StaticMessage.DataNotFound);

            #endregion

            try
            {
                temp.PriceFrom = model.PriceFrom;
                temp.PriceTo = model.PriceTo;
                temp.Feeship = model.Feeship;
                temp.IsActive = model.IsActive;
                var result = _shippingFeeByTotalBillRepository.Update(model.Id, temp);
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);

            

                result = _unitOfWork.Commit();
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);

                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }

    }
}
