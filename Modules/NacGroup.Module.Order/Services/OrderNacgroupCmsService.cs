﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using NacGroup.Module.Core.Data;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Core.Services;
using NacGroup.Module.Order.Data;
using NacGroup.Module.Order.Models.Schema;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace NacGroup.Module.Order.Services
{
    public interface IOrderNacGroupCmsService
    {
        PagedResult<Models.Schema.Order> Filter(string ordernumber, string customername, string phone, int? status, string fromdate, string todate, int page, int pagesize);
        PagedResult<Models.Schema.Order> GetOrderByCustomerId(string customerid, int page, int pagesize);
        ApiResponseModel Update(Models.Schema.Order model);
        ApiResponseModel UpdateCustomize(Models.Schema.Order model, string name);
        Models.Schema.Order GetById(string id);
        IEnumerable<Models.Schema.Order> Get();
        ApiResponseModel UpdateStatus(string id, OrderStatus? orderStatus);
        List<decimal> TotalSalesChart(DateTime start, int partNumber, int numberEachPart, StatisticalTimeType type, OrderStatus? status, ref List<DateTime> timeList);
        decimal GetTotalRevenue(DateTime last, DateTime current, OrderStatus? status);
        decimal GetTotalRevenue(OrderStatus? status);
        int GetToTalOrderCount();
        int GetToTalOrderCount(DateTime last, DateTime current, OrderStatus? status);
        List<OrderItem> GetListOrder(DateTime timefrom, DateTime timeto, OrderStatus? orderStatus,int page, int pagesize);
    }

    public class OrderNacGroupCmsService : IOrderNacGroupCmsService
    {
        private readonly IOrderRepository _orderRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly ISendMailService _sendMailService;
        private readonly string _dateFormat;
        private readonly IViewRenderService _viewRenderService;
        private readonly GlobalConfiguration _globalConfiguration;
        private readonly IHttpClientFactory _httpClientFactory;
        public OrderNacGroupCmsService(
            IUnitOfWork unitOfWork, 
            IOrderRepository orderRepository, 
            ISendMailService sendMailService,
            IViewRenderService viewRenderService,
            IGettingGlobalConfigService gettingGlobalConfigService,
            IHttpClientFactory httpClientFactory
           )
        {
            _unitOfWork = unitOfWork;
            _orderRepository = orderRepository;
            _sendMailService = sendMailService;
            _viewRenderService = viewRenderService;
            _httpClientFactory = httpClientFactory;
            _globalConfiguration = gettingGlobalConfigService.GetConfiguration();
            _dateFormat = _globalConfiguration.GlobalWebSetting["WebsiteConfig"]["DateCShapeFormat"].Value<string>();

        }

        public IEnumerable<Models.Schema.Order> Get()
        {
            return _orderRepository.Get();
        }

        public Models.Schema.Order GetById(string id)
        {
            return _orderRepository.GetById(id);
        }
        public PagedResult<Models.Schema.Order> Filter(string ordernumber, string customername, string phone, int? status, string fromdate, string todate, int page, int pagesize)
        {
            DateTime fromDate = DateTime.MinValue, toDate = DateTime.MaxValue;
            if (!string.IsNullOrEmpty(fromdate))
            {
                var isDate = DateTime.TryParseExact(fromdate, _dateFormat, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out fromDate);
                if (!isDate) return new PagedResult<Models.Schema.Order>();
            }
            if (!string.IsNullOrEmpty(todate))
            {
                var isDate = DateTime.TryParseExact(todate, _dateFormat, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out toDate);
                if (!isDate) return new PagedResult<Models.Schema.Order>();
            }

            return _orderRepository.Filter(ordernumber, customername, phone, status, fromDate, toDate, page, pagesize);
        }

        public PagedResult<Models.Schema.Order> GetOrderByCustomerId(string customerid,int page, int pagesize)
        {
       
            return _orderRepository.GetOrderByCustomerId(customerid, page, pagesize);
        }
        public ApiResponseModel Update(Models.Schema.Order model)
        {
            #region Validate

            
            var temp = _orderRepository.GetById(model.Id);
            if (temp == null)
                return new ApiResponseModel("error", StaticMessage.DataNotFound);

            //if (string.IsNullOrEmpty(model.City)) return new ApiResponseModel("error", "Please select province / city");
            //if (string.IsNullOrEmpty(model.District)) return new ApiResponseModel("error", "Please select district");
            //if (string.IsNullOrEmpty(model.Ward)) return new ApiResponseModel("error", "Please select ward");
            //if (string.IsNullOrEmpty(model.Street)) return new ApiResponseModel("error", "Please insert street");
           
            #endregion

            try
            {
                temp.OrderStatusNacGroup = model.OrderStatusNacGroup;
                temp.OrderPaymentStatusNacGroup = model.OrderPaymentStatusNacGroup;
                temp.OrderPaymentMethodNacGroup = model.OrderPaymentMethodNacGroup;
                temp.FirstName = model.FirstName;
                temp.LastName = model.LastName;
                temp.CustomerPhone = model.CustomerPhone;
                temp.CustomerEmail = model.CustomerEmail;
                temp.City = model.City;
                temp.District = model.District;
                temp.Ward = model.Ward;
                temp.Street = model.Street;
                temp.Notes = model.Notes;
                temp.UpdatedDate = DateTime.Now;
                switch (model.OrderStatus)
                {
                    case OrderStatus.ON_HOLD:
                        // temp.OrderStatus = OrderStatus.ON_HOLD;              
                        return new ApiResponseModel("error", "Unable to transition to ON_HOLD");
                    case OrderStatus.PENDING_PAYMENT: 
                       
                        switch (temp.OrderStatus)
                        {
                            case OrderStatus.ON_HOLD:
                                temp.OrderStatus = OrderStatus.PENDING_PAYMENT;
                                break;
                            default:
                                return new ApiResponseModel("error", "Unable to transition to PENDING_PAYMENT");
                        }
                        
                        break;
                    case OrderStatus.PAYMENT_RECEIVED:
                        switch (temp.OrderStatus)
                        {
                            case OrderStatus.PENDING_PAYMENT:
                                temp.OrderStatus = OrderStatus.PAYMENT_RECEIVED;
                                break;
                            default:
                                return new ApiResponseModel("error", "Unable to transition to PAYMENT_RECEIVED");
                        }
                        break;         
                    case OrderStatus.CANCELED:     
                        switch (temp.OrderStatus)
                        {
                            case OrderStatus.ON_HOLD:
                                RestoreQuantity(temp);
                                temp.OrderStatus = OrderStatus.CANCELED;
                                break;
                            case OrderStatus.PENDING_PAYMENT:
                                RestoreQuantity(temp);
                                temp.OrderStatus = OrderStatus.CANCELED;
                                break;
                            case OrderStatus.PAYMENT_RECEIVED:
                                RestoreQuantity(temp);
                                temp.OrderStatus = OrderStatus.CANCELED;
                                break;
                            default:
                                return new ApiResponseModel("error", "Unable to transition to CANCELED");      
                        }
                        break;
                    case OrderStatus.PACKAGING:
                        switch (temp.OrderStatus)
                        {
                            case OrderStatus.PAYMENT_RECEIVED:
                                temp.OrderStatus = OrderStatus.PACKAGING;
                                break;             
                            default:
                                return new ApiResponseModel("error", "Unable to transition to PACKAGING");
                        }
                        break;
                    case OrderStatus.ORDER_SHIPPED:                
                        switch (temp.OrderStatus)
                        {
                            case OrderStatus.PACKAGING:
                                temp.OrderStatus = OrderStatus.ORDER_SHIPPED;
                                break;
                            default:
                                return new ApiResponseModel("error", "Unable to transition to ORDER_SHIPPED");
                        }
                        break;
                    case OrderStatus.BACK_ORDER:
                     
                        switch (temp.OrderStatus)
                        {
                            case OrderStatus.ORDER_SHIPPED:
                                temp.OrderStatus = OrderStatus.BACK_ORDER;
                                break;
                            default:
                                return new ApiResponseModel("error", "Unable to transition to BACK_ORDER");
                        }
                        break;
                    case OrderStatus.ORDER_ARCHIVED:
                        switch (temp.OrderStatus)
                        {
                            case OrderStatus.BACK_ORDER:
                                temp.OrderStatus = OrderStatus.ORDER_ARCHIVED;
                                break;
                            case OrderStatus.ORDER_SHIPPED:
                                temp.OrderStatus = OrderStatus.ORDER_ARCHIVED;
                                break;
                            default:
                                return new ApiResponseModel("error", "Unable to transition to ORDER_ARCHIVED");
                        }
                        break;
                }
                var result = _orderRepository.Update(model.Id, temp);
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                result = _unitOfWork.Commit();
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }

        public ApiResponseModel UpdateStatus(string id,OrderStatus? orderStatus)
        {
            #region Validate


            var temp = _orderRepository.GetById(id);
            if (temp == null)
                return new ApiResponseModel("error", StaticMessage.DataNotFound);
            #endregion
            try
            {
                switch (orderStatus)
                {
                    case OrderStatus.ON_HOLD:
                        // temp.OrderStatus = OrderStatus.ON_HOLD;              
                        return new ApiResponseModel("error", "Unable to transition to ON_HOLD");
                    case OrderStatus.PENDING_PAYMENT:

                        switch (temp.OrderStatus)
                        {
                            case OrderStatus.ON_HOLD:
                                temp.OrderStatus = OrderStatus.PENDING_PAYMENT;
                                break;
                            default:
                                return new ApiResponseModel("error", "Unable to transition to PENDING_PAYMENT");
                        }

                        break;
                    case OrderStatus.PAYMENT_RECEIVED:
                        switch (temp.OrderStatus)
                        {
                            case OrderStatus.PENDING_PAYMENT:
                                temp.OrderStatus = OrderStatus.PAYMENT_RECEIVED;
                                break;
                            default:
                                return new ApiResponseModel("error", "Unable to transition to PAYMENT_RECEIVED");
                        }
                        break;
                    case OrderStatus.CANCELED:
                        switch (temp.OrderStatus)
                        {
                            case OrderStatus.ON_HOLD:
                                RestoreQuantity(temp);
                                temp.OrderStatus = OrderStatus.CANCELED;
                                break;
                            case OrderStatus.PENDING_PAYMENT:
                                RestoreQuantity(temp);
                                temp.OrderStatus = OrderStatus.CANCELED;
                                break;
                            case OrderStatus.PAYMENT_RECEIVED:
                                RestoreQuantity(temp);
                                temp.OrderStatus = OrderStatus.CANCELED;
                                break;
                            default:
                                return new ApiResponseModel("error", "Unable to transition to CANCELED");
                        }
                        break;
                    case OrderStatus.PACKAGING:
                        switch (temp.OrderStatus)
                        {
                            case OrderStatus.PAYMENT_RECEIVED:
                                temp.OrderStatus = OrderStatus.PACKAGING;
                                break;
                            default:
                                return new ApiResponseModel("error", "Unable to transition to PACKAGING");
                        }
                        break;
                    case OrderStatus.ORDER_SHIPPED:
                        switch (temp.OrderStatus)
                        {
                            case OrderStatus.PACKAGING:
                                temp.OrderStatus = OrderStatus.ORDER_SHIPPED;
                                break;
                            default:
                                return new ApiResponseModel("error", "Unable to transition to ORDER_SHIPPED");
                        }
                        break;
                    case OrderStatus.BACK_ORDER:

                        switch (temp.OrderStatus)
                        {
                            case OrderStatus.ORDER_SHIPPED:
                                temp.OrderStatus = OrderStatus.BACK_ORDER;
                                break;
                            default:
                                return new ApiResponseModel("error", "Unable to transition to BACK_ORDER");
                        }
                        break;
                    case OrderStatus.ORDER_ARCHIVED:
                        switch (temp.OrderStatus)
                        {
                            case OrderStatus.BACK_ORDER:
                                temp.OrderStatus = OrderStatus.ORDER_ARCHIVED;
                                break;
                            case OrderStatus.ORDER_SHIPPED:
                                temp.OrderStatus = OrderStatus.ORDER_ARCHIVED;
                                break;
                            default:
                                return new ApiResponseModel("error", "Unable to transition to ORDER_ARCHIVED");
                        }
                        break;
                }
                var result = _orderRepository.Update(temp.Id, temp);
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                result = _unitOfWork.Commit();
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully,temp.OrderStatus);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }

        public ApiResponseModel RestoreQuantity(Models.Schema.Order order)
        {
            var httpClient = _httpClientFactory.CreateClient();
            foreach (var sku in order.OrderItems)
            {
                dynamic data = new
                {
                    productId = sku.SkuId,
                    skuCode = sku.SkuCode,
                    quantity = sku.Quantity
                };
                var request = new HttpRequestMessage(HttpMethod.Post, $"{_globalConfiguration.WebsiteUrl}/api/products/restore")
                {
                    Content = new StringContent(JsonConvert.SerializeObject(data), Encoding.UTF8, "application/json")

                };
                var updateResult = httpClient.SendAsync(request).Result;
                if (!updateResult.IsSuccessStatusCode) return new ApiResponseModel("error", updateResult.Content.ReadAsStringAsync().Result);
            }
            return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
        }

        public ApiResponseModel UpdateCustomize(Models.Schema.Order model, string name)
        {
           
            try
            {
                if (model == null || string.IsNullOrEmpty(name)) return new ApiResponseModel("error", StaticMessage.DataNotFound);

                var temp = _orderRepository.GetById(model.Id);
                if (temp == null) return new ApiResponseModel("error", StaticMessage.DataNotFound);
                #region Validate
                
              
                #endregion
                temp[name] = model[name];
                var result = _orderRepository.Update(model.Id, temp);
                if (result < 0) return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);

                result = _unitOfWork.Commit();
                if (result < 0) return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }

        public List<decimal> TotalSalesChart(DateTime start , int partNumber ,int numberEachPart , StatisticalTimeType type,OrderStatus? status, ref List<DateTime> timeList)
        {
            var timelst = new List<DateTime>();
            for (var i = 0; i <= partNumber; i++)
            {
                switch (type)
                {
                    case StatisticalTimeType.Hour:
                        timelst.Add(start.AddHours(numberEachPart * i));
                        break;
                    case StatisticalTimeType.Day:
                        timelst.Add(start.AddDays(numberEachPart * i));
                        break;
                    case StatisticalTimeType.Month:
                        timelst.Add(start.AddMonths(numberEachPart * i));
                        break;
                    case StatisticalTimeType.Year:
                        timelst.Add(start.AddYears(numberEachPart * i));
                        break;
                }
            }
            var totalAmount = new List<decimal>();
            for (int i = 1; i < timelst.Count; i++)
            {
                var curent = timelst[i];
                var last = timelst[i - 1];
                totalAmount.Add(GetTotalRevenue(last, curent, status));
            }
            timeList = timelst;
            return totalAmount;
        }

        public decimal GetTotalRevenue(DateTime last , DateTime current , OrderStatus? status)
        {
            return _orderRepository.GetTotalRevenue(last, current , status);
        }

        public List<OrderItem> GetListOrder(DateTime timefrom, DateTime timeto, OrderStatus? orderStatus,int page, int pagesize)
        {
            return _orderRepository.GetListOrder(timefrom , timeto, orderStatus,page,pagesize);
        }


        public decimal GetTotalRevenue(OrderStatus? status)
        {
            return _orderRepository.GetTotalRevenue(status);
        }

        public int GetToTalOrderCount()
        {
            return _orderRepository.GetToTalOrderCount();
        }
        public int GetToTalOrderCount(DateTime last, DateTime current, OrderStatus? status)
        {
            return _orderRepository.GetToTalOrderCount(last,current,status);
        }
    }


}
