﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using MongoDB.Bson;
using NacGroup.Infrastructure.Shared;
using NacGroup.Module.Core.Data;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Core.Models.Schema;
using NacGroup.Module.Core.Services;
using NacGroup.Module.Order.Data;
using NacGroup.Module.Order.Models;
using NacGroup.Module.Order.Models.Schema;

using Newtonsoft.Json.Linq;
using System.Numerics;
using NacGroup.Module.Order.Strategy;
using Newtonsoft.Json;
using System.Text;

namespace NacGroup.Module.Order.Services
{
    public interface IOrderNacGroupService
    {
       
        ApiResponseModel UpdatePickUpTime(ref Models.Schema.Order shoppingCart, DateTime pickupTime);
        ApiResponseModel UpdateTips(ref Models.Schema.Order shoppingCart, decimal amount);
        ApiResponseModel ApplyCoupon(ref Models.Schema.Order shoppingCart, string couponcode);
        ApiResponseModel ClearCoupon(ref Models.Schema.Order shoppingCart);
        ApiResponseModel AddItems(ref Models.Schema.Order shoppingCart, string skuId, string skucode, int quantity);
        ApiResponseModel UpdateShipping(Models.Schema.Order shoppingCart, string city, string district);
        ApiResponseModel UpdateShippingTotalBill(Models.Schema.Order shoppingCart);
        ApiResponseModel UpdateDiscount(ref Models.Schema.Order shoppingCart);
        ApiResponseModel UpdateQuantity(Guid id, int quantity, ref Models.Schema.Order shoppingCart);
        ApiResponseModel UpdateSaleTaxRate(ref Models.Schema.Order shoppingCart);

        ApiResponseModel ConfirmCheckout(Models.Schema.Order shoppingCart);

    }

    public class OrderNacGroupService : IOrderNacGroupService
    {
        private readonly IOrderRepository _orderRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILoggerRepository _loggerRepository;
        private readonly ICouponService _couponService;
        private readonly IOrderUserProfileRepository _orderUserProfileRepository;
        private readonly IOrderPickUpSettingService _orderPickUpSettingService;
        
        private readonly ISendMailService _sendMailService;
        private readonly IViewRenderService _viewRenderService;
        private readonly GlobalConfiguration _globalConfiguration;
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly IShippingFeeByTotalBillRepository _shippingFeeByTotalBillRepository;
        private readonly IShippingFeeByAreaRepository _shippingFeeByAreaRepository;
        private readonly ShippingType _shippingType;
        private readonly bool _shippingUsed;
        private readonly IShippingStrategy _shippingStrategy;
        private readonly IShippingApiSettingRepository _shippingApiSettingRepository;

        public OrderNacGroupService(
            IUnitOfWork unitOfWork, 
            IOrderRepository orderRepository, 
            ILoggerRepository loggerRepository, 
            ICouponService couponService, 
            IOrderUserProfileRepository orderUserProfileRepository,
            IOrderPickUpSettingService orderPickUpSettingService, 
            
            ISendMailService sendMailService,
            IViewRenderService viewRenderService,
            IGettingGlobalConfigService gettingGlobalConfigService, 
            IHttpClientFactory httpClientFactory,
            IShippingFeeByTotalBillRepository shippingFeeByTotalBillRepository,
            IShippingFeeByAreaRepository shippingFeeByAreaRepository,
            IShippingStrategy shippingStrategy,
            IShippingApiSettingRepository shippingApiSettingRepository
            )
        {
            _unitOfWork = unitOfWork;
            _orderRepository = orderRepository;
            _loggerRepository = loggerRepository;
            _couponService = couponService;
            _orderUserProfileRepository = orderUserProfileRepository;
            _orderPickUpSettingService = orderPickUpSettingService;
           
            _sendMailService = sendMailService;
            _viewRenderService = viewRenderService;
            _httpClientFactory = httpClientFactory;
            _shippingFeeByTotalBillRepository = shippingFeeByTotalBillRepository;
            _shippingFeeByAreaRepository = shippingFeeByAreaRepository;
            _globalConfiguration = gettingGlobalConfigService.GetConfiguration();
            _shippingStrategy = shippingStrategy;
            var config = shippingApiSettingRepository.GetSetting().Value;
            var shipApiConfig = JsonConvert.DeserializeObject<JObject>(config.ToJson());
            _shippingType = shipApiConfig["Type"].Value<int>() == 0 ? ShippingType.EasyShip : ShippingType.FedEx;
            _shippingUsed = shipApiConfig["Used"].Value<bool>();

        }

        public ApiResponseModel ConfirmCheckout(Models.Schema.Order shoppingCart)
        {
            try
            {
                #region Validate

                if (shoppingCart == null || !shoppingCart.OrderItems.Any()) return new ApiResponseModel("error", "Your shopping cart is empty");
            
                if (string.IsNullOrEmpty(shoppingCart.Recipient.FullName)) return new ApiResponseModel("error", "Vui lòng điền họ và tên đầy đủ");      
                if (string.IsNullOrEmpty(shoppingCart.CustomerPhone)) return new ApiResponseModel("error", "Vui lòng điền số điện thoại");
                if (string.IsNullOrEmpty(shoppingCart.CustomerEmail)) return new ApiResponseModel("error", "Vui lòng điền địa chỉ email");
                 if (!shoppingCart.CustomerEmail.IsEmail()) return new ApiResponseModel("error", "Email sai định dạng");
                if (string.IsNullOrEmpty(shoppingCart.City)) return new ApiResponseModel("error", "Vui lòng chọn tỉnh thành");
                if (string.IsNullOrEmpty(shoppingCart.District)) return new ApiResponseModel("error", "Vui lòng chọn quận huyện");
                if (string.IsNullOrEmpty(shoppingCart.Ward)) return new ApiResponseModel("error", "Vui lòng chọn phường xã");
                if (string.IsNullOrEmpty(shoppingCart.Street)) return new ApiResponseModel("error", "Vui lòng điền địa chỉ số nhà");


             
                #endregion

                #region Validate Stock Product
                var httpClient = _httpClientFactory.CreateClient();
                foreach (var product in shoppingCart.OrderItems)
                {

                    var getproductResult = httpClient.GetAsync($"{_globalConfiguration.WebsiteUrl}/api/products?skuId={product.SkuId}&skucode={product.SkuCode}").Result;

                    if (!getproductResult.IsSuccessStatusCode) return new ApiResponseModel("error", StaticMessage.ParameterInvalid);

                    var itemProduct = getproductResult.Content.ReadAsAsync<SkuNacGroup>().Result;
                    if (!itemProduct.IsActive)
                    {
                        return new ApiResponseModel("error", $"{itemProduct.Name} Sold Out");
                    }
                    if (itemProduct.IsInventory == true)
                    {
                        if (itemProduct.StockQuantity == 0)
                        {
                            return new ApiResponseModel("error", $"{itemProduct.Name} Sold Out");
                        }
                        if (itemProduct.StockQuantity < product.Quantity)
                        {
                            return new ApiResponseModel("error", $"{itemProduct.Name} Sold Out");
                        }
                    }

                }
                #endregion

                #region Lưu đơn hàng vào website

                var oldOrder = _orderRepository.Get().LastOrDefault();
                var ordernumber = $"{DateTime.Now.Year}{DateTime.Now.Month}{DateTime.Now.Day}" + 1;
                shoppingCart.OrderNumber = oldOrder != null && oldOrder.OrderNumber != null ? (BigInteger.Parse(oldOrder.OrderNumber) + 1).ToString() : ordernumber;
                shoppingCart.OrderStatus = OrderStatus.PAYMENT_RECEIVED;
                var addOrderWebsiteResult = AddOrder(shoppingCart);
                if (addOrderWebsiteResult.status == "error") return addOrderWebsiteResult;
                #endregion

                #region Update Product
                foreach (var product in shoppingCart.OrderItems)
                {
                    dynamic model = new
                    {
                        productId = product.SkuId,
                        skuCode = product.SkuCode,
                        quantity = product.Quantity
                    };
                    var request = new HttpRequestMessage(HttpMethod.Post, $"{_globalConfiguration.WebsiteUrl}/api/products/update")
                    {
                        Content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json")

                    };
                    var updateResult = httpClient.SendAsync(request).Result;
                    if (!updateResult.IsSuccessStatusCode) return new ApiResponseModel("error", updateResult.Content.ReadAsStringAsync().Result);

                }
                #endregion

                #region Nếu có coupon thì update coupon user đã dùng
                if (shoppingCart.Coupon != null && shoppingCart.Coupon.Value < 0)
                {
                    var currentUser = _orderUserProfileRepository.GetSingle(m => m.UserProfileId == shoppingCart.CustomerId);
                    if (currentUser != null)
                    {
                        if (currentUser.UsedCoupons == null) currentUser.UsedCoupons = new List<string>();
                        if (!currentUser.UsedCoupons.Contains(shoppingCart.Coupon.Id))
                        {
                            currentUser.UsedCoupons.Add(shoppingCart.Coupon.Id);
                            _orderUserProfileRepository.Update(currentUser.Id, currentUser);
                            _unitOfWork.Commit();
                        }
                    }
                    else
                    {
                        _orderUserProfileRepository.Add(new OrderUserProfile
                        {
                            UserProfileId = shoppingCart.CustomerId,
                            UsedCoupons = new List<string> { shoppingCart.Coupon.Id }
                        });
                        _unitOfWork.Commit();
                    }

                }
                #endregion
                var currentWebOrder = _orderRepository.GetById(shoppingCart.Id);

               

                #region Update order
                var updateOrder = _orderRepository.Update(currentWebOrder.Id, currentWebOrder);
                if (updateOrder < 0) return new ApiResponseModel("error", "Data Updated failed");
                updateOrder = _unitOfWork.Commit();
                if (updateOrder < 0) return new ApiResponseModel("error", "Data Updated failed");
                #endregion

                try
                {

                    var mailModel = JObject.FromObject(new
                    {
                        WebsiteName = _globalConfiguration.GlobalWebSetting["WebInfomation"]["WebsiteName"].ToString(),
                        WebsiteUrl = _globalConfiguration.GlobalWebSetting["WebInfomation"]["WebsiteURL"].ToString(),
                        WebsiteLogo = _globalConfiguration.GlobalThemeConfig["Data"][0]["Src"],
                        OrderModel = currentWebOrder
                    }); ;

                    var mailmessage = _viewRenderService.RenderToStringAsync("_OrderSuccessMail", mailModel).Result;
                    var sendmailResult = _sendMailService.SendMail(new EmailModel(_globalConfiguration.SendMailUserName,
                        _globalConfiguration.SendMailPassword, currentWebOrder.CustomerEmail,
                        $"Cảm ơn bạn đã đặt hàng trên website chúng tôi!",
                        mailmessage, _globalConfiguration.GlobalWebSetting["WebInfomation"]["WebsiteName"].ToString()));
                    if (!sendmailResult) _loggerRepository.Add(new Logger(LogLevel.Error, "Lỗi send mail confirm order"));

                    //Gui mail cho chu tiem
                    var sendmailBossResult = _sendMailService.SendMail(new EmailModel(_globalConfiguration.SendMailUserName,
                        _globalConfiguration.SendMailPassword, _globalConfiguration.GlobalWebSetting["WebInfomation"]["WebsiteNotification"].ToString(),
                        $"Thông báo! Có đơn đặt hàng mới trên {_globalConfiguration.GlobalWebSetting["WebInfomation"]["WebsiteName"]}",
                        mailmessage, _globalConfiguration.GlobalWebSetting["WebInfomation"]["WebsiteName"].ToString()));
                    if (!sendmailBossResult) _loggerRepository.Add(new Logger(LogLevel.Error, "Lỗi send mail order cho chủ tiệm"));
                }
                catch (Exception e)
                {
                    _loggerRepository.Add(new Logger(LogLevel.Error, "Lỗi send mail xac nhan don hang", e));
                }

                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully, currentWebOrder);
            }
            catch (Exception e)
            {
                _loggerRepository.Add(new Logger(LogLevel.Error, "Lỗi xác nhận", e));
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }

       
        private ApiResponseModel AddOrder(Models.Schema.Order order)
        {
            order.Id = ObjectId.GenerateNewId().ToString();
            var result = _orderRepository.Add(order);
            if (result < 0) return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            result = _unitOfWork.Commit();
            if (result < 0) return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            return new ApiResponseModel("success");
        }
        public ApiResponseModel UpdatePickUpTime(ref Models.Schema.Order shoppingCart, DateTime pickupTime)
        {
            if (shoppingCart == null || !shoppingCart.OrderItems.Any())
                return new ApiResponseModel("error", "Your shopping cart is empty, please try again");
            if (pickupTime == DateTime.MinValue)
            {
                return new ApiResponseModel("error", "Please select pick-up time");
            }
            shoppingCart.OrderGPosInformation.OrderDateTime = pickupTime;
            return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
        }
        public ApiResponseModel UpdateTips(ref Models.Schema.Order shoppingCart, decimal amount)
        {
            if (!shoppingCart.OrderItems.Any())
                return new ApiResponseModel("error", "Your shopping cart is empty, please try again");
            return shoppingCart.UpdateTips(amount);
        }
        public ApiResponseModel ApplyCoupon(ref Models.Schema.Order shoppingCart, string couponcode)
        {
            if (!shoppingCart.OrderItems.Any())
                return new ApiResponseModel("error", "Your shopping cart is empty, please try again");

            var coupon = _couponService.GetByCode(couponcode);
            if (coupon == null)
            {
                return new ApiResponseModel("error", "Promotion is not available");
            }

            var dateTimeNow = TimeZoneInfo.ConvertTime(DateTime.Now, _globalConfiguration.LocalTimeZoneInfo);
            if (coupon.StartDate > dateTimeNow || coupon.ExpiredDate < dateTimeNow) return new ApiResponseModel("error", "Promotion is not available");

            if (coupon.IsOneTimeUse)
            {
                var customerId = shoppingCart.CustomerId;
                var orderUserProfile = _orderUserProfileRepository.GetSingle(m => m.UserProfileId == customerId);
                if (orderUserProfile != null && orderUserProfile.UsedCoupons.Contains(coupon.Id))
                    return new ApiResponseModel("error", "Promotion is not available");
            }

            return shoppingCart.ApplyCoupon(new OrderCoupon
            {
                Value = coupon.Value,
                Id = coupon.Id,
                CouponCode = couponcode,
                StartDate = coupon.StartDate,
                ExpiredDate = coupon.ExpiredDate,
                ValueType = coupon.ValueType
            });
        }
        public ApiResponseModel UpdateDiscount(ref Models.Schema.Order shoppingCart)
        {
            if (!shoppingCart.OrderItems.Any())
                return new ApiResponseModel("error", "Your shopping cart is empty, please try again");
            return shoppingCart.UpdateDiscount(shoppingCart.Coupon);
        }
        public ApiResponseModel UpdateShipping(Models.Schema.Order shoppingCart, string city, string district)
        {
            var totalBill = _shippingFeeByTotalBillRepository.Get(m => m.IsActive == true);
            var feeTotalBill = totalBill.FirstOrDefault(m => m.PriceFrom <= shoppingCart.SubTotalAmount && m.PriceTo >= shoppingCart.SubTotalAmount);
            // check phí ship total bill nếu có thì update fee ship total bill còn không thì lấy phí ship khu vực
            if (feeTotalBill != null)
            {
                shoppingCart.UpdateShipping(feeTotalBill.Feeship);

            }
            else
            {
                // phí ship khu vực
                var feeShipArea = _shippingFeeByAreaRepository.GetSingle(m => m.IsDefault == false && m.City.Name == city);
                if (feeShipArea != null)
                {
                    if (string.IsNullOrEmpty(district))
                    {
                        shoppingCart.UpdateShipping(feeShipArea.Price);
                    }
                    else
                    {
                        var feeShip = feeShipArea.City.DistrictList.Find(m => m.Name == district && m.IsActive);
                        if (feeShip != null)
                        {
                            shoppingCart.UpdateShipping(feeShip.Price);
                        }
                        else
                        {
                            return new ApiResponseModel("error", "Khu vực này chưa hỗ trợ");
                        }
                        
                    }
                    
                }
                else
                {
                    var allfeeship = _shippingFeeByAreaRepository.GetSingle(m => m.IsDefault == true);
                    if (allfeeship != null)
                    {
                        shoppingCart.UpdateShipping(allfeeship.Price);
                    }
                }
            }
            if (!shoppingCart.OrderItems.Any())
                return new ApiResponseModel("error", "Your shopping cart is empty, please try again");
            return new ApiResponseModel("success",StaticMessage.DataUpdateSuccessfully);
        }
        public ApiResponseModel UpdateShippingTotalBill(Models.Schema.Order shoppingCart)
        {
            var totalBill = _shippingFeeByTotalBillRepository.Get(m => m.IsActive == true);
            var feetotalBill = totalBill.FirstOrDefault(m => m.PriceFrom <= shoppingCart.SubTotalAmount && m.PriceTo >= shoppingCart.SubTotalAmount);
            // check phí ship total bill
            if (feetotalBill != null)
            {
                shoppingCart.UpdateShipping(feetotalBill.Feeship);

            } 
            if (!shoppingCart.OrderItems.Any())
                return new ApiResponseModel("error", "Your shopping cart is empty, please try again");
            return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
        }
        public ApiResponseModel ClearCoupon(ref Models.Schema.Order shoppingCart)
        {
            if (!shoppingCart.OrderItems.Any())
                return new ApiResponseModel("error", "Your shopping cart is empty, please try again");

            return shoppingCart.ClearCoupon();
        }
        public ApiResponseModel AddItems(ref Models.Schema.Order shoppingCart,string skuId,  string skucode, int quantity)
        {
            var httpClient = _httpClientFactory.CreateClient();
            var getproductResult = httpClient.GetAsync($"{_globalConfiguration.WebsiteUrl}/api/products?skuId={skuId}&skucode={skucode}").Result;

            if (!getproductResult.IsSuccessStatusCode) return new ApiResponseModel("error", StaticMessage.ParameterInvalid);

            var itemProduct = getproductResult.Content.ReadAsAsync<SkuNacGroup>().Result;

            #region Validate product
            if (!itemProduct.IsActive)
            {
                return new ApiResponseModel("error", $"{itemProduct.Name} stopped trading");
            }
            #endregion
            var oldItem = shoppingCart.OrderItems.FirstOrDefault(m => m.SkuCode == itemProduct.SkuCode);
            if (oldItem == null)
            {
                #region validate product
                if (itemProduct.IsInventory)
                {
                    if (itemProduct.StockQuantity == 0)
                    {
                        return new ApiResponseModel("error", $"{itemProduct.Name} Out of stock");
                    }
                    if (itemProduct.StockQuantity < quantity)
                    {
                        return new ApiResponseModel("error", $"{itemProduct.Name} Out of stock");
                    }
                }
                #endregion 
                var result = shoppingCart.AddToCart(new OrderItem
                {
                    Id = Guid.NewGuid(),
                    Name = itemProduct.Name,
                    SkuId = skuId,
                    Avatar = itemProduct.ImageList.FirstOrDefault()?.Src,
                    ModifierItems = new List<OrderModifierItem>(),
                    Quantity = quantity,
                    SkuCode = itemProduct.SkuCode ?? skuId,
                    ListedPrice = itemProduct.Price,
                    Weight = itemProduct.Weight,
                    Width = itemProduct.Width,
                    Height = itemProduct.Height,
                    Length = itemProduct.Length,
                    Category = itemProduct.Category,
                    SaleTaxRate = itemProduct.SaleTaxRate != 0 ? itemProduct.SaleTaxRate : 0,
                    SkuPrice = (itemProduct.SpecialPrice != 0 && itemProduct.FromDate <= DateTime.Now && DateTime.Now <= itemProduct.ToDate ) ? itemProduct.SpecialPrice : itemProduct.Price
                }); ;
                if (result.status == "error")
                {
                    return new ApiResponseModel("error", result.message);
                }
            }
            else
            {
                if (itemProduct.IsInventory)
                {
                    if (itemProduct.StockQuantity < (oldItem.Quantity + quantity))
                    {
                        return new ApiResponseModel("error", $"{itemProduct.Name} Out of stock");
                    }
                }
                var result = shoppingCart.UpdateQuantity(oldItem.Id, oldItem.Quantity + quantity);
                if (result.status == "error")
                {
                    return new ApiResponseModel("error", result.message);
                }
            }
            
            return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
        }

        public ApiResponseModel UpdateSaleTaxRate(ref Models.Schema.Order shoppingCart)
        {

            return shoppingCart.UpdateSaleTaxRate(shoppingCart.SaleTaxRate);
        }

        public ApiResponseModel UpdateQuantity(Guid id, int quantity, ref Models.Schema.Order shoppingCart)
        {
            var sku = shoppingCart.OrderItems.FirstOrDefault( m => m.Id == id);
            if (sku == null) return new ApiResponseModel("error", StaticMessage.DataNotFound);
            var httpClient = _httpClientFactory.CreateClient();
            var getproductResult = httpClient.GetAsync($"{_globalConfiguration.WebsiteUrl}/api/products?skuId={sku.SkuId}&skucode={sku.SkuCode}").Result;

            if (!getproductResult.IsSuccessStatusCode) return new ApiResponseModel("error", StaticMessage.ParameterInvalid);

            var itemProduct = getproductResult.Content.ReadAsAsync<SkuNacGroup>().Result;
            #region Validate product
            if (!itemProduct.IsActive)
            {
                return new ApiResponseModel("error", "Product stopped trading");
            }
            if (itemProduct.IsInventory == true)
            {
                if (itemProduct.StockQuantity == 0)
                {
                    return new ApiResponseModel("error", $"{itemProduct.Name} Out of stock");
                }
                if (itemProduct.StockQuantity < quantity)
                {
                    return new ApiResponseModel("error", $"{itemProduct.Name} Out of stock");
                }
            }

            #endregion
            shoppingCart.UpdateQuantity(id, quantity);
            return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
        }

       
    }

   
}
