﻿using System.Collections.Generic;
using NacGroup.Module.Core.Data;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Order.Data;
using NacGroup.Module.Order.Models.Schema;

namespace NacGroup.Module.Order.Services
{
    public interface IShippingService
    {
        IEnumerable<CityCollection> GetCity();
        IEnumerable<DistrictCollection> GetDistrict();
        IEnumerable<WardCollection> GetWard();
        ApiResponseModel AddDistrict(DistrictCollection data);
        ApiResponseModel AddWard(WardCollection data);
        ApiResponseModel Add(CityCollection data);
        CityCollection GetByCode(string code);
    }

    public class ShippingService : IShippingService
    {
        private readonly IShippingServiceRepository _shippingServiceRepository;
        private readonly IShippingServiceDistrictRepository _shippingServiceDistrictRepository;
        private readonly IShippingServiceWardRepository _shippingServiceWardRepository;
        private readonly IUnitOfWork _unitOfWork;

        public ShippingService(IUnitOfWork unitOfWork, IShippingServiceRepository shippingServiceRepository, IShippingServiceDistrictRepository shippingServiceDistrictRepository, IShippingServiceWardRepository shippingServiceWardRepository)
        {
            _unitOfWork = unitOfWork;
            _shippingServiceRepository = shippingServiceRepository;
            _shippingServiceDistrictRepository = shippingServiceDistrictRepository;
            _shippingServiceWardRepository = shippingServiceWardRepository;
        }

        public IEnumerable<CityCollection> GetCity()
        {
            return _shippingServiceRepository.Get();
        }

        public IEnumerable<DistrictCollection> GetDistrict()
        {
            return _shippingServiceDistrictRepository.Get();
        }
        public IEnumerable<WardCollection> GetWard()
        {
            return _shippingServiceWardRepository.Get();
        }


        public ApiResponseModel Add(CityCollection data)
        {
            if (data == null) return new ApiResponseModel("error", StaticMessage.ParameterInvalid);
            if (string.IsNullOrEmpty(data.Code)) return new ApiResponseModel("error", "Code is null");
            if (string.IsNullOrEmpty(data.Name)) return new ApiResponseModel("error", "Name is null");
            if (string.IsNullOrEmpty(data.Level)) return new ApiResponseModel("error", "Level is null");
            try
            {
                var result = _shippingServiceRepository.Add(data);
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                result = _unitOfWork.Commit();
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }

        public ApiResponseModel AddDistrict(DistrictCollection data)
        {
            if (data == null) return new ApiResponseModel("error", StaticMessage.ParameterInvalid);
            if (string.IsNullOrEmpty(data.Code)) return new ApiResponseModel("error", "Code is null");
            if (string.IsNullOrEmpty(data.Name)) return new ApiResponseModel("error", "Name is null");
            if (string.IsNullOrEmpty(data.Level)) return new ApiResponseModel("error", "Level is null");
            try
            {
                var result = _shippingServiceDistrictRepository.Add(data);
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                result = _unitOfWork.Commit();
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }

        public ApiResponseModel AddWard(WardCollection data)
        {
            if (data == null) return new ApiResponseModel("error", StaticMessage.ParameterInvalid);
            if (string.IsNullOrEmpty(data.Code)) return new ApiResponseModel("error", "Code is null");
            if (string.IsNullOrEmpty(data.Name)) return new ApiResponseModel("error", "Name is null");
            if (string.IsNullOrEmpty(data.Level)) return new ApiResponseModel("error", "Level is null");
            try
            {
                var result = _shippingServiceWardRepository.Add(data);
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                result = _unitOfWork.Commit();
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }

        public CityCollection GetByCode(string code)
        {
            return _shippingServiceRepository.GetByCode(code);
        }

        public DistrictCollection GetDistrictByCode(string code)
        {
            return _shippingServiceDistrictRepository.GetByCode(code);

        }

    }
}
