﻿using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using MongoDB.Bson;
using NacGroup.Infrastructure.Shared;
using NacGroup.Module.Order.Data;
using NacGroup.Module.Order.Models;
using NacGroup.Module.Order.Models.Schema;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace NacGroup.Module.Order.Services
{
    public interface IEasyShipService
    {
        ShippingType shippingType { get; } 
        HttpResponseMessage MakeShipment(EasyShipment easyShipment);
        
    }
    public class EasyShipService : IEasyShipService
    {
        public ShippingType shippingType => ShippingType.EasyShip;
        private readonly EasyShipApiConfig _easyShipApiConfig;
        private readonly HttpClient _httpClient;


        public EasyShipService(IHttpClientFactory httpClientFactory, IEasyShipApiSettingRepository apiSettingRepository)
        {
            _httpClient = httpClientFactory.CreateClient();
            var config = apiSettingRepository.GetSetting().Value;
            var easyShipApiConfig = JsonConvert.DeserializeObject<EasyShipApiConfig>(config["Test"].ToJson());
            _easyShipApiConfig = new EasyShipApiConfig
            {
                ApiRateUrl = easyShipApiConfig.ApiRateUrl,
                ApiShipmentUrl = easyShipApiConfig.ApiShipmentUrl,
                AccessToken = easyShipApiConfig.AccessToken
            };
        }

        public HttpResponseMessage MakeEasyShipmentRequest(HttpMethod method, string parameter = null)
        {
            var resquestUri = $"{_easyShipApiConfig.ApiShipmentUrl}";
            var httprequest = new HttpRequestMessage(method, resquestUri);
            httprequest.Headers.Authorization = new AuthenticationHeaderValue("Bearer", _easyShipApiConfig.AccessToken);
            httprequest.Content = string.IsNullOrEmpty(parameter) ? null : new StringContent(parameter, Encoding.UTF8, "application/json");
            var response = _httpClient.SendAsync(httprequest).Result;
            return response;
        }

        public HttpResponseMessage MakeEasyShipRateRequest(HttpMethod method, string parameter = null)
        {
            var resquestUri = $"{_easyShipApiConfig.ApiRateUrl}";
            var httprequest = new HttpRequestMessage(method, resquestUri);
            httprequest.Headers.Authorization = new AuthenticationHeaderValue("Bearer", _easyShipApiConfig.AccessToken);
            httprequest.Content = string.IsNullOrEmpty(parameter) ? null : new StringContent(parameter, Encoding.UTF8, "application/json");
            var response = _httpClient.SendAsync(httprequest).Result;
            return response;
        }
        public HttpResponseMessage MakeRateShip(RateShip rateShip)
        {
            var response = MakeEasyShipRateRequest(HttpMethod.Post, JsonConvert.SerializeObject(rateShip));
            return response;
        }
        public HttpResponseMessage MakeShipment(EasyShipment easyShipment)
        {
                var response = MakeEasyShipmentRequest(HttpMethod.Post, JsonConvert.SerializeObject(easyShipment));
                return response;
        }

        public EasyShipModel GetShipment(string easyship_shipment_id)
        {
            try
            {
                var response = MakeEasyShipmentRequest(HttpMethod.Get, $"/{easyship_shipment_id}");
                if (response.IsSuccessStatusCode)
                {
                    return response.Content.ReadAsAsync<EasyShipModel>().Result;
                }

                return null;
            }
            catch
            {
                return null;
            }
        }

        //public HttpResponseMessage MakePayment(MxPaymentModel paymentinfo)
        //{
        //    paymentinfo.merchantId = _merchantApiConfig.MerchantId;
        //    var response = MakeMxMerchantRequest(HttpMethod.Post, "payment?echo=true&includeCustomerMatches=true", JsonConvert.SerializeObject(paymentinfo));
        //    return response;
        //}
    }

    public interface IFedExService
    {
      
       // HttpResponseMessage MakeShipment(EasyShipment easyShipment);
     
    }

    public class FedExService : IFedExService, IEasyShipService
    {
        public ShippingType shippingType => ShippingType.FedEx;
        private readonly EasyShipApiConfig _easyShipApiConfig;
        private readonly HttpClient _httpClient;


        public FedExService(IHttpClientFactory httpClientFactory, IEasyShipApiSettingRepository apiSettingRepository)
        {
            _httpClient = httpClientFactory.CreateClient();
            var config = apiSettingRepository.GetSetting().Value;
            var easyShipApiConfig = JsonConvert.DeserializeObject<EasyShipApiConfig>(config["Test"].ToJson());
            _easyShipApiConfig = new EasyShipApiConfig
            {
                ApiRateUrl = easyShipApiConfig.ApiRateUrl,
                ApiShipmentUrl = easyShipApiConfig.ApiShipmentUrl,
                AccessToken = easyShipApiConfig.AccessToken
            };
        }

        public HttpResponseMessage MakeEasyShipmentRequest(HttpMethod method, string parameter = null)
        {
            var resquestUri = $"{_easyShipApiConfig.ApiShipmentUrl}";
            var httprequest = new HttpRequestMessage(method, resquestUri);
            httprequest.Headers.Authorization = new AuthenticationHeaderValue("Bearer", _easyShipApiConfig.AccessToken);
            httprequest.Content = string.IsNullOrEmpty(parameter) ? null : new StringContent(parameter, Encoding.UTF8, "application/json");
            var response = _httpClient.SendAsync(httprequest).Result;
            return response;
        }

        public HttpResponseMessage MakeEasyShipRateRequest(HttpMethod method, string parameter = null)
        {
            var resquestUri = $"{_easyShipApiConfig.ApiRateUrl}";
            var httprequest = new HttpRequestMessage(method, resquestUri);
            httprequest.Headers.Authorization = new AuthenticationHeaderValue("Bearer", _easyShipApiConfig.AccessToken);
            httprequest.Content = string.IsNullOrEmpty(parameter) ? null : new StringContent(parameter, Encoding.UTF8, "application/json");
            var response = _httpClient.SendAsync(httprequest).Result;
            return response;
        }
        public HttpResponseMessage MakeRateShip(RateShip rateShip)
        {
            var response = MakeEasyShipRateRequest(HttpMethod.Post, JsonConvert.SerializeObject(rateShip));
            return response;
        }
        public HttpResponseMessage MakeShipment(EasyShipment easyShipment)
        {
            var response = MakeEasyShipmentRequest(HttpMethod.Post, JsonConvert.SerializeObject(easyShipment));
            return response;
        }

        public EasyShipModel GetShipment(string easyship_shipment_id)
        {
            try
            {
                var response = MakeEasyShipmentRequest(HttpMethod.Get, $"/{easyship_shipment_id}");
                if (response.IsSuccessStatusCode)
                {
                    return response.Content.ReadAsAsync<EasyShipModel>().Result;
                }

                return null;
            }
            catch
            {
                return null;
            }
        }
    }
}
