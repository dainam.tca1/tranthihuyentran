﻿using System.Collections.Generic;
using System.Linq;
using NacGroup.Module.Core.Data;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Order.Data;
using NacGroup.Module.Order.Models.Schema;

namespace NacGroup.Module.Order.Services
{
    public interface IShippingFeeByAreaService
    {
        List<ShippingFeeByDistrict> GetAll();
        ApiResponseModel Delete(List<string> idList);
        ApiResponseModel Delete(string id);
        ApiResponseModel Add(ShippingFeeByDistrict model);
        ShippingFeeByDistrict GetById(string id);
        ApiResponseModel Update(ShippingFeeByDistrict model);
    }
    public class ShippingFeeByAreaService : IShippingFeeByAreaService
    {
        private readonly IShippingFeeByAreaRepository _shippingFeeByAreaRepository;
        private readonly IUnitOfWork _unitOfWork;

        public ShippingFeeByAreaService(IShippingFeeByAreaRepository shippingFeeByAreaRepository, IUnitOfWork unitOfWork)
        {
            _shippingFeeByAreaRepository = shippingFeeByAreaRepository;
            _unitOfWork = unitOfWork;
        }

        public List<ShippingFeeByDistrict> GetAll()
        {
            return _shippingFeeByAreaRepository.Get().ToList();
        }

        public ApiResponseModel Delete(List<string> idList)
        {
            if (idList == null)
                return new ApiResponseModel("error", StaticMessage.SelectDataToRemove);
            foreach (var item in idList)
            {
                var result = Delete(item);
                if (result.status == "error") return result;
            }
            return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
        }

        public ApiResponseModel Delete(string id)
        {
            var resultcode = _shippingFeeByAreaRepository.Delete(id);
            if (resultcode < 0)
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            resultcode = _unitOfWork.Commit();
            if (resultcode < 0)
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
        }

        public ApiResponseModel Add(ShippingFeeByDistrict model)
        {
            try
            {
                #region Validate
                if (model.IsDefault == true)
                {
                    var oldModel = _shippingFeeByAreaRepository.GetSingle(m => m.IsDefault == true);
                    if (oldModel != null) return new ApiResponseModel("error", "Phí ship cho tất cả khu vực đã có rồi");
                    if (model.Price < 0)
                    {
                        return new ApiResponseModel("error", "Phí ship phải lớn hơn 0");
                    }
                    model.City.Name = "";
                    model.City.Level = "";
                    model.City.Id = "";
                    model.City.Code = "";
                    model.City.DistrictList = new List<District>();
                }
                else
                {
                    if (string.IsNullOrEmpty(model.City.Code))
                    {
                        return new ApiResponseModel("error", "Vui lòng chọn tỉnh thành");
                    }
                    var oldModel = _shippingFeeByAreaRepository.GetSingle(m => m.City.Name.Contains(model.City.Name));
                    if (oldModel != null) return new ApiResponseModel("error", "Khu vực đã tồn tại");
                   
                }
                #endregion
                var result = _shippingFeeByAreaRepository.Add(model);
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);

                result = _unitOfWork.Commit();
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);

                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }

        public ShippingFeeByDistrict GetById(string id)
        {
            return _shippingFeeByAreaRepository.GetById(id);
        }

        public ApiResponseModel Update(ShippingFeeByDistrict model)
        {
            #region Validate
            var temp = _shippingFeeByAreaRepository.GetById(model.Id);
            if (temp == null)
                return new ApiResponseModel("error", StaticMessage.DataNotFound);
          
            if (model.IsDefault == true)
            {
                if (model.Price < 0)
                {
                    return new ApiResponseModel("error", "Phí ship phải lớn hơn 0");
                }
                model.City.Name = "";
                model.City.Level = "";
                model.City.Id = "";
                model.City.Code = "";
                model.City.DistrictList = new List<District>();
            }
            else
            {
                if (string.IsNullOrEmpty(model.City.Code))
                {
                    return new ApiResponseModel("error", "Vui lòng chọn tỉnh thành");
                }
            }
            #endregion

            try
            {
                if (model.IsDefault == false) {
                    temp.City = model.City;
                }
                temp.Price = model.Price;
                var result = _shippingFeeByAreaRepository.Update(model.Id, temp);
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                result = _unitOfWork.Commit();
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);

                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }

    }
}
