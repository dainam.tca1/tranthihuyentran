﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using NacGroup.Module.Core.Data;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Order.Data;
using NacGroup.Module.Order.Models;
using Newtonsoft.Json;

namespace NacGroup.Module.Order.Services
{

    public interface IOrderPickUpSettingService
    {
        ApiResponseModel Update(OrderPickUpSetting model);
        OrderPickUpSetting Get();
    }

    public class OrderPickUpSettingService : IOrderPickUpSettingService
    {
        private readonly IOrderPickUpSettingRepository _posShoppingCartSettingRepository;
        private readonly IUnitOfWork _unitOfWork;

        public OrderPickUpSettingService(IUnitOfWork unitOfWork, IOrderPickUpSettingRepository posShoppingCartSettingRepository)
        {
            _unitOfWork = unitOfWork;
            _posShoppingCartSettingRepository = posShoppingCartSettingRepository;
        }
 

        public ApiResponseModel Update(OrderPickUpSetting model)
        {
            try
            {
                #region Validate
                if (model?.DayWork == null || model.DayOff == null || model.DaySpecial == null || model.DayWork.Count != 7)
                    return new ApiResponseModel("error", StaticMessage.ParameterInvalid);


                var temp = _posShoppingCartSettingRepository.GetSetting();

                if (temp == null)
                    return new ApiResponseModel("error", StaticMessage.DataNotFound);
                #endregion

       
                temp.Value = BsonSerializer.Deserialize<BsonDocument>(JsonConvert.SerializeObject(model));
                var result = _posShoppingCartSettingRepository.Update(temp.Id, temp);
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                result = _unitOfWork.Commit();
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);

                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }

        public OrderPickUpSetting Get()
        {
            var settingItem = _posShoppingCartSettingRepository.GetSetting();
            return OrderPickUpSetting.MapFromAppSetting(settingItem);
        }
    }
}
