﻿using System.Linq;
using System.Net.Http;
using Microsoft.AspNetCore.Mvc;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Core.Models.Schema;
using NacGroup.Module.Core.Models.ViewModels;
using NacGroup.Module.Order.Models;
using NacGroup.Module.Order.Models.Schema;
using NacGroup.Module.Order.Services;

namespace NacGroup.Module.Order.Strategy
{

    public interface IShippingStrategy
    { 
        HttpResponseMessage MakeShipment(EasyShipment easyShipment, ShippingType shippingType);   
    }
    public class ShippingStrategy : IShippingStrategy
    {
        private readonly IEasyShipService[] _easyShipService;
        public ShippingStrategy(IEasyShipService[] easyShipService)
        {
            _easyShipService = easyShipService;
        }
        public HttpResponseMessage MakeShipment(EasyShipment easyShipment,ShippingType shippingType)
        {
            return _easyShipService.FirstOrDefault(x => x.shippingType == shippingType)?.MakeShipment(easyShipment);
        }
    }
}
