﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using NacGroup.Module.Core.Interfaces;
using NacGroup.Module.Order.Data;
using NacGroup.Module.Order.Extensions;
using NacGroup.Module.Order.Services;
using NacGroup.Module.Order.Strategy;

namespace NacGroup.Module.Order
{
    public class ModuleInitializer : IModuleInitializer
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddTransient<IOrderPickUpSettingRepository, OrderPickUpSettingRepository>();
            services.AddTransient<IOrderPickUpSettingService, OrderPickUpSettingService>();

            services.AddTransient<IShippingApiSettingRepository, ShippingApiSettingRepository>();
            services.AddTransient<IEasyShipApiSettingRepository, EasyShipApiSettingRepository>();
            services.AddTransient<EasyShipService>();
            services.AddTransient<FedExService>();
            services.AddTransient<IShippingStrategy, ShippingStrategy>();
            services.AddTransient<IShipmentStrategyFactory, ShipmentStrategyFactory>();
            services.AddTransient<IEasyShipService[]>(provider =>
            {
                var factory = (IShipmentStrategyFactory) provider.GetService(typeof(IShipmentStrategyFactory));
                return factory.Create();
            });
            services.AddTransient<ICouponRepository, CouponRepository>();
            services.AddTransient<ICouponService, CouponService>();
            services.AddTransient<IOrderNacGroupService, OrderNacGroupService>();
           
            services.AddTransient<IOrderNacGroupCmsService, OrderNacGroupCmsService>();
            services.AddTransient<IShippingService, ShippingService>();
            services.AddTransient<IShippingServiceRepository, ShippingServiceRepository>();   
            services.AddTransient<IShippingServiceDistrictRepository, ShippingServiceDistrictRepository>();
            services.AddTransient<IShippingServiceWardRepository, ShippingServiceWardRepository>();
            services.AddTransient<IShippingFeeByTotalBillService, ShippingFeeByTotalBillService>();
            services.AddTransient<IShippingFeeByTotalBillRepository, ShippingFeeByTotalBillRepository>();
            services.AddTransient<IShippingFeeByAreaService, ShippingFeeByAreaService>();
            services.AddTransient<IShippingFeeByAreaRepository, ShippingFeeByAreaRepository>();

            services.AddTransient<IOrderRepository, OrderRepository>();
            services.AddTransient<IOrderPickupRepository, OrderPickupRepository>();
            services.AddTransient<IOrderUserProfileRepository, OrderUserProfileRepository>();

        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {

        }
    }
}
