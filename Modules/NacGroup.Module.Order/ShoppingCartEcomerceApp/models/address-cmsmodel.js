class addressCmsModel {
    constructor() {
        this.Id = null;
        this.FullName = null;
        this.Phone = null;
        this.Street = null;
        this.PhoneNumber = null;
        this.District = null;
        this.City = null;
        this.Ward = null;
        this.IsDefault = false;
    }
}

module.exports = addressCmsModel;
