

class OrderCmsModel {
    constructor() {
        this._id = null;
        this.OrderItems = [];
        this.CreatedDate = new Date();
        this.TotalTax = 0;
        this.TotalAmount = 0;
        this.TotalSubAmount = 0;
        this.TotalShipping = 0;
        this.FirstName = null;
        this.LastName = null;
        this.CustomerId = null;
        this.CustomerEmail = null;
        this.CustomerPhone = null;
        this.Notes = null;
        this.City = null;
        this.District = null;
        this.Street = null;
        this.Ward = null;
        this.OrderStatusNacGroup = 0;
        this.OrderPaymentStatusNacGroup = 0;
        this.OrderPaymentMethodNacGroup = 0;
        this.ConfirmDate = new Date();
        this.Gender = 0;
        this.Discount = null;
        this.Recipient = {
            Id : null,
            FullName : null,
            Phone : null,
            Street : null,
            PhoneNumber : null,
            District : null,
            City : null,
            Ward : null,
            IsDefault : false,
        };
        this.UpdatedDate = null;
        this.OrderMxMerchant = {
            AuthMessage: null,
            CardType: null,
            AuthCode: null,
            CardAccount: null,
            RecordNo: null,
            ServerDate: null,
            ServerTime: null
        };
    }
}
module.exports = OrderCmsModel;
