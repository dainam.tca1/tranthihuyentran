//#region Thông báo
const GLOBAL_ERROR_MESSAGE = "Opps! System has some error. Please try again!";
const PARAMETER_INVALID = "Parameters is invalable";
const DATA_UPDATE_SUCCESSFULL = "Update successfully";
const DATA_UPDATE_IMG = "Upload image successfully";
const DATA_NOTFOUND = "Data is not found";
const DATA_NOT_RECOVER = "Data won't be recovered?";
const DATA_NOT_SELECT = "Please select data need to remove";
const DATA_LOADING_SUCCESSFULL = "Success";
const DATA_CHANGE_PLAYLIST = "Do you want all devices to run 1 play list";
//#endregion
//#region validate CategoryBlog
const VALID_TITLE_CATE = "Vui lòng nhập tên chuyên mục";
const VALID_URL = "Vui lòng nhập URL";
const VALID_SELECT_CATE = "Vui lòng chọn danh mục";
const VALID_URL_EXIST = "URL đã tồn tại vui lòng thử lại";
const VALID_PRICE_SERVICE = "Vui lòng nhập giá dịch vụ";
const VALID_TITLE_SERVICE = "Vui lòng nhập tên dịch vụ";
const VALID_TIME_SERVICE = "Vui lòng nhập thời gian dịch vụ";
const VALID_TIME = "Thời gian phải là số";
const VALID_PRICE = "Tiền phải là số";
const VALID_NAME = "Vui lòng bạn nhập họ và tên";
const VALID_SELECT_SERVICE = "Vui lòng chọn nghiệp vụ nhân viên";
//#endregion
//#region Text UI
const ERROR = "Error";
const SUCCESS = "Success";
const NOTI = "Notification";
//#endregion

//#region Message giỏ hàng
const QUANTITY_MORETHAN_ZERO = "Quantity must be greater than zero";
//#endregion
module.exports = {
  VALID_NAME,
  VALID_SELECT_SERVICE,
  VALID_TIME,
  VALID_PRICE,
  VALID_TITLE_SERVICE,
  VALID_PRICE_SERVICE,
  VALID_SELECT_CATE,
  VALID_TIME_SERVICE,
  GLOBAL_ERROR_MESSAGE,
  PARAMETER_INVALID,
  DATA_NOT_RECOVER,
  DATA_NOT_SELECT,
  DATA_UPDATE_SUCCESSFULL,
  DATA_UPDATE_IMG,
  DATA_LOADING_SUCCESSFULL,
  DATA_CHANGE_PLAYLIST,
  VALID_URL_EXIST,
  VALID_URL,
  VALID_TITLE_CATE,
  DATA_NOTFOUND,
  ERROR,
  SUCCESS,
  NOTI,
  QUANTITY_MORETHAN_ZERO
};
