const THEME_TEXT_TYPE = {
  Normal: 0,
  Html: 1,
  Link: 2
};

const THEMETYPE = {
  Group: 0,
  Image: 1,
  Text: 2,
  ImageList: 3,
  TextImageList: 4
};

const BOOKINGTYPE = {
  Individual: 0,
  Group: 1
};

const GENDERTYPE = {
  Undefine: 0,
  Male: 1,
  Female: 2
};
const DEVICE_STATUS = {
    Inactive: 0,
    Active: 1,
    Warning: 2,
};
const WEBSITE_STATUS = {
    Inactive: 0,
    Active: 1,
    Warning: 2,
}
const PRODUCT_TYPE = {
    Website: 0,
    Device: 1,
    Hosting: 2,
    Video: 3,
    Design: 4
};
const ORDER_STATUS = {
    New: 0,
    Deployed: 1,
    Complete: 2,
    Cancel: 3
};
const PAYMENT_STATUS = {
    NotPaid: 0,
    Paid: 1
};
const PAYMENT_METHOD = {
    COD: 0,
    CreditCard: 1
};
module.exports = {
  THEMETYPE,
  THEME_TEXT_TYPE,
  BOOKINGTYPE,
  GENDERTYPE,
  DEVICE_STATUS,
  WEBSITE_STATUS,
    PRODUCT_TYPE,
    ORDER_STATUS,
    PAYMENT_STATUS,
  PAYMENT_METHOD
};
