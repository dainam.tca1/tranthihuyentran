import React, { Component } from "react";
import Process from "./process";
import AddressModel from "../models/address-cmsmodel";
import OrderCmsModel from "../models/order-cmsmodel";

const apiurl = "/ordernacgroup";

class Shipping extends Component {
    constructor() {
        super();
        var that = this;
        that.state = {
            model: new OrderCmsModel(),
            ex: {
                Title: "Thông tin vận chuyển & địa chỉ",
                Step:1,
                User: null,
                CityList: [],
                WardList: [],
                WardTempList: [],
                DistrictList: [],
                DistrictTempList: [],
                AddressModel: new AddressModel(),
                IsUpdate: false
            }
        };
        $("#loading").show();
        that.setState(that.state);
        $.get(apiurl + "/getcart", function (response) {
            that.state.model = response;
            that.setState(that.state);
            $.get("/account/profile",
                function (response) {
                    if (response != null) {
                        that.state.ex.User = response;
                        that.setState(that.state);
                        $.get(apiurl + "/getcity",
                            res => {
                                that.state.ex.CityList = res;
                                that.setState(that.state);
                            });
                        $.get(apiurl + "/getdistrict",
                            res => {
                                that.state.ex.DistrictTempList = res;
                                that.setState(that.state);
                            });
                        $.get(apiurl + "/getward",
                            res => {
                                that.state.ex.WardTempList = res;

                                that.setState(that.state);
                            });
                        $("#loading").hide();
                    }
                 

                }).fail(res => {
                    location.href = "/account/login?ReturnUrl=/checkout/address";
                });
            if (that.state.model.OrderItems.length <= 0) {
                window.location.replace("/");
            }

        }).fail(res => {
            location.href = "/account/login?ReturnUrl=/checkout/address";
        });

    }


    handleUpdateRecipient(obj) {
        $("#loading").show();
        $.ajax({
            url: apiurl + "/updateshipping",
            type: "POST",
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify({
                model: obj
            }),
            success: response => {
                $("#loading").hide();
                if (response.status == "success") {
                    window.location.href = "/checkout/payment";
                } else {
                    toastr["error"](response.message, ERROR);
                }
            }
        });
    }
    handleEditAddress(id) {
        //update

        $("#loading").show();
        if (id == null || id == "") {
            $("#loading").hide();
            alertify.alert("Lỗi!", "Tham số không hợp lệ");
            return -1;
        }
        this.state.ex.AddressModel = this.state.ex.User.Recipient.find(e => { return e.Id == id });
        this.state.ex.DistrictList = this.state.ex.DistrictTempList
            ? this.state.ex.DistrictTempList.filter(d => { return d.City.Name == this.state.ex.AddressModel.City })
            : [];
        this.state.ex.WardList = this.state.ex.WardTempList
            ? this.state.ex.WardTempList.filter(d => { return d.District.Name == this.state.ex.AddressModel.District })
            : [];
       // console.log(this.state.ex.WardList);
        if (this.state.ex.AddressModel == null) {
            alertify.alert("Lỗi!", "Không tìm thấy dữ liệu");
            return -1;
        } else {
            $("#address-form").is(":visible") ? $("#address-form").hide(0, function () {
                $("#address-form").slideDown("slow", function () {
                    $("html,body").animate({
                        scrollTop: $("#address-form").offset().top - 200
                    }, 300)
                })
            }) : $("#address-form").slideDown("slow", function () {
                $("html,body").animate({
                    scrollTop: $("#address-form").offset().top - 200
                }, 300)
            })
            setTimeout(() => {
                $("#loading").hide();
            }, 1000)
        }
        this.state.ex.IsUpdate = true;
        this.setState(this.state);
    }

    handleRemoveAddress(id) {
        var that = this;
        if (id == "" || id == null) {
            alertify.alert("Lỗi!", "Tham số không hợp lệ.");
            return -1;
        }
        alertify.confirm('Bạn có chắc chắn!', 'Dữ liệu không thể phục hồi.', function () {
            $("#loading").show();
            that.state.ex.User.Recipient = that.state.ex.User.Recipient.filter(e => { return e.Id != id });
            if (that.state.ex.User.Recipient.length == 0) {
                that.state.ex.User.Recipient = [];
            }
            $.ajax({
                url: "/account/profile",
                type: "POST",
                data: that.state.ex.User,
                success: function (response) {
                    $("#loading").hide();
                    if (response.status == "success") {
                        alertify.alert("Success", response.message, function () {
                            that.props.history.push("/acc/emptypage");
                            that.props.history.replace({
                                pathname: that.props.location.pathname,
                                search: that.props.location.search
                            });
                        });
                    } else {
                        alertify.alert("Lỗi!", response.message);
                    }
                },
                error: function (er) {
                    $("#loading").hide();
                    alertify.alert(
                        "Lỗi",
                        "Rất tiếc! Hệ thống đang gặp trục trặc. Vui lòng thử lại sau!"
                    );
                }
            });

        }
            , function () { alertify.error('Hủy bỏ') });

    }
    description() {
        var tenant = $("#tenantName").val();
        if (tenant == "NailSuplier") {
            return (
                <div className="des">
                    <p>- FREE SHIPPING for purchase $100 & up (US only).</p>
                    <p>- Purchase under $100 flat shipping fee $13.95 (US only).</p>
                    <p>- Customers from Hawaii, Virgin Islands, Puerto Rico call to order.</p>
                </div>
            )
        }
    }
    submitprofile() {
        var that = this;
        $("#loading").show();
        //#region Validate
        if (this.state.ex.AddressModel.FullName == null || this.state.ex.AddressModel.FullName == "") {
            $("#loading").hide();
            alertify.alert("Lỗi!", "Vui lòng điền họ và tên");
            return -1;
        }
    
        if (this.state.ex.AddressModel.PhoneNumber == null || this.state.ex.AddressModel.PhoneNumber == "") {
            $("#loading").hide();
            alertify.alert("Lỗi!", "Vui lòng điền số điện thoại");
            return -1;
        }
       
        if (this.state.ex.AddressModel.City == null || this.state.ex.AddressModel.City == "") {
            $("#loading").hide();
            alertify.alert("Lỗi!", "Vui lòng chọn thành phố");
            return -1;
        }
        if (this.state.ex.AddressModel.District == null || this.state.ex.AddressModel.District == "") {
            $("#loading").hide();
            alertify.alert("Lỗi!", "Vui lòng chọn quận, huyện");
            return -1;
        }
        if (this.state.ex.AddressModel.Ward == null || this.state.ex.AddressModel.Ward == "") {
            $("#loading").hide();
            alertify.alert("Lỗi!", "Vui lòng chọn phường");
            return -1;
        }
        if (this.state.ex.AddressModel.Street == null || this.state.ex.AddressModel.Street == "") {
            $("#loading").hide();
            alertify.alert("Lỗi!", "Vui lòng điền địa chỉ");
            return -1;
        }

        //#endregion

        if (this.state.ex.IsUpdate == false) {
            if (this.state.ex.User.Recipient.length == 0) {
                //check if addresslist of user not found Id set = 1 
                this.state.ex.AddressModel.Id = 1;
                this.state.ex.AddressModel.IsDefault = true;
                // push object to array
                this.state.ex.User.Recipient.push(this.state.ex.AddressModel);

            }

            else {
                //find address newest of modelProfile
                var oldAddress = this.state.ex.User.Recipient[this.state.ex.User.Recipient.length - 1];
                if (oldAddress == null) {
                    $("#loading").hide();
                    alertify.alert("Lỗi!", "Không tìm thấy dữ liệu");
                    return -1;
                }
                //if model IsDefault == true
                if (this.state.ex.AddressModel.IsDefault == true) {
                    var findOldDefaultAddress = this.state.ex.User.Recipient.filter(e => { return e.IsDefault == true });
                    if (findOldDefaultAddress.length > 0) {
                        this.state.ex.User.Recipient = this.state.ex.User.Recipient ? this.state.ex.User.Recipient.map(e => {
                            return { ...e, IsDefault: false };
                        }) : [];

                    }
                }
                this.state.ex.AddressModel.Id = parseInt(oldAddress.Id) + 1;
                this.state.ex.User.Recipient.push(this.state.ex.AddressModel);
            }
        } else {
            if (that.state.ex.AddressModel.IsDefault == true) {
                that.state.ex.User.Recipient.forEach(e => {
                    if (e.Id == that.state.ex.AddressModel.Id) {
                        e = that.state.ex.AddressModel;
                        e.IsDefault = true;

                    } else {
                        e.IsDefault = false;

                    }

                })
            } else {
                that.state.ex.User.Recipient.forEach(e => {
                    if (e.Id == that.state.ex.AddressModel.Id) {
                        e = that.state.model;

                    }
                })
            }
        }

        //update profile ajax
        $.ajax({
            url: "/account/profile",
            type: "POST",
            data: this.state.ex.User,
            success: function (response) {
                $("#loading").hide();
                if (response.status == "success") {
                    if (that.state.ex.IsUpdate == false) {
                        that.handleUpdateRecipient(that.state.ex.AddressModel);
                    }
                    alertify.alert("Success", response.message, function () {
                        that.props.history.push("/checkout/emptypage");
                        that.props.history.replace({
                            pathname: that.props.location.pathname,
                            search: that.props.location.search
                        });
                    });
                } else {
                    alertify.alert("Lỗi!", response.message);
                }
            },
            error: function (er) {
                $("#loading").hide();
                alertify.alert(
                    "Lỗi",
                    "Rất tiếc! Hệ thống đang gặp sự cố. Vui lòng thử lại sau!"
                );
            }
        });
    }
    /**
     * ResetForm
     */
    ResetForm() {
        var that = this;
        this.state.ex.AddressModel = new AddressModel();
        //#region reset list 
        this.state.ex.DistrictList = [];
        that.state.ex.AddressModel.District = "";
        that.state.ex.WardList = [];
        that.state.ex.AddressModel.Ward = "";
        //#endregion
        //#region clear field value input
        $("input[name='fullname']").val("");
        $("input[name='phonenumber']").val("");
        $("textarea[name='address']").val("");
     
        //#endregion



        this.setState(this.state);
    }
    componentWillMount() { }
    componentDidMount() {
        //Sửa title trang
        document.title = "Danh sách địa chỉ";
        // this.setState(this.state);
        //Active hight light menu
    }
    render() {
        return (
            <React.Fragment>
                {this.state.ex && this.state.model ? (
                    this.state.model.OrderItems.length > 0 ||
                        this.state.ex.User != null ? (
                            <React.Fragment>
                                <h1 className="hidden">{this.state.ex.Titlte}</h1>

                                <div className="container" id="shipping-address">
                                    <div className="row hidden-xs clearfix bs-wizard">
                                        <Process Step={this.state.ex.Step} />
                                    </div>
                                    <div className="row clearfix">
                                        <div className="col-md-12 p-2">
                                            <h3>1. Địa chỉ giao hàng</h3>
                                            <h5 className="visible-md-block visible-lg-block">Chọn một địa chỉ giao hàng có sẵn bên dưới:</h5>
                                            {this.description()}

                                        </div>
                                    </div>
                                    <div className="row clearfix">
                                        {this.state.ex.User ? this.state.ex.User.Recipient.map(
                                            c => {
                                                return (
                                                    <div className="col-md-6 col-sm-6 col-xs-12">
                                                        <div className={`panel panel-default address-item ${c.IsDefault == true ? 'is-default' : ''}`}>
                                                            <div className="panel-body">
                                                                <p className="name">{c.FullName}</p>
                                                                <p className="address" title={`${c.Street}, ${c.Ward}, ${c.District}, ${c.City}`}>
                                                                    Địa chỉ: {`${c.Street}, ${c.Ward}, ${c.District}, ${c.City}`}</p>
                                                                <p className="address">
                                                                    VN </p>
                                                                <p className="phone">Số điện thoại: {c.PhoneNumber}</p>
                                                                <p className="action">
                                                                    <a href="javascript:void(0)" onClick={e => { this.handleUpdateRecipient(c) }} className={`btn ${c.IsDefault == true ? 'btn-primary' : 'btn-second'}`}>
                                                                        Gửi đến địa chỉ này
                                                                        </a>
                                                                    <a href="javascript:void(0)" onClick={e => { this.handleEditAddress(c.Id) }} className="btn btn-default" >Sửa </a>
                                                                    {c.IsDefault == false ? (
                                                                        <a href="javascript:void(0)" onClick={e => { this.handleRemoveAddress(c.Id) }} className="btn btn-default" >Xóa</a>
                                                                    ) : ""}
                                                                </p>
                                                                {c.IsDefault == true ? (
                                                                    <span className="default">Mặc định</span>
                                                                ) : ""}

                                                            </div>
                                                        </div>
                                                    </div>
                                                )
                                            }
                                        ) : ""}
                                    </div>
                                    <div className="row clearfix">
                                        <div className="col-md-6 col-sm-6 col-xs-12">
                                            <p className="other p-2">
                                                Bạn muốn giao hàng đến địa chỉ này? <a href="javascript:void(0)" onClick={e => {
                                                    $("#loading").show();
                                                    $("#address-form").is(":visible") ? $("#address-form").hide(0, function () {

                                                        $("#address-form").slideDown("slow", function () {
                                                            $("#loading").hide();
                                                            $("html,body").animate({
                                                                scrollTop: $("#address-form").offset().top - 200
                                                            }, 200)
                                                        })
                                                    }) : $("#address-form").slideDown("slow", function () {
                                                        $("#loading").hide();
                                                        $("html,body").animate({
                                                            scrollTop: $("#address-form").offset().top - 200
                                                        }, 200)
                                                    });
                                                    this.state.ex.IsUpdate = false;
                                                    this.ResetForm();
                                                    this.setState(this.state);
                                                }} id="addNewAddress">Thêm mới địa chỉ giao hàng</a>
                                            </p>
                                        </div>

                                    </div>
                                    <div className="row clearfix">
                                        <div className="col-md-12">
                                            <div id="address-form" className="m-bot-2" style={{ "display": "none" }}>
                                                <div className="panel-body">
                                                   
                                                    <div className="form-group row">
                                                        <label for="firstname" className="col-lg-4 control-label visible-lg-block">Họ và tên(<span className="required">*</span>)</label>
                                                        <div className="col-lg-8 input-wrap has-feedback">
                                                            <input
                                                                type="text"
                                                                name="fullname"
                                                                className="form-control"
                                                                value={this.state.ex.AddressModel.FullName}
                                                                placeholder="Họ và tên"
                                                                onChange={e => {
                                                                    this.state.ex.AddressModel.FullName = e.target.value;
                                                                    this.setState(this.state);
                                                                }}
                                                            />
                                                        </div>
                                                    </div>                                    
                                                    <div className="form-group row">
                                                        <label for="lastname" className="col-lg-4 control-label visible-lg-block">Số điện thoại (<span className="required">*</span>)</label>
                                                        <div className="col-lg-8 input-wrap has-feedback">
                                                            <input
                                                                type="text"
                                                                name="phonenumber"
                                                                className="form-control"
                                                                value={this.state.ex.AddressModel.PhoneNumber}
                                                                placeholder="Số điện thoại"
                                                                onChange={e => {
                                                                    this.state.ex.AddressModel.PhoneNumber = e.target.value;
                                                                    this.setState(this.state);
                                                                }}
                                                            />
                                                        </div>
                                                    </div>
                                                  
                                                    <div className="form-group row">
                                                        <label for="telephone" className="col-lg-4 control-label visible-lg-block">Tỉnh, Thành phố (<span className="required">*</span>)</label>
                                                        <div className="col-lg-8 input-wrap has-feedback">
                                                            
                                                            <select className="form-control"
                                                                    onChange={e => {
                                                                    var that = this;
                                                                        if (e.target.value == "" || e.target.value == null) {
                                                                            $("#loading").show();
                                                                            setTimeout(() => {
                                                                                $("#loading").hide();
                                                                                that.state.ex.DistrictList = [];
                                                                                that.state.ex.AddressModel.District = "";
                                                                                that.state.ex.WardList = [];
                                                                                that.state.ex.AddressModel.Ward = "";
                                                                                that.setState(that.state);
                                                                                return -1;
                                                                            }, 500);
                                                                        };

                                                                        this.state.ex.DistrictList = this.state.ex.DistrictTempList.filter(d => { return d.City.Name == e.target.value });
                                                                        that.state.ex.WardList = [];
                                                                        that.state.ex.AddressModel.Ward = "";
                                                                        this.state.ex.AddressModel.City = e.target.value;
                                                                    this.setState(this.state);
                                                                }}>
                                                                <option value="" readOnly>-- Chọn thành phố --</option>
                                                                {this.state.ex.CityList ? this.state.ex.CityList.map(c => {
                                                                        return <option value={c.Name} selected={this
                                                                            .state.ex.AddressModel.City ==
                                                                            c.Name
                                                                            ? "selected"
                                                                            : ""}>{c.Name}</option>;
                                                                    }) : <option value="" readOnly>-- Chọn thành phố --</option>}

                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div className="form-group row">
                                                        <label for="state" className="col-lg-4 control-label visible-lg-block">Quận , huyện (<span className="required">*</span>)</label>
                                                        <div className="col-lg-8 input-wrap has-feedback">
                                                          
                                                            <select
                                                                className="form-control"
                                                                onChange={e => {
                                                                    var that = this;
                                                                    if (e.target.value == "" || e.target.value == null) {
                                                                        that.state.ex.WardList = [];
                                                                        that.state.ex.AddressModel.Ward = "";
                                                                        that.setState(that.state);
                                                                        return -1;
                                                                    };

                                                                    this.state.ex.WardList = this.state.ex.WardTempList.filter(d => { return d.District.Name == e.target.value });
                                                                    this.state.ex.AddressModel.District = e.target.value;
                                                                    this.setState(this.state);
                                                                }}>
                                                                <option value="" readOnly>-- Chọn quận, huyện --</option>
                                                                {this.state.ex.DistrictList ? this.state.ex.DistrictList.map(c => {
                                                                        return <option value={c.Name} selected={this
                                                                            .state.ex.AddressModel.District ==
                                                                            c.Name
                                                                            ? "selected"
                                                                            : ""}>{c.Name}</option>;
                                                                    }) : <option value="" readOnly>-- Chọn quận, huyện --</option>}

                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div className="form-group row">
                                                        <label for="city" className="col-lg-4 control-label visible-lg-block">Phường xã (<span className="required">*</span>)</label>
                                                        <div className="col-lg-8 input-wrap has-feedback">
                                                        
                                                            <select
                                                                className="form-control"
                                                                onChange={e => {
                                                                    this.state.ex.AddressModel.Ward = e.target.value;
                                                                    this.setState(this.state);
                                                                }}>
                                                                <option value="" readOnly>-- Chọn phường xã --</option>
                                                                {this.state.ex.WardList ? this.state.ex.WardList.map(c => {
                                                                        return <option value={c.Name} selected={this
                                                                            .state.ex.AddressModel.Ward ==
                                                                            c.Name
                                                                            ? "selected"
                                                                            : ""}>{c.Name}</option>;
                                                                    }) : <option value="" readOnly>-- Chọn phường xã --</option>}

                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div className="form-group row">
                                                        <label for="street1" className="col-lg-4 control-label visible-lg-block">Địa chỉ (<span className="required">*</span>)</label>
                                                        <div className="col-lg-8 input-wrap has-feedback">
                                                            <textarea
                                                                name="address"
                                                                className="form-control"
                                                                value={this.state.ex.AddressModel.Street}
                                                                placeholder="Địa chỉ"
                                                                onChange={e => {
                                                                    this.state.ex.AddressModel.Street = e.target.value;
                                                                    this.setState(this.state);
                                                                }}
                                                            ></textarea>
                                                        </div>
                                                    </div>

                                                    <div className="form-group row">
                                                        <div className="col-lg-8 col-md-offset-4">
                                                            <label className="checkbox"> Chọn mặc định
                                                                <input
                                                                    type="checkbox"
                                                                    checked={this.state.ex.AddressModel.IsDefault}
                                                                    value={this.state.ex.AddressModel.IsDefault}
                                                                    onClick={e => {
                                                                        this.state.ex.AddressModel.IsDefault = e.target.checked;
                                                                        this.setState(this.state);
                                                                    }} />
                                                                <span className="checkmark"></span>
                                                            </label>
                                                        </div>

                                                    </div>
                                                    <div className="form-group row btn-container">
                                                        <div className="col-lg-8 col-md-offset-4">
                                                            <a href="javascript:void(0)" onClick={e => {
                                                                $("#loading").show();
                                                                if (this.state.ex.AddressModel != null) {
                                                                    this.state.ex.AddressModel = new AddressModel();
                                                                }

                                                                $("#address-form").is(":visible") ? $("#address-form").hide(0, function () {
                                                                    $("#address-form").slideUp("slow", function () {
                                                                        $("html,body").animate({
                                                                            scrollTop: $("#address-form").offset().top - 200
                                                                        }, 300);
                                                                        $("#loading").hide();
                                                                    })
                                                                }) : $("#address-form").slideUp("slow", function () {

                                                                    $("html,body").animate({
                                                                        scrollTop: $("#address-form").offset().top - 200
                                                                    }, 300);
                                                                    $("#loading").hide();
                                                                });
                                                                this.setState(this.state);
                                                            }} className="btn btn-default btn-outline">Hủy bỏ</a>
                                                            <a href="javascript:void(0)" onClick={e => { this.submitprofile() }} className="btn btn-primary">Lưu</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </React.Fragment>
                        ) : (
                            ""
                        )
                ) : ""}
            </React.Fragment>
        );
    }
}

export default Shipping;
