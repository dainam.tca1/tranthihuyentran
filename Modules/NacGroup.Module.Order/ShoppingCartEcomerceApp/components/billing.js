import React, { Component } from "react";
import Process from "./process";
import AddressModel from "../models/address-cmsmodel";
import OrderCmsModel from "../models/order-cmsmodel";

const apiurl = "/ordernacgroup";

class Shipping extends Component {
    constructor() {
        super();
        var that = this;
        that.state = {
            model: new OrderCmsModel(),
            ex: {
                Title: "Payment Information & Address",
                Step: 3,
                User: null,
                AddressModel: new AddressModel(),
                IsUpdate: false,
                StateList: [],
                CountryList:[]
            }
        };
        $("#loading").show();
        that.setState(that.state);
        $.get(apiurl + "/getcart", function (response) {
            that.state.model = response;
            that.setState(that.state);
            that.GetProfile();
            if (that.state.model.OrderItems.length <= 0) {
                window.location.replace("/");
            }

        }).fail(res => {
            location.href = "/account/login?ReturnUrl=/checkout/cart";
        });

    }
    GetProfile() {
        var that = this;
        $.get("/account/profile",
            function (response) {
                that.state.ex.User = response;
                $.get("/account/getcountry", res => {
                    that.state.ex.CountryList = res;
                            
                    that.setState(that.state);
                })
                that.setState(that.state);
                $("#loading").hide();
            }).fail(res => {
                location.href = "/account/login?ReturnUrl=/checkout/shipping";
            });
    }
  
    handleChangeModel(e) {
        this.state.model = e;
        this.setState(this.state);
    }
    handleUpdateBilling(obj) {
        $("#loading").show();
        $.ajax({
            url: apiurl + "/updatebilling",
            type: "POST",
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify({
                model: obj
            }),
            success: response => {
                $("#loading").hide();
                if (response.status == "success") {
                    window.location.href = "/checkout/payment";
                } else {
                    toastr["error"](response.message, ERROR);
                }
            }
        });
    }
    handleEditAddress(id) {
        //update

        $("#loading").show();
        if (id == null || id == "") {
            $("#loading").hide();
            alertify.alert("Opps!", "Parameter invalid");
            return -1;
        }
        
        this.state.ex.AddressModel = this.state.ex.User.Billing.find(e => { return e.Id == id });
        this.state.ex.StateList = (this.state.ex.CountryList.find(country => { return country.Code == this.state.ex.AddressModel.Country.Value }) != null) ?
            this.state.ex.CountryList.find(country => { return country.Code == this.state.ex.AddressModel.Country.Value }).StateList : [];     
        if (this.state.ex.AddressModel == null) {
            $("#loading").hide();
            alertify.alert("Opps!", "Data Notfound");
            return -1;
        } else {
            $("#address-form").is(":visible") ? $("#address-form").hide(0, function () {
                $("#address-form").slideDown("slow", function () {
                    $("#loading").hide();
                    $("html,body").animate({
                        scrollTop: $("#address-form").offset().top - 200
                    }, 300)
                })
            }) : $("#address-form").slideDown("slow", function () {
                $("#loading").hide();
                $("html,body").animate({
                    scrollTop: $("#address-form").offset().top - 200
                }, 300)
            })
           
        }
        this.state.ex.IsUpdate = true;
        this.setState(this.state);
    }

    submitprofile() {
        var that = this;
        $("#loading").show();
        //#region Validate
        if (this.state.ex.AddressModel.BusinessName == null || this.state.ex.AddressModel.BusinessName == "") {
            $("#loading").hide();
            alertify.alert("Opps!", "Please enter display name address");
            return -1;
        }
        if (this.state.ex.AddressModel.FirstName == null || this.state.ex.AddressModel.FirstName == "") {
            $("#loading").hide();
            alertify.alert("Opps!", "Please enter first name");
            return -1;
        }
        if (this.state.ex.AddressModel.LastName == null || this.state.ex.AddressModel.LastName == "") {
            $("#loading").hide();
            alertify.alert("Opps!", "Please enter last name");
            return -1;
        }
        if (this.state.ex.AddressModel.PhoneNumber == null || this.state.ex.AddressModel.PhoneNumber == "") {
            $("#loading").hide();
            alertify.alert("Opps!", "Please enter phone number");
            return -1;
        }
        if (this.state.ex.AddressModel.Street1 == null || this.state.ex.AddressModel.Street1 == "") {
            $("#loading").hide();
            alertify.alert("Opps!", "Please enter address");
            return -1;
        }
        if (this.state.ex.AddressModel.City == null || this.state.ex.AddressModel.City == "") {
            $("#loading").hide();
            alertify.alert("Opps!", "Please enter city");
            return -1;
        }
        if (this.state.ex.StateList.length > 0) {
            if (this.state.ex.AddressModel.State == null || this.state.ex.AddressModel.State == "") {
                $("#loading").hide();
                    alertify.alert("Opps!", "Please enter state");
                    return -1;
                }
        }
        if (this.state.ex.AddressModel.ZipCode == null || this.state.ex.AddressModel.ZipCode == "") {
            $("#loading").hide();
            alertify.alert("Opps!", "Please enter zip code");
            return -1;
        }
        if (this.state.ex.AddressModel.Country == null || this.state.ex.AddressModel.Country == "") {
            $("#loading").hide();
            alertify.alert("Opps!", "Please enter country");
            return -1;
        }
        //#endregion

        if (this.state.ex.IsUpdate == false) {
            if (this.state.ex.User.Billing.length == 0) {
                //check if addresslist of user not found Id set = 1 
                this.state.ex.AddressModel.Id = 1;
                this.state.ex.AddressModel.IsDefault = true;
                // push object to array
                this.state.ex.User.Billing.push(this.state.ex.AddressModel);     
            }

            else {
                //find address newest of modelProfile
                var old_address = this.state.ex.User.Billing[this.state.ex.User.Billing.length - 1];
                if (old_address == null) {
                    $("#loading").hide();
                    alertify.alert("Opps!", "Data not found");
                    return -1;
                }
                //if model IsDefault == true
                if (this.state.ex.AddressModel.IsDefault == true) {
                    var find_old_default_address = this.state.ex.User.Billing.filter(e => { return e.IsDefault == true });
                    if (find_old_default_address.length > 0) {
                        this.state.ex.User.Billing = this.state.ex.User.Billing ? this.state.ex.User.Billing.map(e => {
                            return { ...e, IsDefault: false };
                        }) : [];
                        
                    }
                }
                this.state.ex.AddressModel.Id = parseInt(old_address.Id) + 1;
                this.state.ex.User.Billing.push(this.state.ex.AddressModel);
            }
        } else {
            if (that.state.ex.AddressModel.IsDefault == true) {
                that.state.ex.User.Billing.forEach(e => {
                    if (e.Id == that.state.ex.AddressModel.Id) {
                        e = that.state.ex.AddressModel;
                        e.IsDefault = true;
                       
                    } else {
                        e.IsDefault = false;
                        
                    }

                })
            } else {
                that.state.ex.User.Billing.forEach(e => {
                    if (e.Id == that.state.ex.AddressModel.Id) {
                        e = that.state.model;
                        
                    }
                })
            }
        }

        //update profile ajax
        $.ajax({
            url: "/account/profile",
            type: "POST",
            data: this.state.ex.User,
            success: function (response) {
                $("#loading").hide();
                if (response.status == "success") {
                    if (that.state.ex.IsUpdate == false) {
                        that.handleUpdateBilling(that.state.ex.AddressModel);
                    }
                    alertify.alert("Success", response.message, function () {
                        that.props.history.push("/checkout/emptypage");
                        that.props.history.replace({
                            pathname: that.props.location.pathname,
                            search: that.props.location.search
                        });
                    });
                } else {
                    alertify.alert("Opps!", response.message);
                }
            },
            error: function (er) {
                $("#loading").hide();
                alertify.alert(
                    "Error",
                    "Opps! System has some error. Please try again!"
                );
            }
        });
    }

    handleUseShippingAddress() {
        this.state.ex.AddressModel = this.state.ex.User.Recipient.find(user => { return user.IsDefault });   
        this.state.ex.User.Billing.push(this.state.ex.AddressModel);
        var that = this;
        alertify.confirm('Are you sure!', 'Use Shipping Address', function () {
            $("#loading").show();
            $.ajax({
                url: "/account/profile",
                type: "POST",
                data: that.state.ex.User,
                success: function (response) {
                    $("#loading").hide();
                    if (response.status == "success") {
                        that.handleUpdateBilling(that.state.ex.AddressModel);

                    } else {
                        alertify.alert("Opps!", response.message);
                    }
                },
                error: function (er) {
                    $("#loading").hide();
                    alertify.alert(
                        "Error",
                        "Opps! System has some error. Please try again!"
                    );
                }
            });

        }
            , function () { alertify.error('Cancel') });
       
    }

    /**
     * Xóa data đã được chọn
     */


    handleRemoveAddress(id) {
        var that = this;
        if (id == "" || id == null) {
            alertify.alert("Opps!", "Parameter invalid");
            return -1;
        }
        alertify.confirm('Are you sure!', 'Data not recover', function () {
            $("#loading").show();
            that.state.ex.User.Billing = that.state.ex.User.Billing.filter(e => { return e.Id != id });
            if (that.state.ex.User.Billing.length == 0) {
                that.state.ex.User.Billing = [];            
            }
            $.ajax({
                url: "/account/profile",
                type: "POST",
                data: that.state.ex.User,
                success: function (response) {
                    $("#loading").hide();
                    if (response.status == "success") {
                        alertify.alert("Success", response.message, function () {
                            that.props.history.push("/accountapp/emptypage");
                            that.props.history.replace({
                                pathname: that.props.location.pathname,
                                search: that.props.location.search
                            });
                        });
                    } else {
                        alertify.alert("Opps!", response.message);
                    }
                },
                error: function (er) {
                    $("#loading").hide();
                    alertify.alert(
                        "Error",
                        "Opps! System has some error. Please try again!"
                    );
                }
            });

        }
            , function () { alertify.error('Cancel') });

    }
    componentWillMount() { }
    componentDidMount() {
        //Sửa title trang
        document.title = "Billing Information";
        // this.setState(this.state);
        //Active hight light menu
    }
    render() {
        return (
            <React.Fragment>
                {this.state.ex && this.state.model ? (
                    this.state.model.OrderItems.length > 0 ||
                        this.state.user != null ? (
                            <main id="cart-page">
                                <h1 className="hidden">{this.state.ex.Titlte}</h1>
                                <div id="breadcrumb" className="container">
                                    <ul className="breadcrumb-body my-ul">
                                        <li>
                                            <a href="/" className="text-black">
                                                Home
                                            </a>
                                        </li>
                                        <li className="active text-primary">{this.state.ex.Title}</li>
                                    </ul>
                                </div>
                                <div className="container" id="shipping-address">
                                    <div className="row hidden-xs  clearfix bs-wizard">
                                        <Process Step={this.state.ex.Step} />
                                    </div>
                                    <div className="row clearfix">
                                        <div className="col-md-12 p-2">
                                            <h3>2. Payment address</h3>
                                            {this.state.ex.User && this.state.ex.User.Billing.length == 0 ? (
                                                <React.Fragment>
                                                    <span className="same-as" onClick={e => {
                                                        this.handleUseShippingAddress();
                                                    }}>
                                                        <input
                                                            type="checkbox"
                                                        /> <i className="ecs-placeholder" /> Same as Shipping address
                                                    </span>
                                                </React.Fragment>
                                            ): ""}
                                            <h5 className="visible-md-block visible-lg-block">Select the payment address available below:</h5>
                                        </div>
                                    </div>
                                    <div className="row clearfix">
                                        {this.state.ex.User ? this.state.ex.User.Billing.map(
                                            c => {
                                                return (
                                                    <div className="col-md-6 col-sm-6 col-xs-12">
                                                        <div className={`panel panel-default address-item ${c.IsDefault == true ? 'is-default' : ''}`}>
                                                            <div className="panel-body">
                                                                <p className="name">{c.FirstName} {c.LastName}</p>
                                                                <p className="address" title={`${c.Street1}, ${c.City}, ${c.State.Value} ${c.ZipCode}`}>
                                                                    Address: {`${c.Street1}, ${c.City}, ${c.State.Value} ${c.ZipCode}`}</p>
                                                                <p className="address"> {c.Country.Name} </p>
                                                                <p className="phone">Phone: {c.PhoneNumber}</p>
                                                                <p className="action">
                                                                    <a href="javascript:void(0)" onClick={e => { this.handleUpdateBilling(c) }} class={`btn ${c.IsDefault == true ? 'btn-primary' : 'btn-second'}`}>
                                                                        Select
                                                                        </a>
                                                                    <a href="javascript:void(0)" onClick={e => { this.handleEditAddress(c.Id) }} className="btn btn-default" >Edit</a>
                                                                    {c.IsDefault == false ? (
                                                                        <a href="javascript:void(0)" onClick={e => { this.handleRemoveAddress(c.Id) }} className="btn btn-default" >Remove</a>
                                                                    ): ""}
                                                                </p>
                                                                {c.IsDefault == true ? (
                                                                    <span className="default">Default</span>
                                                                ) : ""}

                                                            </div>
                                                        </div>
                                                    </div>
                                                )
                                            }
                                        ) : ""}
                                    </div>
                                    <div className="row clearfix">
                                        <div className="col-md-6 col-sm-6 col-xs-12">
                                            <p className="other p-2">
                                                You want to pay to another address? <a href="javascript:void(0)" onClick={e => {
                                                    $("#loading").show();
                                                    $("#address-form").is(":visible") ? $("#address-form").hide(0, function () {
                                                        $("#address-form").slideDown("slow", function () {
                                                            $("#loading").hide();
                                                            $("html,body").animate({
                                                                scrollTop: $("#address-form").offset().top - 200
                                                            }, 300)
                                                        })
                                                    }) : $("#address-form").slideDown("slow", function () {
                                                        $("#loading").hide();
                                                        $("html,body").animate({
                                                            scrollTop: $("#address-form").offset().top - 200
                                                        }, 300)
                                                    });
                                                    this.state.ex.IsUpdate = false;
                                                    this.setState(this.state);
                                                }} id="addNewAddress">Add new billing address</a>
                                            </p>
                                        </div>

                                    </div>
                                    <div className="row clearfix">
                                        <div className="col-md-12">
                                            <div id="address-form" className="m-bot-2" style={{ "display": "none" }}>
                                                <div className="panel-body">
                                                    <div className="form-group row">
                                                        <label for="businessname" className="col-lg-4 control-label visible-lg-block">Business Name (<span className="required">*</span>)</label>
                                                        <div className="col-lg-8 input-wrap has-feedback">
                                                            <input
                                                                type="text"
                                                                name="businessname"
                                                                className="form-control"
                                                                value={this.state.ex.AddressModel.BusinessName}
                                                                placeholder="Business Name"
                                                                onChange={e => {
                                                                    this.state.ex.AddressModel.BusinessName = e.target.value;
                                                                    this.setState(this.state);
                                                                }}
                                                            />
                                                        </div>
                                                    </div>
                                                    <div className="form-group row">
                                                        <label for="firstname" className="col-lg-4 control-label visible-lg-block">First Name (<span className="required">*</span>)</label>
                                                        <div className="col-lg-8 input-wrap has-feedback">
                                                            <input
                                                                type="text"
                                                                name="firstname"
                                                                className="form-control"
                                                                value={this.state.ex.AddressModel.FirstName}
                                                                placeholder="First Name"
                                                                onChange={e => {
                                                                    this.state.ex.AddressModel.FirstName = e.target.value;
                                                                    this.setState(this.state);
                                                                }}
                                                            />
                                                        </div>
                                                    </div>
                                                    <div className="form-group row">
                                                        <label for="lastname" className="col-lg-4 control-label visible-lg-block">Last Name (<span className="required">*</span>)</label>
                                                        <div className="col-lg-8 input-wrap has-feedback">
                                                            <input
                                                                type="text"
                                                                name="lastname"
                                                                className="form-control"
                                                                value={this.state.ex.AddressModel.LastName}
                                                                placeholder="Last Name"
                                                                onChange={e => {
                                                                    this.state.ex.AddressModel.LastName = e.target.value;
                                                                    this.setState(this.state);
                                                                }}
                                                            />
                                                        </div>
                                                    </div>
                                                    <div className="form-group row">
                                                        <label for="lastname" className="col-lg-4 control-label visible-lg-block">Phone (<span className="required">*</span>)</label>
                                                        <div className="col-lg-8 input-wrap has-feedback">
                                                            <input
                                                                type="text"
                                                                name="phonenumber"
                                                                className="form-control"
                                                                value={this.state.ex.AddressModel.PhoneNumber}
                                                                placeholder="Phone Number"
                                                                onChange={e => {
                                                                    this.state.ex.AddressModel.PhoneNumber = e.target.value;
                                                                    this.setState(this.state);
                                                                }}
                                                            />
                                                        </div>
                                                    </div>
                                                    <div className="form-group row">
                                                        <label for="street1" className="col-lg-4 control-label visible-lg-block">Address Line 1 (<span className="required">*</span>)</label>
                                                        <div className="col-lg-8 input-wrap has-feedback">
                                                            <textarea
                                                                name="address"
                                                                className="form-control"
                                                                value={this.state.ex.AddressModel.Street1}
                                                                placeholder="Address Line 1"
                                                                onChange={e => {
                                                                    this.state.ex.AddressModel.Street1 = e.target.value;
                                                                    this.setState(this.state);
                                                                }}
                                                            ></textarea>
                                                        </div>
                                                    </div>
                                                    <div className="form-group row">
                                                        <label for="street2" className="col-lg-4 control-label visible-lg-block">Address Line 2 </label>
                                                        <div className="col-lg-8 input-wrap has-feedback">
                                                            <textarea
                                                                name="address"
                                                                className="form-control"
                                                                value={this.state.ex.AddressModel.Street2}
                                                                placeholder="Address Line 2"
                                                                onChange={e => {
                                                                    this.state.ex.AddressModel.Street2 = e.target.value;
                                                                    this.setState(this.state);
                                                                }}
                                                            ></textarea>
                                                        </div>
                                                    </div>
                                               
                                                    <div className="form-group row">
                                                        <label for="telephone" className="col-lg-4 control-label visible-lg-block">Country (<span className="required">*</span>)</label>
                                                        <div className="col-lg-8 input-wrap has-feedback">
                                                            <select className="form-control"
                                                                onChange={e => {
                                                                    var that = this;
                                                                    if (e.target.value == "" || e.target.value == null) {
                                                                        that.state.ex.AddressModel.Country.Name = null;
                                                                        that.state.ex.AddressModel.Country.Value = null;
                                                                        that.state.ex.AddressModel.State.Name = null;
                                                                        that.state.ex.AddressModel.State.Value = null;
                                                                        that.state.ex.StateList = [];
                                                                        that.setState(that.state);
                                                                        return -1;
                                                                    };
                                                                    $("#loading").show();
                                                                    var country = this.state.ex.CountryList.find(country => { return country.Code == e.target.value });
                                                                    this.state.ex.AddressModel.Country.Name = country.Name;
                                                                    this.state.ex.AddressModel.Country.Value = country.Code;
                                                                    if (this.state.ex.AddressModel.Country == null || this.state.ex.AddressModel.Country == "") {
                                                                        setTimeout(function () {
                                                                            that.state.ex.AddressModel.State.Name = null;
                                                                            that.state.ex.AddressModel.State.Value = null;
                                                                            that.state.ex.StateList = [];
                                                                            $("#loading").hide();
                                                                        }, 500)
                                                                        return -1;
                                                                    }
                                                                    this.state.ex.StateList = this.state.ex.CountryList.find(d => { return d.Code == this.state.ex.AddressModel.Country.Value }).StateList;
                                                                    setTimeout(function () {
                                                                        that.state.ex.AddressModel.State.Name = null;
                                                                        that.state.ex.AddressModel.State.Value = null;
                                                                        $("#loading").hide();
                                                                    }, 500)
                                                                    this.setState(this.state);
                                                                }}>
                                                                <option value="" readOnly>-- Select Country --</option>
                                                                {this.state.ex.CountryList ? this.state.ex.CountryList.map(c => {
                                                                    return <option value={c.Code} selected={this.state.ex.AddressModel.Country.Value == c.Code ? "selected" : ""}>{c.Name}</option>
                                                                }) : <option value="">-- Select Country --</option>}

                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div className="form-group row">
                                                        <label for="state" className="col-lg-4 control-label visible-lg-block">State (<span className="required">*</span>)</label>
                                                        <div className="col-lg-8 input-wrap has-feedback">
                                                            <select className="form-control"
                                                                onChange={e => {
                                                                    var that = this;
                                                                    var state = this.state.ex.StateList.find(state => { return state.CodeProvince == e.target.value });
                                                                    that.state.ex.AddressModel.State.Name = state.Name;
                                                                    that.state.ex.AddressModel.State.Value = state.CodeProvince;
                                                                    this.setState(this.state);
                                                                }}>
                                                                <option value="" readOnly>-- Select State --</option>
                                                                {this.state.ex.StateList ? this.state.ex.StateList.map(c => {
                                                                    return <option value={c.CodeProvince} selected={this.state.ex.AddressModel.State.Value == c.CodeProvince ? "selected" : ""}>{c.Name}</option>
                                                                }) : <option value="">-- Select State --</option>}

                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div className="form-group row">
                                                        <label for="city" className="col-lg-4 control-label visible-lg-block">City (<span className="required">*</span>)</label>
                                                        <div className="col-lg-8 input-wrap has-feedback">
                                                            <input
                                                                type="text"
                                                                name="city"
                                                                value={this.state.ex.AddressModel.City}
                                                                className="form-control"
                                                                placeholder="City"
                                                                onChange={e => {
                                                                    this.state.ex.AddressModel.City = e.target.value;
                                                                    this.setState(this.state);
                                                                }}
                                                            />
                                                        </div>
                                                    </div>
                                                    <div className="form-group row">
                                                        <label for="zipcode" className="col-lg-4 control-label visible-lg-block">Zip Code (<span className="required">*</span>)</label>
                                                        <div className="col-lg-8 input-wrap has-feedback">
                                                            <input
                                                                type="text"
                                                                name="zipcode"
                                                                className="form-control"
                                                                value={this.state.ex.AddressModel.ZipCode}
                                                                placeholder="Zip Code"
                                                                onChange={e => {
                                                                    this.state.ex.AddressModel.ZipCode = e.target.value;
                                                                    this.setState(this.state);
                                                                }}
                                                            />
                                                        </div>
                                                    </div>
                                               
                                                    <div className="form-group row">
                                                        <div className="col-lg-8 col-md-offset-4">
                                                            <label className="checkbox"> Make Default
                                                                <input
                                                                    type="checkbox"
                                                                    checked={this.state.ex.AddressModel.IsDefault}
                                                                    value={this.state.ex.AddressModel.IsDefault}
                                                                    onClick={e => {
                                                                        this.state.ex.AddressModel.IsDefault = e.target.checked;
                                                                        this.setState(this.state);
                                                                    }} />
                                                                <span className="checkmark"></span>
                                                            </label>
                                                        </div>

                                                    </div>
                                                    <div className="form-group row btn-container">
                                                        <div className="col-lg-8 col-md-offset-4">
                                                            <a href="javascript:void(0)" onClick={e => {
                                                                if (this.state.ex.AddressModel != null) {                                                               
                                                                    this.state.ex.AddressModel = new AddressModel();
                                                                }
                                                                $("#loading").show();
                                                                $("#address-form").is(":visible") ? $("#address-form").hide(0, function () {
                                                                    $("#address-form").slideUp("slow", function () {
                                                                        $("#loading").hide();
                                                                        $("html,body").animate({
                                                                            scrollTop: $("#address-form").offset().top - 200
                                                                        }, 300)
                                                                    })
                                                                }) : $("#address-form").slideUp("slow", function () {
                                                                    $("#loading").hide();
                                                                    $("html,body").animate({
                                                                        scrollTop: $("#address-form").offset().top - 200
                                                                    }, 300)
                                                                });
                                                                this.setState(this.state);
                                                            }} className="btn btn-default btn-outline">Cancel</a>
                                                            <a href="javascript:void(0)" onClick={e => { this.submitprofile() }} className="btn btn-primary">Save</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </main>
                        ) : (
                            ""
                        )
                ) : ""}
            </React.Fragment>
        );
    }
}

export default Shipping;
