import React, { Component } from "react";
import { Link } from "react-router-dom";
import Process from "./process";
import OrderCmsModel from "../models/order-cmsmodel";
import moment from "moment";
const apiurl = "/ordernacgroup";
import AppContext from "../../../../NacGroup/CmsApp/components/app-context";
class Payment extends Component {
    constructor(props,context) {
        super(props,context);
        var that = this;
        that.state = {
            model: new OrderCmsModel(),
            ex: {
                Title: "Thông tin thanh toán",
                Step: 2,
                AddressList: [],
                CourierId: null,
                YearList: [],
                MonthList: [],
                IsPaid: false,
                Payment: {
                    CartNumber: null,
                    ExpirationMonth: "01",
                    ExpirationYear: moment().format("YY"),
                    CcvCode: null,
                    CartName: null
                },
            }
        };
        // Get all Month
        var i;
        for (i = 0; i < 12; i++) {
            that.state.ex.MonthList.push({
                value: ("0" + (i + 1)).slice(-2),
                label: moment.months(i)
            });
        }
        // Get all Year
        for (i = moment().format("YY"); i <= 70; i++) {
            that.state.ex.YearList.push({
                value: "" + i + "",
                label: "20" + i.toString()
            });
        }
        $("#loading").show();
        that.setState(that.state);
        $.get(apiurl + "/getcart", function (response) {
            that.state.model = response;
            that.setState(that.state);    
            $("#loading").hide();
            if (that.state.model.OrderItems.length <= 0) {
                window.location.replace("/");
            }
        });
        //$.get("/account/getprofile",
        //    function (response) {
        //        that.state.ex.AddressList = response.data ? response.data.AddressList : [];
        //        that.setState(that.state);
        //    });
    }
    submitForm() {
        var that = this;
        if (that.state.ex.IsPaid == true) {
            return -1;
        }
        this.state.ex.IsPaid = true;
        this.setState(this.state);
        $.ajax({
            url: apiurl + "/confirmcheckout",
            type: "POST",
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify({
                checkout: that.state.ex.Payment
            }),
            beforeSend: xhr => {
                $("#loading").show();
            },
            success: response => {
                alertify.closeAll();
                $("#loading").hide();
                if (response.status == "success") {
                    if (response.data != null) {
                        location.href = "/checkout/payment/success?code=" + response.data.OrderNumber
                    }
                    that.setState(that.state);
                } else {
                    alertify.error(response.message);
                }
            },
            error: e => {
                $("#loading").show();
            }
        });
    }
    /**
     * Xóa data đã được chọn
     */
    componentWillMount() { }
    componentDidMount() {
        //Sửa title trang
        document.title = "Thông tin đơn hàng";
        // this.setState(this.state);
        //Active hight light menu
    }
    render() {
        const convertMoney = (price) => {
            return (
                this.context.CurrencyType === 0
                    ? (
                        price
                            ? price.formatMoney(
                                2,
                                ".",
                                ",",
                                "$"
                            )
                            : "0"
                    )
                    : (
                        price
                            ? price.formatMoney(
                                0,
                                ",",
                                "."
                            )
                            : "0"
                    )
            );
        }
        return (
            <React.Fragment>
                {this.state && this.state.model ? (
                    this.state.model.OrderItems.length > 0 ||
                        this.state.user != null ? (
                            <main id="cart-page">
                                <h1 className="hidden">{this.state.ex.Titlte}</h1>
                               
                                <div className="container" id="payment">
                                    <div className="row hidden-xs clearfix bs-wizard">
                                        <Process Step={this.state.ex.Step} />
                                    </div>
                                    <div className="row clearfix">
                                        <div className="col-md-12 p-2">
                                            <h3>3. Mua hàng</h3>
                                        </div>
                                    </div>
                                   
                                    <div className="row clearfix">
                                       
                                        <div className="col-md-12">
                                            <div className="panel panel-default cart">
                                                <div className="panel-header flex-center">
                                                    <div className="v-title flex-left">
                                                        Địa chỉ giao hàng
                                                    </div>
                                                    <div className="v-button flex-right">
                                                        <Link to="/checkout/address" className="btn btn-default">Chỉnh sửa</Link>
                                                    </div>
                                                </div>
                                                <div className="panel-body">
                                                    <h6>{this.state.model.Recipient.FullName}</h6>
                                                    <p className="end">
                                                        {this.state.model.Street}, {this.state.model.Ward}, {this.state.model.District}, {this.state.model.City}<br />
                                                        VN <br />
                                                        Phone: {this.state.model.CustomerPhone} <br />
                                                    </p>
                                                </div>
                                            </div>
                                       
                                            <div className="panel panel-default cart">
                                                <div className="panel-header flex-center">
                                                    <div className="v-title flex-left">
                                                        Giỏ hàng ({this.state.model.OrderItems.length} sản phẩm)
                                                    </div>
                                                    <div className="v-button flex-right">
                                                        <Link to="/checkout/cart" className="btn btn-default">Chỉnh sửa</Link>
                                                    </div>
                                                </div>
                                                <div className="panel-body">
                                                    <div className="product-list">
                                                        {this.state.model.OrderItems ? this.state.model.OrderItems.map(c => {
                                                            return (
                                                                <div className="item">
                                                                    <p className="title">
                                                                        <strong>{c.Quantity} x </strong>
                                                                        <a href="javascript:void(0)" target="_blank">{c.Name}</a>
                                                                       
                                                                    </p>
                                                                    <p className="price text-right">
                                                                        <span>{convertMoney(c.Price)}</span>
                                                                    </p>
                                                                </div>
                                                            )
                                                        }) : ""}

                                                    </div>
                                                    <p className="list-info-price">
                                                        <b className="text-left text-normal">Tạm tính</b>
                                                        <span className="text-right">{convertMoney(this.state.model.SubTotalAmount)}</span>
                                                    </p>
                                                    <p className="list-info-price">
                                                        <b className="text-left text-normal">Giảm giá</b>
                                                        <span className="text-right">{convertMoney(this.state.model.DiscountAmount)}</span>
                                                    </p>

                                                    <p className="list-info-price">
                                                        <b className="text-left text-normal">Phí vận chuyển</b>
                                                        <span className="text-right">{convertMoney(this.state.model.ShippingAmount)}</span>
                                                    </p>

                                                    <p className="list-info-price">
                                                        <b className="text-left text-normal">Thuế</b>
                                                        <span className="text-right">{convertMoney(this.state.model.TotalTax)}</span>
                                                    </p>

                                                    <p className="total2">
                                                        <span className="text-bold">Thành tiền:</span>
                                                        <span className="text-right price">{convertMoney(this.state.model.TotalAmount)}</span>
                                                    </p>
                                                    <p className="text-right">
                                                       
                                                    </p>
                                                </div>

                                            </div>
                                            <div className="popover-content hidden">
                                                <p className="ship"><span className="ecs-shopping-cart-add-button"></span> Đơn hàng của bạn sẽ được miễn phí vận chuyển.</p>
                                            </div>
                                            <div className="button-container">
                                                <a href="javascript:void(0);" onClick={e => {
                                                    this.submitForm();
                                                }} className="btn btn-custom">Đặt hàng</a>
                                                <p>(Vui lòng kiểm tra lại đơn hàng trước khi đặt thanh toán)</p>
                                            </div>
                                        </div>                 
                                    </div>
                                    
                                </div>
                            </main>
                        ) : (
                            ""
                        )
                ) : ""}
            </React.Fragment>
        );
    }
}
Payment.contextType = AppContext;
export default Payment;
