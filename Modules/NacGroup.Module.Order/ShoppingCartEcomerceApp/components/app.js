import React, { Component } from "react";
import { BrowserRouter, Redirect, Route } from "react-router-dom";

import ShoppingCart from "./index";
import Shipping from "./shipping";
import Payment from "./payment";
import Empty from "./empty";
import Success from "./success";
import AppProvider from "../../../../NacGroup/CmsApp/components/app-provider";
class App extends Component {
    constructor() {
        super();
    }
    render() {
        return (
            <AppProvider>
                <BrowserRouter>
                    <React.Fragment>
                        <Route exact path="/checkout/cart" component={ShoppingCart} />
                        <Route exact path="/checkout/address" component={Shipping} />
                        <Route exact path="/checkout/payment" component={Payment} />
                        <Route exact path="/checkout/empty" component={Empty} />
                        <Route exact path="/checkout/payment/success" component={Success} />
                    </React.Fragment>
                </BrowserRouter>
            </AppProvider>
        );
    }
}

export default App;