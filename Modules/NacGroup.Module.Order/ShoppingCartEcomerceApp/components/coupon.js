﻿import React, { Component } from "react";
import "../helper/string-helper";
import { DelayInput } from "react-delay-input";
const apiurl = "/ordernacgroup";
// 

class Coupon extends Component {
  constructor(props) {
      super(props);
      var that = this;
      that.state = {
          ex: {
              couponCode: null
          }
      }
  }

    ApplyCoupon() {
        var that = this;
        var couponCode = that.state.ex.couponcode;
        if (couponCode == null || couponCode == "") {
            alertify.error("Mã giảm giá không được rỗng");
            return -1;
        }
        $.ajax({
            url: apiurl + "/applycoupon",
            type: "POST",
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify(couponCode),
            beforeSend: xhr => {
                $("#loading").show();
            },
            success: response => {
                $("#loading").hide();
                if (response.status == "success") {
                    that.props.onChangeModel(response.data);
                    alertify.success("Áp dụng thành công mã giảm giá");
                    that.setState(that.state);
                } else {
                    alertify.error(response.message);
                }
            },
            error: e => {
                $("#loading").show();
            }
        });
    }
    RemoveCoupon() {
        var that = this;
        $.ajax({
            url: apiurl + "/clearcoupon",
            type: "POST",
            dataType: "json",
            contentType: "application/json",
            beforeSend: xhr => {
                $("#loading").show();
            },
            success: response => {
                $("#loading").hide();
                if (response.status == "success") {
                    that.props.onChangeModel(response.data);
                    that.state.ex.couponCode = "";
                    alertify.success("Xóa thành công mã giảm giá");
                    that.setState(that.state);
                } else {
                    alert(response.message);
                }
            },
            error: e => {
                $("#loading").show();
            }
        });
    }
  componentWillMount() {}

  componentDidMount() {}

  componentWillUnmount() {
    $("#scriptloading").html("");
  }
  render() {
    return (
      <React.Fragment>
            <div className="section-body-plus">
                <h4 className="h5">Mã giảm giá / Quà tặng</h4>
                <div className="item flex-center clearfix">
                    {this.props.Model.Coupon != null ? (
                        <React.Fragment>
                            <DelayInput
                                type="text"
                                className="form-control"
                                placeholder="Mã khuyến mãi"
                                value={this.props.Model.Coupon.CouponCode}
                                readOnly
                            />
                            <a
                                href="javascript:;"
                                className="btn btn-primary"
                                onClick={e => this.RemoveCoupon()}
                            >
                                Hủy bỏ
                        </a>
                        </React.Fragment>
                    ) : (
                            <React.Fragment>
                                <DelayInput
                                    type="text"
                                    className="form-control"
                                    placeholder="Mã khuyến mãi"
                                    value={this.state.ex.couponCode}
                                    onChange={e => {
                                        this.state.ex.couponCode = e.target.value.toUpperCase();
                                        this.setState(this.state);
                                    }}
                                />
                                <a
                                    href="javascript:;"
                                    className="btn btn-primary"
                                    onClick={e => this.ApplyCoupon()}
                                >
                                    Áp dụng
                        </a>
                            </React.Fragment>
                        )}
                </div>
        </div>
      </React.Fragment>
    );
  }
}

export default Coupon;
