﻿import React, { Component } from "react";
import { Link } from "react-router-dom";
class Process extends Component {
  constructor(props) {
      super(props);
      var that = this;
      that.state = {
          ex: {
              Name: {
                  Login: "Đăng nhập",
                  Shipping: "Địa chỉ",
                  Payment: "Thanh toán & mua hàng"
              }
          }
      }
      that.setState(that.state);
  }
  componentWillMount() {
   
  }
  componentDidMount() {
   
  }

  componentWillUnmount() {
    $("#scriptloading").html("");
  
  }
  render() {
    return (
        <React.Fragment>
           
            {this.props.Step === 0 ? (
                <React.Fragment>             
                    <div className="col-lg-4 col-md-4 col-xs-4 col-sm-4 bs-wizard-step active">
                        <div className="text-center bs-wizard-stepnum">
                            <span>{this.state.ex.Name.Login}</span>
                        </div>
                        <div className="progress">
                            <div className="progress-bar">1</div>
                        </div>
                        <span className="bs-wizard-dot"><Link to="/gio-hang/dia-chi">1</Link></span>
                    </div>
                   
                    <div className="col-lg-4 col-md-4 col-xs-4 col-sm-4 bs-wizard-step disabled">
                        <div className="text-center bs-wizard-stepnum">
                            <span>{this.state.ex.Name.Shipping}</span>
                        </div>
                        <div className="progress">
                            <div className="progress-bar"></div>
                        </div>
                        <span className="bs-wizard-dot">2</span>
                    </div>
                    <div className="col-lg-4 col-md-4 col-xs-4 col-sm-4 bs-wizard-step disabled">
                        <div className="text-center bs-wizard-stepnum">
                            <span>{this.state.ex.Name.Payment}</span>
                        </div>
                        <div className="progress">
                            <div className="progress-bar"></div>
                        </div>
                        <span className="bs-wizard-dot">3</span>
                    </div>
                </React.Fragment>
            ) : ""}
            {this.props.Step === 1 ? (
                <React.Fragment>
                    <div className="col-lg-4 col-md-4 col-xs-4 col-sm-4 bs-wizard-step complete">
                        <div className="text-center bs-wizard-stepnum">
                            <span>{this.state.ex.Name.Login}</span>
                        </div>
                        <div className="progress">
                            <div className="progress-bar"></div>
                        </div>
                        <span className="bs-wizard-dot">1</span>
                    </div>
                    <div className="col-lg-4 col-md-4 col-xs-4 col-sm-4 bs-wizard-step active">
                        <div className="text-center bs-wizard-stepnum">
                            <span>{this.state.ex.Name.Shipping}</span>
                        </div>
                        <div className="progress">
                            <div className="progress-bar"></div>
                        </div>
                        <span className="bs-wizard-dot"><Link to="/gio-hang/dia-chi">2</Link></span>
                    </div>
                    <div className="col-lg-4 col-md-4 col-xs-4 col-sm-4 bs-wizard-step disabled">
                        <div className="text-center bs-wizard-stepnum">
                            <span>{this.state.ex.Name.Payment}</span>
                        </div>
                        <div className="progress">
                            <div className="progress-bar"></div>
                        </div>
                        <span className="bs-wizard-dot">3</span>
                    </div>
                </React.Fragment>
            ) : ""}
            {this.props.Step === 2 ? (
                <React.Fragment>
                    <div className="col-lg-4 col-md-4 col-xs-4 col-sm-4 bs-wizard-step complete">
                        <div className="text-center bs-wizard-stepnum">
                            <span>{this.state.ex.Name.Login}</span>
                        </div>
                        <div className="progress">
                            <div className="progress-bar"></div>
                        </div>
                        <span className="bs-wizard-dot">1</span>
                    </div>     
                    <div className="col-lg-4 col-md-4 col-xs-4 col-sm-4 bs-wizard-step complete">
                        <div className="text-center bs-wizard-stepnum">
                            <span>{this.state.ex.Name.Shipping}</span>
                        </div>
                        <div className="progress">
                            <div className="progress-bar"></div>
                        </div>
                        <span className="bs-wizard-dot"><Link to="/gio-hang/dia-chi">2</Link></span>
                    </div>     
                    <div className="col-lg-4 col-md-4 col-xs-4 col-sm-4 bs-wizard-step active">
                        <div className="text-center bs-wizard-stepnum">
                            <span>{this.state.ex.Name.Payment}</span>
                        </div>
                        <div className="progress">
                            <div className="progress-bar"></div>
                        </div>
                        <span className="bs-wizard-dot">3</span>
                    </div>
                </React.Fragment>
            ) : ""}
          
      </React.Fragment>
    );
  }
}

export default Process;
