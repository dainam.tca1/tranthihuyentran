import React, { Component } from "react";

import Amount from "./amount";
import CartItem from "./cart-item";
import Coupon from "./coupon";
import Process from "./process";
import moment from "moment";
import OrderCmsModel from "../models/order-cmsmodel";
import { Link } from "react-router-dom"

const apiurl = "/ordernacgroup";

class ShoppingCart extends Component {
    constructor() {
        super();
        var that = this;
        that.state = {
            model: new OrderCmsModel()
        };
        $("#loading").show();
        that.setState(that.state);
        $.get(apiurl + "/getcart", function (response) {
            that.state.model = response;
            $("#loading").hide();
            that.setState(that.state);
            if (that.state.model.OrderItems.length <= 0) {
                window.location.replace("/");
            }
        });
    }

    handleChangeModel(e) {
        this.state.model = e;
        this.setState(this.state);
    }


    /**
     * Xóa data đã được chọn
     */
    componentWillMount() { }
    componentDidMount() {
        //Sửa title trang
        //  document.title = this.state.ex.Title;
        // this.setState(this.state);
        //Active hight light menu
    }
    render() {
        return (
            <React.Fragment>
                {this.state && this.state.model ? (
                    this.state.model.OrderItems.length > 0 ||
                        this.state.user != null ? (
                            <React.Fragment>
                                <h1 className="hidden">Shopping Cart</h1>
                                <div className="container" id="cart-bag">
                                    <div className="row clearfix bs-wizard">
                                        <Process />
                                    </div>
                                    <div className="row clearfix">
                                        <section id="cart" className="col col-md-9 col-sm-12 p-top-0 p-bot-4">
                                            <CartItem
                                                OrderItems={this.state.model.OrderItems}
                                                onChangeModel={e => this.handleChangeModel(e)}
                                            />

                                        </section>
                                        <section className="col col-md-3 col-sm-12 p-top-0 p-bot-4" id="cartbar">
                                            <h2 className="h3 section-header m-bot-2 text-uppercase">

                                            </h2>
                                            <Amount
                                                TotalAmount={this.state.model.TotalAmount}
                                                SubTotalAmount={this.state.model.SubTotalAmount}
                                                DiscountAmount={this.state.model.DiscountAmount}
                                               
                                            />

                                            <div className="section-footer-plus">
                                                <a
                                                    href="/checkout/address"
                                                    className="btn-loading btn btn-primary btn-big"

                                                >
                                                    Tiến hành đặt hàng
                                                    </a>
                                            </div>
                                            <Coupon
                                                onChangeModel={e => this.handleChangeModel(e)}
                                                Model={this.state.model}
                                            />
                                        </section>
                                    </div>
                                </div>
                            </React.Fragment>
                        ) : (
                            ""
                        )
                ) : ""}
            </React.Fragment>
        );
    }
}

export default ShoppingCart;
