﻿import React, { Component } from "react";
import { DelayInput } from "react-delay-input";
import {
    GLOBAL_ERROR_MESSAGE,
    ERROR,
    SUCCESS,
    NOTI,
    DATA_NOT_RECOVER
} from "../constants/message";
import AppContext from "../../../../NacGroup/CmsApp/components/app-context";
const apiurl = "/ordernacgroup";
class CartItem extends Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            ex: {
                Type: null,
                HtmlPrice: null
            }
        };

        this.setState(this.state);
    }
    handleDecrease(id, sku) {
        var qty = $("#quantity-" + sku).val();
        var quantity = parseInt(qty) - 1;
        if (quantity == 0 || isNaN(quantity) || quantity == "") {
            alertify.error("Số lượng phải lớn hơn không");
            return false;
        }
        var that = this;
        $.ajax({
            url: apiurl + "/updatequantity",
            type: "POST",
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify({
                id: id,
                quantity: quantity,
            }),
            beforeSend: xhr => {
                $("#loading").show();
            },
            success: response => {
                $("#loading").hide();
                if (response.status == "success") {
                    alertify.success("Cập nhật số lượng thành công");
                    $("#quantity-" + sku).val(quantity);
                    that.props.onChangeModel(response.data);
                    // that.setState(that.state);
                } else {
                    alertify.error(response.message);
                }
            },
            error: e => {
                $("#loading").show();
            }
        });
    }
    handleIncrease(id, sku) {
        var qty = $("#quantity-" + sku).val();

        var quantity = parseInt(qty) + 1;
        if (quantity == 0 || isNaN(qty) || quantity == "") {
            alertify.error("Số lượng phải lớn hơn không");
            return false;
        }

        var that = this;
        $.ajax({
            url: apiurl + "/updatequantity",
            type: "POST",
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify({
                id: id,
                quantity: quantity,
            }),
            beforeSend: xhr => {
                $("#loading").show();
            },
            success: response => {
                $("#loading").hide();
                if (response.status == "success") {
                    alertify.success("Cập nhật số lượng thành công");
                    $("#quantity-" + sku).val(quantity);
                    that.props.onChangeModel(response.data);
                    // that.setState(that.state);
                } else {
                    alertify.error(response.message);
                }
            },
            error: e => {
                $("#loading").show();
            }
        });
    }
    handleChangeQuantity(id, qty) {
        if (qty == 0 || isNaN(qty) || qty == "") {
            alertify.error("Số lượng phải lớn hơn không");
            return false;
        }
        var that = this;
        $.ajax({
            url: apiurl + "/updatequantity",
            type: "POST",
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify({
                id: id,
                quantity: qty,
            }),
            beforeSend: xhr => {
                $("#loading").show();
            },
            success: response => {
                $("#loading").hide();
                if (response.status == "success") {
                    alertify.success("Cập nhật thành công");
                    that.props.onChangeModel(response.data);
                    
                } else {
                    alertify.error(response.message);
                }
            },
            error: e => {
                $("#loading").show();
            }
        });
    }

    handleRemoveItem(id) {
        var that = this;
        alertify.confirm(
            "Thông báo",
            "Dữ liệu không thể phục hồi",
            function () {
                $.ajax({
                    url: apiurl + "/removeitem",
                    type: "POST",
                    dataType: "json",
                    contentType: "application/json",
                    data: JSON.stringify(id),
                    beforeSend: xhr => {
                        $("#loading").show();
                    },
                    success: response => {
                        $("#loading").hide();
                        if (response.status == "success") {
                            that.props.onChangeModel(response.data);
                            if (response.data.OrderItems.length == 0) {
                                window.location.replace("/");
                            }
                            alertify.success("Cập nhật thành công");
                            that.setState(that.state);
                        } else {
                            alertify.error(response.message);
                        }
                    },
                    error: e => {
                        $("#loading").show();
                    }
                });
                return true;
            },
            function () { }
        );
    }
    componentWillMount() { }
    componentDidMount() { }


    componentWillUnmount() {
        $("#scriptloading").html("");
    }
    render() {
        var that = this;
        const discount = (listedPrice, price) => {
            var html = "";
            if (price < listedPrice) {
                html += "-" + ((listedPrice - price) / listedPrice * 100).toFixed(0) + "%";
            };
            return html;
        }
        const price = (listedPrice, price) => {

            if (price < listedPrice) {
                this.state.ex.HtmlPrice =
                    <React.Fragment>
                        <div className="new-price h5">
                            {this.context.CurrencyType === 0 ? (
                                price ? price.formatMoney(
                                    2,
                                    ".",
                                    ",",
                                    "$"
                                ) : "0"
                            ) : (
                                    price ? price.formatMoney(
                                        0,
                                        ",",
                                        "."
                                    ) : "0"
                                )}
                        </div>
                        <div className="old-price" >
                            | {this.context.CurrencyType === 0 ? (
                                listedPrice ? listedPrice.formatMoney(
                                    2,
                                    ".",
                                    ",",
                                    "$"
                                ) : "0"
                            ) : (
                                    listedPrice ? listedPrice.formatMoney(
                                        0,
                                        ",",
                                        "."
                                    ) : "0"
                                )}
                            <span className="h6">
                                {discount(listedPrice, price)}
                            </span>
                        </div>
                    </React.Fragment>

            } else {
                this.state.ex.HtmlPrice =
                    <React.Fragment>
                    <div className="new-price h5">
                        {this.context.CurrencyType === 0 ? (
                            price ? price.formatMoney(
                                2,
                                ".",
                                ",",
                                "$"
                            ) : "0"
                        ) : (
                                price ? price.formatMoney(
                                    0,
                                    ",",
                                    "."
                                ) : "0"
                            )}
                    </div>
                </React.Fragment>
            }
            return that.state.ex.HtmlPrice;
        }
        return (
            <React.Fragment>
                <h3 className="h4 section-header m-bot-2 text-black text-uppercase">
                    GIỎ HÀNG <span>({this.props.OrderItems ? this.props.OrderItems.length : 0} sản phẩm)</span>
                </h3>
                <div className="section-body">
                    {this.props.OrderItems
                        ? this.props.OrderItems.map(sku => {
                            return (
                                <div className="item flex-container">
                                    <div className="img-container">
                                        <img src={sku.Avatar} alt="" className="" />
                                    </div>
                                    <div className="text-container">
                                        <div className="v-height">
                                            <h3 className="h5">
                                                <a href={`/productinfo/${sku.SkuCode}`} target="_blank">
                                                    {sku.Name}
                                                </a>
                                            </h3>
                                            <div className="btn-container">
                                                <a
                                                    href="javascript:;"
                                                    onClick={e => this.handleRemoveItem(sku.Id)}
                                                    style={{ "color": "#ee2347", fontWeight: 500 }}
                                                >
                                                    Xóa
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="price text-black">
                                        {price(sku.ListedPrice, sku.SkuPrice)}
                                    </div>
                                    <div className="quantity-container">
                                        <div className="quantity-control flex-container flex-center">
                                            <a href="javascript:;" className="decrease" onClick={e => this.handleDecrease(sku.Id, sku.SkuCode)}><span className="nac-minus"></span></a>
                                            <DelayInput
                                                type="number"
                                                name="quantity"
                                                className="form-control quantitydetail"
                                                min="1"
                                                max="99"
                                                id={`quantity-${sku.SkuCode}`}
                                                value={sku.Quantity}
                                                onChange={e => this.handleChangeQuantity(sku.Id, e.target.value)}
                                            />
                                            <a href="javascript:;" className="increase" onClick={e => this.handleIncrease(sku.Id, sku.SkuCode)}><span className="nac-plus"></span></a>
                                        </div>

                                    </div>
                                </div>
                            );
                        })
                        : ""}
                </div>
            </React.Fragment>
        );
    }
}
CartItem.contextType = AppContext;
export default CartItem;
