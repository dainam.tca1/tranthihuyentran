﻿import React, { Component } from "react";
import { DelayInput } from "react-delay-input";
import moment from "moment";
const apiurl = "/ordernacgroup";
class Profile extends Component {
  constructor(props) {
    super(props);
    var that = this;
    that.state = {
      Month: [],
      Year: [],
      ex: {
        Toggle: true,
        couponcode: null,
        Gender: {
          Male: 0,
          Female: 1
        },
        DistrictList: [],
        DistrictListDefault: [],
        CityList: [],
        CityListDefault: [],
        WardListDefault: [],
        WardList: []
      }
    };
    $.get(apiurl + "/getcity", function(response) {
      that.state.ex.CityListDefault = response.data;
      that.state.ex.CityList = response.data
        ? response.data.map(e => {
            return { label: e.Name, value: e.Name };
          })
        : [];
      that.setState(that.state);
    });
    $.get(apiurl + "/getdistrict", function(response) {
      that.state.ex.DistrictListDefault = response.data;
      if (that.props.Model.District != null) {
        that.state.ex.DistrictList = that.state.ex.DistrictListDefault.filter(
          e => {
            return e.City.Name == that.props.Model.City;
          }
        );
        that.state.ex.DistrictList = that.state.ex.DistrictList.map(e => {
          return { label: e.Name, value: e.Name };
        });
      }
      that.setState(that.state);
    });
    $.get(apiurl + "/getward", function(response) {
      that.state.ex.WardListDefault = response.data;
      if (that.props.Model.Ward != null) {
        console.log("123");
        that.state.ex.WardList = that.state.ex.WardListDefault.filter(e => {
          return e.District.Name == that.props.Model.District;
        });
        that.state.ex.WardList = that.state.ex.WardList.map(e => {
          return { label: e.Name, value: e.Name };
        });
      }
      that.setState(that.state);
    });
    // Get all Month
    //var i;
    //for (i = 0; i < 12; i++) {
    //    that.state.Month.push({
    //        value: ("0" + (i + 1)).slice(-2),
    //        label: moment.months(i)
    //    });
    //}
    //// Get all Year
    //for (i = 19; i <= 70; i++) {
    //    that.state.Year.push({
    //        value: "" + i + "",
    //        label: "20" + i
    //    });
    //}
    this.handleClickNewInfo = this.handleClickNewInfo.bind(this);
    that.setState(that.state);
  }
  ApplyCoupon() {
    var that = this;
    var couponcode = that.state.ex.couponcode;
    if (couponcode == null || couponcode == "") {
      alertify.error("Mã giảm giá đang trống");
      return -1;
    }
    $.ajax({
      url: apiurl + "/applycoupon",
      type: "POST",
      dataType: "json",
      contentType: "application/json",
      data: JSON.stringify(couponcode),
      beforeSend: xhr => {
        $("#loading").show();
      },
      success: response => {
        $("#loading").hide();
        if (response.status == "success") {
          that.props.onChangeModel(response.data);
          alertify.success("Áp dụng mã giảm giá thành công");
          that.setState(that.state);
        } else {
          alertify.error(response.message);
        }
      },
      error: e => {
        $("#loading").show();
      }
    });
  }
  RemoveCoupon() {
    var that = this;
    $.ajax({
      url: apiurl + "/clearcoupon",
      type: "POST",
      dataType: "json",
      contentType: "application/json",
      beforeSend: xhr => {
        $("#loading").show();
      },
      success: response => {
        $("#loading").hide();
        if (response.status == "success") {
          that.props.onChangeModel(response.data);
          that.state.ex.couponcode = "";
          alertify.success("Gỡ bỏ mã giảm giá thành công");
          that.setState(that.state);
        } else {
          alert(response.message);
        }
      },
      error: e => {
        $("#loading").show();
      }
    });
  }

  handleClickNewInfo() {
    var that = this;
    that.state.ex.Toggle = !that.state.ex.Toggle;
    if (that.state.ex.Toggle == true) {
      if (that.props.User.OrderWebnailMoney.AddressItem != null) {
        $(
          "#ProfileDetails_" +
            that.props.User.OrderWebnailMoney.AddressItem.Id +
            "_Id"
        ).prop("checked", false);
        $.ajax({
          url: apiurl + "/clearinformation",
          type: "POST",
          dataType: "json",
          contentType: "application/json",
          data: JSON.stringify({ model: that.props.User }),
          beforeSend: xhr => {
            $("#loading").show();
          },
          success: response => {
            $("#loading").hide();
            if (response.status == "success") {
              //  that.state.model = response.data;
              that.props.onChangeModel(response.data);
              that.setState(that.state);
              toastr["success"](response.message, SUCCESS);
            } else {
              toastr["error"](response.message, ERROR);
            }
            toastr.clear();
          },
          error: e => {
            $("#loading").show();
          }
        });
        that.setState(that.state);
      }
    }
    that.setState(that.state);
  }

  handleAddInfoAddress(id) {
    var that = this;
    var item = this.props.AddressList.find(e => e.Id == id);
    if (item != null) {
      this.props.Model.OrderWebnailMoney.AddressItem = item;
      $("#loading").show();
      $.ajax({
        url: apiurl + "/updateprofile",
        type: "POST",
        dataType: "json",
        contentType: "application/json",
        data: JSON.stringify({
          model: that.props.User.OrderWebnailMoney.AddressItem
        }),
        success: response => {
          $("#loading").hide();
          if (response.status == "success") {
            var modelCart = response.data;
            that.props.onChangeTotalTax(modelCart.TotalTax);
            that.props.onChangeTotalAmount(modelCart.TotalAmount);
            that.setState(that.props);
          } else {
            toastr["error"](response.message, ERROR);
          }
        }
      });
    }
    this.state.ex.Toggle = false;
    this.setState(this.state);
  }

  handleChangeName(value) {
    var that = this;
    if (value == null || value == "") {
      return -1;
    }
    $("#loading").show();
    $.ajax({
      url: apiurl + "/updatename",
      type: "POST",
      dataType: "json",
      contentType: "application/json",
      data: JSON.stringify({ name: value }),
      beforeSend: xhr => {
        $("#loading").show();
      },
      success: response => {
        $("#loading").hide();
        if (response.status == "error") {
          alertify.error(response.message);
        }
      },
      error: e => {
        $("#loading").show();
      }
    });
  }

  handleChangePhone(value) {
    var that = this;
    if (value == null || value == "") {
      return -1;
    }
    $("#loading").show();
    $.ajax({
      url: apiurl + "/updatephone",
      type: "POST",
      dataType: "json",
      contentType: "application/json",
      data: JSON.stringify({ phone: value }),
      beforeSend: xhr => {
        $("#loading").show();
      },
      success: response => {
        $("#loading").hide();
        if (response.status == "error") {
          alertify.error(response.message);
        }
      },
      error: e => {
        $("#loading").show();
      }
    });
  }

  handleChangeEmail(value) {
    var that = this;
    if (value == null || value == "") {
      return -1;
    }
    $("#loading").show();
    $.ajax({
      url: apiurl + "/updateemail",
      type: "POST",
      dataType: "json",
      contentType: "application/json",
      data: JSON.stringify({ email: value }),
      beforeSend: xhr => {
        $("#loading").show();
      },
      success: response => {
        $("#loading").hide();
        if (response.status == "error") {
          alertify.error(response.message);
        }
      },
      error: e => {
        $("#loading").show();
      }
    });
  }

  handleSelectGender(gender) {
    var that = this;
    switch (gender) {
      case that.state.ex.Gender.Male:
        $("#Gender_1").prop("checked", true);
        break;
      case that.state.ex.Gender.Female:
        $("#Gender_2").prop("checked", true);
        break;
    }
    $("#loading").show();
    $.ajax({
      url: apiurl + "/updategender",
      type: "POST",
      dataType: "json",
      contentType: "application/json",
      data: JSON.stringify({ gender: gender }),
      beforeSend: xhr => {
        $("#loading").show();
      },
      success: response => {
        $("#loading").hide();
        if (response.status == "error") {
          alertify.error(response.message);
        }
      },
      error: e => {
        $("#loading").show();
      }
    });
  }

  handleGetCity() {
    var that = this;
    // $("#loading").show();
    that.state.ex.WardList = [];
    that.state.ex.DistrictList = [];

    that.setState(that.state);
  }

  handleChangeDistrict(value) {
    var that = this;
    $("#loading").show();
    if (value == null || value == "") {
      that.state.ex.DistrictList = [];
      that.setState(that.state);
      $("#loading").hide();
      return -1;
    }
    that.state.ex.DistrictList = that.state.ex.DistrictListDefault.filter(e => {
      return e.City.Name == value;
    });
    if (that.state.ex.DistrictList.length < 0) {
      $("#loading").hide();
      // toastr["error"]("DATA NOT FOUND", ERROR);
    } else {
      $("#loading").hide();
    }
    that.state.ex.DistrictList = that.state.ex.DistrictList.map(e => {
      return { label: e.Name, value: e.Name };
    });
    that.setState(that.state);
  }

  handleChangeWard(value) {
    var that = this;
    $("#loading").show();
    if (value == null || value == "") {
      that.state.ex.WardList = [];
      that.setState(that.state);
      $("#loading").hide();
      return -1;
    }
    that.state.ex.WardList = that.state.ex.WardListDefault.filter(e => {
      return e.District.Name == value;
    });
    if (that.state.ex.WardList.length < 0) {
      $("#loading").hide();
      //toastr["error"]("DATA NOT FOUND", ERROR);
    } else {
      $("#loading").hide();
    }
    that.state.ex.WardList = that.state.ex.WardList.map(e => {
      return { label: e.Name, value: e.Name };
    });
    that.setState(that.state);
  }
  handleUpdateCity() {
    $("#loading").show();
    $.ajax({
      url: apiurl + "/updatecity",
      type: "POST",
      dataType: "json",
      contentType: "application/json",
      data: JSON.stringify({ city: this.props.Model.City }),
      beforeSend: xhr => {
        $("#loading").show();
      },
      success: response => {
        $("#loading").hide();
        if (response.status == "error") {
          alertify.error(response.message);
        } else {
          this.props.onChangeModel(response.data);
        }
      },
      error: e => {
        $("#loading").show();
      }
    });
  }
  handleUpdateDistrict() {
    $("#loading").show();
    $.ajax({
      url: apiurl + "/updatedistrict",
      type: "POST",
      dataType: "json",
      contentType: "application/json",
      data: JSON.stringify({ district: this.props.Model.District }),
      beforeSend: xhr => {
        $("#loading").show();
      },
      success: response => {
        $("#loading").hide();
        if (response.status == "error") {
          alertify.error(response.message);
        } else {
          this.props.onChangeModel(response.data);
        }
      },
      error: e => {
        $("#loading").show();
      }
    });
  }
  handleUpdateWard() {
    $("#loading").show();
    $.ajax({
      url: apiurl + "/updateward",
      type: "POST",
      dataType: "json",
      contentType: "application/json",
      data: JSON.stringify({ ward: this.props.Model.Ward }),
      beforeSend: xhr => {
        $("#loading").show();
      },
      success: response => {
        $("#loading").hide();
        if (response.status == "error") {
          alertify.error(response.message);
        }
      },
      error: e => {
        $("#loading").show();
      }
    });
  }
  handleUpdateStreet() {
    var that = this;
    if (that.props.Model.Street == null || that.props.Model.Street == "") {
      return -1;
    }
    $("#loading").show();
    $.ajax({
      url: apiurl + "/updatestreet",
      type: "POST",
      dataType: "json",
      contentType: "application/json",
      data: JSON.stringify({ street: this.props.Model.Street }),
      beforeSend: xhr => {
        $("#loading").show();
      },
      success: response => {
        $("#loading").hide();
        if (response.status == "error") {
          alertify.error(response.message);
        }
      },
      error: e => {
        $("#loading").show();
      }
    });
  }

  initData() {
    setTimeout(() => {
      var that = this;
      if (that.props.AddressList != null) {
        if (that.props.AddressList.length > 0) {
          if (that.props.User.OrderWebnailMoney.AddressItem.Id == 0) {
            $("#ProfileDetails_" + that.props.AddressList[0].Id + "_Id").prop(
              "checked",
              true
            );
            var itemAddress = that.props.AddressList.find(
              e => e.Id == that.props.AddressList[0].Id
            );
            if (itemAddress != null) {
              that.props.User.OrderWebnailMoney.AddressItem = itemAddress;
              that.props.User.OrderWebnailMoney.OrderBusinessName =
                itemAddress.BusinessName;
              $("#loading").show();
              $.ajax({
                url: apiurl + "/updateprofile",
                type: "POST",
                dataType: "json",
                contentType: "application/json",
                data: JSON.stringify({
                  model: that.props.User.OrderWebnailMoney.AddressItem
                }),
                success: response => {
                  $("#loading").hide();
                  if (response.status == "success") {
                    var modelCart = response.data;
                    that.props.onChangeTotalTax(modelCart.TotalTax);
                    that.props.onChangeTotalAmount(modelCart.TotalAmount);
                    that.setState(that.props);
                  } else {
                    toastr["error"](response.message, ERROR);
                  }
                }
              });
            }
          } else {
            $(
              "#ProfileDetails_" +
                that.props.User.OrderWebnailMoney.AddressItem.Id +
                "_Id"
            ).prop("checked", true);
            var itemAddress = that.props.AddressList.find(
              e => e.Id == that.props.User.OrderWebnailMoney.AddressItem.Id
            );
            if (itemAddress != null) {
              that.props.User.OrderWebnailMoney.AddressItem = itemAddress;
              that.props.User.OrderWebnailMoney.OrderBusinessName =
                itemAddress.BusinessName;
            }
          }

          that.state.ex.Toggle = false;
          that.setState(that.state);
        }
      }
    }, 500);
  }
  initGender() {
    var that = this;
    setTimeout(() => {
      switch (that.props.Model.Gender) {
        case that.state.ex.Gender.Male:
          $("#Gender_1").prop("checked", true);
          break;
        case that.state.ex.Gender.Female:
          $("#Gender_2").prop("checked", true);
          break;
      }
    }, 500);
  }
  componentWillMount() {
    this.initGender();
  }
  componentDidMount() {
    // this.initData();
  }

  componentWillUnmount() {
    $("#scriptloading").html("");
    var that = this;
    that.state.ex.Toggle = false;
    that.setState(that.state);
  }
  render() {
    return (
      <React.Fragment>
        {this.state && this.state.ex ? (
          <React.Fragment>
            <div className="section-body">
              <h2 className="h2 text-bold text-uppercase text-center text-black hidden">
                Thông tin đơn hàng
              </h2>
              <div className="oldaddress">
                {this.props.AddressList
                  ? this.props.AddressList.map(c => {
                      return (
                        <div className="item">
                          <div className="checkbox-container">
                            <input
                              type="radio"
                              name="ProfileDetails_Id"
                              id={`ProfileDetails_${c.Id}_Id`}
                              value={c.Id}
                              onClick={e => {
                                this.handleAddInfoAddress(c.Id);
                              }}
                            />
                            <label for={`ProfileDetails_${c.Id}_Id`}>
                              <span>
                                <b>Customer Name:</b> {c.FirstName} {c.LastName}{" "}
                                - {this.props.Model.CustomerPhone}
                              </span>{" "}
                              <span>
                                <b>Address:</b> {c.Street} , {c.State}{" "}
                                {c.ZipCode}, {c.City}, {c.Country}
                              </span>
                            </label>
                          </div>
                        </div>
                      );
                    })
                  : ""}
              </div>
              <div className="newaddressbtn">
                <a href="javascript:;" style={{ fontWeight: "bold" }}>
                  Nhập thông tin giao hàng{" "}
                  <i className="demo-icon ecs-down-open-mini" />
                </a>
              </div>
              <div
                className="newaddresform"
                style={{ display: this.state.ex.Toggle ? "block" : "none" }}
              >
                <div className="section-header">
                  <div className="item clearfix">
                    <div className="label clearfix flex-center">
                      <div className="checkbox-container">
                        <input
                          type="radio"
                          name="Gender"
                          id="Gender_1"
                          value={this.state.ex.Gender.Male}
                          onClick={e => this.handleSelectGender(e.target.value)}
                        />
                        <label for={`Gender_1`}>
                          <span>Anh</span>
                        </label>
                      </div>
                      <div className="checkbox-container">
                        <input
                          type="radio"
                          name="Gender"
                          id="Gender_2"
                          value={this.state.ex.Gender.Female}
                          onClick={e => this.handleSelectGender(e.target.value)}
                        />
                        <label for={`Gender_2`}>
                          <span>Chị</span>
                        </label>
                      </div>
                    </div>
                    <DelayInput
                      type="text"
                      className="form-control"
                      placeholder="Họ và tên (bắt buộc)"
                      value={this.props.Model.FirstName}
                      onChange={e => {
                        this.props.Model.FirstName = e.target.value;
                        this.setState(this.state);
                      }}
                      onBlur={e => this.handleChangeName(e.target.value)}
                    />
                  </div>

                  <div className="item clearfix">
                    <div className="label clearfix">Điện thoại</div>
                    <DelayInput
                      type="text"
                      className="form-control"
                      placeholder="Số điện thoại (bắt buộc)"
                      value={this.props.Model.CustomerPhone}
                      onChange={e => {
                        this.props.Model.CustomerPhone = e.target.value;
                        this.setState(this.state);
                      }}
                      onBlur={e => this.handleChangePhone(e.target.value)}
                    />
                  </div>
                  <div className="item clearfix">
                    <div className="label clearfix">Email</div>
                    <DelayInput
                      type="email"
                      className="form-control"
                      placeholder="Email (không bắt buộc)"
                      value={this.props.Model.CustomerEmail}
                      onChange={e => {
                        this.props.Model.CustomerEmail = e.target.value;
                        this.setState(this.state);
                      }}
                      onBlur={e => this.handleChangeEmail(e.target.value)}
                    />
                  </div>
                </div>
                <div className="section-body">
                  <div className="item clearfix">
                    <select
                      className="form-control"
                      value={this.props.Model.City}
                      onChange={e => {
                        this.props.Model.City = e.target.value;
                        this.handleGetCity(this.props.Model.City);
                        this.handleChangeDistrict(this.props.Model.City);
                        this.handleUpdateCity();
                        this.setState(this.state);
                      }}
                    >
                      <option disabled={true} selected={true}>
                        Tỉnh / Thành Phố
                      </option>
                      {this.state.ex.CityList ? (
                        this.state.ex.CityList.map(e => {
                          return <option value={e.value}>{e.label}</option>;
                        })
                      ) : (
                                                <option selected={true}>Tỉnh / Thành Phố</option>
                      )}
                    </select>
                  </div>
                  <div className="item clearfix">
                    <select
                      className="form-control"
                      value={this.props.Model.District}
                      onChange={e => {
                        this.props.Model.District = e.target.value;
                        this.handleChangeWard(e.target.value);
                        this.handleUpdateDistrict();
                        this.setState(this.state);
                      }}
                    >
                                        <option selected={true}>Quận / Huyện</option>
                      {this.state.ex.DistrictList ? (
                        this.state.ex.DistrictList.map(e => {
                          return <option value={e.value}>{e.label}</option>;
                        })
                      ) : (
                        <option selected={true}>Quận / Huyện</option>
                      )}
                    </select>
                  </div>
                  <div className="item clearfix">
                    <select
                      className="form-control"
                      value={this.props.Model.Ward}
                      onChange={e => {
                        this.props.Model.Ward = e.target.value;
                        this.handleUpdateWard();
                        this.setState(this.state);
                      }}
                    >
                      <option selected={true}>Phường / Xã</option>
                      {this.state.ex.WardList ? (
                        this.state.ex.WardList.map(e => {
                          return <option value={e.value}>{e.label}</option>;
                        })
                      ) : (
                                                <option selected={true}>Phường / Xã</option>
                      )}
                    </select>
                  </div>

                  <div className="item clearfix">
                    <DelayInput
                      type="text"
                      className="form-control"
                      placeholder="Số nhà, tên đường (bắt buộc)"
                      value={this.props.Model.Street}
                      onChange={e => {
                        this.props.Model.Street = e.target.value;
                        this.setState(this.state);
                      }}
                      onBlur={e => {
                        this.handleUpdateStreet();
                      }}
                    />
                  </div>
                </div>
                <div className="section-footer">
                  <div className="item clearfix">
                    {this.props.Model.Coupon != null ? (
                      <React.Fragment>
                        <DelayInput
                          type="text"
                          className="form-control"
                          placeholder="Mã Khuyến Mãi"
                          value={this.props.Model.Coupon.CouponCode}
                          readOnly
                        />
                        <a
                          href="javascript:;"
                          className="btn btn-primary"
                          onClick={e => this.RemoveCoupon()}
                        >
                          Hủy
                        </a>
                      </React.Fragment>
                    ) : (
                      <React.Fragment>
                        <DelayInput
                          type="text"
                          className="form-control"
                                                    placeholder="Mã Khuyến Mãi"
                          value={this.state.ex.couponcode}
                          onChange={e => {
                            this.state.ex.couponcode = e.target.value.toUpperCase();
                            this.setState(this.state);
                          }}
                        />
                        <a
                          href="javascript:;"
                          className="btn btn-primary"
                          onClick={e => this.ApplyCoupon()}
                        >
                          Áp Dụng
                        </a>
                      </React.Fragment>
                    )}
                  </div>
                  <div className="item clearfix">
                    <textarea
                      className="form-control"
                      placeholder="Lời nhắn (Không bắt buộc)"
                      value={this.props.Model.Notes}
                      onChange={e => {
                        this.props.Model.Notes = e.target.value;
                        this.setState(this.state);
                      }}
                    />
                  </div>
                </div>
              </div>
              <div className="newaddresform hidden">
                <div className="payment p-2">
                  <h4 className="h4 text-bold text-black">
                    Your Payment Information
                  </h4>
                  <div className="item-pay p-1">
                    <label className="text-bold text-black">Credit Card</label>
                    <span>
                      <img
                        // src="/themes/royalbeauty/global/img/pay2.jpg"
                        alt=""
                      />
                    </span>
                  </div>
                  <div className="item-pay clearfix">
                    <div className="label text-bold text-black p-bot-1 ">
                      Card Number *
                    </div>
                    <DelayInput
                      type="text"
                      className="form-control"
                      placeholder="Card Number"
                      value={this.props.Payment.CartNumber}
                      onChange={e => {
                        this.props.Payment.CartNumber = e.target.value;
                        this.setState(this.state);
                      }}
                    />
                  </div>
                  <div className="item-pay clearfix">
                    <div className="label text-bold text-black p-1">
                      Expiration Month - Year *
                    </div>
                    <div className="expiration flex-container">
                      <select
                        name=""
                        id=""
                        value={
                          this.props.Payment.ExpirationMonth
                            ? this.props.Payment.ExpirationMonth
                            : "01"
                        }
                        className="form-control"
                        onChange={e => {
                          this.props.Payment.ExpirationMonth = e.target.value;
                          this.setState(this.state);
                        }}
                      >
                        {this.state.Month
                          ? this.state.Month.map(e => {
                              return <option value={e.value}>{e.label}</option>;
                            })
                          : ""}
                      </select>
                      <select
                        name=""
                        id=""
                        value={this.props.Payment.ExpirationYear}
                        className="form-control"
                        onChange={e => {
                          this.props.Payment.ExpirationYear = e.target.value;
                          this.setState(this.state);
                        }}
                      >
                        {this.state.Year
                          ? this.state.Year.map(e => {
                              return <option value={e.value}>{e.label}</option>;
                            })
                          : ""}
                      </select>
                    </div>
                  </div>
                  <div className="item-pay clearfix">
                    <div className="label text-bold text-black p-1">
                      CCV Code *
                    </div>
                    <DelayInput
                      type="text"
                      className="form-control"
                      placeholder="Enter CCV Code"
                      value={this.props.Payment.CcvCode}
                      onChange={e => {
                        this.props.Payment.CcvCode = e.target.value;
                        this.setState(this.state);
                      }}
                    />
                  </div>
                </div>
              </div>
            </div>
          </React.Fragment>
        ) : (
          ""
        )}
      </React.Fragment>
    );
  }
}

export default Profile;
