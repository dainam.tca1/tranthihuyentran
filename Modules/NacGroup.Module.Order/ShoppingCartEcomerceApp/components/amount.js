﻿import React, { Component } from "react";
import "../helper/string-helper";
// 
import AppContext from "../../../../NacGroup/CmsApp/components/app-context";
class Amount extends Component {
    constructor(props, context) {
        super(props, context);
        var that = this;

    }

    componentWillMount() { }

    componentDidMount() { }

    componentWillUnmount() {
        $("#scriptloading").html("");
    }
    render() {
        const convertMoney = (price) => {
            return (
                this.context.CurrencyType === 0 ? (
                    price ? price.formatMoney(
                        2,
                        ".",
                        ",",
                        "$"
                    ) : "0"
                ) : (
                        price ? price.formatMoney(
                            0,
                            ",",
                            "."
                        ) : "0"
                    )
            )
        }
        return (
            <React.Fragment>
                <div className="section-body">
                    <div className="item clearfix">
                        <label>Tạm tính:</label>
                        <p>{convertMoney(this.props.SubTotalAmount)}</p>
                    </div>

                    <div className="item clearfix">
                        <label>Giảm giá:</label>
                        <p>{convertMoney(this.props.DiscountAmount)}</p>
                    </div>         
                    <div className="item flex-center">
                        <label>Thành tiền:</label>
                        <p className="h2">
                            <p>{convertMoney(this.props.TotalAmount)}</p>
                        </p>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}
Amount.contextType = AppContext;
export default Amount;
