﻿import React, { Component } from "react";
import Process from "./process";
function GetURLParameter(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}
class Success extends Component {
    constructor(props) {
        super(props);
        var that = this;
        that.state = {
            model: null,
            ex: {
                Title: "Đặt hàng thành công",
                Step: 2,

            }
        }
        that.setState(that.state);
        $("#loading").show();
        $.get("/checkout/success", { code: GetURLParameter('code') }, function (res) {
            that.state.model = res;
            $("#loading").hide();
            that.setState(that.state);
        }).fail((e) => {
            $("#loading").hide();
        })
    }
    
  componentWillMount() {
   
  }
    componentDidMount() {
        document.title = "Đặt hàng thành công";
  }

  componentWillUnmount() {
    $("#scriptloading").html("");
  
  }
  render() {
    return (
        <React.Fragment>
            {this.state.ex && this.state.model ? (
                <React.Fragment>
                    <h1 className="hidden">{this.state.ex.Titlte}</h1>
                 
                    <div className="container" id="payment">
                        <div className="row clearfix bs-wizard">
                            <Process Step={this.state.ex.Step} />
                        </div>
                        <div className="row clearfix">
                            <div className="col-md-12">
                                <div className="panel panel-default cart success m-top-4">
                                    <div className="panel-body">
                                        <h3>Cảm ơn bạn đã mua hàng tại trang web của chúng tôi!</h3>

                                        <p>Mã đơn hàng: </p>
                                        <div className="well well-sm">
                                            {this.state.model.OrderNumber}
                                        </div>
                                        <p>
                                            <img src="/img/ico2.png" height="25" width="30" alt="" />
                                            Bạn có thể xem lại <a href={`/tai-khoan/don-hang/${this.state.model.OrderNumber}`}>đơn hàng</a>
                                                    </p>
                                        <p>
                                            Các chi tiết đặt hàng đã được gửi đến địa chỉ email <span>{this.state.model.CustomerEmail}</span>. Nếu không tìm thấy xin vui lòng kiểm tra trong hộp thư <strong>Spam</strong> or <strong>Junk Folder</strong>.
                                                    </p>
                                        <div className="alert alert-success no-icon" role="alert">
                                            <p>Để giúp xử lý đơn hàng nhanh chóng, chúng tôi sẽ gọi cho bạn để xác nhận đơn hàng của bạn.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </React.Fragment>
            ): ""}
          
      </React.Fragment>
    );
  }
}

export default Success;
