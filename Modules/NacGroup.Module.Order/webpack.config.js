﻿"use strict";

module.exports = [
    {
        entry: "./ShoppingCartApp/app.js",
        output: {
            path: __dirname,
            filename: "../../NacGroup/wwwroot/js/shoppingcart.js"
        },
        module: {
            rules: [
                {
                    test: /\.(js|jsx)$/,
                    exclude: /node_modules/,
                    use: {
                        loader: "babel-loader",
                        options: {
                            presets: ["@babel/preset-env", "@babel/preset-react"]
                        }
                    }
                }
            ]
        }
    },
    {
        entry: "./ShoppingCartApp/orderstatus-app.js",
        output: {
            path: __dirname,
            filename: "../../NacGroup/wwwroot/js/orderstatus.js"
        },
        module: {
            rules: [
                {
                    test: /\.(js|jsx)$/,
                    exclude: /node_modules/,
                    use: {
                        loader: "babel-loader",
                        options: {
                            presets: ["@babel/preset-env", "@babel/preset-react"]
                        }
                    }
                }
            ]
        }
    },
    {
        entry: "./ShoppingCartEcomerceApp/app.js",
        output: {
            path: __dirname,
            filename: "../../NacGroup/wwwroot/js/shoppingcarteco.js"
        },
        module: {
            rules: [
                {
                    test: /\.(js|jsx)$/,
                    exclude: /node_modules/,
                    use: {
                        loader: "babel-loader",
                        options: {
                            presets: ["@babel/preset-env", "@babel/preset-react"]
                        }
                    }
                }
            ]
        }
    },
   
];
