﻿using NacGroup.Module.Order.Services;

namespace NacGroup.Module.Order.Extensions
{
    public interface IShipmentStrategyFactory
    {
        IEasyShipService[] Create();
    }
    public class ShipmentStrategyFactory : IShipmentStrategyFactory
    {
        private readonly EasyShipService _easyShipService;
        private readonly FedExService _fedExService;

        public ShipmentStrategyFactory(
            EasyShipService easyShipService,
            FedExService fedExService)
        {
            _easyShipService = easyShipService;
            _fedExService = fedExService;
        }

        public IEasyShipService[] Create()
        {
            return new IEasyShipService[]{ _easyShipService ,_fedExService};
        }
    }
}
