import React from "react";
import ReactDOM from "react-dom";
import App from "./components/orderstatusapp";

ReactDOM.render(<App />, document.getElementById("app"));
