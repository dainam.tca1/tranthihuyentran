import React, { Component } from "react";
import { BrowserRouter, Redirect, Route } from "react-router-dom";
import OrderStatusList from "./orderstatuslist";

class App extends Component {
    render() {
        return (
            <BrowserRouter>
                <React.Fragment>
                    <Route exact path="/order-status" component={OrderStatusList} />
                </React.Fragment>
            </BrowserRouter>
        );
    }
}

export default App;
