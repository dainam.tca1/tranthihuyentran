import React, { Component } from "react";
import { Link } from "react-router-dom";

class ButtonViewCart extends Component {
  constructor(props) {
    super(props);
    var that = this;
    that.state = {
        model: null,
        ex: {
            Quantity:0
        }
      };
      that.setState(that.state);
     
  }
    viewCart() {
        var that = this;
      
        if (that.props.model.OrderItems.length > 0) {
            $("#viewCartModal").modal("show");
        }
    }
    RemoveItems(id) {
        var that = this;
        alertify.confirm(
            'Warning',
            'Are you sure delete product',
            function () {
                $("#loading").show();
                $.ajax({
                    url: "/gposorder/removeitem",
                    type: "POST",
                    dataType: "json",
                    contentType: "application/json",
                    data: JSON.stringify(id),
                    success: function (response) {
                        $("#loading").hide();
                        if (response.status == "success") {
                            $(".cart-count-txt").text(
                                response.data.OrderItems.length
                            );
                            $("p.count").text(response.data.OrderItems.length);
                            $(".cart-money").text("$" + response.data.TotalAmount.toFixed(2));
                            $("#info-modal .skuinput").val(response.data.NewItemId);
                            if (response.data.OrderItems.length == 0) {
                              //  $("#viewCartModal .modal-body .nc-table-cart table tbody").empty();
                                $("#viewCartModal").modal("hide");
                            }
                          //  $("#viewCartModal .modal-body .nc-table-cart tr td.subtotal").text("$" + response.data.SubTotalAmount.toFixed(2));     
                            that.props.onChange(response.data);
                            that.setState(that.state);
                        } else {
                            alertify.alert("Error", response.message);
                        }
                    },
                    error: function (er) {
                        $("#loading").hide();
                        alertify.alert("Error", "Error");
                    }
                });
            },
            function () { }
        )
    }

    Increase(id) {
        var that = this;
        that.state.ex.Quantity = $('#quantity-value-' + id + '').val();
        if (that.state.ex.Quantity == $('#quantity-value-' + id + '').val()) {
            that.state.ex.Quantity = parseInt(that.state.ex.Quantity) + 1;
            that.setState(that.state);
        }
        $('#quantity-value-' + id + '').val(that.state.ex.Quantity);
        $("#loading").show();
        $.ajax({
            url: "/gposorder/updatequantity",
            type: "POST",
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify({
                id: id,
                quantity: that.state.ex.Quantity,
            }),
            success: response => {
                $("#loading").hide();
                if (response.status == "success") {
                    that.props.onChange(response.data);
                    $(".cart-money").text("$" + response.data.TotalAmount.toFixed(2));
                   // $("#viewCartModal .modal-body .nc-table-cart td.subtotal").text("$" + response.data.SubTotalAmount.toFixed(2));
                    that.setState(that.state);
                } else {
                    alertify.alert("Error", response.message);
                }
            },
            error: function (er) {
                $("#loading").hide();
                alertify.alert("Error", "Error");
            }
        });
        that.setState(that.state);
    }
    Decrease(id) {
        var that = this;
        that.state.ex.Quantity = $('#quantity-value-' + id + '').val();
        if (that.state.ex.Quantity == $('#quantity-value-' + id + '').val()) {
            that.state.ex.Quantity = parseInt(that.state.ex.Quantity) - 1;
            that.setState(that.state);
        }
        $('#quantity-value-' + id + '').val(that.state.ex.Quantity);
        if (that.state.ex.Quantity == 0) {
            alertify.alert("Warning","Minimum quantity is 1");
            return -1;
        } else {
            $("#loading").show();
            $.ajax({
                url: "/gposorder/updatequantity",
                type: "POST",
                dataType: "json",
                contentType: "application/json",
                data: JSON.stringify({
                    id: id,
                    quantity: that.state.ex.Quantity,
                }),
                success: response => {
                    $("#loading").hide();
                    if (response.status == "success") {
                        that.props.onChange(response.data);
                        $(".cart-money").text("$" + response.data.TotalAmount.toFixed(2));
                       // $("#viewCartModal .modal-body .nc-table-cart td.subtotal").text("$" + response.data.SubTotalAmount.toFixed(2));
                        that.setState(that.state);
                    } else {
                        alertify.alert("Error", response.message);
                    }
                },
                error: function (er) {
                    $("#loading").hide();
                    alertify.alert("Error", "Error");
                }
            });
        }

        that.setState(that.state);
    }
  render() {
    return (
      <React.Fragment>
            <a href="javascript:;" onClick={e => this.viewCart()}  className="cart-scroll-button hidden-xs hidden-sm">
                <p className="count">{this.props.model.OrderItems.length}</p>
                <p className="icon-cart"><a href="javascript:;" ><span className="demo-icon ecs-basket"></span></a></p>
            </a>
            <div id="viewCartModal" className="modal fade" tabIndex="-1" role="dialog" aria-labelledby="viewCartLabel" aria-hidden="true">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header">

                        </div>
                        <div className="modal-body">
                            <div className="nc-table-cart">
                                <table>
                                    <thead>
                                        <tr>
                                            <th>
                                                item
                                                    </th>
                                            <th>
                                                qty
                                                    </th>
                                            <th>
                                                sub total
                                                    </th>
                                            <th>

                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {this.props.model.OrderItems.length > 0 ? this.props.model.OrderItems.map(d => {
                                            return (
                                                <tr>
                                                    <td>
                                                        <div className="title">
                                                            {d.Name}
                                                        </div>
                                                        <ul>
                                                            {d.ModifierItems.length > 0 ? d.ModifierItems.map(modi => {
                                                                return (
                                                                    <React.Fragment>
                                                                        <li>+ {modi.Name}</li>
                                                                    </React.Fragment>
                                                                )
                                                            }) : ""}
                                                        </ul>
                                                        <div>
                                                        </div>
                                                        {d.SpecialRequest ? (
                                                            <React.Fragment>
                                                                <div style={{ fontSize: "12px", textTransform: "none" }}>Special request: {d.SpecialRequest}</div>
                                                            </React.Fragment>
                                                        ): ""}
                                                    </td>
                                                    <td>
                                                        <span className="qty">
                                                            <div className="nc-quantity-container">
                                                                <div className="nc-divide">
                                                                    <div className="nc-divide-inner">
                                                                        <a href="javascript:;" onClick={e => this.Decrease(d.Id)}>-</a>
                                                                    </div>
                                                                </div>
                                                                <div className="nc-input">
                                                                    <input disabled="disabled" type="text" id={`quantity-value-${d.Id}`} value={d.Quantity} />
                                                                </div>
                                                                <div className="nc-increase">
                                                                    <div className="nc-increase-inner">
                                                                        <a href="javascript:;" onClick={e => this.Increase(d.Id)}>+</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </span>
                                                    </td>
                                                    <td>
                                                        <span className="sub">${d.Price ? d.Price.toFixed(2) : "0.00"}</span>
                                                    </td>
                                                    <td>
                                                        <a href="javascript:;" onClick={e => this.RemoveItems(d.Id)} ><span className="demo-icon ecs-trash"></span></a>
                                                    </td>
                                                </tr>
                                            )

                                        }) : ""}
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colSpan="2"></td>
                                            <td>SUBTOTAL</td>
                                            <td className="subtotal">${this.props.model.SubTotalAmount ? this.props.model.SubTotalAmount.toFixed(2) : "0.00"}</td>
                                        </tr>
                                        <tr>
                                            <td colSpan="2"></td>
                                            <td>TAX</td>
                                            <td className="tax">${this.props.model.TotalTax ? this.props.model.TotalTax.toFixed(2) : "0.00"}</td>
                                        </tr>
                                        <tr>
                                            <td colSpan="2"></td>
                                            <td>TOTAL</td>
                                            <td className="total">${this.props.model.TotalAmount ? this.props.model.TotalAmount.toFixed(2) : "0.00"}</td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>

                        </div>
                        <div className="modal-footer ">
                            <button type="button" className="btn btn-secondary" data-dismiss="modal">Continue shopping</button>
                            <a href="/gposshoppingcart/orderinformation" className="btn btn-primary">Checkout</a>
                        </div>
                    </div>
                </div>
            </div >
      </React.Fragment>
    );
  }
}

export default ButtonViewCart;
