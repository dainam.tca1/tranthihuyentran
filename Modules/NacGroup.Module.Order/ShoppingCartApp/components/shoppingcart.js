import React, { Component } from "react";
import { Link } from "react-router-dom";

class Booking extends Component {
  constructor(props) {
    super(props);
    var that = this;
    that.state = {
      model: null
    };
    document.title = "Shopping Cart";
    $("#loading").show();
    $.ajax({
      url: "/gposorder/getcart",
      type: "GET",
      dataType: "json",
      contentType: "application/json",
      success: response => {
        $("#loading").hide();
        that.state.model = response;
        that.setState(that.state);
      },
      error: function(er) {
        $("#loading").hide();
      }
    });
  }

  RemoveCartItem(itemId) {
    var that = this;
    $("#loading").show();
    $.ajax({
      url: "/gposorder/removeitem",
      type: "POST",
      dataType: "json",
      contentType: "application/json",
      data: JSON.stringify(itemId.toUpperCase()),
      success: response => {
        $("#loading").hide();
        if (response.status === "success") {
          that.state.model = response.data;
          $(".cart-container .cart-count-txt").text(
            that.state.model.OrderItems.length
          );
          $(".cart-container .cart-money").text(
            "$" + that.state.model.TotalAmount.toFixed(2)
          );
          that.setState(that.state);
        } else {
          alert(response.message);
        }
      },
      error: function(er) {
        $("#loading").hide();
        alert("System error");
      }
    });
  }

  UpdateQuantity(itemId, quantity) {
    if (!quantity || isNaN(quantity)) {
      var index = this.state.model.OrderItems.findIndex(e => {
        return e.Id == itemId;
      });
      if (index < 0) {
        return false;
      }
      this.state.model.OrderItems[index].Quantity = null;
      this.setState(this.state);
      return false;
    }

    var that = this;
    $("#loading").show();
    $.ajax({
      url: "/gposorder/UpdateQuantity",
      type: "POST",
      dataType: "json",
      contentType: "application/json",
      data: JSON.stringify({
        id: itemId.toUpperCase(),
        quantity: quantity
      }),
      success: response => {
        $("#loading").hide();
        if (response.status === "success") {
          that.state.model = response.data;
          $(".cart-container .cart-count-txt").text(
            that.state.model.OrderItems.length
          );
          $(".cart-container .cart-money").text(
            "$" + that.state.model.TotalAmount.toFixed(2)
          );
          that.setState(that.state);
        } else {
          alert(response.message);
        }
      },
      error: function(er) {
        $("#loading").hide();
        alert("System error");
      }
    });
    return true;
  }

  render() {
    return (
      <React.Fragment>
        <div id="breadcrumb-cart" className="container h1 p-top-2">
          <h2 className="breadcrumb-header text-semibold p-bot-1">My Order</h2>
        </div>

        {this.state.model && this.state.model.OrderItems.length > 0 ? (
          <React.Fragment>
            <div className="container">
              <div className="row clearfix">
                <div className="col col-sm-12 col-lg-12">
                  <section id="cart" className="">
                    <div className="section-header btn-container flex-center m-bot-2">
                      <a href="/" className="btn btn-border-primary h5">
                        Add More Food
                      </a>
                      <Link
                        to="/gposshoppingcart/orderinformation"
                        className="btn btn-primary h5"
                      >
                        Checkout
                      </Link>
                    </div>
                    <div className="bg-white">
                      <div className="section-body">
                        {this.state.model.OrderItems.map(sku => {
                          return (
                            <div className="item">
                              <div className="img-container">
                                <img src={sku.Avatar} alt={sku.Name} />
                              </div>
                              <div className="text-container">
                                <h3 className="h5">
                                  <a href="javascript:">{sku.Name}</a>
                                </h3>
                                <div className="des m-top-1">
                                  {sku.ModifierItems.map(modi => {
                                    return <p>+{modi.Name}</p>;
                                  })}
                                  {sku.SpecialRequest && (
                                    <p>Special Request: {sku.SpecialRequest}</p>
                                  )}
                                </div>
                              </div>
                              <div className="btn-container">
                                <div className="price">
                                  <div className="new-price h5">
                                    ${sku.Price.toFixed(2)}
                                  </div>
                                  {/* <div className="old-price">$8.84</div> */}
                                </div>
                                <input
                                  className="form-control"
                                  value={sku.Quantity}
                                  onChange={e => {
                                    this.UpdateQuantity(
                                      sku.Id,
                                      parseInt(e.target.value)
                                    );
                                  }}
                                />
                                <a
                                  href="javascript:"
                                  className=""
                                  onClick={e => {
                                    this.RemoveCartItem(sku.Id);
                                  }}
                                >
                                  Remove
                                </a>
                              </div>
                            </div>
                          );
                        })}
                      </div>
                      <div className="total-contaier section-footer m-top-2">
                        <p className="h5 text-bold clearfix">
                          <span>Subtotal</span>
                          <span>
                            ${this.state.model.SubTotalAmount.toFixed(2)}
                          </span>
                        </p>
                        <p className="clearfix">
                          <span>Discount</span>
                          <span>
                            {this.state.model.DiscountAmount > 0 ? (
                              <React.Fragment>
                                -${this.state.model.DiscountAmount.toFixed(2)}
                              </React.Fragment>
                            ) : (
                              <React.Fragment>$0.00</React.Fragment>
                            )}
                          </span>
                        </p>
                        <p className="clearfix">
                          <span>Tax</span>
                          <span>${this.state.model.TotalTax.toFixed(2)}</span>
                        </p>
                        <p className="clearfix">
                          <span>Tips</span>
                          <span>${this.state.model.Tips.toFixed(2)}</span>
                        </p>
                        <p className="clearfix h5 text-bold">
                          <span>Total</span>
                          <span>
                            ${this.state.model.TotalAmount.toFixed(2)}
                          </span>
                        </p>
                      </div>
                      <div className="btn-container section-footer-plus flex-center m-top-2">
                        <a href="/" className="btn btn-border-primary h5">
                          Add More Food
                        </a>
                        <Link
                          to="/gposshoppingcart/orderinformation"
                          className="btn btn-primary h5"
                        >
                          Checkout
                        </Link>
                      </div>
                    </div>
                  </section>
                </div>
              </div>
            </div>
          </React.Fragment>
        ) : (
          <React.Fragment>
            <section id="cart-empty" className="container">
              <h2 className="bg-white section-header text-center h3 text-bold p-4">
                There are no items in your shopping cart.
              </h2>
            </section>
          </React.Fragment>
        )}
      </React.Fragment>
    );
  }
}

export default Booking;
