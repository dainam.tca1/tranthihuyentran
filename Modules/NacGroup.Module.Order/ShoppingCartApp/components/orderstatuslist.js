import React, { Component } from "react";
import { Link } from "react-router-dom";
import moment from "moment";

class OrderStatusList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            model: null,
            currentpage: 0,
            pagesize: 6,
            pagedata: null,
            pagenumber: 0
        };

        $("#loading").show();
        this.getdata();
        var that = this;
        setInterval(() => {
            that.getdata();
        }, 60 * 1000);
        setInterval(() => {
            if (that.state.model) {
                that.nextpage();
            }
        }, 5000);
    }

    getdata() {
        var that = this;
        $.ajax({
            url: "/gposorder/getdata",
            type: "GET",
            dataType: "json",
            contentType: "application/json",
            success: response => {
                $("#loading").hide();
                if (response.status == "success") {
                    that.state.model = response.data ? response.data.filter(e => {
                        return ((
                            e.Status.toLowerCase() == "processing" ||
                            e.Status.toLowerCase() == "rfp" ||
                            e.Status.toLowerCase() == "pending" ||
                            e.Status.toLowerCase() == "preparing") && e.IsMobile
                        )
                    }) : "";
                    that.state.pagenumber = Math.ceil(
                        that.state.model.length / that.state.pagesize
                    );
                    that.setState(that.state);
                    if (!that.state.pagedata) {
                        that.nextpage();
                    }
                } else {
                }
            },
            error: function (er) {
                $("#loading").hide();
            }
        });
    }

    nextpage() {
        if (this.state.currentpage == this.state.pagenumber) {
            this.state.currentpage = 1;
        } else {
            this.state.currentpage++;
        }
        this.state.pagedata = this.state.model.slice(
            (this.state.currentpage - 1) * this.state.pagesize,
            (this.state.currentpage - 1) * this.state.pagesize + this.state.pagesize
        );
        this.setState(this.state);
    }

    render() {
        return (
            this.state.model &&
            this.state.pagedata && (
                <div className="container-fluid">
                    
                    <table className="table table-striped">
                        <thead>
                            <tr style={{ background: "#07f3f7" }}>
                                <th>Order Number</th>
                                <th>Customer Name</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.pagedata.map(order => {
                                return (
                                    <tr>
                                        <td>#{order.OrderNumber}</td>
                                        <td>
                                            {order.CustomerFirstName} {order.CustomerLastName}
                                        </td>
                                        <td>
                                            {order.Status.toLowerCase() == "rfp" ? (
                                                <span className="label label-success blink_me">
                                                    Ready For Pick-up
                        </span>
                                            ) : (
                                                    <span className="label label-default">
                                                        {order.Status}
                                                    </span>
                                                )}
                                        </td>
                                    </tr>
                                );
                            })}
                        </tbody>
                    </table>
                </div>
            )
        );
    }
}

export default OrderStatusList;
