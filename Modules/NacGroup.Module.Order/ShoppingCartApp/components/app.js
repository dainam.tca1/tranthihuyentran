import React, { Component } from "react";
import { BrowserRouter, Route } from "react-router-dom";
import ShoppingCart from "./shoppingcart";
import OrderInfo from "./orderinfo";
import checkout from "./checkout";
import categoryDetail from "./categorydetail";
import ViewOrder from "./vieworder";

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <React.Fragment>
          <Route
            exact
            path="/gposshoppingcart/orderinformation"
            component={OrderInfo}
                />
            <Route
                    exact
                    path="/gposshoppingcart/vieworder"
                    component={ViewOrder}
                />
          <Route exact path="/gposshoppingcart/checkout" component={checkout} />
          <Route
            exact
            path="/gposshoppingcart/category/:id"
            component={categoryDetail}
          />
          <Route exact path="/gposshoppingcart" component={ShoppingCart} />
        </React.Fragment>
      </BrowserRouter>
    );
  }
}

export default App;
