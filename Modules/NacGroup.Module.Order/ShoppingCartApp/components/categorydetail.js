﻿import React, { Component } from "react";
import ButtonViewCart from "./buttonpopup";

import CategoryList from "./categorylist";
import { Link } from "react-router-dom";

class CategoryDetail extends Component {
  constructor(props) {
    super(props);
    var that = this;
    that.state = {
      model: null,
      ex: {
        AllProduct: [],
        Modifier: [],
        ItemProduct: null,
        TotalAmount: 0,
        Total: 0,
        Quantity: 0,
        IsCheckedModifier: [],
        PriceModifier: 0,
        SkuPrice: 0,
        CategoryList: [],
        Note: null,
        Title: null
      }
    };
    $("#loading").show();

    $.get("/api/gposproductcategories/getcategory", function(response) {
      $("#loading").hide();
      that.state.ex.CategoryList = response;
      var item = that.state.ex.CategoryList.find(e => {
        return e.Id == that.props.match.params.id;
      });
      if (item != null) {
        that.state.ex.Title = item.Name;
        document.title = that.state.ex.Title;
      }
      that.getProductByCategoryId();
      that.setState(that.state);
    }).fail(function() {
      alertify.alert("error", "DATA Not found");
    });
    that.getCart();
    that.setState(that.state);
  }

  OrderNowPopup(id) {
    var that = this;
    if (that.state.ex.Modifier === null) {
      alertify.alert("Error", "Please wait for loading data");
      return -1;
    }
    that.state.ex.ItemProduct = that.state.ex.AllProduct.find(function(e) {
      return e.PosId == id;
    });
    if (!that.state.ex.ItemProduct) {
      alertify.alert("Error", "Data is not found");
      return -1;
    }
    if (that.state.ex.ItemProduct.Skus.length > 0) {
      // $('#product-detail-mobile input:radio[name=type]:first').attr('checked', true);
      setTimeout(function() {
        $("#cartModal .type input:radio[name=type]:first").prop(
          "checked",
          true
        );
      }, 500);
      $(".modifier input:checkbox[name=modifier]").prop("checked", false);
      that.state.ex.TotalAmount = that.state.ex.ItemProduct.Skus[0].Price;
      that.state.ex.Note = "";
      that.state.ex.Quantity = 1;
      $("#cartModal").modal("show");
      that.setState(that.state);
    }
    that.setState(that.state);
  }
  Increase() {
    var that = this;

    if (that.state.ex.Modifier === null) {
      alertify.alert("Error", "Please wait for loading data");
      return -1;
    }
    if (!that.state.ex.ItemProduct) {
      alertify.alert("Error", "Data is not found");
      return -1;
    }
    var item = that.state.ex.ItemProduct.Skus.find(e => {
      return e.Id == $("input:radio[name=type]:checked").val();
    });
    that.state.ex.SkuPrice = item.Price;
    that.state.ex.Quantity = $(
      "#quantity-value-" + that.state.ex.ItemProduct.PosId + ""
    ).val();
    if (
      that.state.ex.Quantity ==
      $("#quantity-value-" + that.state.ex.ItemProduct.PosId + "").val()
    ) {
      that.state.ex.Quantity = parseInt(that.state.ex.Quantity) + 1;
      that.setState(that.state);
    }
    $("#quantity-value-" + that.state.ex.ItemProduct.PosId + "").val(
      that.state.ex.Quantity
    );
    that.state.ex.SkuPrice = item.Price;
    that.state.ex.Total =
      parseInt(that.state.ex.Quantity) * that.state.ex.SkuPrice;
    var IsCheckedModifier = [];
    $("input:checkbox[name=modifier]:checked").each(function() {
      IsCheckedModifier.push($(this).val());
    });
    var price_modifier = 0;
    IsCheckedModifier.forEach(function(e) {
      price_modifier += parseFloat(e);
    });
    that.state.ex.TotalAmount = that.state.ex.Total + price_modifier;
    that.setState(that.state);
  }
  Decrease() {
    var that = this;

    if (that.state.ex.Modifier === null) {
      alertify.alert("Error", "Please wait for loading data");
      return -1;
    }
    if (!that.state.ex.ItemProduct) {
      alertify.alert("Error", "Data is not found");
      return -1;
    }
    var item = that.state.ex.ItemProduct.Skus.find(e => {
      return e.Id == $("input:radio[name=type]:checked").val();
    });
    that.state.ex.Quantity = $(
      "#quantity-value-" + that.state.ex.ItemProduct.PosId + ""
    ).val();
    if (that.state.ex.Quantity == 1) {
      item = null;
      $("input:radio[name=type]:checked").attr("checked", false);
      $("#cartModal").modal("hide");
      $("#product-detail-mobile").removeClass("showmenu");
      setTimeout(function() {
        that.state.ex.ItemProduct = null;
        that.setState(that.state);
      }, 300);
    }
    if (item != null) {
      that.state.ex.SkuPrice = item.Price;
    } else {
      that.state.ex.SkuPrice = 0;
    }
    if (
      that.state.ex.Quantity ==
      $("#quantity-value-" + that.state.ex.ItemProduct.PosId + "").val()
    ) {
      that.state.ex.Quantity = parseInt(that.state.ex.Quantity) - 1;
      that.setState(that.state);
    }
    that.state.ex.Total =
      parseInt(that.state.ex.Quantity) * that.state.ex.SkuPrice;
    var IsCheckedModifier = [];
    $("input:checkbox[name=modifier]:checked").each(function() {
      IsCheckedModifier.push($(this).val());
    });
    var price_modifier = 0;
    IsCheckedModifier.forEach(function(e) {
      price_modifier += parseFloat(e);
    });
    that.state.ex.TotalAmount = that.state.ex.Total + price_modifier;
    that.setState(that.state);
  }
  SelectType() {
    var that = this;

    if (that.state.ex.Modifier === null) {
      alertify.alert("Error", "Please wait for loading data");
      return -1;
    }
    if (!that.state.ex.ItemProduct) {
      alertify.alert("Error", "Data is not found");
      return -1;
    }
    var item = that.state.ex.ItemProduct.Skus.find(e => {
      return e.Id == $("input:radio[name=type]:checked").val();
    });

    that.state.ex.SkuPrice = item.Price;
    that.state.ex.Quantity = $(
      "#quantity-value-" + that.state.ex.ItemProduct.PosId + ""
    ).val();
    that.state.ex.Total =
      parseInt(that.state.ex.Quantity) * that.state.ex.SkuPrice;
    var IsCheckedModifier = [];
    $("input:checkbox[name=modifier]:checked").each(function() {
      IsCheckedModifier.push($(this).val());
    });
    var price_modifier = 0;
    IsCheckedModifier.forEach(function(e) {
      price_modifier += parseFloat(e);
    });
    that.state.ex.TotalAmount = that.state.ex.Total + price_modifier;
    that.setState(that.state);
  }
  selectModifier() {
    var that = this;
    if (that.state.ex.Modifier === null) {
      alertify.alert("Error", "Please wait for loading data");
      return -1;
    }
    if (!that.state.ex.ItemProduct) {
      alertify.alert("Error", "Data is not found");
      return -1;
    }
    var item = that.state.ex.ItemProduct.Skus.find(e => {
      return e.Id == $("input:radio[name=type]:checked").val();
    });
    that.state.ex.SkuPrice = item.Price;
    that.state.ex.Quantity = $(
      "#quantity-value-" + that.state.ex.ItemProduct.PosId + ""
    ).val();
    that.state.ex.Total =
      parseInt(that.state.ex.Quantity) * that.state.ex.SkuPrice;
    var IsCheckedModifier = [];
    $("input:checkbox[name=modifier]:checked").each(function() {
      IsCheckedModifier.push($(this).val());
    });
    var price_modifier = 0;
    IsCheckedModifier.forEach(function(e) {
      price_modifier += parseFloat(e);
    });
    that.state.ex.TotalAmount = that.state.ex.Total + price_modifier;
    that.setState(that.state);
  }
  isEqualArray(arr1, arr2) {
    // if length is not equal
    if (arr1.length != arr2.length) return false;
    else {
      // comapring each element of array
      for (var i = 0; i < arr1.length; i++)
        if (arr1[i].Id != arr2[i].Id) return false;
      return true;
    }
  }
  AddtoCart() {
    var that = this;
    if (that.state.ex.Quantity == 0) {
      alertify.alert("Error", "Please select ");
      return -1;
    }
    if (that.state.ex.Modifier === null) {
      alertify.alert("Error", "Please wait for loading data");
      return -1;
    }

    var selectedModifiers = [];

    var IsCheckedModifier = [];
    $("input:checkbox[name=modifier]:checked").each(function() {
      IsCheckedModifier.push($(this).data("modi"));
    });
    var item = that.state.ex.ItemProduct.Skus.find(e => {
      return e.Id == $("input:radio[name=type]:checked").val();
    });
    if (item == null) {
      alertify.alert("Error", "Please select type");
      return -1;
    }
    IsCheckedModifier.forEach(function(e) {
      var modiitem = that.state.ex.Modifier.find(function(modi) {
        return modi.Id === parseInt(e);
      });
      if (modiitem) {
        selectedModifiers.push(modiitem);
      }
    });
    // tim cac item co trong gio hang
    var old_product = that.state.model.OrderItems.filter(e => {
      return e.SkuId == item.Id;
    });
    //Neu co
    var item_product = null;
    var IsCheck = false;
    if (old_product != null) {
      var old_product2 = old_product.filter(e => {
        return e.ModifierItems.length == IsCheckedModifier.length;
      });
      if (old_product2 != null) {
        old_product2.forEach(e => {
          var check = this.isEqualArray(e.ModifierItems, selectedModifiers);
          if (check == true) {
            IsCheck = true;
            item_product = e;
          }
        });
      }
    }
    if (IsCheck == false) {
      $("#loading").show();
      $.ajax({
        url: "/gposorder/addtocartgpos",
        type: "POST",
        dataType: "json",
        contentType: "application/json",
        data: JSON.stringify({
          skuId: item.Id,
          quantity: that.state.ex.Quantity,
          modifierList: selectedModifiers,
          note: $("#note-" + that.state.ex.ItemProduct.PosId).val()
        }),
        success: response => {
          $("#loading").hide();
          if (response.status == "success") {
            $("#cartModal").modal("hide");
            $("#notiModal").modal("toggle");
            $(".cart-count-txt").text(response.data.NewCart.OrderItems.length);
            $("p.count").text(response.data.NewCart.OrderItems.length);
            $(".cart-money").text(
              "$" + response.data.NewCart.TotalAmount.toFixed(2)
            );
            $("#info-modal .skuinput").val(response.data.NewItemId);
            $("#product-detail-mobile").removeClass("showmenu");
            //  $("#viewCartModal .modal-body .nc-table-cart tr td.subtotal").text("$" + response.data.NewCart.SubTotalAmount.toFixed(2))
            this.state.model = response.data.NewCart;
            setTimeout(function() {
              that.state.ex.ItemProduct = null;
              that.setState(that.state);
            }, 300);
            this.setState(this.state);
          } else {
            alertify.alert("Error", response.message);
          }
        },
        error: function(er) {
          $("#loading").hide();
          alertify.alert("Error", "Error");
        }
      });
    } else {
      $("#loading").show();
      $.ajax({
        url: "/gposorder/updatequantity",
        type: "POST",
        dataType: "json",
        contentType: "application/json",
        data: JSON.stringify({
          id: item_product.Id,
          quantity:
            parseInt(item_product.Quantity) + parseInt(that.state.ex.Quantity)
        }),
        success: response => {
          $("#loading").hide();
          if (response.status == "success") {
            $("#cartModal").modal("hide");
            $("#notiModal").modal("toggle");
            $(".cart-money").text("$" + response.data.TotalAmount.toFixed(2));
            $("#info-modal .skuinput").val(response.data.NewItemId);
            $("#product-detail-mobile").removeClass("showmenu");
            that.state.model = response.data;
            setTimeout(function() {
              that.state.ex.ItemProduct = null;
              that.setState(that.state);
            }, 300);
            that.setState(that.state);
          } else {
            alertify.alert("Error", response.message);
          }
        },
        error: function(er) {
          $("#loading").hide();
          alertify.alert("Error", "Error");
        }
      });
    }
  }
  GetProductDetail(id) {
    var that = this;
    if (that.state.ex.Modifier === null) {
      alertify.alert("Error", "Please wait for loading data");
      return -1;
    }
    that.state.ex.ItemProduct = that.state.ex.AllProduct.find(function(e) {
      return e.PosId == id;
    });
    if (!that.state.ex.ItemProduct) {
      alertify.alert("Error", "Data is not found");
      return -1;
    }

    if (that.state.ex.ItemProduct.Skus.length > 0) {
      $("#product-detail-mobile").addClass("showmenu");

      setTimeout(function() {
        $("#product-detail-mobile .type input:radio[name=type]:first").prop(
          "checked",
          true
        );
        $(".modifier input:checkbox[name=modifier]").prop("checked", false);
      }, 100);
      that.state.ex.TotalAmount = that.state.ex.ItemProduct.Skus[0].Price;
      that.state.ex.Note = "";
      that.state.ex.Quantity = 1;
      that.setState(that.state);
    }
    that.setState(that.state);
  }
  Remove() {
    $("#product-detail-mobile").removeClass("showmenu");
    var that = this;
    setTimeout(function() {
      that.state.ex.ItemProduct = null;
      that.setState(that.state);
    }, 300);
  }
  RefreshViewCart(e) {
    this.state.model = e;
    this.setState(this.state);
  }
  componentWillMount() {
    var that = this;
  }
  getCart() {
    var that = this;
    $("#loading").show();
    $.get("/gposorder/getcart", function(response) {
      $("#loading").hide();
      that.state.model = response;
      that.setState(that.state);
    }).fail(function() {
      alertify.alert("Error", "Data is not found");
    });
  }
  getProductByCategoryId() {
    var that = this;
    $("#loading").show();
    $.get(
      "/api/gposproducts?categoryid=" + that.props.match.params.id,
      function(response) {
        $("#loading").hide();
        that.state.ex.AllProduct = response;
        that.setState(that.state);
        that.getModifierByCategoryId();
      }
    ).fail(function() {
      alertify.alert("DATA Not found");
    });
  }
  getModifierByCategoryId() {
    var that = this;
    $.get(
      "/api/gposproductcategories/modifiers?categoryid=" +
        that.props.match.params.id,
      function(res) {
        that.state.ex.Modifier = res;
        that.setState(that.state);
      }
    );
  }
  componentDidMount() {
    var that = this;

    setTimeout(function() {
      $(
        "ul.nav-sidebar li[data-id=" + that.props.match.params.id + "]"
      ).addClass("active");
    }, 300);
  }
  render() {
    return (
      <React.Fragment>
        {this.state.ex && this.state.model ? (
          <React.Fragment>
            <main id="product-page">
              <input type="hidden" id="cateid" value="@category.Id" />
              <section id="product-category" className="hidden-xs hidden-sm">
                <div className="nc-section-header">
                  <div className="container">
                    <div className="row">
                      <div className="col-md-3">
                        <div className="nc-title-category">
                          <h5 className="h4">Choose a category</h5>
                        </div>
                      </div>
                      <div className="col-md-9">
                        <h3 className="title text-center">
                          {this.state.ex.Title}
                        </h3>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="nc-section-body">
                  <div className="container flex-container clearfix">
                    <div className="nc-col-left">
                      <CategoryList
                        CategoryList={this.state.ex.CategoryList}
                        Id={this.props.match.params.id}
                      />
                    </div>
                    <div className="nc-col-right">
                      <div id="nc-product-list">
                        <div className="nc-product-res-list">
                          {this.state.ex.AllProduct
                            ? this.state.ex.AllProduct.map(c => {
                                return (
                                  <div
                                    className="nc-item-product flex-container"
                                    data-id={c.PosId}
                                  >
                                    <div className="nc-ava">
                                      <img
                                        className="avatar"
                                        src={c.Avatar}
                                        alt={c.Name}
                                      />
                                    </div>
                                    <div className="nc-info">
                                      <div className="nc-title">
                                        <h4 className="h5">{c.Name}</h4>
                                      </div>
                                      <div className="nc-des">
                                        <div
                                          dangerouslySetInnerHTML={{
                                            __html: c.Description
                                          }}
                                        />
                                      </div>
                                    </div>
                                    <div className="nc-price">
                                      <span>
                                        ${c.Skus[0].Price
                                          ? c.Skus[0].Price.toFixed(2)
                                          : "0.00"}
                                      </span>
                                    </div>
                                    <div className="nc-button">
                                      <a
                                        href="javascript:void(0)"
                                        className="btn"
                                        onClick={e =>
                                          this.OrderNowPopup(c.PosId)
                                        }
                                        data-proid={c.PosId}
                                      >
                                        Order Now
                                      </a>
                                    </div>
                                  </div>
                                );
                              })
                            : ""}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="nc-section-footer" />
              </section>
              <section
                id="product-category-mobile"
                className="visible-xs visible-sm"
              >
                <div className="nc-section-header" />
                <div className="nc-section-body">
                  <div className="container flex-container col-3item">
                    {this.state.ex.AllProduct
                      ? this.state.ex.AllProduct.map(c => {
                          return (
                            <div className="nc-item-product" data-id={c.PosId}>
                              <a
                                href="javascript:;"
                                onClick={e => this.GetProductDetail(c.PosId)}
                              >
                                <div className="nc-ava">
                                  <img
                                    className="avatar"
                                    src={c.Avatar}
                                    alt={c.Name}
                                  />
                                </div>
                                <div className="nc-name">
                                  <span>{c.Name}</span>
                                </div>
                                <div className="nc-price">
                                  <span>
                                    ${c.Skus[0].Price
                                      ? c.Skus[0].Price.toFixed(2)
                                      : "0.00"}
                                  </span>
                                </div>
                              </a>
                            </div>
                          );
                        })
                      : ""}
                  </div>
                </div>
                <div className="nc-section-footer">
                  {this.state.model.OrderItems.length > 0 ? (
                    <div className="view-cart clearfix">
                      <div className="col-sm-6 col-xs-6">
                        {this.state.ex.ItemProduct != null ? (
                          <React.Fragment>
                            <div className="total clearfix">
                              <div className="total-number">
                                <span>
                                  ${this.state.ex.TotalAmount
                                    ? this.state.ex.TotalAmount.toFixed(2)
                                    : "0.00"}
                                </span>
                              </div>
                            </div>
                          </React.Fragment>
                        ) : (
                          <React.Fragment>
                            <div className="total clearfix">
                              <div className="count">
                                <span className="demo-icon ecs-basket" />
                                <span className="count-number">
                                  {this.state.model.OrderItems.length}
                                </span>
                              </div>
                              <div className="total-number">
                                <span>
                                  ${this.state.model.TotalAmount
                                    ? this.state.model.TotalAmount.toFixed(2)
                                    : "0.00"}
                                </span>
                              </div>
                            </div>
                          </React.Fragment>
                        )}
                      </div>
                      <div className="col-sm-6 col-xs-6">
                        <div className="nc-button-view">
                          {this.state.ex.ItemProduct != null ? (
                            <React.Fragment>
                              <a
                                href="javascript:;"
                                className="btn"
                                onClick={e => this.AddtoCart()}
                              >
                                Add to cart
                              </a>
                            </React.Fragment>
                          ) : (
                            <React.Fragment>
                              <a
                                href="/gposshoppingcart/vieworder"
                                className="btn"
                              >
                                View cart
                              </a>
                            </React.Fragment>
                          )}
                        </div>
                      </div>
                    </div>
                  ) : this.state.ex.ItemProduct != null ? (
                    <div className="view-cart clearfix">
                      <div className="col-sm-6 col-xs-6">
                        <div className="total clearfix">
                          <div className="total-number">
                            <span>
                              ${this.state.ex.TotalAmount
                                ? this.state.ex.TotalAmount.toFixed(2)
                                : "0.00"}
                            </span>
                          </div>
                        </div>
                      </div>
                      <div className="col-sm-6 col-xs-6">
                        <div className="nc-button-view">
                          <a
                            href="javascript:;"
                            className="btn"
                            onClick={e => this.AddtoCart()}
                          >
                            Add to cart
                          </a>
                        </div>
                      </div>
                    </div>
                  ) : (
                    ""
                  )}
                </div>
              </section>
              <section
                id="product-detail-mobile"
                className="visible-xs visible-sm"
              >
                {this.state.ex.ItemProduct != null ? (
                  <React.Fragment>
                    <div className="nc-section-header">
                      <div className="container">
                        <div className="row">
                          <div className="col-md-12">
                            {" "}
                            <a
                              href="javascript:;"
                              className="remove-class"
                              onClick={e => this.Remove()}
                            >
                              {" "}
                              <span className="demo-icon ecs-angle-double-left" />{" "}
                              Back
                            </a>
                          </div>
                          <div className="col-md-12">
                            <div className="nc-information-product clearfix">
                              <div className="nc-ava">
                                <img
                                  src={
                                    this.state.ex.ItemProduct.Avatar
                                      ? this.state.ex.ItemProduct.Avatar
                                      : ""
                                  }
                                  alt=""
                                />
                              </div>
                              <div className="nc-info">
                                <div className="nc-name">
                                  {this.state.ex.ItemProduct.Name}
                                </div>
                                <div className="nc-action flex-container">
                                  <div className="nc-price">
                                    ${this.state.ex.ItemProduct.Skus[0].Price
                                      ? this.state.ex.ItemProduct.Skus[0].Price.toFixed(
                                          2
                                        )
                                      : "0.00"}
                                  </div>
                                  <div className="nc-quantity">
                                    <div className="nc-quantity-container">
                                      <div className="nc-divide">
                                        <div className="nc-divide-inner">
                                          <a
                                            href="javascript:;"
                                            onClick={e => this.Decrease()}
                                          >
                                            -
                                          </a>
                                        </div>
                                      </div>
                                      <div className="nc-input">
                                        <input
                                          disabled="disabled"
                                          type="text"
                                          id={`quantity-value-${
                                            this.state.ex.ItemProduct.PosId
                                          }`}
                                          value={this.state.ex.Quantity}
                                        />
                                      </div>
                                      <div className="nc-increase">
                                        <div className="nc-increase-inner">
                                          <a
                                            href="javascript:;"
                                            onClick={e => this.Increase()}
                                          >
                                            +
                                          </a>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div className="nc-des-container">
                              <div
                                dangerouslySetInnerHTML={{
                                  __html: this.state.ex.ItemProduct.Description
                                }}
                              />
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="nc-section-body">
                      <div className="container">
                        <div className="row">
                          <div className="col-md-12">
                            <div className="list-select-option">
                              <div className="nc-list-option type">
                                <h3 className="h4 nc-name">Select Type</h3>
                                {this.state.ex.ItemProduct.Skus
                                  ? this.state.ex.ItemProduct.Skus.map(c => {
                                      return (
                                        <React.Fragment>
                                          <div className="nc-item clearfix">
                                            <div className="nc-item-left">
                                              <div className="nc-name-item">
                                                <span>{c.Name}</span>
                                              </div>
                                              <div className="nc-price-item">
                                                <span>
                                                  ${c.Price
                                                    ? c.Price.toFixed(2)
                                                    : "0.00"}
                                                </span>
                                              </div>
                                            </div>
                                            <div className="nc-item-right">
                                              <div className="nc-button">
                                                <label className="nc-button-container">
                                                  <input
                                                    type="radio"
                                                    name="type"
                                                    value={c.Id}
                                                    id={`type-${c.Id}`}
                                                    data-type={`${c.Id}`}
                                                    data-proid={`${
                                                      this.state.ex.ItemProduct
                                                        .Id
                                                    }`}
                                                    onClick={e =>
                                                      this.SelectType()
                                                    }
                                                  />
                                                  <span className="checkmark" />
                                                </label>
                                              </div>
                                            </div>
                                          </div>
                                        </React.Fragment>
                                      );
                                    })
                                  : ""}
                              </div>
                              <div className="nc-list-option modifier">
                                <h3 className="h4 nc-name">Modifiers</h3>

                                {this.state.ex.Modifier
                                  ? this.state.ex.Modifier.map(c => {
                                      return (
                                        <React.Fragment>
                                          <div className="nc-item clearfix">
                                            <div className="nc-item-left">
                                              <div className="nc-name-item">
                                                <span>{c.Name}</span>
                                              </div>
                                              <div className="nc-price-item">
                                                <span>
                                                  ${c.Price
                                                    ? c.Price.toFixed(2)
                                                    : "0.00"}
                                                </span>
                                              </div>
                                            </div>
                                            <div className="nc-item-right">
                                              <div className="nc-button">
                                                <label className="nc-button-container">
                                                  <input
                                                    type="checkbox"
                                                    name="modifier"
                                                    value={c.Price}
                                                    id={`modi-${c.Id}`}
                                                    data-modi={`${c.Id}`}
                                                    data-proid={`${
                                                      this.state.ex.ItemProduct
                                                        .Id
                                                    }`}
                                                    onClick={e =>
                                                      this.selectModifier()
                                                    }
                                                  />
                                                  <span className="checkmark" />
                                                </label>
                                              </div>
                                            </div>
                                          </div>
                                        </React.Fragment>
                                      );
                                    })
                                  : ""}
                              </div>
                            </div>
                            <div className="nc-note-container">
                              {/*
                                                        <textarea
                                name=""
                                id={`note-${this.state.ex.ItemProduct.PosId}`}
                                cols="30"
                                rows="10"
                                value={this.state.ex.Note}
                                placeholder="Add special request"
                                onChange={e => {
                                  this.state.ex.Note = e.target.value;
                                  this.setState(this.state);
                                }}
                              />*/}
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </React.Fragment>
                ) : (
                  ""
                )}
              </section>
            </main>
            <ButtonViewCart
              model={this.state.model}
              onChange={e => this.RefreshViewCart(e)}
            />
          </React.Fragment>
        ) : (
          ""
        )}
        <div
          id="cartModal"
          className="modal fade"
          tabIndex="-1"
          role="dialog"
          aria-labelledby="cartModalLabel"
          aria-hidden="true"
        >
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              {this.state.ex.ItemProduct ? (
                <React.Fragment>
                  <div className="modal-header">
                    <div className="item-product clearfix">
                      <div className="nc-ava">
                        <img
                          src={
                            this.state.ex.ItemProduct.Avatar
                              ? this.state.ex.ItemProduct.Avatar
                              : ""
                          }
                          alt=""
                        />
                      </div>
                      <div className="nc-info">
                        <div className="nc-title">
                          <span>{this.state.ex.ItemProduct.Name}</span>
                        </div>
                        <div className="nc-price">
                          <span>
                            ${this.state.ex.ItemProduct.Skus[0].Price
                              ? this.state.ex.ItemProduct.Skus[0].Price.toFixed(
                                  2
                                )
                              : "0.00"}
                          </span>
                        </div>
                      </div>
                      <div className="nc-quantity">
                        <div className="nc-quantity-container clearfix">
                          <div className="nc-divide">
                            <div className="nc-divide-inner">
                              <a
                                href="javascript:;"
                                onClick={e => this.Decrease()}
                              >
                                -
                              </a>
                            </div>
                          </div>
                          <div className="nc-input">
                            <input
                              disabled="disabled"
                              type="text"
                              id={`quantity-value-${
                                this.state.ex.ItemProduct.PosId
                              }`}
                              value={this.state.ex.Quantity}
                            />
                          </div>
                          <div className="nc-increase">
                            <div className="nc-increase-inner">
                              <a
                                href="javascript:;"
                                onClick={e => this.Increase()}
                              >
                                +
                              </a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="modal-body">
                    <div className="type">
                      <div className="nc-option-type">
                        <div className="label">
                          <span>Select type</span>
                        </div>
                        {this.state.ex.ItemProduct.Skus
                          ? this.state.ex.ItemProduct.Skus.map(c => {
                              return (
                                <div className="nc-option-list">
                                  <div className="nc-item-option clearfix">
                                    <div className="nc-title">
                                      <span>{c.Name}</span>
                                    </div>
                                    <div className="nc-price">
                                      <span>
                                        ${c.Price ? c.Price.toFixed(2) : "0.00"}
                                      </span>
                                    </div>
                                    <div className="nc-button">
                                      <label className="nc-button-container">
                                        <input
                                          type="radio"
                                          name="type"
                                          value={c.Id}
                                          id={`type-${c.Id}`}
                                          data-type={`${c.Id}`}
                                          data-proid={`${
                                            this.state.ex.ItemProduct.Id
                                          }`}
                                          onClick={e => this.SelectType()}
                                        />
                                        <span className="checkmark" />
                                      </label>
                                    </div>
                                  </div>
                                </div>
                              );
                            })
                          : ""}
                      </div>
                    </div>
                    <div className="nc-option-type modifier">
                      <div className="label">
                        <span>Modifiers</span>
                      </div>
                      <div className="nc-option-list">
                        {this.state.ex.Modifier
                          ? this.state.ex.Modifier.map(c => {
                              return (
                                <div className="nc-item-option clearfix">
                                  <div className="nc-title">
                                    <span>{c.Name}</span>
                                  </div>
                                  <div className="nc-price">
                                    <span>
                                      ${c.Price ? c.Price.toFixed(2) : "0.00"}
                                    </span>
                                  </div>
                                  <div className="nc-button">
                                    <label className="nc-button-container">
                                      <input
                                        type="checkbox"
                                        name="modifier"
                                        value={c.Price}
                                        id={`modi-${c.Id}`}
                                        data-modi={`${c.Id}`}
                                        data-proid={`${
                                          this.state.ex.ItemProduct.Id
                                        }`}
                                        onClick={e => this.selectModifier()}
                                      />
                                      <span className="checkmark" />
                                    </label>
                                  </div>
                                </div>
                              );
                            })
                          : ""}
                      </div>
                    </div>
                  </div>
                  <div className="modal-footer ">
                    <div className="nc-section-body">
                      <div className="note">
                        {/*
                                            <textarea
                                                name=""
                                                id={`note-${this.state.ex.ItemProduct.PosId}`}
                                                cols="30"
                                                rows="10"
                                                value={this.state.ex.Note}
                                                placeholder="Add special request"
                                                onChange={
                                                    e => {
                                                        this.state.ex.Note = e.target.value;
                                                        this.setState(this.state);
                                                    }
                                                }
                                            >
                                            </textarea>*/}
                      </div>
                    </div>
                    <div className="nc-section-footer clearfix">
                      <div className="total">
                        <span>
                          ${this.state.ex.TotalAmount
                            ? this.state.ex.TotalAmount.toFixed(2)
                            : "0.00"}
                        </span>
                      </div>
                      <div className="nc-button">
                        <a
                          href="javascript:;"
                          className="btn btn-primary"
                          onClick={e => this.AddtoCart()}
                          data-proid
                        >
                          Add to cart
                        </a>
                      </div>
                    </div>
                  </div>
                </React.Fragment>
              ) : (
                ""
              )}
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default CategoryDetail;
