import React, { Component } from "react";
import { Link } from "react-router-dom";
import moment from "moment";
import ButtonViewCart from "./buttonpopup";

class OrderInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      model: null,
      ex: {
        Tips: 0,
        DateList: [],
        SelectHour: null,
        SelectMinute: null,
        PaymentLink: null,
        MxPaymentType: 0
      }
    };
    this.hourlistselect = React.createRef();
    this.minutelistselect = React.createRef();
    var that = this;
    document.title = "Order Infomation";
    $("#loading").show();

    $.get("/gposorder/GetAvailableDate", response => {
      that.state.ex.DateList = response;
      that.setState(that.state);
    });
    $.get("/gposorder/GetPaymentLink", response => {
      that.state.ex.PaymentLink = response;
      that.setState(that.state);
      return 0;
    });
    $.get("/gposorder/GetMxMerchantPaymentType", response => {
      that.state.ex.MxPaymentType = response;
      that.setState(that.state);
      return 0;
    });
    $.ajax({
      url: "/gposorder/getcart",
      type: "GET",
      dataType: "json",
      contentType: "application/json",
      success: response => {
        $("#loading").hide();
        if (!response || response.OrderItems.length === 0) {
          that.props.history.push("/gposshoppingcart");
          return false;
        }
        that.state.model = response;
        that.state.ex.Tips = that.state.model.Tips;
        that.state.ex.SelectHour = that.state.model.OrderGPosInformation
          .OrderDateTime
          ? moment
              .utc(that.state.model.OrderGPosInformation.OrderDateTime)
              .hour()
          : null;

        that.state.ex.SelectMinute = that.state.model.OrderGPosInformation
          .OrderDateTime
          ? moment
              .utc(that.state.model.OrderGPosInformation.OrderDateTime)
              .minute()
          : 0;
        that.setState(that.state);
      },
      error: function(er) {
        $("#loading").hide();
      }
    });
  }

  /**Render dropdown list giờ */
  drawhourlist() {
    var that = this;
    var storeOrderDateTime = that.state.model.OrderGPosInformation.OrderDateTime
      ? moment.utc(that.state.model.OrderGPosInformation.OrderDateTime)
      : null;
    var pickdate = that.state.ex.DateList.find(e => {
      return (
        storeOrderDateTime &&
        moment.utc(e.Day).isSame(storeOrderDateTime, "day")
      );
    });
    if (!pickdate) {
      return (
        <select className="form-control" value={null}>
          <option value={null}>HH</option>
        </select>
      );
    }

    var result = that.groupBy(pickdate.HourList, function(item) {
      return [item.Hour];
    });

    return (
      <select
        className="form-control"
        value={that.state.ex.SelectHour}
        ref={that.hourlistselect}
        onChange={e => {
          that.onChangeHour(e);
        }}
      >
        <option value={null}>HH</option>
        {result.map(e => {
          return that.showhourlistampm(e[0].Hour);
        })}
      </select>
    );
  }

  showhourlistampm(hour) {
    if (hour <= 11) {
      return <option value={hour}>{hour} AM</option>;
    } else if (hour == 12) {
      return <option value={hour}>{hour} PM</option>;
    } else {
      return <option value={hour}>{hour - 12} PM</option>;
    }
  }

  /**Render dropdown list phút theo giờ */
  drawminutelist() {
    var that = this;
    var storeOrderDateTime = that.state.model.OrderGPosInformation.OrderDateTime
      ? moment.utc(that.state.model.OrderGPosInformation.OrderDateTime)
      : null;
    if (!that.state.ex.SelectHour) {
      return (
        <select className="form-control" value={null}>
          <option value={null}>0</option>
        </select>
      );
    }

    var pickdate = that.state.ex.DateList.find(e => {
      return (
        storeOrderDateTime &&
        moment.utc(e.Day).isSame(storeOrderDateTime, "day")
      );
    });
    if (!pickdate) {
      return (
        <select className="form-control" value={null}>
          <option value={0}>0</option>
        </select>
      );
    }

    var result = that.groupBy(pickdate.HourList, function(item) {
      return [item.Hour];
    });

    var minlst = result.find(e => {
      return e[0].Hour == that.state.ex.SelectHour;
    });

    if (!minlst || minlst.length == 0) {
      return (
        <select className="form-control" value={null}>
          <option value={0}>0</option>
        </select>
      );
    }

    return (
      <select
        className="form-control"
        value={that.state.ex.SelectMinute}
        ref={that.minutelistselect}
        onChange={e => {
          if (!e.target.value || isNaN(e.target.value)) {
            that.state.model.OrderGPosInformation.OrderDateTime = storeOrderDateTime
              .startOf("hour")
              .toDate();
            that.state.ex.SelectMinute = 0;
          } else {
            that.state.model.OrderGPosInformation.OrderDateTime = storeOrderDateTime
              .set("minute", parseInt(e.target.value))
              .toDate();
            that.state.ex.SelectMinute = parseInt(e.target.value);
          }
          that.setState(that.state);
        }}
      >
        {minlst.map(e => {
          return <option value={e.Min}>{e.Min}</option>;
        })}
      </select>
    );
  }

  groupBy(array, f) {
    var groups = {};
    array.forEach(function(o) {
      var group = JSON.stringify(f(o));
      groups[group] = groups[group] || [];
      groups[group].push(o);
    });
    return Object.keys(groups).map(function(group) {
      return groups[group];
    });
  }

  onChangeHour(e) {
    var that = this;
    var storeOrderDateTime = that.state.model.OrderGPosInformation.OrderDateTime
      ? moment.utc(that.state.model.OrderGPosInformation.OrderDateTime)
      : null;
    if (!storeOrderDateTime) {
      return false;
    }

    var pickdate = that.state.ex.DateList.find(e => {
      return (
        storeOrderDateTime &&
        moment.utc(e.Day).isSame(storeOrderDateTime, "day")
      );
    });
    if (!pickdate) {
      return false;
    }

    var result = that.groupBy(pickdate.HourList, function(item) {
      return [item.Hour];
    });

    if (!e.target.value || isNaN(e.target.value)) {
      that.state.model.OrderGPosInformation.OrderDateTime = storeOrderDateTime
        .startOf("day")
        .toDate();
      that.state.ex.SelectHour = null;
    } else {
      that.state.model.OrderGPosInformation.OrderDateTime = storeOrderDateTime
        .set("hour", parseInt(e.target.value))
        .toDate();
      that.state.ex.SelectHour = parseInt(e.target.value);
    }
    var minlst = result.find(e => {
      return e[0].Hour == that.state.ex.SelectHour;
    });
    if (!minlst) {
      that.state.model.OrderGPosInformation.OrderDateTime = storeOrderDateTime
        .startOf("hour")
        .toDate();
      that.state.ex.SelectMinute = 0;
    } else {
      that.state.model.OrderGPosInformation.OrderDateTime = storeOrderDateTime
        .set("minute", minlst[0].Min)
        .toDate();
      that.state.ex.SelectMinute = minlst[0].Min;
    }
    that.setState(that.state);
    return true;
  }

  applyCoupon() {
    var that = this;
    $("#loading").show();
    $.ajax({
      url: "/gposorder/applycoupon",
      type: "POST",
      dataType: "json",
      contentType: "application/json",
      data: JSON.stringify(that.state.model.CouponCode),
      success: response => {
        $("#loading").hide();
        if (response.status == "success") {
          alertify.alert("Success", "Promotion code is actived", function() {
            var pickupdatetemp =
              that.state.model.OrderGPosInformation.OrderDateTime;
            that.state.model = response.data;
            that.state.model.OrderGPosInformation.OrderDateTime = pickupdatetemp;
            $(".cart-container .cart-count-txt").text(
              that.state.model.OrderItems.length
            );
            $(".cart-container .cart-money").text(
              "$" + that.state.model.TotalAmount.toFixed(2)
            );
            that.setState(that.state);
          });
        } else {
          alertify.alert("Error", response.message);
        }
      },
      error: function(er) {
        $("#loading").hide();
        alertify.alert(
          "Error",
          "Opps! System has some error. Please try again !"
        );
      }
    });
  }

  removeCoupon() {
    var that = this;
    $("#loading").show();
    $.ajax({
      url: "/gposorder/clearcoupon",
      type: "POST",
      dataType: "json",
      contentType: "application/json",
      success: response => {
        $("#loading").hide();
        if (response.status == "success") {
          alertify.alert("Success", "Promotion code is removed", function() {
            var pickupdatetemp =
              that.state.model.OrderGPosInformation.OrderDateTime;
            that.state.model = response.data;
            that.state.model.CouponCode = "";
            that.state.model.OrderGPosInformation.OrderDateTime = pickupdatetemp;
            $(".cart-container .cart-count-txt").text(
              that.state.model.OrderItems.length
            );
            $(".cart-container .cart-money").text(
              "$" + that.state.model.TotalAmount.toFixed(2)
            );
            that.setState(that.state);
          });
        } else {
          alertify.alert("Error", response.message);
        }
      },
      error: function(er) {
        $("#loading").hide();
        alertify.alert(
          "Error",
          "Opps! System has some error. Please try again !"
        );
      }
    });
  }

  updateTips(am) {
    var that = this;
    $("#loading").show();
    $.ajax({
      url: "/gposorder/updatetips",
      type: "POST",
      dataType: "json",
      contentType: "application/json",
      data: JSON.stringify(am && !isNaN(am) ? Number(am) : 0),
      success: response => {
        $("#loading").hide();
        if (response.status == "success") {
          alertify.alert("Success", "Tips is updated", function() {
            var pickupdatetemp =
              that.state.model.OrderGPosInformation.OrderDateTime;
            that.state.model = response.data;
            that.state.model.OrderGPosInformation.OrderDateTime = pickupdatetemp;
            $(".cart-container .cart-count-txt").text(
              that.state.model.OrderItems.length
            );
            $(".cart-container .cart-money").text(
              "$" + that.state.model.TotalAmount.toFixed(2)
            );
            that.setState(that.state);
          });
        } else {
          alertify.alert("Error", response.message);
        }
      },
      error: function(er) {
        $("#loading").hide();
        alertify.alert(
          "Error",
          "Opps! System has some error. Please try again !"
        );
      }
    });
  }

  gotocheckout() {
    if (!this.state.model) {
      alertify.alert(
        "Error",
        "Your shopping cart is expired, please try again"
      );
      return false;
    }
    if (this.state.model.OrderItems.length === 0) {
      alertify.alert("Error", "Your shopping cart is empty, please try again");
      return false;
    }

    if (!this.state.model.OrderGPosInformation.OrderDateTime) {
      alertify.alert("Error", "Please select pick-up date");
      return false;
    }
    if (!this.state.ex.SelectHour) {
      alertify.alert("Error", "Please select pick-up hour");
      return false;
    }

    var that = this;
    $("#loading").show();

    if (that.state.ex.MxPaymentType == 0) {
      $.ajax({
        url: "/gposorder/UpdatePickUpTime",
        type: "POST",
        dataType: "json",
        contentType: "application/json",
        data: JSON.stringify(
          this.state.model.OrderGPosInformation.OrderDateTime
        ),
        success: response => {
          $("#loading").hide();
          if (response.status == "success") {
            that.props.history.push("/gposshoppingcart/checkout");
          } else {
            alertify.alert("Error", response.message);
          }
        },
        error: function(er) {
          $("#loading").hide();
          alertify.alert("Error", "Opps! System has error.");
        }
      });
    } else {
      $.ajax({
        url: "/gposorder/gotocheckout",
        type: "POST",
        dataType: "json",
        contentType: "application/json",
        data: JSON.stringify(
          this.state.model.OrderGPosInformation.OrderDateTime
        ),
        success: response => {
          $("#loading").hide();
          if (response.status == "success") {
            window.location.href = `${that.state.ex.PaymentLink}?amt=${
              response.data.TotalAmount
            }&customerName=${response.data.FirstName} ${
              response.data.LastName
            }&cell=${response.data.CustomerPhone}&email=${
              response.data.CustomerEmail
            }&memo=${response.data.Id}&allowPartial=0`;
          } else {
            alertify.alert("Error", response.message);
          }
        },
        error: function(er) {
          $("#loading").hide();
          alertify.alert("Error", "Opps! System has error.");
        }
      });
    }
  }

  viewCart() {
    var that = this;

    if (that.state.model.OrderItems.length > 0) {
      $("#viewCartModal").modal("show");
    }
  }

  RefreshViewCart(e) {
    var that = this;
    this.state.model = e;
    this.setState(this.state);
    if (!this.state.model || this.state.model.OrderItems.length === 0) {
      that.props.history.push("/gposshoppingcart");
      return false;
    }
  }
  render() {
    return (
      <React.Fragment>
        {this.state.ex && this.state.model ? (
          <React.Fragment>
            <section id="checkout">
              <div className="nc-section-header">
                <div className="container">
                  <div className="row">
                    <div className="col-md-4 col-md-offset-8 hidden-xs hidden-sm">
                      <div className="nc-title-category">
                        <h5 className="h4 text-bold">My order</h5>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="nc-section-body">
                <div className="container">
                  <div className="row">
                    <div className="col-md-8 col-sm-12 col-xs-12">
                      <div className="nc-contact-information">
                        <div className="nc-info">
                          <h3 className="h4 nc-title">Contact</h3>
                          <div className="nc-name">
                            {this.state.model.FirstName}{" "}
                            {this.state.model.LastName}
                          </div>
                          <div className="nc-name">
                            {this.state.model.CustomerEmail}
                          </div>
                          <div className="nc-name">{`${this.state.model.CustomerPhone.slice(
                            0,
                            3
                          )}-${this.state.model.CustomerPhone.slice(
                            3,
                            6
                          )}-${this.state.model.CustomerPhone.slice(6)}`}</div>
                        </div>
                        <div className="nc-info">
                          <h3 className="h4 nc-title">Promotion Code</h3>
                          {this.state.model.Coupon &&
                          this.state.model.DiscountAmount < 0 ? (
                            <React.Fragment>
                              <div className="coupon clearfix">
                                <input
                                  type="text"
                                  className="form-control"
                                  //   placeholder="Enter your coupon code"
                                  value={this.state.model.Coupon.CouponCode}
                                  onChange={e => {
                                    this.state.model.CouponCode =
                                      e.target.value;
                                    this.setState(this.state);
                                  }}
                                  readOnly
                                />
                                <a
                                  href="javascript:;"
                                  className="btn btn-default"
                                  onClick={e => {
                                    this.removeCoupon();
                                  }}
                                >
                                  Remove
                                </a>
                              </div>
                            </React.Fragment>
                          ) : (
                            <React.Fragment>
                              <div className="coupon clearfix">
                                <input
                                  value={this.state.model.CouponCode}
                                  placeholder="Enter your coupon code"
                                  className="form-control"
                                  onChange={e => {
                                    this.state.model.CouponCode =
                                      e.target.value;
                                    this.setState(this.state);
                                  }}
                                />
                                <a
                                  href="javascript:"
                                  className="btn"
                                  onClick={e => {
                                    this.applyCoupon();
                                  }}
                                >
                                  Apply
                                </a>
                              </div>
                            </React.Fragment>
                          )}
                        </div>
                        <div className="nc-info">
                          <h3 className="h4 nc-title">
                            When do you want your order ?
                          </h3>
                          <div className="checkout-date">
                            <div className="form-group">
                              <label for="">Pick-up Date *</label>
                              <select
                                name=""
                                id=""
                                className="form-control"
                                value={
                                  this.state.model.OrderGPosInformation
                                    .OrderDateTime
                                    ? moment
                                        .utc(
                                          this.state.model.OrderGPosInformation
                                            .OrderDateTime
                                        )
                                        .format("MM/DD/YYYY")
                                    : null
                                }
                                onChange={e => {
                                  this.state.model.OrderGPosInformation.OrderDateTime = moment
                                    .utc(e.target.value, "MM/DD/YYYY")
                                    .toDate();
                                  this.state.ex.SelectHour = null;
                                  this.state.ex.SelectMinute = 0;
                                  if (this.minutelistselect.current) {
                                    this.minutelistselect.current.value = null;
                                  }
                                  if (this.hourlistselect.current) {
                                    this.hourlistselect.current.value = null;
                                  }

                                  this.setState(this.state);
                                }}
                              >
                                <option value={null}>
                                  Please select pick-up date
                                </option>
                                {this.state.ex.DateList.map(d => {
                                  return (
                                    <option
                                      value={moment
                                        .utc(d.Day)
                                        .format("MM/DD/YYYY")}
                                    >
                                      {moment
                                        .utc(d.Day)
                                        .format("dddd - (MM/DD/YYYY)")}
                                    </option>
                                  );
                                })}
                              </select>
                            </div>
                            <div className="form-group clearfix">
                              <label for="">Pick-up Time *</label>
                              <div className="form-time clearfix">
                                {this.drawhourlist()}
                                {this.drawminutelist()}
                              </div>
                            </div>
                            <div className="form-group nc-billing visible-sm visible-xs">
                              {this.state.model.OrderItems.map(sku => {
                                return (
                                  <div className="nc-item-product-mobile clearfix">
                                    <div className="nc-left">
                                      <div className="nc-name">
                                        {sku.Name} ({sku.Quantity}x)
                                      </div>
                                      <div className="nc-modifiers">
                                        {sku.ModifierItems.map(modi => {
                                          return (
                                            <React.Fragment>
                                              <span>{modi.Name} ,</span>
                                            </React.Fragment>
                                          );
                                        })}
                                      </div>
                                    </div>
                                    <div className="nc-right">
                                      <div className="nc-price">
                                        ${sku.Price.toFixed(2)}
                                      </div>
                                    </div>
                                  </div>
                                );
                              })}

                              <div className="nc-item-subtotal clearfix">
                                <div className="nc-left">Subtotal:</div>
                                <div className="nc-right">
                                  ${this.state.model.SubTotalAmount
                                    ? this.state.model.SubTotalAmount.toFixed(2)
                                    : "0.00"}
                                </div>
                              </div>
                              <div className="nc-item-subtotal clearfix">
                                <div className="nc-left">Discount:</div>
                                <div className="nc-right">
                                  ${this.state.model.DiscountAmount
                                    ? this.state.model.DiscountAmount.toFixed(2)
                                    : "0.00"}
                                </div>
                              </div>
                              <div className="nc-item-subtotal clearfix">
                                <div className="nc-left">Tax:</div>
                                <div className="nc-right">
                                  ${this.state.model.TotalTax
                                    ? this.state.model.TotalTax.toFixed(2)
                                    : "0.00"}
                                </div>
                              </div>
                              <div className="nc-item-subtotal clearfix">
                                <div className="nc-left">Tips:</div>
                                <div className="nc-right">
                                  ${this.state.model.Tips
                                    ? this.state.model.Tips.toFixed(2)
                                    : "0.00"}
                                </div>
                              </div>
                              <div className="nc-item-total clearfix">
                                <div className="nc-left">Total:</div>
                                <div className="nc-right">
                                  ${this.state.model.TotalAmount
                                    ? this.state.model.TotalAmount.toFixed(2)
                                    : "0.00"}
                                </div>
                              </div>
                            </div>
                            <div className="form-group">
                              <h3 className="h4 nc-title">Tips (optional)</h3>
                              <React.Fragment>
                                <div className="coupon clearfix">
                                  <input
                                    type="text"
                                    className="form-control"
                                    placeholder="Tips (optional)"
                                    value={this.state.ex.Tips}
                                    onChange={e => {
                                      this.state.ex.Tips = e.target.value;
                                      this.setState(this.state);
                                    }}
                                  />
                                  <a
                                    href="javascript:;"
                                    className="btn"
                                    onClick={e => {
                                      this.updateTips(this.state.ex.Tips);
                                    }}
                                  >
                                    Update
                                  </a>
                                </div>
                              </React.Fragment>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="button-container-contact">
                        <a
                          href="javascript:;"
                          className="btn"
                          onClick={e => {
                            this.gotocheckout();
                          }}
                        >
                          Continue
                        </a>
                      </div>
                    </div>
                    <div className="col-md-4 col-sm-12 col-xs-12 hidden-xs hidden-sm">
                      <div className="nc-billing-information">
                        <div className="nc-list-item">
                          <table>
                            <thead>
                              <tr>
                                <th />
                                <th />
                              </tr>
                            </thead>
                            <tbody>
                              {this.state.model.OrderItems.map(sku => {
                                return (
                                  <tr>
                                    <td>
                                      <div className="nc-title">
                                        {" "}
                                        {sku.Name} ({sku.Quantity}x)
                                      </div>
                                      <ul>
                                        {sku.ModifierItems.map(modi => {
                                          return (
                                            <React.Fragment>
                                              <li>+ {modi.Name}</li>
                                            </React.Fragment>
                                          );
                                        })}
                                      </ul>
                                      {sku.SpecialRequest ? (
                                        <React.Fragment>
                                          <div
                                            style={{
                                              fontSize: "12px",
                                              textTransform: "none"
                                            }}
                                          >
                                            Special request:{" "}
                                            {sku.SpecialRequest}
                                          </div>
                                        </React.Fragment>
                                      ) : (
                                        ""
                                      )}
                                    </td>
                                    <td>
                                      ${sku.Price
                                        ? sku.Price.toFixed(2)
                                        : "0.00"}
                                    </td>
                                  </tr>
                                );
                              })}

                              <tr>
                                <td>
                                  <div className="nc-title">Subtotal</div>
                                </td>
                                <td>
                                  ${this.state.model.SubTotalAmount
                                    ? this.state.model.SubTotalAmount.toFixed(2)
                                    : "0.00"}
                                </td>
                              </tr>
                              <tr>
                                <td>
                                  <div className="nc-title">Discount</div>
                                </td>
                                <td>
                                  {this.state.model.DiscountAmount !== 0 ? (
                                    <React.Fragment>
                                      ${this.state.model.DiscountAmount.toFixed(
                                        2
                                      )}
                                    </React.Fragment>
                                  ) : (
                                    <React.Fragment>$0.00</React.Fragment>
                                  )}
                                </td>
                              </tr>
                              <tr>
                                <td>
                                  <div className="nc-title"> Tax</div>
                                </td>
                                <td>
                                  ${this.state.model.TotalTax
                                    ? this.state.model.TotalTax.toFixed(2)
                                    : "0.00"}
                                </td>
                              </tr>
                              <tr>
                                <td>
                                  <div className="nc-title">Tips </div>
                                </td>
                                <td>
                                  ${this.state.model.Tips
                                    ? this.state.model.Tips.toFixed(2)
                                    : "0.00"}
                                </td>
                              </tr>
                              <tr>
                                <td>
                                  <div className="nc-title">TOTAL</div>
                                </td>
                                <td>
                                  ${this.state.model.TotalAmount
                                    ? this.state.model.TotalAmount.toFixed(2)
                                    : "0.00"}
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      </div>
                      <div className="button-container">
                        <a
                          href="javascript:;"
                          className="btn"
                          onClick={e => this.viewCart()}
                        >
                          update cart
                        </a>
                        <a href="/" className="btn">
                          continue shopping
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="nc-section-footer" />
            </section>
            <ButtonViewCart
              model={this.state.model}
              onChange={e => this.RefreshViewCart(e)}
            />
          </React.Fragment>
        ) : (
          ""
        )}
      </React.Fragment>
    );
  }
}

export default OrderInfo;
