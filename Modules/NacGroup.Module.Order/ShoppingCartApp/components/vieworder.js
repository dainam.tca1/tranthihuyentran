import React, { Component } from "react";
import { Link } from "react-router-dom";
import moment from "moment";

class ViewOrder extends Component {
  constructor(props) {
    super(props);
    this.state = {
        model: null,
        ex: {
            Quantity:0
        }
    };
    var that = this;
    document.title = "Order Infomation";
    $("#loading").show();

    $.ajax({
      url: "/gposorder/getcart",
      type: "GET",
      dataType: "json",
      contentType: "application/json",
      success: response => {
        $("#loading").hide();
        if (!response || response.OrderItems.length === 0) {
          that.props.history.push("/gposshoppingcart");
          return false;
        }
        that.state.model = response;
        that.setState(that.state);
      },
      error: function(er) {
        $("#loading").hide();
      }
    });
  }
    Increase(id) {
        var that = this;
        that.state.ex.Quantity = $('#quantity-value-' + id + '').val();
        if (that.state.ex.Quantity == $('#quantity-value-' + id + '').val()) {
            that.state.ex.Quantity = parseInt(that.state.ex.Quantity) + 1;
            that.setState(that.state);
        }
        $('#quantity-value-' + id + '').val(that.state.ex.Quantity);
        $("#loading").show();
        $.ajax({
            url: "/gposorder/updatequantity",
            type: "POST",
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify({
                id: id,
                quantity: that.state.ex.Quantity,
            }),
            success: response => {
                $("#loading").hide();
                if (response.status == "success") {
                    that.state.model = response.data;
                    that.setState(that.state);
                } else {
                    alertify.alert("Error", response.message);
                }
            },
            error: function (er) {
                $("#loading").hide();
                alertify.alert("Error", "Error");
            }
        });
        that.setState(that.state);
    }
    Decrease(id) {
        var that = this;
        that.state.ex.Quantity = $('#quantity-value-' + id + '').val();
        if (that.state.ex.Quantity == $('#quantity-value-' + id + '').val()) {
            that.state.ex.Quantity = parseInt(that.state.ex.Quantity) - 1;
            that.setState(that.state);
        }
        $('#quantity-value-' + id + '').val(that.state.ex.Quantity);
        if (that.state.ex.Quantity == 0) {
            alertify.confirm(
                'Warning',
                'Are you sure delete product',
                function () {
                    $("#loading").show();
                    $.ajax({
                        url: "/gposorder/removeitem",
                        type: "POST",
                        dataType: "json",
                        contentType: "application/json",
                        data: JSON.stringify(id),
                        success: function (response) {
                            $("#loading").hide();
                            if (response.status == "success") {
                                $(".cart-count-txt").text(
                                    response.data.OrderItems.length
                                );
                                $(".cart-money").text("$" + response.data.TotalAmount.toFixed(2));
                                if (!response.data || response.data.OrderItems.length === 0) {
                                    that.props.history.push("/gposshoppingcart");
                                    return false;
                                }
                                that.state.model = response.data;
                                that.setState(that.state);
                                
                            } else {
                                alertify.alert("Error", response.message);
                            }
                        },
                        error: function (er) {
                            $("#loading").hide();
                            alertify.alert("Error", "Error");
                        }
                    });
                },
                function () { }
            )
        } else {
            $("#loading").show();
            $.ajax({
                url: "/gposorder/updatequantity",
                type: "POST",
                dataType: "json",
                contentType: "application/json",
                data: JSON.stringify({
                    id: id,
                    quantity: that.state.ex.Quantity,
                }),
                success: response => {
                    $("#loading").hide();
                    if (response.status == "success") {
                        that.state.model = response.data;
                        that.setState(that.state);
                    } else {
                        alertify.alert("Error", response.message);
                    }
                },
                error: function (er) {
                    $("#loading").hide();
                    alertify.alert("Error", "Error");
                }
            });
        }
      
        that.setState(that.state);
    }
  render() {
    return (
      <React.Fragment>
            {this.state.ex && this.state.model ? (
                <section id="my-order" className="visible-xs visible-sm">
                    <div className="nc-section-header">
                        <div className="container">
                            <div className="row">
                                <div className="col-md-12">
                                    <h3 className="h4 nc-name">my order</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="nc-section-body">
                        <div className="container">
                            <div className="row">
                                <div className="col-md-12">
                                    {this.state.model.OrderItems.length > 0 ? (
                                        this.state.model.OrderItems.map(c => {
                                            return (
                                                <div className="nc-item-product">
                                                    <div className="nc-item-main flex-container">
                                                        <div className="nc-item-main-left">
                                                            <div className="nc-name">
                                                                {c.Name}
                                                            </div>
                                                            <div className="nc-price">
                                                                ${c.SkuPrice ? c.SkuPrice.toFixed(2) : "0.00"}
                                                            </div>
                                                        </div>
                                                        <div className="nc-item-main-right">
                                                            <div className="nc-quantity-container">
                                                                <div className="nc-divide">
                                                                    <div className="nc-divide-inner">
                                                                        <a href="javascript:;" onClick={e => this.Decrease(c.Id)}>-</a>
                                                                    </div>
                                                                </div>
                                                                <div className="nc-input">
                                                                    <input disabled="disabled" type="text" id={`quantity-value-${c.Id}`} value={c.Quantity} />
                                                                </div>
                                                                <div className="nc-increase">
                                                                    <div className="nc-increase-inner">
                                                                        <a href="javascript:;" onClick={e => this.Increase(c.Id)}>+</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    {c.ModifierItems.length > 0 ? c.ModifierItems.map(modi => {
                                                        return (
                                                            <React.Fragment>
                                                                <div className="nc-item-modifier clearfix">
                                                                    <div className="nc-modifier-left">
                                                                        + {modi.Name}
                                                                    </div>

                                                                </div>
                                                            </React.Fragment>
                                                        )
                                                    }) : ""}
                                                    {c.SpecialRequest ? (
                                                        <React.Fragment>
                                                            <div style={{ fontSize: "12px", textTransform: "none" }}>Special Request: {c.SpecialRequest}</div>
                                                        </React.Fragment>
                                                    ) : ""}

                                                </div>
                                            )
                                        })
                                    ) : ""}


                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="nc-section-footer">
                        <div className="container">
                            <div className="row">
                                <div className="col-md-12">
                                    <div className="nc-sub clearfix">
                                        <div className="nc-left">
                                            Subtotal
                                        </div>
                                        <div className="nc-right">
                                            $ {this.state.model.SubTotalAmount ? this.state.model.SubTotalAmount.toFixed(2) : "0.00"}
                                        </div>
                                    </div>
                                    <div className="nc-sub clearfix">
                                        <div className="nc-left">
                                            Discount
                                        </div>
                                        <div className="nc-right">
                                            $ {this.state.model.DiscountAmount ? this.state.model.DiscountAmount.toFixed(2) : "0.00"}
                                        </div>
                                    </div>
                                    <div className="nc-tax clearfix">
                                        <div className="nc-left">
                                            Tax
                                    </div>
                                     <div className="nc-right">
                                            $ {this.state.model.TotalTax ? this.state.model.TotalTax.toFixed(2) : "0.00"}
                                       </div>
                                    </div>
                                    <div className="nc-sub clearfix">
                                        <div className="nc-left">
                                           Tips
                                        </div>
                                        <div className="nc-right">
                                            $ {this.state.model.Tips ? this.state.model.Tips.toFixed(2) : "0.00"}
                                        </div>
                                    </div>
                                  
                                    <div className="nc-total clearfix">
                                        <div className="nc-left">
                                            Total
                                    </div>
                                        <div className="nc-right">
                                            $ {this.state.model.TotalAmount ? this.state.model.TotalAmount.toFixed(2) : "0.00"}
                                        </div>
                                    </div>
                                    <div className="nc-button">
                                        <a href="/" className="btn"> Continue shopping</a>
                                        <a href="/gposshoppingcart/orderinformation" className="btn"> Checkout</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            ): ""}
      </React.Fragment>
    );
  }
}

export default ViewOrder;
