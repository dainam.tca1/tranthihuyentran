import React, { Component } from "react";
import { Link } from "react-router-dom";
import moment from "moment";

class Checkout extends Component {
  constructor(props) {
    super(props);
    this.state = {
      model: null,
      ex: {
        CartNumber: null,
        ExpirationMonth: "01",
        ExpirationYear: "19",
        CCVCode: null,
        OrderDetails: null,
        IsPaid: false,
        IsLoading: false
      }
    };
    var that = this;
    document.title = "Checkout";
    $("#loading").show();
    $.ajax({
      url: "/gposorder/getcart",
      type: "GET",
      dataType: "json",
      contentType: "application/json",
      success: response => {
        $("#loading").hide();
        if (!response || response.OrderItems.length === 0) {
          that.props.history.push("/gposshoppingcart");
          return false;
        }
        that.state.model = response;
        that.setState(that.state);
      },
      error: function(er) {
        $("#loading").hide();
      }
    });
  }

  submitcheckout() {
    if (this.state.ex.IsLoading) {
      return -1;
    }
    var that = this;
    that.state.ex.IsLoading = true;
    that.setState(that.state);
    $("#loading").show();
    $.ajax({
      url: "/gposorder/confirmcheckout",
      type: "POST",
      dataType: "json",
      contentType: "application/json",
      data: JSON.stringify({
        CartNumber: this.state.ex.CartNumber,
        ExpirationMonth: this.state.ex.ExpirationMonth,
        ExpirationYear: this.state.ex.ExpirationYear,
        CcvCode: this.state.ex.CCVCode
      }),
      success: response => {
        if (response.status == "success") {
          window.location.href =
            "/gposorder/paymment-success/" + response.data.Id;
        } else {
          $("#loading").hide();
          that.state.ex.IsLoading = false;
          that.setState(that.state);
          alertify.alert("Error", response.message);
        }
      },
      error: function(er) {
        $("#loading").hide();
        that.state.ex.IsLoading = false;
        that.setState(that.state);
        alertify.alert("Error", "Opps! System has error.");
      }
    });
    return 0;
  }

  render() {
    return (
      <React.Fragment>
        {this.state.ex.IsPaid && this.state.ex.OrderDetails ? (
          <React.Fragment>
            <section className="container" id="cart-success">
              <h2 className="h2 text-center text-bold m-bot-3">
                Thanks For Your Order, {this.state.ex.OrderDetails.FirstName}.
              </h2>
              <div className="section-body bg-white">
                <div className="section-content">
                  <h2 className="h5 text-bold">Order Summary</h2>
                  {this.state.ex.OrderDetails.OrderItems.map(item => {
                    return (
                      <div className="item clearfix">
                        <h3 className="h">
                          {item.Name} (x{item.Quantity})
                          {item.ModifierItems.map(modi => {
                            return (
                              <React.Fragment>
                                <br />
                                <span className="h6">+{modi.Name}</span>
                              </React.Fragment>
                            );
                          })}
                          {item.SpecialRequest && (
                            <React.Fragment>
                              <br />
                              <span className="h6">
                                Special Request: {item.SpecialRequest}
                              </span>
                            </React.Fragment>
                          )}
                        </h3>
                        <div className="price text-bold">
                          ${Number(item.Price * item.Quantity).toFixed(2)}
                        </div>
                      </div>
                    );
                  })}
                </div>
                <div className="section-total">
                  <div className="item clearfix">
                    <h3 className="h">Subtotal</h3>
                    <div className="price text-bold">
                      ${this.state.ex.OrderDetails.SubTotalAmount.toFixed(2)}
                    </div>
                  </div>
                  <div className="item clearfix">
                    <h3 className="h">Discount</h3>
                    <div className="price text-bold">
                      ${this.state.ex.OrderDetails.DiscountAmount.toFixed(2)}
                    </div>
                  </div>
                  <div className="item clearfix">
                    <h3 className="h">Tax</h3>
                    <div className="price text-bold">
                      ${this.state.ex.OrderDetails.TotalTax.toFixed(2)}
                    </div>
                  </div>

                  <div className="item clearfix">
                    <h3 className="h">Tips</h3>
                    <div className="price text-bold">
                      ${this.state.ex.OrderDetails.Tips.toFixed(2)}
                    </div>
                  </div>
                  <div className="item clearfix">
                    <h3 className="h">Total</h3>
                    <div className="price text-bold">
                      ${this.state.ex.OrderDetails.TotalAmount.toFixed(2)}
                    </div>
                  </div>
                </div>
                <div className="section-information">
                  <h2 className="h5 text-bold">Billing Information</h2>
                  <div className="item clearfix">
                    <h3 className="h">Order Number:</h3>
                    <div className="text-bold">
                      {
                        this.state.ex.OrderDetails.OrderGPosInformation
                          .OrderPosNumber
                      }
                    </div>
                  </div>

                  <div className="item clearfix">
                    <h3 className="h">Order Date:</h3>
                    <div className="text-bold">
                      {moment
                        .utc(this.state.ex.OrderDetails.CreatedDate)
                        .format("MM/DD/YYYY hh:mm:ss A")}
                    </div>
                  </div>

                  <div className="item clearfix">
                    <h3 className="h">Pick-up Time:</h3>
                    <div className="text-bold">
                      {moment
                        .utc(
                          this.state.ex.OrderDetails.OrderGPosInformation
                            .OrderDateTime
                        )
                        .format("MM/DD/YYYY hh:mm:ss A")}
                    </div>
                  </div>
                </div>
              </div>
              <div className="section-footer text-center m-top-4">
                <a
                  className="btn btn-container btn-primary h5 m-top-2"
                  href="/"
                >
                  Go Homepage
                </a>
              </div>
            </section>
          </React.Fragment>
        ) : (
          <React.Fragment>
            {this.state.model && (
              <section id="payment-form">
                <div className="nc-section-header" />
                <div className="nc-section-body">
                  <div className="container">
                    <div className="row">
                      <div className="col-md-12">
                        <div className="nc-payment-form">
                          <h3 className="h4 nc-title">Payment</h3>
                          <div className="nc-des">
                            <p>Your payment information</p>
                            <p>credit card</p>
                          </div>
                          <div className="nc-payment-form-inner">
                            <div className="form-group">
                              <label for="">card number * </label>
                              <input
                                maxLength="19"
                                type="text"
                                placeholder="Card Number"
                                className="form-control"
                                value={this.state.ex.CartNumber}
                                onChange={e => {
                                  this.state.ex.CartNumber = e.target.value;
                                  this.setState(this.state);
                                }}
                              />
                            </div>
                            <div className="form-group">
                              <label for="">expiration month - year *</label>
                              <div className="form-select clearfix">
                                <select
                                  className="form-control"
                                  value={this.state.ex.ExpirationMonth}
                                  onChange={e => {
                                    this.state.ex.ExpirationMonth =
                                      e.target.value;
                                    this.setState(this.state);
                                  }}
                                >
                                  <option value="01">January</option>
                                  <option value="02">February</option>
                                  <option value="03">March</option>
                                  <option value="04">April</option>
                                  <option value="05">May</option>
                                  <option value="06">June</option>
                                  <option value="07">July</option>
                                  <option value="08">August</option>
                                  <option value="09">September</option>
                                  <option value="10">October</option>
                                  <option value="11">November</option>
                                  <option value="12">December</option>
                                </select>
                                <select
                                  className="form-control"
                                  value={this.state.ex.ExpirationYear}
                                  onChange={e => {
                                    this.state.ex.ExpirationYear =
                                      e.target.value;
                                    this.setState(this.state);
                                  }}
                                >
                                  <option value="19">2019</option>
                                  <option value="20">2020</option>
                                  <option value="21">2021</option>
                                  <option value="22">2022</option>
                                  <option value="23">2023</option>
                                  <option value="24">2024</option>
                                  <option value="25">2025</option>
                                  <option value="26">2026</option>
                                  <option value="27">2027</option>
                                  <option value="28">2028</option>
                                  <option value="29">2029</option>
                                </select>
                              </div>
                            </div>
                            <div className="form-group">
                              <label for="">ccv code *</label>
                              <input
                                maxLength="4"
                                type="text"
                                placeholder="Enter CCV Code"
                                className="form-control"
                                value={this.state.ex.CCVCode}
                                onChange={e => {
                                  this.state.ex.CCVCode = e.target.value;
                                  this.setState(this.state);
                                }}
                              />
                            </div>
                          </div>
                        </div>
                        <div className="nc-button-container">
                          <a
                            href="javascript:;"
                            className="btn"
                            onClick={e => {
                              this.submitcheckout();
                            }}
                          >
                            check out
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="nc-section-footer" />
              </section>
            )}
          </React.Fragment>
        )}
      </React.Fragment>
    );
  }
}

export default Checkout;
