﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Order.Models.Schema;
using NacGroup.Module.Order.Services;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace NacGroup.Module.Order.Controllers
{
    [Authorize(Roles = "Admin", Policy = "TenantAuthorize")]
    [Route("cms/api/shippingbybill/{action=Index}")]
    public class ShippingTotalBillCmsApiController : Controller
    {
        private IShippingFeeByTotalBillService _shippingFeeByTotalBillRepository;

        public ShippingTotalBillCmsApiController(IShippingFeeByTotalBillService shippingFeeByTotalBillRepository)
        {
            _shippingFeeByTotalBillRepository = shippingFeeByTotalBillRepository;
        }


        [HttpGet]
        public ActionResult<ApiResponseModel> Index()
        {
            var paged = _shippingFeeByTotalBillRepository.GetAll();

            return new ApiResponseModel("success", new PagedResult<ShippingFeeByTotalBill>(paged, 1, paged.Count, paged.Count));
        }


        [HttpPost]
        public ActionResult<ApiResponseModel> RemoveList([FromBody] List<string> idList)
        {
            return _shippingFeeByTotalBillRepository.Delete(idList);
        }

        [HttpPost]
        public ActionResult<ApiResponseModel> Add([FromBody] ShippingFeeByTotalBill model)
        {
            return _shippingFeeByTotalBillRepository.Add(model);
        }

        public ActionResult<ApiResponseModel> Update(string id)
        {
            var item = _shippingFeeByTotalBillRepository.GetById(id);
            if (item == null)
                return new ApiResponseModel("error", StaticMessage.DataNotFound);

            return new ApiResponseModel("success", item);
        }
        [HttpPost]
        public ActionResult<ApiResponseModel> Update([FromBody] ShippingFeeByTotalBill model)
        {
            return _shippingFeeByTotalBillRepository.Update(model);
        }

    }
}
