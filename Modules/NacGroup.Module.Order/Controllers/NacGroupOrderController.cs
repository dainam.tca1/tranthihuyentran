﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using NacGroup.Infrastructure;
using NacGroup.Module.Core.Extensions;
using NacGroup.Module.Core.Interfaces;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Core.Models.Schema;
using NacGroup.Module.Core.Services;
using NacGroup.Module.Order.Models;
using NacGroup.Module.Order.Models.Schema;
using NacGroup.Module.Order.Services;
using Newtonsoft.Json.Linq;

namespace NacGroup.Module.Order.Controllers
{
    [Route("ordernacgroup/{action=Index}")]
    public class NacGroupOrderController : Controller
    {
        private readonly IOrderPickUpSettingService _orderPickUpSettingService;
        private readonly IUserProfileService _userProfileService;
        private readonly IOrderNacGroupService _orderNacGroupService;
        private readonly IOrderNacGroupCmsService _orderNacGroupCmsService;
        private readonly IShippingService _shippingService;
        private readonly GlobalConfiguration _globalConfiguration;
        private readonly Tenant _tenant;
        private readonly IMemoryCache _memoryCache;
        

        public NacGroupOrderController(
            IMemoryCache memoryCache,
            IOrderPickUpSettingService orderPickUpSettingService , 
            IOrderNacGroupService orderNacGroupService, 
          
            IGettingGlobalConfigService gettingGlobalConfigService,
            IShippingService shippingService,
            IOrderNacGroupCmsService orderNacGroupCmsService,
            ITenantProvider tenantProvider, 
            IUserProfileService userProfileService
        )
        {
            _memoryCache = memoryCache;
            _orderPickUpSettingService = orderPickUpSettingService;
            _globalConfiguration = gettingGlobalConfigService.GetConfiguration();
            _orderNacGroupService = orderNacGroupService;
            _shippingService = shippingService;
            _orderNacGroupCmsService = orderNacGroupCmsService;
            _tenant = tenantProvider.GetTenant();
            _userProfileService = userProfileService;

        }

       


        [Route("/checkout/{id?}/{param?}")]
        public IActionResult ShoppingCart()
        {
                
            var shoppingCart = GetShoppingCart();
            if (shoppingCart.OrderItems.Count == 0)
            {
                return Redirect("/");
            }
            return View("Index");
        }

        [Authorize(Policy = "TenantAuthorize")]
        [Route("/checkout/address")]
        public IActionResult Shipping()
        {

            var shoppingCart = GetShoppingCart();
            if (shoppingCart.OrderItems.Count == 0)
            {
                return Redirect("/");
            }
            ViewData["TernantName"] = _globalConfiguration.TenantName;
            return View("Index");
        }

        [Authorize(Policy = "TenantAuthorize")]
        [Route("/checkout/payment")]
        public IActionResult Payment()
        {

            var shoppingCart = GetShoppingCart();
            if (shoppingCart.OrderItems.Count == 0)
            {
                return Redirect("/");
            }
            if (string.IsNullOrEmpty(shoppingCart.Recipient.FullName) || string.IsNullOrEmpty(shoppingCart.Recipient.FullName))
            {
                return Redirect("/checkout/address");
            }
            return View("Index");
        }

        [Authorize(Policy = "TenantAuthorize")]
        [Route("/checkout/payment/success")]
        public IActionResult Success()
        {
            return View("Index");
        }

        [HttpGet]
        [Route("/checkout/success")]
        public ActionResult<Models.Schema.Order> Success(string code)
        {

            var order = _orderNacGroupCmsService.Get().FirstOrDefault(m => m.OrderNumber == code);
            if (order == null)
            {
                return null;
            }
            return Ok(order);
        }

        [HttpGet]
        public ActionResult<Models.Schema.Order> GetCart()
        {
            return GetShoppingCart();
        }

        [HttpGet]
        public IActionResult GetCity()
        {
            var cacheKey = $"_{ConstKey.CityCacheKey}";
            if (_memoryCache.TryGetValue(cacheKey, out List<CityCollection> result)) return Ok(result);

            result = _shippingService.GetCity().ToList();

            if (result.Count < 0)
            {
                return null;
            }
            _memoryCache.Set(cacheKey, result, DateTimeOffset.Now.AddDays(1));
            return Ok(result);
        }

        [HttpGet]
        public ActionResult<ApiResponseModel> GetDistrict()
        {
            var cacheKey = $"_{ConstKey.DistrictCacheKey}";
            if (_memoryCache.TryGetValue(cacheKey, out List<DistrictCollection> result)) return Ok(result);

            result = _shippingService.GetDistrict().ToList();

            if (result.Count < 0)
            {
                return null;
            }
            _memoryCache.Set(cacheKey, result, DateTimeOffset.Now.AddDays(1));
            return Ok(result);
            
        }

        [HttpGet]
        public IActionResult GetWard()
        {
            var cacheKey = $"_{ConstKey.WardCacheKey}";
            if (_memoryCache.TryGetValue(cacheKey, out List<WardCollection> result)) return Ok(result);

            result = _shippingService.GetWard().ToList();

            if (result.Count < 0)
            {
                return null;
            }
            _memoryCache.Set(cacheKey, result, DateTimeOffset.Now.AddDays(1));
            return Ok(result);
        }

        public ActionResult<List<OrderPickUpAvailableDate>> GetAvailableDate()
        {
            var shoppingCartSetting = _orderPickUpSettingService.Get();
            if (shoppingCartSetting == null)
                return new List<OrderPickUpAvailableDate>();

            var dateTimeNow = TimeZoneInfo.ConvertTime(DateTime.Now, _globalConfiguration.LocalTimeZoneInfo);
            return shoppingCartSetting.GetAvalableDateList(2, dateTimeNow);
        }

        [HttpPost]
        public ActionResult<ApiResponseModel> GotoCheckout([FromBody] DateTime pickupTime)
        {
            if (!User.Identity.IsAuthenticated)
                return new ApiResponseModel("error", "Please login to continue");
            var shoppingCart = GetShoppingCart();
            var result = _orderNacGroupService.UpdatePickUpTime(ref shoppingCart, pickupTime);
            if (result.status == "error") return result;
            SaveCart(shoppingCart);
            return result;
        }

        [HttpPost]
        public ActionResult<ApiResponseModel> UpdateTips([FromBody] decimal amount)
        {
            if (!User.Identity.IsAuthenticated)
                return new ApiResponseModel("error", "Please login to continue");
            var shoppingCart = GetShoppingCart();
            var result = _orderNacGroupService.UpdateTips(ref shoppingCart, amount);
            if (result.status == "error")
                return result;
            SaveCart(shoppingCart);
            return new ApiResponseModel("success", shoppingCart);
        }

        [HttpPost]
        public ActionResult<ApiResponseModel> ApplyCoupon([FromBody] string couponcode)
        {
            if (!User.Identity.IsAuthenticated)
                return new ApiResponseModel("error", "Please login to continue");

            var shoppingCart = GetShoppingCart();

            var result = _orderNacGroupService.ApplyCoupon(ref shoppingCart, couponcode);
            if (result.status == "error")
                return result;
            SaveCart(shoppingCart);
            return new ApiResponseModel("success", shoppingCart);
        }

        [HttpPost]
        public ActionResult<ApiResponseModel> ClearCoupon()
        {
            if (!User.Identity.IsAuthenticated)
                return new ApiResponseModel("error", "Please login to continue");

            var shoppingCart = GetShoppingCart();

            var result = _orderNacGroupService.ClearCoupon(ref shoppingCart);
            if (result.status == "error")
                return result;
            SaveCart(shoppingCart);
            return new ApiResponseModel("success", shoppingCart);
        }

        [HttpPost]
        public ActionResult<ApiResponseModel> UpdateQuantity([FromBody] JObject model)
        {
            var shoppingCart = GetShoppingCart();
            if (model?["id"] == null || model["quantity"] == null)
                return new ApiResponseModel("error", StaticMessage.ParameterInvalid);

            var id = new Guid(model["id"].ToString().ToUpper());
            var quantity = model["quantity"].ToObject<int>();
            var result = _orderNacGroupService.UpdateQuantity(id, quantity, ref shoppingCart);
            if (!string.IsNullOrEmpty(shoppingCart.City))
            {
                var updateshipping = _orderNacGroupService.UpdateShipping(shoppingCart, shoppingCart.City, shoppingCart.District);
                if (updateshipping.status == "error")
                {
                    return new ApiResponseModel("error", updateshipping.message);
                }
            }
            else
            {
                var updateShipping = _orderNacGroupService.UpdateShippingTotalBill(shoppingCart);
                if (updateShipping.message == "error") return new ApiResponseModel("error", updateShipping.message);
            }
            
            
            if (result.status == "error")
                return result;
            if (shoppingCart.Coupon != null)
            {
                var updatediscount = _orderNacGroupService.UpdateDiscount(ref shoppingCart);
                if (updatediscount.status == "error")
                {
                    return new ApiResponseModel("error", updatediscount.message);
                }
            }
           

            SaveCart(shoppingCart);
            return new ApiResponseModel("success", shoppingCart);
        }

        [HttpPost]
        public ActionResult<ApiResponseModel> RemoveItem([FromBody]Guid id)
        {
            var shoppingCart = GetShoppingCart();
            var result = shoppingCart.RemoveCartItem(id);
            if (result.status == "error")
                return result;
            if (shoppingCart.Coupon != null)
            {
                var updatediscount = _orderNacGroupService.UpdateDiscount(ref shoppingCart);
                if (updatediscount.status == "error")
                {
                    return new ApiResponseModel("error", updatediscount.message);
                }
            }
            SaveCart(shoppingCart);
            return new ApiResponseModel("success", shoppingCart);

        }

       

        [HttpPost]
        public ActionResult<ApiResponseModel> AddToCart([FromBody] JObject model)
        {
            if (model?["id"] == null || model?["skucode"] == null || model["quantity"] == null) return new ApiResponseModel("error", StaticMessage.ParameterInvalid);
            var skuid = model["id"].ToString();
            var skuCode = model["skucode"].ToString();
            var quantity = model["quantity"].ToObject<int>();
            var shoppingCart = GetShoppingCart();
            var resultItem = _orderNacGroupService.AddItems(ref shoppingCart, skuid, skuCode, quantity);
            var updateShipping = _orderNacGroupService.UpdateShippingTotalBill(shoppingCart);
            if (updateShipping.message == "error") return new ApiResponseModel("error", updateShipping.message);
            if (resultItem.status == "error")
                return resultItem;
            SaveCart(shoppingCart);
          
            return new ApiResponseModel("success", new { NewCart = shoppingCart, NewItemId = shoppingCart?.OrderItems.LastOrDefault()?.Id });
        }

        [HttpPost]
        public ActionResult<ApiResponseModel> AddToCartMulti([FromBody] JArray model)
        {
            if (model == null) return new ApiResponseModel("error", StaticMessage.ParameterInvalid);
            var shoppingCart = GetShoppingCart();
            var skuList = model.Value<JArray>();
            foreach (var sku in skuList)
            {
                var resultItem = _orderNacGroupService.AddItems(ref shoppingCart, sku["id"].ToString(), sku["code"].ToString(), (int)sku["qty"]);
                if (resultItem.status == "error")
                    return resultItem;
            }
            var updateShipping = _orderNacGroupService.UpdateShippingTotalBill(shoppingCart);
            if (updateShipping.message == "error") return new ApiResponseModel("error", updateShipping.message);

            SaveCart(shoppingCart);

            return new ApiResponseModel("success", new { NewCart = shoppingCart, NewItemId = shoppingCart?.OrderItems.LastOrDefault()?.Id });
        }
       

        [HttpPost]
        public ActionResult<ApiResponseModel> UpdateEmail([FromBody] JObject model)
        {
            if (model?["email"] == null) return new ApiResponseModel("error", StaticMessage.ParameterInvalid);
            var email = model["email"].ToObject<string>();
            var shoppingCart = GetShoppingCart();
            shoppingCart.CustomerEmail = email;
            SaveCart(shoppingCart);
            return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
        }

        [HttpPost]
        public ActionResult<ApiResponseModel> UpdatePhone([FromBody] JObject model)
        {
            if (model?["phone"] == null) return new ApiResponseModel("error", StaticMessage.ParameterInvalid);
            var phone = model["phone"].ToObject<string>();
            var shoppingCart = GetShoppingCart();
            shoppingCart.CustomerPhone = phone;
            SaveCart(shoppingCart);
            return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
        }

        [HttpPost]
        public ActionResult<ApiResponseModel> UpdateName([FromBody] JObject model)
        {
            if (model?["name"] == null) return new ApiResponseModel("error", StaticMessage.ParameterInvalid);
            var name = model["name"].ToObject<string>();
            var shoppingCart = GetShoppingCart();
            shoppingCart.FirstName = name;
            SaveCart(shoppingCart);
            return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
        }

        [HttpPost]
        public ActionResult<ApiResponseModel> UpdateGender([FromBody] JObject model)
        {
            if (model?["gender"] == null) return new ApiResponseModel("error", StaticMessage.ParameterInvalid);
            var gender = model["gender"].ToObject<int>();
            var shoppingCart = GetShoppingCart();
            shoppingCart.Gender = (OrderGenderNacGroup)gender;
            SaveCart(shoppingCart);
            return new ApiResponseModel("success",StaticMessage.DataUpdateSuccessfully);
        }

       

        [HttpPost]
        public ActionResult<ApiResponseModel> UpdateShipping([FromBody] JObject model)
        {
            if (model?["model"] == null) return new ApiResponseModel("error", StaticMessage.ParameterInvalid);
            var shippingAddress = model["model"].ToObject<AddressModel>();
            var shoppingCart = GetShoppingCart();
            shoppingCart.FirstName = shippingAddress.FullName;

            shoppingCart.CustomerPhone = shippingAddress.PhoneNumber;
            shoppingCart.City = shippingAddress.City;
            shoppingCart.District = shippingAddress.District;
            shoppingCart.Ward = shippingAddress.Ward;
            shoppingCart.Street = shippingAddress.Street;
            shoppingCart.Recipient.FullName = shippingAddress.FullName;
            shoppingCart.Recipient.Street = shippingAddress.Street;
            shoppingCart.Recipient.District = shippingAddress.District;
            shoppingCart.Recipient.Ward = shippingAddress.Ward;
            shoppingCart.Recipient.PhoneNumber = shippingAddress.PhoneNumber;
            shoppingCart.Recipient.City = shippingAddress.City;
            var result = _orderNacGroupService.UpdateShipping(shoppingCart, shoppingCart.City, shoppingCart.District);
            if (result.status == "error")
            {
                return new ApiResponseModel("error", result.message);
            }

            SaveCart(shoppingCart);
            return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
        }

        [HttpPost]
        public ActionResult<ApiResponseModel> ConfirmCheckout([FromBody] JObject model)
        {

            var shoppingCart = GetShoppingCart();
            var confirmCheckoutResult = _orderNacGroupService.ConfirmCheckout(shoppingCart);
            if (confirmCheckoutResult.status == "success") RemoveCart();
            return confirmCheckoutResult;
        }


        private Models.Schema.Order GetShoppingCart()
        {
            var shoppingCart = HttpContext.Session.Get<Models.Schema.Order>("ShoppingCartNacGroup") ?? new Models.Schema.Order { OrderMxMerchant = new OrderMxMerchant(), OrderGPosInformation = new OrderGPos() };

            var currentUser = User.Identity.IsAuthenticated
                ? _userProfileService.GetById(User.Identity.Name)
                : null;
            if (currentUser != null)
            {

                shoppingCart.CustomerEmail = currentUser.Email;
                shoppingCart.CustomerId = currentUser.Id;
                SaveCart(shoppingCart);
            }


            return shoppingCart;
        }

        private void SaveCart(Models.Schema.Order cart)
        {
            HttpContext.Session.Set("ShoppingCartNacGroup", cart);
        }

        private void RemoveCart()
        {
            HttpContext.Session.Remove("ShoppingCartNacGroup");
        }
    }
}