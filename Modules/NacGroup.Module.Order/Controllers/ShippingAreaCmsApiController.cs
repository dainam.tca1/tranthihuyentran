﻿using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Order.Models.Schema;
using NacGroup.Module.Order.Services;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using Microsoft.Extensions.Caching.Memory;
using System.Linq;


namespace NacGroup.Module.Order.Controllers
{
    [Authorize(Roles = "Admin", Policy = "TenantAuthorize")]
    [Route("cms/api/shippingbyarea/{action=Index}")]
    public class ShippingAreaCmsApiController : Controller
    {
        private readonly IShippingFeeByAreaService _shippingFeeByAreaService;
        private readonly IShippingService _shippingService;
        private readonly IMemoryCache _memoryCache;

        public ShippingAreaCmsApiController(
            IMemoryCache memoryCache,
            IShippingFeeByAreaService shippingFeeByAreaService,
            IShippingService shippingService
            )
        {
            _memoryCache = memoryCache;
            _shippingFeeByAreaService = shippingFeeByAreaService;
            _shippingService = shippingService;
        }


        [HttpGet]
        public ActionResult<ApiResponseModel> Index()
        {
            var paged = _shippingFeeByAreaService.GetAll();

            return new ApiResponseModel("success", new PagedResult<ShippingFeeByDistrict>(paged, 1, paged.Count, paged.Count));
        }

        [HttpGet]
        public IActionResult GetCity()
        {
            var cacheKey = $"_{ConstKey.CityCacheKey}";
            if (_memoryCache.TryGetValue(cacheKey, out List<CityCollection> result)) return Ok(result);

            result = _shippingService.GetCity().ToList();

            if (result.Count < 0)
            {
                return null;
            }
            _memoryCache.Set(cacheKey, result, DateTimeOffset.Now.AddDays(1));
            return Ok(result);
        }

        [HttpGet]
        public IActionResult GetDistrict()
        {
            var cacheKey = $"_{ConstKey.DistrictCacheKey}";
            if (_memoryCache.TryGetValue(cacheKey, out List<DistrictCollection> result)) return Ok(result);

            result = _shippingService.GetDistrict().ToList();

            if (result.Count < 0)
            {
                return null;
            }
            _memoryCache.Set(cacheKey, result, DateTimeOffset.Now.AddDays(1));
            return Ok(result);
        }

        [HttpGet]
        public ActionResult<ApiResponseModel> GetWard()
        {
            return new ApiResponseModel("success", _shippingService.GetWard());
        }


        [HttpPost]
        public ActionResult<ApiResponseModel> RemoveList([FromBody] List<string> idList)
        {
            return _shippingFeeByAreaService.Delete(idList);
        }

        [HttpPost]
        public ActionResult<ApiResponseModel> Add([FromBody] ShippingFeeByDistrict model)
        {
            return _shippingFeeByAreaService.Add(model);
        }

        public ActionResult<ApiResponseModel> Update(string id)
        {
            var item = _shippingFeeByAreaService.GetById(id);
            if (item == null)
                return new ApiResponseModel("error", StaticMessage.DataNotFound);

            return new ApiResponseModel("success", item);
        }
        [HttpPost]
        public ActionResult<ApiResponseModel> Update([FromBody] ShippingFeeByDistrict model)
        {
            return _shippingFeeByAreaService.Update(model);
        }

    }
}
