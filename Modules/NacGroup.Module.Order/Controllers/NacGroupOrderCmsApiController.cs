﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Core.Services;
using NacGroup.Module.Order.Models.Schema;
using NacGroup.Module.Order.Services;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NacGroup.Module.Order.Controllers
{
    [Authorize(Roles = "Admin", Policy = "TenantAuthorize")]
    [Route("cms/api/orderhistories/{action=Index}")]
    public class NacGroupOrderCmsApiController : Controller
    {
        private readonly IOrderNacGroupCmsService _orderNacGroupCmsService;
        private readonly GlobalConfiguration _globalConfiguration;
        private readonly DateTime _timeZone;

        public NacGroupOrderCmsApiController(
            IOrderNacGroupCmsService orderNacGroupCmsService,
             IGettingGlobalConfigService gettingGlobalConfigService
            )
        {
            _orderNacGroupCmsService = orderNacGroupCmsService;
            _globalConfiguration = gettingGlobalConfigService.GetConfiguration();
            _timeZone = TimeZoneInfo.ConvertTime(DateTime.Today,_globalConfiguration.LocalTimeZoneInfo);

        }

        [HttpGet]
        public ActionResult<ApiResponseModel> Index(string ordernumber, string customername, string phone, int? status, string fromDate, string toDate, int page = 1, int pagesize = 10)
        {
            return new ApiResponseModel("success", _orderNacGroupCmsService.Filter(ordernumber, customername,phone,status, fromDate, toDate, page, pagesize));
        }

       

        [HttpGet]
        public ActionResult<ApiResponseModel> GetUpdate(string id)
        {
            var item = _orderNacGroupCmsService.GetById(id);
            if (item == null)
                return new ApiResponseModel("error", StaticMessage.DataNotFound);

            return new ApiResponseModel("success", item);
        }

        [HttpPost]
        public ActionResult<ApiResponseModel> Update([FromBody]Models.Schema.Order order)
        {
            var item = _orderNacGroupCmsService.Update(order);
            if (item == null)
                return new ApiResponseModel("error", StaticMessage.DataNotFound);

            return new ApiResponseModel("success", item);
        }

        [HttpPost]
        public ActionResult<ApiResponseModel> UpdateStatus([FromBody]JObject data)
        {
            if (data == null || data["id"] == null || data["status"] == null) return new ApiResponseModel("error", StaticMessage.ParameterInvalid);
            var id = data["id"].Value<string>();
            var status = (OrderStatus)data["status"]?.Value<int>();
            return _orderNacGroupCmsService.UpdateStatus(id,status);
        }


        [HttpPost]
        public ActionResult<ApiResponseModel> UpdateCustomize([FromBody]JObject data)
        {
            var model = data["model"].ToObject<Models.Schema.Order>();
            var name = data["name"].Value<string>();
            return _orderNacGroupCmsService.UpdateCustomize(model, name);
        }


        #region Dashboard

        [HttpGet]
        public ActionResult<ApiResponseModel> GetCount()
        {
            var firstDayOfMonth = new DateTime(_timeZone.Date.Year, _timeZone.Date.Month, 1);
            return new ApiResponseModel("success",
                Json(new
                {
                    TotalSales = _orderNacGroupCmsService.GetTotalRevenue(null),
                    OrderCount = _orderNacGroupCmsService.GetToTalOrderCount(),
                    TotalSalesToday = _orderNacGroupCmsService.GetTotalRevenue(_timeZone.Date, _timeZone.Date.AddDays(1), null),
                    TotalSalesYesterday = _orderNacGroupCmsService.GetTotalRevenue(_timeZone.Date.AddDays(-1), _timeZone.Date, null),
                    TotalSalesLastweek = _orderNacGroupCmsService.GetTotalRevenue(_timeZone.Date.AddDays(-7), _timeZone.Date, null),
                    TotalSalesMonth = _orderNacGroupCmsService.GetTotalRevenue(firstDayOfMonth, firstDayOfMonth.AddMonths(1), null),
                    TotalSalesLastMonth = _orderNacGroupCmsService.GetTotalRevenue(firstDayOfMonth.AddMonths(-1), firstDayOfMonth, null),
                    OrderCountToday = _orderNacGroupCmsService.GetToTalOrderCount(_timeZone.Date, _timeZone.Date.AddDays(1), null),
                    OrderCountYesterday = _orderNacGroupCmsService.GetToTalOrderCount(_timeZone.Date.AddDays(-1), _timeZone.Date, null),
                    OrderCountLastweek = _orderNacGroupCmsService.GetToTalOrderCount(_timeZone.Date.AddDays(-7), _timeZone.Date, null),
                    OrderCountMonth = _orderNacGroupCmsService.GetToTalOrderCount(firstDayOfMonth, firstDayOfMonth.AddMonths(1), null),
                    OrderCountLastMonth = _orderNacGroupCmsService.GetToTalOrderCount(firstDayOfMonth.AddMonths(-1), firstDayOfMonth, null)
                })) ;
        }

        [HttpGet]
        public ActionResult<ApiResponseModel> Today()
        {
            var timelist = new List<DateTime>();
            return new ApiResponseModel("success", Json(new
            {
                ProductBestSelling = _orderNacGroupCmsService.GetListOrder(_timeZone.Date, _timeZone.Date.AddDays(1), null,1,5),
                TotalSalesList = new List<List<decimal>> { _orderNacGroupCmsService.TotalSalesChart(_timeZone.Date, 8, 3, StatisticalTimeType.Hour, null, ref timelist) },
                Labels = new List<string> { "0h - 3h", "3h - 6h", "6h - 9h", "9h - 12h", "12h - 15h", "15h - 18h", "18h - 21h", "21h - 24h" }
            }));
        }

        [HttpGet]
        public ActionResult<ApiResponseModel> Yesterday()
        {
            var timelist = new List<DateTime>();
            return new ApiResponseModel("success", Json(new
            {
                ProductBestSelling = _orderNacGroupCmsService.GetListOrder(_timeZone.Date.AddDays(-1), _timeZone.Date, null, 1, 5),
                TotalSalesList = new List<List<decimal>> { _orderNacGroupCmsService.TotalSalesChart(_timeZone.Date.AddDays(-1), 8, 3, StatisticalTimeType.Hour, null, ref timelist) },
                Labels = new List<string> { "0h - 3h", "3h - 6h", "6h - 9h", "9h - 12h", "12h - 15h", "15h - 18h", "18h - 21h", "21h - 24h" }
            }));
        }

        [HttpGet]
        public ActionResult<ApiResponseModel> LastWeek()
        {
            var timelist = new List<DateTime>();
            timelist.Remove(timelist.LastOrDefault());     
            return new ApiResponseModel("success", Json(new
            {
                ProductBestSelling = _orderNacGroupCmsService.GetListOrder(_timeZone.Date.AddDays(-7), _timeZone.Date, null, 1, 5),
                TotalSalesList = new List<List<decimal>> { _orderNacGroupCmsService.TotalSalesChart(_timeZone.Date.AddDays(-7), 7, 1, StatisticalTimeType.Day, null, ref timelist) },
                Labels = timelist.Select(m => m.ToString("dd/MM/yyyy")).ToList()
            }));
        }

        [HttpGet]
        public ActionResult<ApiResponseModel> Month()
        {
            var firstDayOfMonth = new DateTime(_timeZone.Date.Year, _timeZone.Date.Month, 1);
            var timelist = new List<DateTime>();
            var totalsalelist = new List<List<decimal>>
            {
                _orderNacGroupCmsService.TotalSalesChart(firstDayOfMonth, DateTime.DaysInMonth(firstDayOfMonth.Year,firstDayOfMonth.Month), 1, StatisticalTimeType.Day, null,ref timelist)
            };
            timelist.Remove(timelist.LastOrDefault());
            return new ApiResponseModel("success", Json(new
            {
                ProductBestSelling = _orderNacGroupCmsService.GetListOrder(firstDayOfMonth, firstDayOfMonth.AddMonths(1), null, 1, 5),
                TotalSalesList = totalsalelist,
                Labels = timelist.Select(m => m.ToString("dd")).ToList()
            }));
        }

        [HttpGet]
        public ActionResult<ApiResponseModel> LastMonth()
        {
            var firstDayOfMonth = new DateTime(_timeZone.Date.Year, _timeZone.Date.Month, 1).AddMonths(-1);
            var timelist = new List<DateTime>();
            var totalsalelist = new List<List<decimal>>
            {
                _orderNacGroupCmsService.TotalSalesChart(firstDayOfMonth, DateTime.DaysInMonth(firstDayOfMonth.Year,firstDayOfMonth.Month), 1, StatisticalTimeType.Day, null,ref timelist)
            };
            timelist.Remove(timelist.LastOrDefault());
            return new ApiResponseModel("success", Json(new
            {
                ProductBestSelling = _orderNacGroupCmsService.GetListOrder(firstDayOfMonth, new DateTime(_timeZone.Date.Year, _timeZone.Date.Month, 1), null, 1, 5),
                TotalSalesList = totalsalelist,
                Labels = timelist.Select(m => m.ToString("dd")).ToList()
            }));
        }
        #endregion
    }
}
