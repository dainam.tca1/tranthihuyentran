﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Order.Models.Schema;
using NacGroup.Module.Order.Services;

namespace NacGroup.Module.Order.Controllers
{
    [Authorize(Roles = "Admin", Policy = "TenantAuthorize")]
    [Route("cms/api/coupon/{action=Index}")]
    public class CouponCmsApiController : Controller
    {
        private readonly ICouponService _couponService;

        public CouponCmsApiController(ICouponService couponService)
        {
            _couponService = couponService;
        }

        [HttpGet]
        public ActionResult<ApiResponseModel> Index(string code,bool? isactive,int page = 1, int pagesize = 10) =>
            new ApiResponseModel("success", _couponService.Get(code,isactive,page, pagesize));

        [HttpPost]
        public ActionResult<ApiResponseModel> RemoveList([FromBody] List<string> idList)
        {
            return _couponService.Delete(idList);
        }

        [HttpPost]
        public ActionResult<ApiResponseModel> Add([FromBody] Coupon model)
        {
            return _couponService.Add(model);
        }

        public ActionResult<ApiResponseModel> GetUpdate(string id)
        {
            var item = _couponService.GetById(id);
            if (item == null)
                return new ApiResponseModel("error", StaticMessage.DataNotFound);

            return new ApiResponseModel("success", item);
        }
        [HttpPost]
        public ActionResult<ApiResponseModel> Update([FromBody] Coupon model)
        {
            return _couponService.Update(model);
        }
    }
}
