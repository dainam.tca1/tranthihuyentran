﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Order.Models;
using NacGroup.Module.Order.Services;

namespace NacGroup.Module.Order.Controllers
{
    [Authorize(Roles = "Admin", Policy = "TenantAuthorize")]
    [Route("cms/api/gposshoppingcartsetting/{action=Index}")]
    public class GPosShoppingCartSettingCmsApiController : Controller
    {
        private readonly IOrderPickUpSettingService _gPosShoppingCartSettingService;

        public GPosShoppingCartSettingCmsApiController(IOrderPickUpSettingService gPosShoppingCartSettingService)
        {
            _gPosShoppingCartSettingService = gPosShoppingCartSettingService;
        }

        [HttpGet]
        public ActionResult<ApiResponseModel> Index()
        {
            return new ApiResponseModel("success", _gPosShoppingCartSettingService.Get());
        }


        [HttpPost]
        public ActionResult<ApiResponseModel> Update([FromBody] OrderPickUpSetting model)
        {
            return _gPosShoppingCartSettingService.Update(model);
        }

    }
}
