﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NacGroup.Module.Core.Services;

using NacGroup.Module.Core.Models;
using NacGroup.Module.Order.Services;
using System.Linq;
using NacGroup.Module.Core.Interfaces;

namespace NacGroup.Module.Order.Controllers
{
    public class OrderHistoryController : Controller
    {
        private readonly IUserProfileService _userProfileService;
        private readonly IOrderNacGroupCmsService _orderNacGroupCmsService;
        private readonly Tenant _tenant;

        public OrderHistoryController(
          
            IOrderNacGroupCmsService orderNacGroupCmsService,
            ITenantProvider tenantProvider,
            IUserProfileService userProfileService
            )
        {
            _userProfileService = userProfileService;
            _tenant = tenantProvider.GetTenant();
            _orderNacGroupCmsService = orderNacGroupCmsService;
        }


        [Authorize(Policy = "TenantAuthorize")]
        [Route("/api/accountapp/order-histories", Order = 0)]
        public ActionResult<ApiResponseModel> OrderHistories(int page = 1,int pagesize = 10)
        {
            var currentUser = _userProfileService.GetById(User.Identity.Name);
            if (currentUser == null)
            {
                HttpContext.SignOutAsync();
                return Redirect("/account/login?ReturnUrl=/accountapp/orderhistory");
            }
            var order = _orderNacGroupCmsService.GetOrderByCustomerId(currentUser.Id, page, pagesize);
            return new ApiResponseModel("success", order);
        }


        [Authorize(Policy = "TenantAuthorize")]
        [Route("/api/accountapp/order-histories/{orderNumber}", Order = 1)]
        public ActionResult<ApiResponseModel> OrderHistoriesDetail(string orderNumber)
        {
            var currentUser = _userProfileService.GetById(User.Identity.Name);
            if (currentUser == null)
            {
                HttpContext.SignOutAsync();
                return Redirect("/account/login?ReturnUrl=/accountapp/orderhistory");
            }
            var order = _orderNacGroupCmsService.Get().FirstOrDefault(m => m.OrderNumber == orderNumber && m.CustomerId == currentUser.Id);
            if (order == null)
            {
                HttpContext.SignOutAsync();
                return Redirect("/account/login?ReturnUrl=/accountapp/orderhistory");
            }
            return new ApiResponseModel("success", order);
        }


    }
}