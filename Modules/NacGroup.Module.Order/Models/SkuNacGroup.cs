﻿using System;
using System.Collections.Generic;
namespace NacGroup.Module.Order.Models
{
    public class SkuNacGroup
    {
        public string SkuCode { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public decimal SpecialPrice { get; set; }
        public decimal StockQuantity { get; set; }
        public decimal Weight { get; set; }
        public decimal Height { get; set; }
        public decimal Width { get; set; }
        public string Category { get; set; }
        public decimal Length { get; set; }
        public int Sort { get; set; }
        public decimal SaleTaxRate { get; set; }
        public List<ProductAttribute> Attributes { get; set; }
        public List<ProductImage> ImageList { get; set; }
        public bool IsActive { get; set; }
        public bool IsInventory { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
    }

    public class ProductAttribute
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Slug { get; set; }
        public int Sort { get; set; }
        public List<ProductAttributeValue> Values { get; set; }
    }

    public class ProductAttributeValue
    {
        public string Id { get; set; }   
        public string Value { get; set; }     
        public string Slug { get; set; }
        public int Sort { get; set; }
    }
    public class ProductImage
    {
        public string Id { get; set; }
        public string Src { get; set; }
        public string Alt { get; set; }
        public int Sort { get; set; }
    }
}
