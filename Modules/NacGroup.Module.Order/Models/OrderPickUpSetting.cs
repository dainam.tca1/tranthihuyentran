﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using MongoDB.Bson;
using NacGroup.Module.Core.Models.Schema;
using Newtonsoft.Json;

namespace NacGroup.Module.Order.Models
{
    public class OrderPickUpSetting
    {
        public List<OrderPickUpDateOff> DayOff { get; set; }
        public List<OrderPickUpSpecialDate> DaySpecial { get; set; }
        public List<OrderPickUpWorkingDate> DayWork { get; set; }
        public int SessionLength { get; set; }
        public int MaxOrderPerSession { get; set; }
        public MxMerchantPaymentType MxPaymentType { get; set; }

        public static OrderPickUpSetting MapFromAppSetting(AppSetting appSetting)
        {
            if (appSetting == null || appSetting.Value == null) return null;
            return JsonConvert.DeserializeObject<OrderPickUpSetting>(appSetting.Value.ToJson());
        }

        public static OrderPickUpSetting Create()
        {
            return new OrderPickUpSetting
            {
                DayOff = new List<OrderPickUpDateOff>(),
                DaySpecial = new List<OrderPickUpSpecialDate>(),
                DayWork = new List<OrderPickUpWorkingDate>
                {
                    new OrderPickUpWorkingDate {Day = "Monday", DayIndex = DayOfWeek.Monday},
                    new OrderPickUpWorkingDate {Day = "Tuesday", DayIndex = DayOfWeek.Tuesday},
                    new OrderPickUpWorkingDate {Day = "Wednesday", DayIndex = DayOfWeek.Wednesday},
                    new OrderPickUpWorkingDate {Day = "Thursday", DayIndex = DayOfWeek.Thursday},
                    new OrderPickUpWorkingDate {Day = "Friday", DayIndex = DayOfWeek.Friday},
                    new OrderPickUpWorkingDate {Day = "Saturday", DayIndex = DayOfWeek.Saturday},
                    new OrderPickUpWorkingDate {Day = "Sunday", DayIndex = DayOfWeek.Sunday}
                }
            };
        }


        public List<OrderPickUpAvailableDate> GetAvalableDateList(int numberOfDate, DateTime dateTimeNow)
        {
            if (numberOfDate <= 0)
                return new List<OrderPickUpAvailableDate>();
            var result = new List<OrderPickUpAvailableDate>();
            var dateTimeNowClone = dateTimeNow;
            while (result.Count < numberOfDate)
            {
                if (DayOff.Any(m => m.Day.Date.CompareTo(dateTimeNow.Date) == 0))
                {
                    dateTimeNow = dateTimeNow.AddDays(1);
                    continue;
                }

                List<OrderPickUpAvailableDateHourItem> hourList;
                var specialDay = DaySpecial.FirstOrDefault(m => m.Day.Date.CompareTo(dateTimeNow.Date) == 0);
                if (specialDay != null)
                {
                    hourList = GetWorkingHourListbyDate(specialDay.Day, specialDay.Open, specialDay.Close);
                }
                else
                {
                    var workday = DayWork.FirstOrDefault(m => m.DayIndex == dateTimeNow.DayOfWeek);
                    if (workday == null || !workday.IsActive)
                    {
                        dateTimeNow = dateTimeNow.AddDays(1);
                        continue;
                    }

                    hourList = GetWorkingHourListbyDate(dateTimeNow.Date, workday.Open, workday.Close);
                }

                if (hourList.Count > 0)
                {
                    if (dateTimeNow.Date.CompareTo(dateTimeNowClone
                            .Date) == 0)
                    {
                        var minHours = dateTimeNow.AddMinutes(10);
                        hourList = hourList.Where(m =>
                            dateTimeNow.Date.AddHours(m.Hour).AddMinutes(m.Min) > minHours).ToList();
                    }
                    result.Add(new OrderPickUpAvailableDate
                    {
                        Day = dateTimeNow.Date,
                        HourList = hourList
                    });
                }

                dateTimeNow = dateTimeNow.AddDays(1);
            }

            return result;
        }

        private List<OrderPickUpAvailableDateHourItem> GetWorkingHourListbyDate(DateTime seletedDate, string openHours, string closeHours)
        {
            var result = new List<OrderPickUpAvailableDateHourItem>();
            try
            {
                var startHourParse = DateTime.ParseExact(openHours, "h:mm tt",
                    CultureInfo.InvariantCulture, DateTimeStyles.None);
                var endHourParse = DateTime.ParseExact(closeHours, "h:mm tt",
                    CultureInfo.InvariantCulture, DateTimeStyles.None);
                var startSeletedDate = seletedDate.Date.Add(startHourParse.TimeOfDay);
                var endSeletedDate = seletedDate.Date.Add(endHourParse.TimeOfDay);
                while (startSeletedDate.CompareTo(endSeletedDate) < 0)
                {
                    result.Add(new OrderPickUpAvailableDateHourItem(startSeletedDate.Hour,startSeletedDate.Minute));
                    startSeletedDate = startSeletedDate.AddMinutes(SessionLength);
                }
                result.Add(new OrderPickUpAvailableDateHourItem(endSeletedDate.Hour, endSeletedDate.Minute));
                return result;
            }
            catch(Exception e)
            {
                return result;
            }
        }
    }

    public class OrderPickUpSpecialDate
    {
        public DateTime Day { get; set; }
        public string Open { get; set; }
        public string Close { get; set; }
    }

    public class OrderPickUpWorkingDate
    {
        public string Day { get; set; }
        public string Open { get; set; }
        public string Close { get; set; }
        public DayOfWeek DayIndex { get; set; }
        public bool IsActive { get; set; }
    }

    public class OrderPickUpDateOff
    {
        public DateTime Day { get; set; }
    }

    public class OrderPickUpAvailableDate
    {
        public OrderPickUpAvailableDate()
        {
            HourList = new List<OrderPickUpAvailableDateHourItem>();
        }
        public DateTime Day { get; set; }
        public List<OrderPickUpAvailableDateHourItem> HourList { get; set; }
    }
    public class OrderPickUpAvailableDateHourItem
    {
        public OrderPickUpAvailableDateHourItem(int hour, int min)
        {
            Hour = hour;
            Min = min;
        }
        public int Hour { get; set; }
        public int Min { get; set; }
    }

    public enum MxMerchantPaymentType
    {
        WebsiteForm,
        MxMerchantForm
    }
}
