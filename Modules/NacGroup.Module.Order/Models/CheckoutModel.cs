﻿namespace NacGroup.Module.Order.Models
{
    public class CheckoutModel
    {
        public string CartNumber { get; set; }
        public string ExpirationMonth { get; set; }
        public string ExpirationYear { get; set; }
        public string CcvCode { get; set; }
    }
}
