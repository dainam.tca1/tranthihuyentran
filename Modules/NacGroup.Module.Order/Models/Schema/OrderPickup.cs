﻿using System;
using System.Collections.Generic;
using System.Linq;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using NacGroup.Module.Core.Models;

namespace NacGroup.Module.Order.Models.Schema
{
    [BsonIgnoreExtraElements]
    public class OrderPickup : Entity, ITrackingUpdate
    {
        public OrderPickup()
        {
            OrderGPosInformation = new OrderGPosPickup(); 
            OrderMxMerchant = new OrderMxMerchantPickup();
            OrderItems = new List<OrderItemPickup>();
            ItemQuantityChanged += UpdateSubTotal;
            ItemQuantityChanged += UpdateTotalTax;
            SubTotalChanged += UpdateTotalAmount;
            TotalTaxChanged += UpdateTotalAmount;
            TipsChanged += UpdateTotalAmount;
            DiscountChanged += UpdateTotalAmount;
        }


        #region Properties
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("FirstName")]
        public string FirstName { get; set; }

        [BsonElement("LastName")]
        public string LastName { get; set; }

        [BsonElement("Gender")]
        public OrderGenderNacGroup Gender { get; set; }

        [BsonElement("CustomerPhone")]
        public string CustomerPhone { get; set; }

        private string _customerEmail;
        [BsonElement("CustomerEmail")]
        public string CustomerEmail
        {
            get => _customerEmail;
            set => _customerEmail = value?.ToLower();
        }

        [BsonElement("ShoppingCartItems")]
        public List<OrderItemPickup> OrderItems { get; set; }
        [BsonElement("Notes")]
        public string Notes { get; set; }
        [BsonElement("Tips")]
        public decimal Tips { get; set; }
        [BsonElement("DiscountAmount")]
        public decimal DiscountAmount { get; set; }
        [BsonElement("TotalAmount")]
        public decimal TotalAmount { get; set; }
        [BsonElement("SubTotalAmount")]
        public decimal SubTotalAmount { get; set; }
        [BsonElement("TotalTax")]
        public decimal TotalTax { get; set; }
        [BsonElement("CustomerId")]
        public string CustomerId { get; set; }
        [BsonElement("CreatedDate")]
        public DateTime CreatedDate { get; set; }
        [BsonElement("UpdatedDate")]
        public DateTime UpdatedDate { get; set; }
        [BsonElement("Coupon")]
        public OrderCoupon Coupon { get; set; }
        [BsonElement("OrderGPosInformation")]
        public OrderGPosPickup OrderGPosInformation { get; set; }
        public OrderMxMerchantPickup OrderMxMerchant { get; set; }
        public OrderPickupPaymentStatus PaymentStatus { get; set; }

        public object this[string propertyName]
        {
            get => this.GetType().GetProperty(propertyName).GetValue(this, null);
            set => this.GetType().GetProperty(propertyName).SetValue(this, value, null);
        }
        #endregion

        #region Methods

        private void UpdateTotalAmount(object sender, EventArgs e)
        {
            TotalAmount = SubTotalAmount + TotalTax + Tips + DiscountAmount;
        }

        private void UpdateSubTotal(object sender, EventArgs e)
        {
            SubTotalAmount = OrderItems.Sum(m => m.Price * m.Quantity);
            OnSubTotalChanged();
        }

        private void UpdateTotalTax(object sender, EventArgs e)
        {
            TotalTax = Math.Round(OrderItems.Sum(m => m.Price * m.Quantity * (m.SaleTaxRate / 100)), 2, MidpointRounding.AwayFromZero);
            OnTotalTaxChanged();
        }
        public ApiResponseModel AddToCart(OrderItemPickup cartItem)
        {
            try
            {
                if(cartItem.Quantity <= 0 || cartItem.Price < 0 || string.IsNullOrEmpty(cartItem.Name) || string.IsNullOrEmpty(cartItem.SkuId) || string.IsNullOrEmpty(cartItem.SkuCode)) 
                    return new ApiResponseModel("error",StaticMessage.ParameterInvalid);

                var oldItem = OrderItems.FirstOrDefault(m => m.Id == cartItem.Id);
                if (oldItem == null)
                {
                    OrderItems.Add(cartItem);
                    OnItemQuantityChanged();
                    return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully, cartItem.Id);
                }

                UpdateQuantity(cartItem.Id, oldItem.Quantity + cartItem.Quantity);
                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully, cartItem.Id);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }

        }

        public ApiResponseModel UpdateTips(decimal amount)
        {
            try
            {
                if (amount < 0) return new ApiResponseModel("error", "Tip must be more than zero");
                Tips = amount;
                OnTipsChanged();
                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }

        public ApiResponseModel UpdateQuantity(Guid itemId, int quantity)
        {
            try
            {
                #region Validate
                if (quantity <= 0)
                    return new ApiResponseModel("error", StaticMessage.ParameterInvalid);
                var oldItem = OrderItems.FirstOrDefault(m => m.Id == itemId);
                if (oldItem == null)
                {
                    return new ApiResponseModel("error", StaticMessage.DataNotFound);
                }
                #endregion

                oldItem.UpdateQuantity(quantity);
                OnItemQuantityChanged();
                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }

        
        public ApiResponseModel RemoveCartItem(Guid itemId)
        {
            try
            {
                #region Validate
                var oldItem = OrderItems.FirstOrDefault(m => m.Id == itemId);
                if (oldItem == null)
                {
                    return new ApiResponseModel("error", StaticMessage.DataNotFound);
                }
                #endregion

                OrderItems.Remove(oldItem);
                OnItemQuantityChanged();
                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }
        public ApiResponseModel ClearCoupon()
        {
            try
            {
                Coupon = null;
                DiscountAmount = 0;
                OnDiscountChanged();
                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }
        public ApiResponseModel UpdateDiscount(OrderCoupon orderCoupon)
        {
            try
            {
                if (orderCoupon == null || string.IsNullOrEmpty(orderCoupon.CouponCode) || orderCoupon.Value <= 0)
                    return new ApiResponseModel("error", "Promotion code is not avalable");

                Coupon = orderCoupon;
                DiscountAmount = Math.Round(Coupon.ValueType == CouponValueType.Percent
                                    ? (SubTotalAmount * Coupon.Value / 100)
                                    : Coupon.Value, 2) * -1;
                OnDiscountChanged();
                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }
        public ApiResponseModel ApplyCoupon(OrderCoupon orderCoupon)
        {
            try
            {
                if (orderCoupon == null || string.IsNullOrEmpty(orderCoupon.CouponCode) || orderCoupon.Value <= 0)
                    return new ApiResponseModel("error", "Promotion code is not avalable");

                Coupon = orderCoupon;
                DiscountAmount = Math.Round(Coupon.ValueType == CouponValueType.Percent
                                     ? (SubTotalAmount * Coupon.Value / 100)
                                     : Coupon.Value, 2) * -1;
                OnDiscountChanged();
                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }
        public ApiResponseModel UpdateModifiers(Guid itemId, IEnumerable<OrderModifierItemPickup> modifierList)
        {
            try
            {
                var modifierGPosItems = modifierList.ToList();
                if (!modifierGPosItems.Any())
                    return new ApiResponseModel("error", StaticMessage.ParameterInvalid);

                var cartItem = OrderItems.FirstOrDefault(m => m.Id == itemId);

                if (cartItem == null)
                    return new ApiResponseModel("error", StaticMessage.ParameterInvalid);

                var result = cartItem.UpdateModifier(modifierGPosItems);
                if (result.status == "error") return result;
                OnItemQuantityChanged();
                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }

        public ApiResponseModel UpdateSpecialRequest(Guid itemId, string note)
        {
            try
            {
                var cartItem = OrderItems.FirstOrDefault(m => m.Id == itemId);
                if (cartItem == null)
                    return new ApiResponseModel("error", StaticMessage.ParameterInvalid);
                var result = cartItem.UpdateSpecialRequest(note);
                return result;
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }
        #endregion

        #region Events

        /// <summary>
        /// Sự kiện được kích hoạt sau khi số lượng sản phẩm trong giỏ hàng thay đổi (Thêm, tăng giảm số lượng, xóa sản phẩm)
        /// </summary>
        public event EventHandler ItemQuantityChanged;

        /// <summary>
        /// Sự kiện kích hoạt khi subtotal thay đổi
        /// </summary>
        public event EventHandler SubTotalChanged;

        public event EventHandler TotalTaxChanged;
        public event EventHandler TipsChanged;
        public event EventHandler DiscountChanged;
        public virtual void OnItemQuantityChanged()
        {
            ItemQuantityChanged?.Invoke(this, EventArgs.Empty);
        }

        public virtual void OnSubTotalChanged()
        {
            SubTotalChanged?.Invoke(this, EventArgs.Empty);
        }

        public virtual void OnTotalTaxChanged()
        {
            TotalTaxChanged?.Invoke(this, EventArgs.Empty);
        }
        protected virtual void OnTipsChanged()
        {
            TipsChanged?.Invoke(this, EventArgs.Empty);
        }

        protected virtual void OnDiscountChanged()
        {
            DiscountChanged?.Invoke(this, EventArgs.Empty);
        }
      
        #endregion

    }


    public class OrderItemPickup
    {
        public OrderItemPickup()
        {
            ModifierItems = new List<OrderModifierItemPickup>();
        }

        /// <summary>
        /// Id của item giỏ hàng sẽ được tạo mới mỗi khi add to cart
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Id sku sản phẩm
        /// </summary>
        public string SkuId { get; set; }
        /// <summary>
        /// Mã sku sản phẩm
        /// </summary>
        public string SkuCode { get; set; }
        public string Name { get; set; }
        public int Quantity { get; set; }
        public string Avatar { get; set; }
        /// <summary>
        /// Yêu cầu riêng của khách đối với mỗi item
        /// </summary>
        public string SpecialRequest { get; set; }
        /// <summary>
        /// Giá của item
        /// </summary>
        public decimal Price
        {
            get { return SkuPrice + ModifierItems.Sum(m => m.Price); }
        }

        /// <summary>
        /// Giá của sku chưa tính khuyến mãi và modifier
        /// </summary>
        public decimal SkuPrice { get; set; }

        public List<OrderModifierItemPickup> ModifierItems { get; set; }

        /// <summary>
        /// Phần trăm thuế tính trên giá của item
        /// </summary>
        public decimal SaleTaxRate { get; set; }


        public void UpdateQuantity(int quantity)
        {
            Quantity = quantity;
        }

        public ApiResponseModel UpdateModifier(IEnumerable<OrderModifierItemPickup> modifierList)
        {
            var modifierGPosItems = modifierList.ToList();
            if (!modifierGPosItems.Any())
                return new ApiResponseModel("error", StaticMessage.ParameterInvalid);
            ModifierItems = modifierGPosItems;
            return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
        }

        public ApiResponseModel UpdateSpecialRequest(string note)
        {
            SpecialRequest = note;
            return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
        }
    }

    public class OrderModifierItemPickup
    {
        public OrderModifierItemPickup()
        {
            Categories = new List<string>();
        }
        public string Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public string Note { get; set; }
        public List<string> Categories { get; set; }
    }

    public class OrderCouponPickup
    {
        public string Id { get; set; }
        public string CouponCode { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime ExpiredDate { get; set; }
        public decimal Value { get; set; }
        public CouponValueType ValueType { get; set; }
    }

    public class OrderGPosPickup
    {
        #region Properties
        public string OrderWebId { get; set; }
        public string OrderPosId { get; set; }
        public string OrderPosNumber { get; set; }
        public DateTime OrderDateTime { get; set; }
        public string GPosCustomerId { get; set; }
        #endregion
    }

    public class OrderMxMerchantPickup
    {
        public string AuthMessage { get; set; }
        public string CardType { get; set; }
        public string AuthCode { get; set; }
        public string AcctNo { get; set; }
        public string RecordId { get; set; }
    }

    public enum OrderPickupPaymentStatus
    {
        Spending,
        Paid
    }
}
