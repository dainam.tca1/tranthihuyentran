﻿using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using NacGroup.Module.Core.Models;

namespace NacGroup.Module.Order.Models.Schema
{
    [BsonIgnoreExtraElements]
    public class OrderUserProfile : Entity
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("UserProfileId")]
        public string UserProfileId { get; set; }

        [BsonElement("UsedCoupons")]
        public List<string> UsedCoupons { get; set; }

    }
}
