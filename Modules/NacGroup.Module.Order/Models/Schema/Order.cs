﻿using System;
using System.Collections.Generic;
using System.Linq;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Core.Models.Schema;
using Remotion.Linq.Parsing.ExpressionVisitors.MemberBindings;

namespace NacGroup.Module.Order.Models.Schema
{
    [BsonIgnoreExtraElements]
    public class Order : Entity, ITrackingUpdate
    {
        public Order()
        {
            OrderItems = new List<OrderItem>();
            Recipient = new AddressModel();
            ItemQuantityChanged += UpdateSubTotal;
            ItemQuantityChanged += UpdateTotalTax;
            SaleTaxRateChanged += UpdateTotalTax; 
            SubTotalChanged += UpdateTotalAmount;
            TotalTaxChanged += UpdateTotalAmount;
            TipsChanged += UpdateTotalAmount;
            DiscountChanged += UpdateTotalAmount;
            ShippingChanged += UpdateTotalAmount;
        }


        #region Properties
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("FirstName")]
        public string FirstName { get; set; }

        [BsonElement("LastName")]
        public string LastName { get; set; }

        [BsonElement("Gender")]
        public OrderGenderNacGroup Gender { get; set; }

        [BsonElement("CustomerPhone")]
        public string CustomerPhone { get; set; }
        [BsonElement("City")]
        public string City { get; set; }
        [BsonElement("District")]
        public string District { get; set; }
        [BsonElement("Ward")]
        public string Ward { get; set; }
        [BsonElement("Street")]
        public string Street { get; set; }

        private string _customerEmail;
        [BsonElement("CustomerEmail")]
        public string CustomerEmail
        {
            get => _customerEmail;
            set => _customerEmail = value?.ToLower();
        }

        [BsonElement("ShoppingCartItems")]
        public List<OrderItem> OrderItems { get; set; }
        [BsonElement("Notes")]
        public string Notes { get; set; }
        [BsonElement("Tips")]
        public decimal Tips { get; set; }
        [BsonElement("DiscountAmount")]
        public decimal SaleTaxRate { get; set; }
        [BsonElement("SaleTaxRate")]
        public decimal DiscountAmount { get; set; }
        [BsonElement("TotalAmount")]
        public decimal TotalAmount { get; set; }
        [BsonElement("SubTotalAmount")]
        public decimal SubTotalAmount { get; set; }
        [BsonElement("ShippingAmount")]
        public decimal ShippingAmount { get; set; }
        [BsonElement("TotalTax")]
        public decimal TotalTax { get; set; }
        [BsonElement("CustomerId")]
        public string CustomerId { get; set; }
        [BsonElement("CreatedDate")]
        public DateTime CreatedDate { get; set; }
        [BsonElement("UpdatedDate")]
        public DateTime UpdatedDate { get; set; }
        [BsonElement("Coupon")]
        public OrderCoupon Coupon { get; set; }
        [BsonElement("OrderGPosInformation")]
        public OrderGPos OrderGPosInformation { get; set; }
        public OrderMxMerchant OrderMxMerchant { get; set; }
        [BsonElement("OrderStatusNacGroup")]
        public OrderStatusNacGroup OrderStatusNacGroup { get; set; }
        [BsonElement("OrderPaymentStatusNacGroup")]
        public OrderPaymentStatusNacGroup OrderPaymentStatusNacGroup { get; set; }
        [BsonElement("OrderPaymentMethodNacGroup")]
        public OrderPaymentMethodNacGroup OrderPaymentMethodNacGroup { get; set; }
        [BsonElement("OrderNumber")]
        public string OrderNumber { get; set; }
        [BsonElement("EasyShipId")]
        public string EasyShipId { get; set; }

        [BsonElement("PaymentStatus")]
        public PaymentStatus PaymentStatus { get; set; }

        [BsonElement("OrderStatus")]
        public OrderStatus OrderStatus { get; set; }

        [BsonElement("Recipient")]
        public AddressModel Recipient { get; set; }


        [BsonElement("PaymentMethod")]
        public PaymentMethod PaymentMethod { get; set; }


        public object this[string propertyName]
        {
            get => this.GetType().GetProperty(propertyName).GetValue(this, null);
            set => this.GetType().GetProperty(propertyName).SetValue(this, value, null);
        }
        #endregion

        #region Methods

        private void UpdateTotalAmount(object sender, EventArgs e)
        {
            TotalAmount = SubTotalAmount + TotalTax + Tips + DiscountAmount + ShippingAmount;
        }

        private void UpdateSubTotal(object sender, EventArgs e)
        {
            SubTotalAmount = OrderItems.Sum(m => m.Price * m.Quantity);
            OnSubTotalChanged();
        }

        private void UpdateTotalTax(object sender, EventArgs e)
        {
            TotalTax = Math.Round(OrderItems.Sum(m => m.Price * m.Quantity * (m.SaleTaxRate / 100)) + (SaleTaxRate * SubTotalAmount), 2, MidpointRounding.AwayFromZero) ;
            OnTotalTaxChanged();
        }
        public ApiResponseModel AddToCart(OrderItem cartItem)
        {
            try
            {
                if(cartItem.Quantity <= 0 || cartItem.Price < 0 || string.IsNullOrEmpty(cartItem.Name) || string.IsNullOrEmpty(cartItem.SkuId) || string.IsNullOrEmpty(cartItem.SkuCode)) 
                    return new ApiResponseModel("error",StaticMessage.ParameterInvalid);

                var oldItem = OrderItems.FirstOrDefault(m => m.Id == cartItem.Id);
                if (oldItem == null)
                {
                    OrderItems.Add(cartItem);
                    OnItemQuantityChanged();
                    return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully, cartItem.Id);
                }

                UpdateQuantity(cartItem.Id, oldItem.Quantity + cartItem.Quantity);
                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully, cartItem.Id);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }

        }

        public ApiResponseModel UpdateSaleTaxRate(decimal tax)
        {
            try
            {
                if (tax < 0) return new ApiResponseModel("error", "Tax must be more than zero");
                SaleTaxRate = Math.Round(tax / 100, 2,MidpointRounding.AwayFromZero);
                OnSaleTaxRateChanged();
                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }
        public ApiResponseModel UpdateTips(decimal amount)
        {
            try
            {
                if (amount < 0) return new ApiResponseModel("error", "Tip must be more than zero");
                Tips = amount;
                OnTipsChanged();
                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }

        public ApiResponseModel UpdateQuantity(Guid itemId, int quantity)
        {
            try
            {
                #region Validate
                if (quantity <= 0)
                    return new ApiResponseModel("error", StaticMessage.ParameterInvalid);
                var oldItem = OrderItems.FirstOrDefault(m => m.Id == itemId);
                if (oldItem == null)
                {
                    return new ApiResponseModel("error", StaticMessage.DataNotFound);
                }
                #endregion

                oldItem.UpdateQuantity(quantity);
                OnItemQuantityChanged();
                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }

        
        public ApiResponseModel RemoveCartItem(Guid itemId)
        {
            try
            {
                #region Validate
                var oldItem = OrderItems.FirstOrDefault(m => m.Id == itemId);
                if (oldItem == null)
                {
                    return new ApiResponseModel("error", StaticMessage.DataNotFound);
                }
                #endregion

                OrderItems.Remove(oldItem);
                OnItemQuantityChanged();
                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }
        public ApiResponseModel ClearCoupon()
        {
            try
            {
                Coupon = null;
                DiscountAmount = 0;
                OnDiscountChanged();
                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }
        public ApiResponseModel UpdateDiscount(OrderCoupon orderCoupon)
        {
            try
            {
                if (orderCoupon == null || string.IsNullOrEmpty(orderCoupon.CouponCode) || orderCoupon.Value <= 0)
                    return new ApiResponseModel("error", "Promotion code is not avalable");

                Coupon = orderCoupon;
                DiscountAmount = Math.Round(Coupon.ValueType == CouponValueType.Percent
                                    ? (SubTotalAmount * Coupon.Value / 100)
                                    : Coupon.Value, 2) * -1;
                OnDiscountChanged();
                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }
        public ApiResponseModel ApplyCoupon(OrderCoupon orderCoupon)
        {
            try
            {
                if (orderCoupon == null || string.IsNullOrEmpty(orderCoupon.CouponCode) || orderCoupon.Value <= 0)
                    return new ApiResponseModel("error", "Promotion code is not avalable");

                Coupon = orderCoupon;
                DiscountAmount = Math.Round(Coupon.ValueType == CouponValueType.Percent
                                     ? (SubTotalAmount * Coupon.Value / 100)
                                     : Coupon.Value, 2) * -1;
                OnDiscountChanged();
                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }
        public ApiResponseModel UpdateShipping(decimal amount)
        {
            try
            {
                if (amount < 0) return new ApiResponseModel("error", "Shipping fee must be more than zero");
                ShippingAmount = amount;
                OnShippingChanged();
                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }
        public ApiResponseModel UpdateModifiers(Guid itemId, IEnumerable<OrderModifierItem> modifierList)
        {
            try
            {
                var modifierGPosItems = modifierList.ToList();
                if (!modifierGPosItems.Any())
                    return new ApiResponseModel("error", StaticMessage.ParameterInvalid);

                var cartItem = OrderItems.FirstOrDefault(m => m.Id == itemId);

                if (cartItem == null)
                    return new ApiResponseModel("error", StaticMessage.ParameterInvalid);

                var result = cartItem.UpdateModifier(modifierGPosItems);
                if (result.status == "error") return result;
                OnItemQuantityChanged();
                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }

        public ApiResponseModel UpdateSpecialRequest(Guid itemId, string note)
        {
            try
            {
                var cartItem = OrderItems.FirstOrDefault(m => m.Id == itemId);
                if (cartItem == null)
                    return new ApiResponseModel("error", StaticMessage.ParameterInvalid);
                var result = cartItem.UpdateSpecialRequest(note);
                return result;
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }
        #endregion

        #region Events

        /// <summary>
        /// Sự kiện được kích hoạt sau khi số lượng sản phẩm trong giỏ hàng thay đổi (Thêm, tăng giảm số lượng, xóa sản phẩm)
        /// </summary>
        public event EventHandler ItemQuantityChanged;

        /// <summary>
        /// Sự kiện kích hoạt khi subtotal thay đổi
        /// </summary>
        public event EventHandler SubTotalChanged;

        public event EventHandler TotalTaxChanged;
        public event EventHandler TipsChanged;
        public event EventHandler DiscountChanged;
        public event EventHandler ShippingChanged;
        public event EventHandler SaleTaxRateChanged;
        public virtual void OnItemQuantityChanged()
        {
            ItemQuantityChanged?.Invoke(this, EventArgs.Empty);
        }
        public virtual void OnSaleTaxRateChanged()
        {
            SaleTaxRateChanged?.Invoke(this, EventArgs.Empty);
        }

        public virtual void OnSubTotalChanged()
        {
            SubTotalChanged?.Invoke(this, EventArgs.Empty);
        }

        public virtual void OnTotalTaxChanged()
        {
            TotalTaxChanged?.Invoke(this, EventArgs.Empty);
        }
        protected virtual void OnTipsChanged()
        {
            TipsChanged?.Invoke(this, EventArgs.Empty);
        }

        protected virtual void OnDiscountChanged()
        {
            DiscountChanged?.Invoke(this, EventArgs.Empty);
        }
        protected virtual void OnShippingChanged()
        {
            ShippingChanged?.Invoke(this, EventArgs.Empty);
        }
        #endregion

    }

    [BsonIgnoreExtraElements]
    public class OrderItem
    {
        public OrderItem()
        {
            ModifierItems = new List<OrderModifierItem>();
        }

        /// <summary>
        /// Id của item giỏ hàng sẽ được tạo mới mỗi khi add to cart
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Id sku sản phẩm
        /// </summary>
        public string SkuId { get; set; }
        /// <summary>
        /// Mã sku sản phẩm
        /// </summary>
        public string SkuCode { get; set; }
        public string Name { get; set; }
        public int Quantity { get; set; }
        public string Avatar { get; set; }
        public decimal Weight { get; set; }
        public decimal Height { get; set; }
        public decimal Width { get; set; }
        public decimal Length { get; set; }
        public string Category { get; set; }

        /// <summary>
        /// Yêu cầu riêng của khách đối với mỗi item
        /// </summary>
        public string SpecialRequest { get; set; }
        /// <summary>
        /// Giá của item
        /// </summary>
        public decimal Price
        {
            get { return SkuPrice + ModifierItems.Sum(m => m.Price); }
        }

        public decimal ListedPrice { get; set; }


        /// <summary>
        /// Giá của sku chưa tính khuyến mãi và modifier
        /// </summary>
        public decimal SkuPrice { get; set; }

        public List<OrderModifierItem> ModifierItems { get; set; }

        /// <summary>
        /// Phần trăm thuế tính trên giá của item
        /// </summary>
        public decimal SaleTaxRate { get; set; }


        public void UpdateQuantity(int quantity)
        {
            Quantity = quantity;
        }

        public ApiResponseModel UpdateModifier(IEnumerable<OrderModifierItem> modifierList)
        {
            var modifierGPosItems = modifierList.ToList();
            if (!modifierGPosItems.Any())
                return new ApiResponseModel("error", StaticMessage.ParameterInvalid);
            ModifierItems = modifierGPosItems;
            return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
        }

        public ApiResponseModel UpdateSpecialRequest(string note)
        {
            SpecialRequest = note;
            return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
        }
    }

    [BsonIgnoreExtraElements]
    public class OrderModifierItem
    {
        public OrderModifierItem()
        {
            Categories = new List<string>();
        }
        public string Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public string Note { get; set; }
        public List<string> Categories { get; set; }
    }

    public class OrderCoupon
    {
        public string Id { get; set; }
        public string CouponCode { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime ExpiredDate { get; set; }
        public decimal Value { get; set; }
        public CouponValueType ValueType { get; set; }
    }

    public class OrderGPos
    {
        #region Properties
        public string OrderWebId { get; set; }
        public string OrderPosId { get; set; }
        public string OrderPosNumber { get; set; }
        public DateTime OrderDateTime { get; set; }
        public string GPosCustomerId { get; set; }
        #endregion
    }

    public class OrderMxMerchant
    {
        public string AuthMessage { get; set; }
        public string CardType { get; set; }
        public string AuthCode { get; set; }
        public string AcctNo { get; set; }
        public string RecordId { get; set; }
    }
    public enum OrderStatusNacGroup
    {
        Waiting,
        Processing,
        Delivery,
        Complete,
        Cancel
    }
    public enum OrderPaymentStatusNacGroup
    {
        NotPaid,
        Paid
    }
    public enum OrderPaymentMethodNacGroup
    {
        COD,
        Transfer,
        Online
    }
    public enum PaymentMethod
    {
        COD,
        CreditCard
    }
    public enum OrderGenderNacGroup
    {
        Male,
        Female
    }

    public enum OrderStatus
    {
        ON_HOLD,  
        PENDING_PAYMENT,
        PAYMENT_RECEIVED,
        PACKAGING,
        ORDER_SHIPPED,
        ORDER_ARCHIVED,
        BACK_ORDER,
        CANCELED,
    }
    public enum PaymentStatus
    {
        CAPTURED,
        AUTHORIZED,
        DECILNED,
        ERROR,
        REVIEW,
        VOIDED,
        UNKNOWN
    }

    [BsonIgnoreExtraElements]
    public class OrderStatusEasyShip
    {
        public string awaiting_shipment { get => awaiting_shipment ; set => awaiting_shipment =  "awaiting_shipment"; }
        public string awaiting_payment { get => awaiting_payment; set => awaiting_payment = "awaiting_payment"; }
        public string awaiting_fulfillment { get => awaiting_fulfillment; set => awaiting_fulfillment = "awaiting_fulfillment"; }
        public string partially_shipped { get => partially_shipped; set => partially_shipped = "partially_shipped"; }
    }

    public class CountryValue
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
    public enum StatisticalTimeType
    {
        Hour,
        Day,
        Month,
        Year
    }
}
