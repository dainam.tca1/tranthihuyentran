﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using NacGroup.Module.Core.Models;
using System.Collections.Generic;

namespace NacGroup.Module.Order.Models.Schema
{
    [BsonIgnoreExtraElements]
    public class CityCollection : Entity
    {
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        [BsonElement("Name")]
        public string Name { get; set; }
        [BsonElement("Code")]
        public string Code { get; set; }
        [BsonElement("Level")]
        public string Level { get; set; }
    }
    public class DistrictCollection : Entity
    {
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        [BsonElement("Name")]
        public string Name { get; set; }
        [BsonElement("Code")]
        public string Code { get; set; }
        [BsonElement("Level")]
        public string Level { get; set; }
        [BsonElement("IsActive")]
        public bool IsActive { get; set; }
        [BsonElement("City")]
        public CityCollection City { get; set; }
    }
    public class WardCollection : Entity
    {
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        [BsonElement("Name")]
        public string Name { get; set; }
        [BsonElement("Code")]
        public string Code { get; set; }
        [BsonElement("Level")]
        public string Level { get; set; }
        [BsonElement("District")]
        public DistrictCollection District { get; set; }
    }
    
    public class ShippingFeeByDistrict : Entity
    {
        public ShippingFeeByDistrict()
        {
            City = new City
            {
                Id = null,
                Name = null,
                Code = null,
                Level = null,
                DistrictList = new List<District>()
            };
        }

        #region Properties
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        [BsonElement("City")]
        public City City { get; set; }
        [BsonElement("Price")]
        public decimal Price { get; set; }
        [BsonElement("IsDefault")]
        public bool IsDefault { get; set; }
        #endregion
    }
    public class District
    {
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Level { get; set; }
        public bool IsActive { get; set; }
        public decimal Price { get; set; }
    }
    public class City
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Level { get; set; }
        public List<District> DistrictList { get; set; }
    }
    public class ShippingFeeByTotalBill : Entity
    {
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        [BsonElement("PriceFrom")]
        public decimal PriceFrom { get; set; }
        [BsonElement("PriceTo")]
        public decimal PriceTo { get; set; }
        [BsonElement("Feeship")]
        public decimal Feeship { get; set; }
        [BsonElement("IsActive")]
        public bool IsActive { get; set; }

    }

    public enum ShippingType
    {
        EasyShip,
        FedEx
    }

}
