﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using NacGroup.Module.Core.Models;

namespace NacGroup.Module.Order.Models.Schema
{
    [BsonIgnoreExtraElements]
    public class Coupon : Entity
    {
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        [BsonElement("CouponCode")]
        public string CouponCode { get; set; }
        [BsonElement("StartDate")]
        public DateTime StartDate { get; set; }
        [BsonElement("ExpiredDate")]
        public DateTime ExpiredDate { get; set; }
        [BsonElement("Value")]
        public decimal Value { get; set; }
        [BsonElement("IsOneTimeUse")]
        public bool IsOneTimeUse { get; set; }
        [BsonElement("ValueType")]
        public CouponValueType ValueType { get; set; }
        [BsonElement("IsActive")]
        public bool IsActive { get; set; }
    }

    public enum CouponValueType
    {
        Percent,
        Money
    }

   
}
