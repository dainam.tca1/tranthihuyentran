﻿using System;
using System.Collections.Generic;
namespace NacGroup.Module.Order.Models
{
    public class EasyShipModel
    {
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public string easyship_shipment_id { get; set; }
        public string destination_name { get; set; }
        public string destination_address_line_1 { get; set; }
        public string destination_address_line_2 { get; set; }
        public string destination_city { get; set; }
        public string destination_state { get; set; }
        public string destination_postal_code { get; set; }
        public string destination_phone_number { get; set; }
        public string destination_email_address { get; set; }
        public string order_notes { get; set; }
        public DateTime order_created_at { get; set; }
        public string platform_name { get; set; }
        public string platform_order_number { get; set; }
        public decimal total_customs_value { get; set; }
        public decimal total_actual_weight { get; set; }
        public decimal total_dimensional_weight { get; set; }
        public decimal total_volumetric_weight { get; set; }
        public bool is_insured { get; set; }
        public string currency { get; set; }
        public string warehouse_state { get; set; }
        public Country origin_country { get; set; }
        public Country destination_country { get; set; }
        public IEnumerable<Item> items { get; set; }
        public Box box { get; set; }
        public Courier selected_courier { get; set; }
        public Doument shipping_documents { get; set; }
    }

    public class Courier
    {
        public string id { get; set; }
        public decimal name { get; set; }
        public decimal min_delivery_time { get; set; }
        public decimal max_delivery_time { get; set; }
        public decimal shipment_charge { get; set; }
        public decimal fuel_surcharge { get; set; }
        public decimal remote_area_surcharge { get; set; }
        public decimal shipment_charge_total { get; set; }
        public decimal warehouse_handling_fee { get; set; }
        public decimal insurance_fee { get; set; }
        public decimal import_tax_charge { get; set; }
        public decimal import_duty_charge { get; set; }
        public decimal ddp_handling_fee { get; set; }
        public decimal total_charge { get; set; }
        public bool is_above_threshold { get; set; }
        public string effective_incoterms { get; set; }
        public decimal estimated_import_tax { get; set; }
        public decimal estimated_import_duty { get; set; }
        public bool courier_does_pickup { get; set; }
        public string courier_dropoff_url { get; set; }
        public string courier_remarks { get; set; }
        public string payment_recipient { get; set; }
        
    }
    public class Doument
    {
        public Label label { get; set; }
        public Label commercial_invoice { get; set; }
        public Label packing_slip { get; set; }
        public Label battery_form { get; set; }
        public Label battery_caution_label { get; set; }
    }
    public class Label
    {
        public bool required { get; set; }
        public string page_size { get; set; }
        public string format { get; set; }
        public List<string> base64_encoded_strings { get; set; }
        public string url { get; set; }
        public string message { get; set; }

    }
    public class Country
    {
        public string name { get; set; }
        public string alpha2 { get; set; }
    }
    public class EasyShipment
    {
        public string platform_name { get; set; }
        public string platform_order_number { get; set; }
        public string selected_courier_id { get; set; }
        public string destination_country_alpha2 { get; set; }
        public string destination_city { get; set; }
        public string destination_postal_code { get; set; }
        public string destination_state  { get; set; }
        public string destination_name { get; set; }
        public string destination_address_line_1 { get; set; }
        public string destination_address_line_2 { get; set; }
        public string destination_phone_number { get; set; }
        public string destination_email_address { get; set; }
        public string order_notes { get; set; }
        public string consignee_tax_id { get; set; }
        public string taxes_duties_paid_by { get; set; }
        public IEnumerable<Item> items { get; set; }
    }

    public class RateShip
    {
        public string origin_postal_code { get; set; }
        public string destination_country_alpha2 { get; set; }
        public string destination_postal_code { get; set; }
        public string taxes_duties_paid_by { get; set; }
        public bool is_insured { get; set; }
        public IEnumerable<Item> items { get; set; }
    }

    public class Box
    {
        public string name { get; set; }
        public decimal length { get; set; }
        public decimal width { get; set; }
        public decimal height { get; set; }

    }

    public class Item
    {
        public string id { get; set; }
        public string description { get; set; }
        public string sku { get; set; }
        public decimal actual_weight { get; set; }
        public decimal dimensional_weight { get; set; }
        public decimal volumetric_weight { get; set; }
        public decimal height { get; set; }
        public decimal width { get; set; }
        public decimal length { get; set; }
        public string category { get; set; }
        public int quantity { get; set; }
        public string origin_currency { get; set; }
        public decimal origin_customs_value { get; set; }
        public string declared_currency { get; set; }
        public decimal declared_customs_value { get; set; }
    }

    public class EasyShipApiConfig
    {
        public string ApiRateUrl { get; set; }
        public string ApiShipmentUrl { get; set; }
        public string AccessToken { get; set; }
    }

   
}
