﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Core.Models.Schema;
using NacGroup.Module.Core.Services;
using NacGroup.Module.MiniCRM.Models.Schema;
using NacGroup.Module.MiniCRM.Services;

namespace NacGroup.Module.MiniCRM.Controllers
{
    [Authorize(Roles = "Admin", Policy = "TenantAuthorize")]
    [Route("crm/api/customerlevel/{action=Index}")]
    public class CustomerLevelCmsApiController : Controller
    {
        private readonly ICustomerLevelService _customerLevelService;

        public CustomerLevelCmsApiController(ICustomerLevelService customerLevelService)
        {
            _customerLevelService = customerLevelService;
        }

        [HttpGet]
        public ActionResult<ApiResponseModel> Index(string keyword, int page = 1, int pagesize = 10) =>
            new ApiResponseModel("success", _customerLevelService.Filter(keyword, page, pagesize));

        [HttpPost]
        public ActionResult<ApiResponseModel> RemoveList([FromBody] List<string> idList)
        {
            return _customerLevelService.Delete(idList);
        }

        [HttpPost]
        public ActionResult<ApiResponseModel> Add([FromBody] CustomerLevel model)
        {
            return _customerLevelService.Add(model);
        }

        public ActionResult<ApiResponseModel> GetUpdate(string id)
        {
            var item = _customerLevelService.GetById(id);
            if (item == null)
                return new ApiResponseModel("error", StaticMessage.DataNotFound);

            return new ApiResponseModel("success", item);
        }
        [HttpPost]
        public ActionResult<ApiResponseModel> Update([FromBody] CustomerLevel model)
        {
            return _customerLevelService.Update(model);
        }
    }
}
