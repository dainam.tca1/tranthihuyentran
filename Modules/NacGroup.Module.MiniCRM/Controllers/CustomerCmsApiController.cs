﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Core.Models.Schema;
using NacGroup.Module.Core.Services;
using NacGroup.Module.MiniCRM.Models.Schema;
using NacGroup.Module.MiniCRM.Services;

namespace NacGroup.Module.MiniCRM.Controllers
{
    [Authorize(Roles = "Admin", Policy = "TenantAuthorize")]
    [Route("cms/api/customer/{action=Index}")]
    public class CustomerCmsApiController : Controller
    {
        private readonly ICustomerService _customerService;

        public CustomerCmsApiController(ICustomerService customerService)
        {
            _customerService = customerService;
        }

        [HttpGet]
        public ActionResult<ApiResponseModel> Index(string keyword, int page = 1, int pagesize = 10) =>
            new ApiResponseModel("success", _customerService.Filter(keyword, page, pagesize));

        [HttpGet]
        public ActionResult<ApiResponseModel> Get(string keyword, Gender? gender, int birthday, int birthMonth, int birthYear, string district, string city, string refName, string refDetail, decimal totalAmountFrom, decimal totalAmountTo, string level, string label, int page, int pagesize) =>
           new ApiResponseModel("success", _customerService.Filter(keyword, gender, birthday, birthMonth, birthYear, district, city, refName, refDetail, totalAmountFrom, totalAmountTo, level, label, page, pagesize));

        [HttpPost]
        public ActionResult<ApiResponseModel> RemoveList([FromBody] List<string> idList)
        {
            return _customerService.Delete(idList);
        }

        [HttpPost]
        public ActionResult<ApiResponseModel> Add([FromBody] Customer model)
        {
            return _customerService.Add(model);
        }

        public ActionResult<ApiResponseModel> GetUpdate(string id)
        {
            var item = _customerService.GetById(id);
            if (item == null)
                return new ApiResponseModel("error", StaticMessage.DataNotFound);

            return new ApiResponseModel("success", item);
        }
        [HttpPost]
        public ActionResult<ApiResponseModel> Update([FromBody] Customer model)
        {
            return _customerService.Update(model);
        }

        [HttpPost]
        public ActionResult<ApiResponseModel> ImportExcel()
        {
            var files = Request.Form.Files;
            if (!files.Any())
                return new ApiResponseModel("error", "Please select files");
            return _customerService.ImportExcel(files);

        }

        [HttpPost]
        public ActionResult<Task<ApiResponseModel>> ExportExcel()
        {

            return _customerService.ExportExcel();

        }
    }
}
