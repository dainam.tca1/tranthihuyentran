﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Core.Models.Schema;
using NacGroup.Module.Core.Services;
using NacGroup.Module.MiniCRM.Models.Schema;
using NacGroup.Module.MiniCRM.Services;

namespace NacGroup.Module.MiniCRM.Controllers
{
    [Authorize(Roles = "Admin", Policy = "TenantAuthorize")]
    [Route("crm/api/orderlabel/{action=Index}")]
    public class OrderLabelCmsApiController : Controller
    {
        private readonly IOrderLabelService _orderLabelService;

        public OrderLabelCmsApiController(IOrderLabelService orderLabelService)
        {
           _orderLabelService = orderLabelService;
        }

        [HttpGet]
        public ActionResult<ApiResponseModel> Index(string name, string color, int page = 1, int pagesize = 10) =>
            new ApiResponseModel("success",_orderLabelService.Filter(name, color,  page, pagesize));

        [HttpPost]
        public ActionResult<ApiResponseModel> RemoveList([FromBody] List<string> idList)
        {
            return _orderLabelService.Delete(idList);
        }

        [HttpPost]
        public ActionResult<ApiResponseModel> Add([FromBody] OrderLabel model)
        {
            return _orderLabelService.Add(model);
        }

        public ActionResult<ApiResponseModel> GetUpdate(string id)
        {
            var item =_orderLabelService.GetById(id);
            if (item == null)
                return new ApiResponseModel("error", StaticMessage.DataNotFound);

            return new ApiResponseModel("success", item);
        }
        [HttpPost]
        public ActionResult<ApiResponseModel> Update([FromBody] OrderLabel model)
        {
            return _orderLabelService.Update(model);
        }
    }
}
