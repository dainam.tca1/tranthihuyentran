﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Core.Models.Schema;
using NacGroup.Module.Core.Services;
using NacGroup.Module.MiniCRM.Models.Schema;
using NacGroup.Module.MiniCRM.Services;

namespace NacGroup.Module.MiniCRM.Controllers
{
    [Authorize(Roles = "Admin", Policy = "TenantAuthorize")]
    [Route("crm/api/customerlabel/{action=Index}")]
    public class CustomerLabelCmsApiController : Controller
    {
        private readonly ICustomerLabelService _customerLabelService;

        public CustomerLabelCmsApiController(ICustomerLabelService customerLabelService)
        {
           _customerLabelService = customerLabelService;
        }

        [HttpGet]
        public ActionResult<ApiResponseModel> Index(string name, string color, int page = 1, int pagesize = 10) =>
            new ApiResponseModel("success",_customerLabelService.Filter(name, color,  page, pagesize));

        [HttpPost]
        public ActionResult<ApiResponseModel> RemoveList([FromBody] List<string> idList)
        {
            return _customerLabelService.Delete(idList);
        }

        [HttpPost]
        public ActionResult<ApiResponseModel> Add([FromBody] CustomerLabel model)
        {
            return _customerLabelService.Add(model);
        }

        public ActionResult<ApiResponseModel> GetUpdate(string id)
        {
            var item =_customerLabelService.GetById(id);
            if (item == null)
                return new ApiResponseModel("error", StaticMessage.DataNotFound);

            return new ApiResponseModel("success", item);
        }
        [HttpPost]
        public ActionResult<ApiResponseModel> Update([FromBody] CustomerLabel model)
        {
            return _customerLabelService.Update(model);
        }
    }
}
