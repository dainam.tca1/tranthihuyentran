﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Core.Models.Schema;
using NacGroup.Module.Core.Services;
using NacGroup.Module.MiniCRM.Models.Schema;
using NacGroup.Module.MiniCRM.Services;

namespace NacGroup.Module.MiniCRM.Controllers
{
    [Authorize(Roles = "Admin", Policy = "TenantAuthorize")]
    [Route("crm/api/customerorder/{action=Index}")]
    public class CustomerOrderCmsApiController : Controller
    {
        private readonly ICustomerOrderService _customerOrderService;

        public CustomerOrderCmsApiController(ICustomerOrderService customerOrderService)
        {
           _customerOrderService = customerOrderService;
        }

        [HttpGet]
        public ActionResult<ApiResponseModel> Index(string orderId, decimal? amountFrom, decimal? amountTo, string label, string customerId, DateTime timeFrom, DateTime timeTo, int page =1 , int pagesize = 10) =>
            new ApiResponseModel("success",_customerOrderService.Filter(orderId, amountFrom, amountTo,label,customerId,timeFrom,timeTo,page,pagesize));

        [HttpPost]
        public ActionResult<ApiResponseModel> RemoveList([FromBody] List<string> idList)
        {
            return _customerOrderService.Delete(idList);
        }

        [HttpPost]
        public ActionResult<ApiResponseModel> Add([FromBody] CustomerOrder model)
        {
            return _customerOrderService.Add(model);
        }

        public ActionResult<ApiResponseModel> GetUpdate(string id)
        {
            var item =_customerOrderService.GetById(id);
            if (item == null)
                return new ApiResponseModel("error", StaticMessage.DataNotFound);

            return new ApiResponseModel("success", item);
        }
        [HttpPost]
        public ActionResult<ApiResponseModel> Update([FromBody] CustomerOrder model)
        {
            return _customerOrderService.Update(model);
        }
    }
}
