﻿using System.Collections.Generic;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using MongoDB.Bson;
using NacGroup.Infrastructure.Shared;
using NacGroup.Module.Core.Data;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Core.Models.DomainEvents;
using NacGroup.Module.Core.Models.Schema;
using NacGroup.Module.MiniCRM.Data;
using NacGroup.Module.MiniCRM.Models.Schema;

namespace NacGroup.Module.MiniCRM.Services
{
    public interface ICustomerLabelService
    {
        CustomerLabel GetById(string id);
        ApiResponseModel Delete(List<string> idlst);
        ApiResponseModel Delete(string id);
        ApiResponseModel Add(CustomerLabel model);
        ApiResponseModel Update(CustomerLabel model);
        PagedResult<CustomerLabel> Filter(string name, string color, int page, int pagesize);

    }

    public class CustomerLabelService : ICustomerLabelService
    {
        private readonly ICustomerLabelRepository _customerLabelRepository;
        private readonly IMediator _mediator;
        private readonly IUnitOfWork _unitOfWork;

        public CustomerLabelService(IUnitOfWork unitOfWork, ICustomerLabelRepository customerLabelRepository, IMediator mediator)
        {
            _unitOfWork = unitOfWork;
            _mediator = mediator;
            _customerLabelRepository = customerLabelRepository;
        }

        public CustomerLabel GetById(string id)
        {
            return _customerLabelRepository.GetById(id);
        }

        public ApiResponseModel Delete(List<string> idlst)
        {
            if (idlst == null)
                return new ApiResponseModel("error", StaticMessage.SelectDataToRemove);
            foreach (var item in idlst)
            {
                var result = Delete(item);
                if (result.status == "error") return result;
            }
            return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
        }

        public ApiResponseModel Delete(string id)
        {
            var resultcode = _customerLabelRepository.Delete(id);
            #region Kiểm tra các table reference tới table này
            _mediator.Publish(new CustomerLabelRemoveDomainEvent(id));
            #endregion
            if (resultcode < 0)
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            resultcode = _unitOfWork.Commit();
            if (resultcode < 0)
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
        }

        public ApiResponseModel Add(CustomerLabel model)
        {
            try
            {
                if (string.IsNullOrEmpty(model.Name)) return new ApiResponseModel("error", "Name is required");
                if (string.IsNullOrEmpty(model.Url)) return new ApiResponseModel("error", "Url is required");
                if (string.IsNullOrEmpty(model.Color)) return new ApiResponseModel("error", "Color is required");
           
                var oldcate = _customerLabelRepository.GetSingle(m => m.Url == model.Url);
                if (oldcate != null)
                    return new ApiResponseModel("error", "Name is exist");
                var result = _customerLabelRepository.Add(model);
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                result = _unitOfWork.Commit();
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully, model);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }

        public ApiResponseModel Update(CustomerLabel model)
        {
            try
            {
                var temp = _customerLabelRepository.GetById(model.Id);
                if (temp == null)
                {
                    return new ApiResponseModel("error", StaticMessage.DataNotFound);
                }
                if (string.IsNullOrEmpty(model.Name)) return new ApiResponseModel("error", "Name is required");
                if (string.IsNullOrEmpty(model.Url)) return new ApiResponseModel("error", "Url is required");
                if (string.IsNullOrEmpty(model.Color)) return new ApiResponseModel("error", "Color is required");
                #region Kiểm tra các table reference tới table này
                bool IsChangedLabel = temp.Name != model.Name || temp.Url != model.Url || temp.Color != model.Color;
                if (IsChangedLabel)
                {
                    _mediator.Publish(new CustomerLabelChangedDomainEvent(temp.Id,model.Name,model.Url,model.Color));
                }
                #endregion
                var result = _customerLabelRepository.Update(temp.Id, model);
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                result = _unitOfWork.Commit();
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully, temp);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }

        public PagedResult<CustomerLabel> Filter(string name,string color, int page, int pagesize)
        {
            return _customerLabelRepository.Filter(name, color, page, pagesize);
        }
    }

}
