﻿using System;
using System.Collections.Generic;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using MongoDB.Bson;
using NacGroup.Infrastructure.Shared;
using NacGroup.Module.Core.Data;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Core.Models.Schema;
using NacGroup.Module.MiniCRM.Data;
using NacGroup.Module.MiniCRM.Models.Schema;

namespace NacGroup.Module.MiniCRM.Services
{
    public interface ICustomerOrderService
    {
        CustomerOrder GetById(string id);
        ApiResponseModel Delete(List<string> idlst);
        ApiResponseModel Delete(string id);
        ApiResponseModel Add(CustomerOrder model);
        ApiResponseModel Update(CustomerOrder model);
        PagedResult<CustomerOrder> Filter(string orderId, decimal? amountFrom, decimal? amountTo, string label, string customerId, DateTime timeFrom, DateTime timeTo, int page, int pagesize);
        int TotalOrderByCustomer(string customerId);
    }

    public class CustomerOrderService : ICustomerOrderService
    {
        private readonly ICustomerOrderRepository _customerOrderRepository;
        private readonly IMediator _mediator;
        private readonly IUnitOfWork _unitOfWork;

        public CustomerOrderService(IUnitOfWork unitOfWork, ICustomerOrderRepository customerOrderRepository, IMediator mediator)
        {
            _unitOfWork = unitOfWork;
            _mediator = mediator;
            _customerOrderRepository = customerOrderRepository;
        }

        public int TotalOrderByCustomer(string customerId)
        {
            return _customerOrderRepository.TotalOrderByCustomer(customerId);
        }

        public CustomerOrder GetById(string id)
        {
            return _customerOrderRepository.GetById(id);
        }

        public ApiResponseModel Delete(List<string> idlst)
        {
            if (idlst == null)
                return new ApiResponseModel("error", StaticMessage.SelectDataToRemove);
            foreach (var item in idlst)
            {
                var result = Delete(item);
                if (result.status == "error") return result;
            }
            return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
        }

        public ApiResponseModel Delete(string id)
        {
            var resultcode = _customerOrderRepository.Delete(id);
            if (resultcode < 0)
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            resultcode = _unitOfWork.Commit();
            if (resultcode < 0)
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
        }

        public ApiResponseModel Add(CustomerOrder model)
        {

            try
            {
                if (string.IsNullOrEmpty(model.OrderId)) return new ApiResponseModel("error", "OrderId is required");
                if (string.IsNullOrEmpty(model.CustomerId)) return new ApiResponseModel("error", "CustomerId is required");
                if (model.Amount < 0 ) return new ApiResponseModel("error", "Amount is greater than 0");
                if (model.Label == null) return new ApiResponseModel("error", "Url is required");
                if (model.OrderDateTime == DateTime.MinValue) return new ApiResponseModel("error", "Order Date Time is required");
                var result = _customerOrderRepository.Add(model);
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                result = _unitOfWork.Commit();
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }

        public ApiResponseModel Update(CustomerOrder model)
        {
            try
            {
                var temp = _customerOrderRepository.GetById(model.Id);
                if (temp == null)
                {
                    return new ApiResponseModel("error", StaticMessage.DataNotFound);
                }

                temp.Description = model.Description;
                temp.Label = model.Label;
                temp.DetailUrl = model.DetailUrl;
                var result = _customerOrderRepository.Update(temp.Id, temp);
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                result = _unitOfWork.Commit();
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully, temp);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }

        public PagedResult<CustomerOrder> Filter(string orderId, decimal? amountFrom, decimal? amountTo, string label, string customerId, DateTime timeFrom, DateTime timeTo, int page, int pagesize)
        {
            return _customerOrderRepository.Filter(orderId,amountFrom, amountTo,label,customerId, timeFrom,timeTo,page,pagesize);
        }
    }

}
