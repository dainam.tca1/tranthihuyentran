﻿using System.Collections.Generic;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using MongoDB.Bson;
using NacGroup.Infrastructure.Shared;
using NacGroup.Module.Core.Data;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Core.Models.Schema;
using NacGroup.Module.MiniCRM.Data;
using NacGroup.Module.MiniCRM.Models.Schema;

namespace NacGroup.Module.MiniCRM.Services
{
    public interface IOrderLabelService
    {
        OrderLabel GetById(string id);
        ApiResponseModel Delete(List<string> idlst);
        ApiResponseModel Delete(string id);
        ApiResponseModel Add(OrderLabel model);
        ApiResponseModel Update(OrderLabel model);
        PagedResult<OrderLabel> Filter(string name, string color, int page, int pagesize);

    }

    public class OrderLabelService : IOrderLabelService
    {
        private readonly IOrderLabelRepository _orderLabelRepository;
        private readonly IMediator _mediator;
        private readonly IUnitOfWork _unitOfWork;

        public OrderLabelService(IUnitOfWork unitOfWork, IOrderLabelRepository orderLabelRepository, IMediator mediator)
        {
            _unitOfWork = unitOfWork;
            _mediator = mediator;
            _orderLabelRepository = orderLabelRepository;
        }

        public OrderLabel GetById(string id)
        {
            return _orderLabelRepository.GetById(id);
        }

        public ApiResponseModel Delete(List<string> idlst)
        {
            if (idlst == null)
                return new ApiResponseModel("error", StaticMessage.SelectDataToRemove);
            foreach (var item in idlst)
            {
                var result = Delete(item);
                if (result.status == "error") return result;
            }
            return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
        }

        public ApiResponseModel Delete(string id)
        {
            var resultcode = _orderLabelRepository.Delete(id);
            if (resultcode < 0)
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            resultcode = _unitOfWork.Commit();
            if (resultcode < 0)
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
        }

        public ApiResponseModel Add(OrderLabel model)
        {

            try
            {
                if (string.IsNullOrEmpty(model.Name)) return new ApiResponseModel("error", "Name is required");
                if (string.IsNullOrEmpty(model.Url)) return new ApiResponseModel("error", "Url is required");
                if (string.IsNullOrEmpty(model.Color)) return new ApiResponseModel("error", "Color is required");
           
                var oldcate = _orderLabelRepository.GetSingle(m => m.Url == model.Url);
                if (oldcate != null)
                    return new ApiResponseModel("error", "Name is exist");
                var result = _orderLabelRepository.Add(model);
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                result = _unitOfWork.Commit();
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully, model);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }

        public ApiResponseModel Update(OrderLabel model)
        {
            try
            {
                var temp = _orderLabelRepository.GetById(model.Id);
                if (temp == null)
                {
                    return new ApiResponseModel("error", StaticMessage.DataNotFound);
                }
                if (string.IsNullOrEmpty(model.Name)) return new ApiResponseModel("error", "Name is required");
                if (string.IsNullOrEmpty(model.Url)) return new ApiResponseModel("error", "Url is required");
                if (string.IsNullOrEmpty(model.Color)) return new ApiResponseModel("error", "Color is required");

                

                var result = _orderLabelRepository.Update(temp.Id, model);
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                result = _unitOfWork.Commit();
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully, temp);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }

        public PagedResult<OrderLabel> Filter(string name,string color, int page, int pagesize)
        {
            return _orderLabelRepository.Filter(name, color, page, pagesize);
        }
    }

}
