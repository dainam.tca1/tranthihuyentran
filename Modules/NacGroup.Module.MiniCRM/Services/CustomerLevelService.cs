﻿using System.Collections.Generic;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using MongoDB.Bson;
using NacGroup.Infrastructure.Shared;
using NacGroup.Module.Core.Data;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Core.Models.DomainEvents;
using NacGroup.Module.Core.Models.Schema;
using NacGroup.Module.MiniCRM.Data;
using NacGroup.Module.MiniCRM.Models.Schema;

namespace NacGroup.Module.MiniCRM.Services
{
    public interface ICustomerLevelService
    {
        CustomerLevel GetById(string id);
        ApiResponseModel Delete(List<string> idlst);
        ApiResponseModel Delete(string id);
        ApiResponseModel Add(CustomerLevel model);
        ApiResponseModel Update(CustomerLevel model);
        PagedResult<CustomerLevel> Filter(string name, int page, int pagesize);

    }

    public class CustomerLevelService : ICustomerLevelService
    {
        private readonly ICustomerLevelRepository _customerLevelRepository;
        private readonly IMediator _mediator;
        private readonly IUnitOfWork _unitOfWork;

        public CustomerLevelService(IUnitOfWork unitOfWork, ICustomerLevelRepository customerLevelRepository, IMediator mediator)
        {
            _unitOfWork = unitOfWork;
            _mediator = mediator;
            _customerLevelRepository = customerLevelRepository;
        }

        public CustomerLevel GetById(string id)
        {
            return _customerLevelRepository.GetById(id);
        }

        public ApiResponseModel Delete(List<string> idlst)
        {
            if (idlst == null)
                return new ApiResponseModel("error", StaticMessage.SelectDataToRemove);
            foreach (var item in idlst)
            {
                var result = Delete(item);
                if (result.status == "error") return result;
            }
            return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
        }

        public ApiResponseModel Delete(string id)
        {
            var resultcode = _customerLevelRepository.Delete(id);
            if (resultcode < 0)
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            resultcode = _unitOfWork.Commit();
            if (resultcode < 0)
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
        }

        public ApiResponseModel Add(CustomerLevel model)
        {

            try
            {
                if (string.IsNullOrEmpty(model.Name)) return new ApiResponseModel("error", "Name is required");
                if (string.IsNullOrEmpty(model.Url)) return new ApiResponseModel("error", "Url is required");
               
                if (model.AmountRequired < 0) return new ApiResponseModel("error", "Amount is required");
                var oldcate = _customerLevelRepository.GetSingle(m => m.Url == model.Url);
                if (oldcate != null)
                    return new ApiResponseModel("error", "Name is exist");
                var result = _customerLevelRepository.Add(model);
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                result = _unitOfWork.Commit();
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully, model);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }

        public ApiResponseModel Update(CustomerLevel model)
        {
            try
            {
                var temp = _customerLevelRepository.GetById(model.Id);
                if (temp == null)
                {
                    return new ApiResponseModel("error", StaticMessage.DataNotFound);
                }
                if (string.IsNullOrEmpty(model.Name)) return new ApiResponseModel("error", "Name is required");
                if (string.IsNullOrEmpty(model.Url)) return new ApiResponseModel("error", "Url is required");

                if (model.AmountRequired < 0) return new ApiResponseModel("error", "Amount is required");

                #region Kiểm tra các table reference tới table này
                bool IsChangedLabel = temp.Name != model.Name || temp.Url != model.Url || temp.AmountRequired != model.AmountRequired;
                if (IsChangedLabel)
                {
                    _mediator.Publish(new CustomerLevelChangedDomainEvent(temp.Id, model.Name,model.AmountRequired, model.Url));
                }
                #endregion

                var result = _customerLevelRepository.Update(temp.Id, model);
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                result = _unitOfWork.Commit();
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully, temp);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }

        public PagedResult<CustomerLevel> Filter(string name, int page, int pagesize)
        {
            return _customerLevelRepository.Filter(name, page, pagesize);
        }
    }

}
