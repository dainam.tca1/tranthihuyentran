﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using MongoDB.Bson;
using NacGroup.Infrastructure.Shared;
using NacGroup.Module.Core.Data;
using NacGroup.Module.Core.Interfaces;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Core.Models.DomainEvents;
using NacGroup.Module.Core.Models.Schema;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using NacGroup.Module.MiniCRM.Data;
using NacGroup.Module.MiniCRM.Models.Schema;

namespace NacGroup.Module.MiniCRM.Services
{
    public interface ICustomerService
    {
        Customer GetById(string id);
        ApiResponseModel Delete(List<string> idlst);
        ApiResponseModel Delete(string id);
        ApiResponseModel Add(Customer model);
        ApiResponseModel Update(Customer model);
        PagedResult<Customer> Filter(string keyword, int page, int pagesize);
        PagedResult<Customer> Filter(string keyword, Gender? gender, int birthday, int birthMonth, int birthYear, string district, string city, string refName, string refDetail, decimal totalAmountFrom, decimal totalAmountTo, string level,string label, int page, int pagesize);
        Customer GetByPhone(string modelCustomerPhone);
        List<Customer> Get();
        ApiResponseModel ImportExcel(IFormFileCollection file);
        Task<ApiResponseModel> ExportExcel();
    }

    public class CustomerService : ICustomerService, INotificationHandler<CustomerLevelChangedDomainEvent> ,INotificationHandler<CustomerLabelChangedDomainEvent>
    {
        private readonly ICustomerRepository _customerRepository;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILoggerRepository _loggerRepository;
        private readonly Tenant _tenant;
        private readonly ICustomerLevelRepository _customerLevelRepository;
        private readonly ICustomerLabelRepository _customerLabelRepository;

        public CustomerService(ICustomerLevelRepository customerLevelRepository, ICustomerLabelRepository customerLabelRepository, ILoggerRepository loggerRepository, IUnitOfWork unitOfWork, ICustomerRepository customerRepository, IHostingEnvironment hostingEnvironment, ITenantProvider tenantProvider)
        {
            _unitOfWork = unitOfWork;
            _hostingEnvironment = hostingEnvironment;
            _customerRepository = customerRepository;
            _loggerRepository = loggerRepository;
            _tenant = tenantProvider.GetTenant();
            _customerLevelRepository = customerLevelRepository;
            _customerLabelRepository = customerLabelRepository;
        }

        public List<Customer> Get()
        {
            return _customerRepository.Get().ToList();
        }

        public PagedResult<Customer> Filter(string keyword, Gender? gender, int birthday, int birthMonth, int birthYear, string district, string city, string refName, string refDetail, decimal totalAmountFrom, decimal totalAmountTo, string level, string label, int page, int pagesize)
        {
            return _customerRepository.Filter(keyword,gender,birthday,birthMonth,birthYear,district,city,refName,refDetail,totalAmountFrom,totalAmountTo,level,label,page,pagesize);
        }
        public Customer GetById(string id)
        {
            return _customerRepository.GetById(id);
        }

        public ApiResponseModel Delete(List<string> idlst)
        {
            if (idlst == null)
                return new ApiResponseModel("error", StaticMessage.SelectDataToRemove);
            foreach (var item in idlst)
            {
                var result = Delete(item);
                if (result.status == "error") return result;
            }
            return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
        }

        public ApiResponseModel Delete(string id)
        {
            var resultcode = _customerRepository.Delete(id);
            if (resultcode < 0)
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            resultcode = _unitOfWork.Commit();
            if (resultcode < 0)
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
        }

        public ApiResponseModel Add(Customer model)
        {

            try
            {
                if (string.IsNullOrEmpty(model.FirstName)) return new ApiResponseModel("error", "First name is required");
                if (string.IsNullOrEmpty(model.LastName)) return new ApiResponseModel("error", "Last Name is required");
                if (string.IsNullOrEmpty(model.Phone)) return new ApiResponseModel("error", "Phone number is required");
                if (!string.IsNullOrEmpty(model.Email) && !model.Email.IsEmail()) return new ApiResponseModel("error", "Email is invalid");
                var oldcate = _customerRepository.GetSingle(m => m.Phone == model.Phone);
                if (oldcate != null)
                    return new ApiResponseModel("error", "Phone number is exist");
                if (!string.IsNullOrEmpty(model.Email))
                {
                    oldcate = _customerRepository.GetSingle(m => m.Email == model.Email);
                    if (oldcate != null)
                        return new ApiResponseModel("error", "Email is exist");
                }

                model.Id = ObjectId.GenerateNewId().ToString();
                var cusLevel = _customerLevelRepository.GetSingle(m => m.AmountRequired <= model.TotalAmount);
                if (cusLevel != null)
                {
                    model.Level = cusLevel;
                }
                var result = _customerRepository.Add(model);
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                result = _unitOfWork.Commit();
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully, model);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }

        public ApiResponseModel Update(Customer model)
        {
            try
            {
                var temp = _customerRepository.GetById(model.Id);
                if (temp == null) return new ApiResponseModel("error", StaticMessage.DataNotFound);
                if (string.IsNullOrEmpty(model.FirstName)) return new ApiResponseModel("error", "First name is required");
                if (string.IsNullOrEmpty(model.LastName)) return new ApiResponseModel("error", "Last Name is required");
                if (string.IsNullOrEmpty(model.Phone)) return new ApiResponseModel("error", "Phone number is required");
                if (!string.IsNullOrEmpty(model.Email) && !model.Email.IsEmail()) return new ApiResponseModel("error", "Email is invalid");
                var oldcate = _customerRepository.GetSingle(m => m.Phone == model.Phone);
                if (oldcate != null && oldcate.Id != temp.Id)
                    return new ApiResponseModel("error", "Phone number is exist");
                if (!string.IsNullOrEmpty(model.Email))
                {
                    oldcate = _customerRepository.GetSingle(m => m.Email == model.Email);
                    if (oldcate != null && oldcate.Id != temp.Id)
                        return new ApiResponseModel("error", "Email is exist");
                }

                var cusLevel = _customerLevelRepository.GetSingle(m => m.AmountRequired <= model.TotalAmount);
                if (cusLevel != null)
                {
                    temp.Level = cusLevel;
                }
                temp.FirstName = model.FirstName;
                temp.LastName = model.LastName;
                temp.Phone = model.Phone;
                temp.Email = model.Email?.ToLower();
                temp.BirthDay = model.BirthDay;
                temp.BirthMonth = model.BirthMonth;
                temp.BirthYear = model.BirthYear;
                var result = _customerRepository.Update(temp.Id, temp);
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                result = _unitOfWork.Commit();
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully, temp);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }

        public PagedResult<Customer> Filter(string keyword, int page, int pagesize)
        {
            return _customerRepository.Filter(keyword, page, pagesize);
        }

        public Customer GetByPhone(string modelCustomerPhone)
        {
            return _customerRepository.GetSingle(m => m.Phone == modelCustomerPhone);
        }
        public ApiResponseModel ImportExcel(IFormFileCollection files)
        {
            try
            {
                var excelFolder = Path.Combine(_hostingEnvironment.WebRootPath, $"tenants/{_tenant.TenantName.ToLower()}/upload/excel");
                if (!Directory.Exists(excelFolder))
                {
                    Directory.CreateDirectory(excelFolder);
                }
                var file = files.FirstOrDefault();
                var filenamewithoutext = Path.GetFileNameWithoutExtension(file.FileName).ToUrl();
                var extentionfile = Path.GetExtension(file.FileName).ToLower();
                var filename = $"{filenamewithoutext}{extentionfile}";
                var folderimg = Path.Combine(excelFolder, DateTime.Today.Year + "-" + DateTime.Today.Month);
                if (!Directory.Exists(folderimg))
                {
                    Directory.CreateDirectory(folderimg);
                }
                ISheet sheet;

                var path = Path.Combine(folderimg, filename);//duong dan luu file
                var customerList = new List<CustomerExcel>();
                using (FileStream fs = File.Create(path))
                {
                    file.CopyTo(fs);
                    fs.Flush();
                    fs.Position = 0;
                    if (extentionfile == ".xls")
                    {
                        HSSFWorkbook hssfwb = new HSSFWorkbook(fs); //This will read the Excel 97-2000 formats  
                        sheet = hssfwb.GetSheetAt(0); //get first sheet from workbook  
                    }
                    else
                    {
                        XSSFWorkbook hssfwb = new XSSFWorkbook(fs); //This will read 2007 Excel format  
                        sheet = hssfwb.GetSheetAt(0); //get first sheet from workbook   
                    }
                    IRow headerRow = sheet.GetRow(0); //Get Header Row
                    int cellCount = headerRow.LastCellNum;
                    for (int i = (sheet.FirstRowNum + 1); i <= sheet.LastRowNum; i++) //Read Excel File
                    {
                        IRow row = sheet.GetRow(i);
                        if (row == null) continue;
                        if (row.Cells.All(d => d.CellType == CellType.Blank)) continue;
                        var data_row = new CustomerExcel { };
                        for (int j = row.FirstCellNum; j < cellCount; j++)
                        {
                            if (row.GetCell(j) != null)
                            {
                                switch (j)
                                {
                                    case 0:
                                        data_row.FirstName = row.GetCell(j).ToString().Trim();
                                        break;
                                    case 1:
                                        data_row.LastName = row.GetCell(j).ToString().Trim();
                                        break;
                                    case 2:
                                        data_row.Email = row.GetCell(j).ToString().Trim();
                                        break;
                                    case 3:
                                        data_row.Phone = row.GetCell(j).ToString().Trim();
                                        break;
                                    case 4:
                                        data_row.BirthMonth = int.Parse(row.GetCell(j).ToString().Trim());
                                        break;
                                    case 5:
                                        data_row.BirthDay= int.Parse(row.GetCell(j).ToString().Trim());
                                        break;
                                    case 6:
                                        data_row.BirthYear = int.Parse(row.GetCell(j).ToString().Trim());
                                        break;
                                    case 7:
                                        data_row.Level = row.GetCell(j).ToString().Trim();
                                        break;
                                    case 8:
                                        data_row.Label = row.GetCell(j).ToString().Trim();
                                        break;
                                    case 9:
                                        data_row.TotalAmount = decimal.Parse(row.GetCell(j).ToString().Trim());
                                        break;
                                }
                            }

                        }
                        customerList.Add(data_row);

                    }
                 

                    if (customerList.Count() > 0)
                    {                      
                        var s = 0;
                        var error_List = new List<string>();
                        
                        foreach (var cus in customerList)
                        {
                            var customer = new Customer { };
                            #region Validate label
                            if (!string.IsNullOrEmpty(cus.Label))
                            {
                                var cusLabelList = cus.Label.Split("|");
                                foreach (var label in cusLabelList) 
                                {
                                    var labelname = label.ToUrl();
                                    var labelTemp = _customerLabelRepository.GetSingle(m=> m.Url == labelname);
                                    if (labelTemp == null)
                                    {
                                        error_List.Add($"{label} Label not found");
                                    }
                                    customer.Labels.Add(labelTemp);
                                }

                            }
                            #endregion

                            #region Validate label
                            if (!string.IsNullOrEmpty(cus.Level))
                            {
                                var levelName = cus.Level.ToUrl();
                                var levelTemp = _customerLevelRepository.GetSingle(m => m.Url == levelName);
                                if (levelTemp == null)
                                {
                                    error_List.Add($"{cus.Level} Label not found");
                                }
                                customer.Level = levelTemp;

                            }
                            #endregion
                            #region Add Product
                            if (error_List.Count() == 0)
                            {
                                customer.FirstName = cus.FirstName;
                                customer.LastName = cus.LastName;
                                customer.Email = cus.Email;
                                customer.Phone = cus.Phone;
                                customer.BirthDay = cus.BirthDay;
                                customer.BirthMonth = cus.BirthMonth;
                                customer.BirthYear = cus.BirthYear;
                                customer.TotalAmount = cus.TotalAmount;
                                if (!customer.Labels.Any()) customer.Labels = new List<CustomerLabel>();
                                if (customer.Level == null) customer.Level = new CustomerLevel { };

                                var cusResult = _customerRepository.Add(customer);
                                if (cusResult < 0) return new ApiResponseModel("error", "Cannot Added Customer");
                                cusResult = _unitOfWork.Commit();
                                if (cusResult < 0) return new ApiResponseModel("error", "Cannot Added Customer");
                                continue;
                            }
                            else
                            {
                                string delimiter = ",";
                                _loggerRepository.Add(new Logger(LogLevel.Error, $"Import Excel Customer {cus.FirstName} {cus.LastName}", null, error_List.Aggregate((c, d) => c + delimiter + d)));
                                _unitOfWork.Commit();
                                continue;
                            }


                            #endregion
                        }
                    }

                }

                return new ApiResponseModel("success", StaticMessage.DataLoadingSuccessfull);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }

        public async Task<ApiResponseModel> ExportExcel()
        {
            try
            {

                string webRootPath = Path.Combine(_hostingEnvironment.WebRootPath, $"tenants/{_tenant.TenantName.ToLower()}/upload/excel");
                string fileName = @"customerList.xlsx";
                FileInfo file = new FileInfo(Path.Combine(webRootPath, fileName));
                var memoryStream = new MemoryStream();
                using (var fs = new FileStream(Path.Combine(webRootPath, fileName), FileMode.Create, FileAccess.Write))
                {
                    IWorkbook workbook = new XSSFWorkbook();
                    ISheet excelSheet = workbook.CreateSheet("Customer List");

                    IRow row = excelSheet.CreateRow(0);
                    row.CreateCell(0).SetCellValue("FirstName");
                    row.CreateCell(1).SetCellValue("LastName");
                    row.CreateCell(2).SetCellValue("Email");
                    row.CreateCell(3).SetCellValue("Phone");
                    row.CreateCell(4).SetCellValue("BirthDay");
                    row.CreateCell(5).SetCellValue("BirthMonth");
                    row.CreateCell(6).SetCellValue("BirthYear");
                    

                    var customerList = _customerRepository.Get();
                    int counter = 1;
                    foreach (var customer in customerList)
                    {
                        row = excelSheet.CreateRow(counter);
                        row.CreateCell(0).SetCellValue(customer.FirstName);
                        row.CreateCell(1).SetCellValue(customer.LastName);
                        row.CreateCell(2).SetCellValue(customer.Email);
                        row.CreateCell(3).SetCellValue(customer.Phone);
                        row.CreateCell(4).SetCellValue(customer.BirthDay);
                        row.CreateCell(5).SetCellValue(customer.BirthMonth);
                        row.CreateCell(6).SetCellValue(customer.BirthYear);        
                        counter++;
                    }
                    workbook.Write(fs);
                }
                using (var fileStream = new FileStream(Path.Combine(webRootPath, fileName), FileMode.Open))
                {
                    await fileStream.CopyToAsync(memoryStream);
                }
                memoryStream.Position = 0;
                return new ApiResponseModel("success", StaticMessage.DataLoadingSuccessfull, $"{memoryStream}/{fileName}");
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }


        public class CustomerExcel
        {
            public string FirstName { get; set; }
            public string LastName { get; set; }
            private string _email;
            public string Email
            {
                get => _email;
                set => _email = value?.ToLower();
            }
            public string Phone { get; set; }
            public int BirthDay { get; set; }
            public int BirthMonth { get; set; }
            public int BirthYear { get; set; }
            public string Level { get; set; }
            public string Label { get; set; }
            public decimal TotalAmount { get; set; }
        }

        public Task Handle(CustomerLevelChangedDomainEvent notification, CancellationToken cancellationToken)
        {
            try
            {
                var customerList = _customerRepository.Get().Where(m => m.Level.Id == notification.Id);
                if (customerList.Count() > 0)
                {
                    foreach (var customer in customerList)
                    {
                        var tempLevel = new CustomerLevel
                        {
                            Id = notification.Id,
                            Name = notification.Name,
                            Url = notification.Url,
                            AmountRequired = notification.AmountRequired
                        };
                        customer.Level = tempLevel;
                        var updateCustomer = _customerRepository.Update(customer.Id,customer);
                        updateCustomer = _unitOfWork.Commit();
                    }
                }

                return Task.CompletedTask;
            }
            catch
            {
                return Task.CompletedTask;
            }
        }

        public Task Handle(CustomerLabelChangedDomainEvent notification, CancellationToken cancellationToken)
        {
            try
            {
                var customerList = _customerRepository.Get().Where(m => m.Labels.Where( l => l.Id == notification.Id).Any());

                if (customerList.Count() > 0)
                {
                    foreach (var customer in customerList)
                    {

                        var tempLabel = new CustomerLabel
                        {
                            Id = notification.Id,
                            Name = notification.Name,
                            Url = notification.Url,
                            Color = notification.Color,
                        };

                        var customerLabel = customer.Labels.FirstOrDefault(m => m.Id == notification.Id);
                        customerLabel = tempLabel;     
                        var updateCustomer = _customerRepository.Update(customer.Id, customer);
                        updateCustomer = _unitOfWork.Commit();
                    }
                }


                return Task.CompletedTask;
            }
            catch
            {
                return Task.CompletedTask;
            }
        }

        public Task Handle(CustomerLabelRemoveDomainEvent notification, CancellationToken cancellationToken)
        {
            try
            {
                var customerList = _customerRepository.Get().Where(m => m.Labels.Where(l => l.Id == notification.Id).Any());

                if (customerList.Count() > 0)
                {
                    foreach (var customer in customerList)
                    {

                        customer.Labels.Where(m => m.Id != notification.Id);
                        var updateCustomer = _customerRepository.Update(customer.Id, customer);
                        updateCustomer = _unitOfWork.Commit();
                    }
                }


                return Task.CompletedTask;
            }
            catch
            {
                return Task.CompletedTask;
            }
        }


    }

}
