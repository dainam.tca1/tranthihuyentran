﻿using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using NacGroup.Infrastructure.Shared;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Core.Models.Schema;

namespace NacGroup.Module.MiniCRM.Models.Schema
{
    [BsonIgnoreExtraElements]
    public class Customer : Entity
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        private string _firstName;
        [BsonElement("FirstName")]
        public string FirstName { get => _firstName; set => _firstName = value?.ToUpper(); }

        private string _lastName;
        [BsonElement("LastName")]
        public string LastName { get => _lastName; set => _lastName = value?.ToUpper(); }

        [BsonElement("Gender")]
        public Gender Gender { get; set; }

        private string _email;
        [BsonElement("Email")]
        public string Email
        {
            get => _email;
            set => _email = value?.ToLower();
        }
        [BsonElement("Phone")]
        public string Phone { get; set; }
        [BsonElement("BirthDay")]
        public int BirthDay { get; set; }
        [BsonElement("BirthMonth")]
        public int BirthMonth { get; set; }
        [BsonElement("BirthYear")]
        public int BirthYear { get; set; }
        [BsonElement("AddressList")]
        public IEnumerable<CustomerAddress> AddressList { get; set; }
        [BsonElement("Level")]
        public CustomerLevel Level { get; set; }
        [BsonElement("Labels")]
        public List<CustomerLabel> Labels { get; set; }
        [BsonElement("ReferenceName")]
        public string ReferenceName { get; set; }
        [BsonElement("ReferenceDetail")]
        public string ReferenceDetail { get; set; }
        [BsonElement("TotalAmount")]
        public decimal TotalAmount { get; set; }
        [BsonElement("LastBuyTime")]
        public DateTime LastBuyTime { get; set; }
    }

    public class CustomerAddress
    {
        public string Street { get; set; }
        public string Ward { get; set; }
        public string District { get; set; }
        public string City { get; set; }
    }
    public enum Gender
    {
        Male,
        Female
    }

}
