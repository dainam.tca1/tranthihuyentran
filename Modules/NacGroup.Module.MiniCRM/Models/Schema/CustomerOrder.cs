﻿using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using NacGroup.Infrastructure.Shared;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Core.Models.Schema;

namespace NacGroup.Module.MiniCRM.Models.Schema
{
    [BsonIgnoreExtraElements]
    public class CustomerOrder: Entity
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("OrderId")]
        public string OrderId { get; set; }

        [BsonElement("Amount")]
        public decimal Amount { get; set; }

        [BsonElement("Description")]
        public string Description { get; set; }

        [BsonElement("Label")]
        public OrderLabel Label { get; set; }

        [BsonElement("OrderDateTime")]
        public DateTime OrderDateTime { get; set; }

        [BsonElement("CustomerId")]
        public string CustomerId { get; set; }

        [BsonElement("DetailUrl")]
        public string DetailUrl { get; set; }

    
    }

}
