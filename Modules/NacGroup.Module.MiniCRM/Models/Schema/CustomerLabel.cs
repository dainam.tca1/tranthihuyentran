﻿using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using NacGroup.Infrastructure.Shared;
using NacGroup.Module.Core.Models;

namespace NacGroup.Module.MiniCRM.Models.Schema
{
    [BsonIgnoreExtraElements]
    public class CustomerLabel : Entity
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("Name")]
        public string Name { get; set; }

        [BsonElement("Url")]
        public string Url { get; set; }

        [BsonElement("Color")]
        public string Color { get; set; }
    }

}
