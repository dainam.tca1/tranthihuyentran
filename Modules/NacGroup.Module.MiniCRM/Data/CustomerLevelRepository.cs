﻿using MongoDB.Driver;
using NacGroup.Infrastructure.Shared;
using NacGroup.Module.Core.Data;
using NacGroup.Module.Core.Interfaces;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Core.Models.Schema;
using NacGroup.Module.MiniCRM.Models.Schema;

namespace NacGroup.Module.MiniCRM.Data
{
    public interface ICustomerLevelRepository : IRepository<CustomerLevel>
    {
        PagedResult<CustomerLevel> Filter(string name, int page, int pagesize);
    }

    public class CustomerLevelRepository : MongoRepository<CustomerLevel>, ICustomerLevelRepository, IMigrationRepository
    {
        public CustomerLevelRepository(IMongoContext context) : base(context)
        {
        }


        public void Migration()
        {
            DbSet.Indexes.CreateOne(model: new CreateIndexModel<CustomerLevel>("{Url:1}", new CreateIndexOptions { Unique = true }));
        }

        public void Seed()
        {
           
        }

        public PagedResult<CustomerLevel> Filter(string name, int page, int pagesize)
        {
            var filter = Builders<CustomerLevel>.Filter;
            var filterdefine = filter.Empty;
            if (!string.IsNullOrEmpty(name))
            {
                name = name.ToUrl();
                filterdefine &= filter.Where(m => m.Url.Contains(name));
            }
          
            var totalItem = DbSet.CountDocuments(filterdefine);
            var all = DbSet.Find(filterdefine).Skip((page - 1) * pagesize).Limit(pagesize);
            return new PagedResult<CustomerLevel>(all.ToList(), page, pagesize, (int)totalItem);
        }
    }
}
