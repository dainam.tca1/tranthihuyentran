﻿using System.Collections.Generic;
using System.Linq;
using MongoDB.Driver;
using NacGroup.Infrastructure.Shared;
using NacGroup.Module.Core.Interfaces;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Core.Models.Schema;
using System.Linq;
using NacGroup.Module.Core.Data;
using NacGroup.Module.MiniCRM.Models.Schema;

namespace NacGroup.Module.MiniCRM.Data
{
    public interface ICustomerRepository : IRepository<Customer>
    {
        PagedResult<Customer> Filter(string keyword, int page, int pagesize);
        List<Customer> GetByIdList(List<string> idlist);
        PagedResult<Customer> Filter(
            string keyowrd,
            Gender? gender,
            int birthday, 
            int birthMonth,
            int birthYear,
            string district,
            string city,
            string refName,
            string refDetail,
            decimal totalAmountFrom, 
            decimal totalAmountTo, 
            string level,
            string label,
            int page, int pagesize);
    }

    public class CustomerRepository : MongoRepository<Customer>, ICustomerRepository, IMigrationRepository
    {
        public CustomerRepository(IMongoContext context) : base(context)
        {
        }


        public void Migration()
        {
            DbSet.Indexes.CreateOne(model: new CreateIndexModel<Customer>("{Email:1}"));
            DbSet.Indexes.CreateOne(model: new CreateIndexModel<Customer>("{Phone:1}", new CreateIndexOptions { Unique = true }));
        }

        public void Seed()
        {
           
        }

        public PagedResult<Customer> Filter(string keyword, int page, int pagesize)
        {
            var filter = Builders<Customer>.Filter;
            var filterdefine = filter.Empty;
            if (!string.IsNullOrEmpty(keyword))
            {
                keyword = keyword.ToUpper();
                var emailkeyword = keyword.ToLower();
                filterdefine &= filter.Where(m => m.FirstName.Contains(keyword) || m.LastName.Contains(keyword) || m.Phone.Contains(keyword) || m.Email.Contains(emailkeyword));
            }
          
            var totalItem = DbSet.CountDocuments(filterdefine);
            var all = DbSet.Find(filterdefine).Skip((page - 1) * pagesize).Limit(pagesize);
            return new PagedResult<Customer>(all.ToList(), page, pagesize, (int)totalItem);
        }

        public List<Customer> GetByIdList(List<string> idlsList)
        {
            return Get(m => idlsList.Contains(m.Id)).ToList();
        }
        public PagedResult<Customer> Filter(
            string keyword,
            Gender? gender,
            int birthday,
            int birthMonth,
            int birthYear,
            string district,
            string city,
            string refName,
            string refDetail,
            decimal totalAmountFrom,
            decimal totalAmountTo,
            string level,
            string label,
            int page, int pagesize)
        {
            var filter = Builders<Customer>.Filter;
            var filterdefine = filter.Empty;
            if (!string.IsNullOrEmpty(keyword))
            {
                keyword = keyword.ToUpper();
                var emailkeyword = keyword.ToLower();
                filterdefine &= filter.Where(m => m.FirstName.Contains(keyword) || m.LastName.Contains(keyword) || m.Phone.Contains(keyword) || m.Email.Contains(emailkeyword));
            }

            if (gender.HasValue) filterdefine &= filter.Where(m => m.Gender == gender);
          

            if (birthday >= 0) filterdefine &= filter.Where(m => m.BirthDay == birthday);

            if (birthMonth >= 0) filterdefine &= filter.Where(m => m.BirthMonth == birthMonth);

            if (birthYear >= 0) filterdefine &= filter.Where(m => m.BirthYear == birthYear);

            if (!string.IsNullOrEmpty(district)) filterdefine &= filter.Where(m => m.AddressList.Where(a => a.District == district).Any());
            if (!string.IsNullOrEmpty(city)) filterdefine &= filter.Where(m => m.AddressList.Where(a => a.City == city).Any());
            if (!string.IsNullOrEmpty(refName)) filterdefine &= filter.Where(m => m.ReferenceName == refName);
            if (!string.IsNullOrEmpty(refDetail)) filterdefine &= filter.Where(m => m.ReferenceDetail == refDetail);
            if (totalAmountFrom > 0) filterdefine &= filter.Where(m => m.TotalAmount >= totalAmountFrom);
            if (totalAmountTo > 0) filterdefine &= filter.Where(m => m.TotalAmount <= totalAmountTo);

            level = level.ToUrl();
            if (!string.IsNullOrEmpty(level)) filterdefine &= filter.Where(m => m.Level.Name  == level);
            label = label.ToUrl();
            if (!string.IsNullOrEmpty(label)) filterdefine &= filter.Where(m => m.Labels.Where(l => l.Url == label).Any());

            var totalItem = DbSet.CountDocuments(filterdefine);
            var all = DbSet.Find(filterdefine).Skip((page - 1) * pagesize).Limit(pagesize);
            return new PagedResult<Customer>(all.ToList(), page, pagesize, (int)totalItem);
        }
    }
}
