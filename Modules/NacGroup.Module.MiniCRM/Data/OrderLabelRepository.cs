﻿using MongoDB.Driver;
using NacGroup.Infrastructure.Shared;
using NacGroup.Module.Core.Data;
using NacGroup.Module.Core.Interfaces;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Core.Models.Schema;
using NacGroup.Module.MiniCRM.Models.Schema;

namespace NacGroup.Module.MiniCRM.Data
{
    public interface IOrderLabelRepository : IRepository<OrderLabel>
    {
        PagedResult<OrderLabel> Filter(string name,string color, int page, int pagesize);
    }

    public class OrderLabelRepository : MongoRepository<OrderLabel>, IOrderLabelRepository, IMigrationRepository
    {
        public OrderLabelRepository(IMongoContext context) : base(context)
        {
        }


        public void Migration()
        {
            DbSet.Indexes.CreateOne(model: new CreateIndexModel<OrderLabel>("{Url:1}", new CreateIndexOptions { Unique = true }));
        }

        public void Seed()
        {
           
        }

        public PagedResult<OrderLabel> Filter(string name,string color, int page, int pagesize)
        {
            var filter = Builders<OrderLabel>.Filter;
            var filterdefine = filter.Empty;
            if (!string.IsNullOrEmpty(name))
            {
                name = name.ToUrl();
                filterdefine &= filter.Where(m => m.Url.Contains(name));
            }
            if (!string.IsNullOrEmpty(color))
            {
                filterdefine &= filter.Where(m => m.Color.Contains(color));
            }

            var totalItem = DbSet.CountDocuments(filterdefine);
            var all = DbSet.Find(filterdefine).Skip((page - 1) * pagesize).Limit(pagesize);
            return new PagedResult<OrderLabel>(all.ToList(), page, pagesize, (int)totalItem);
        }
    }
}
