﻿using MongoDB.Driver;
using NacGroup.Infrastructure.Shared;
using NacGroup.Module.Core.Data;
using NacGroup.Module.Core.Interfaces;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Core.Models.Schema;
using NacGroup.Module.MiniCRM.Models.Schema;

namespace NacGroup.Module.MiniCRM.Data
{
    public interface ICustomerLabelRepository : IRepository<CustomerLabel>
    {
        PagedResult<CustomerLabel> Filter(string name,string color, int page, int pagesize);
    }

    public class CustomerLabelRepository : MongoRepository<CustomerLabel>, ICustomerLabelRepository, IMigrationRepository
    {
        public CustomerLabelRepository(IMongoContext context) : base(context)
        {
        }


        public void Migration()
        {
            DbSet.Indexes.CreateOne(model: new CreateIndexModel<CustomerLabel>("{Url:1}", new CreateIndexOptions { Unique = true }));
        }

        public void Seed()
        {
           
        }

        public PagedResult<CustomerLabel> Filter(string name,string color, int page, int pagesize)
        {
            var filter = Builders<CustomerLabel>.Filter;
            var filterdefine = filter.Empty;
            if (!string.IsNullOrEmpty(name))
            {
                name = name.ToUrl();
                filterdefine &= filter.Where(m => m.Url.Contains(name));
            }
            if (!string.IsNullOrEmpty(color))
            {
                filterdefine &= filter.Where(m => m.Color.Contains(color));
            }

            var totalItem = DbSet.CountDocuments(filterdefine);
            var all = DbSet.Find(filterdefine).Skip((page - 1) * pagesize).Limit(pagesize);
            return new PagedResult<CustomerLabel>(all.ToList(), page, pagesize, (int)totalItem);
        }
    }
}
