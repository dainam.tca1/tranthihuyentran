﻿using MongoDB.Driver;
using NacGroup.Infrastructure.Shared;
using NacGroup.Module.Core.Interfaces;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Core.Models.Schema;
using System;
using System.Collections.Generic;
using System.Linq;
using NacGroup.Module.Core.Data;
using NacGroup.Module.MiniCRM.Models.Schema;

namespace NacGroup.Module.MiniCRM.Data
{
    public interface ICustomerOrderRepository : IRepository<CustomerOrder>
    {
        PagedResult<CustomerOrder> Filter(string orderId,decimal? amountFrom, decimal? amountTo,string label,string customerId,DateTime timeFrom,DateTime timeTo, int page, int pagesize);
        int TotalOrderByCustomer(string customerId);
        List<CustomerOrder> GetListOrder(DateTime? timeFrom, DateTime? timeTo);
    }

    public class CustomerOrderRepository : MongoRepository<CustomerOrder>, ICustomerOrderRepository, IMigrationRepository
    {
        public CustomerOrderRepository(IMongoContext context) : base(context)
        {
        }


        public void Migration()
        {
            DbSet.Indexes.CreateOne(model: new CreateIndexModel<CustomerOrder>("{OrderId:1}", new CreateIndexOptions { Unique = true }));
        }

        public void Seed()
        {
           
        }
        public int TotalOrderByCustomer(string customerId)
        {
            return Get(m => m.CustomerId == customerId).Count();
        }

        public List<CustomerOrder> GetListOrder(DateTime? timeFrom, DateTime? timeTo)
        {
            return Get().Where(m => m.OrderDateTime >= timeFrom && m.OrderDateTime <= timeTo).ToList();
        }

        public PagedResult<CustomerOrder> Filter(string orderId, decimal? amountFrom, decimal? amountTo, string label, string customerId, DateTime timeFrom, DateTime timeTo, int page, int pagesize)
        {
            var filter = Builders<CustomerOrder>.Filter;
            var filterdefine = filter.Empty;
            if (!string.IsNullOrEmpty(orderId))
            {
                filterdefine &= filter.Where(m => m.OrderId.Contains(orderId));
            }
            if (!string.IsNullOrEmpty(customerId))
            {
                filterdefine &= filter.Where(m => m.CustomerId.Contains(customerId));
            }

            if (!string.IsNullOrEmpty(label))
            {
                label = label.ToUrl();
                filterdefine &= filter.Where(m => m.Label.Id.Contains(label));
            }

            if (amountFrom > 0)
            {
                filterdefine &= filter.Where(m => m.Amount >= amountFrom);
            }

            if (amountTo > 0)
            {
                filterdefine &= filter.Where(m => m.Amount <= amountTo);
            }

            if (timeFrom != DateTime.MinValue)
            {
                filterdefine &= filter.Where(m => m.OrderDateTime >= timeFrom);
            }
            if (timeTo != DateTime.MinValue)
            {
                filterdefine &= filter.Where(m => m.OrderDateTime <= timeTo);
            }

            var totalItem = DbSet.CountDocuments(filterdefine);
            var all = DbSet.Find(filterdefine).Skip((page - 1) * pagesize).Limit(pagesize);
            return new PagedResult<CustomerOrder>(all.ToList(), page, pagesize, (int)totalItem);
        }
    }
}
