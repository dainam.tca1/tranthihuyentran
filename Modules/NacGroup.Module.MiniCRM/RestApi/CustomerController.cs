﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using NacGroup.Infrastructure.Shared;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Core.Services;
using System.Linq;
using NacGroup.Module.MiniCRM.Services;

namespace NacGroup.Module.MiniCRM.RestApi
{
    [Route("api/customer")]
    [ApiController]
    public class CustomerController : ControllerBase
    {

        private readonly ICustomerService _customerService;

        public CustomerController(ICustomerService customerService)
        {
            _customerService = customerService;
        }

        [HttpGet]
        public ActionResult<ApiResponseModel> Index(int day, int month)
        {
            var basicAuthorSplit = Request.Headers["Authorization"].ToString().Split(" ")[1];
            var authorzial = basicAuthorSplit.Base64Decode().Split(":");
            var user = authorzial[0];
            var pass = authorzial[1];
            if (user != "dan@dkmobility.com") return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            if (pass != "qJI$J3jE+NA1") return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            var customerList = _customerService.Get().Where(m => m.BirthDay == day && m.BirthMonth == month).ToList();
            return new ApiResponseModel("success", StaticMessage.DataLoadingSuccessfull, customerList);
        }
    }
}