﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using NacGroup.Module.Core.Interfaces;
using NacGroup.Module.MiniCRM.Data;
using NacGroup.Module.MiniCRM.Services;

namespace NacGroup.Module.MiniCRM
{
    public class ModuleInitializer : IModuleInitializer
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddTransient<ICustomerRepository, CustomerRepository>();
            services.AddTransient<ICustomerService, CustomerService>();

            services.AddTransient<ICustomerLevelRepository, CustomerLevelRepository>();
            services.AddTransient<ICustomerLevelService, CustomerLevelService>();

            services.AddTransient<ICustomerLabelRepository, CustomerLabelRepository>();
            services.AddTransient<ICustomerLabelService, CustomerLabelService>();

            services.AddTransient<ICustomerOrderRepository, CustomerOrderRepository>();
            services.AddTransient<ICustomerOrderService, CustomerOrderService>();

            services.AddTransient<IOrderLabelRepository, OrderLabelRepository>();
            services.AddTransient<IOrderLabelService, OrderLabelService>();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
        }
    }
}
