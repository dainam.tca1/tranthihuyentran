﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Product.Models.Schema;
using NacGroup.Module.Product.Services;
using Newtonsoft.Json.Linq;

namespace NacGroup.Module.Product.Controllers
{
    [Authorize(Roles = "Admin", Policy = "TenantAuthorize")]
    [Route("cms/api/comment/{action=Index}")]
    public class CommentCmsApiController : Controller
    {
        private ICommentService _commentService;

        public CommentCmsApiController(ICommentService commentService)
        {
            _commentService = commentService;
        }


        [HttpGet]
        public ActionResult<ApiResponseModel> Index(int page , int pagesize = 10)
        {
            var paged = _commentService.FilterComment(page, pagesize);
            return new ApiResponseModel("success", paged);
        }


        [HttpPost]
        public ActionResult<ApiResponseModel> Add([FromBody] Comment model)
        {
            return _commentService.AddAdmin(model);
        }

        public ActionResult<ApiResponseModel> Update(string id)
        {
            var item = _commentService.GetById(id);
            if (item == null)
                return new ApiResponseModel("error", StaticMessage.DataNotFound);

            return new ApiResponseModel("success", item);
        }

        [HttpPost]
        public ActionResult<ApiResponseModel> Update([FromBody] Comment model)
        {
            return _commentService.Update(model);
        }

        [HttpPost]
        public ActionResult<ApiResponseModel> UpdateCustomize([FromBody]JObject data)
        {
            var model = data["model"].ToObject<Comment>();
            var name = data["name"].Value<string>();
            return _commentService.UpdateCustomize(model, name);
        }
    }
}