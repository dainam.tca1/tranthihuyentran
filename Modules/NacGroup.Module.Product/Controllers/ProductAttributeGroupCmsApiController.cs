﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Product.Models.Schema;
using NacGroup.Module.Product.Services;
using Newtonsoft.Json.Linq;

namespace NacGroup.Module.Product.Controllers
{
    [Authorize(Roles = "Admin", Policy = "TenantAuthorize")]
    [Route("cms/api/productattributegroup/{action=Index}")]
    public class ProductAttributeGroupCmsApiController : Controller
    {
        private readonly IProductAttributeGroupService _productAttributeGroupService;

        public ProductAttributeGroupCmsApiController(IProductAttributeGroupService productAttributeGroupService)
        {
            _productAttributeGroupService = productAttributeGroupService;
        }


        [HttpGet]
        public ActionResult<ApiResponseModel> Index()
        {
            var paged = _productAttributeGroupService.GetAll().ToList();

            return new ApiResponseModel("success", new PagedResult<ProductAttributeGroup>(paged,1,paged.Count,paged.Count));
        }


        [HttpPost]
        public ActionResult<ApiResponseModel> RemoveList([FromBody] List<string> idList)
        {
            return _productAttributeGroupService.Delete(idList);
        }

        [HttpPost]
        public ActionResult<ApiResponseModel> Add([FromBody] ProductAttributeGroup model)
        {
            return _productAttributeGroupService.Add(model);
        }

    }
}