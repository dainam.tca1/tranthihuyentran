﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Product.Models.Schema;
using NacGroup.Module.Product.Services;
using Newtonsoft.Json.Linq;
using NacGroup.Infrastructure.Shared;

namespace NacGroup.Module.Product.Controllers
{
    [Authorize(Roles = "Admin", Policy = "TenantAuthorize")]
    [Route("cms/api/productcategory/{action=Index}")]
    public class ProductCategoryCmsApiController : Controller
    {
        private IProductCategoryService _productCategoryService;

        public ProductCategoryCmsApiController(IProductCategoryService productCategoryService)
        {
            _productCategoryService = productCategoryService;
        }


        [HttpGet]
        public ActionResult<ApiResponseModel> Index()
        {
            var paged = _productCategoryService.GetAll();

            return new ApiResponseModel("success", new PagedResult<ProductCategory>(paged,1,paged.Count,paged.Count));
        }

        [HttpGet]
        public IActionResult GetShippingCategoryList()
        {
            return Ok(StaticShippingCategoryList.ShippingCategoryList);
        }


        [HttpPost]
        public ActionResult<ApiResponseModel> RemoveList([FromBody] List<string> idList)
        {
            return _productCategoryService.Delete(idList);
        }

        [HttpPost]
        public ActionResult<ApiResponseModel> Add([FromBody] ProductCategory model)
        {
            return _productCategoryService.Add(model);
        }

        public ActionResult<ApiResponseModel> Update(string id)
        {
            var item = _productCategoryService.GetById(id);
            if (item == null)
                return new ApiResponseModel("error", StaticMessage.DataNotFound);

            return new ApiResponseModel("success", item);
        }
        [HttpPost]
        public ActionResult<ApiResponseModel> Update([FromBody] ProductCategory model)
        {
            return _productCategoryService.Update(model);
        }

        [HttpPost]
        public ActionResult<ApiResponseModel> UpdateCustomize([FromBody]JObject data)
        {
            var model = data["model"].ToObject<ProductCategory>();
            var name = data["name"].Value<string>();
            return _productCategoryService.UpdateCustomize(model, name);
        }
    }
}