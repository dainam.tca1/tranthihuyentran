﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Product.Models.Schema;
using NacGroup.Module.Product.Services;
using Newtonsoft.Json.Linq;

namespace NacGroup.Module.Product.Controllers
{
    [Authorize(Roles = "Admin", Policy = "TenantAuthorize")]
    [Route("cms/api/brand/{action=Index}")]
    public class BrandCmsApiController : Controller
    {
        private IBrandService _brandService;

        public BrandCmsApiController(IBrandService brandService)
        {
            _brandService = brandService;
        }


        [HttpGet]
        public ActionResult<ApiResponseModel> Index()
        {
            var paged = _brandService.GetAll().OrderBy(m=>m.Sort).ToList();

            return new ApiResponseModel("success", new PagedResult<Brand>(paged,1,paged.Count,paged.Count));
        }


        [HttpPost]
        public ActionResult<ApiResponseModel> RemoveList([FromBody] List<string> idList)
        {
            return _brandService.Delete(idList);
        }

        [HttpPost]
        public ActionResult<ApiResponseModel> Add([FromBody] Brand model)
        {
            return _brandService.Add(model);
        }

        public ActionResult<ApiResponseModel> Update(string id)
        {
            var item = _brandService.GetById(id);
            if (item == null)
                return new ApiResponseModel("error", StaticMessage.DataNotFound);

            return new ApiResponseModel("success", item);
        }
        [HttpPost]
        public ActionResult<ApiResponseModel> Update([FromBody] Brand model)
        {
            return _brandService.Update(model);
        }

        [HttpPost]
        public ActionResult<ApiResponseModel> UpdateCustomize([FromBody]JObject data)
        {
            var model = data["model"].ToObject<Brand>();
            var name = data["name"].Value<string>();
            return _brandService.UpdateCustomize(model, name);
        }
    }
}