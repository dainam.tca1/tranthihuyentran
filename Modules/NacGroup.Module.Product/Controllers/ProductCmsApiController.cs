﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NacGroup.Module.Core.Interfaces;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Product.Services;
using Newtonsoft.Json.Linq;
using NacGroup.Infrastructure.Shared;

namespace NacGroup.Module.Product.Controllers
{
    [Authorize(Roles = "Admin", Policy = "TenantAuthorize")]
    [Route("cms/api/product/{action=Index}")]
    public class ProductCmsApiController : Controller
    {
        private readonly IProductService _productService;
        private readonly Tenant _tenant;

        public ProductCmsApiController(ITenantProvider tenantProvider, IProductService productService)
        {
            _tenant = tenantProvider.GetTenant();
            _productService = productService;
        }

        //[Route("/nacgroup/test")]
        //public ActionResult<ApiResponseModel> CloneExcel()
        //{
        //    var count = 0;

        //    try
        //    {
        //        //Lets open the existing excel file and read through its content . Open the excel using openxml sdk
        //        using (SpreadsheetDocument doc = SpreadsheetDocument.Open(Path.Combine("wwwroot/ccc.xlsx"), false))
        //        {
        //            WorkbookPart workbookPart = doc.WorkbookPart;
        //            Sheets thesheetcollection = workbookPart.Workbook.GetFirstChild<Sheets>();
        //            StringBuilder excelResult = new StringBuilder();

        //            //using for each loop to get the sheet from the sheetcollection  
        //            foreach (Sheet thesheet in thesheetcollection)
        //            {
        //                excelResult.AppendLine("Excel Sheet Name : " + thesheet.Name);
        //                excelResult.AppendLine("----------------------------------------------- ");
        //                //statement to get the worksheet object by using the sheet id  
        //                Worksheet theWorksheet = ((WorksheetPart)workbookPart.GetPartById(thesheet.Id)).Worksheet;

        //                SheetData thesheetdata = (SheetData)theWorksheet.GetFirstChild<SheetData>();
        //                foreach (Row thecurrentrow in thesheetdata)
        //                {
        //                    //CityCollection exceldata = new CityCollection();
        //                    //DistrictCollection exceldata = new DistrictCollection();
        //                    WardCollection exceldata = new WardCollection();
        //                    foreach (Cell thecurrentcell in thecurrentrow)
        //                    {
        //                        //statement to take the integer value  
        //                        string currentcellvalue = string.Empty;
        //                        if (thecurrentcell.DataType != null)
        //                        {
        //                            count = count + 1;

        //                            if (thecurrentcell.DataType == CellValues.SharedString)
        //                            {
        //                                int id;
        //                                if (Int32.TryParse(thecurrentcell.InnerText, out id))
        //                                {
        //                                    SharedStringItem item = workbookPart.SharedStringTablePart.SharedStringTable.Elements<SharedStringItem>().ElementAt(id);
        //                                    if (item.Text != null)
        //                                    {
        //                                        //code to take the string value  
        //                                        excelResult.Append(item.Text.Text + " ");
        //                                        if (count > 4)
        //                                        {
        //                                            count = 1;
        //                                        }
        //                                        if (count == 1)
        //                                        {
        //                                            exceldata.Code = item.Text.Text;
        //                                        }
        //                                        else if (count == 2)
        //                                        {
        //                                            exceldata.Name = item.Text.Text;
        //                                        }
        //                                        else if (count == 3)
        //                                        {
        //                                            exceldata.Level = item.Text.Text;

        //                                        }
        //                                        else if (count == 4)
        //                                        {

        //                                            var citycode = _shippingService.GetDistrictByCode(item.Text.Text);
        //                                            exceldata.District = citycode;
        //                                            var result = _shippingService.AddWard(exceldata);
        //                                        }

        //                                    }
        //                                    else if (item.InnerText != null)
        //                                    {
        //                                        currentcellvalue = item.InnerText;
        //                                    }
        //                                    else if (item.InnerXml != null)
        //                                    {
        //                                        currentcellvalue = item.InnerXml;
        //                                    }
        //                                }
        //                            }
        //                        }
        //                        else
        //                        {
        //                            excelResult.Append(Convert.ToInt16(thecurrentcell.InnerText) + " ");
        //                        }
        //                    }
        //                    excelResult.AppendLine();
        //                }
        //                excelResult.Append("");
        //                Console.WriteLine(excelResult.ToString());
        //                Console.ReadLine();
        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {

        //    }
        //    return new ApiResponseModel("success");
        //}


        [HttpPost]
        public ActionResult<ApiResponseModel> ImportExcel()
        {
            var files = Request.Form.Files;
            if (!files.Any())
                return new ApiResponseModel("error", "Please select files");
            return _productService.ImportExcel(files);
           
        }

        [HttpPost]
        public ActionResult<ApiResponseModel> UpdateAll()
        {
              
            return _productService.UpdateAll();

        }

        [HttpGet]
        public IActionResult GetProductTypeList()
        {
            return Ok(ProductType.ProductTypeList);
        }

        [HttpGet]
        public IActionResult GetWeightList()
        {
            return Ok(Dimension.WeightList);
        }

        [HttpGet]
        public IActionResult GetDimensionList()
        {
            return Ok(Dimension.DimensionList);
        }

        [HttpPost]
        public ActionResult<ApiResponseModel> UpdateSort([FromBody]List<Models.Schema.Product> model)
        {
            if (model == null) return new ApiResponseModel("error", StaticMessage.ParameterInvalid);
            return _productService.UpdateSort(model);
        }

        [HttpGet]
        public ActionResult<ApiResponseModel> Index(string name, string categoryid, string skucode, int page = 1,int pagesize = 10)
        {
           return new ApiResponseModel("success", _productService.Filter(name, categoryid, skucode, page,pagesize));
        }

        [HttpGet]
        public ActionResult<ApiResponseModel> Get()
        {
            return new ApiResponseModel("success", _productService.Get());
        }

        [HttpPost]
        public ActionResult<ApiResponseModel> RemoveList([FromBody] List<string> idList)
        {
            return _productService.Delete(idList);
        }

        [HttpPost]
        public ActionResult<ApiResponseModel> Add([FromBody] Models.Schema.Product model)
        {
            return _productService.Add(model);
        }

        public ActionResult<ApiResponseModel> Update(string id)
        {
            var item = _productService.GetById(id);
            if (item == null)
                return new ApiResponseModel("error", StaticMessage.DataNotFound);

            return new ApiResponseModel("success", item);
        }

        [HttpPost]
        public ActionResult<ApiResponseModel> Update([FromBody] Models.Schema.Product model)
        {
            return _productService.Update(model);
        }

        [HttpPost]
        public ActionResult<ApiResponseModel> UpdateCustomize([FromBody]JObject data)
        {
            var model = data["model"].ToObject<Models.Schema.Product>();
            var name = data["name"].Value<string>();
            return _productService.UpdateCustomize(model, name);
        }

    }
}
