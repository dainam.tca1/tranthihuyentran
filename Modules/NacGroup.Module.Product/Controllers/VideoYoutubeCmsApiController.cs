﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Product.Models.Schema;
using NacGroup.Module.Product.Services;
using Newtonsoft.Json.Linq;

namespace NacGroup.Module.Product.Controllers
{
    [Authorize(Roles = "Admin", Policy = "TenantAuthorize")]
    [Route("cms/api/video/{action=Index}")]
    public class VideoCmsApiController : Controller
    {
        private IVideoYoutubeService _videoYoutubeService;

        public VideoCmsApiController(IVideoYoutubeService videoYoutubeService)
        {
            _videoYoutubeService = videoYoutubeService;
        }


        [HttpGet]
        public ActionResult<ApiResponseModel> Index()
        {
            var paged = _videoYoutubeService.GetAll().OrderBy(m=>m.Sort).ToList();

            return new ApiResponseModel("success", new PagedResult<VideoYoutube>(paged,1,paged.Count,paged.Count));
        }


        [HttpPost]
        public ActionResult<ApiResponseModel> RemoveList([FromBody] List<string> idList)
        {
            return _videoYoutubeService.Delete(idList);
        }

        [HttpPost]
        public ActionResult<ApiResponseModel> Add([FromBody] VideoYoutube model)
        {
            return _videoYoutubeService.Add(model);
        }

        public ActionResult<ApiResponseModel> Update(string id)
        {
            var item = _videoYoutubeService.GetById(id);
            if (item == null)
                return new ApiResponseModel("error", StaticMessage.DataNotFound);

            return new ApiResponseModel("success", item);
        }
        [HttpPost]
        public ActionResult<ApiResponseModel> Update([FromBody] VideoYoutube model)
        {
            return _videoYoutubeService.Update(model);
        }

        [HttpPost]
        public ActionResult<ApiResponseModel> UpdateCustomize([FromBody]JObject data)
        {
            var model = data["model"].ToObject<VideoYoutube>();
            var name = data["name"].Value<string>();
            return _videoYoutubeService.UpdateCustomize(model, name);
        }
    }
}