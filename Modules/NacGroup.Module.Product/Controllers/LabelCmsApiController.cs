﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Product.Models.Schema;
using NacGroup.Module.Product.Services;

namespace NacGroup.Module.Product.Controllers
{
    [Authorize(Roles = "Admin", Policy = "TenantAuthorize")]
    [Route("cms/api/label/{action=Index}")]
    public class LabelCmsApiController : Controller
    {
        private readonly ILabelService _labelService;

        public LabelCmsApiController(ILabelService labelService)
        {
            _labelService = labelService;
        }


        [HttpGet]
        public ActionResult<ApiResponseModel> Index()
        {
            var paged = _labelService.GetAll().ToList();

            return new ApiResponseModel("success", new PagedResult<ProductLabel>(paged, 1, paged.Count, paged.Count));
        }


        [HttpPost]
        public ActionResult<ApiResponseModel> RemoveList([FromBody] List<string> idList)
        {
            return _labelService.Delete(idList);
        }

        [HttpPost]
        public ActionResult<ApiResponseModel> Add([FromBody] ProductLabel model)
        {
            return _labelService.Add(model);
        }

    }
}