﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Product.Models.Schema;
using NacGroup.Module.Product.Services;
using Newtonsoft.Json.Linq;

namespace NacGroup.Module.Product.Controllers
{
    [Authorize(Roles = "Admin", Policy = "TenantAuthorize")]
    [Route("cms/api/productattribute/{action=Index}")]
    public class ProductAttributeCmsApiController : Controller
    {
        private readonly IProductAttributeService _productAttributeService;

        public ProductAttributeCmsApiController(IProductAttributeService productAttributeService)
        {
            _productAttributeService = productAttributeService;
        }


        [HttpGet]
        public ActionResult<ApiResponseModel> Index()
        {
            var paged = _productAttributeService.GetAll().ToList();

            return new ApiResponseModel("success", new PagedResult<ProductAttribute>(paged,1,paged.Count,paged.Count));
        }


        [HttpPost]
        public ActionResult<ApiResponseModel> RemoveList([FromBody] List<string> idList)
        {
            return _productAttributeService.Delete(idList);
        }

        [HttpPost]
        public ActionResult<ApiResponseModel> Add([FromBody] ProductAttribute model)
        {
            return _productAttributeService.Add(model);
        }

    }
}