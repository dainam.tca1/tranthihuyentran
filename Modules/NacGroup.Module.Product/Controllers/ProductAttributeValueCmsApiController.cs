﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Product.Models.Schema;
using NacGroup.Module.Product.Services;

namespace NacGroup.Module.Product.Controllers
{
    [Authorize(Roles = "Admin", Policy = "TenantAuthorize")]
    [Route("cms/api/productattributevalue/{action=Index}")]
    public class ProductAttributeValueCmsApiController : Controller
    {
        private readonly IProductAttributeValueService _productAttributeValueService;

        public ProductAttributeValueCmsApiController(IProductAttributeValueService productAttributeValueService)
        {
            _productAttributeValueService = productAttributeValueService;
        }


        [HttpGet]
        public ActionResult<ApiResponseModel> Index()
        {
            var paged = _productAttributeValueService.GetAll().ToList();

            return new ApiResponseModel("success", new PagedResult<ProductAttributeValue>(paged,1,paged.Count,paged.Count));
        }


        [HttpPost]
        public ActionResult<ApiResponseModel> RemoveList([FromBody] List<string> idList)
        {
            return _productAttributeValueService.Delete(idList);
        }

        [HttpPost]
        public ActionResult<ApiResponseModel> Add([FromBody] ProductAttributeValue model)
        {
            return _productAttributeValueService.Add(model);
        }

    }
}