﻿using NacGroup.Module.Product.Services;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Product.Models.Schema;

namespace NacGroup.Module.Product.Controllers
{
    [Route("comment/{action=Index}")]
    public class CommentController : Controller
    {
        private readonly ICommentService _commentService;

        public CommentController(ICommentService commentService)
        {
            _commentService = commentService;
        }


        [HttpGet]
        public ActionResult<ApiResponseModel> Index(string id,int page, int pagesize)
        {
            var comment = _commentService.Filter(id,page,pagesize);
            if (comment.Results.Count() <= 0)
                return new ApiResponseModel("error", StaticMessage.DataNotFound);


            return new ApiResponseModel("success", comment.Results);
        }

        [HttpPost]
        public ActionResult<ApiResponseModel> Rating([Bind("Name,Email,Phone,Body,ProductId,Rating")] Comment model)
        {

            var result = _commentService.Add(model);
            if (result.status == "error")
            {
                return new ApiResponseModel("error", result.message);
            }
            return new ApiResponseModel("success", result.message);
        }

        [HttpPost]
        public ActionResult<ApiResponseModel> Reply([Bind("Name,Email,Phone,Body,ProductId,ParentId")] Comment model)
        {

            var result = _commentService.AddReply(model);
            if (result.status == "error")
            {
                return new ApiResponseModel("error", result.message);
            }
            return new ApiResponseModel("success", result.message);
        }






    }
}