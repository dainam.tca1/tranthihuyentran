﻿using System.Collections.Generic;
using NacGroup.Module.Product.Services;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using System;
using NacGroup.Infrastructure.Shared;
using NacGroup.Module.Product.Models;
using Newtonsoft.Json;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Product.Models.Schema;

namespace NacGroup.Module.Product.Controllers
{
    [Route("product/{action=Index}")]
    public class ProductController : Controller
    {
        private readonly IProductWebService _productWebService;

        public ProductController(IProductWebService productWebService)
        {
            _productWebService = productWebService;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="url"></param>
        /// <param name="attr">Dinh dang mau-sac|trang@kich-thuoc|s</param>
        /// <returns></returns>
        [Route("/productdetail/{url}/{attr?}")]
        public IActionResult Index(string url, string attr)
        {
            var product = _productWebService.GetByUrl(url);
            if (product == null)
                return Redirect("/");
            if (string.IsNullOrEmpty(attr))
            {
                ViewData["ProductSku"] = product.Skus.FirstOrDefault();
            }
            else
            {
                var attrList = new List<Tuple<string, string>>();
                var attrliststr = attr.Split("@");
                if (!attrliststr.Any())
                    return Redirect("/");
                foreach (var item in attrliststr)
                {
                    var attributestr = item.Split("|");
                    if (attributestr.Count() != 2)
                        return Redirect("/");
                    attrList.Add(new Tuple<string, string>(attributestr[0], attributestr[1]));
                }

                var sku = product.Skus.FirstOrDefault(m => attrList.All(at =>
                m.Attributes.Any(a => at.Item1 == a.Slug && a.Values.FirstOrDefault() != null && at.Item2 == a.Values.FirstOrDefault().Slug)));


                while (sku == null && attrList.Count > 1)
                {
                    attrList.Remove(attrList[attrList.Count - 1]);
                    sku = product.Skus.FirstOrDefault(m => attrList.All(at =>
                    m.Attributes.Any(a => at.Item1 == a.Slug && a.Values.FirstOrDefault() != null && at.Item2 == a.Values.FirstOrDefault().Slug)));
                }

                if (sku == null)
                {
                    return Redirect("/");
                }
                ViewData["ProductSku"] = sku;
            }

            
            return View(product);
        }


        [Route("/productinfo/{code}")]
        public IActionResult Index(string code)
        {
            var sku = _productWebService.GetSkuByCode(code);
            if (sku == null)
                return Redirect("/");
        
            return View("_Sku",sku);
        }

        [Route("/productcat/{url}",Name = "danh-muc-san-pham")]
        public IActionResult GetProductByCategoryUrl(string url,ProductFilterSortType sort = ProductFilterSortType.NewToOld, int page = 1, int pagesize = 20)
        {
            var productcat = _productWebService.GetCategoryByUrl(url);
            if (productcat == null)
                return NotFound();

            var filterUrl = Url.RouteUrl("danh-muc-san-pham", new
            {
                url = productcat.Url,
                sort = (int)sort,
                pagesize = (int)pagesize
            });
            if (!string.IsNullOrEmpty(filterUrl) && filterUrl.Contains("?"))
                filterUrl += "&";
            else
                filterUrl += "?";
            ViewData["FilterUrl"] = filterUrl;
            ViewData["Sort"] = (int)sort;
            ViewData["ProductCat"] = productcat;
            return View("CategoryDetail", _productWebService.Filter(null, productcat.Id, null, null, null, sort, page, pagesize, true));
        }


        [Route("/promotion-deals", Name = "san-pham-khuyen-mai")]
        public IActionResult GetProductSpecial(ProductFilterSortType sort = ProductFilterSortType.NewToOld, int page = 1, int pagesize = 20)
        {
           
            var product = _productWebService.GetAll().Where(m => m.Skus.Where(s => s.FromDate < DateTime.Now && s.ToDate > DateTime.Now && s.SpecialPrice > 0 && s.IsActive).Any());
            var sku_list = new List<Sku>();
            foreach (var item in product)
            {
                var special_sku = item.Skus.Where(s => s.FromDate < DateTime.Now && s.ToDate > DateTime.Now && s.SpecialPrice > 0);
                if (special_sku.Any())
                {
                    foreach (var sku_spec in special_sku)
                    {
                        sku_list.Add(sku_spec);
                    }
                }
            }
            var result = sku_list.Skip((page - 1) * pagesize).Take(pagesize).ToList();
            var filterUrl = Url.RouteUrl("san-pham-khuyen-mai", new
            {
                sort = (int)sort,
                pagesize = (int)pagesize
            });
            if (!string.IsNullOrEmpty(filterUrl) && filterUrl.Contains("?"))
                filterUrl += "&";
            else
                filterUrl += "?";
            ViewData["FilterUrl"] = filterUrl;
            ViewData["Sort"] = (int)sort;
            return View("_PromotionDeals", new PagedResult<Sku>(result, page,pagesize, sku_list.Count()));
        }

        [Route("/tim-kiem", Name= "tim-kiem")]
        public IActionResult Search(string keyword, int page = 1, int pagesize = 20, ProductFilterSortType sort = ProductFilterSortType.NewToOld)
        {
            var filterUrl = Url.RouteUrl("tim-kiem", new { keyword, sort = (int)sort, pagesize = (int)pagesize });
            if (!string.IsNullOrEmpty(filterUrl) && filterUrl.Contains("?"))
                filterUrl += "&";
            else
                filterUrl += "?";

            ViewData["FilterUrl"] = filterUrl;
            ViewData["Sort"] = (int)sort;
            ViewData["Keyword"] = keyword;
            var product = _productWebService.Filter(keyword, null, null, null, null, sort, page, pagesize).Results.Where( m=> m.IsActive);
            if (product == null)
                return NotFound();

            return View("Search", new PagedResult<Models.Schema.Product>(product, page, pagesize, product.Count()));
        }


        [HttpGet]
        public IActionResult SearchAjax(string keyword, int page = 1, int pagesize = 20, ProductFilterSortType sort = ProductFilterSortType.NewToOld)
        {
            var filterUrl = Url.RouteUrl("tim-kiem", new { keyword, sort = (int)sort});
            if (!string.IsNullOrEmpty(filterUrl) && filterUrl.Contains("?"))
                filterUrl += "&";
            else
                filterUrl += "?";

            ViewData["FilterUrl"] = filterUrl;
            ViewData["Sort"] = (int)sort;
            ViewData["Keyword"] = keyword;
            var product = _productWebService.Filter(keyword, null, null, null, null, sort, page, pagesize).Results.Where(m => m.IsActive);
            if (product == null)
                return NotFound();

            return PartialView("_ListSearch", new PagedResult<Models.Schema.Product> (product,page,pagesize, product.Count()));
        }

        

        public IEnumerable<ProductCategory> GetCategories()
        {
            var categories = _productWebService.GetCategories();
            if (categories == null)
            {
                return null;
            }
            return categories;
        }
    }
}