﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Product.Models.Schema;
using NacGroup.Module.Product.Services;
using Newtonsoft.Json.Linq;

namespace NacGroup.Module.Product.Controllers
{
    [Authorize(Roles = "Admin", Policy = "TenantAuthorize")]
    [Route("cms/api/productcustomcategory/{action=Index}")]
    public class ProductCustomCategoryCmsApiController : Controller
    {
        private IProductCustomCategoryService _productCustomCategoryService;

        public ProductCustomCategoryCmsApiController(IProductCustomCategoryService productCustomCategoryService)
        {
            _productCustomCategoryService = productCustomCategoryService;
        }


        [HttpGet]
        public ActionResult<ApiResponseModel> Index()
        {
            var paged = _productCustomCategoryService.GetAll();

            return new ApiResponseModel("success", new PagedResult<ProductCustomCategory>(paged,1,paged.Count,paged.Count));
        }


        [HttpPost]
        public ActionResult<ApiResponseModel> RemoveList([FromBody] List<string> idList)
        {
            return _productCustomCategoryService.Delete(idList);
        }

        [HttpPost]
        public ActionResult<ApiResponseModel> Add([FromBody] ProductCustomCategory model)
        {
            return _productCustomCategoryService.Add(model);
        }

        public ActionResult<ApiResponseModel> Update(string id)
        {
            var item = _productCustomCategoryService.GetById(id);
            if (item == null)
                return new ApiResponseModel("error", StaticMessage.DataNotFound);

            return new ApiResponseModel("success", item);
        }
        [HttpPost]
        public ActionResult<ApiResponseModel> Update([FromBody] ProductCustomCategory model)
        {
            return _productCustomCategoryService.Update(model);
        }

        [HttpPost]
        public ActionResult<ApiResponseModel> UpdateCustomize([FromBody]JObject data)
        {
            var model = data["model"].ToObject<ProductCustomCategory>();
            var name = data["name"].Value<string>();
            return _productCustomCategoryService.UpdateCustomize(model, name);
        }
    }
}