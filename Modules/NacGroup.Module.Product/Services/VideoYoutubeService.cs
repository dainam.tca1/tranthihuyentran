﻿using System;
using System.Collections.Generic;
using System.Linq;
using MediatR;
using NacGroup.Module.Core.Data;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Core.Models.DomainEvents;
using NacGroup.Module.Product.Data;
using NacGroup.Module.Product.Models.Schema;

namespace NacGroup.Module.Product.Services
{
    public interface IVideoYoutubeService
    {
        List<VideoYoutube> GetAll();
        ApiResponseModel Delete(List<string> idList);
        ApiResponseModel Delete(string id);
        ApiResponseModel Add(VideoYoutube model);
        VideoYoutube GetById(string id);
        ApiResponseModel Update(VideoYoutube model);
        ApiResponseModel UpdateCustomize(VideoYoutube model, string name);
    }
    public class VideoYoutubeService : IVideoYoutubeService
    {
        private readonly IVideoYoutubeRepository _videoYoutubeRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMediator _mediator;

        public VideoYoutubeService(IVideoYoutubeRepository videoYoutubeRepository, IUnitOfWork unitOfWork, IMediator mediator)
        {
            _videoYoutubeRepository = videoYoutubeRepository;
            _unitOfWork = unitOfWork;
            _mediator = mediator;
        }

        public List<VideoYoutube> GetAll()
        {
            return _videoYoutubeRepository.Get().ToList();
        }

        public ApiResponseModel Delete(List<string> idList)
        {
            if (idList == null)
                return new ApiResponseModel("error", StaticMessage.SelectDataToRemove);
            foreach (var item in idList)
            {
                Delete(item);
            }
            return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
        }

        public ApiResponseModel Delete(string id)
        {
            var resultcode = _videoYoutubeRepository.Delete(id);
            if (resultcode < 0)
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            resultcode = _unitOfWork.Commit();
            if (resultcode < 0)
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
        }

        public ApiResponseModel Add(VideoYoutube model)
        {
            #region Validate
            if (string.IsNullOrEmpty(model.Title)) return new ApiResponseModel("error", "Title is required");
            if (string.IsNullOrEmpty(model.Url)) return new ApiResponseModel("error", "Url is required");
            var oldcate = _videoYoutubeRepository.GetSingle(m => m.Url == model.Url);
            if (oldcate != null)
                return new ApiResponseModel("error", "Url is exist");
            #endregion

            try
            {
                
                var result = _videoYoutubeRepository.Add(model);
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);

                result = _unitOfWork.Commit();
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);

                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }

        public VideoYoutube GetById(string id)
        {
            return _videoYoutubeRepository.GetById(id);
        }

        public ApiResponseModel Update(VideoYoutube model)
        {
            #region Validate
            if (string.IsNullOrEmpty(model.Title)) return new ApiResponseModel("error", "Title is required");
            if (string.IsNullOrEmpty(model.Url)) return new ApiResponseModel("error", "Url is required");

            var temp = _videoYoutubeRepository.GetById(model.Id);
            if (temp == null)
                return new ApiResponseModel("error", StaticMessage.DataNotFound);

            var oldcate = _videoYoutubeRepository.GetSingle(m => m.Url == model.Url);
            if (oldcate != null && temp.Id != oldcate.Id)
                return new ApiResponseModel("error", "Url is exist");

            #endregion

            try
            {
                temp.Avatar = model.Avatar;
                temp.MetaDescription = model.MetaDescription;
                temp.MetaKeyword = model.MetaKeyword;
                temp.MetaTitle = model.MetaTitle;
                temp.Sort = model.Sort;
                temp.Url = model.Url;
                temp.Description = model.Description;
                temp.UpdatedDate = DateTime.Now;
                var result = _videoYoutubeRepository.Update(model.Id, temp);
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);       
                result = _unitOfWork.Commit();
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);

                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }

        public ApiResponseModel UpdateCustomize(VideoYoutube model, string name)
        {
            try
            {
                if (model == null || string.IsNullOrEmpty(name)) return new ApiResponseModel("error", StaticMessage.DataNotFound);

                var temp = _videoYoutubeRepository.GetById(model.Id);
                if (temp == null) return new ApiResponseModel("error", StaticMessage.DataNotFound);

                temp[name] = model[name];
                var result = _videoYoutubeRepository.Update(model.Id, temp);
                if (result < 0) return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                result = _unitOfWork.Commit();
                if (result < 0) return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }
    }
}
