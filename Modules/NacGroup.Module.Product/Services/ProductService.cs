﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using NacGroup.Module.Core.Data;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Core.Models.DomainEvents;
using NacGroup.Module.Product.Data;
using NacGroup.Module.Product.Models;
using NacGroup.Module.Product.Models.Schema;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Hosting;
using NacGroup.Module.Core.Interfaces;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using NacGroup.Infrastructure.Shared;
using Microsoft.AspNetCore.Http;
using NacGroup.Module.Core.Models.Schema;

namespace NacGroup.Module.Product.Services
{
    public interface IProductService
    {
        PagedResult<Models.Schema.Product> Filter(string name, string categoryid, string skucode, int page, int pagesize);
        ApiResponseModel Delete(List<string> idList);
        ApiResponseModel Delete(string id);
        ApiResponseModel Add(Models.Schema.Product model);
        Models.Schema.Product GetById(string id);
        ApiResponseModel Update(Models.Schema.Product model);
        ApiResponseModel UpdateCustomize(Models.Schema.Product model, string name);
        Sku GetByCode(string id, string code);
        ApiResponseModel UpdateSku(string productId,string skuCode, decimal? quantity);
        ApiResponseModel UpdateSkuRestore(string productId, string skuCode, decimal? quantity);
        List<Models.Schema.Product> Get();
        ApiResponseModel ImportExcel(IFormFileCollection file);

        ApiResponseModel UpdateSort(List<Models.Schema.Product> model);
        ApiResponseModel UpdateAll();

    }
    public class ProductService : IProductService, INotificationHandler<ProductCategoryChangedDomainEvent>,
        INotificationHandler<BrandChangedDomainEvent>,
        INotificationHandler<BrandRemovedDomainEvent>
    {
        private readonly IProductRepository _productRepository;
        private readonly IProductAttributeRepository _productAttributeRepository;
        private readonly IProductAttributeValueRepository _productAttributeValueRepository;
        private readonly IBrandRepository _brandRepository;
        private readonly IProductCategoryRepository _productCategoryRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILoggerRepository _loggerRepository;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly Tenant _tenant;

        public ProductService(ILoggerRepository loggerRepository, IProductCategoryRepository productCategoryRepository,IBrandRepository brandRepository,IProductAttributeValueRepository productAttributeValueRepository, IProductAttributeRepository productAttributeRepository, ITenantProvider tenantProvider, IHostingEnvironment hostingEnvironment, IProductRepository productRepository, IUnitOfWork unitOfWork)
        {
            _productRepository = productRepository;
            _hostingEnvironment = hostingEnvironment;
            _brandRepository = brandRepository;
            _productCategoryRepository = productCategoryRepository;
            _unitOfWork = unitOfWork;
            _loggerRepository = loggerRepository;
            _productAttributeRepository = productAttributeRepository;
            _productAttributeValueRepository = productAttributeValueRepository;
            _tenant = tenantProvider.GetTenant();
        }

        public PagedResult<Models.Schema.Product> Filter(string name, string categoryid, string skucode, int page, int pagesize)
        {
            return _productRepository.Filter(name, categoryid, skucode, null, null, ProductFilterSortType.NewToOld, page, pagesize);
        }

        public List<Models.Schema.Product> Get()
        {
            return _productRepository.Get().ToList();
        }

        public ApiResponseModel Delete(List<string> idList)
        {
            if (idList == null)
                return new ApiResponseModel("error", StaticMessage.SelectDataToRemove);
            foreach (var item in idList)
            {
                var result = Delete(item);
                if (result.status == "error") return result;
            }
            return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
        }

        public ApiResponseModel UpdateSort(List<Models.Schema.Product> model)
        {
            if (model.Count() == 0 || model == null)
                return new ApiResponseModel("error", StaticMessage.SelectDataToRemove);
            foreach (var item in model)
            {
                var result = Sort(item.Id,item.Sort);
                if (result.status == "error") return result;
            }
            return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
        }

        public ApiResponseModel Sort(string id,int sort)
        {
            var temp = _productRepository.GetById(id);
            if (temp == null) return new ApiResponseModel("error", StaticMessage.DataNotFound);
            temp.Sort = sort;
            var resultcode = _productRepository.Update(id,temp);
            if (resultcode < 0)
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            resultcode = _unitOfWork.Commit();
            if (resultcode < 0)
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
        }

        public ApiResponseModel Delete(string id)
        {
            var resultcode = _productRepository.Delete(id);
            if (resultcode < 0)
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            resultcode = _unitOfWork.Commit();
            if (resultcode < 0)
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
        }

        public ApiResponseModel Add(Models.Schema.Product model)
        {
            try
            {
                #region Validate

                if (string.IsNullOrEmpty(model.Name)) return new ApiResponseModel("error", "Product name is required");
                if (string.IsNullOrEmpty(model.Url)) return new ApiResponseModel("error", "Product url is required");

                var oldproduct = _productRepository.GetSingle(m => m.Url == model.Url);
                if (oldproduct != null) return new ApiResponseModel("error", "Product url is exist");

                if (model.MainCategory == null || string.IsNullOrEmpty(model.MainCategory.Id) || !model.Categories.Any()) return new ApiResponseModel("error", "Product category is required");

                if (model.Categories.Select(m => m.Id).Distinct().Count() < model.Categories.Count)
                {
                    return new ApiResponseModel("error", "Main category must be different from related category");
                }

                if (model.ProductType == null)
                {
                    model.ProductType = ProductTypeDefine.Simple;
                }

                //if (model.Brand != null && string.IsNullOrEmpty(model.Brand.Id))
                //    return new ApiResponseModel("error", "Brand is not valid");

                if (model.ShippingCategory != null && string.IsNullOrEmpty(model.ShippingCategory))
                    return new ApiResponseModel("error", "Shipping Category is required");

                if (!model.Skus.Any()) return new ApiResponseModel("error", "Sku info is required");

                List<string> allSkutributeId = new List<string>();
                foreach (var sku in model.Skus)
                {
                    if (string.IsNullOrEmpty(sku.SkuCode)) return new ApiResponseModel("error", "Sku code is required");
                    if (string.IsNullOrEmpty(sku.Name)) return new ApiResponseModel("error", "Sku name is required");
                    #region valid more option
                    //if (sku.Width <= 0) return new ApiResponseModel("error", "Width attribute must be greater than 0");
                    //if (sku.Height <= 0) return new ApiResponseModel("error", "Height attribute must be greater than 0");
                    //if (sku.Length <= 0) return new ApiResponseModel("error", "Length attribute must be greater than 0");
                    //if (sku.Depth <= 0) return new ApiResponseModel("error", "Depth attribute must be greater than 0");
                    //if (sku.Weight <= 0) return new ApiResponseModel("error", "Weight attribute must be greater than 0");
                    if (sku.SpecialPrice != 0)
                    {
                        if (sku.SpecialPrice > sku.Price) return new ApiResponseModel("error", "Special price cannot be bigger than original price");
                        if (sku.ToDate < sku.FromDate) return new ApiResponseModel("error", "The start date is not greater than the end date");
                    }
                    #endregion
                    if (sku.Attributes.Any(m => string.IsNullOrEmpty(m.Id) || string.IsNullOrEmpty(m.Name) || string.IsNullOrEmpty(m.Slug)))
                        return new ApiResponseModel("error", "Sku attribute is not valid");
                    if (sku.Attributes.Any(m => !m.Values.Any())) return new ApiResponseModel("error", "Sku attribute value is required");
                    if (sku.Attributes.Select(m => m.Id).Distinct().Count() < sku.Attributes.Count)
                        return new ApiResponseModel("error", "Sku attribute cannot be duplicated");

                    if (_productRepository.GetSingle(m => m.Skus.Any(x => x.SkuCode == sku.SkuCode)) != null)
                        return new ApiResponseModel("error", "Sku code cannot be duplicated");

                    allSkutributeId.AddRange(sku.Attributes.Select(m => m.Id));
                    allSkutributeId = allSkutributeId.Distinct().ToList();
                    if (allSkutributeId.Any(m => sku.Attributes.All(x => x.Id != m))) return new ApiResponseModel("error", "Sku attribute is not valid");
                }
                if (model.Skus.Select(m => m.Name).Distinct().Count() < model.Skus.Count)
                    return new ApiResponseModel("error", "Sku attribute cannot be duplicated");

                if (model.Skus.Select(m => m.SkuCode).Distinct().Count() < model.Skus.Count)
                    return new ApiResponseModel("error", "Sku code cannot be duplicated");

                if (model.AttributeGroups.Select(m => m.Id).Distinct().Count() < model.AttributeGroups.Count)
                    return new ApiResponseModel("error", "Product attribute group cannot be duplicated");

                foreach (var attributeGroup in model.AttributeGroups)
                {
                    if (string.IsNullOrEmpty(attributeGroup.Id) || string.IsNullOrEmpty(attributeGroup.Name) || string.IsNullOrEmpty(attributeGroup.Slug) ||
                       !attributeGroup.Attributes.Any()) return new ApiResponseModel("error", "Product attribute group is not valid");

                    if (attributeGroup.Attributes.Select(m => m.Id).Distinct().Count() < attributeGroup.Attributes.Count)
                        return new ApiResponseModel("error", "Product attribute cannot be duplicated");

                    foreach (var attr in attributeGroup.Attributes)
                    {
                        if (string.IsNullOrEmpty(attr.Id) || string.IsNullOrEmpty(attr.Name) || string.IsNullOrEmpty(attr.Slug) ||
                            !attr.Values.Any()) return new ApiResponseModel("error", "Product attribute is not valid");
                    }
                }

                #endregion
                model.MainCategory.FilterList = new List<ProductAttributeGroup>();
                model.Categories = model.Categories.Select(m =>
                {
                    m.FilterList = new List<ProductAttributeGroup>();
                    return m;
                }).ToList();

                if (model.Skus.Any(m => m.Attributes.Any()))
                {
                    model.SkuAttributes = new List<ProductAttribute>();

                    foreach (var sku in model.Skus)
                    {
                        if (!model.SkuAttributes.Any())
                        {
                            
                            model.SkuAttributes.AddRange(JsonConvert.DeserializeObject<List<ProductAttribute>>(JsonConvert.SerializeObject(model.Skus.FirstOrDefault().Attributes)));
                            continue;
                        }
                        foreach (var att in sku.Attributes)
                        {
                            var currentAttr = model.SkuAttributes.FirstOrDefault(m => m.Id == att.Id);
                            var addItems = JsonConvert.DeserializeObject<List<ProductAttributeValue>>(JsonConvert.SerializeObject(att.Values)).Where(m=>currentAttr.Values.All(x=>x.Slug != m.Slug));
                            currentAttr.Values.AddRange(addItems);
                        }
                        
                    }
                }
                foreach (var sku in model.Skus)
                {
                    sku.Category = model.ShippingCategory;
                }
                var result = _productRepository.Add(model);
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);

                result = _unitOfWork.Commit();
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);

                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
            }
            catch (Exception e)
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }

        }

        public Models.Schema.Product GetById(string id)
        {
            return _productRepository.GetById(id);
        }

        public Sku GetByCode(string id , string skucode)
        {
            var item_product = _productRepository.GetById(id);
            var sku = item_product.Skus.Find(m => m.SkuCode == skucode);        
            return sku;
        }

        public ApiResponseModel Update(Models.Schema.Product model)
        {
            try
            {
                #region Validate

                var temp = _productRepository.GetById(model.Id);
                if (temp == null) return new ApiResponseModel("error", StaticMessage.DataNotFound);

                if (string.IsNullOrEmpty(model.Name)) return new ApiResponseModel("error", "Product name is required");
                if (string.IsNullOrEmpty(model.Url)) return new ApiResponseModel("error", "Product url is required");

                var oldproduct = _productRepository.GetSingle(m => m.Url == model.Url);
                if (oldproduct != null && oldproduct.Id != temp.Id) return new ApiResponseModel("error", "Product url is exist");

                if (model.MainCategory == null || string.IsNullOrEmpty(model.MainCategory.Id) || !model.Categories.Any()) return new ApiResponseModel("error", "Product category is required");

                if (model.Categories.Select(m => m.Id).Distinct().Count() < model.Categories.Count)
                {
                    return new ApiResponseModel("error", "Main category must be different from related category");
                }

                if (model.ProductType == null)
                {
                    model.ProductType = ProductTypeDefine.Simple;
                }

                //if (model.Brand != null && string.IsNullOrEmpty(model.Brand.Id))
                //    return new ApiResponseModel("error", "Brand is not valid");

                if (model.ShippingCategory != null && string.IsNullOrEmpty(model.ShippingCategory))
                    return new ApiResponseModel("error", "Shipping Category is required");

                if (!model.Skus.Any()) return new ApiResponseModel("error", "Sku info is required");

                List<string> allSkutributeId = new List<string>();
                foreach (var sku in model.Skus)
                {
                    if (string.IsNullOrEmpty(sku.SkuCode)) return new ApiResponseModel("error", "Sku code is required");
                    if (string.IsNullOrEmpty(sku.Name)) return new ApiResponseModel("error", "Sku name is required");
                    #region valid more option
                    //if (sku.Width <= 0) return new ApiResponseModel("error", "Width attribute must be greater than 0");
                    //if (sku.Height <= 0) return new ApiResponseModel("error", "Height attribute must be greater than 0");
                    //if (sku.Length <= 0) return new ApiResponseModel("error", "Length attribute must be greater than 0");
                    //if (sku.Depth <= 0) return new ApiResponseModel("error", "Depth attribute must be greater than 0");
                    //if (sku.Weight <= 0) return new ApiResponseModel("error", "Weight attribute must be greater than 0");
                    if (sku.SpecialPrice != 0)
                    {
                        if (sku.SpecialPrice > sku.Price) return new ApiResponseModel("error", "Special price cannot be bigger than original price");
                        if (sku.ToDate < sku.FromDate) return new ApiResponseModel("error", "The start date is not greater than the end date");
                    }
                    #endregion
                    if (sku.Attributes.Any(m => string.IsNullOrEmpty(m.Id) || string.IsNullOrEmpty(m.Name) || string.IsNullOrEmpty(m.Slug)))
                        return new ApiResponseModel("error", "Sku attribute is not valid");
                    if (sku.Attributes.Any(m => !m.Values.Any())) return new ApiResponseModel("error", "Sku attribute value is required");
                    if (sku.Attributes.Select(m => m.Id).Distinct().Count() < sku.Attributes.Count)
                        return new ApiResponseModel("error", "Sku attribute cannot be duplicated");

                    var prooldsku = _productRepository.GetSingle(m => m.Skus.Any(x => x.SkuCode == sku.SkuCode));
                    if (prooldsku != null && prooldsku.Id != temp.Id)
                        return new ApiResponseModel("error", "Sku code cannot be duplicated");

                    allSkutributeId.AddRange(sku.Attributes.Select(m => m.Id));
                    allSkutributeId = allSkutributeId.Distinct().ToList();
                    if (allSkutributeId.Any(m => sku.Attributes.All(x => x.Id != m))) return new ApiResponseModel("error", "Sku attribute is not valid");
                }
                if (model.Skus.Select(m => m.Name).Distinct().Count() < model.Skus.Count)
                    return new ApiResponseModel("error", "Sku attribute cannot be duplicated");

                if (model.Skus.Select(m => m.SkuCode).Distinct().Count() < model.Skus.Count)
                    return new ApiResponseModel("error", "Sku code cannot be duplicated");

                if (model.AttributeGroups.Select(m => m.Id).Distinct().Count() < model.AttributeGroups.Count)
                    return new ApiResponseModel("error", "Product attribute group cannot be duplicated");

                foreach (var attributeGroup in model.AttributeGroups)
                {
                    if (string.IsNullOrEmpty(attributeGroup.Id) || string.IsNullOrEmpty(attributeGroup.Name) || string.IsNullOrEmpty(attributeGroup.Slug) ||
                       !attributeGroup.Attributes.Any()) return new ApiResponseModel("error", "Product attribute group is not valid");

                    if (attributeGroup.Attributes.Select(m => m.Id).Distinct().Count() < attributeGroup.Attributes.Count)
                        return new ApiResponseModel("error", "Product attribute cannot be duplicated");

                    foreach (var attr in attributeGroup.Attributes)
                    {
                        if (string.IsNullOrEmpty(attr.Id) || string.IsNullOrEmpty(attr.Name) || string.IsNullOrEmpty(attr.Slug) ||
                            !attr.Values.Any()) return new ApiResponseModel("error", "Product attribute is not valid");
                    }
                }

                #endregion
                model.MainCategory.FilterList = new List<ProductAttributeGroup>();
                model.Categories = model.Categories.Select(m =>
                {
                    m.FilterList = new List<ProductAttributeGroup>();
                    return m;
                }).ToList();
                if (model.Skus.Any(m => m.Attributes.Any()))
                {
                    model.SkuAttributes = new List<ProductAttribute>();

                    foreach (var sku in model.Skus)
                    {
                        if (!model.SkuAttributes.Any())
                        {

                            model.SkuAttributes.AddRange(JsonConvert.DeserializeObject<List<ProductAttribute>>(JsonConvert.SerializeObject(model.Skus.FirstOrDefault().Attributes)));
                            continue;
                        }
                        foreach (var att in sku.Attributes)
                        {
                            var currentAttr = model.SkuAttributes.FirstOrDefault(m => m.Id == att.Id);
                            var addItems = JsonConvert.DeserializeObject<List<ProductAttributeValue>>(JsonConvert.SerializeObject(att.Values)).Where(m => currentAttr.Values.All(x => x.Slug != m.Slug));
                            currentAttr.Values.AddRange(addItems);
                        }
                    }
                }
                foreach (var sku in model.Skus)
                {
                    sku.Category = model.ShippingCategory;
                }
                var result = _productRepository.Update(temp.Id,model);
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);

                result = _unitOfWork.Commit();
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);

                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
            }
            catch (Exception e)
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }

        }


        public ApiResponseModel UpdateAll()
        {
            try
            {
                var product_all = _productRepository.Get();
                if(product_all == null || !product_all.Any()) return new ApiResponseModel("error", StaticMessage.DataNotFound);

                foreach (var product in product_all)
                {
                    if (product.ProductType == null) product.ProductType = ProductTypeDefine.Simple;
                    if (product.Skus.Count() > 25) {
                        product.ProductType = ProductTypeDefine.Collection;
                        var result = _productRepository.Update(product.Id, product);
                        if (result < 0)
                            return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);

                        result = _unitOfWork.Commit();
                        if (result < 0)
                            return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                    };     
                }
                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
            }
            catch (Exception e)
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }
        public ApiResponseModel UpdateSku(string skuId, string skucode, decimal? quantity)
        {
            try
            {
                #region Validate
                var temp = _productRepository.GetById(skuId);
                if (temp == null) return new ApiResponseModel("error", StaticMessage.DataNotFound);

                foreach (var sku in temp.Skus)
                {
                    if (sku.SkuCode == skucode && sku.IsInventory == true && sku.IsActive == true)
                    {
                        sku.StockQuantity = sku.StockQuantity - quantity??0;
                    }
                }
                #endregion
                var result = _productRepository.Update(temp.Id, temp);
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);

                result = _unitOfWork.Commit();
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);

                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }

        public ApiResponseModel UpdateSkuRestore(string skuId, string skucode, decimal? quantity)
        {
            try
            {
                #region Validate
                var temp = _productRepository.GetById(skuId);
                if (temp == null) return new ApiResponseModel("error", StaticMessage.DataNotFound);

                foreach (var sku in temp.Skus)
                {
                    if (sku.SkuCode == skucode && sku.IsInventory == true && sku.IsActive == true)
                    {
                        sku.StockQuantity = sku.StockQuantity + quantity ?? 0;
                    }
                }
                #endregion
                var result = _productRepository.Update(temp.Id, temp);
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);

                result = _unitOfWork.Commit();
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);

                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
            }
            catch 
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }

        public ApiResponseModel UpdateCustomize(Models.Schema.Product model, string name)
        {
            try
            {
                if (model == null || string.IsNullOrEmpty(name)) return new ApiResponseModel("error", StaticMessage.DataNotFound);

                var temp = _productRepository.GetById(model.Id);
                if (temp == null) return new ApiResponseModel("error", StaticMessage.DataNotFound);

                temp[name] = model[name];
                var result = _productRepository.Update(model.Id, temp);
                if (result < 0) return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);

                result = _unitOfWork.Commit();
                if (result < 0) return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }

        public ApiResponseModel ImportExcel(IFormFileCollection files)
        {
            try
            {


                //if (files.Any(m => m.Length > 2 * 1024 * 1024))
                //    return Json(new { uploaded = 0, error = "Excel file must be less than 2MB" });


                var excelFolder = Path.Combine(_hostingEnvironment.WebRootPath, $"tenants/{_tenant.TenantName.ToLower()}/upload/excel");

                if (!Directory.Exists(excelFolder))
                {
                    Directory.CreateDirectory(excelFolder);
                }


                var file = files.FirstOrDefault();


                var filenamewithoutext = Path.GetFileNameWithoutExtension(file.FileName).ToUrl();
                var extentionfile = Path.GetExtension(file.FileName).ToLower();
                var filename = $"{filenamewithoutext}{extentionfile}";


                var folderimg = Path.Combine(excelFolder, DateTime.Today.Year + "-" + DateTime.Today.Month);

                if (!Directory.Exists(folderimg))
                {
                    Directory.CreateDirectory(folderimg);
                }
                ISheet sheet;

                var path = Path.Combine(folderimg, filename);//duong dan luu file
                var data_list = new List<ExcelSku>();
                using (FileStream fs = File.Create(path))
                {
                    file.CopyTo(fs);
                    fs.Flush();
                    fs.Position = 0;
                    if (extentionfile == ".xls")
                    {
                        HSSFWorkbook hssfwb = new HSSFWorkbook(fs); //This will read the Excel 97-2000 formats  
                        sheet = hssfwb.GetSheetAt(0); //get first sheet from workbook  
                    }
                    else
                    {
                        XSSFWorkbook hssfwb = new XSSFWorkbook(fs); //This will read 2007 Excel format  
                        sheet = hssfwb.GetSheetAt(0); //get first sheet from workbook   
                    }
                    IRow headerRow = sheet.GetRow(0); //Get Header Row
                    int cellCount = headerRow.LastCellNum;
                    for (int i = (sheet.FirstRowNum + 1); i <= sheet.LastRowNum; i++) //Read Excel File
                    {
                        IRow row = sheet.GetRow(i);
                        if (row == null) continue;
                        if (row.Cells.All(d => d.CellType == CellType.Blank)) continue;
                        var data_row = new ExcelSku { };
                        for (int j = row.FirstCellNum; j < cellCount; j++)
                        {
                            if (row.GetCell(j) != null)
                            {
                                switch (j)
                                {
                                    case 0:
                                        data_row.SkuCode = row.GetCell(j).ToString().Trim();
                                        break;
                                    case 1:
                                        data_row.Name = row.GetCell(j).ToString().Trim();
                                        break;
                                    case 2:
                                        data_row.Price = decimal.Parse(row.GetCell(j).ToString().Trim());
                                        break;
                                    case 3:
                                        data_row.CategoryName = row.GetCell(j).ToString().Trim();
                                        break;
                                    case 4:
                                        data_row.Brand = row.GetCell(j).ToString().Trim();
                                        break;
                                    case 5:
                                        data_row.AttributeValue = row.GetCell(j).ToString().Trim();
                                        break;
                                    case 6:
                                        data_row.Quantity = decimal.Parse(row.GetCell(j).ToString().Trim());
                                        break;
                                    case 7:
                                        data_row.SaleTaxRate = decimal.Parse(row.GetCell(j).ToString().Trim());
                                        break;
                                }
                            }

                        }
                        data_list.Add(data_row);

                    }
                    var attrName = _productAttributeRepository.GetSingle(m => m.Slug == "color");
                    if (attrName == null)
                    {
                        var attrNameResult = _productAttributeRepository.Add(new ProductAttribute {
                            Name = "color",
                            Slug = "color",
                            Sort = 0 ,
                            Values = new List<ProductAttributeValue>()
                        });
                        if (attrNameResult < 0) return new ApiResponseModel("error", "Cannot Added attr name");
                        attrNameResult = _unitOfWork.Commit();
                        if (attrNameResult < 0) return new ApiResponseModel("error", "Cannot Added attr name");
                    }
                    
                    if (data_list.Count() > 0)
                    {                   
                        var result = data_list.GroupBy(m => m.Name, (key, g) => new { Name = key, Sku = g.ToList() });
                        var s = 0;
                        foreach (var product in result)
                        {
                            var productObject = new Models.Schema.Product { };
                            productObject.Name = product.Name;
                            productObject.Avatar = null;
                            productObject.ShortDescription = product.Name;
                            productObject.Description = product.Name;
                            productObject.MetaKeyword = product.Name.ToLower();
                            productObject.MetaDescription = product.Name.ToLower();
                            productObject.MetaTitle = product.Name.ToLower();
                            productObject.Url = product.Name.ToUrl().ToLower();
                            productObject.Sort = 0;
                            List<string> error_List = new List<string>();
                            var attributeValueList = new List<ProductAttributeValue>();
                            var brand = new Brand { };
                            var main_cate = new ProductCategory { };
                            var categories = new List<ProductCategory> { };
                            productObject.Skus = new List<Sku>();
                            var i = 0;
                            foreach (var sku in product.Sku.ToList())
                            {
                                if (string.IsNullOrEmpty(sku.SkuCode.ToString())) error_List.Add($"{sku.SkuCode}  null");                       
                                #region validate Atttr

                                string slugattr = sku.AttributeValue.ToUrl().ToLower().ToString();
                                var attr = _productAttributeValueRepository.GetSingle(m => m.Slug == slugattr);
                                var attrValueRes = new ProductAttributeValue { };
                                if (product.Sku.Count() > 1)
                                {
                                    if (string.IsNullOrEmpty(sku.AttributeValue.ToString())) error_List.Add($"{sku.SkuCode} attribute null");

                                    if (attr == null)
                                    {
                                        var model = new ProductAttributeValue
                                        {
                                            Slug = sku.AttributeValue.ToLower().ToUrl(),
                                            Value = sku.AttributeValue.ToString(),
                                            Sort = 0
                                        };
                                        var attrValueResult = _productAttributeValueRepository.Add(model);
                                        if (attrValueResult < 0) error_List.Add($"{model.Value} cannot added");
                                        attrValueResult = _unitOfWork.Commit();
                                        if (attrValueResult < 0) error_List.Add($"{model.Value} cannot added");
                                        attrValueRes = _productAttributeValueRepository.GetSingle(m => m.Slug == slugattr);

                                    }
                                }
                                var name = $"{product.Name} {(attr == null ? attrValueRes.Value : attr.Value)}";



                                #endregion

                                #region validate duplidate
                                //validate if sku code > 1;
                                if (product.Sku.Count() > 1)
                                {
                                    var sku_code_dup = productObject.Skus.Find(m => m.SkuCode == sku.SkuCode);
                                    if (sku_code_dup != null) error_List.Add($"{sku_code_dup.SkuCode} duplicate skucode in product");
                                    var sku_attr_dup = productObject.Skus.Find(m => m.Name == name);
                                    if (sku_attr_dup != null) error_List.Add($"{sku_attr_dup.SkuCode} duplicate attribute {sku.AttributeValue} in product");
                                }
                                #endregion
                                var skuObject = new Sku { };
                                skuObject.Name = (product.Sku.Count() > 1)? name : sku.Name;
                                skuObject.SkuCode = sku.SkuCode;
                                skuObject.Price = sku.Price;
                                skuObject.StockQuantity = sku.Quantity;
                                skuObject.FromDate = DateTime.Now;
                                skuObject.ToDate = DateTime.Now;
                                skuObject.WeightUnit = WeightUnit.Gram;
                                skuObject.SaleTaxRate = sku.SaleTaxRate;
                                skuObject.IsActive = true;
                                skuObject.ImageList = new List<ProductImage>();
                                skuObject.Sort = i;
                                skuObject.IsInventory = sku.Quantity > 0 ? true : false; 
                                skuObject.Attributes = product.Sku.Count() > 1 ? new List<ProductAttribute>(){
                                        new ProductAttribute
                                        {
                                            Id = attrName.Id,
                                            Name = attrName.Name,
                                            Slug = attrName.Slug,
                                            Sort = attrName.Sort,
                                            Values = new List<ProductAttributeValue>()
                                            {
                                                new ProductAttributeValue
                                                {
                                                    Value = attr == null ? attrValueRes.Value : attr.Value,
                                                    Slug = attr == null ? attrValueRes.Slug : attr.Slug,
                                                    Sort = attr == null ? attrValueRes.Sort : attr.Sort,
                                                    Id = attr == null ? attrValueRes.Id : attr.Id
                                                }
                                            }
                                        }
                                    } : new List<ProductAttribute>();
                                if (product.Sku.Count() > 1)
                                {
                                    attributeValueList.Add((attr == null) ? attrValueRes : attr);
                                }
                                productObject.Skus.Add(skuObject);
                                #region validate Categories
                                if (i == 0)
                                {
                                    string brandSlug = sku.Brand.ToLower().ToUrl();
                                    if (!string.IsNullOrEmpty(brandSlug))
                                    {
                                        brand = _brandRepository.GetSingle(m => m.Url == brandSlug);
                                        if (brand == null) error_List.Add($"{sku.Brand} brand not found");
                                    }
                                   


                                    var cateSplit = sku.CategoryName.Split("|");
                                    for (var j = 0; j < cateSplit.Count(); j++)
                                    {
                                        string cateSlug = cateSplit[j].ToLower().ToUrl();
                                        var related_cate = _productCategoryRepository.GetSingle(m => m.Url == cateSlug);
                                        if (related_cate == null)
                                        {
                                            error_List.Add($"{cateSplit[j]} category not found");
                                        }
                                        if (j == 0)
                                        {
                                            string cateMainSlug = cateSplit[0].ToLower().ToUrl();
                                            main_cate = (_productCategoryRepository.GetSingle(m => m.Url == cateMainSlug) == null) ? new ProductCategory { } : _productCategoryRepository.GetSingle(m => m.Url == cateMainSlug);
                                        }
                                        categories.Add(related_cate == null ? new ProductCategory { } : related_cate);
                                    }
                                }
                                #endregion
                                i++;

                            }
                            productObject.SkuAttributes = product.Sku.Count() > 1 ? new List<ProductAttribute>
                            {
                                new ProductAttribute
                                {
                                    Id = attrName.Id,
                                    Name = attrName.Name,
                                    Slug = attrName.Slug,
                                    Sort = attrName.Sort,
                                    Values = attributeValueList
                                }
                            } : new List<ProductAttribute>();
                            productObject.CreatedDate = DateTime.Now;
                            productObject.UpdatedDate = DateTime.Now;
                            productObject.IsActive = true;
                            productObject.Benefits = null;
                            productObject.ShippingCategory = null;
                            productObject.Ingredient = null;
                            productObject.AttributeGroups = new List<ProductAttributeGroup>();
                            productObject.Brand = brand;
                            productObject.ProductImages = new List<ProductImage>();
                            productObject.MainCategory = main_cate;
                            productObject.RelatedSkus = new List<Sku>();
                            productObject.Categories = categories;
                            productObject.Sort = s;
                            #region validate
             

                            foreach (var sku_c in productObject.Skus)
                            {
                                //valudate attribute chỗ này chớ
                                if (_productRepository.GetSingle(m => m.Skus.Any(x => x.SkuCode == sku_c.SkuCode)) != null) error_List.Add($"Sku {sku_c.SkuCode} duplicated");
                            }
                            #endregion
                            s++;
                            #region Add Product
                            if (error_List.Count() == 0)
                            {
                                var productResult = _productRepository.Add(productObject);
                                if (productResult < 0) return new ApiResponseModel("error", "Cannot Added Product");
                                productResult = _unitOfWork.Commit();
                                if (productResult < 0) return new ApiResponseModel("error", "Cannot Added Product");
                                continue;
                            }
                            else
                            {
                                string delimiter = ",";
                                _loggerRepository.Add(new Logger(LogLevel.Error, $"Import Excel {productObject.Name}", null, error_List.Aggregate((c, d) => c + delimiter + d)));
                                _unitOfWork.Commit();
                                continue;
                            }

                          
                            #endregion
                        }
                    }

                }

                return new ApiResponseModel("success", StaticMessage.DataLoadingSuccessfull);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }

        public Task Handle(ProductCategoryChangedDomainEvent notification, CancellationToken cancellationToken)
        {
            try
            {
                var proList = _productRepository.Get(m => m.Categories.Any(x=>x.Id == notification.Id));

                var cateTemp = new ProductCategory
                {
                    Id = notification.Id,
                    Avatar = notification.Avatar,
                    Description = notification.Description,
                    MetaDescription = notification.MetaDescription,
                    MetaKeyword = notification.MetaKeyword,
                    MetaTitle = notification.MetaTitle,
                    Sort = notification.Sort,
                    Url = notification.Url,
                    Name = notification.Name
            };
                foreach (var pro in proList)
                {
                    if (pro.MainCategory.Id == notification.Id)
                    {
                        pro.MainCategory = cateTemp;
                    }

                    var cat = pro.Categories.FirstOrDefault(m => m.Id == cateTemp.Id);
                    cat = cateTemp;
                    _productRepository.Update(pro.Id, pro);
                }
                return Task.CompletedTask;
            }
            catch
            {
                return Task.CompletedTask;
            }
        }

        public Task Handle(BrandChangedDomainEvent notification, CancellationToken cancellationToken)
        {
            try
            {
                var proList = _productRepository.Get(m => m.Brand != null && m.Brand.Id == notification.Id);

                var brandTemp = new Brand
                {
                    Id = notification.Id,
                    Avatar = notification.Avatar,
                    Description = notification.Description,
                    MetaDescription = notification.MetaDescription,
                    MetaKeyword = notification.MetaKeyword,
                    MetaTitle = notification.MetaTitle,
                    Sort = notification.Sort,
                    Url = notification.Url,
                    Name = notification.Name
                };
                foreach (var pro in proList)
                {
                    pro.Brand = brandTemp;
                    _productRepository.Update(pro.Id, pro);
                }
                return Task.CompletedTask;
            }
            catch
            {
                return Task.CompletedTask;
            }
        }

        public Task Handle(BrandRemovedDomainEvent notification, CancellationToken cancellationToken)
        {
            try
            {
                var proList = _productRepository.Get(m => m.Brand != null && m.Brand.Id == notification.Id);

                foreach (var pro in proList)
                {
                    pro.Brand = null;
                    _productRepository.Update(pro.Id, pro);
                }
                return Task.CompletedTask;
            }
            catch
            {
                return Task.CompletedTask;
            }
        }
    }
}
