﻿using System.Collections.Generic;
using NacGroup.Module.Core.Data;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Product.Data;
using NacGroup.Module.Product.Models.Schema;

namespace NacGroup.Module.Product.Services
{
    public interface IProductAttributeGroupService
    {
        ApiResponseModel Delete(List<string> idlst);
        ApiResponseModel Delete(string id);
        ProductAttributeGroup GetById(string id);
        ApiResponseModel Add(ProductAttributeGroup model);
        IEnumerable<ProductAttributeGroup> GetAll();
       
    }

    public class ProductAttributeGroupService : IProductAttributeGroupService
    {
        private readonly IProductAttributeGroupRepository _productAttributeGroupRepository;
        private readonly IUnitOfWork _unitOfWork;

        public ProductAttributeGroupService(IUnitOfWork unitOfWork, IProductAttributeGroupRepository productAttributeGroupRepository)
        {
            _unitOfWork = unitOfWork;
            _productAttributeGroupRepository = productAttributeGroupRepository;
        }
        public ApiResponseModel Delete(List<string> idlst)
        {
            if (idlst == null)
                return new ApiResponseModel("error", StaticMessage.SelectDataToRemove);
            foreach (var item in idlst)
            {
                Delete(item);
            }
            return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
        }

        public ApiResponseModel Delete(string id)
        {
            //TODO: Kiểm tra reference class (Product, Product Category)

            var resultcode = _productAttributeGroupRepository.Delete(id);
            if (resultcode < 0)
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            resultcode = _unitOfWork.Commit();
            if (resultcode < 0)
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
        }

        public ProductAttributeGroup GetById(string id)
        {
            return _productAttributeGroupRepository.GetById(id);
        }

        public ApiResponseModel Add(ProductAttributeGroup model)
        {
            #region Validate
            if (string.IsNullOrEmpty(model.Name)) return new ApiResponseModel("error", "Attribute group name is required");
            if (string.IsNullOrEmpty(model.Slug)) return new ApiResponseModel("error", "Slug is required");

            var oldcate = _productAttributeGroupRepository.GetSingle(m => m.Slug == model.Slug);
            if (oldcate != null)
                return new ApiResponseModel("error", "Attribute group is exist");
            #endregion

            try
            {
                var result = _productAttributeGroupRepository.Add(model);
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                result = _unitOfWork.Commit();
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }

        public IEnumerable<ProductAttributeGroup> GetAll()
        {
            return _productAttributeGroupRepository.Get();
        }
    }
}
