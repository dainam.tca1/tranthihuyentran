﻿using NacGroup.Module.Product.Data;
using NacGroup.Module.Core.Data;
using NacGroup.Module.Core.Models;
using System.Collections.Generic;
using NacGroup.Module.Product.Models;
using System.Linq;
using NacGroup.Module.Product.Models.Schema;

namespace NacGroup.Module.Product.Services
{

    public interface IProductWebService
    {

        IEnumerable<Models.Schema.Product> GetAll();
        Models.Schema.Product GetByUrl(string url);
        Sku GetSkuByCode(string code);
        Models.Schema.Product GetProductByCode(string code);
        Models.Schema.ProductCategory GetCategoryByUrl(string url);
        PagedResult<Models.Schema.Product> GetProductByCategoryUrl(string categoryurl, int page, int pagesize);
        IEnumerable<ProductCategory> GetCategories();
        PagedResult<Models.Schema.Product> Filter(string name, string categoryid, string skucode, List<string> brandIds, List<ProductAttributeFilterParameter> attributes, ProductFilterSortType sortType, int page, int pagesize, bool? isActive = null);
    }

    public class ProductWebService : IProductWebService
    {
        private readonly IProductRepository _productRepository;
        private readonly IProductCategoryRepository _productCategoryRepository;
        private readonly IUnitOfWork _unitOfWork;

        public ProductWebService(
            IProductRepository productRepository,
            IProductCategoryRepository productCategoryRepository,
            IUnitOfWork unitOfWork)
        {
            _productRepository = productRepository;
            _productCategoryRepository = productCategoryRepository;
            _unitOfWork = unitOfWork;
        }
        public IEnumerable<Models.Schema.Product> GetAll()
        {
            return _productRepository.Get().Where(m => m.IsActive);
        }
        public Models.Schema.Product GetByUrl(string url)
        {
            return _productRepository.GetSingle(m => m.Url == url && m.IsActive);
        }
        public Models.Schema.ProductCategory GetCategoryByUrl(string url)
        {
            return _productCategoryRepository.GetSingle(m => m.Url == url);
        }
        public PagedResult<Models.Schema.Product> GetProductByCategoryUrl(string categoryurl, int page, int pagesize)
        {
            return _productRepository.Get(page, pagesize, m => m.MainCategory.Url == categoryurl);
        }
        
        public PagedResult<Models.Schema.Product> Filter(string name, string categoryid, string skucode, List<string> brandIds, List<ProductAttributeFilterParameter> attributes, ProductFilterSortType sortType, int page, int pagesize, bool? isActive = null)
        {
            return _productRepository.Filter(name, categoryid, skucode, brandIds, attributes, sortType,page, pagesize, isActive);
        }
        public IEnumerable<ProductCategory> GetCategories()
        {
            return _productCategoryRepository.Get();
        }

        public Sku GetSkuByCode(string code)
        {
            return _productRepository.Get().FirstOrDefault(m => m.Skus.FirstOrDefault(s => s.SkuCode == code) != null && m.IsActive).Skus.Find( s => s.SkuCode == code);
        }
        public Models.Schema.Product GetProductByCode(string code)
        {
            return _productRepository.Get().FirstOrDefault(m => m.Skus.Find(s => s.SkuCode == code) != null && m.IsActive);
        }
    }
}
