﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using NacGroup.Module.Core.Data;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Core.Models.DomainEvents;
using NacGroup.Module.Product.Data;
using NacGroup.Module.Product.Models;
using NacGroup.Module.Product.Models.Schema;
using Newtonsoft.Json;

namespace NacGroup.Module.Product.Services
{
    public interface ICommentService
    {
        PagedResult<Comment> Filter(string productId, int page, int pagesize);
        PagedResult<Comment> FilterComment(int page, int pagesize);
        ApiResponseModel Add(Comment model);
        ApiResponseModel AddReply(Comment model);
        ApiResponseModel AddAdmin(Comment model);
        Comment GetById(string id);
        ApiResponseModel Update(Comment model);
        ApiResponseModel UpdateCustomize(Comment model, string name);
    }
    public class CommentService : ICommentService
    {
        private readonly ICommentRepository _commentRepository;
        private readonly IProductRepository _productRepository;
        private readonly IUnitOfWork _unitOfWork;

        public CommentService(
            ICommentRepository commentRepository,
            IProductRepository productRepository,
            IUnitOfWork unitOfWork)
        {
            _commentRepository = commentRepository;
            _productRepository = productRepository;
            _unitOfWork = unitOfWork;
        }

        public PagedResult<Comment> Filter(string productId, int page, int pagesize)
        {
            return _commentRepository.Filter(productId, page, pagesize);
        }

        public PagedResult<Comment> FilterComment(int page, int pagesize)
        {
            return _commentRepository.Filter(page, pagesize);
        }


        public ApiResponseModel Add(Comment model)
        {
            try
            {
                #region Validate
                if (model.Rating == 0 || model.Rating.Equals(null)) return new ApiResponseModel("error", "Vui lòng chọn đánh giá của bạn");
                if (string.IsNullOrEmpty(model.Body)) return new ApiResponseModel("error", "Mời bạn nhập đánh giá sản phẩm");
                if (model.Body.Length <= 80) return new ApiResponseModel("error", "Đánh giá sản phẩm tối thiểu 80 kí tự");
               
                if (string.IsNullOrEmpty(model.Name)) return new ApiResponseModel("error","Mời bạn nhập họ và tên");
                if (string.IsNullOrEmpty(model.Phone)) return new ApiResponseModel("error", "Mời bạn nhập số điện thoại");

                #endregion
                model.CommentStatus = CommentStatus.Spending;
                var result = _commentRepository.Add(model);
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);

                result = _unitOfWork.Commit();
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);

                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
            }
            catch (Exception e)
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }

        }
        public ApiResponseModel AddReply(Comment model)
        {
            try
            {
                #region Validate   
                if (string.IsNullOrEmpty(model.Body)) return new ApiResponseModel("error", "Vui lòng nhập bình luận");
                if (model.Body.Length <= 80) return new ApiResponseModel("error", "Bình luận nhập tối thiểu 80 kí tự");
                if (string.IsNullOrEmpty(model.Name)) return new ApiResponseModel("error", "Mời bạn nhập họ và tên");
                if (string.IsNullOrEmpty(model.Phone)) return new ApiResponseModel("error", "Mời bạn nhập số điện thoại");

                #endregion
                model.CommentStatus = CommentStatus.Spending;
                var result = _commentRepository.Add(model);
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);

                result = _unitOfWork.Commit();
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);

                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
            }
            catch (Exception e)
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }

        }

        public ApiResponseModel AddAdmin(Comment model)
        {
            try
            {
                #region Validate

                if (string.IsNullOrEmpty(model.Body)) return new ApiResponseModel("error", "Content Is Required");
                #endregion

                var result = _commentRepository.Add(model);
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);

                result = _unitOfWork.Commit();
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);

                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
            }
            catch (Exception e)
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }

        }

        public Comment GetById(string id)
        {
            return _commentRepository.GetById(id);
        }



        public ApiResponseModel Update(Comment model)
        {
            try
            {
                #region Validate
                

                #endregion
                var temp = _commentRepository.GetById(model.Id);
                if (temp == null) return new ApiResponseModel("error", StaticMessage.DataNotFound);
                model.ActiveStatus = !model.ActiveStatus;
                model.UpdatedDate = DateTime.Now;
                var result = _commentRepository.Update(temp.Id,model);

                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);

                result = _unitOfWork.Commit();
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);

                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }

        }

        public ApiResponseModel UpdateCustomize(Comment model, string name)
        {
            try
            {
                if (model == null || string.IsNullOrEmpty(name)) return new ApiResponseModel("error", StaticMessage.DataNotFound);

                var temp = _productRepository.GetById(model.Id);
                if (temp == null) return new ApiResponseModel("error", StaticMessage.DataNotFound);

                temp[name] = model[name];
                var result = _productRepository.Update(model.Id, temp);
                if (result < 0) return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);

                result = _unitOfWork.Commit();
                if (result < 0) return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }
       
    }
}
