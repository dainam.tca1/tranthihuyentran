﻿using System.Collections.Generic;
using NacGroup.Module.Core.Data;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Product.Data;
using NacGroup.Module.Product.Models.Schema;

namespace NacGroup.Module.Product.Services
{
    public interface IProductAttributeService
    {
        ApiResponseModel Delete(List<string> idlst);
        ApiResponseModel Delete(string id);
        ProductAttribute GetById(string id);
        ApiResponseModel Add(ProductAttribute model);
        IEnumerable<ProductAttribute> GetAll();
       
    }

    public class ProductAttributeService : IProductAttributeService
    {
        private readonly IProductAttributeRepository _productAttributeRepository;
        private readonly IUnitOfWork _unitOfWork;

        public ProductAttributeService(IUnitOfWork unitOfWork, IProductAttributeRepository productAttributeRepository)
        {
            _unitOfWork = unitOfWork;
            _productAttributeRepository = productAttributeRepository;
        }
        public ApiResponseModel Delete(List<string> idlst)
        {
           
            if (idlst == null)
                return new ApiResponseModel("error", StaticMessage.SelectDataToRemove);
            foreach (var item in idlst)
            {
                Delete(item);
            }
            return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
        }

        public ApiResponseModel Delete(string id)
        {
            //TODO: Kiểm tra reference class
            var resultcode = _productAttributeRepository.Delete(id);
            if (resultcode < 0)
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            resultcode = _unitOfWork.Commit();
            if (resultcode < 0)
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
        }

        public ProductAttribute GetById(string id)
        {
            return _productAttributeRepository.GetById(id);
        }

        public ApiResponseModel Add(ProductAttribute model)
        {
            #region Validate
            if (string.IsNullOrEmpty(model.Name)) return new ApiResponseModel("error", "Attribute name is required");
            if (string.IsNullOrEmpty(model.Slug)) return new ApiResponseModel("error", "Slug is required");

            var oldcate = _productAttributeRepository.GetSingle(m => m.Slug == model.Slug);
            if (oldcate != null)
                return new ApiResponseModel("error", "Attribute is exist");
            #endregion

            try
            {
                var result = _productAttributeRepository.Add(model);
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                result = _unitOfWork.Commit();
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }

        public IEnumerable<ProductAttribute> GetAll()
        {
            return _productAttributeRepository.Get();
        }
    }
}
