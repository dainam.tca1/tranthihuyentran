﻿using System;
using System.Collections.Generic;
using System.Linq;
using MediatR;
using NacGroup.Module.Core.Data;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Core.Models.DomainEvents;
using NacGroup.Module.Product.Data;
using NacGroup.Module.Product.Models;
using NacGroup.Module.Product.Models.Schema;

namespace NacGroup.Module.Product.Services
{
    public interface IProductCategoryService
    {
        List<ProductCategory> GetAll();
        ApiResponseModel Delete(List<string> idList);
        ApiResponseModel Delete(string id);
        ApiResponseModel Add(ProductCategory model);
        ProductCategory GetById(string id);
        ApiResponseModel Update(ProductCategory model);
        ApiResponseModel UpdateCustomize(ProductCategory model, string name);
    }
    public class ProductCategoryService : IProductCategoryService
    {
        private readonly IProductCategoryRepository _productCategoryRepository;
        private readonly IProductRepository _productRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMediator _mediator;

        public ProductCategoryService(IProductCategoryRepository productCategoryRepository, IUnitOfWork unitOfWork, IProductRepository productRepository, IMediator mediator)
        {
            _productCategoryRepository = productCategoryRepository;
            _unitOfWork = unitOfWork;
            _productRepository = productRepository;
            _mediator = mediator;
        }

        public List<ProductCategory> GetAll()
        {
            return _productCategoryRepository.Get().OrderBy(m=>m.Sort).ThenByDescending(m=>m.Id).ToList();
        }

        public ApiResponseModel Delete(List<string> idList)
        {
            if (idList == null)
                return new ApiResponseModel("error", StaticMessage.SelectDataToRemove);
            foreach (var item in idList)
            {
                var result = Delete(item);
                if (result.status == "error") return result;
            }
            return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
        }

        public ApiResponseModel Delete(string id)
        {
            var prolst = _productRepository.Filter(null, id, null, null, null, ProductFilterSortType.NewToOld, 1, 1);
            if (prolst.TotalItemCount > 0)
            {
                return new ApiResponseModel("error", "Please remove product in category before");
            }
            var resultcode = _productCategoryRepository.Delete(id);
            if (resultcode < 0)
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            resultcode = _unitOfWork.Commit();
            if (resultcode < 0)
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
        }

        public ApiResponseModel Add(ProductCategory model)
        {
            #region Validate
            if (string.IsNullOrEmpty(model.Name)) return new ApiResponseModel("error", "Category name is required");
            if (string.IsNullOrEmpty(model.Url)) return new ApiResponseModel("error", "Category url is required");
            var oldcate = _productCategoryRepository.GetSingle(m => m.Url == model.Url);
            if (oldcate != null)
                return new ApiResponseModel("error", "Url is exist");
            #endregion

            try
            {
                
                var result = _productCategoryRepository.Add(model);
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);

                result = _unitOfWork.Commit();
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);

                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }

        public ProductCategory GetById(string id)
        {
            return _productCategoryRepository.GetById(id);
        }

        public ApiResponseModel Update(ProductCategory model)
        {
            #region Validate
            if (string.IsNullOrEmpty(model.Name)) return new ApiResponseModel("error", "Category name is required");
            if (string.IsNullOrEmpty(model.Url)) return new ApiResponseModel("error", "Category url is required");

            var temp = _productCategoryRepository.GetById(model.Id);
            if (temp == null)
                return new ApiResponseModel("error", StaticMessage.DataNotFound);

            var oldcate = _productCategoryRepository.GetSingle(m => m.Url == model.Url);
            if (oldcate != null && temp.Id != oldcate.Id)
                return new ApiResponseModel("error", "Url is exist");

            #endregion

            try
            {
                temp.Avatar = model.Avatar;
                temp.MetaDescription = model.MetaDescription;
                temp.MetaKeyword = model.MetaKeyword;
                temp.MetaTitle = model.MetaTitle;
                temp.Sort = model.Sort;
                temp.Url = model.Url;
                temp.Description = model.Description;
                temp.Name = model.Name;
                temp.FilterList = model.FilterList;
                var result = _productCategoryRepository.Update(model.Id, temp);
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);

                #region Kiểm tra các table reference tới table này
                _mediator.Publish(new ProductCategoryChangedDomainEvent(temp.Id, temp.Name,temp.Description,temp.Avatar,temp.MetaKeyword,temp.MetaDescription,temp.MetaTitle,temp.Url,temp.Sort));
                #endregion

                result = _unitOfWork.Commit();
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);

                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }

        public ApiResponseModel UpdateCustomize(ProductCategory model, string name)
        {
            try
            {
                if (model == null || string.IsNullOrEmpty(name)) return new ApiResponseModel("error", StaticMessage.DataNotFound);

                var temp = _productCategoryRepository.GetById(model.Id);
                if (temp == null) return new ApiResponseModel("error", StaticMessage.DataNotFound);

                temp[name] = model[name];
                var result = _productCategoryRepository.Update(model.Id, temp);
                if (result < 0) return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);

                _mediator.Publish(new ProductCategoryChangedDomainEvent(temp.Id, temp.Name, temp.Description, temp.Avatar, temp.MetaKeyword, temp.MetaDescription, temp.MetaTitle, temp.Url, temp.Sort));

                result = _unitOfWork.Commit();
                if (result < 0) return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }
    }
}
