﻿using System.Collections.Generic;
using System.Linq;
using NacGroup.Module.Core.Data;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Product.Data;
using NacGroup.Module.Product.Models.Schema;

namespace NacGroup.Module.Product.Services
{
    public interface ILabelService
    {
        ApiResponseModel Delete(List<string> idlst);
        ApiResponseModel Delete(string id);
        ProductLabel GetById(string id);
        ApiResponseModel Add(ProductLabel model);
        List<ProductLabel> GetAll();

    }

    public class LabelService : ILabelService
    {
        private readonly ILabelRepository _labelRepository;
        private readonly IUnitOfWork _unitOfWork;

        public LabelService(IUnitOfWork unitOfWork, ILabelRepository labelRepository)
        {
            _unitOfWork = unitOfWork;
            _labelRepository = labelRepository;
        }
        public ApiResponseModel Delete(List<string> idlst)
        {

            if (idlst == null)
                return new ApiResponseModel("error", StaticMessage.SelectDataToRemove);
            foreach (var item in idlst)
            {
                Delete(item);
            }
            return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
        }

        public ApiResponseModel Delete(string id)
        {
            //TODO: Kiểm tra reference class
            var resultcode = _labelRepository.Delete(id);
            if (resultcode < 0)
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            resultcode = _unitOfWork.Commit();
            if (resultcode < 0)
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
        }

        public ProductLabel GetById(string id)
        {
            return _labelRepository.GetById(id);
        }

        public ApiResponseModel Add(ProductLabel model)
        {
            #region Validate
            if (string.IsNullOrEmpty(model.Name)) return new ApiResponseModel("error", "Attribute name is required");
            if (string.IsNullOrEmpty(model.Slug)) return new ApiResponseModel("error", "Slug is required");

            var oldcate = _labelRepository.GetSingle(m => m.Slug == model.Slug);
            if (oldcate != null)
                return new ApiResponseModel("error", "Attribute is exist");
            #endregion

            try
            {
                var result = _labelRepository.Add(model);
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                result = _unitOfWork.Commit();
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }

        public List<ProductLabel> GetAll()
        {
            return _labelRepository.Get().ToList();
        }
    }
}
