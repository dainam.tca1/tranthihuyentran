﻿using System;
using System.Collections.Generic;
using System.Linq;
using MediatR;
using NacGroup.Module.Core.Data;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Core.Models.DomainEvents;
using NacGroup.Module.Product.Data;
using NacGroup.Module.Product.Models.Schema;

namespace NacGroup.Module.Product.Services
{
    public interface IBrandService
    {
        List<Brand> GetAll();
        ApiResponseModel Delete(List<string> idList);
        ApiResponseModel Delete(string id);
        ApiResponseModel Add(Brand model);
        Brand GetById(string id);
        ApiResponseModel Update(Brand model);
        ApiResponseModel UpdateCustomize(Brand model, string name);
    }
    public class BrandService : IBrandService
    {
        private readonly IBrandRepository _brandRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMediator _mediator;

        public BrandService(IBrandRepository brandRepository, IUnitOfWork unitOfWork, IMediator mediator)
        {
            _brandRepository = brandRepository;
            _unitOfWork = unitOfWork;
            _mediator = mediator;
        }

        public List<Brand> GetAll()
        {
            return _brandRepository.Get().ToList();
        }

        public ApiResponseModel Delete(List<string> idList)
        {
            if (idList == null)
                return new ApiResponseModel("error", StaticMessage.SelectDataToRemove);
            foreach (var item in idList)
            {
                Delete(item);
            }
            return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
        }

        public ApiResponseModel Delete(string id)
        {
            var resultcode = _brandRepository.Delete(id);
            if (resultcode < 0)
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);

            _mediator.Publish(new BrandRemovedDomainEvent(id));
            resultcode = _unitOfWork.Commit();
            if (resultcode < 0)
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
        }

        public ApiResponseModel Add(Brand model)
        {
            #region Validate
            if (string.IsNullOrEmpty(model.Name)) return new ApiResponseModel("error", "Category name is required");
            if (string.IsNullOrEmpty(model.Url)) return new ApiResponseModel("error", "Category url is required");
            var oldcate = _brandRepository.GetSingle(m => m.Url == model.Url);
            if (oldcate != null)
                return new ApiResponseModel("error", "Url is exist");
            #endregion

            try
            {
                
                var result = _brandRepository.Add(model);
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);

                result = _unitOfWork.Commit();
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);

                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }

        public Brand GetById(string id)
        {
            return _brandRepository.GetById(id);
        }

        public ApiResponseModel Update(Brand model)
        {
            #region Validate
            if (string.IsNullOrEmpty(model.Name)) return new ApiResponseModel("error", "Category name is required");
            if (string.IsNullOrEmpty(model.Url)) return new ApiResponseModel("error", "Category url is required");

            var temp = _brandRepository.GetById(model.Id);
            if (temp == null)
                return new ApiResponseModel("error", StaticMessage.DataNotFound);

            var oldcate = _brandRepository.GetSingle(m => m.Url == model.Url);
            if (oldcate != null && temp.Id != oldcate.Id)
                return new ApiResponseModel("error", "Url is exist");

            #endregion

            try
            {
                temp.Avatar = model.Avatar;
                temp.MetaDescription = model.MetaDescription;
                temp.MetaKeyword = model.MetaKeyword;
                temp.MetaTitle = model.MetaTitle;
                temp.Sort = model.Sort;
                temp.Url = model.Url;
                temp.Description = model.Description;
                temp.Name = model.Name;

                var result = _brandRepository.Update(model.Id, temp);
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);

                #region Kiểm tra các table reference tới table này
                _mediator.Publish(new BrandChangedDomainEvent(temp.Id, temp.Name, temp.Description, temp.Avatar, temp.MetaKeyword, temp.MetaDescription, temp.MetaTitle, temp.Url, temp.Sort));
                #endregion

                result = _unitOfWork.Commit();
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);

                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }

        public ApiResponseModel UpdateCustomize(Brand model, string name)
        {
            try
            {
                if (model == null || string.IsNullOrEmpty(name)) return new ApiResponseModel("error", StaticMessage.DataNotFound);

                var temp = _brandRepository.GetById(model.Id);
                if (temp == null) return new ApiResponseModel("error", StaticMessage.DataNotFound);

                temp[name] = model[name];
                var result = _brandRepository.Update(model.Id, temp);
                if (result < 0) return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                _mediator.Publish(new BrandChangedDomainEvent(temp.Id, temp.Name, temp.Description, temp.Avatar, temp.MetaKeyword, temp.MetaDescription, temp.MetaTitle, temp.Url, temp.Sort));
                result = _unitOfWork.Commit();
                if (result < 0) return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }
    }
}
