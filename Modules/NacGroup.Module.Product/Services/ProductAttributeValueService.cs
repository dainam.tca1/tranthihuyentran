﻿using System.Collections.Generic;
using NacGroup.Module.Core.Data;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Product.Data;
using NacGroup.Module.Product.Models.Schema;

namespace NacGroup.Module.Product.Services
{
    public interface IProductAttributeValueService
    {
        ApiResponseModel Delete(List<string> idlst);
        ApiResponseModel Delete(string id);
        ProductAttributeValue GetById(string id);
        ApiResponseModel Add(ProductAttributeValue model);
        IEnumerable<ProductAttributeValue> GetAll();
       
    }

    public class ProductAttributeValueService : IProductAttributeValueService
    {
        private readonly IProductAttributeValueRepository _productAttributeValueRepository;
        private readonly IUnitOfWork _unitOfWork;

        public ProductAttributeValueService(IUnitOfWork unitOfWork, IProductAttributeValueRepository productAttributeValueRepository)
        {
            _unitOfWork = unitOfWork;
            _productAttributeValueRepository = productAttributeValueRepository;
        }
        public ApiResponseModel Delete(List<string> idlst)
        {
            if (idlst == null)
                return new ApiResponseModel("error", StaticMessage.SelectDataToRemove);
            foreach (var item in idlst)
            {
                Delete(item);
            }
            return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
        }

        public ApiResponseModel Delete(string id)
        {
            //TODO: Kiểm tra reference class
            var resultcode = _productAttributeValueRepository.Delete(id);
            if (resultcode < 0)
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            resultcode = _unitOfWork.Commit();
            if (resultcode < 0)
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
        }

        public ProductAttributeValue GetById(string id)
        {
            return _productAttributeValueRepository.GetById(id);
        }

        public ApiResponseModel Add(ProductAttributeValue model)
        {
            #region Validate
            if (string.IsNullOrEmpty(model.Value)) return new ApiResponseModel("error", "Attribute value is required");
            if (string.IsNullOrEmpty(model.Slug)) return new ApiResponseModel("error", "Slug is required");

            var oldcate = _productAttributeValueRepository.GetSingle(m => m.Slug == model.Slug);
            if (oldcate != null)
                return new ApiResponseModel("error", "Attribute value is exist");
            #endregion

            try
            {
                var result = _productAttributeValueRepository.Add(model);
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                result = _unitOfWork.Commit();
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }

        public IEnumerable<ProductAttributeValue> GetAll()
        {
            return _productAttributeValueRepository.Get();
        }
    }
}
