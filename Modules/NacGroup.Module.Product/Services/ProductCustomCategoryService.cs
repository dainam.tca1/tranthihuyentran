﻿using System;
using System.Collections.Generic;
using System.Linq;
using MediatR;
using NacGroup.Module.Core.Data;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Core.Models.DomainEvents;
using NacGroup.Module.Product.Data;
using NacGroup.Module.Product.Models;
using NacGroup.Module.Product.Models.Schema;

namespace NacGroup.Module.Product.Services
{
    public interface IProductCustomCategoryService
    {
        List<ProductCustomCategory> GetAll();
        ApiResponseModel Delete(List<string> idList);
        ApiResponseModel Delete(string id);
        ApiResponseModel Add(ProductCustomCategory model);
        ProductCustomCategory GetById(string id);
        ApiResponseModel Update(ProductCustomCategory model);
        ApiResponseModel UpdateCustomize(ProductCustomCategory model, string name);
    }
    public class ProductCustomCategoryService : IProductCustomCategoryService
    {
        private readonly IProductCustomCategoryRepository _productCustomCategoryRepository;
        private readonly IProductRepository _productRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMediator _mediator;

        public ProductCustomCategoryService(IProductCustomCategoryRepository productCustomCategoryRepository, IUnitOfWork unitOfWork, IProductRepository productRepository, IMediator mediator)
        {
            _productCustomCategoryRepository = productCustomCategoryRepository;
            _unitOfWork = unitOfWork;
            _productRepository = productRepository;
            _mediator = mediator;
        }

        public List<ProductCustomCategory> GetAll()
        {
            return _productCustomCategoryRepository.Get().ToList();
        }

        public ApiResponseModel Delete(List<string> idList)
        {
            if (idList == null)
                return new ApiResponseModel("error", StaticMessage.SelectDataToRemove);
            foreach (var item in idList)
            {
                var result = Delete(item);
                if (result.status == "error") return result;
            }
            return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
        }

        public ApiResponseModel Delete(string id)
        {
            var resultcode = _productCustomCategoryRepository.Delete(id);
            if (resultcode < 0)
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            resultcode = _unitOfWork.Commit();
            if (resultcode < 0)
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
        }

        public ApiResponseModel Add(ProductCustomCategory model)
        {
            #region Validate
            if (string.IsNullOrEmpty(model.Name)) return new ApiResponseModel("error", "Category name is required");
            if (string.IsNullOrEmpty(model.Url)) return new ApiResponseModel("error", "Category url is required");
            var oldcate = _productCustomCategoryRepository.GetSingle(m => m.Url == model.Url);
            if (oldcate != null)
                return new ApiResponseModel("error", "Url is exist");
            #endregion

            try
            {
                
                var result = _productCustomCategoryRepository.Add(model);
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);

                result = _unitOfWork.Commit();
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);

                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }

        public ProductCustomCategory GetById(string id)
        {
            return _productCustomCategoryRepository.GetById(id);
        }

        public ApiResponseModel Update(ProductCustomCategory model)
        {
            #region Validate
            if (string.IsNullOrEmpty(model.Name)) return new ApiResponseModel("error", "Category name is required");
            if (string.IsNullOrEmpty(model.Url)) return new ApiResponseModel("error", "Category url is required");

            var temp = _productCustomCategoryRepository.GetById(model.Id);
            if (temp == null)
                return new ApiResponseModel("error", StaticMessage.DataNotFound);

            var oldcate = _productCustomCategoryRepository.GetSingle(m => m.Url == model.Url);
            if (oldcate != null && temp.Id != oldcate.Id)
                return new ApiResponseModel("error", "Url is exist");

            #endregion

            try
            {
                temp.Avatar = model.Avatar;
                temp.MetaDescription = model.MetaDescription;
                temp.MetaKeyword = model.MetaKeyword;
                temp.MetaTitle = model.MetaTitle;
                temp.Url = model.Url;
                temp.Description = model.Description;
                temp.Name = model.Name;
                var result = _productCustomCategoryRepository.Update(model.Id, temp);
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);

                #region Kiểm tra các table reference tới table này
                //_mediator.Publish(new ProductCustomCategoryChangedDomainEvent(temp.Id, temp.Name,temp.Description,temp.Avatar,temp.MetaKeyword,temp.MetaDescription,temp.MetaTitle,temp.Url));
                #endregion

                result = _unitOfWork.Commit();
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);

                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }

        public ApiResponseModel UpdateCustomize(ProductCustomCategory model, string name)
        {
            try
            {
                if (model == null || string.IsNullOrEmpty(name)) return new ApiResponseModel("error", StaticMessage.DataNotFound);

                var temp = _productCustomCategoryRepository.GetById(model.Id);
                if (temp == null) return new ApiResponseModel("error", StaticMessage.DataNotFound);

                temp[name] = model[name];
                var result = _productCustomCategoryRepository.Update(model.Id, temp);
                if (result < 0) return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);

                //_mediator.Publish(new ProductCustomCategoryChangedDomainEvent(temp.Id, temp.Name, temp.Description, temp.Avatar, temp.MetaKeyword, temp.MetaDescription, temp.MetaTitle, temp.Url));

                result = _unitOfWork.Commit();
                if (result < 0) return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }
    }
}
