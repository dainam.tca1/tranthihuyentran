﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using NacGroup.Module.Core.Interfaces;
using NacGroup.Module.Product.Data;
using NacGroup.Module.Product.Services;


namespace NacGroup.Module.Product
{
    public class ModuleInitializer : IModuleInitializer
    {
        public void ConfigureServices(IServiceCollection services)
        {
          

            services.AddTransient<IBrandRepository, BrandRepository>();
            services.AddTransient<IVideoYoutubeRepository, VideoYoutubeRepository>();
            services.AddTransient<ICommentRepository, CommentRepository>();
            services.AddTransient<IProductAttributeGroupRepository, ProductAttributeGroupRepository>();
            services.AddTransient<IProductAttributeRepository, ProductAttributeRepository>();
            services.AddTransient<IProductAttributeValueRepository, ProductAttributeValueRepository>();
            services.AddTransient<IProductCategoryRepository, ProductCategoryRepository>();
            services.AddTransient<IProductRepository, ProductRepository>();
            services.AddTransient<ILabelRepository, LabelRepository>();


            services.AddTransient<IBrandService, BrandService>();
            services.AddTransient<IVideoYoutubeService, VideoYoutubeService>();
            services.AddTransient<ICommentService, CommentService>();
            services.AddTransient<IProductAttributeGroupService, ProductAttributeGroupService>();
            services.AddTransient<IProductAttributeService, ProductAttributeService>();
            services.AddTransient<IProductAttributeValueService, ProductAttributeValueService>();
            services.AddTransient<IProductCategoryService, ProductCategoryService>();
            services.AddTransient<IProductService, ProductService>();
            services.AddTransient<IProductWebService, ProductWebService>();
            services.AddTransient<ILabelService, LabelService>();


        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            
        }
    }
}
