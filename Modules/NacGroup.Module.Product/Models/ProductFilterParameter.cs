﻿using System.Collections.Generic;

namespace NacGroup.Module.Product.Models
{
    public class ProductAttributeFilterParameter
    {
        public string Slug { get; set; }
        public List<string> ValueSlugs { get; set; }
    }

    public enum ProductFilterSortType
    {
        NewToOld,
        OldToNew,
        AToZ,
        ZToA,
        PriceLowToHigh,
        PriceHighToLow
    }
}
