﻿using System;
using System.Collections.Generic;
using System.Linq;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using NacGroup.Module.Core.Models;

namespace NacGroup.Module.Product.Models.Schema
{
    [BsonIgnoreExtraElements]
    public class VideoYoutube : Entity, ITrackingUpdate
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("Title")]
        public string Title { get; set; }
        [BsonElement("Avatar")]
        public string Avatar { get; set; }

        [BsonElement("Description")]
        public string Description { get; set; }

        [BsonElement("MetaDescription")]
        public string MetaDescription { get; set; }

        [BsonElement("MetaKeyword")]
        public string MetaKeyword { get; set; }

        [BsonElement("Url")]
        public string Url { get; set; }

        [BsonElement("Link")]
        public string Link { get; set; }
  
        [BsonElement("MetaTitle")]
        public string MetaTitle { get; set; }

        [BsonElement("Sort")]
        public int Sort { get; set; }

        [BsonElement("IsActive")]
        public bool IsActive { get; set; }

        [BsonElement("CreatedDateDate")]
        public DateTime CreatedDate { get; set; }

        [BsonElement("UpdatedDate")]
        public DateTime UpdatedDate { get; set; }  

        public object this[string propertyName]
        {
            get => this.GetType().GetProperty(propertyName)?.GetValue(this, null);
            set => this.GetType().GetProperty(propertyName)?.SetValue(this, value, null);
        }
    }
}
