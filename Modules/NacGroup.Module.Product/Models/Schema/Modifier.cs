﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace NacGroup.Module.Product.Models.Schema
{
    [BsonIgnoreExtraElements]
    public class Modifier
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public int Id { get; set; }
        [BsonElement("Price")]
        public decimal Price { get; set; }
        [BsonElement("Name")]
        public string Name { get; set; }
    }
}
