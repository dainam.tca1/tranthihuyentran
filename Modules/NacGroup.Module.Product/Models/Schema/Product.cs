﻿using System;
using System.Collections.Generic;
using System.Linq;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using NacGroup.Module.Core.Models;
using NacGroup.Infrastructure.Shared;

namespace NacGroup.Module.Product.Models.Schema
{
    [BsonIgnoreExtraElements]
    public class Product : Entity, ITrackingUpdate
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("Name")]
        public string Name { get; set; }

        [BsonElement("Avatar")]
        public string Avatar { get; set; }

        [BsonElement("ShortDescription")]
        public string ShortDescription { get; set; }

        [BsonElement("Description")]
        public string Description { get; set; }

        [BsonElement("MetaKeyword")]
        public string MetaKeyword { get; set; }

        [BsonElement("MetaDescription")]
        public string MetaDescription { get; set; }

        [BsonElement("MetaTitle")]
        public string MetaTitle { get; set; }

        [BsonElement("Url")]
        public string Url { get; set; }

        [BsonElement("Sort")]
        public int Sort { get; set; }

        [BsonElement("Skus")]
        public List<Sku> Skus { get; set; }

        [BsonElement("AttributeGroups")]
        public List<ProductAttributeGroup> AttributeGroups { get; set; }

        [BsonElement("SkuAttributes")]
        public List<ProductAttribute> SkuAttributes { get; set; }
        [BsonElement("Brand")]
        public Brand Brand { get; set; }
        [BsonElement("Label")]
        public ProductLabel Label { get; set; }
        [BsonElement("Categories")]
        public List<ProductCategory> Categories { get; set; }
        [BsonElement("MainCategory")]
        public ProductCategory MainCategory { get; set; }
        [BsonElement("CreatedDate")]
        public DateTime CreatedDate { get; set; }
        [BsonElement("UpdatedDate")]
        public DateTime UpdatedDate { get; set; }
        [BsonElement("IsActive")]
        public bool IsActive { get; set; }
        [BsonElement("ProductImages")]
        public List<ProductImage> ProductImages { get; set; }
        [BsonElement("RelatedSkus")]
        public List<Sku> RelatedSkus { get; set; }
        [BsonElement("Benefits")]
        public string Benefits { get; set; }
        [BsonElement("Ingredient")]
        public string Ingredient { get; set; }

        [BsonElement("ProductType")]
        public ProductTypeDefine ProductType { get; set; }

        [BsonElement("ShippingCategory")]
        public string ShippingCategory { get; set; }

        public object this[string propertyName]
        {
            get => this.GetType().GetProperty(propertyName)?.GetValue(this, null);
            set => this.GetType().GetProperty(propertyName)?.SetValue(this, value, null);
        }

    }

    [BsonIgnoreExtraElements]
    public class Sku
    {

        public string SkuCode { get; set; }
        public string Name { get; set; }
        public string Category { get; set; }
        [BsonRepresentation(BsonType.Decimal128)]
        public decimal Price { get; set; }
        [BsonRepresentation(BsonType.Decimal128)]
        public decimal SpecialPrice { get; set; }
        [BsonRepresentation(BsonType.Decimal128)]
        public decimal StockQuantity { get; set; }
        [BsonRepresentation(BsonType.DateTime)]
        public DateTime FromDate{ get; set; }
        [BsonRepresentation(BsonType.DateTime)]
        public DateTime ToDate { get; set; }
        [BsonRepresentation(BsonType.Decimal128)]
        public decimal Width { get; set; }
        [BsonRepresentation(BsonType.Decimal128)]
        public decimal Height { get; set; }
        [BsonRepresentation(BsonType.Decimal128)]
        public decimal Depth { get; set; }
        public DimensionUnit DimensionUnit{ get; set; }
        [BsonRepresentation(BsonType.Decimal128)]
        public decimal Weight { get; set; }
        [BsonRepresentation(BsonType.Decimal128)]
        public decimal Length { get; set; }
        public WeightUnit WeightUnit { get; set; }
        [BsonRepresentation(BsonType.Decimal128)]
        public decimal SaleTaxRate { get; set; }
        public int Sort { get; set; }
        public bool IsInventory { get; set; }
        public List<ProductAttribute> Attributes { get; set; }
        public List<ProductImage> ImageList { get; set; }

        public bool IsActive { get; set; }
    }

    public class ProductImage
    {
        public string Id { get; set; }
        public string Src { get; set; }
        public string Alt { get; set; }
        public int Sort { get; set; }
    }

    
    public class ExcelSku
    {
        public string SkuCode { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public string CategoryName { get; set; }
        public string Brand { get; set; }
        public string AttributeValue { get; set; }
        public decimal Quantity { get; set; }
        public decimal SaleTaxRate { get; set; }
    }
}
