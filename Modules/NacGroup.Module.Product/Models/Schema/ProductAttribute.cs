﻿using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using NacGroup.Module.Core.Models;

namespace NacGroup.Module.Product.Models.Schema
{
    [BsonIgnoreExtraElements]
    public class ProductAttribute : Entity
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        [BsonElement("Name")]
        public string Name { get; set; }
        [BsonElement("Slug")]
        public string Slug { get; set; }
        [BsonElement("Sort")]
        public int Sort { get; set; }
        [BsonElement("Values")]
        public List<ProductAttributeValue> Values { get; set; }
    }
}
