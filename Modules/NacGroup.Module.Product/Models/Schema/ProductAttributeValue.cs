﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using NacGroup.Module.Core.Models;

namespace NacGroup.Module.Product.Models.Schema
{
    [BsonIgnoreExtraElements]
    public class ProductAttributeValue : Entity
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        [BsonElement("Value")]
        public string Value { get; set; }
        [BsonElement("Slug")]
        public string Slug { get; set; }
        [BsonElement("Sort")]
        public int Sort { get; set; }
    }
}
