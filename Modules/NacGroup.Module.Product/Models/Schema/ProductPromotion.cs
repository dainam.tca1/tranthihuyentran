﻿using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using NacGroup.Module.Core.Models;

namespace NacGroup.Module.Product.Models.Schema
{
    [BsonIgnoreExtraElements]
    public class ProductPromotion : Entity
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("ValueType")]
        public ProductPromotionType ValueType { get; set; }

        [BsonElement("Value")]
        public decimal Value { get; set; }

        [BsonElement("StartDate")]
        public DateTime StartDate { get; set; }

        [BsonElement("EndDate")]
        public DateTime EndDate { get; set; }
    }

    public enum ProductPromotionType
    {
        Percent,
        Amount
    }
}
