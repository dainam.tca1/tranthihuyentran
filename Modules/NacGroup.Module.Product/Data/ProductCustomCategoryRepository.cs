﻿using System.Collections.Generic;
using MongoDB.Driver;
using NacGroup.Module.Core.Data;
using NacGroup.Module.Core.Interfaces;
using NacGroup.Module.Product.Models.Schema;

namespace NacGroup.Module.Product.Data
{
    public interface IProductCustomCategoryRepository : IRepository<ProductCustomCategory>
    {
    }

    public class ProductCustomCategoryRepository : MongoRepository<ProductCustomCategory>, IProductCustomCategoryRepository, IMigrationRepository
    {
        public ProductCustomCategoryRepository(IMongoContext context) : base(context)
        {
   
        }
        public void Migration()
        {
            DbSet.Indexes.CreateOne(model: new CreateIndexModel<ProductCustomCategory>("{Url:1}", new CreateIndexOptions { Unique = true }));
        }

        public void Seed()
        {
            
        }

    }
}
