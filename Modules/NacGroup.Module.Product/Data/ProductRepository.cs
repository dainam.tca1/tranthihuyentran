﻿using System.Collections.Generic;
using System.Linq;
using MongoDB.Driver;
using NacGroup.Infrastructure.Shared;
using NacGroup.Module.Core.Data;
using NacGroup.Module.Core.Interfaces;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Product.Models;
using NacGroup.Module.Product.Models.Schema;

namespace NacGroup.Module.Product.Data
{
    public interface IProductRepository : IRepository<Models.Schema.Product>
    {
        PagedResult<Models.Schema.Product> Filter(string name, string categoryid, string skucode, List<string> brandIds, List<ProductAttributeFilterParameter> attributes, ProductFilterSortType sortType, int page, int pagesize, bool? isactive = null);

    }

    public class ProductRepository : MongoRepository<Models.Schema.Product>, IProductRepository, IMigrationRepository
    {
        public ProductRepository(IMongoContext context) : base(context)
        {

        }
        public void Migration()
        {
            DbSet.Indexes.CreateOne(model: new CreateIndexModel<Models.Schema.Product>("{Url:1}", new CreateIndexOptions { Unique = true }));
            DbSet.Indexes.CreateOne(model: new CreateIndexModel<Models.Schema.Product>("{\"Brand.Id\":1}"));
            DbSet.Indexes.CreateOne(model: new CreateIndexModel<Models.Schema.Product>("{\"Brand.Url\":1}"));
            DbSet.Indexes.CreateOne(model: new CreateIndexModel<Models.Schema.Product>("{\"Categories.Id\":1}"));
            DbSet.Indexes.CreateOne(model: new CreateIndexModel<Models.Schema.Product>("{\"Categories.Url\":1}"));
            DbSet.Indexes.CreateOne(model: new CreateIndexModel<Models.Schema.Product>("{\"SkuAttributes.Id\":1}"));
            DbSet.Indexes.CreateOne(model: new CreateIndexModel<Models.Schema.Product>("{\"SkuAttributes.Slug\":1}"));
            DbSet.Indexes.CreateOne(model: new CreateIndexModel<Models.Schema.Product>("{\"SkuAttributes.Values.Id\":1}"));
            DbSet.Indexes.CreateOne(model: new CreateIndexModel<Models.Schema.Product>("{\"SkuAttributes.Values.Slug\":1}"));
            DbSet.Indexes.CreateOne(model: new CreateIndexModel<Models.Schema.Product>("{\"AttributeGroups.Attributes.Id\":1}"));
            DbSet.Indexes.CreateOne(model: new CreateIndexModel<Models.Schema.Product>("{\"AttributeGroups.Attributes.Slug\":1}"));
            DbSet.Indexes.CreateOne(model: new CreateIndexModel<Models.Schema.Product>("{\"AttributeGroups.Attributes.Values.Id\":1}"));
            DbSet.Indexes.CreateOne(model: new CreateIndexModel<Models.Schema.Product>("{\"AttributeGroups.Attributes.Values.Slug\":1}"));
            DbSet.Indexes.CreateOne(model: new CreateIndexModel<Models.Schema.Product>("{\"Skus.SkuCode\":1}"));
            DbSet.Indexes.CreateOne(model: new CreateIndexModel<Models.Schema.Product>("{\"Skus.Price\":1}"));
        }

        public void Seed()
        {

        }

        public PagedResult<Models.Schema.Product> Filter(string name, string categoryid, string skucode, List<string> brandUrls, List<ProductAttributeFilterParameter> attributes, ProductFilterSortType sortType, int page,
            int pagesize, bool? isactive = null)
        {
            var filter = Builders<Models.Schema.Product>.Filter;
            var filterdefine = filter.Empty;
            if (!string.IsNullOrEmpty(name))
            {
                var nameurl = name.ToUrl();
                filterdefine &= filter.Where(m => m.Url.Contains(nameurl) || (m.Brand != null && m.Brand.Url.Contains(nameurl)) || (m.MainCategory != null && m.MainCategory.Url.Contains(nameurl)));
            }
            if (!string.IsNullOrEmpty(categoryid))
            {
                filterdefine &= filter.Where(m => m.Categories.Any(x=>x.Id == categoryid));
            }
            if (!string.IsNullOrEmpty(skucode))
            {
                filterdefine &= filter.Where(m => m.Skus.Any(x=>x.SkuCode == skucode));
            }

            if (brandUrls != null && brandUrls.Any())
            {
                filterdefine &= filter.Where(m => brandUrls.Contains(m.Url));
            }

            if (attributes != null && attributes.Any())
            {
                foreach (var attr in attributes)
                {
                    filterdefine &= filter.Where(m => m.AttributeGroups.Any(x => x.Attributes.Any(k=>k.Slug == attr.Slug && k.Values.Any(p=>attr.ValueSlugs.Contains(p.Slug)))));
                }
             
            }

            if (isactive != null)
            {
                filterdefine &= filter.Where(m => m.IsActive == isactive);
            }

            var all = DbSet.Find(filterdefine);
            switch (sortType)
            {
                case ProductFilterSortType.NewToOld:
                    all = all.SortBy(m => m.Sort).ThenByDescending(m => m.CreatedDate);
                    break;
                case ProductFilterSortType.OldToNew:
                    all = all.SortBy(m => m.Sort).ThenBy(m => m.CreatedDate);
                    break;
                case ProductFilterSortType.AToZ:
                    all = all.SortBy(m => m.Name).ThenBy(m=>m.Sort).ThenByDescending(m => m.CreatedDate);
                    break;
                case ProductFilterSortType.ZToA:
                    all = all.SortByDescending(m => m.Name).ThenBy(m => m.Sort).ThenByDescending(m => m.CreatedDate);
                    break;
                case ProductFilterSortType.PriceLowToHigh:
                    all = all.SortBy(m=>m.Skus[0].Price).ThenBy(m => m.Sort).ThenByDescending(m => m.CreatedDate);
                    break;
                case ProductFilterSortType.PriceHighToLow:
                    all = all.SortByDescending(m => m.Skus[0].Price).ThenBy(m => m.Sort).ThenByDescending(m => m.CreatedDate);
                    break;
            }
            var totalItem = DbSet.CountDocuments(filterdefine);
            all = all.Skip((page - 1) * pagesize).Limit(pagesize);
            return new PagedResult<Models.Schema.Product>(all.ToList(), page, pagesize, (int)totalItem);
        }
    }
}
