﻿using MongoDB.Driver;
using NacGroup.Module.Core.Data;
using NacGroup.Module.Core.Interfaces;
using NacGroup.Module.Product.Models.Schema;

namespace NacGroup.Module.Product.Data
{
    public interface IVideoYoutubeRepository : IRepository<VideoYoutube>
    {
    }

    public class VideoYoutubeRepository : MongoRepository<VideoYoutube>, IVideoYoutubeRepository, IMigrationRepository
    {
        public VideoYoutubeRepository(IMongoContext context) : base(context)
        {
   
        }
        public void Migration()
        {
            DbSet.Indexes.CreateOne(model: new CreateIndexModel<VideoYoutube>("{Url:1}", new CreateIndexOptions { Unique = true }));
        }

        public void Seed()
        {
         
        }

    }
}
