﻿using MongoDB.Driver;
using NacGroup.Module.Core.Data;
using NacGroup.Module.Core.Interfaces;
using NacGroup.Module.Product.Models.Schema;

namespace NacGroup.Module.Product.Data
{
    public interface ILabelRepository : IRepository<ProductLabel>
    {
    }

    public class LabelRepository : MongoRepository<ProductLabel>, ILabelRepository, IMigrationRepository
    {
        public LabelRepository(IMongoContext context) : base(context)
        {
   
        }
        public void Migration()
        {
            DbSet.Indexes.CreateOne(model: new CreateIndexModel<ProductLabel>("{Slug:1}", new CreateIndexOptions { Unique = true }));
        }

        public void Seed()
        {
            
        }

    }
}
