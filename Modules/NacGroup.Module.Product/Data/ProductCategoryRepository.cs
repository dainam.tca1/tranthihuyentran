﻿using System.Collections.Generic;
using MongoDB.Driver;
using NacGroup.Module.Core.Data;
using NacGroup.Module.Core.Interfaces;
using NacGroup.Module.Product.Models.Schema;

namespace NacGroup.Module.Product.Data
{
    public interface IProductCategoryRepository : IRepository<ProductCategory>
    {
    }

    public class ProductCategoryRepository : MongoRepository<ProductCategory>, IProductCategoryRepository, IMigrationRepository
    {
        public ProductCategoryRepository(IMongoContext context) : base(context)
        {
   
        }
        public void Migration()
        {
            DbSet.Indexes.CreateOne(model: new CreateIndexModel<ProductCategory>("{Url:1}", new CreateIndexOptions { Unique = true }));
        }

        public void Seed()
        {
            
        }

    }
}
