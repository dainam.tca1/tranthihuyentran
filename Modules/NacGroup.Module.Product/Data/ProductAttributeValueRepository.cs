﻿using MongoDB.Driver;
using NacGroup.Module.Core.Data;
using NacGroup.Module.Core.Interfaces;
using NacGroup.Module.Product.Models.Schema;

namespace NacGroup.Module.Product.Data
{
    public interface IProductAttributeValueRepository : IRepository<ProductAttributeValue>
    {
    }

    public class ProductAttributeValueRepository : MongoRepository<ProductAttributeValue>, IProductAttributeValueRepository, IMigrationRepository
    {
        public ProductAttributeValueRepository(IMongoContext context) : base(context)
        {
   
        }
        public void Migration()
        {
            DbSet.Indexes.CreateOne(model: new CreateIndexModel<ProductAttributeValue>("{Slug:1}", new CreateIndexOptions { Unique = true }));
        }

        public void Seed()
        {
           
        }

    }
}
