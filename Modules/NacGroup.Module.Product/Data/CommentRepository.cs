﻿using System.Linq;
using MongoDB.Driver;
using NacGroup.Module.Core.Data;
using NacGroup.Module.Core.Interfaces;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Product.Models.Schema;

namespace NacGroup.Module.Product.Data
{
    public interface ICommentRepository : IRepository<Comment>
    {
        PagedResult<Comment> Filter(string productId, int page, int pagesize);
        PagedResult<Comment> Filter(int page, int pagesize);
    }

    public class CommentRepository : MongoRepository<Comment>, ICommentRepository, IMigrationRepository
    {
        public CommentRepository(IMongoContext context) : base(context)
        {

        }
        public void Migration()
        {
         
        }

        public void Seed()
        {

        }

        public PagedResult<Comment> Filter(string productId, int page,int pagesize)
        {
            var filter = Builders<Comment>.Filter;
            var filterdefine = filter.Empty;
            if (!string.IsNullOrEmpty(productId))
            {
                
                filterdefine &= filter.Where(m => m.ProductId == productId);
            }

            var all = DbSet.Find(filterdefine);
            var totalItem = DbSet.CountDocuments(filterdefine);
            all = all.Skip((page - 1) * pagesize).Limit(pagesize).SortBy( m => m.CreatedDate);
            return new PagedResult<Comment>(all.ToList(), page, pagesize, (int)totalItem);
        }
        public PagedResult<Comment> Filter(int page, int pagesize)
        {
            var filter = Builders<Comment>.Filter;
            var filterdefine = filter.Empty;
            var all = DbSet.Find(filterdefine);
            var totalItem = DbSet.CountDocuments(filterdefine);
            all = all.Skip((page - 1) * pagesize).Limit(pagesize).SortByDescending(m => m.UpdatedDate);
            return new PagedResult<Comment>(all.ToList(), page, pagesize, (int)totalItem);
        }
    }
}
