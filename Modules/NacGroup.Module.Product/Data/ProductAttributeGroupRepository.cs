﻿using MongoDB.Driver;
using NacGroup.Module.Core.Data;
using NacGroup.Module.Core.Interfaces;
using NacGroup.Module.Product.Models.Schema;

namespace NacGroup.Module.Product.Data
{
    public interface IProductAttributeGroupRepository : IRepository<ProductAttributeGroup>
    {
    }

    public class ProductAttributeGroupRepository : MongoRepository<ProductAttributeGroup>, IProductAttributeGroupRepository, IMigrationRepository
    {
        public ProductAttributeGroupRepository(IMongoContext context) : base(context)
        {
   
        }
        public void Migration()
        {
            DbSet.Indexes.CreateOne(model: new CreateIndexModel<ProductAttributeGroup>("{Slug:1}", new CreateIndexOptions { Unique = true }));
        }

        public void Seed()
        {

        }

    }
}
