﻿using MongoDB.Driver;
using NacGroup.Module.Core.Data;
using NacGroup.Module.Core.Interfaces;
using NacGroup.Module.Product.Models.Schema;

namespace NacGroup.Module.Product.Data
{
    public interface IProductAttributeRepository : IRepository<ProductAttribute>
    {
    }

    public class ProductAttributeRepository : MongoRepository<ProductAttribute>, IProductAttributeRepository, IMigrationRepository
    {
        public ProductAttributeRepository(IMongoContext context) : base(context)
        {
   
        }
        public void Migration()
        {
            DbSet.Indexes.CreateOne(model: new CreateIndexModel<ProductAttribute>("{Slug:1}", new CreateIndexOptions { Unique = true }));
        }

        public void Seed()
        {
            
        }

    }
}
