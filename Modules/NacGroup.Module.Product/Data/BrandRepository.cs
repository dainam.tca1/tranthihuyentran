﻿using MongoDB.Driver;
using NacGroup.Module.Core.Data;
using NacGroup.Module.Core.Interfaces;
using NacGroup.Module.Product.Models.Schema;

namespace NacGroup.Module.Product.Data
{
    public interface IBrandRepository : IRepository<Brand>
    {
    }

    public class BrandRepository : MongoRepository<Brand>, IBrandRepository, IMigrationRepository
    {
        public BrandRepository(IMongoContext context) : base(context)
        {
   
        }
        public void Migration()
        {
            DbSet.Indexes.CreateOne(model: new CreateIndexModel<Brand>("{Url:1}", new CreateIndexOptions { Unique = true }));
        }

        public void Seed()
        {
         
        }

    }
}
