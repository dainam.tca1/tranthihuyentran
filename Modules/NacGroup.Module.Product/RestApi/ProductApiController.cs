﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Product.Models.Schema;
using NacGroup.Module.Product.Services;
using Newtonsoft.Json.Linq;
using System.Linq;


namespace NacGroup.Module.Product.RestApi
{
    [Route("api/products")]
    [ApiController]
    public class ProductApiController : ControllerBase
    {
        private readonly IProductService _productService;
        private readonly IBrandService _brandService;


        public ProductApiController(
            IProductService productService,
            IBrandService brandService
            )
        {
            _brandService = brandService;
            _productService = productService;
        }



        #region Api Product
        [HttpGet]
        public ActionResult<Sku> GetByCode(string skuId, string skucode)
        {
            return _productService.GetByCode(skuId, skucode);
        }

        [HttpGet("list")]
        public ActionResult<List<Models.Schema.Product>> Get()
        {
            return _productService.Get();
        }

        [HttpPost("update")]
        public ActionResult<ApiResponseModel> UpdateStockProduct([FromBody] JObject model)
        {
            if (model == null || model?["productId"] == null || model?["skuCode"] == null || model?["quantity"] == null) return new ApiResponseModel("error", StaticMessage.ParameterInvalid);
            var productId = model?["productId"].Value<string>();
            var skuCode = model?["skuCode"].Value<string>();
            var quantity = model?["quantity"].Value<decimal>();
            return _productService.UpdateSku(productId, skuCode, quantity);
        }

        [HttpPost("add")]
        public ActionResult<ApiResponseModel> AddProduct([FromBody] Models.Schema.Product model)
        {
            if (model == null) return new ApiResponseModel("error", StaticMessage.ParameterInvalid);
            return _productService.Add(model);
        }

        [HttpPost("restore")]
        public ActionResult<ApiResponseModel> UpdateRestoreQuantityStockProduct([FromBody] JObject model)
        {
            if (model == null || model?["productId"] == null || model?["skuCode"] == null || model?["quantity"] == null) return new ApiResponseModel("error", StaticMessage.ParameterInvalid);
            var productId = model?["productId"].Value<string>();
            var skuCode = model?["skuCode"].Value<string>();
            var quantity = model?["quantity"].Value<decimal>();
            return _productService.UpdateSkuRestore(productId, skuCode, quantity);
        }

        #endregion

        #region Brand

        [HttpGet("brand")]
        public ActionResult<List<Brand>> GetBrand()
        {
            return _brandService.GetAll();
        }

        [HttpPost("brand/add")]
        public ActionResult<ApiResponseModel> AddBrand([FromBody] Brand model)
        {
            if (model == null) return new ApiResponseModel("error", StaticMessage.ParameterInvalid);
            return _brandService.Add(model);
        }

        #endregion

        //[HttpGet("{id}")]
        //public ActionResult<GPosProduct> GetById(string id)
        //{
        //    return _productSChemaService.G(_gPosService, categoryid);
        //}

    }
}