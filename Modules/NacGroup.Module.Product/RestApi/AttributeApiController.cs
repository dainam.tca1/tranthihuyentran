﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Product.Models.Schema;
using NacGroup.Module.Product.Services;
using Newtonsoft.Json.Linq;
using System.Linq;


namespace NacGroup.Module.Product.RestApi
{
    [Route("api/attribute")]
    [ApiController]
    public class AttributeApiController : ControllerBase
    {
 
        private readonly IProductAttributeValueService _productAttributeValueService;
        private readonly IProductAttributeService _productAttributeService;

        public AttributeApiController(
            IProductAttributeValueService productAttributeValueService,
            IProductAttributeService productAttributeService

            )
        {
            _productAttributeValueService = productAttributeValueService;
            _productAttributeService = productAttributeService;
        }

        #region Product Attribute Value

        [HttpGet("value/list")]
        public ActionResult<List<ProductAttributeValue>> GetAttributeValue()
        {
            return _productAttributeValueService.GetAll().ToList();
        }

        [HttpGet("value")]
        public ActionResult<ProductAttributeValue> GetAttributeValueBySlug(string slug)
        {
            if (slug == null) return null;
            return _productAttributeValueService.GetAll().FirstOrDefault(m => m.Slug == slug);
        }

        [HttpPost("value")]
        public ActionResult<ApiResponseModel> AddAttributeValue([FromBody] ProductAttributeValue model)
        {
            if (model == null) return new ApiResponseModel("error", StaticMessage.ParameterInvalid);
            return _productAttributeValueService.Add(model);
        }

        [HttpGet("name")]
        public ActionResult<List<ProductAttribute>> GetAttributeName()
        {
            return _productAttributeService.GetAll().ToList();
        }

        #endregion



    }
}