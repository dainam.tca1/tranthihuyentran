﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Product.Models.Schema;
using NacGroup.Module.Product.Services;
using Newtonsoft.Json.Linq;
using System.Linq;


namespace NacGroup.Module.Product.RestApi
{
    [Route("api/productcategory")]
    [ApiController]
    public class ProductCategoryApiController : ControllerBase
    {
        private readonly IProductCategoryService _productCategoryService;


        public ProductCategoryApiController(
            IProductCategoryService productCategoryService
            )
        {
            _productCategoryService = productCategoryService;
        }



        #region Api Product Category
        [HttpGet]
        public ActionResult<List<ProductCategory>> Get()
        {
            return _productCategoryService.GetAll();
        }
        #endregion

 

    }
}