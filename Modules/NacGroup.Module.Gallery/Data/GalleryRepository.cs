﻿using MongoDB.Driver;
using NacGroup.Infrastructure.Shared;
using NacGroup.Module.Core.Data;
using NacGroup.Module.Core.Interfaces;
using NacGroup.Module.Core.Models;

namespace NacGroup.Module.Gallery.Data
{
    public interface IGalleryRepository : IRepository<Models.Schema.Gallery>
    {
        Models.Schema.Gallery GetByUrl(string url);

        PagedResult<Models.Schema.Gallery> Filter(string title, int page, int pagesize);


    }

    public class GalleryRepository : MongoRepository<Models.Schema.Gallery>, IGalleryRepository, IMigrationRepository
    {
        public GalleryRepository(IMongoContext context) : base(context)
        {
   
        }

        public void Migration()
        {
            DbSet.Indexes.CreateOne(model: new CreateIndexModel<Models.Schema.Gallery>("{Url:1}", new CreateIndexOptions { Unique = true }));
        }

        public void Seed()
        {
            
        }

        public PagedResult<Models.Schema.Gallery> Filter(string title, int page, int pagesize)
        {
            var filter = Builders<Models.Schema.Gallery>.Filter;
            var filterdefine = filter.Empty;
            if (!string.IsNullOrEmpty(title))
            {
                var titurl = title.ToUrl();
                filterdefine = filterdefine & filter.Where(m => m.Url.Contains(titurl));
            }
            var totalItem = DbSet.CountDocuments(filterdefine);
            var all = DbSet.Find(filterdefine).SortBy(m => m.Sort).Skip((page - 1) * pagesize).Limit(pagesize);
            return new PagedResult<Models.Schema.Gallery>(all.ToList(), page, pagesize, (int)totalItem);
        }

        public Models.Schema.Gallery GetByUrl(string url)
        {
            var all = DbSet.Find(m => m.Url == url);
            return all.FirstOrDefault();
        }

       
    }
}
