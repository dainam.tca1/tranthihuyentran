﻿using System.Collections.Generic;
using System.Linq;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Gallery.Data;
using NacGroup.Module.Gallery.Models.Schema;

namespace NacGroup.Module.Gallery.Services
{
    public interface IGalleryWebService
    {
        IEnumerable<Models.Schema.Gallery> GetAll();
        PagedResult<GalleryImageModel> FilterGallery(string id, int page, int pagesize);

    }

    public class GalleryWebService : IGalleryWebService
    {
        private readonly IGalleryRepository _galleryRepository;

        public GalleryWebService(IGalleryRepository galleryRepository)
        {
            _galleryRepository = galleryRepository;
        }
        public PagedResult<GalleryImageModel> FilterGallery(string id, int page, int pagesize)
        {
            switch (id)
            {
                case null:
                case "all":
                    var result = _galleryRepository.Get();
                    List<GalleryImageModel> listAlbum = new List<GalleryImageModel>();
                    var Default = result.Where(m => m.IsDefault == true).ToList();
                    var notDefault = result.Where(m => m.IsDefault == false).ToList();

                    foreach (var item in Default)
                    {
                        foreach (var item_child in item.ListAlbum)
                        {
                            listAlbum.Add(item_child);
                        }
                    }

                    foreach (var item in notDefault)
                    {
                        foreach (var item_child in item.ListAlbum)
                        {
                            listAlbum.Add(item_child);
                        }
                    }

                    var count = listAlbum.Count();
                    var album = listAlbum.Skip((page - 1) * pagesize).Take(pagesize).ToList();


                    var result1 = new PagedResult<GalleryImageModel>(album, page, pagesize, count);
                    return result1;
                default:
                    var gallery = _galleryRepository.GetById(id);
                    var listAlbum1 = gallery.ListAlbum;
                    var count1 = listAlbum1.Count();
                    var album1 = listAlbum1.Skip((page - 1) * pagesize).Take(pagesize).ToList();
                    var result2 = new PagedResult<GalleryImageModel>(album1,page,pagesize,count1);
                    return result2;
            }
            
        }

        public IEnumerable<Models.Schema.Gallery> GetAll()
        {
            return _galleryRepository.Get().Where(m => m.IsDefault != true);
        }
    }
}
