﻿using System;
using System.Collections.Generic;
using NacGroup.Module.Core.Data;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Gallery.Data;

namespace NacGroup.Module.Gallery.Services
{
    public interface IGalleryService
    {
        PagedResult<Models.Schema.Gallery> Get(int page, int pagesize);
        ApiResponseModel Delete(List<string> idlst);
        ApiResponseModel Delete(string id);
        Models.Schema.Gallery GetById(string id);
        ApiResponseModel Add(Models.Schema.Gallery model);
        ApiResponseModel UpdateCustomize(Models.Schema.Gallery model , string name);
        PagedResult<Models.Schema.Gallery> Filter(string name, int page, int pagesize);
        IEnumerable<Models.Schema.Gallery> GetAll();
        ApiResponseModel Update(Models.Schema.Gallery model);
    }

    public class GalleryService : IGalleryService
    {
        private readonly IGalleryRepository _galleryRepository;
        private readonly IUnitOfWork _unitOfWork;

        public GalleryService(IGalleryRepository galleryRepository, IUnitOfWork unitOfWork)
        {
            _galleryRepository = galleryRepository;
            _unitOfWork = unitOfWork;
        }

        public PagedResult<Models.Schema.Gallery> Get(int page, int pagesize) =>
        _galleryRepository.Get(page, pagesize);

        public ApiResponseModel Delete(List<string> idlst)
        {
            if (idlst == null)
                return new ApiResponseModel("error", StaticMessage.SelectDataToRemove);
            foreach (var item in idlst)
            {
                Delete(item);
            }
            return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
        }

        public ApiResponseModel Delete(string id)
        {
            var resultcode = _galleryRepository.Delete(id);
            if (resultcode < 0)
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            resultcode = _unitOfWork.Commit();
            if (resultcode < 0)
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
        }

        public Models.Schema.Gallery GetById(string id)
        {
            return _galleryRepository.GetById(id);
        }
        public ApiResponseModel Add(Models.Schema.Gallery model)
        {
            if (string.IsNullOrEmpty(model.Title)) return new ApiResponseModel("error", "Title is required");
            if (string.IsNullOrEmpty(model.Url)) return new ApiResponseModel("error", "Url is required");

            var oldcate = _galleryRepository.GetByUrl(model.Url);
            if (oldcate != null)
                return new ApiResponseModel("error", "Url is exist");
            try
            {
                var result = _galleryRepository.Add(model);
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                result = _unitOfWork.Commit();
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }

        public PagedResult<Models.Schema.Gallery> Filter(string name, int page, int pagesize)
        {
            return _galleryRepository.Filter(name, page, pagesize);
        }

        public ApiResponseModel Update(Models.Schema.Gallery model)
        {
            if (string.IsNullOrEmpty(model.Title)) return new ApiResponseModel("error", "Title is required");
            if (string.IsNullOrEmpty(model.Url)) return new ApiResponseModel("error", "Url is required");

            var temp = _galleryRepository.GetById(model.Id);
            if (temp == null)
                return new ApiResponseModel("error", StaticMessage.DataNotFound);

            var oldcate = _galleryRepository.GetByUrl(model.Url);
            if (oldcate != null && temp.Id != oldcate.Id)
                return new ApiResponseModel("error", "Url is exist");
            try
            {
                temp.Avatar = model.Avatar;
                temp.MetaDescription = model.MetaDescription;
                temp.MetaKeyword = model.MetaKeyword;
                temp.MetaTitle = model.MetaTitle;
                temp.Title = model.Title;
                temp.ShortDescription = model.ShortDescription;
                temp.ListAlbum = model.ListAlbum;
                temp.PageContent = model.PageContent;
                temp.Url = model.Url;
                temp.IsDefault = model.IsDefault;
                temp.Sort = model.Sort;
                temp.UpdatedDate = DateTime.Now;
                var result = _galleryRepository.Update(model.Id, temp);
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                result = _unitOfWork.Commit();
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }

        public IEnumerable<Models.Schema.Gallery> GetAll()
        {
            return _galleryRepository.Get();
        }

        public ApiResponseModel UpdateCustomize(Models.Schema.Gallery model, string name)
        {
            #region validate

            if (model == null || string.IsNullOrEmpty(name))
                return new ApiResponseModel("error", StaticMessage.DataNotFound);
            #endregion

            try
            {
                var temp = _galleryRepository.GetById(model.Id);
                if (temp == null)  return new ApiResponseModel("error", StaticMessage.DataNotFound);
                temp[name] = model[name];
                var result = _galleryRepository.Update(model.Id,temp);
                if(result < 0 ) return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                result = _unitOfWork.Commit();
                if(result < 0) return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);


            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }
        //public ApiResponseModel CreateStaticPage(Models.StaticPage model)
        //{
        //    #region Validate
        //    try
        //    {
        //        var oldmodel = _staticpages.Find(m => m.Url == model.Url).FirstOrDefault();
        //        if (oldmodel != null)
        //        {
        //            return new ApiResponseModel("error", "Url is exist");
        //        }
        //        _staticpages.InsertOne(model);
        //        return new ApiResponseModel("success", "Update data successfully");
        //    }
        //    catch (System.Exception e)
        //    {
        //        return new ApiResponseModel("error", "System Error");
        //    }

        //    #endregion
        //}

    }
}
