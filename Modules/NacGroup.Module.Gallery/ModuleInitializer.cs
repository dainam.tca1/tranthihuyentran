﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using NacGroup.Module.Core.Interfaces;
using NacGroup.Module.Gallery.Data;
using NacGroup.Module.Gallery.Services;

namespace NacGroup.Module.Gallery
{
    public class ModuleInitializer : IModuleInitializer
    {
        public void ConfigureServices(IServiceCollection services)
        {
            #region Gallery
            services.AddTransient<IGalleryRepository, GalleryRepository>();
            services.AddTransient<IGalleryService, GalleryService>();
            services.AddTransient<IGalleryWebService, GalleryWebService>();
            #endregion

        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
        }
    }
}
