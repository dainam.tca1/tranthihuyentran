﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NacGroup.Module.Core.Interfaces;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Gallery.Services;
using Newtonsoft.Json.Linq;

namespace NacGroup.Module.Gallery.Controllers
{
    [Authorize(Roles = "Admin", Policy = "TenantAuthorize")]
    [Route("cms/api/gallery/{action=Index}")]
    public class GalleryCmsApiController : Controller
    {
        private readonly IGalleryService _galleryService;

        public GalleryCmsApiController(IGalleryService galleryService)
        {
            _galleryService = galleryService;
        }

        [HttpGet]
        public ActionResult<ApiResponseModel> Index(string name, int page = 1, int pagesize = 10)
        {
            return new ApiResponseModel("success", _galleryService.Filter(name, page, pagesize));
        }

        [HttpPost]
        public ActionResult<ApiResponseModel> RemoveList([FromBody] List<string> idList)
        {
            return _galleryService.Delete(idList);
        }

        [HttpPost]
        public ActionResult<ApiResponseModel> Add([FromBody] Models.Schema.Gallery model)
        {
            return _galleryService.Add(model);
        }

        public ActionResult<ApiResponseModel> GetUpdate(string id)
        {
            var item = _galleryService.GetById(id);
            if (item == null)
                return new ApiResponseModel("error", StaticMessage.DataNotFound);

            return new ApiResponseModel("success", item);
        }

        [HttpPost]
        public ActionResult<ApiResponseModel> Update([FromBody] Models.Schema.Gallery model)
        {
            return _galleryService.Update(model);
        }

        [HttpPost]
        public ActionResult<ApiResponseModel> UpdateCustomize([FromBody] JObject data)
        {
            var model = data["model"].ToObject<Models.Schema.Gallery>();
            var name = data["name"].Value<string>();
            return _galleryService.UpdateCustomize(model,name);
        }
    }
}
