﻿using Microsoft.AspNetCore.Mvc;
using NacGroup.Module.Gallery.Services;

namespace NacGroup.Module.Gallery.Controllers
{
    [Route("gallery/{action=Index}")]
    public class GalleryController : Controller
    {
        private readonly IGalleryWebService _galleryWebService;

        public GalleryController(IGalleryWebService galleryWebService)
        {
            _galleryWebService = galleryWebService;
        }

        public IActionResult Index()
        {
            var result = _galleryWebService.FilterGallery("all", 1, 20);
            if (result == null)
            {
                return NotFound();
            }

            ViewData["AllCat"] = _galleryWebService.GetAll();
            return View(result);
        }

        public IActionResult GetAlbum(string id, int pages)
        {
            var result = _galleryWebService.FilterGallery(id, pages, 20);
            if (result == null)
            {
                return NotFound();
            }

            ViewData["Id"] = id;
            return View("_FilterGallery", result);
        }
      
    }
}