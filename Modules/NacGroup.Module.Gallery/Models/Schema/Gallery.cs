﻿using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using NacGroup.Module.Core.Models;

namespace NacGroup.Module.Gallery.Models.Schema
{
    [BsonIgnoreExtraElements]
    public class Gallery : Entity
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("Title")]
        public string Title { get; set; }

        [BsonElement("IsDefault")]
        public bool IsDefault { get; set; }

        [BsonElement("ShortDescription")]
        public string ShortDescription { get; set; }

        [BsonElement("PageContent")]
        public string PageContent { get; set; }

        [BsonElement("MetaKeyword")]
        public string MetaKeyword { get; set; }

        [BsonElement("MetaDescription")]
        public string MetaDescription { get; set; }

        [BsonElement("MetaTitle")]
        public string MetaTitle { get; set; }

        [BsonElement("Url")]
        public string Url { get; set; }

        [BsonElement("Avatar")]
        public string Avatar { get; set; }

        [BsonElement("CreatedDate")]
        public DateTime CreatedDate { get; set; }

        [BsonElement("UpdatedDate")]
        public DateTime UpdatedDate { get; set; }

        [BsonElement("ListAlbum")]
        public IEnumerable<GalleryImageModel> ListAlbum { get; set; }
        
        [BsonElement("Sort")]
        public int Sort { get; set; }

        public object this[string propertyName]
        {
            get => GetType().GetProperty(propertyName)?.GetValue(this, null);
            set => GetType().GetProperty(propertyName)?.SetValue(this, value, null);
        }
    }

    public class GalleryImageModel
    {
        public string Src { get; set; }
        public string Alt { get; set; }
        public string Url { get; set; }
        public int Sort { get; set; }
        public bool IsChecked { get; set; }
    }
}
