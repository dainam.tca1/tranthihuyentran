﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using NacGroup.Module.Gallery.Services;

namespace NacGroup.Module.Gallery.RestApi
{
    [Route("/api/gallery")]
    [ApiController]
    public class GalleryApiController : ControllerBase
    {
        private readonly IGalleryService _galleryService;

        public GalleryApiController(IGalleryService galleryService)
        {
            _galleryService = galleryService;
        }

        public ActionResult<List<Models.Schema.Gallery>> Get()
        {
            return _galleryService.GetAll().ToList();
        }
     
        
    }
}
