﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using NacGroup.Module.Core.Models;

namespace NacGroup.Module.Core.Interfaces
{
    public interface IRepository<T> where T : class
    {
        T GetSingle(Expression<Func<T, bool>> expression);
        T GetById(string id);
        int Add(T entity);
        int Update(string id, T entity);
        int Delete(string entity);
        IEnumerable<T> Get();
        IEnumerable<T> Get(Expression<Func<T, bool>> predicate);
        PagedResult<T> Get(int page, int pagesize);
        PagedResult<T> Get(int page, int pagesize, Expression<Func<T, bool>> predicate);
    }
}
