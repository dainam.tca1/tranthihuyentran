﻿using NacGroup.Module.Core.Models;

namespace NacGroup.Module.Core.Interfaces
{
    public interface ITenantProvider
    {
        Tenant GetTenant();
    }
   
}
