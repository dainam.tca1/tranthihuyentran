﻿using System.Collections.Generic;
using NacGroup.Module.Core.Models;

namespace NacGroup.Module.Core.Interfaces
{
    public interface IModuleConfigurationManager
    {
        IEnumerable<ModuleInfo> GetModules(string contentRootPath);
    }
}
