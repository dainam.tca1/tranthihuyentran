﻿namespace NacGroup.Module.Core.Interfaces
{
    public interface IMigrationRepository
    {
        void Migration();
        void Seed();
    }
}
