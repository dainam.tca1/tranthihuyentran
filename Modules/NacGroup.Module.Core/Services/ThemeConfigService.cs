﻿using System.Collections.Generic;
using Microsoft.Extensions.Caching.Memory;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using NacGroup.Module.Core.Data;
using NacGroup.Module.Core.Interfaces;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Core.Models.CmsModel;
using Newtonsoft.Json;

namespace NacGroup.Module.Core.Services
{
    public interface IThemeConfigService
    {
        ApiResponseModel Add(ThemeConfigModel model);
        PagedResult<ThemeConfigModel> Filter(string key, int page, int pagesize);
        ApiResponseModel Delete(List<string> idList);
        ThemeConfigModel GetByKey(string id);
        ApiResponseModel Update(ThemeConfigModel model);
    }

    public class ThemeConfigService : IThemeConfigService
    {
        private readonly IAppSettingRepository _appSettingRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMemoryCache _memoryCache;
        private readonly string _tenantName;
        public ThemeConfigService(IAppSettingRepository appSettingRepository, IUnitOfWork unitOfWork, IMemoryCache memoryCache, ITenantProvider tenantProvider)
        {
            _appSettingRepository = appSettingRepository;
            _unitOfWork = unitOfWork;
            _memoryCache = memoryCache;
            _tenantName = tenantProvider.GetTenant()?.TenantName;
        }

        public ApiResponseModel Add(ThemeConfigModel model)
        {
            #region Validate
            if (model == null || model.Data == null)
                return new ApiResponseModel("error", StaticMessage.ParameterInvalid);

            if (string.IsNullOrEmpty(model.Slug))
                return new ApiResponseModel("error", "Please enter theme key");
            if (string.IsNullOrEmpty(model.Name))
                return new ApiResponseModel("error", "Please enter theme name");

            var oldtheme = _appSettingRepository.GetSingle(m => m.Key == model.Slug);
            if(oldtheme != null)
                return new ApiResponseModel("error", "Theme key is exist");
            #endregion

            try
            {
                model.Id = model.Slug;
                var temp = model.MapToAppSetting();
                var result = _appSettingRepository.Add(temp);
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                result = _unitOfWork.Commit();
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);

                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
                  
        }

        public ApiResponseModel Delete(List<string> idlst)
        {
            if (idlst == null)
                return new ApiResponseModel("error", StaticMessage.SelectDataToRemove);
            foreach (var item in idlst)
            {
                Delete(item);
            }
            return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
        }

        public ApiResponseModel Delete(string id)
        {
            var resultcode = _appSettingRepository.DeleteByKey(id);
            if (resultcode < 0)
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
        }

        public PagedResult<ThemeConfigModel> Filter(string key, int page, int pagesize)
        {
            var result = _appSettingRepository.FilterThemeConfig(key,page, pagesize);
            return new PagedResult<ThemeConfigModel>(ThemeConfigModel.MapFromAppSetting(result.Results), result.CurrentPage, result.PageSize, result.TotalItemCount);
        }

        public ThemeConfigModel GetByKey(string id)
        {
            return ThemeConfigModel.MapFromAppSetting(_appSettingRepository.GetSingle(m => m.Key == id));
        }

        public ApiResponseModel Update(ThemeConfigModel model)
        {

            #region Validate
            if (model == null || model.Data == null)
                return new ApiResponseModel("error", StaticMessage.ParameterInvalid);

            if (string.IsNullOrEmpty(model.Slug))
                return new ApiResponseModel("error", "Please enter theme key");
            if (string.IsNullOrEmpty(model.Name))
                return new ApiResponseModel("error", "Please enter theme name");

            var temp = _appSettingRepository.GetSingle(m=>m.Key == model.Slug);
        
            if (temp == null)
                return new ApiResponseModel("error", StaticMessage.DataNotFound);
            #endregion
 
            try
            {
                model.Id = model.Slug;
                temp.Value = BsonSerializer.Deserialize<BsonDocument>(JsonConvert.SerializeObject(model));
                var result = _appSettingRepository.Update(temp.Id, temp);
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                result = _unitOfWork.Commit();
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);

                if (temp.Key == ConstKey.GlobalThemeKey)
                {
                    _memoryCache.Remove($"{_tenantName}_{ConstKey.GlobalConfigCacheKey}");
                }

                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }      
        }
    }
}
