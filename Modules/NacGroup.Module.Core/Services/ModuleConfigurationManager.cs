﻿using System;
using System.Collections.Generic;
using System.IO;
using NacGroup.Module.Core.Interfaces;
using NacGroup.Module.Core.Models;
using Newtonsoft.Json;

namespace NacGroup.Module.Core.Services
{
    public class ModuleConfigurationManager : IModuleConfigurationManager
    {
        public static readonly string ModulesFilename = "modules.json";

        public IEnumerable<ModuleInfo> GetModules(string contentRootPath)
        {
            dynamic moduleConfigs = GetModuleConfigs(contentRootPath);
            foreach (dynamic module in moduleConfigs)
            {
                yield return new ModuleInfo
                {
                    Id = module.id,
                    Version = Version.Parse(module.version.ToString()),
                    Name = module.name
                };
            }
        }

        private static dynamic GetModuleConfigs(string contentRootPath)
        {
            var modulesPath = Path.Combine(contentRootPath, ModulesFilename);
            using (var reader = new StreamReader(modulesPath))
            {
                string content = reader.ReadToEnd();
                dynamic modulesData = JsonConvert.DeserializeObject(content);
                return modulesData;
            }
        }
    }
}
