﻿using System.Collections.Generic;
using System.Linq;
using NacGroup.Module.Core.Data;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Core.Models.Schema;
using NacGroup.Module.Core.Models.ViewModels;

namespace NacGroup.Module.Core.Services
{
    public interface IMenuConfigService
    {
        ApiResponseModel Delete(List<string> idlst);
        ApiResponseModel Delete(string id);
        MenuConfig GetById(string id);
        MenuConfigViewModel GetByName(string name);
        ApiResponseModel Add(MenuConfig model);
        ApiResponseModel Update(MenuConfig model);
        PagedResult<MenuConfig> GetAndSortByLevel();
        ApiResponseModel UpdateCustomize(MenuConfig model, string name);
        List<MenuConfigViewModel> Get();

    }

    public class MenuConfigService : IMenuConfigService
    {
        private readonly IMenuConfigRepository _menuConfigRepository;
        private readonly IUnitOfWork _unitOfWork;

        public MenuConfigService(IUnitOfWork unitOfWork, IMenuConfigRepository menuConfigRepository)
        {
            _unitOfWork = unitOfWork;
            _menuConfigRepository = menuConfigRepository;
        }

        public MenuConfigViewModel GetByName(string name)
        {
            if (string.IsNullOrEmpty(name)) return null;
            var menuConfig = Get().FirstOrDefault(m => m.Text == name);
            return menuConfig;
        }

        public ApiResponseModel Delete(List<string> idlst)
        {
            if (idlst == null)
                return new ApiResponseModel("error", StaticMessage.SelectDataToRemove);
            foreach (var item in idlst)
            {
                Delete(item);
            }
            return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
        }

        public ApiResponseModel Delete(string id)
        {

            if(_menuConfigRepository.GetChildrenMenus(id).Any())
                return new ApiResponseModel("error", "Please remove all children menu before");

            var resultcode = _menuConfigRepository.Delete(id);
            if (resultcode < 0)
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            resultcode = _unitOfWork.Commit();
            if (resultcode < 0)
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
        }

        public MenuConfig GetById(string id)
        {
            return _menuConfigRepository.GetById(id);
        }

        public ApiResponseModel Add(MenuConfig model)
        {
            #region Validate
            if (string.IsNullOrEmpty(model.Text)) return new ApiResponseModel("error", "Menu text is required");

            //Parent category
            MenuConfig parentcate = null;
            if (!string.IsNullOrEmpty(model.ParentId))
            {
                parentcate = _menuConfigRepository.GetById(model.ParentId);
                if (parentcate == null) return new ApiResponseModel("error", "Parent menu is not found");
            }
            #endregion

            try
            {
                model.MenuLevel = parentcate != null ? parentcate.MenuLevel + 1 : 0;

                var result = _menuConfigRepository.Add(model);
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);

                result = _unitOfWork.Commit();
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);

                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }

        public ApiResponseModel Update(MenuConfig model)
        {
            #region Validate
            if (string.IsNullOrEmpty(model.Text)) return new ApiResponseModel("error", "Menu text is required");


            var temp = _menuConfigRepository.GetById(model.Id);
            if (temp == null)
                return new ApiResponseModel("error", StaticMessage.DataNotFound);


            MenuConfig parentcate = null;
            if (!string.IsNullOrEmpty(model.ParentId))
            {
                parentcate = _menuConfigRepository.GetById(model.ParentId);
                if (parentcate == null) return new ApiResponseModel("error", "Parent category is not found");

                if (model.ParentId != temp.ParentId)
                {
                    if (parentcate.MenuLevel > temp.MenuLevel) return new ApiResponseModel("error", "Parent menu level must lower than current menu");
                }

                if (model.ParentId == temp.Id) return new ApiResponseModel("error", "Parent menu level must different from current menu");

            }
            #endregion

            try
            {
                model.MenuLevel = parentcate != null ? parentcate.MenuLevel + 1 : 0;
                var isChangeChildLevel = temp.MenuLevel != model.MenuLevel;
               

                temp.Avatar = model.Avatar;
                temp.Sort = model.Sort;
                temp.Url = model.Url;
                temp.Text = model.Text;
                temp.ParentId = string.IsNullOrEmpty(model.ParentId) ? null : model.ParentId;
                var result = _menuConfigRepository.Update(model.Id, temp);
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);

                #region Kiểm tra các table reference tới table này
                if (isChangeChildLevel)
                {
                    UpdateChildCategoryLevel(temp);
                }
                #endregion

                result = _unitOfWork.Commit();

              
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }

        private void UpdateChildCategoryLevel(MenuConfig category)
        {
            var childrentlist = _menuConfigRepository.GetChildrenMenus(category.Id).ToArray();

            if (!childrentlist.Any())
                return;

            foreach (var item in childrentlist)
            {
                item.MenuLevel = category.MenuLevel + 1;
                _menuConfigRepository.Update(item.Id, item);
                UpdateChildCategoryLevel(item);
            }
        }

        public PagedResult<MenuConfig> GetAndSortByLevel()
        {
            var catlst = _menuConfigRepository.Get().ToList();
            //Chuyển về dạng tree
            catlst = catlst.OrderBy(x => x.Sort).ThenBy(x => x.Text).ToList();
            var stack = new Stack<MenuConfig>();

            // Grab all the items without parents
            foreach (var section in catlst.Where(x => x.ParentId == null).Reverse())
            {
                stack.Push(section);
                catlst.Remove(section);
            }

            var output = new List<MenuConfig>();
            while (stack.Any())
            {
                var currentSection = stack.Pop();

                var children = catlst.Where(x => x.ParentId == currentSection.Id).OrderBy(m => m.Sort).Reverse();

                foreach (var section in children)
                {
                    stack.Push(section);
                    catlst.Remove(section);
                }
                output.Add(currentSection);
            }

            return new PagedResult<MenuConfig>(output, 1, output.Count, output.Count);
        }


        public List<MenuConfigViewModel> Get()
        {
            var catlst = _menuConfigRepository.Get().ToList();
            //Chuyển về dạng tree
            catlst = catlst.OrderBy(x => x.Sort).ThenBy(x => x.Text).ToList();
            var output = new List<MenuConfigViewModel>();
            var rootlist = catlst.Where(x => x.ParentId == null).ToList();
            foreach (var section in rootlist)
            {
                var result = Get(section, catlst);
                output.Add(result);
            }
            return output;
        }

        private MenuConfigViewModel Get(MenuConfig menuitem, List<MenuConfig> listmenu)
        {
            var result = MenuConfigViewModel.MapFromEntity(menuitem);
            listmenu.Remove(menuitem);
            var childrentlist = listmenu.Where(m=>m.ParentId == menuitem.Id).ToList();

            if (!childrentlist.Any())
                return result;

            foreach (var item in childrentlist)
            {
                listmenu.Remove(item);
                var child = Get(item, listmenu);
                result.Children.Add(child);
            }

            return result;
        }

        public ApiResponseModel UpdateCustomize(MenuConfig model, string name)
        {
            try
            {
                if (model == null || string.IsNullOrEmpty(name)) return new ApiResponseModel("error", StaticMessage.DataNotFound);

                var temp = _menuConfigRepository.GetById(model.Id);
                if (temp == null) return new ApiResponseModel("error", StaticMessage.DataNotFound);

                temp[name] = model[name];
                var result = _menuConfigRepository.Update(model.Id, temp);
                if (result < 0) return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                result = _unitOfWork.Commit();
                if (result < 0) return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }

    }
}
