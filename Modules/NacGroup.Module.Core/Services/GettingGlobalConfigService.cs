﻿using Microsoft.Extensions.Caching.Memory;
using MongoDB.Bson;
using NacGroup.Module.Core.Data;
using NacGroup.Module.Core.Interfaces;
using NacGroup.Module.Core.Models;
using Newtonsoft.Json.Linq;

namespace NacGroup.Module.Core.Services
{
    public interface IGettingGlobalConfigService
    {
        GlobalConfiguration GetConfiguration();
    }
    public class GettingGlobalConfigService : IGettingGlobalConfigService
    {
        private readonly IThemeConfigService _themeConfigService;
        private readonly IAppSettingRepository _appSettingRepository;
        private readonly IMemoryCache _memoryCache;
        private readonly Tenant _currentTenant;

        public GettingGlobalConfigService(IThemeConfigService themeConfigService, IAppSettingRepository appSettingRepository, IMemoryCache memoryCache, ITenantProvider tenantProvider)
        {
            _currentTenant = tenantProvider.GetTenant();
            _themeConfigService = themeConfigService;
            _appSettingRepository = appSettingRepository;
            _memoryCache = memoryCache;
        }

        public GlobalConfiguration GetConfiguration()
        {
            var cacheKey = $"{_currentTenant?.TenantName}_{ConstKey.GlobalConfigCacheKey}";

            if (_memoryCache.TryGetValue(cacheKey, out GlobalConfiguration config)) return config;

            var curency = _appSettingRepository.GetSingle(m => m.Key == ConstKey.CurrencySettingKey);


            config = new GlobalConfiguration
            {
                GlobalThemeConfig = JObject.FromObject(_themeConfigService.GetByKey(ConstKey.GlobalThemeKey)),
                GlobalWebSetting = JObject.Parse(_appSettingRepository.GetSingle(m => m.Key == ConstKey.GlobalWebSettingKey).Value.ToJson()),
                SendMailUserName = "noreply.webnails.money@gmail.com",
                SendMailPassword = "ywlfioeonxnnicsx",
                WebsiteTheme = _currentTenant?.ActiveTheme,
                TenantName = _currentTenant?.TenantName,
                TenantId = _currentTenant?.Id,
                StoreKey = _currentTenant?.StoreKey,
                Currency = curency != null ? (CurrencyType)JObject.Parse(curency.Value.ToJson())["Currency"].Value<int>() : CurrencyType.Dollar
            };
            _memoryCache.Set(cacheKey, config);

            return config;
        }
    }
}
