﻿using System.Net.Mail;

namespace NacGroup.Module.Core.Services
{
    public class EmailModel
    {
        public string From { get; set; }
        public string To { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public string Pass { get; set; }
        public string DisplayName { get; set; }

        public EmailModel(string from, string pass, string to, string subject, string body,string displayName)
        {
            this.From = from;
            this.To = to;
            this.Subject = subject;
            this.Body = body;
            this.Pass = pass;
            DisplayName = displayName;
        }
    }
    public interface ISendMailService
    {
        bool SendMail(EmailModel model);
    }
    public class SendMailService : ISendMailService
    {
        public bool SendMail(EmailModel model)
        {
            if (string.IsNullOrEmpty(model.From) || string.IsNullOrEmpty(model.Pass) || string.IsNullOrEmpty(model.To) || string.IsNullOrEmpty(model.Subject) || string.IsNullOrEmpty(model.Body))
                return false;
            try
            {
                var mail = new MailMessage();
                mail.To.Add(model.To);
                mail.From = new MailAddress(model.From,$"Noreply {model.DisplayName}");
                mail.Subject = model.Subject;
                mail.Body = model.Body;
                mail.IsBodyHtml = true;
                var smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    UseDefaultCredentials = false,
                    Credentials = new System.Net.NetworkCredential(model.From, model.Pass),
                    EnableSsl = true,
                };
                //  relay-hosting.secureserver.net
                // 465
                // Enter seders User name and password           
                smtp.Send(mail);
                return true;
            }
            catch (System.Exception ex)
            {
                return false;
            }

        }
    }

    public class SendMailPleskService : ISendMailService
    {
        public bool SendMail(EmailModel model)
        {
            if (string.IsNullOrEmpty(model.From) || string.IsNullOrEmpty(model.Pass) || string.IsNullOrEmpty(model.To) || string.IsNullOrEmpty(model.Subject) || string.IsNullOrEmpty(model.Body))
                return false;
            try
            {
                var mail = new MailMessage();
                mail.To.Add(model.To);
                mail.From = new MailAddress(model.From, $"Noreply {model.DisplayName}");
                mail.Subject = model.Subject;
                mail.Body = model.Body;
                mail.IsBodyHtml = true;
                var smtp = new SmtpClient
                {
                    Host = "mail.dienmayhanam.com.vn",
                    Port = 25,
                    UseDefaultCredentials = false,
                    Credentials = new System.Net.NetworkCredential(model.From, model.Pass),
                    EnableSsl = false
                };
                //  relay-hosting.secureserver.net
                // 465
                // Enter seders User name and password           
                smtp.Send(mail);
                return true;
            }
            catch (System.Exception ex)
            {
                return false;
            }

        }
    }
}
