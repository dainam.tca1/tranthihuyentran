﻿using System;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Caching.Memory;
using MongoDB.Driver;
using NacGroup.Infrastructure;
using NacGroup.Module.Core.Interfaces;
using NacGroup.Module.Core.Models;

namespace NacGroup.Module.Core.Services
{
    public class DatabaseTenantProvider : ITenantProvider
    {
        private string _host;
        private readonly IMemoryCache _memoryCache;

        public DatabaseTenantProvider(IHttpContextAccessor accessor, IMemoryCache memoryCache)
        {
            _memoryCache = memoryCache;
            _host = accessor.HttpContext.Request.Host.ToString();
        }

        public Tenant GetTenant()
        {
            if (_host.StartsWith("www."))
            {
                _host = _host.Replace("www.", "");

            }
            var cacheKey = $"{_host}_{ConstKey.TenantInformationCacheKey}";

            if (_memoryCache.TryGetValue(cacheKey, out Tenant result)) return result;

            var mongoClient = new MongoClient(AllAppGlobalConfig.ConnectionString);
            var database = mongoClient.GetDatabase(AllAppGlobalConfig.DatabaseName);
            var collect = database.GetCollection<Tenant>("WebsiteService");

            result = collect.Find(m => m.Domain == _host).FirstOrDefault();

            if (result == null || result.Status == WebsiteStatus.InActive) return null;
            _memoryCache.Set(cacheKey, result, DateTimeOffset.Now.AddDays(1));
            return result;
        }
    }

}
