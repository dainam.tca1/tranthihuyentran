﻿using System.Net.Http;
using System.Text;
using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace NacGroup.Module.Core.Services
{
    public interface IZaloLoggerService
    {
        void SendMessage(string message);
        void SaveToken(string token);
    }
    public class ZaloLoggerService : IZaloLoggerService
    {
        private readonly IMemoryCache _memoryCache;
        private const string ZaloTokenCacheKey = "ZALOTOKEN_CACHE_KEY";
        private readonly HttpClient _httpClient;
        public ZaloLoggerService(IMemoryCache memoryCache, IHttpClientFactory httpClientFactory)
        {
            _memoryCache = memoryCache;
            _httpClient = httpClientFactory.CreateClient();
        }
        public void SendMessage(string message)
        {
            if(_memoryCache.TryGetValue(ZaloTokenCacheKey,out string token))
            {
                var res = _httpClient.GetAsync(
                        "https://openapi.zalo.me/v2.0/oa/getfollowers?access_token=" + token + "&data={\"offset\":0,\"count\":5}")
                    .Result;
                var followers = res.Content.ReadAsAsync<JObject>().Result;
                foreach (var item in followers["data"]["followers"])
                {
                    _httpClient.PostAsync("https://openapi.zalo.me/v2.0/oa/message?access_token=" + token, new StringContent(JsonConvert.SerializeObject(new
                        {
                            recipient = new { user_id = item["user_id"].ToString() },
                            message = new { text = message }
                        }), Encoding.UTF8, "application/json"));
                }
            }
        }

        public void SaveToken(string token)
        {
            _memoryCache.Set(ZaloTokenCacheKey, token);
        }
    }
}
