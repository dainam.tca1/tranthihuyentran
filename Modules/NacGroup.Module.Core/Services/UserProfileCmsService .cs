﻿using System.Collections.Generic;
using MongoDB.Bson;
using NacGroup.Infrastructure;
using NacGroup.Infrastructure.Shared;
using NacGroup.Module.Core.Data;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Core.Models.CmsModel;
using NacGroup.Module.Core.Models.Schema;
using NacGroup.Module.Core.Models.ViewModels;


namespace NacGroup.Module.Core.Services
{
    public interface IUserProfileCmsService
    {
        UserProfile GetById(string id);
        ApiResponseModel UpdateAdminPassword(ChangePasswordCmsModel model, LanguageType lang);
        IEnumerable<UserProfile> GetAllByRole(RoleType type);
        ApiResponseModel RegisterAdmin(RegisterAccountViewModel model, LanguageType lang);
        ApiResponseModel Delete(List<string> idlst);
        ApiResponseModel Delete(string id);
    }

    public class UserProfileCmsService : IUserProfileCmsService
    {
        private readonly IUserProfileRepository _userProfileRepository;
        private readonly IUnitOfWork _unitOfWork;

        public UserProfileCmsService (IUserProfileRepository userProfileRepository, IUnitOfWork unitOfWork)
        {
            _userProfileRepository = userProfileRepository;
            _unitOfWork = unitOfWork;
        }

        public UserProfile GetById(string id)
        {
            return _userProfileRepository.GetById(id);
        }

        public ApiResponseModel UpdateAdminPassword(ChangePasswordCmsModel model, LanguageType lang)
        {
            //get profile user first
            var user = _userProfileRepository.GetSingle(m => m.Id == model.Id);
            if (user == null)
                return new ApiResponseModel("error", StaticMessage.DataNotFound);
            #region Validate
                if (string.IsNullOrEmpty(model.Password)) return new ApiResponseModel("error", CoreStaticMessage.PasswordIsRequired[(int)lang]);
                // validate new password form
                if (string.IsNullOrEmpty(model.NewPassword))
                    return new ApiResponseModel("error", CoreStaticMessage.NewPasswordIsRequired[(int)lang]);
                if (model.NewPassword.Length < 6)
                    return new ApiResponseModel("error", CoreStaticMessage.NewPasswordMustSix[(int)lang]);
                if (model.NewPassword != model.ConfirmPassword)
                    return new ApiResponseModel("error", CoreStaticMessage.ConfirmPasswordWrong[(int)lang]);
                // check current password
                if (!user.IsValidPassword(model.Password))
                    return new ApiResponseModel("error", CoreStaticMessage.CurrentPasswordWrong[(int)lang]);      
            #endregion
            if (!string.IsNullOrEmpty(model.NewPassword))
            {
                user.PasswordHash = user.GeneratePassword(model.NewPassword);
            }

            try
            {
                var result = _userProfileRepository.Update(user.Id, user);
                if (result < 0)
                    return new ApiResponseModel("error", CoreStaticMessage.GlobalErrorMessage[(int)lang]);
                result = _unitOfWork.Commit();
                if (result < 0)
                    return new ApiResponseModel("error", CoreStaticMessage.GlobalErrorMessage[(int)lang]);
                return new ApiResponseModel("success", CoreStaticMessage.DataUpdateSuccessfully[(int)lang]);
            }
            catch
            {
                return new ApiResponseModel("error", CoreStaticMessage.GlobalErrorMessage[(int)lang]);
            }
        }

        public IEnumerable<UserProfile> GetAllByRole(RoleType type)
        {
            return _userProfileRepository.Get(m => m.Role == type);
        }

        public ApiResponseModel RegisterAdmin(RegisterAccountViewModel model, LanguageType lang)
        {
            try
            {
                #region Validate
                if (model == null)
                    return new ApiResponseModel("error", StaticMessage.ParameterInvalid);
                if (string.IsNullOrEmpty(model.FirstName))
                    return new ApiResponseModel("error", CoreStaticMessage.NameIsrequired[(int)lang]);
                if (string.IsNullOrEmpty(model.Phone))
                    return new ApiResponseModel("error", CoreStaticMessage.PhoneIsrequired[(int)lang]);
                var numbericPhone = model.Phone.Replace("-", "");
                if (string.IsNullOrEmpty(model.Email))
                    return new ApiResponseModel("error", CoreStaticMessage.EmailIsrequired[(int)lang]);
                if (!model.Email.IsEmail())
                    return new ApiResponseModel("error", CoreStaticMessage.EmailFormatWrong[(int)lang]);
                if (string.IsNullOrEmpty(model.Password))
                    return new ApiResponseModel("error", CoreStaticMessage.PasswordIsRequired[(int)lang]);
                if (model.Password.Length < 6)
                    return new ApiResponseModel("error", CoreStaticMessage.PasswordMustSix[(int)lang]);
                if (model.Password != model.ConfirmPassword)
                    return new ApiResponseModel("error", CoreStaticMessage.ConfirmPasswordWrong[(int)lang]);



                var oldUser = _userProfileRepository.GetSingle(m => m.Email == model.Email);
                if (oldUser != null) return new ApiResponseModel("error", CoreStaticMessage.EmailIsExist[(int)lang]);
                oldUser = _userProfileRepository.GetSingle(m => m.Phone == numbericPhone);
                if (oldUser != null) return new ApiResponseModel("error", CoreStaticMessage.PhoneIsExist[(int)lang]);
                #endregion
                var temp = new UserProfile
                {
                    Id = ObjectId.GenerateNewId().ToString(),
                    Email = model.Email,
                    FirstName = model.FirstName,
                    LastName = null,
                    Phone = numbericPhone,
                    IsVerified = true,
                    Role = RoleType.Admin,
                    Gender = GenderType.Undefined,
                    Recipient = new List<AddressModel>(),
                 
                };
                temp.PasswordHash = temp.GeneratePassword(model.Password);

                var addUserResult = _userProfileRepository.Add(temp);
                if (addUserResult < 0)
                    return new ApiResponseModel("error", CoreStaticMessage.GlobalErrorMessage[(int)lang]);
                addUserResult = _unitOfWork.Commit();
                if (addUserResult < 0)
                    return new ApiResponseModel("error", CoreStaticMessage.GlobalErrorMessage[(int)lang]);
                return new ApiResponseModel("success");
            }
            catch 
            {
                return new ApiResponseModel("error", CoreStaticMessage.GlobalErrorMessage[(int)lang]);
            }
        }

        public ApiResponseModel Delete(List<string> idlst)
        {
            if (idlst == null)
                return new ApiResponseModel("error", StaticMessage.SelectDataToRemove);
            foreach (var item in idlst)
            {
                Delete(item);
            }
            return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
        }

        public ApiResponseModel Delete(string id)
        {
            //TODO: Kiểm tra reference class
            var resultcode = _userProfileRepository.Delete(id);
            if (resultcode < 0)
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            resultcode = _unitOfWork.Commit();
            if (resultcode < 0)
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
        }
    }

}
