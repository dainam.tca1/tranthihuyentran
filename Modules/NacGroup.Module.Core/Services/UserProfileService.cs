﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using MongoDB.Bson;
using NacGroup.Infrastructure;
using NacGroup.Infrastructure.Shared;
using NacGroup.Module.Core.Data;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Core.Models.Schema;
using NacGroup.Module.Core.Models.ViewModels;

namespace NacGroup.Module.Core.Services
{
    public interface IUserProfileService
    {
        ApiResponseModel IsAuthenticated(string username, string password);
        ApiResponseModel JwtAuthenticated(string username, string password);
        ApiResponseModel Register(RegisterAccountViewModel model);
        ApiResponseModel ActivateAccount(string code, string acid);
        UserProfile GetByPhone(string phone);
        ApiResponseModel ConfirmAccountKit(RegisterAccountViewModel model);
        ApiResponseModel ForgotPassword(string email);
        ApiResponseModel CheckResetPassword(string code, string acid);
        ApiResponseModel ResetPassword(string code, string acid, ResetPasswordViewModel model);
        UserProfile GetById(string id);
        ApiResponseModel UpdateProfile(UpdateProfileAccountViewModel model);
        ApiResponseModel Delete(string id);
    }

    public class UserProfileService : IUserProfileService
    {
        private readonly IUserProfileRepository _userProfileRepository;
        private readonly IViewRenderService _viewRenderService;
        private readonly ISendMailService _sendMailService;
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILoggerRepository _loggerRepository;
        private readonly GlobalConfiguration _globalConfiguration;

        public UserProfileService(IUserProfileRepository userProfileRepository, IUnitOfWork unitOfWork, IViewRenderService viewRenderService, ISendMailService sendMailService, ILoggerRepository loggerRepository, IGettingGlobalConfigService gettingGlobalConfigService)
        {
            _userProfileRepository = userProfileRepository;
            _unitOfWork = unitOfWork;
            _viewRenderService = viewRenderService;
            _sendMailService = sendMailService;
            _loggerRepository = loggerRepository;
            _globalConfiguration = gettingGlobalConfigService.GetConfiguration();
        }


        public ApiResponseModel IsAuthenticated(string username, string password)
        {
            if (string.IsNullOrEmpty(username))
                return new ApiResponseModel("error", "Phone or email is required");
            if (string.IsNullOrEmpty(password))
                return new ApiResponseModel("error", "Password is required");

            var user = username == AllAppGlobalConfig.DefaultRootUserName && password == AllAppGlobalConfig.DefaultRootPassword ? new UserProfile
            {
                FirstName = "Admin",
                LastName = "Root",
                Id = "123",
                Email = "nacgroup.vn@gmail.com",
                Role = RoleType.Admin,
                Phone = AllAppGlobalConfig.DefaultRootUserName
            } : _userProfileRepository.GetSingle(m => m.Phone == username || m.Email == username);
            if (user == null)
                return new ApiResponseModel("error", "Username or password is wrong");
            if ((username != AllAppGlobalConfig.DefaultRootUserName || password != AllAppGlobalConfig.DefaultRootPassword) && !user.IsValidPassword(password))
                return new ApiResponseModel("error", "Username or password is wrong");

            return new ApiResponseModel("success", "Is Authenticated", user);
        }

        public ApiResponseModel JwtAuthenticated(string username, string password)
        {
            if (string.IsNullOrEmpty(username))
                return new ApiResponseModel("error", "Phone or email is required");
            if (string.IsNullOrEmpty(password))
                return new ApiResponseModel("error", "Password is required");

            var user = _userProfileRepository.GetSingle(m => m.Phone == username || m.Email == username);
            if (user == null)
                return null;
            if (!user.IsValidPassword(password))
                return new ApiResponseModel("error", "Username or password is wrong");

            var claims = new[] {
                new Claim(ClaimTypes.GivenName, Convert.ToString(user.FirstName) + " "  + Convert.ToString(user.LastName)),
                new Claim(ClaimTypes.Email, Convert.ToString(user.Email)),
                new Claim(ClaimTypes.Name, Convert.ToString(user.Id)),
                new Claim(ClaimTypes.NameIdentifier, _globalConfiguration.TenantName),
                new Claim( ClaimTypes.Role,user.Role.ToString()),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(AllAppGlobalConfig.JwtSecretKey));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var token = new JwtSecurityToken(null,
                null,
                claims,
                expires: DateTime.Now.AddYears(1),
                signingCredentials: creds);

            return new ApiResponseModel("success", "Is Authenticated", new JwtSecurityTokenHandler().WriteToken(token));
        }

        public ApiResponseModel Register(RegisterAccountViewModel model)
        {
            try
            {
                #region Validate
                if (model == null)
                    return new ApiResponseModel("error", StaticMessage.ParameterInvalid);
                if (string.IsNullOrEmpty(model.Email))
                    return new ApiResponseModel("error", "Email is required");
                if (!model.Email.IsEmail())
                    return new ApiResponseModel("error", "Email format is wrong");
                if (string.IsNullOrEmpty(model.Password))
                    return new ApiResponseModel("error", "Password is required");
                if (model.Password.Length < 6)
                    return new ApiResponseModel("error", "Password must have minimum 6 characters");
                if (model.Password != model.ConfirmPassword)
                    return new ApiResponseModel("error", "Confirm password is not match");
                if (string.IsNullOrEmpty(model.FirstName))
                    return new ApiResponseModel("error", "First name is required");
                if (string.IsNullOrEmpty(model.LastName))
                    return new ApiResponseModel("error", "Last name is required");
                if (string.IsNullOrEmpty(model.Phone))
                    return new ApiResponseModel("error", "Phone number is required");
                var numbericPhone = model.Phone.Replace("-", "");

                //if (!string.IsNullOrEmpty(model.Birthday) && DateTime.TryParseExact(model.Birthday, _dateFormat, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out var birthDay))
                //    return new ApiResponseModel("error", "Birthday format is wrong");
                var oldUser = _userProfileRepository.GetSingle(m => m.Email == model.Email);
                if (oldUser != null) return new ApiResponseModel("error", "Email is exist");
                oldUser = _userProfileRepository.GetSingle(m => m.Phone == numbericPhone);
                if (oldUser != null) return new ApiResponseModel("error", "Phone number is exist");
                #endregion
                var temp = new UserProfile
                {
                    Id = ObjectId.GenerateNewId().ToString(),
                    Email = model.Email,
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    Phone = numbericPhone,
                    IsVerified = false,
                    Token = $"{model.Email}|{DateTime.UtcNow}".Base64Encode(),
                    Role = RoleType.Customer,
                    Recipient = new List<AddressModel>()
                };
                temp.PasswordHash = temp.GeneratePassword(model.Password);

                var addUserResult = _userProfileRepository.Add(temp);
                if (addUserResult < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                addUserResult = _unitOfWork.Commit();
                if (addUserResult < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);

                var verifylink = $"{_globalConfiguration.WebsiteUrl}/account/verifyaccount?code={ temp.Token}&acid={ temp.Id}";
                var mailmessage = _viewRenderService.RenderToStringAsync("_VerifyAccountMail", new
                {
                    WebsiteName = _globalConfiguration.GlobalWebSetting["WebInfomation"]["WebsiteName"].ToString(),
                    NewUser = temp,
                    VerifyLink = verifylink
                }).Result;

                var sendmailResult = _sendMailService.SendMail(new EmailModel(_globalConfiguration.SendMailUserName,
                    _globalConfiguration.SendMailPassword, temp.Email,
                    $"Welcome to {_globalConfiguration.GlobalWebSetting["WebInfomation"]["WebsiteName"]}",
                    mailmessage, _globalConfiguration.GlobalWebSetting["WebInfomation"]["WebsiteName"].ToString()));
                if (!sendmailResult) _loggerRepository.Add(new Logger(LogLevel.Error, "Lỗi send mail verify account"));
                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully, temp.Id);
            }
            catch (Exception e)
            {
                _loggerRepository.Add(new Logger(LogLevel.Error, "Lỗi register account", e));
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }

        public ApiResponseModel ActivateAccount(string code, string acid)
        {
            try
            {
                if (string.IsNullOrEmpty(code) || string.IsNullOrEmpty(acid))
                    return new ApiResponseModel("error", StaticMessage.ParameterInvalid);
                var account = _userProfileRepository.GetById(acid);
                if (account == null || account.Token != code)
                {
                    return new ApiResponseModel("error", StaticMessage.ParameterInvalid);
                }

                account.Token = null;
                account.IsVerified = true;
                var result = _userProfileRepository.Update(acid, account);
                if (result < 0) return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                result = _unitOfWork.Commit();
                if (result < 0) return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
            }
            catch (Exception e)
            {
                _loggerRepository.Add(new Logger(LogLevel.Error, "Lỗi activate account link", e));
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }

        public UserProfile GetByPhone(string phone)
        {
            return _userProfileRepository.GetSingle(m => m.Phone == phone);
        }

        public ApiResponseModel ConfirmAccountKit(RegisterAccountViewModel model)
        {
            try
            {
                #region Validate
                if (model == null)
                    return new ApiResponseModel("error", StaticMessage.ParameterInvalid);
                if (string.IsNullOrEmpty(model.Email))
                    return new ApiResponseModel("error", "Email is required");
                if (!model.Email.IsEmail())
                    return new ApiResponseModel("error", "Email format is wrong");
                if (string.IsNullOrEmpty(model.FirstName))
                    return new ApiResponseModel("error", "First name is required");
                if (string.IsNullOrEmpty(model.LastName))
                    return new ApiResponseModel("error", "Last name is required");
                if (string.IsNullOrEmpty(model.Phone))
                    return new ApiResponseModel("error", "Phone number is required");

                var oldUser = _userProfileRepository.GetSingle(m => m.Email == model.Email);
                if (oldUser != null) return new ApiResponseModel("error", "Email is exist");
                oldUser = _userProfileRepository.GetSingle(m => m.Phone == model.Phone);
                if (oldUser != null) return new ApiResponseModel("error", "Phone number is exist");

                #endregion
                var temp = new UserProfile
                {
                    Id = ObjectId.GenerateNewId().ToString(),
                    Email = model.Email,
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    Phone = model.Phone,
                    IsVerified = true,
                    Role = RoleType.Customer,
                    Recipient = new List<AddressModel>()
                };
                var addUserResult = _userProfileRepository.Add(temp);
                if (addUserResult < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                addUserResult = _unitOfWork.Commit();
                if (addUserResult < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully, temp.Id);
            }
            catch (Exception e)
            {
                _loggerRepository.Add(new Logger(LogLevel.Error, "Lỗi confirm account kit", e));
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }

        public ApiResponseModel ForgotPassword(string email)
        {
            try
            {
                if (string.IsNullOrEmpty(email))
                    return new ApiResponseModel("error", "Email is required");
                if (!email.IsEmail())
                    return new ApiResponseModel("error", "Email format is wrong");
                var webUser = _userProfileRepository.GetSingle(m => m.Email == email);
                if (webUser == null) return new ApiResponseModel("error", "Email is not available");
                webUser.Token = $"{webUser.Email}|{DateTime.UtcNow.Ticks}".Base64Encode();

                var result = _userProfileRepository.Update(webUser.Id, webUser);
                if (result < 0) return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                result = _unitOfWork.Commit();
                if (result < 0) return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);

                var verifylink = $"{_globalConfiguration.WebsiteUrl}/account/checkresetpassword?code={ webUser.Token}&acid={ webUser.Id}";
                var mailmessage = _viewRenderService.RenderToStringAsync("_ResetPasswordMail", new
                {
                    WebsiteName = _globalConfiguration.GlobalWebSetting["WebInfomation"]["WebsiteName"].ToString(),
                    WebUser = webUser,
                    VerifyLink = verifylink
                }).Result;

                var sendmailResult = _sendMailService.SendMail(new EmailModel(_globalConfiguration.SendMailUserName,
                    _globalConfiguration.SendMailPassword, webUser.Email,
                    $"Reset password on {_globalConfiguration.GlobalWebSetting["WebInfomation"]["WebsiteName"]}",
                    mailmessage, _globalConfiguration.GlobalWebSetting["WebInfomation"]["WebsiteName"].ToString()));
                if (!sendmailResult)
                {
                    _loggerRepository.Add(new Logger(LogLevel.Error, "Lỗi send mail verify account"));
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                }
                return new ApiResponseModel("success", "Reset password request has been sent to your email.");
            }
            catch (Exception e)
            {
                _loggerRepository.Add(new Logger(LogLevel.Error, "Lỗi submit reset password account", e));
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }

        }

        public ApiResponseModel CheckResetPassword(string code, string acid)
        {
            try
            {
                if (string.IsNullOrEmpty(code) || string.IsNullOrEmpty(acid))
                    return new ApiResponseModel("error", StaticMessage.ParameterInvalid);
                var account = _userProfileRepository.GetById(acid);
                if (account == null || account.Token != code)
                {
                    return new ApiResponseModel("error", StaticMessage.ParameterInvalid);
                }

                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
            }
            catch (Exception e)
            {
                _loggerRepository.Add(new Logger(LogLevel.Error, "Lỗi check reset password link", e));
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }

        }

        public ApiResponseModel ResetPassword(string code, string acid, ResetPasswordViewModel model)
        {
            try
            {
                var checkResult = CheckResetPassword(code, acid);
                if (checkResult.status == "error") return checkResult;

                if (model == null) return new ApiResponseModel("error", StaticMessage.ParameterInvalid);
                if (string.IsNullOrEmpty(model.Password))
                    return new ApiResponseModel("error", "Password is required");
                if (model.Password.Length < 6)
                    return new ApiResponseModel("error", "Password must have minimum 6 characters");
                if (model.Password != model.ConfirmPassword)
                    return new ApiResponseModel("error", "Confirm password is not match");

                var currentUser = _userProfileRepository.GetById(acid);
                if (currentUser == null) return new ApiResponseModel("error", StaticMessage.ParameterInvalid);

                currentUser.Token = null;
                currentUser.PasswordHash = currentUser.GeneratePassword(model.Password);
                var result = _userProfileRepository.Update(acid, currentUser);
                if (result < 0) return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                result = _unitOfWork.Commit();
                if (result < 0) return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
            }
            catch (Exception e)
            {
                _loggerRepository.Add(new Logger(LogLevel.Error, "Lỗi reset password", e));
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }

        }


        public UserProfile GetById(string id)
        {
            return _userProfileRepository.GetById(id);
        }

        public ApiResponseModel UpdateProfile(UpdateProfileAccountViewModel model)
        {
            //get profile user first
            var user = _userProfileRepository.GetSingle(m => m.Id == model.Id);
            if (user == null)
                return new ApiResponseModel("error", StaticMessage.DataNotFound);
            #region Validate
            if (string.IsNullOrEmpty(model.FirstName)) return new ApiResponseModel("error", "First name is required");
            if (string.IsNullOrEmpty(model.LastName)) return new ApiResponseModel("error", "Last name is required");
            if (!string.IsNullOrEmpty(model.Password) || !string.IsNullOrEmpty(model.NewPassword) || !string.IsNullOrEmpty(model.ConfirmPassword))
            {
                if (string.IsNullOrEmpty(model.Password)) return new ApiResponseModel("error", "Password is required");
                // validate new password form
                if (string.IsNullOrEmpty(model.NewPassword))
                    return new ApiResponseModel("error", "New Password is required");
                if (model.NewPassword.Length < 6)
                    return new ApiResponseModel("error", "New Password must have minimum 6 characters");
                if (model.NewPassword != model.ConfirmPassword)
                    return new ApiResponseModel("error", "Confirm password is not match");
                // check current password
                if (!user.IsValidPassword(model.Password))
                    return new ApiResponseModel("error", "Current Password is wrong");
            }
            #endregion

            user.FirstName = model.FirstName;
            user.LastName = model.LastName;
            user.Recipient = (model.Recipient == null || model.Recipient.Count == 0) ? new List<AddressModel>() : model.Recipient;
         
            if (!string.IsNullOrEmpty(model.NewPassword))
            {
                user.PasswordHash = user.GeneratePassword(model.NewPassword);
            }

            try
            {
                var result = _userProfileRepository.Update(user.Id, user);
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                result = _unitOfWork.Commit();
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }

        public ApiResponseModel Delete(string id)
        {
            var resultcode = _userProfileRepository.Delete(id);
            if (resultcode < 0)
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            resultcode = _unitOfWork.Commit();
            if (resultcode < 0)
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
        }
    }

}
