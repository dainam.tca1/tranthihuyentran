﻿using System.Collections.Generic;
using Microsoft.Extensions.Caching.Memory;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using NacGroup.Module.Core.Data;
using NacGroup.Module.Core.Interfaces;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Core.Models.CmsModel;
using NacGroup.Module.Core.Models.Schema;
using Newtonsoft.Json;

namespace NacGroup.Module.Core.Services
{
    public interface IWebsiteConfigService
    {
        AppSettingCmsModel GetGlobalConfig();
        ApiResponseModel UpdateGlobalConfig(dynamic model);
        PagedResult<AppSettingCmsModel> Filter(string key, int page, int pagesize);
        ApiResponseModel Update(AppSettingCmsModel appSetting);
        ApiResponseModel Delete(List<string> idList);
        ApiResponseModel Delete(string id);
        AppSettingCmsModel GetById(string id);
        ApiResponseModel Add(AppSettingCmsModel model);
        AppSettingCmsModel GetByKey(string key);

    }

    public class WebsiteConfigService : IWebsiteConfigService
    {
        private readonly IAppSettingRepository _appSettingRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMemoryCache _memoryCache;
        private readonly string _tenantName;

        public WebsiteConfigService(IAppSettingRepository appSettingRepository, IUnitOfWork unitOfWork, IMemoryCache memoryCache, ITenantProvider tenantProvider)
        {
            _appSettingRepository = appSettingRepository;
            _unitOfWork = unitOfWork;
            _memoryCache = memoryCache;
            _tenantName = tenantProvider.GetTenant()?.TenantName;
        }

        public PagedResult<AppSettingCmsModel> Filter(string key, int page, int pagesize)
        {
            var pagedAppSetting = _appSettingRepository.FilterAppConfig(key, page, pagesize);
            return new PagedResult<AppSettingCmsModel>(AppSettingCmsModel.MapFromEntity(pagedAppSetting.Results), pagedAppSetting.CurrentPage, pagedAppSetting.PageSize, pagedAppSetting.TotalItemCount);
        }

        public ApiResponseModel Update(AppSettingCmsModel appSetting)
        {
            try
            {
                if (appSetting == null || string.IsNullOrEmpty(appSetting.Id)) return new ApiResponseModel("error", StaticMessage.DataNotFound);

                var webSetting = _appSettingRepository.GetSingle(m => m.Id == appSetting.Id);
                if (webSetting == null)
                    return new ApiResponseModel("error", StaticMessage.DataNotFound);


                var oldcate = _appSettingRepository.GetSingle(m => m.Key == appSetting.Key);
                if (oldcate != null && webSetting.Id != oldcate.Id)
                    return new ApiResponseModel("error", "Key is exist");


                webSetting.Value = BsonSerializer.Deserialize<BsonDocument>(JsonConvert.SerializeObject(appSetting.Value));
                var result = _appSettingRepository.Update(webSetting.Id, webSetting);
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                result = _unitOfWork.Commit();
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);

                if (appSetting.Key == ConstKey.CurrencySettingKey)
                {
                    _memoryCache.Remove($"{_tenantName}_{ConstKey.GlobalConfigCacheKey}");
                }

                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }

        }
        public ApiResponseModel Delete(List<string> idlst)
        {
            if (idlst == null)
                return new ApiResponseModel("error", StaticMessage.SelectDataToRemove);
            foreach (var item in idlst)
            {
                Delete(item);
            }
            return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
        }

        public AppSettingCmsModel GetById(string id)
        {
            return AppSettingCmsModel.MapFromEntity(_appSettingRepository.GetById(id));
        }

        public ApiResponseModel Add(AppSettingCmsModel model)
        {
            if (model == null || model.Value == null || model.Key == null) return new ApiResponseModel("error", StaticMessage.ParameterInvalid);

            var oldcate = _appSettingRepository.GetSingle(m => m.Key == model.Key);
            if (oldcate != null)
                return new ApiResponseModel("error", "Key is exist");
            try
            {
                var temp = new AppSetting
                {
                    Key = model.Key,
                    Value = BsonSerializer.Deserialize<BsonDocument>(JsonConvert.SerializeObject(model.Value))
                };
                var result = _appSettingRepository.Add(temp);
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                result = _unitOfWork.Commit();
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);

                if (model.Key == ConstKey.CurrencySettingKey)
                {
                    _memoryCache.Remove($"{_tenantName}_{ConstKey.GlobalConfigCacheKey}");
                }

                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }

        public ApiResponseModel Delete(string id)
        {
            var resultcode = _appSettingRepository.Delete(id);
            if (resultcode < 0)
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            resultcode = _unitOfWork.Commit();
            if (resultcode < 0)
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
        }


        public AppSettingCmsModel GetByKey(string key)
        {
            return AppSettingCmsModel.MapFromEntity(_appSettingRepository.GetSingle(m => m.Key == key));
        }

        #region Global webconfig
        public AppSettingCmsModel GetGlobalConfig()
        {
            return AppSettingCmsModel.MapFromEntity(_appSettingRepository.GetSingle(m => m.Key == ConstKey.GlobalWebSettingKey));
        }

        public ApiResponseModel UpdateGlobalConfig(dynamic model)
        {
            try
            {
                if (model == null) return new ApiResponseModel("error", StaticMessage.DataNotFound);

                var webSetting = _appSettingRepository.GetSingle(m => m.Key == ConstKey.GlobalWebSettingKey);
                if (webSetting == null)
                    return new ApiResponseModel("error", StaticMessage.DataNotFound);
                webSetting.Value = BsonSerializer.Deserialize<BsonDocument>(JsonConvert.SerializeObject(model));
                var result = _appSettingRepository.Update(webSetting.Id, webSetting);
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
                result = _unitOfWork.Commit();
                if (result < 0)
                    return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);

                _memoryCache.Remove($"{_tenantName}_{ConstKey.GlobalConfigCacheKey}");

                return new ApiResponseModel("success", StaticMessage.DataUpdateSuccessfully);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            }
        }

        #endregion
    }
}
