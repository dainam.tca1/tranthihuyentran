﻿using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using NacGroup.Module.Core.Interfaces;

namespace NacGroup.Module.Core.Extensions
{

    public class TenantRequirement : IAuthorizationRequirement
    {
        
    }
    public class TenantAuthorizeHandler : AuthorizationHandler<TenantRequirement>
    {
        private readonly string _currentTennantName;

        public TenantAuthorizeHandler(ITenantProvider tenantProvider)
        {
            _currentTennantName = tenantProvider.GetTenant().TenantName;
        }

        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context,
            TenantRequirement requirement)
        {
            if (!context.User.HasClaim(c => c.Type == ClaimTypes.NameIdentifier))
            {
                return Task.CompletedTask;
            }

            var ternantName = context.User.FindFirst(c => c.Type == ClaimTypes.NameIdentifier).Value;

            if (ternantName == _currentTennantName)
            {
                context.Succeed(requirement);
            }

            return Task.CompletedTask;
        }
    }
}
