﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using NacGroup.Infrastructure.Shared;
using NacGroup.Module.Core.Models;

namespace NacGroup.Module.Core.RestApi
{
    [Route("api/cache")]
    [ApiController]
    public class CacheController : ControllerBase
    {
        private readonly IMemoryCache _memoryCache;


        public CacheController(IMemoryCache memoryCache)
        {
            _memoryCache = memoryCache;
        }

        [HttpDelete("{cacheKey}")]
        public ApiResponseModel Delete(string cacheKey)
        {
            var basicAuthorSplit = Request.Headers["Authorization"].ToString().Split(" ")[1];
            var authorzial = basicAuthorSplit.Base64Decode().Split(":");
            var user = authorzial[0];
            var pass = authorzial[1];
            if (user != "dan@dkmobility.com") return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            if (pass != "qJI$J3jE+NA1") return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);
            _memoryCache.Remove(cacheKey);
            return new ApiResponseModel("success", StaticMessage.DataLoadingSuccessfull);
        }
    }
}