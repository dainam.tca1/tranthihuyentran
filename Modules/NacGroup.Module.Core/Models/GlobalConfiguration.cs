﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using TimeZoneConverter;

namespace NacGroup.Module.Core.Models
{
    public class GlobalConfiguration
    {
        public JObject GlobalThemeConfig { get; set; }
        public JObject GlobalWebSetting { get; set; }
        public string SendMailUserName { get; set; }
        public string SendMailPassword { get; set; }
        public string WebsiteTheme { get; set; }
        public string TenantName { get; set; }
        public string TenantId { get; set; }
        public string StoreKey { get; set; }
        public TimeZoneInfo LocalTimeZoneInfo => GlobalWebSetting.HasValues
            ? TZConvert.GetTimeZoneInfo(GlobalWebSetting["WebsiteConfig"]["TimeZone"].ToString())
            : TZConvert.GetTimeZoneInfo(TimeZoneInfo.Local.Id);

        public string WebsiteUrl => GlobalWebSetting["WebInfomation"]["WebsiteURL"].ToString();
        public CurrencyType Currency { get; set; }
    }

    public static class ModuleManager
    {
        public static IList<ModuleInfo> Modules { get; set; } = new List<ModuleInfo>();
    }

    public enum CurrencyType
    {
        Dollar,
        Vnd
    }
}
