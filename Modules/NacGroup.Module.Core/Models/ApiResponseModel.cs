﻿namespace NacGroup.Module.Core.Models
{
    public class ApiResponseModel
    {
        public ApiResponseModel()
        {

        }

        public ApiResponseModel(string stt)
        {
            status = stt;
        }

        public ApiResponseModel(string stt, string msg)
        {
            status = stt;
            message = msg;
        }

        public ApiResponseModel(string stt, dynamic dt)
        {
            status = stt;
            data = dt;
        }

        public ApiResponseModel(string stt, string msg, dynamic dt)
        {
            status = stt;
            message = msg;
            data = dt;
        }
        public string status { get; set; }
        public dynamic data { get; set; }
        public string message { get; set; }
    }
}
