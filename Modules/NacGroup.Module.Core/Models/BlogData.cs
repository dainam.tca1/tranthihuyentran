﻿using System;

namespace NacGroup.Module.Blog.Models.Querys
{
    public class BlogData
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string ShortDescription { get; set; }
        public string CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string CategoryUrl { get; set; }
        public string CategoryAvatar { get; set; }
        public string PageContent { get; set; }
        public string ThumbnailFB { get; set; }
        public string MetaKeyword { get; set; }
        public string MetaDescription { get; set; }
        public string MetaTitle { get; set; }
        public string Url { get; set; }
        public DateTime CreatedDate { get; set; }
        public string Avatar { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string UserProfileId { get; set; }
        public string EcsUserName { get; set; }
        public int ViewCount { get; set; }
        public int Sort { get; set; }
    }
}
