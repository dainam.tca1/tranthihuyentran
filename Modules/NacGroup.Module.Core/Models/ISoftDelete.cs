﻿namespace NacGroup.Module.Core.Models
{
    public interface ISoftDelete
    {
        bool IsDeleted { get; set; }
    }
}
