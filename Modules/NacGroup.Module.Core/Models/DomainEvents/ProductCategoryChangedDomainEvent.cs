﻿using MediatR;

namespace NacGroup.Module.Core.Models.DomainEvents
{
    public class ProductCategoryChangedDomainEvent : INotification
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string Avatar { get; set; }

        public string MetaKeyword { get; set; }
        public string MetaDescription { get; set; }
        public string MetaTitle { get; set; }
        public string Url { get; set; }
        public int Sort { get; set; }

        public ProductCategoryChangedDomainEvent(string id, string name, string description, string avatar, string metaKeyword, string metaDescription,
            string metaTitle, string url, int sort)
        {
            Id = id;
            Name = name;
            Description = description;
            Avatar = avatar;
            MetaKeyword = metaKeyword;
            MetaDescription = metaDescription;
            MetaTitle = metaTitle;
            Url = url;
            Sort = sort;
        }
    }
}
