﻿using MediatR;

namespace NacGroup.Module.Core.Models.DomainEvents
{
    public class CustomerLabelRemoveDomainEvent : INotification
    {
        public string Id { get; set; }
        public CustomerLabelRemoveDomainEvent(string id )
        {
            Id = id; 
        }
    }
}
