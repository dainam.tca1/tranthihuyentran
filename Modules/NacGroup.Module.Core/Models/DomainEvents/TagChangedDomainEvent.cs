﻿using MediatR;

namespace NacGroup.Module.Core.Models.DomainEvents
{
    public class TagChangedDomainEvent : INotification
    {
        public string Id { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }
        public string Slug { get; set; }
        public Method Method { get; set; }
        

        public TagChangedDomainEvent(string id, string name, string description, string slug, Method method)
        {
            Id = id;
            Name = name;
            Description = description;
            Slug = slug;
            Method = method;
        }
    }
    public enum Method
    {
        Update,
        Delete
    }
}
