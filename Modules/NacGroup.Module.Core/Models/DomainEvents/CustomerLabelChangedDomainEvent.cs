﻿using MediatR;

namespace NacGroup.Module.Core.Models.DomainEvents
{
    public class CustomerLabelChangedDomainEvent : INotification
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string Url { get; set; }

        public string Color { get; set; }   

        public CustomerLabelChangedDomainEvent(string id, string name, string url, string color)
        {
            Id = id;
            Name = name;
            Color = color;
            Url = url;
        }
    }
}
