﻿using MediatR;

namespace NacGroup.Module.Core.Models.DomainEvents
{
    public class CustomerLevelChangedDomainEvent : INotification
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string Url { get; set; }

        public decimal AmountRequired { get; set; }

        public CustomerLevelChangedDomainEvent(string id, string name, decimal amount, string url)
        {
            Id = id;
            Name = name;
            AmountRequired = amount;
            Url = url;
        }
    }
}
