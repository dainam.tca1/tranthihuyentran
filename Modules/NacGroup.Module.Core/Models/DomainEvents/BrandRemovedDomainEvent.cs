﻿using MediatR;

namespace NacGroup.Module.Core.Models.DomainEvents
{
    public class BrandRemovedDomainEvent : INotification
    {
        public string Id { get; set; }

        public BrandRemovedDomainEvent(string id)
        {
            Id = id;

        }
    }
}
