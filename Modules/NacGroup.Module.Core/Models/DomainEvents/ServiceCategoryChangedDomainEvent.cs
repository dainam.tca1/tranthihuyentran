﻿using MediatR;

namespace NacGroup.Module.Core.Models.DomainEvents
{
    public class NailServiceCategoryChangedDomainEvent : INotification
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string Avatar { get; set; }

        public string Url { get; set; }

        public NailServiceCategoryChangedDomainEvent(string id, string name, string avatar, string url)
        {
            Id = id;
            Name = name;
            Avatar = avatar;
            Url = url;
        }
    }
}
