﻿namespace NacGroup.Module.Core.Models
{
    public static class ConstKey
    {
        public const string GlobalThemeKey = "THEME_GLOBAL";
        public const string GlobalWebSettingKey = "GLOBAL_WEB_SETTING";
        public const string BookingSettingKey = "BOOKING_SETTING";
        public const string OrderPickUpSettingKey = "ORDER_PICKUP_SETTING";
        public const string AccountKitSettingKey = "FACEBOOK_ACCOUNTKIT";
        public const string GPosApiSettingKey = "GPOS_CONFIG_KEY";
        public const string MxMerchantApiSettingKey = "MERCHANT_CONFIG_KEY";
        public const string MxMerchantApiSettingPlusKey = "MERCHANT_CONFIG_KEY_PLUS";
        public const string EasyShipApiSettingKey = "EASYSHIP_CONFIG_KEY";
        public const string ShippingApiSettingKey = "SHIPPING_CONFIG_KEY";
        public const string GiftCardTemplateKey = "THEME_GIFT_CARD_TEMPLATE";
        public const string CoverImageThemeKey = "THEME_COVER_IMG";
        public const string CurrencySettingKey = "CURRENCY_SETTING_KEY";
        public const string GiftCardSettingKey = "GIFT_CARD_SETTING_KEY";
        public const string CountrySettingKey = "COUNTRY_SETTING_KEY";


        #region Cache key
        public const string ProductPosCacheKey = "PRODUCT_POS_CACHEKEY";
        public const string CustomerPosCacheKey = "CUSTOMER_POS_CACHEKEY";
        public const string GlobalConfigCacheKey = "GLOBAL_CONFIG_CACHEKEY";
        public const string TenantInformationCacheKey = "TENANT_INFORMATION_CACHEKEY";
        public const string SiteMapCacheKey = "SITE_MAP_CACHEKEY";
        public const string CountryCacheKey = "COUNTRY_CACHEKEY";
        public const string CityCacheKey = "CITY_CACHEKEY";
        public const string DistrictCacheKey = "DISTRICT_CACHEKEY";
        public const string WardCacheKey = "WARD_CACHEKEY";
        #endregion

        #region Session Key

        public const string LoginReturnUrlSessionKey = "LoginReturnUrl";
        public const string FacebookUserKitSessionKey = "FacebookUserKit";
        public const string ResetPasswordTimeSessionKey = "ResetPasswordTime";

        #endregion
    }
}
