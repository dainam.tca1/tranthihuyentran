﻿using System.Collections.Generic;
using NacGroup.Module.Core.Models.Schema;

namespace NacGroup.Module.Core.Models.ViewModels
{
    public class MenuConfigViewModel
    {
        
        public string Id { get; set; }


        public string Text { get; set; }


        public string Avatar { get; set; }


        public string ParentId { get; set; }


        public int MenuLevel { get; set; }


        public string Url { get; set; }


        public int Sort { get; set; }

        public List<MenuConfigViewModel> Children { get; set; }

        public object this[string propertyName]
        {
            get => this.GetType().GetProperty(propertyName)?.GetValue(this, null);
            set => this.GetType().GetProperty(propertyName)?.SetValue(this, value, null);
        }

        public static MenuConfigViewModel Create()
        {
            return new MenuConfigViewModel
            {
                Children = new List<MenuConfigViewModel>()
            };
        }

        public static MenuConfigViewModel MapFromEntity(MenuConfig entity)
        {
            if (entity == null) return null;
            var result = Create();
            result.Id = entity.Id;
            result.Text = entity.Text;
            result.Avatar = entity.Avatar;
            result.ParentId = entity.ParentId;
            result.MenuLevel = entity.MenuLevel;
            result.Url = entity.Url;
            result.Sort = entity.Sort;
            return result;
        }
    }
}
