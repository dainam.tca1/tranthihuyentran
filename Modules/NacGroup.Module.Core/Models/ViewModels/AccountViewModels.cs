﻿
using NacGroup.Module.Core.Models.Schema;
using System.Collections.Generic;

namespace NacGroup.Module.Core.Models.ViewModels
{
    public class RegisterAccountViewModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string Birthday { get; set; }
    }

   
    public class UpdateProfileAccountViewModel
    {
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Password { get; set; }
        public string NewPassword { get; set; }
        public string ConfirmPassword { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public List<AddressModel> Recipient { get; set; }
    }


    public class ResetPasswordViewModel
    {
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
    }

    public class FacebookAccountKitSettingModel
    {
        public string ClientId { get; set; }
        public string AccountKitId { get; set; }
        public string Version { get; set; }
        public string CallBackUrl { get; set; }
        public string AccountKitSecret { get; set; }
        public string MeEndpointBaseUrl => $"https://graph.accountkit.com/{Version}/me";
        public string TokenExchangeBaseUrl => $"https://graph.accountkit.com/{Version}/access_token";
    }

    public class FacebookUserKit
    {
        public string Phone { get; set; }
    }
}
