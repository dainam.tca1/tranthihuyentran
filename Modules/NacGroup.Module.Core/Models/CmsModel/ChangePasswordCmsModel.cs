﻿namespace NacGroup.Module.Core.Models.CmsModel
{
    public class ChangePasswordCmsModel
    {
        public string Id { get; set; }
        public string Password { get; set; }
        public string NewPassword { get; set; }
        public string ConfirmPassword { get; set; }
    }
}
