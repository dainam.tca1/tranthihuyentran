﻿namespace NacGroup.Module.Core.Models.CmsModel
{
    public class LoginCmsModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public bool? Remember { get; set; }
    }
}
