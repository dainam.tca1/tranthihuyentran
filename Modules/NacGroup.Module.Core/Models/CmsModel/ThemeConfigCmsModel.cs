﻿using System.Collections.Generic;
using System.Linq;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using NacGroup.Module.Core.Models.Schema;
using Newtonsoft.Json;

namespace NacGroup.Module.Core.Models.CmsModel
{
    public class ThemeConfigModel
    {     
        public ThemeConfigModel()
        {
            ThemeType = ThemeType.Group;
        }
        public string Id { get; set; }
        public string Name { get; set; }
        public string Slug { get; set; }
        public ThemeType ThemeType { get; set; }
        public dynamic Data { get; set; }

        public AppSetting MapToAppSetting()
        {            
            return new AppSetting
            {
                Key = this.Slug,
                Value = BsonSerializer.Deserialize<BsonDocument>(JsonConvert.SerializeObject(this))
            };
        }
        
        public static ThemeConfigModel MapFromAppSetting(AppSetting appsetting)
        {
            if (appsetting == null || appsetting.Value == null) return null;
            return JsonConvert.DeserializeObject<ThemeConfigModel>(appsetting.Value.ToJson());          
        }
        public static IEnumerable<ThemeConfigModel> MapFromAppSetting(IEnumerable<AppSetting> el)
        {
            return el != null ? el.Select(MapFromAppSetting) : new List<ThemeConfigModel>();
        }
    }

    public class ThemeConfigImageItem
    {
        public ThemeConfigImageItem()
        {
            ThemeType = ThemeType.Image;
        }
        public string Name { get; set; }
        public string Url { get; set; }
        public string Src { get; set; }
        public string Alt { get; set; }
        public int Sort { get; set; }
        public ThemeType ThemeType { get; set; }
    }

    public class ThemeConfigTextItem
    {
        public ThemeConfigTextItem()
        {
            ThemeType = ThemeType.Text;
            TextType = TextType.Normal;
        }
        public string Name { get; set; }
        public string Value { get; set; }
        public string Url { get; set; }
        public int Sort { get; set; }
        public ThemeType ThemeType { get; set; }
        public TextType TextType { get; set; }
    }

    public class ThemeConfigImageList
    {
        public ThemeConfigImageList()
        {
            ThemeType = ThemeType.ImageList;
            Data = new List<dynamic>();
        }

        public string Name { get; set; }
        public string Slug { get; set; }
        public ThemeType ThemeType { get; set; }
        public int Sort { get; set; }
        public dynamic Data { get; set; }


    }

    public class ThemeConfigTextImageList
    {
        public ThemeConfigTextImageList()
        {
            ThemeType = ThemeType.TextImageList;
            Template = new
            {
                ImageList = new List<dynamic>(),
                TextList = new List<dynamic>()
            };
            Data = new List<dynamic>();
        }
        public string Name { get; set; }
        public string Slug { get; set; }
        public ThemeType ThemeType { get; set; }
        public int Sort { get; set; }
        public dynamic Data { get; set; }

        public dynamic Template { get; set; }
    }

    public enum ThemeType
    {
        Group,
        Image,
        Text,
        ImageList,
        TextImageList
    }

    public enum TextType
    {
        Normal,
        Html,
        Link
    }
}
