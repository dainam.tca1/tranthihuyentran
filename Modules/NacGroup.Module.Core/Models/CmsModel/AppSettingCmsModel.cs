﻿using System.Collections.Generic;
using System.Linq;
using MongoDB.Bson;
using NacGroup.Module.Core.Models.Schema;
using Newtonsoft.Json;

namespace NacGroup.Module.Core.Models.CmsModel
{
    public class AppSettingCmsModel
    {
        public string Id { get; set; }

        public string Key { get; set; }

        public dynamic Value { get; set; }

        public static AppSettingCmsModel MapFromEntity(AppSetting app)
        {
            
            return app != null ? new AppSettingCmsModel
            {
                Id = app.Id,
                Value = JsonConvert.DeserializeObject(app.Value.ToJson()),
                Key = app.Key
            } : null;
        }

        public static IEnumerable<AppSettingCmsModel> MapFromEntity(IEnumerable<AppSetting> applst)
        {
            return applst.Select(MapFromEntity);
        }
    }
}
