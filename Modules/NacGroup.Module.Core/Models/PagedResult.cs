﻿using System.Collections.Generic;

namespace NacGroup.Module.Core.Models
{
    public class PagedResult<T> : BasePagedResult where T : class
    {
        public PagedResult()
        {
            Results = new List<T>();
        }

        public PagedResult(IEnumerable<T> results, int currentPage, int pageSize, int totalItemCount):base(currentPage,pageSize,totalItemCount)
        {
            Results = results;
        }

        public IEnumerable<T> Results { get; set; }
    }
}
