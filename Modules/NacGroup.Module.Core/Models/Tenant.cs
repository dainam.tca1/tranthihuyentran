﻿using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using NacGroup.Infrastructure;

namespace NacGroup.Module.Core.Models
{
    [BsonIgnoreExtraElements]
    public class Tenant
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string Domain { get; set; }
        public string ConnectionUrl { get; set; }
        public string DatabaseName { get; set; }
        public string TenantName { get; set; }
        public string ActiveTheme { get; set; }
        public string CustomerName { get; set; }
        public string CustomerEmail { get; set; }
        public string CustomerId { get; set; }
        public string StoreKey { get; set; }
        public WebsiteStatus Status { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ExpDate { get; set; }
        public IEnumerable<Features> FeatureList { get; set; }
        public LanguageType DefaultLanguage { get; set; }
    }
    public enum WebsiteStatus
    {
        InActive,
        Active,
        Warning
    }

    [BsonIgnoreExtraElements]
    public class Features
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public bool IsActive { get; set; }
    }
}
