﻿using System.Collections.Generic;

namespace NacGroup.Module.Core.Models
{
    public static class StaticMessage
    {
        public const string GlobalErrorMessage = "Opps! System has error";
        public const string ParameterInvalid = "Parameter invalid";
        public const string DataUpdateSuccessfully = "Update successfully";
        public const string DataNotFound = "Data is not found";
        public const string SelectDataToRemove = "Please select data need to remove";
        public const string DataLoadingSuccessfull = "Success";
    }

    public static class CoreStaticMessage
    {
        public static List<string> GlobalErrorMessage => new List<string>
        {
            "Opps! System has error",
            "Rất tiếc, hệ thống xảy ra lỗi"
        };
        public static List<string> ParameterInvalid => new List<string>
        {
            "Parameter invalid",
            "Tham số không hợp lệ"
        };
        public static List<string> DataUpdateSuccessfully => new List<string>
        {
            "Update successfully",
            "Cập nhật thành công"
        };
        public static List<string> DataNotFound => new List<string>
        {
            "Data is not found",
            "Không tìm thấy dữ liệu"
        };
        public static List<string> SelectDataToRemove => new List<string>
        {
            "Please select data need to remove",
            "Vui lòng chọn dữ liệu cần xóa"
        };
        public static List<string> DataLoadingSuccessfull => new List<string>
        {
            "Success",
            "Thành công"
        };
        //login admin
        public static string[] Login = { "Login", "Đăng nhập" };
        public static string[] SignIn = { "Sign In", "Đăng nhập" };
        public static string[] SignInToAdmin = { "Sign In To Admin", "Đăng nhập vào trang quản trị" };
        public static string[] Email = { "Email", "Email" };
        public static string[] Password = { "Password", "Mật Khẩu" };
        public static string[] RememberMe = { "Remember me", "Nhớ tôi" };
        public static string[] ForgetPassword = { "Forget Password", "Quên mật khẩu" };
        public static string[] EnterEmail= { "Enter your email to reset your password", "Nhập email để thiết lập lại mật khẩu của bạn" };
        public static string[] Request = { "Request", "Gửi" };
        public static string[] Cancel = { "Cancel", "Huỷ" };

        //admin change password

        public static string[] TitlePageView = { "Change Password", "Đổi Mật Khẩu" };
        public static string[] CurrentPasswordView = { "Current Password", "Password hiện tại" };
        public static string[] NewPasswordView = { "New Password", "Password mới" };
        public static string[] ConfirmPasswordView = { "Confirm Password", "Xác nhận lại password" };
        public static string[] BackToDashBoard = { "Back to Dashboard", "Trở về" };
        public static string[] Update = { "Update", "Cập nhật" };
        public static string[] PleaseLoginFirst = { "Please login to continute", "Vui lòng đăng nhập để tiếp tục" };
        public static string[] UpdatePasswordSuccessfull = { "Your password has been updated", "Mật khẩu của bạn đã được cập nhật" };
        public static string[] PasswordIsRequired = { "Password is required", "Vui lòng nhập mật khẩu" };
        public static string[] NewPasswordIsRequired = { "New Password is required", "Vui lòng nhập mật khẩu mới" };
        public static string[] NewPasswordMustSix = { "New Password must have minimum 6 characters", "Mật khẩu mới phải tối thiểu 6 kí tự" };
        public static string[] ConfirmPasswordWrong = { "Confirm password does not match", "Mật khẩu xác nhận không trùng khớp" };
        public static string[] CurrentPasswordWrong = { "Current Password is wrong", "Mật khẩu hiện tại không chính xác" };

        //admin manager
        public static string[] AddAdminSuccess = { "Admin account has been created successfully!", "Tài khoản quản trị viên đã được tạo thành công" };
        public static string[] NameIsrequired = { "Name is required", "Vui lòng nhập tên" };
        public static string[] PhoneIsrequired = { "Phone is required", "Vui lòng nhập số điện thoại" };
        public static string[] PhoneIsExist = { "Phone number is exist", "Số điện thoại đã tồn tại" };
        public static string[] EmailIsrequired = { "Email is required", "Vui lòng nhập email" };
        public static string[] EmailFormatWrong = { "Email format is wrong", "Email sai định dạng" };
        public static string[] EmailIsExist = { "Email is exist", "Email đã tồn tại" };
        public static string[] PasswordMustSix = { "Password must have minimum 6 characters", "Mật khẩu phải tối thiểu 6 kí tự" };


    }
}
