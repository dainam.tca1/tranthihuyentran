﻿using System;

namespace NacGroup.Module.Core.Models
{
    public interface ITrackingUpdate
    {
        DateTime CreatedDate { get; set; }
        DateTime UpdatedDate { get; set; }
    }
}
