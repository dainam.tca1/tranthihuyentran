﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.Collections.Generic;

namespace NacGroup.Module.Core.Models.Schema
{
    public class AppSetting : Entity
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("Key")]
        public string Key { get; set; }

        [BsonElement("Value")]
        public BsonDocument Value { get; set; }
    }
    public class Country
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string CodeProvince { get; set; }

        public IEnumerable<Country> StateList { get; set; }
    }
}
