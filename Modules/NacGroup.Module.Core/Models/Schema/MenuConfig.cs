﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace NacGroup.Module.Core.Models.Schema
{
    [BsonIgnoreExtraElements]
    public class MenuConfig : Entity
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("Text")]
        public string Text { get; set; }

        [BsonElement("Avatar")]
        public string Avatar { get; set; }

        [BsonElement("ParentId")]
        public string ParentId { get; set; }

        [BsonElement("MenuLevel")]
        public int MenuLevel { get; set; }

        [BsonElement("Url")]
        public string Url { get; set; }

        [BsonElement("Sort")]
        public int Sort { get; set; }

        public object this[string propertyName]
        {
            get => this.GetType().GetProperty(propertyName).GetValue(this, null);
            set => this.GetType().GetProperty(propertyName).SetValue(this, value, null);
        }
        public static string CreateId()
        {
           return ObjectId.GenerateNewId().ToString();
        }

    }
}
