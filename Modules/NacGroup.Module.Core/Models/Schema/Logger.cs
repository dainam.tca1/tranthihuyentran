﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace NacGroup.Module.Core.Models.Schema
{
    [BsonIgnoreExtraElements]
    public class Logger : Entity
    {
        public Logger()
        {
        }

        public Logger(LogLevel level, string message, Exception e = null, string userAgent = null, string ipAddress = null)
        {
            Date = DateTime.UtcNow;
            LogLevel = level;
            Message = message;
            ExceptionMessage = e?.Message;
            Exception = e?.ToString();
            UserAgent = userAgent;
        }

        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("Date")]
        public DateTime Date { get; set; }

        [BsonElement("Message")]
        public string Message { get; set; }

        [BsonElement("LogLevel")]
        public LogLevel LogLevel { get; set; }

        [BsonElement("ExceptionMessage")]
        public string ExceptionMessage { get; set; }

        [BsonElement("Exception")]
        public string Exception { get; set; }

        [BsonElement("UserAgent")]
        public string UserAgent { get; set; }

        [BsonElement("IpAddress")]
        public string IpAddress { get; set; }
    }

    public enum LogLevel
    {
        Error,
        Warning,
        Success
    }
}
