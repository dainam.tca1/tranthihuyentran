﻿using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using NacGroup.Infrastructure.Shared;

namespace NacGroup.Module.Core.Models.Schema
{
    [BsonIgnoreExtraElements]
    public class UserProfile : Entity
    {
        public UserProfile()
        {
            UsedCoupons = new List<UsedCoupon>();

        }

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("FirstName")]
        public string FirstName { get; set; }

        [BsonElement("LastName")]
        public string LastName { get; set; }

        private string _email;

        [BsonElement("Email")]
        public string Email
        {
            get => _email;
            set => _email = value?.ToLower();
        }

        [BsonElement("Phone")]
        public string Phone { get; set; }

        [BsonElement("PasswordHash")]
        public string PasswordHash { get; set; }

        [BsonElement("IsRootMember")]
        public bool IsRootMember { get; set; }

        [BsonElement("IsDeleted")]
        public bool IsDeleted { get; set; }

        [BsonElement("IsVerified")]
        public bool IsVerified { get; set; }

        [BsonElement("Role")]
        public RoleType Role { get; set; }

        [BsonElement("Token")]
        public string Token { get; set; }

        [BsonElement("Gender")]
        public GenderType Gender { get; set; }
        [BsonElement("UsedCoupons")]
        public List<UsedCoupon> UsedCoupons { get; set; }

        [BsonElement("Recipient")]
        public List<AddressModel> Recipient { get; set; }


        public string GeneratePassword(string password)
        {
            return (Phone + password).GetMd5Hash();
        }

        public bool IsValidPassword(string password)
        {
            return GeneratePassword(password) == PasswordHash;
        }
    }

    [BsonIgnoreExtraElements]
    public class AddressModel
    {
        public int Id { get; set; }
        public string Street { get; set; }
        public string PhoneNumber { get; set; }
        public string City { get; set; }
        public string ZipCode { get; set; }
        public string FullName { get; set; }
        public string District { get; set; }
        public string Ward { get; set; }
        public bool IsDefault { get; set; }
    }
    public enum RoleType
    {
        Customer,
        Admin
    }

    public enum GenderType
    {
        Undefined,
        Male,
        Female
    }

    [BsonIgnoreExtraElements]
    public class UsedCoupon
    {
        public string Id { get; set; }
        public string CouponCode { get; set; }
        public decimal Value { get; set; }
        public int ValueType { get; set; }
        public DateTime UsedDate { get; set; }

    }


}
