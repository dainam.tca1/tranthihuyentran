﻿using System;

namespace NacGroup.Module.Core.Models
{
    public abstract class BasePagedResult
    {
        protected BasePagedResult()
        {
            
        }
        protected BasePagedResult(int currentPage, int pageSize,int totalItemCount)
        {
            CurrentPage = currentPage;
            PageSize = pageSize;
            TotalItemCount = totalItemCount;
        }
        public int CurrentPage { get; set; }

        public int PageSize { get; set; }
       
        public int TotalItemCount { get; set; }

        public int TotalPageCount
        {
            get
            {
                var pagecount = PageSize > 0 ?(double)TotalItemCount / PageSize : 0;
                return (int) Math.Ceiling(pagecount);
            }
        }
    }
}
