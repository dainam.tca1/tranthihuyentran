﻿using System;
using System.Reflection;

namespace NacGroup.Module.Core.Models
{
    public class ModuleInfo
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string[] CopyFolders { get; set; }

        public Version Version { get; set; }

        public Assembly Assembly { get; set; }

        public bool IsBundledWithHost { get; set; }
    }
}
