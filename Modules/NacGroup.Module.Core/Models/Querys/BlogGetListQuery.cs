﻿using MediatR;
using NacGroup.Module.Blog.Models.Querys;

namespace NacGroup.Module.Core.Models.Querys
{
    public class BlogGetListQuery: IRequest<PagedResult<BlogData>>
    {
        public int Page { get; set; }
        public int PageSize { get; set; }
        public BlogGetListQuery(int page = 1, int pagesize = 15)
        {
            Page = page;
            PageSize = pagesize;
        }
    }
}
