﻿namespace NacGroup.Module.Core.Data
{
    public interface IUnitOfWork
    {
        int Commit();
    }
    public class UnitOfWork : IUnitOfWork
    {
        private IMongoContext _mongoContext;

        public UnitOfWork(IMongoContext mongoContext)
        {
            _mongoContext = mongoContext;
        }
        public int Commit()
        {
            return _mongoContext.SaveChange();
        }
    }
}
