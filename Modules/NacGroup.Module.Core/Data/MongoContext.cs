﻿using System;
using System.Collections.Generic;
using Microsoft.Extensions.Caching.Memory;
using MongoDB.Bson;
using MongoDB.Driver;
using NacGroup.Module.Core.Interfaces;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Core.Models.Schema;
using Newtonsoft.Json.Linq;
using TimeZoneConverter;

namespace NacGroup.Module.Core.Data
{
    public class MongoContext : IMongoContext
    {
        private readonly IMongoDatabase _database;
        private readonly IClientSessionHandle _session;
        private List<Action> _commandList;
        private readonly IMemoryCache _memoryCache;

        public TimeZoneInfo CurrentTimeZoneInfo { get; }

        public MongoContext(ITenantProvider tenantProvider, IMemoryCache memoryCache)
        {
            _memoryCache = memoryCache;
            _commandList = new List<Action>();
            CurrentTenant = tenantProvider.GetTenant();
            // Every command will be stored and it will be processed at SaveChanges
            var mongoClient = new MongoClient(CurrentTenant.ConnectionUrl);
            _database = mongoClient.GetDatabase(CurrentTenant.DatabaseName);
            _session = mongoClient.StartSession();

            var cacheKey = $"{CurrentTenant?.TenantName}_{ConstKey.GlobalConfigCacheKey}";
            if (_memoryCache.TryGetValue(cacheKey, out GlobalConfiguration config))
            {
                CurrentTimeZoneInfo = config.LocalTimeZoneInfo;
            }
            else
            {
                var setting = JObject.Parse(GetCollection<AppSetting>("AppSetting").Find(m => m.Key == ConstKey.GlobalWebSettingKey).FirstOrDefault().Value.ToJson());
                CurrentTimeZoneInfo = setting.HasValues
                    ? TZConvert.GetTimeZoneInfo(setting["WebsiteConfig"]["TimeZone"].ToString())
                    : TZConvert.GetTimeZoneInfo(TimeZoneInfo.Local.Id);
            }

        }

        public IMongoCollection<T> GetCollection<T>(string name)
        {
            return _database.GetCollection<T>(name);
        }

        public int SaveChange()
        {
            try
            {
                _session.StartTransaction();
                foreach (var cmd in _commandList)
                {
                    cmd();
                }

                try
                {
                    _session.CommitTransaction();
                }
                catch (Exception e)
                {
                    _commandList.Clear();
                    var s = new LoggerRepository(this);
                    s.Add(new Logger(LogLevel.Error, e.Message, e));
                    return -1;
                }
                _commandList.Clear();
                return 0;
            }
            catch (Exception e)
            {
                _commandList.Clear();
                //Session.AbortTransaction();
                var s = new LoggerRepository(this);
                s.Add(new Logger(LogLevel.Error, e.Message, e));
                return -1;
            }

        }

        public IClientSessionHandle GetSession()
        {
            return _session;
        }

        public void AddCommand(Action item)
        {
            _commandList.Add(item);
        }

        public Tenant CurrentTenant { get; }
    }

    public interface IMongoContext
    {
        IMongoCollection<T> GetCollection<T>(string name);
        int SaveChange();
        IClientSessionHandle GetSession();
        void AddCommand(Action item);
        Tenant CurrentTenant { get; }
        TimeZoneInfo CurrentTimeZoneInfo { get; }
    }
}
