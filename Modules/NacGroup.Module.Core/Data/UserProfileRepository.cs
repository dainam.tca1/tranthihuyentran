﻿using MongoDB.Driver;
using NacGroup.Infrastructure.Shared;
using NacGroup.Module.Core.Interfaces;
using NacGroup.Module.Core.Models.Schema;
using System.Collections.Generic;

namespace NacGroup.Module.Core.Data
{
    public interface IUserProfileRepository : IRepository<UserProfile>
    {
    }

    public class UserProfileRepository : MongoRepository<UserProfile>, IUserProfileRepository, IMigrationRepository
    {
        public UserProfileRepository(IMongoContext context) : base(context)
        {
        }


        public void Migration()
        {
            DbSet.Indexes.CreateOne(model: new CreateIndexModel<UserProfile>("{Email:1}", new CreateIndexOptions { Unique = true }));
            DbSet.Indexes.CreateOne(model: new CreateIndexModel<UserProfile>("{Phone:1}", new CreateIndexOptions { Unique = true }));
        }

        public void Seed()
        {
            if (DbSet.EstimatedDocumentCount() == 0)
            {
                var user = new UserProfile
                {
                    Email = "nacgroup.vn@gmail.com",
                    FirstName = "Admin",
                    LastName = null,
                    Gender = GenderType.Male,
                    IsRootMember = true,
                    IsVerified = true,
                    Phone = "admin",
                    Role = RoleType.Admin,
                    Recipient = new List<AddressModel>(),
           
                };
                user.PasswordHash = (user.Phone + "123456").GetMd5Hash();
                Add(user);
                Context.SaveChange();
            }
        }

        //public override int Add(UserProfile entity)
        //{
        //    try
        //    {
        //        DbSet.InsertOne(entity);
        //        return 0;
        //    }
        //    catch
        //    {
        //        return -1;
        //    }
        //}
    }
}
