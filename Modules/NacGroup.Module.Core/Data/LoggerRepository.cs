﻿using NacGroup.Module.Core.Interfaces;
using NacGroup.Module.Core.Models.Schema;

namespace NacGroup.Module.Core.Data
{
    public interface ILoggerRepository : IRepository<Logger>
    {
        
    }

    public class LoggerRepository : MongoRepository<Logger>, ILoggerRepository
    {
        public LoggerRepository(IMongoContext context) : base(context)
        {
        }

        public override int Add(Logger entity)
        {
            try
            {
                DbSet.InsertOne(entity);
                return 0;
            }
            catch
            {
                return -1;
            }

        }
    }
}
