﻿using System.Collections.Generic;
using System.Linq;
using MongoDB.Driver;
using NacGroup.Module.Core.Interfaces;
using NacGroup.Module.Core.Models.Schema;

namespace NacGroup.Module.Core.Data
{
    public interface IMenuConfigRepository : IRepository<MenuConfig>
    {
        IEnumerable<MenuConfig> GetChildrenMenus(string parentId);
    }

    public class MenuConfigRepository : MongoRepository<MenuConfig>, IMenuConfigRepository, IMigrationRepository
    {
        public MenuConfigRepository(IMongoContext context) : base(context)
        {

        }
        public void Migration()
        {
            DbSet.Indexes.CreateOne(model: new CreateIndexModel<MenuConfig>("{ParentId:1}"));
        }

        public void Seed()
        {
            if (!Get().Any())
            {
                var items = new List<MenuConfig>();

                items.Add(new MenuConfig
                {
                    Text = "Main Menu",
                    ParentId = null,
                    MenuLevel = 0,
                    Sort = 0,
                    Id = MenuConfig.CreateId()
                });
                items.Add(new MenuConfig
                {
                    Text = "Home",
                    ParentId = items[0].Id,
                    MenuLevel = 1,
                    Sort = 0,
                    Id = MenuConfig.CreateId()
                });
                items.Add(new MenuConfig
                {
                    Text = "Wine Tasting",
                    ParentId = items[0].Id,
                    MenuLevel = 1,
                    Sort = 2,
                    Id = MenuConfig.CreateId()
                });
                items.Add(new MenuConfig
                {
                    Text = "Services",
                    ParentId = items[0].Id,
                    MenuLevel = 1,
                    Sort = 1,
                    Id = MenuConfig.CreateId()
                });
                items.Add(
                    new MenuConfig
                    {
                        Text = "Service 1",
                        ParentId = items[3].Id,
                        MenuLevel = 2,
                        Sort = 1,
                        Id = MenuConfig.CreateId()
                    });
                items.Add(
                    new MenuConfig
                    {
                        Text = "Service 2",
                        ParentId = items[3].Id,
                        MenuLevel = 2,
                        Sort = 2,
                        Id = MenuConfig.CreateId()
                    });

                foreach (var item in items)
                {
                    Add(item);
                }

                Context.SaveChange();
            }
        }

        public IEnumerable<MenuConfig> GetChildrenMenus(string parentId)
        {
            return Get(m => m.ParentId == parentId);
        }
    }
}
