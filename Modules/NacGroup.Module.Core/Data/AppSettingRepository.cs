﻿using System.Collections.Generic;
using System.IO;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using NacGroup.Module.Core.Interfaces;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Core.Models.CmsModel;
using NacGroup.Module.Core.Models.Schema;
using NacGroup.Module.Core.Models.ViewModels;
using Newtonsoft.Json;
using System.Linq;

namespace NacGroup.Module.Core.Data
{
    public interface IAppSettingRepository : IRepository<AppSetting>
    {
        PagedResult<AppSetting> FilterThemeConfig(string title, int page, int pagesize);

        int DeleteByKey(string key);
        PagedResult<AppSetting> FilterAppConfig(string key, int page, int pagesize);
    }

    public class AppSettingRepository : MongoRepository<AppSetting>, IAppSettingRepository, IMigrationRepository
    {
        public AppSettingRepository(IMongoContext context) : base(context)
        {
        }

        public int DeleteByKey(string key)
        {
            try
            {
                var result = DbSet.DeleteOne(mbox => mbox.Key == key);
                return result.DeletedCount != 0 ? 0 : -1;
            }
            catch
            {
                return -1;
            }
        }

        public PagedResult<AppSetting> FilterAppConfig(string key, int page, int pagesize)
        {
            var filter = Builders<AppSetting>.Filter;
            var filterdefine = filter.Where(m => !m.Key.StartsWith("THEME_") && m.Key != ConstKey.GlobalWebSettingKey && m.Key != ConstKey.GlobalThemeKey);
            if (!string.IsNullOrEmpty(key))
            {
                filterdefine = filterdefine & filter.Where(m => m.Key.Contains(key));
            }
            var totalItem = DbSet.CountDocuments(filterdefine);
            var all = DbSet.Find(filterdefine).Skip((page - 1) * pagesize).Limit(pagesize);
            return new PagedResult<AppSetting>(all.ToList(), page, pagesize, (int)totalItem);
        }

        public PagedResult<AppSetting> FilterThemeConfig(string key, int page, int pagesize)
        {
            var filter = Builders<AppSetting>.Filter;
            var filterdefine = filter.Where(m => m.Key.StartsWith("THEME_"));
            if (!string.IsNullOrEmpty(key))
            {
                filterdefine = filterdefine & filter.Where(m => m.Key.Contains(key));
            }
            var totalItem = DbSet.CountDocuments(filterdefine);
            var all = DbSet.Find(filterdefine).Skip((page - 1) * pagesize).Limit(pagesize);
            return new PagedResult<AppSetting>(all.ToList(), page, pagesize, (int)totalItem);
        }

        public virtual void Migration()
        {
            DbSet.Indexes.CreateOne(model: new CreateIndexModel<AppSetting>("{Key:1}", new CreateIndexOptions { Unique = true }));
        }

        public virtual void Seed()
        {
            if (DbSet.EstimatedDocumentCount() == 0)
            {
                var globalthemevalue = BsonSerializer.Deserialize<BsonDocument>(JsonConvert.SerializeObject(
                    new ThemeConfigModel
                    {
                        Id = ConstKey.GlobalThemeKey,
                        Name = "Global Theme Config",
                        Slug = ConstKey.GlobalThemeKey,
                        ThemeType = ThemeType.Group,
                        Data = new List<dynamic>
                        {
                            new ThemeConfigImageItem
                            {
                                Name = "Header Logo",
                                Src = "/resources/upload/banner/2019-6/logo-2.png"
                            },
                            new ThemeConfigImageItem
                            {
                                Name = "Favicon",
                                Src = "/resources/upload/banner/2019-6/logo-2.png"
                            },
                            new ThemeConfigImageItem
                            {
                                Name = "Footer Logo",
                                Src = "/resources/upload/banner/2019-6/logo-2.png"
                            },
                            new ThemeConfigImageItem
                            {
                                Name = "Facebook Share Image",
                                Src = "/resources/upload/banner/2019-6/logo-2.png"
                            }
                        }
                    }));

                var globalThemeSetting = new AppSetting
                {
                    Key = ConstKey.GlobalThemeKey,
                    Value = globalthemevalue
                };
                Add(globalThemeSetting);
                //Thêm global config
                var globalwebsettingvalue = BsonSerializer.Deserialize<BsonDocument>(JsonConvert.SerializeObject(new
                {
                    SeoConfig = new
                    {
                        MetaTitle = "Royal Beauty",
                        MetaDescription = "Royal Beauty",
                        MetaKeyword = "Royal Beauty",
                        GoogleAnalytic = string.Empty,
                        WebmasterTool = string.Empty,
                        Robot = string.Empty
                    },
                    WebInfomation = new
                    {
                        WebsiteName = "Royal Beauty",
                        WebsiteURL = "https://royal.ecsgroup.com.vn",
                        WebsiteEmail = "royal.ecsgroup.com.vn@gmail.com",
                        WebsiteNotification = "royal.ecsgroup.com.vn@gmail.com",
                        WebsitePhone = "0868854148",
                        WebsiteAddress = "12685 Old Plantation Ln,<br> Orlando, FL 32824",
                        WebsiteGoogleMap = string.Empty,
                        WebsiteFacebook = string.Empty,
                        WebsiteInstagram = string.Empty,
                        WebsiteYoutube = string.Empty,
                        WebsiteTwitter = string.Empty,
                        WebsitePinterest = string.Empty,
                        WebsiteLicense = "Copyright © 2018 WebNails & DK Mobility LLC, All Rights Reserved."
                    },
                    WebsiteConfig = new
                    {
                        TimeZone = "Eastern Standard Time",
                        DateTimeCShapeFormat = "MM/dd/yyyy hh:mm:ss tt",
                        DateCShapeFormat = "MM/dd/yyyy",
                        CustomizeCss = string.Empty,
                        CustomizeJs = string.Empty,
                        GcaptchaPublic = string.Empty,
                        GcaptchaPrivate = string.Empty,
                    }
                }));
                var globalWebsetting = new AppSetting
                {
                    Key = ConstKey.GlobalWebSettingKey,
                    Value = globalwebsettingvalue
                };
                Add(globalWebsetting);
                Context.SaveChange();
            }

            
            var accoutKitSetting = GetSingle(m => m.Key == ConstKey.AccountKitSettingKey);
            if (accoutKitSetting == null)
            {
                accoutKitSetting = new AppSetting
                {
                    Key = ConstKey.AccountKitSettingKey,
                    Value = BsonSerializer.Deserialize<BsonDocument>(JsonConvert.SerializeObject(new FacebookAccountKitSettingModel
                    {
                        AccountKitId = "2f71d1f7a8c82f9ac46814a744a238dc",
                        AccountKitSecret = "e56f0a451d9c69b6ee8267e6e1f39fb5",
                        CallBackUrl = "{0}/account/login/facebookkitcallback",
                        Version = "v1.1",
                        ClientId = "183105709284467"
                    }))
                };
                Add(accoutKitSetting);
                Context.SaveChange();
            }
            var countrySetting = GetSingle(m => m.Key == ConstKey.CountrySettingKey);
           
            if (countrySetting == null)
            {
                List<Country> state_List = new List<Country>();
                using (StreamReader r = new StreamReader("./wwwroot/citylist.json"))
                {
                    string json = r.ReadToEnd();
                    List<dynamic> items = JsonConvert.DeserializeObject<List<dynamic>>(json);

                    for (var i = 0; i < items.Count(); i++)
                    {
                        if (items[i].code == "US")
                        {
                            state_List.Add(new Country
                            {
                                Name = items[i].name,
                                Code = items[i].code,
                                CodeProvince = items[i].codeProvince,
                                StateList = new List<Country>()
                            }); ;
                        }  
                    }
                }
                List<Country> countryList = new List<Country>();
                using (StreamReader r = new StreamReader("./wwwroot/country.json"))
                {
                    string json = r.ReadToEnd();
                    List<dynamic> items = JsonConvert.DeserializeObject<List<dynamic>>(json);
                    for (var i = 0; i < items.Count(); i++)
                    {
                        if (items[i].code == "US")
                        {
                            countryList.Add(new Country
                            {
                                Name = items[i].name,
                                Code = items[i].code,
                                StateList = new List<Country>()
                            });
                        }
                        
                    }

                }
                if (countryList.Count() > 0)
                {
                    foreach (var item in countryList)
                    {
                        item.StateList = state_List.Where(m => m.Code == item.Code);

                    }
                }
                countrySetting = new AppSetting
                {
                    Key = ConstKey.CountrySettingKey,
                    Value = BsonSerializer.Deserialize<BsonDocument>(JsonConvert.SerializeObject(new {
                        Data = countryList
                    }))
                };
                Add(countrySetting);
                Context.SaveChange();
            }
        }
    }
}
