﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using MongoDB.Driver;
using NacGroup.Module.Core.Interfaces;
using NacGroup.Module.Core.Models;

namespace NacGroup.Module.Core.Data
{
    public class MongoRepository<T> : IRepository<T> where T : Entity
    {
        protected readonly IMongoContext Context;
        protected readonly IMongoCollection<T> DbSet;
        public MongoRepository(IMongoContext context)
        {
            Context = context;
            DbSet = Context.GetCollection<T>(typeof(T).Name);
        }

        public virtual int Add(T entity)
        {
            try
            {
                if (entity is ITrackingUpdate changeOrAddredItem)
                {
                    var timeNow = TimeZoneInfo.ConvertTime(DateTime.Now, Context.CurrentTimeZoneInfo);
                    changeOrAddredItem.CreatedDate = timeNow.ToLocalTime();
                    changeOrAddredItem.UpdatedDate = timeNow.ToLocalTime();
                }
                Context.AddCommand(() => DbSet.InsertOne(Context.GetSession(), entity));
                return 0;
            }
            catch
            {
                return -1;
            }
        }


        public virtual int Update(string id, T entity)
        {
            try
            {
                if (entity is ITrackingUpdate changeOrAddedItem)
                {
                    var timeNow = TimeZoneInfo.ConvertTime(DateTime.Now, Context.CurrentTimeZoneInfo);
                    changeOrAddedItem.UpdatedDate = timeNow.ToLocalTime();
                }
                Context.AddCommand( () =>  DbSet.ReplaceOne(Context.GetSession(), Builders<T>.Filter.Eq("Id", id), entity));

                return 0;
            }
            catch
            {
                return -1;
            }
        }

        public virtual int Delete(string id)
        {
            try
            {
                var filter = Builders<T>.Filter.Eq("Id", id);
                if (typeof(ISoftDelete).IsAssignableFrom(typeof(T)))
                {
                    var updateDefine = Builders<T>.Update.Set("IsDeleted", true);
                    Context.AddCommand( () =>  DbSet.UpdateOne(Context.GetSession(), filter, updateDefine));
                    return 0;
                }

                Context.AddCommand( () =>  DbSet.DeleteOne(Context.GetSession(), filter));
                return 0;

            }
            catch
            {
                return -1;
            }
        }


        public virtual IEnumerable<T> Get()
        {
            var all = DbSet.Find(Builders<T>.Filter.Empty);
            return all.ToList();
        }

        public virtual IEnumerable<T> Get(Expression<Func<T, bool>> predicate)
        {
            var all = DbSet.Find(predicate);
            return all.ToList();
        }

        public virtual PagedResult<T> Get(int page, int pagesize)
        {
            var totalItem = DbSet.CountDocuments(Builders<T>.Filter.Empty);
            var all = DbSet.Find(Builders<T>.Filter.Empty).Skip((page - 1) * pagesize).Limit(pagesize);
            return new PagedResult<T>(all.ToList(), page, pagesize, (int)totalItem);
        }

        public virtual PagedResult<T> Get(int page, int pagesize, Expression<Func<T, bool>> predicate)
        {

            var totalItem = DbSet.CountDocuments(predicate);
            var all = DbSet.Find(predicate).Skip((page - 1) * pagesize).Limit(pagesize);
            return new PagedResult<T>(all.ToList(), page, pagesize, (int)totalItem);
        }

        public virtual T GetSingle(Expression<Func<T, bool>> predicate)
        {
            try
            {
                var all = DbSet.Find(predicate);
                return all.FirstOrDefault();
            }
            catch
            {
                return null;
            }

        }

        public virtual T GetById(string id)
        {
            try
            {
                var all = DbSet.Find(Builders<T>.Filter.Eq("Id", id));
                return all.FirstOrDefault();
            }
            catch
            {
                return null;
            }

        }

    }


}
