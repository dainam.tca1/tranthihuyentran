﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using NacGroup.Module.Core.Data;
using NacGroup.Module.Core.Interfaces;
using NacGroup.Module.Core.Services;
using IHostingEnvironment = Microsoft.AspNetCore.Hosting.IHostingEnvironment;

namespace NacGroup.Module.Core
{
    public class ModuleInitializer : IModuleInitializer
    {
        public void ConfigureServices(IServiceCollection services)
        {

        
            services.AddSingleton<ISendMailService, SendMailService>();
          
           
            services.AddTransient<IViewRenderService, ViewRenderService>();
            services.AddTransient<IUserProfileRepository, UserProfileRepository>();
          

            services.AddTransient<IUserProfileService, UserProfileService>();
           
           
            services.AddTransient<IUserProfileCmsService, UserProfileCmsService>();
        
            services.AddTransient<ILoggerRepository, LoggerRepository>();

            services.AddTransient<IAppSettingRepository, AppSettingRepository>();
            services.AddTransient<IThemeConfigService, ThemeConfigService>();
            services.AddTransient<IWebsiteConfigService, WebsiteConfigService>();
            services.AddTransient<IMenuConfigRepository, MenuConfigRepository>();
            services.AddTransient<IMenuConfigService, MenuConfigService>();
            services.AddTransient<IGettingGlobalConfigService, GettingGlobalConfigService>();
            services.AddTransient<IViewRenderService, ViewRenderService>();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {

        }
    }
}
