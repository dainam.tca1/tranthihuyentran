﻿using Microsoft.AspNetCore.Mvc;
namespace NacGroup.Module.Core.Controllers
{
    [Route("/error/{action=Index}")]
    public class ErrorPageController : Controller
    {
        public IActionResult Page404()
        {
            return View();
        }
    }
}
