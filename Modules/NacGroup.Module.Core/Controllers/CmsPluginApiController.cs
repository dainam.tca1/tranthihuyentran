﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using NacGroup.Infrastructure.Shared;
using NacGroup.Module.Core.Interfaces;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Core.Services;

namespace NacGroup.Module.Core.Controllers
{
    
    [Route("cms/api/cmsplugin/{action=Index}")]
    public class CmsPluginApiController : Controller
    {
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly Tenant _tenant;
        private readonly CurrencyType _currencyType;

        public CmsPluginApiController(IHostingEnvironment hostingEnvironment, ITenantProvider tenantProvider, IGettingGlobalConfigService gettingGlobalConfigService)
        {
            _hostingEnvironment = hostingEnvironment;
            _tenant = tenantProvider.GetTenant();
            _currencyType = gettingGlobalConfigService.GetConfiguration().Currency;
        }

        [Authorize(Roles = "Admin", Policy = "TenantAuthorize")]
        [HttpPost]
        public ActionResult<ApiResponseModel> UploadImage()
        {
            try
            {
                var files = Request.Form.Files;
                if (!files.Any())
                    return new ApiResponseModel("error", "Please select image");

                if (files.Any(m => !m.ContentType.StartsWith("image")))
                    return new ApiResponseModel("error", "Image format is wrong");

                if (files.Any(m => m.Length > 2 * 1024 * 1024))
                    return new ApiResponseModel("error", "Image file must be less than 2MB");


                var imageFolder = Path.Combine(_hostingEnvironment.WebRootPath, $"tenants/{_tenant.TenantName.ToLower()}/upload/banner");

                if (!Directory.Exists(imageFolder))
                {
                    Directory.CreateDirectory(imageFolder);
                }

                var fileListResult = new List<string>();

                foreach (var file in files)
                {
                    //var newFileName = Path.GetFileName(file.FileName);
                    var filenamewithoutext = Path.GetFileNameWithoutExtension(file.FileName).ToUrl();
                    var extentionfile = Path.GetExtension(file.FileName);
                    var filename = $"{filenamewithoutext}{extentionfile}";


                    var folderimg = Path.Combine(imageFolder, DateTime.Today.Year + "-" + DateTime.Today.Month);

                    if (!Directory.Exists(folderimg))
                    {
                        Directory.CreateDirectory(folderimg);
                    }

                    var path = Path.Combine(folderimg, filename);//duong dan luu file

                    if (System.IO.File.Exists(path))
                    {
                        var counter = 2;
                        while (System.IO.File.Exists(path))
                        {
                            // if a file with this name already exists,
                            // prefix the filename with a number.
                            filename = $"{filenamewithoutext}-{counter}{extentionfile}";
                            path = Path.Combine(folderimg, filename);
                            counter++;
                        }
                    }
                    using (FileStream fs = System.IO.File.Create(path))
                    {
                        file.CopyTo(fs);
                        fs.Flush();
                    }
                    fileListResult.Add($"/tenants/{_tenant.TenantName.ToLower()}/upload/banner/{DateTime.Today.Year}-{DateTime.Today.Month}/{filename}");
                }

                return new ApiResponseModel("success", "Upload sucessfully", fileListResult);

            }
            catch 
            {
                return new ApiResponseModel("error", "Error");
            }
            

        }

        [Authorize(Roles = "Admin", Policy = "TenantAuthorize")]
        [HttpPost]
        public IActionResult UploadImageCk()
        {
            try
            {
                var files = Request.Form.Files;
                if (!files.Any())
                    return Json(new { uploaded = 0, error = "Please select image" });
               
                if (files.Any(m => !m.ContentType.StartsWith("image")))
                    return Json(new { uploaded = 0, error = "Image format is wrong" });


                if (files.Any(m => m.Length > 2 * 1024 * 1024))
                    return Json(new { uploaded = 0, error = "Image file must be less than 2MB" });


                var imageFolder = Path.Combine(_hostingEnvironment.WebRootPath, $"tenants/{_tenant.TenantName.ToLower()}/upload/ckeditors");

                if (!Directory.Exists(imageFolder))
                {
                    Directory.CreateDirectory(imageFolder);
                }

                var fileListResult = new List<string>();

                var file = files.FirstOrDefault();


                var filenamewithoutext = Path.GetFileNameWithoutExtension(file.FileName).ToUrl();
                var extentionfile = Path.GetExtension(file.FileName);
                var filename = $"{filenamewithoutext}{extentionfile}";


                var folderimg = Path.Combine(imageFolder, DateTime.Today.Year + "-" + DateTime.Today.Month);

                if (!Directory.Exists(folderimg))
                {
                    Directory.CreateDirectory(folderimg);
                }


                var path = Path.Combine(folderimg, filename);//duong dan luu file

                if (System.IO.File.Exists(path))
                {
                    var counter = 2;
                    while (System.IO.File.Exists(path))
                    {
                        // if a file with this name already exists,
                        // prefix the filename with a number.
                        filename = $"{filenamewithoutext}-{counter}{extentionfile}";
                        path = Path.Combine(folderimg, filename);
                        counter++;
                    }
                }
                using (FileStream fs = System.IO.File.Create(path))
                {
                    file.CopyTo(fs);
                    fs.Flush();
                }
                fileListResult.Add($"/tenants/{_tenant.TenantName.ToLower()}/upload/ckeditors/{DateTime.Today.Year}-{DateTime.Today.Month}/{filename}");

                return Json(new { uploaded = 1, fileName = filename, url = fileListResult.FirstOrDefault() });
            }
            catch
            {
                return Json(new { uploaded = 0, error = StaticMessage.GlobalErrorMessage });
            }
        }

      

        public ActionResult<List<Features>> GetFeatures()
        {
            return _tenant.FeatureList.ToList();
        }

        public ActionResult<CurrencyType> GetCurrentType()
        {
            return _currencyType;
        }
      
    }
}