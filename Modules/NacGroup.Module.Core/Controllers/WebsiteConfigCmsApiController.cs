﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Core.Services;
using Newtonsoft.Json.Linq;

namespace NacGroup.Module.Core.Controllers
{
    [Authorize(Roles = "Admin", Policy = "TenantAuthorize")]
    [Route("cms/api/websiteconfig/{action=Index}")]
    public class WebsiteConfigCmsApiController : Controller
    {
        private readonly  IWebsiteConfigService _websiteConfigService;
        public WebsiteConfigCmsApiController(IWebsiteConfigService websiteConfigService)
        {
            _websiteConfigService = websiteConfigService;
        }

        [HttpGet]
        public ActionResult<ApiResponseModel> Index()
        {
            var webSetting = _websiteConfigService.GetGlobalConfig();
            return webSetting == null ? new ApiResponseModel("error", StaticMessage.DataNotFound) : new ApiResponseModel("success", webSetting.Value);
        }

        [HttpGet]
        public ActionResult<ApiResponseModel> GetTimeZoneList()
        {
            return new ApiResponseModel("success", TimeZoneInfo.GetSystemTimeZones().Select(m=>new{m.Id,m.DisplayName}).ToList());
        }

        [HttpPost]
        public ActionResult<ApiResponseModel> Update([FromBody] JObject model)
        {
            return _websiteConfigService.UpdateGlobalConfig(model);
        }
        

    }
}