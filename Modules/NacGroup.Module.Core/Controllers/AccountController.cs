﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.Caching.Memory;
using NacGroup.Module.Core.Extensions;
using NacGroup.Module.Core.Interfaces;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Core.Models.CmsModel;
using NacGroup.Module.Core.Models.Schema;
using NacGroup.Module.Core.Models.ViewModels;
using NacGroup.Module.Core.Services;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace NacGroup.Module.Core.Controllers
{
    [Route("account/{action=Index}", Order = 1)]
    public class AccountController : Controller
    {
        private readonly IUserProfileService _userProfileService;

        private readonly IWebsiteConfigService _websiteConfigService;
        private readonly HttpClient _httpClient;
        private readonly Tenant _tenant;
        private readonly GlobalConfiguration _globalConfiguration;
        private readonly IMemoryCache _memoryCache;
        public AccountController(IMemoryCache memoryCache,IGettingGlobalConfigService gettingGlobalConfigService, IWebsiteConfigService websiteConfigService, IHttpClientFactory httpClientFactory, ITenantProvider tenantProvider, IUserProfileService userProfileService)
        {
            _memoryCache = memoryCache;
            _websiteConfigService = websiteConfigService;
            _userProfileService = userProfileService;

            _httpClient = httpClientFactory.CreateClient();
            _tenant = tenantProvider.GetTenant();
            _globalConfiguration = gettingGlobalConfigService.GetConfiguration();
          
        }

        [Authorize(Policy = "TenantAuthorize")]
        [Route("/accountapp/{id?}/{param?}/{param2?}")]
        public IActionResult Index(string id)
        {
            return View();
        }



        [Authorize(Policy = "TenantAuthorize")]
        public IActionResult OrderHistory()
        {

            throw new Exception();
        }

        public IActionResult LoginAdmin()
        {
            if (!string.IsNullOrEmpty(Request.Query["ReturnUrl"].ToString()))
            {
                HttpContext.Session.Set(ConstKey.LoginReturnUrlSessionKey, Request.Query["ReturnUrl"].ToString());
            }
            return View();
        }

        [HttpPost]
        public ActionResult<ApiResponseModel> Login(LoginCmsModel model)
        {
            if (User.Identity.IsAuthenticated)
            {
                HttpContext.SignOutAsync();
            }

            var isAuthenticatedStatus = _userProfileService.IsAuthenticated(model.UserName.ToLower(), model.Password);
            if (isAuthenticatedStatus.status == "error")
                return isAuthenticatedStatus;

            UserProfile userProfile = JsonConvert.DeserializeObject<UserProfile>(
                JsonConvert.SerializeObject(isAuthenticatedStatus.data));

            var result = AddCookieLogin(userProfile.FirstName, userProfile.LastName, userProfile.Email,
                userProfile.Phone, userProfile.Id, userProfile.Role
            );

            if (!result)
                return new ApiResponseModel("error", StaticMessage.GlobalErrorMessage);

            return new ApiResponseModel("success", "Login successfully", new { ReturnUrl = string.IsNullOrEmpty(Request.Query["ReturnUrl"].ToString()) ? HttpContext.Session.Get<string>(ConstKey.LoginReturnUrlSessionKey) : Request.Query["ReturnUrl"].ToString() });
        }

        public IActionResult Login()
        {
            if (!string.IsNullOrEmpty(Request.Query["ReturnUrl"].ToString()))
            {
                HttpContext.Session.Set(ConstKey.LoginReturnUrlSessionKey, Request.Query["ReturnUrl"].ToString());
            }
            FacebookAccountKitSettingModel accountKitSetting = JsonConvert.DeserializeObject<FacebookAccountKitSettingModel>(JsonConvert.SerializeObject(_websiteConfigService.GetByKey(ConstKey.AccountKitSettingKey).Value));
            return View(accountKitSetting);
        }

        private bool AddCookieLogin(string firstName, string lastName, string email, string phone, string userId, RoleType role, bool remember = true)
        {
            try
            {
                var claims = new List<Claim>
                {
                    new Claim(ClaimTypes.GivenName,firstName + " "  + lastName),
                    new Claim(ClaimTypes.Email, email),
                    new Claim(ClaimTypes.MobilePhone, phone),
                    new Claim(ClaimTypes.Name, userId),
                    new Claim(ClaimTypes.NameIdentifier, _tenant.TenantName),
                    new Claim(ClaimTypes.Role,role.ToString())
                };

                // create identity
                ClaimsIdentity identity = new ClaimsIdentity(claims, "cookie", ClaimTypes.Name, ClaimTypes.Role);


                // create principal
                ClaimsPrincipal principal = new ClaimsPrincipal(identity);

                // sign-in
                HttpContext.SignInAsync(
                    scheme: CookieAuthenticationDefaults.AuthenticationScheme,
                    principal: principal,
                    properties: new AuthenticationProperties
                    {
                        IsPersistent = remember
                        //IsPersistent = true, // for 'remember me' feature
                        //ExpiresUtc = DateTime.UtcNow.AddMinutes(1)
                    });
                return true;
            }
            catch (Exception e)
            {
                return false;
            }

        }

        [HttpPost]
        public ActionResult<ApiResponseModel> Profile(UpdateProfileAccountViewModel model)
        {
            if (User.Identity.IsAuthenticated == false)
            {
                return new ApiResponseModel("error", "Please login to continute");
            }
            //get User ID
            model.Id = User.Identity.Name;
            var profileResult = _userProfileService.UpdateProfile(model);
            if (profileResult.status == "error")
                return profileResult;
            return new ApiResponseModel("success",
                "Your profile has been updated");
        }

        public IActionResult Profile()
        {
            if (!User.Identity.IsAuthenticated)
                return Unauthorized();
            //check login
            var profile = _userProfileService.GetById(User.Identity.Name);
            var model = new UpdateProfileAccountViewModel();
            if (profile != null)
            {
                model.FirstName = profile.FirstName;
                model.LastName = profile.LastName;
                model.Phone = profile.Phone;
                model.Email = profile.Email;
                model.Recipient = profile.Recipient;
               
            }
            return Ok(model);
        }

        public IActionResult GetCountry()
        {
            if (!User.Identity.IsAuthenticated)
                return Unauthorized();
            var profile = _userProfileService.GetById(User.Identity.Name);
            if (profile == null)
            {
                return null;
            }
            var cacheKey = $"_{ConstKey.CountryCacheKey}";
            if (_memoryCache.TryGetValue(cacheKey, out List<Country> result)) return Ok(result);
         
            result = _websiteConfigService.GetByKey(ConstKey.CountrySettingKey).Value["Data"].ToObject<List<Country>>();

            if (result == null)
            {
                return null;
            }
            _memoryCache.Set(cacheKey, result, DateTimeOffset.Now.AddDays(1));
            return Ok(result);
        }

      

        public IActionResult VerifyAccount(string code, string acid)
        {
            var result = _userProfileService.ActivateAccount(code, acid);
            if (result.status == "error")
                return View("AccountMessage", new
                {
                    Title = "Error Activate Account",
                    Message =
                        $"<i class='demo-icon ecs-cancel' style='color:red'></i> Opps! Your activation link is wrong, please try again.",
                    ButtonLink = "/",
                    ButtonMessage = "Back To Home",
                });
            return View("AccountMessage", new
            {
                Title = "Account Activate Successfully",
                Message =
                    $"<i class='demo-icon ecs-check' style='color: green;'></i> Your account has been activated successfully!",
                ButtonLink = "/account/login",
                ButtonMessage = "Go To Login",
            });
        }


        [Route("/account/login/facebookkitcallback")]
        public IActionResult LoginFacebookKitCallback(string code)
        {
            if (!string.IsNullOrEmpty(Request.Query["ReturnUrl"].ToString()))
            {
                HttpContext.Session.Set(ConstKey.LoginReturnUrlSessionKey, Request.Query["ReturnUrl"].ToString());
            }

            var errorMessageModel = new
            {
                Title = "SMS Login Error",
                Message =
                    $"<i class='demo-icon ecs-cancel' style='color:red'></i> Can't activate account via SMS, please try again.",
                ButtonLink = "/account/login",
                ButtonMessage = "Back To Login",
            };

            if (string.IsNullOrEmpty(code))
                return View("AccountMessage", errorMessageModel);

            FacebookAccountKitSettingModel accountKitSetting = JsonConvert.DeserializeObject<FacebookAccountKitSettingModel>(JsonConvert.SerializeObject(_websiteConfigService.GetByKey(ConstKey.AccountKitSettingKey).Value));

            var appAccessToken = new List<string>
                    {"AA", accountKitSetting.ClientId, accountKitSetting.AccountKitSecret}
                .Join("|");
            var paramObject = new
            {
                GrantType = "authorization_code",
                Code = code,
                AccessToken = appAccessToken
            };
            var tokenExchangeUrl = $"{accountKitSetting.TokenExchangeBaseUrl}?grant_type={paramObject.GrantType}&code={paramObject.Code}&access_token={paramObject.AccessToken}"; ;
            var tokenResponseResult = _httpClient.GetAsync(tokenExchangeUrl).Result;
            if (!tokenResponseResult.IsSuccessStatusCode)
                return View("AccountMessage", errorMessageModel);

            var tokenResponseJson =
                JsonConvert.DeserializeObject<JObject>(tokenResponseResult.Content.ReadAsStringAsync().Result);

            var responseAccountKitInfo = _httpClient
                .GetAsync($"{accountKitSetting.MeEndpointBaseUrl}?access_token={tokenResponseJson["access_token"]}")
                .Result;
            if (!responseAccountKitInfo.IsSuccessStatusCode)
                return View("AccountMessage", errorMessageModel);

            var accountkitinfo = JsonConvert.DeserializeObject<JObject>(responseAccountKitInfo.Content.ReadAsStringAsync().Result);

            var oldUser = _userProfileService.GetByPhone(accountkitinfo["phone"]["national_number"].ToString());

            if (oldUser == null)
            {
                HttpContext.Session.Set(ConstKey.FacebookUserKitSessionKey, new FacebookUserKit { Phone = accountkitinfo["phone"]["national_number"].ToString() });
                return View("ActivateAccountKit");
            }

            var result = AddCookieLogin(oldUser.FirstName, oldUser.LastName, oldUser.Email, oldUser.Phone, oldUser.Id, oldUser.Role);
            if (!result) return View("AccountMessage", errorMessageModel);

            var urlRedirect = string.IsNullOrEmpty(Request.Query["ReturnUrl"].ToString())
                ? HttpContext.Session.Get<string>(ConstKey.LoginReturnUrlSessionKey)
                : Request.Query["ReturnUrl"].ToString();

            return Redirect(string.IsNullOrEmpty(urlRedirect) ? "/account" : urlRedirect);
        }

        [HttpPost]
        public ActionResult<ApiResponseModel> ConfirmAccountKit(RegisterAccountViewModel model)
        {
            var userKit = HttpContext.Session.Get<FacebookUserKit>(ConstKey.FacebookUserKitSessionKey);
            model.Phone = userKit?.Phone;
            var confirmAccountKitResult = _userProfileService.ConfirmAccountKit(model);
            if (confirmAccountKitResult.status == "error")
                return confirmAccountKitResult;

            var newUser = _userProfileService.GetByPhone(model.Phone);
            if (newUser != null)
            {
                AddCookieLogin(newUser.FirstName, newUser.LastName, newUser.Email, newUser.Phone, newUser.Id, newUser.Role);
            }

            var urlRedirect = HttpContext.Session.Get<string>(ConstKey.LoginReturnUrlSessionKey);

            return new ApiResponseModel("success",
                "Your account has been created successfully!", new { ReturnUrl = string.IsNullOrEmpty(urlRedirect) ? "/account" : urlRedirect });
        }


        public IActionResult ForgotPassword()
        {
            return View();
        }

        [HttpPost]
        public ActionResult<ApiResponseModel> ForgotPassword(string email)
        {
            var resetPasswordTime = HttpContext.Session.Get<DateTime>(ConstKey.ResetPasswordTimeSessionKey);
            if ((DateTime.UtcNow - resetPasswordTime).Minutes < 5)
                return new ApiResponseModel("error", "You can send 1 request per 5 minutes");
            var forGotPasswordResult = _userProfileService.ForgotPassword(email);

            if (forGotPasswordResult.status == "error")
                return forGotPasswordResult;

            HttpContext.Session.Set(ConstKey.ResetPasswordTimeSessionKey, DateTime.UtcNow);
            return forGotPasswordResult;
        }

        public IActionResult CheckResetPassword(string code, string acid)
        {
            var result = _userProfileService.CheckResetPassword(code, acid);
            if (result.status == "error")
                return View("AccountMessage", new
                {
                    Title = "Error Activate Account",
                    Message =
                        $"<i class='demo-icon ecs-cancel' style='color:red'></i> Opps! Your activation link is wrong, please try again.",
                    ButtonLink = "/",
                    ButtonMessage = "Back To Home",
                });
            return View("ResetPassword");
        }

        [HttpPost]
        public ActionResult<ApiResponseModel> CheckResetPassword(ResetPasswordViewModel model)
        {
            var code = Request.Query["code"].ToString();
            var acid = Request.Query["acid"].ToString();
            var result = _userProfileService.ResetPassword(code, acid, model);
            if (result.status == "error")
                return result;
            return new ApiResponseModel("success", "Password has been changed successfully, please login to continute.");
        }

        public IActionResult Logout(string returnUrl)
        {
            HttpContext.SignOutAsync();
            return Redirect(string.IsNullOrEmpty(returnUrl) ? "/" : returnUrl);
        }


        public IActionResult Register()
        {
            if (User.Identity.IsAuthenticated)
            {
                return Redirect("/account");
            }
            return View();
        }

        [HttpPost]
        public ActionResult<ApiResponseModel> Register(RegisterAccountViewModel model)
        {
            var registerResult = _userProfileService.Register(model);
            if (registerResult.status == "error")
                return registerResult;
            UserProfile newuser = _userProfileService.GetById(registerResult.data.ToString());
            if (newuser != null)
            {
                AddCookieLogin(newuser.FirstName, newuser.LastName, newuser.Email, newuser.Phone, newuser.Id, newuser.Role);
            }
            return new ApiResponseModel("success",
                "Your account has been created successfully! Check your email to activate account.",
                new { ReturnUrl = string.IsNullOrEmpty(Request.Query["ReturnUrl"].ToString()) ? HttpContext.Session.Get<string>(ConstKey.LoginReturnUrlSessionKey) : Request.Query["ReturnUrl"].ToString() });
        }
    }
}