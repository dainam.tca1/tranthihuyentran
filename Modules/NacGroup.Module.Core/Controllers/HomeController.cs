﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using MediatR;
using NacGroup.Module.Core.Models.Querys;

namespace NacGroup.Module.Core.Controllers
{
    //[Route("{action=Index}")]
    public class HomeController : Controller
    {
        private readonly IMediator _mediator;
        public HomeController(IMediator mediator)
        {
            _mediator = mediator;
        }
        public IActionResult Index()
        {
           
            var request = _mediator.Send(new BlogGetListQuery(1, 15));
            return View(request.Result);
        }
        [Route("bai-viet", Order = 0)]
        public IActionResult Page(int page = 1, int pagesize = 15)
        {
            var request = _mediator.Send(new BlogGetListQuery(page, pagesize));
            return View("Index", request.Result);
        }
        [Route("vay-mua-nha", Order = 0)]
        public IActionResult VayMuaNha() => View("_HomeLoan");

        [Route("vay-mua-xe", Order = 0)]
        public IActionResult VayMuaXe() => View("_VayMuaXe");

        [Route("dao-han-giai-chap", Order = 0)]
        public IActionResult DaoHan() => View("_DaoHan");

        [Route("the-tin-dung", Order = 0)]
        public IActionResult TheTinDung() => View("_TheTinDung");
        public IActionResult Store() => View("Store");

        [Route("kien-thuc-vay-von", Order = 0)]
        public IActionResult Tintuc() => View("News");
    }
}