﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Core.Models.CmsModel;
using NacGroup.Module.Core.Services;

namespace NacGroup.Module.Core.Controllers
{
    [Authorize(Roles = "Admin", Policy = "TenantAuthorize")]
    [Route("cms/api/ConfigEditor/{action=Index}")]
    public class ConfigEditorCmsApiController : Controller
    {
        private readonly  IWebsiteConfigService _websiteConfigService;
        public ConfigEditorCmsApiController(IWebsiteConfigService websiteConfigService)
        {
            _websiteConfigService = websiteConfigService;
        }

        [HttpGet]
        public ActionResult<ApiResponseModel> Index(string key, int page = 1, int pagesize = 10)
        {
            PagedResult<AppSettingCmsModel> paged = _websiteConfigService.Filter(key, page, pagesize);
            return new ApiResponseModel("success", paged);
        }

        [HttpPost]
        public ActionResult<ApiResponseModel> RemoveList([FromBody] List<string> idList)
        {
            return _websiteConfigService.Delete(idList);
        }

        [HttpPost]
        public ActionResult<ApiResponseModel> Add([FromBody] AppSettingCmsModel model)
        {
            return _websiteConfigService.Add(model);
        }

        [HttpGet]
        public ActionResult<ApiResponseModel> Update(string id)
        {
            var item = _websiteConfigService.GetById(id);
            if (item == null)
                return new ApiResponseModel("error", StaticMessage.DataNotFound);

            return new ApiResponseModel("success", item);
        }
        [HttpPost]
        public ActionResult<ApiResponseModel> Update([FromBody] AppSettingCmsModel model)
        {
            try
            {
                return _websiteConfigService.Update(model);
            }
            catch
            {
                return new ApiResponseModel("error", StaticMessage.ParameterInvalid);
            }

        }
    }
}