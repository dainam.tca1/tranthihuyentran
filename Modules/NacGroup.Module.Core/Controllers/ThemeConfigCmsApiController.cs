﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Core.Models.CmsModel;
using NacGroup.Module.Core.Services;

namespace NacGroup.Module.Core.Controllers
{
    [Authorize(Roles = "Admin", Policy = "TenantAuthorize")]
    [Route("cms/api/themeconfig/{action=Index}")]
    public class ThemeConfigCmsApiController : Controller
    {
        private readonly IThemeConfigService _themeConfigService;
        public ThemeConfigCmsApiController(IThemeConfigService themeConfigService)
        {
            _themeConfigService = themeConfigService;
        }
        [HttpPost]
        public ActionResult<ApiResponseModel> Add([FromBody] ThemeConfigModel model) => _themeConfigService.Add(model);


        public ActionResult<ApiResponseModel> Index(string key, int page = 1, int pagesize = 10)
        {
            return new ApiResponseModel("success", _themeConfigService.Filter(key, page, pagesize));
        }

        [HttpPost]
        public ActionResult<ApiResponseModel> RemoveList([FromBody] List<string> idList)
        {
            return _themeConfigService.Delete(idList);
        }

        public ActionResult<ApiResponseModel> GetUpdate(string id)
        {
            var item = _themeConfigService.GetByKey(id);
            if (item == null)
                return new ApiResponseModel("error", StaticMessage.DataNotFound);

            return new ApiResponseModel("success", item);
        }
        [HttpPost]
        public ActionResult<ApiResponseModel> Update([FromBody] ThemeConfigModel model)
        {
            return _themeConfigService.Update(model);
        }

    }
}