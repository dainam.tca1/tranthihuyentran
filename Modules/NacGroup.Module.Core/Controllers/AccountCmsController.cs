﻿
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NacGroup.Infrastructure;
using NacGroup.Module.Core.Interfaces;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Core.Models.CmsModel;
using NacGroup.Module.Core.Services;

namespace NacGroup.Module.Core.Controllers
{
    [Authorize(Roles = "Admin", Policy = "TenantAuthorize")]
    [Route("admin/account/{action=Index}")]
    public class AccountCmsController : Controller
    {
        private readonly IUserProfileCmsService _userProfileCmsService;
        private readonly LanguageType _lang;
        public AccountCmsController(IUserProfileCmsService userProfileCmsService, ITenantProvider tenantProvider)
        {
            _userProfileCmsService = userProfileCmsService;
            _lang = tenantProvider.GetTenant().DefaultLanguage;
        }
        public IActionResult ChangePasswordAdmin()
        {
            return View();
        }
        [HttpPost]
        public ActionResult<ApiResponseModel> UpdateAdminPassword(ChangePasswordCmsModel model)
        {
            var lang = string.IsNullOrEmpty(HttpContext.Request.Cookies["cmslang"]) ? _lang : (LanguageType)int.Parse(HttpContext.Request.Cookies["cmslang"]);
            if (User.Identity.IsAuthenticated == false)
            {
                return new ApiResponseModel("error", CoreStaticMessage.PleaseLoginFirst[(int)lang]);
            }
            //get User ID
            model.Id = User.Identity.Name;
            var profileResult = _userProfileCmsService.UpdateAdminPassword(model,lang);
            if (profileResult.status == "error")
                return profileResult;
            return new ApiResponseModel("success",
                CoreStaticMessage.UpdatePasswordSuccessfull[(int)lang]);
        }

    }
}