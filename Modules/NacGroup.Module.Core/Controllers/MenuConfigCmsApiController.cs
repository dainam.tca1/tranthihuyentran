﻿using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Core.Models.Schema;
using NacGroup.Module.Core.Services;
using Newtonsoft.Json.Linq;

namespace NacGroup.Module.Core.Controllers
{
    [Authorize(Roles = "Admin", Policy = "TenantAuthorize")]
    [Route("cms/api/menuconfig/{action=Index}")]
    public class MenuConfigCmsApiController : Controller
    {
        private readonly IMenuConfigService _menuConfigService;

        public MenuConfigCmsApiController(IMenuConfigService menuConfigService)
        {
            _menuConfigService = menuConfigService;

        }

        [HttpGet]
        public ActionResult<ApiResponseModel> Index()
        {
            return new ApiResponseModel("success", _menuConfigService.Get());
        }


        [HttpPost]
        public ActionResult<ApiResponseModel> Remove([FromBody] string id)
        {
            return _menuConfigService.Delete(id);
        }

        [HttpPost]
        public ActionResult<ApiResponseModel> Add([FromBody] MenuConfig model)
        {
            return _menuConfigService.Add(model);
        }

        [HttpPost]
        public ActionResult<ApiResponseModel> Update([FromBody] MenuConfig model)
        {
            return _menuConfigService.Update(model);
        }

        [HttpPost]
        public ActionResult<ApiResponseModel> UpdateSort([FromBody]JArray data)
        {
            if (data == null) return new ApiResponseModel("error", StaticMessage.ParameterInvalid);
            var result = new ApiResponseModel("error", StaticMessage.ParameterInvalid);
            foreach (var jToken in data)
            {
                result = _menuConfigService.UpdateCustomize(new MenuConfig
                {
                    Id = jToken["Id"].ToString(),
                    Sort = Convert.ToInt32(jToken["Sort"])
                }, "Sort");
            }

            return result;
        }

    }
}
