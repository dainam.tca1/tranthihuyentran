﻿
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NacGroup.Infrastructure;
using NacGroup.Module.Core.Interfaces;
using NacGroup.Module.Core.Models;
using NacGroup.Module.Core.Models.Schema;
using NacGroup.Module.Core.Models.ViewModels;
using NacGroup.Module.Core.Services;
using System.Collections.Generic;
using System.Linq;

namespace NacGroup.Module.Core.Controllers
{
    [Authorize(Roles = "Admin", Policy = "TenantAuthorize")]
    [Route("cms/api/admin-mannager/{action=Index}")]
    public class AccountCmsApiController : Controller
    {
        private readonly IUserProfileCmsService _userProfileCmsService;
        private readonly LanguageType _lang;
     

        public AccountCmsApiController(IUserProfileCmsService userProfileCmsService, ITenantProvider tenantProvider)
        {
            _userProfileCmsService = userProfileCmsService;
            _lang = tenantProvider.GetTenant().DefaultLanguage;
            //_lang = string.IsNullOrEmpty(HttpContext.Request.Cookies["cmslang"]) ? tenantProvider.GetTenant().DefaultLanguage : (LanguageType)int.Parse(HttpContext.Request.Cookies["cmslang"]);
        }
        [HttpGet]
        public ActionResult<ApiResponseModel> Index ()
        {
            var paged = _userProfileCmsService.GetAllByRole(RoleType.Admin).ToList();
            return new ApiResponseModel("success", new PagedResult<UserProfile>(paged, 1, paged.Count, paged.Count));
        }
        [HttpPost]
        public ActionResult<ApiResponseModel> RemoveList([FromBody] List<string> idList)
        {
            return _userProfileCmsService.Delete(idList);
        }

        [HttpPost]
        public ActionResult<ApiResponseModel> AddAdminUser([FromBody] RegisterAccountViewModel model)
        {
            var registerResult = _userProfileCmsService.RegisterAdmin(model, _lang);
            if (registerResult.status == "error")
                return registerResult;
            return new ApiResponseModel("success",
                CoreStaticMessage.AddAdminSuccess[(int)_lang]);
        }
    }
}